﻿using Askom.AddonManager;
using ELPLATFORMA.CompensationComponents.Bootstrapper;
using ELPLATFORMA.CompensationComponents.Controls;
using ELPLATFORMA.CompensationComponents.Services;
using EvoObject.SynopticModule.Models;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace EvoObject.SynopticModule
{
    public partial class Synoptic : EvoObjectExtension
    {
        private bool isInitialized = false;
        
        private static readonly object _varsLocker = new object();

        public Dictionary<string, object> Vars = new Dictionary<string, object>();

        

        public Synoptic()
        {
            InitializeComponent();
            

            //isInitialized = true;
            //OnEvoObjectPropertyChangedEvent(new EvoObjectPropertyChangedEventArgs(nameof(SchemePath), SchemePath, SchemePath.GetType()));
        }

        //private void Synoptic_Layout(object sender, LayoutEventArgs e)
        //{
        //    isInitialized = true;
        //    OnEvoObjectPropertyChangedEvent(new EvoObjectPropertyChangedEventArgs(nameof(SchemePath), SchemePath, SchemePath.GetType()));
        //}

        #region EvoObjectBase overrides
        public override string EvoObjectID => "Schemat kompensacji";

        public override string EvoObjectDescription => "Schemat kompensacji";

        private void label2_Click(object sender, EventArgs e)
        {
           
        }
        #endregion EvoObjectBase overrides

        #region Control properties
        private string _SchemePath;
        [EvoObjectProperty(Description = "Ścieżka pliku schematu", EditorType = EditorType.Text )]
        public string SchemePath
        {
            get { return _SchemePath; }
            set
            {
                _SchemePath = value;
                this.ReportMessage(AddonMessageType.Info, "[Synoptic] Ustawiono właściowść SchemePath");
                OnEvoObjectPropertyChangedEvent(new EvoObjectPropertyChangedEventArgs(nameof(SchemePath), value, typeof(string)));

            }
        }

        private Color _BackgroundColor;
        [EvoObjectProperty(Description = "Kolor tła", EditorType = EditorType.Color)]
        public Color BackgroundColor
        {
            get { return _BackgroundColor; }
            set
            {
                _BackgroundColor = value;
                this.ReportMessage(AddonMessageType.Info, "[Synoptic] Smieniono kolor tła");
                OnEvoObjectPropertyChangedEvent(new EvoObjectPropertyChangedEventArgs(nameof(BackgroundColor), value, typeof(Color)));
            }
        }

        #endregion Control properties


    }
}
