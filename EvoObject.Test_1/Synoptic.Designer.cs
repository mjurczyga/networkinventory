﻿using ELPLATFORMA.CompensationComponents.Views;
using System;

namespace EvoObject.SynopticModule
{
    partial class Synoptic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            //this.synoptic1 = new SchemeControlsLib.Views.Synoptic();
            //this.synoptic1.VarsBindingsDone += Synoptic1_VarsBindingsDone;

            this.mainContainer = new MainContainer(this);

            this.SuspendLayout();
            // 
            // elementHost1
            // 
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(0, 0);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(946, 632);
            this.elementHost1.TabIndex = 0;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = this.mainContainer;
            // 
            // Test_1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.elementHost1);
            this.Name = "Test_1";
            this.Size = new System.Drawing.Size(946, 632);
            this.ResumeLayout(false);

        }

        private void Synoptic1_VarsBindingsDone(object sender, SchemeControlsLib.Controls.Events.VarsBindingsDoneEventArgs e)
        {
            //this.RegisterVarsStateChangedEvents(e.VarsNames);


            //this.synoptic1?.SetVars(Vars);

        }

        #endregion
        private SchemeControlsLib.Views.Test test1;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        //private SchemeControlsLib.Views.Synoptic synoptic1;
        private MainContainer mainContainer;
    }
}
