﻿using ELPLATFORMA.CompensationComponents.ViewModels;
using ELPLATFORMA.CompensationComponents.Views;
using System;

namespace TestAsixa
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.mainContainer = new MainContainer();
            this.SuspendLayout();
            // 
            // elementHost1
            // 
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(0, 0);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(991, 623);
            this.elementHost1.TabIndex = 0;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = this.mainContainer;

            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(991, 623);
            this.Controls.Add(this.elementHost1);
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        private void Synoptic1_VarsBindingsDone(object sender, SchemeControlsLib.Controls.Events.VarsBindingsDoneEventArgs e)
        {
            var a = 1;

            string vars = "";
            foreach(var var in e.VarsNames)
            {
                vars += var + Environment.NewLine;
            }
        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private MainContainer mainContainer;
    }
}

