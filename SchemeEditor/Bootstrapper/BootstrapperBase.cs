﻿using Autofac;
using CommonServiceLocator;
using SchemeEditor.Attributes;
using SchemeEditor.Services;
using SchemeEditor.Services.RegionRegistrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SchemeEditor.Bootstrapper
{
    public class BootstrapperBase
    {
        protected IContainer _container;

        public BootstrapperBase()
        {
            var containerBuilder = new ContainerBuilder();

            RegisterTypesByAttribute(containerBuilder);

            ConfigureContainer(containerBuilder);

            _container = containerBuilder.Build();

            RegionControlRegistrator.SetContainer(_container);

            ServiceLocator.SetLocatorProvider(() => new Autofac.Extras.CommonServiceLocator.AutofacServiceLocator(_container));
        }

        private void RegisterTypesByAttribute(ContainerBuilder containerBuilder)
        {
            var typesToRegister = LoadTypesToRegister<ContainerRegistration>().ToArray();
            foreach (var item in typesToRegister)
            {
                if (item.Value.RegisterAs != null)
                {
                    if (item.Value.SingleInstance)
                    {
                        containerBuilder.RegisterType(item.Key).As(item.Value.RegisterAs).SingleInstance();
                    }
                    else
                    {
                        containerBuilder.RegisterType(item.Key).As(item.Value.RegisterAs);
                    }
                }
                else
                {
                    if (item.Value.SingleInstance)
                    {
                        containerBuilder.RegisterType(item.Key).AsSelf().SingleInstance();
                    }
                    else
                    {
                        containerBuilder.RegisterType(item.Key).AsSelf();
                    }
                }
            }
        }

        protected virtual void ConfigureContainer(ContainerBuilder containerBuilder)
        {

        }

        private static List<Assembly> GetListOfEntryAssemblyWithReferences()
        {
            List<Assembly> listOfAssemblies = new List<Assembly>();
            var mainAsm = Assembly.GetEntryAssembly();
            listOfAssemblies.Add(mainAsm);

            foreach (var refAsmName in mainAsm.GetReferencedAssemblies())
            {
                listOfAssemblies.Add(Assembly.Load(refAsmName));
            }
            return listOfAssemblies;
        }

        private Dictionary<Type, T> LoadTypesToRegister<T>() where T : Attribute
        {
            var types = new List<Type>();
            var result = new Dictionary<Type, T>();

            var assemblyes = GetListOfEntryAssemblyWithReferences();

            foreach (var item in assemblyes)
            {
                types.AddRange(item.GetTypes());
            }

            foreach (Type type in types)
            {
                foreach (Attribute attribute in type.GetCustomAttributes(typeof(T), true))
                    result.Add(type, (T)attribute);
            }

            return result;
        }
    }
}
