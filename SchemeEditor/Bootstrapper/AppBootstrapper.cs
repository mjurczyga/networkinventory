﻿using Autofac;
using SchemeControlsLib.Controls;
using SchemeControlsLib.Services;
using SchemeEditor.Services;
using SchemeEditor.Services.EditorServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeEditor.Bootstrapper
{
    public class AppBootstrapper : BootstrapperBase
    {
        protected override void ConfigureContainer(ContainerBuilder containerBuilder)
        {
            base.ConfigureContainer(containerBuilder);
            containerBuilder.RegisterType<VarsService>().As<IVarsService>().SingleInstance();
            containerBuilder.RegisterType<SchemeService>().As<ISchemeService>().SingleInstance();
        }

        public void Start()
        {
            Settings.IsITSApp = true;

            var dtf = _container.Resolve<DataTemplateFactory>();
            dtf.InitMapping();
        }
    }
}
