﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace SchemeEditor.Converters
{
    /// <summary>
    /// Konwerter pobiera pozycję (TOP / LEFT) elementu umieszczonego na Canvas'ie.
    /// </summary>
    public class UIElementCanvasPositionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && parameter != null)
            {
                if (value is UIElement)
                {
                    if (parameter.ToString().ToUpper() == "TOP")
                        return Canvas.GetTop((UIElement)value);
                    else if (parameter.ToString().ToUpper() == "LEFT")
                        return Canvas.GetLeft((UIElement)value);
                }
            }

            return "---";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
