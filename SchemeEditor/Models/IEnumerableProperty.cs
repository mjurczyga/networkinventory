﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeEditor.Models
{
    public class IEnumerableProperty : DependencyObject
    {
        public IEnumerable<string> Value
        {
            get { return (IEnumerable<string>)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
            nameof(Value),
            typeof(IEnumerable<string>),
            typeof(IEnumerableProperty),
            new PropertyMetadata(null));
    }
}
