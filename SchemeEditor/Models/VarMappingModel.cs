﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeEditor.Models
{

    public class VarMappingModel : ObservableObject
    {
        private string _TypeName;
        public string TypeName { get { return _TypeName; } set { _TypeName = value; RaisePropertyChanged(); } }

        private string _ControlName;
        public string ControlName
        {
            get { return _ControlName; }
            set { _ControlName = value; RaisePropertyChanged(); }
        }

        private PropertyClass _StateVarNameProperty;
        public PropertyClass StateVarNameProperty { get { return _StateVarNameProperty; } set { _StateVarNameProperty = value; RaisePropertyChanged(() => StateVarNameProperty); } }

        private string _VarsName;
        public string VarsName
        {
            get { return _VarsName; }
            set { _VarsName = value; RaisePropertyChanged(); }
        }
        /*  private List<PropertyVarBindingModel> _Properties = new List<PropertyVarBindingModel>();
          public List<PropertyVarBindingModel> Properties { get { return _Properties; } set { _Properties = value; RaisePropertyChanged(); } }*/

        private string _ClickEvent;
        public string ClickEvent
        {
            get { return _ClickEvent; }
            set { _ClickEvent = value; RaisePropertyChanged(() => StateVarNameProperty); }
        }


        public VarMappingModel()
        {

        }

        public VarMappingModel(VarMappingModel varMappingModel)
        {
            this.TypeName = varMappingModel.TypeName;
            this.ControlName = varMappingModel.ControlName;
            this.StateVarNameProperty = varMappingModel.StateVarNameProperty;
            this.VarsName = varMappingModel.VarsName;
            this.ClickEvent = varMappingModel.ClickEvent;
            //Properties = varMappingModel.Properties;
        }

        public VarMappingModel(string typeName, string controlName, PropertyClass stateVarNameProperty, string varsName, object clickEvent)//List<PropertyVarBindingModel> property)
        {
            this.TypeName = typeName;
            this.ControlName = controlName;
            this.StateVarNameProperty = stateVarNameProperty;
            this.VarsName = varsName;
            this.ClickEvent = clickEvent?.ToString();
            //Properties = property;
        }

        public override bool Equals(object obj)
        {
            if (obj is VarMappingModel)
            {
                var compared = (VarMappingModel)obj;
                if (this.TypeName == compared.TypeName && this.ControlName == compared.ControlName && this.StateVarNameProperty == compared.StateVarNameProperty && this.ClickEvent == compared.ClickEvent)
                    return true;
            }
            return false;
        }

    }

    public class PropertyVarBindingModel : ObservableObject
    {
        private PropertyClass _StateVarNameProperty;
        public PropertyClass StateVarNameProperty { get { return _StateVarNameProperty; } set { _StateVarNameProperty = value; RaisePropertyChanged(() => StateVarNameProperty); } }

        private string _VarsName;
        public string VarsName
        {
            get { return _VarsName; }
            set { _VarsName = value; RaisePropertyChanged(); }
        }

        public PropertyVarBindingModel(string propertyName, string propertyText, string varsName)
        {
            StateVarNameProperty = new PropertyClass(propertyName, propertyText);
            VarsName = varsName;
        }

        public PropertyVarBindingModel(PropertyClass stateVarNameProperty, string varsName)
        {
            StateVarNameProperty = stateVarNameProperty;
            VarsName = varsName;
        }


    }

    public class PropertyClass : ObservableObject
    {
        private string _PropertyName;
        public string PropertyName { get { return _PropertyName; } set { _PropertyName = value; RaisePropertyChanged("PropertyName"); } }

        private string _PropertyText;
        public string PropertyText { get { return _PropertyText; } set { _PropertyText = value; RaisePropertyChanged("PropertyText"); } }

        public PropertyClass()
        {
        }

        public PropertyClass(string propertyName, string propertyText)
        {
            PropertyName = propertyName;
            PropertyText = propertyText;
        }
    }
}
