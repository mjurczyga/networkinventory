﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeEditor.ViewModels.BaseViewModels
{
    public interface IBaseViewModel
    {
        void OnLoaded();
        void OnUnloaded();
        void StartAction<T>(Action<T> action) where T : class, IBaseViewModel;
    }
}
