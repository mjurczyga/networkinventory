﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using SchemeEditor.Attributes;
using SchemeEditor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeEditor.ViewModels.BaseViewModels
{
    [ContainerRegistration]
    public abstract class BaseViewModel : ObservableObject, IBaseViewModel
    {
        public BaseViewModel()
        {
            Messenger.Default.Register<ApplicationClosingMessage>(this, OnClosingMessage);
        }

        public virtual void OnLoaded()
        {

        }

        protected void OnClosingMessage(ApplicationClosingMessage applicationClosingMessage)
        {
            OnUnloaded();
        }

        public virtual void OnUnloaded()
        {

        }

        protected virtual string GetClassName()
        {
            return this.GetType().Name;
        }

        void IBaseViewModel.StartAction<T>(Action<T> action)
        {
            action(this as T);
        }
    }
}
