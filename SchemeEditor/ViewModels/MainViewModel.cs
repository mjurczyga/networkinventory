﻿using SchemeControlsLib.Controls.Core;
using GalaSoft.MvvmLight.Command;
using SchemeEditor.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SchemeEditor.Attributes;
using SchemeEditor.ViewModels.BaseViewModels;
using SchemeEditor.Services;
using SchemeEditor.ViewModels.AdditionalWindowsViewModels;
using SchemeControlsLib.Utilities;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Models;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Inputs;
using System.Windows.Threading;
using System.IO;
using Microsoft.Win32;
using Autofac;
using SchemeControlsLib.SchemeControlTypes;
using SchemeControlsLib.Controls.Interfaces.CoreElementsInterfaces;
using SchemeControlsLib.Controls.Attributes;
using System.Reflection;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using SchemeEditor.Services.Asix.Models;
using SchemeEditor.Services.Asix;
using SchemeEditor.ViewModels.Asix;
using SchemeControlsLib.Services;
using SchemeControlsLib.Controls;

namespace SchemeEditor.ViewModels
{
    [AutoViewMapper("MainView")]
    public class MainViewModel : BaseViewModel
    {
        private readonly IRegionManager _regionManager;
        private readonly IComponentContext _componentContext;
        private readonly IAsixService _asixService;
        private readonly ISchemeService _schemeService;

        private double _gridLinesFrequency = 5;
        private SchemeCanvas _canvas;

        private bool _SchemeLoading = false;
        public bool SchemeLoading
        {
            get { return _SchemeLoading; }
            set { _SchemeLoading = value; RaisePropertyChanged(() => SchemeLoading); }
        }

        private bool _SchemeSaving = false;
        public bool SchemeSaving
        {
            get { return _SchemeSaving; }
            set { _SchemeSaving = value; RaisePropertyChanged(() => SchemeSaving); }
        }


        public Dictionary<string, ElectricElementBase> Elements { get; set; } = new Dictionary<string, ElectricElementBase>();

        public double GridLinesFrequency { get { return _gridLinesFrequency; } set { _gridLinesFrequency = value; RaisePropertyChanged("GridLinesFrequency"); } }

        public KeyValuePair<string, Type> ToolboxSelectedItem { get; set; }

        public ICommand CanvasLoadedCommand { get; set; }

        public ICommand ElementsListSelectionChangedCommand { get; set; }
        public ICommand SaveCommand { get; set; }
        public ICommand SaveAsCommand { get; set; }
        public ICommand LoadCommand { get; set; }
        public ICommand VarsMapping { get; set; }
        public ICommand GenerateEvoVarsCommand { get; set; }

        #region Widok właściwości kontrolki
        private void AddPropertiesContent(object control)
        {
            var converter = new System.Windows.Media.BrushConverter();

            ElementPropertiesControl propertiesDataContent = new ElementPropertiesControl((FrameworkElement)control) { Foreground = Brushes.White,
                Background = (Brush)converter.ConvertFromString("#1e1e1e"), Text = "Właściwości",
                BorderBrush = (Brush)converter.ConvertFromString("#333337") };
            propertiesDataContent.ElementPropertiesChangedEvent += OnElementPropertiesChanged;


            PropertiesContent = propertiesDataContent;
        }

        private void RemovePropertiesContent()
        {
            if (PropertiesContent != null)
            {
                var propertiesContent = (ElementPropertiesControl)PropertiesContent;
                propertiesContent.ElementPropertiesChangedEvent -= OnElementPropertiesChanged;
                PropertiesContent = null;
            }
        }

        private object _PropertiesContent;
        public object PropertiesContent { get { return _PropertiesContent; } set { _PropertiesContent = value; RaisePropertyChanged("PropertiesContent"); } }
        #endregion

        public MainViewModel(IComponentContext componentContext, IRegionManager regionManager, IAsixService asixService, ISchemeService schemeService) : base()
        {
            _regionManager = regionManager;
            _componentContext = componentContext;
            _asixService = asixService;
            _schemeService = schemeService;

            InitCommands();
         
        }

        public ICommand HeaderMouseLeftButtonDownCommand { get; private set; }

        private void InitCommands()
        {
            CanvasLoadedCommand = new RelayCommand<Canvas>(canvas => OnCanvasLoadedCommand(canvas));
            ElementsListSelectionChangedCommand = new RelayCommand<SelectionChangedEventArgs>(e => OnElementsListSelectionChangedCommand(e));

            SaveCommand = new RelayCommand(OnSaveCommand);
            SaveAsCommand = new RelayCommand(OnSaveAsCommand);
            LoadCommand = new RelayCommand(OnLoadCommand);
            VarsMapping = new RelayCommand(OnVarsMappingCommand);
            GenerateEvoVarsCommand = new RelayCommand(OnGenerateEvoVarsCommand);
            HeaderMouseLeftButtonDownCommand = new RelayCommand<MouseButtonEventArgs>(OnHeaderMouseLeftButtonDown);
        }

        private void OnGenerateEvoVarsCommand()
        {

            //GenerateAsixEvoExcelVars();
            var asixVarsDatabaseGeneratorViewModel = _componentContext.Resolve<AsixVarsDatabaseGeneratorViewModel>();
            _regionManager.ShowNewWindow(asixVarsDatabaseGeneratorViewModel, true, true, "Export zmiennych do asix", WindowStyle.ToolWindow);

        }

        private void GenerateAsixEvoExcelVars()
        {
            // Displays a SaveFileDialog so the user can save the Image
            // assigned to Button2.
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Plik xls|*.xls";
            saveFileDialog1.Title = "Zapisz wygenerowane zmienne do xls";
            saveFileDialog1.ShowDialog();

            var plcVars = "";
            int MBNumber = 0;

            // If the file name is not an empty string open it for saving.
            if (saveFileDialog1.FileName != "")
            {
                using (var fs = new FileStream(saveFileDialog1.FileName, FileMode.Create, FileAccess.Write))
                {
                    IWorkbook workbook = new HSSFWorkbook();

                    ISheet sheet1 = workbook.CreateSheet("Sheet1");
                    var rowIndex = 0;
                    IRow row = sheet1.CreateRow(rowIndex);

                    int i = 0;
                    foreach (var header in HeadersList)
                    {
                        var cell = row.CreateCell(i);
                        cell.SetCellValue(header);
                        i++;
                    }
                    rowIndex++;

                    if (_canvas != null)
                    {
                        var activeElements = _canvas.Children.OfType<ActiveElement>();

                        List<Variable> variableList = new List<Variable>();
                        foreach (var activeElement in activeElements)
                        {
                            var className = activeElement.GetType().GetCustomAttribute<EditorType>(false);
                            var stateProperties = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(activeElement, ElementPropertiesModels.GetPropertiesByElementType(activeElement)).ToDictionary(x => x.Key as DependencyProperty, x => x.Value).Where(x => x.Key.Name.Contains("BindingVarName"));


                            foreach (var stateProperty in stateProperties)
                            {
                                var schemeElementVarName = activeElement.GetValue(stateProperty.Key)?.ToString();
                                if (!string.IsNullOrEmpty(schemeElementVarName))
                                {
                                    IRow varRow = sheet1.CreateRow(rowIndex);
                                    int cellIndex = 0;
                                    foreach(var header in HeadersList)
                                    {
                                        if (header == "Nazwa")
                                            varRow.CreateCell(cellIndex).SetCellValue(schemeElementVarName);
                                        else if (header == "Adres")
                                            varRow.CreateCell(cellIndex).SetCellValue("HR" + (12288 + rowIndex).ToString());
                                        else if (header == "Liczba elementów")
                                            varRow.CreateCell(cellIndex).SetCellValue(1);
                                        else if (header == "Okres próbkowania")
                                            varRow.CreateCell(cellIndex).SetCellValue(1);
                                        else if (header == "Funkcja przeliczająca")
                                            varRow.CreateCell(cellIndex).SetCellValue("NIC_INT");
                                        else if (header == "Kanał")
                                            varRow.CreateCell(cellIndex).SetCellValue("KANALT");
                                        else if (header == "Sprawdzanie zakresu pomiarowego")
                                            varRow.CreateCell(cellIndex).SetCellValue(0);
                                        else if (header == "Rejestracja sterowania")
                                            varRow.CreateCell(cellIndex).SetCellValue(0);
                                        else if (header == "Zmienna nieaktywna")
                                            varRow.CreateCell(cellIndex).SetCellValue(0);



                                        cellIndex++;

                                    }
                                    plcVars += $"{schemeElementVarName} AT%MB{MBNumber.ToString()} : INT; (* {schemeElementVarName} *)" + System.Environment.NewLine;
                                    MBNumber += 2;
                                    rowIndex++;
                                }
                                    
                            }
                        }
                    }

                    workbook.Write(fs);

                    

                    File.WriteAllText(saveFileDialog1.FileName.Replace(".xls", ".txt"), plcVars);
                    GenerateAsixEvoXmlVars(saveFileDialog1.FileName.Replace(".xls", ".xml"));
                }
            }
        }

        private void GenerateAsixEvoXmlVars(string fileName)
        {
            VariableBase variableBase = new VariableBase();
            variableBase.Languages = new Languages() { Language = new List<int>() { 21 } };

            List<XML_Attribute> attribute = new List<XML_Attribute>();
            attribute.Add(new XML_Attribute()
            {
                Name = "Name",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Nazwa" },
                    new LocalName() { LangId = 9, Text = "Name" }
                }
            });
            attribute.Add(new XML_Attribute()
            {
                Name = "Description",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Opis" },
                    new LocalName() { LangId = 9, Text = "Description" }
                }
            });
            attribute.Add(new XML_Attribute()
            {
                Name = "ValueType",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Typ wartości" },
                    new LocalName() { LangId = 9, Text = "Value type" }
                }
            });
            attribute.Add(new XML_Attribute()
            {
                Name = "Name",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Format" },
                    new LocalName() { LangId = 9, Text = "Format" }
                }
            });
            attribute.Add(new XML_Attribute()
            {
                Name = "Channel",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Kanał" },
                    new LocalName() { LangId = 9, Text = "Channel" }
                }
            });
            attribute.Add(new XML_Attribute()
            {
                Name = "Address",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Adres" },
                    new LocalName() { LangId = 9, Text = "Address" }
                }
            });
            attribute.Add(new XML_Attribute()
            {
                Name = "SampleRate",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Częstość odświeżania" },
                    new LocalName() { LangId = 9, Text = "Sampling Period" }
                }
            });
            attribute.Add(new XML_Attribute()
            {
                Name = "ConversionFunction",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Funkcja przeliczająca" },
                    new LocalName() { LangId = 9, Text = "Converting Function" }
                }
            });
            attribute.Add(new XML_Attribute()
            {
                Name = "ReadConvertingFunction",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Funkcja przeliczająca odczytu" },
                    new LocalName() { LangId = 9, Text = "Read Converting Function" }
                }
            });
            attribute.Add(new XML_Attribute()
            {
                Name = "WriteConvertingFunction",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Funkcja przeliczająca zapisu" },
                    new LocalName() { LangId = 9, Text = "Write Converting Function" }
                }
            });
            attribute.Add(new XML_Attribute()
            {
                Name = "ItemNotActive",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Nieaktywna" },
                    new LocalName() { LangId = 9, Text = "Not active" }
                }
            });
            attribute.Add(new XML_Attribute()
            {
                Name = "DisplayRangeFrom",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Zakres wyświetlania od" },
                    new LocalName() { LangId = 9, Text = "Minimum Display Value" }
                }
            });
            attribute.Add(new XML_Attribute()
            {
                Name = "DisplayRangeTo",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Zakres wyświetlania do" },
                    new LocalName() { LangId = 9, Text = "Maximum Display Value" }
                }
            });

            variableBase.Attributes = new XML_Attributes() { Attribute = attribute, Count = attribute.Count() };


            if (_canvas != null)
            {
                var activeElements = _canvas.Children.OfType<ActiveElement>();

                List<Variable> variableList = new List<Variable>();
                foreach (var activeElement in activeElements)
                {
                    var className = activeElement.GetType().GetCustomAttribute<EditorType>(false);
                    var stateProperties = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(activeElement, ElementPropertiesModels.GetPropertiesByElementType(activeElement)).ToDictionary(x => x.Key as DependencyProperty, x => x.Value).Where(x => x.Key.Name.Contains("BindingVarName"));

                    foreach (var stateProperty in stateProperties)
                    {
                        var schemeElementVarName = activeElement.GetValue(stateProperty.Key)?.ToString();
                        if (!string.IsNullOrEmpty(schemeElementVarName))
                            variableList.Add(new Variable(schemeElementVarName, "KANALTESTOWY", "Int16"));
                    }
                }
                variableBase.Variables = new Variables() { Count = variableList.Count(), Variable = variableList };
            }



            //// Displays a SaveFileDialog so the user can save the Image
            //// assigned to Button2.
            //SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            //saveFileDialog1.Filter = "Plik xml|*.xml";
            //saveFileDialog1.Title = "Zapisz wygenerowane zmienne do XML";
            //saveFileDialog1.ShowDialog();

            //// If the file name is not an empty string open it for saving.
            //if (saveFileDialog1.FileName != "")
            //{
                variableBase.SerializeXMLData(fileName);
                //using (StreamWriter streamWriter = new StreamWriter(saveFileDialog1.FileName, false))
                //{
                //    streamWriter.Write(outXml);
                //}
            //}
        }

        public Visibility lbVisibility { get; set; } = Visibility.Visible;

        public KeyValuePair<string, SpecificPlace> LbSelectedItem
        {
            get;
            set;
        } = new KeyValuePair<string, SpecificPlace>("Całość", new SpecificPlace(0, 0, 1));

        public ObservableDictionary<string, SpecificPlace> SpecificPlaces { get; private set; } = new ObservableDictionary<string, SpecificPlace>();

        private void OnHeaderMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                var a = 1;
                if(lbVisibility == Visibility.Visible)
                    lbVisibility = Visibility.Collapsed;
                else if (lbVisibility == Visibility.Collapsed)
                    lbVisibility = Visibility.Visible;
            }
        }

        private void OnVarsMappingCommand()
        {
            VarsMappingViewModel varsMappingViewModel = _componentContext.Resolve<VarsMappingViewModel>();
            varsMappingViewModel.SchemeCanvas = _canvas;
            Window window = new Window() { Content = varsMappingViewModel};
            window.Show();
            window.Height = SystemParameters.PrimaryScreenHeight;

            var specZoomPlaces = _canvas.Children.OfType<ISpecificZoomPlace>();
            var specificPlaces = new ObservableDictionary<string, SpecificPlace>();
            if(!specificPlaces.ContainsKey("Całość"))
                specificPlaces.Add("Całość", new SpecificPlace(0, 0, 1));
            foreach (var specZoomPlace in specZoomPlaces)
            {
                if (!specificPlaces.ContainsKey(specZoomPlace.PlaceName))
                    specificPlaces.Add(specZoomPlace.PlaceName, specZoomPlace.SpecificPlace);
            }

            SpecificPlaces = specificPlaces;

            RaisePropertyChanged("SpecificPlaces");
        }

        private void OnSaveAsCommand()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                SchemeSaving = true;
                ShowActionExecutingWindow(() => { _canvas.SaveScheme(openFileDialog.FileName); }, "Zapisywanie schematu...");
                LibMessagerService.ShowMessage("Zapisano schemat w lokalizacji: " + openFileDialog.FileName, LibMessageTypes.INFO);
                SchemeSaving = false;
            }

        }

        private void OnSaveCommand()
        {
            if (string.IsNullOrEmpty(_canvas.LoadedFileUri))
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                if (openFileDialog.ShowDialog() == true)
                {
                    SchemeSaving = true;
                    ShowActionExecutingWindow(() => { _canvas.SaveScheme(openFileDialog.FileName); }, "Zapisywanie schematu...");
                    LibMessagerService.ShowMessage("Zapisano schemat w lokalizacji: " + openFileDialog.FileName, LibMessageTypes.INFO);
                    SchemeSaving = false;
                }
            }
            else
                ShowActionExecutingWindow(() => { _canvas.SaveScheme(_canvas.LoadedFileUri); }, "Zapisywanie schematu...");
        }

        private void OnLoadCommand()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                SchemeLoading = true;
                //_canvas.LoadedFileUri = openFileDialog.FileName;
                ShowActionExecutingWindow(() => 
                {
                    _canvas.LoadScheme(openFileDialog.FileName);
                }, "Wczytywanie schematu...");
                SchemeLoading = false;
            }
        }  

        private void OnElementsListSelectionChangedCommand(SelectionChangedEventArgs e)
        {
            var x = (KeyValuePair<string, MultiSourceBrushControl>)((ListBox)e.Source).SelectedItem;

            _canvas.SelectElement(x.Value);
        }      

        private void OnCanvasLoadedCommand(Canvas canvas)
        {
            _canvas = canvas as SchemeCanvas;
            _canvas.SelectedElementChangedEvent += OnSelectedElementChanged;
            _schemeService.SetSchemeCanvas(_canvas);
        }

        private void OnSelectedElementChanged(object obj, SelectedElementChangedEventArgs args)
        {
            RemovePropertiesContent();
            if (args.Value is IEditorElement)
                AddPropertiesContent(args.Value);   
        }

        // uaktualnienie schematu na zmiane właściwości
        private void OnElementPropertiesChanged(object sender, PropertiesChangedEventArgs e)
        {
            if (e.Value != null)
            {
                _canvas.UpdateLayout();
                _canvas.UnselectElement();
                _canvas.SelectElement((FrameworkElement)e.Value);
            }
           
        }

        private void ShowActionExecutingWindow(Action action, string text)
        {
            LoadingWindowViewModel loadingWindow = new LoadingWindowViewModel() { Text = text };
            Window window = new Window() { Content = loadingWindow, AllowsTransparency = true, Background = Brushes.Transparent, WindowStyle = WindowStyle.None, WindowState = WindowState.Maximized };

            window.Dispatcher.Invoke(delegate { window.Show(); }, System.Windows.Threading.DispatcherPriority.Normal);

            try
            {
                Dispatcher.CurrentDispatcher.Invoke(action, System.Windows.Threading.DispatcherPriority.Loaded);
            }
            finally
            {
                window.Close();
            }
        }

        public List<string> HeadersList = new List<string>()
        {
            {"Nazwa"},
            {"Opis"},
            {"Adres"},
            {"Liczba elementów"},
            {"Okres próbkowania"},
            {"Funkcja przeliczająca"},
            {"Parametry archiwizacji"},
            {"Archiwum"},
            {"Grupa1"},
            {"Kanał"},
            {"Zakres surowy od"},
            {"Zakres surowy do"},
            {"Zakres pomiarowy od"},
            {"Zakres pomiarowy do"},
            {"Sprawdzanie zakresu pomiarowego"},
            {"Zakres wyświetlania od"},
            {"Zakres wyświetlania do"},
            {"Położenie suwaka"},
            {"Zakres sterowania od"},
            {"Zakres sterowania do"},
            {"Rejestracja sterowania"},
            {"Format"},
            {"Dokładność"},
            {"Baza słupka"},
            {"Zakres słupka od"},
            {"Zakres słupka do"},
            {"Jednostka"},
            {"Minimum krytyczne"},
            {"Minimum"},
            {"Maksimum"},
            {"Maksimum krytyczne"},
            {"Zmienna kontrolna"},
            {"Maska zmiennej kontrolnej"},
            {"Nazwy stanów"},
            {"Zestaw stanów"},
            {"Wartość stanu"},
            {"Tytuł wykresu"},
            {"Zakres wyświetlania krok"},
            {"Zakres wyświetlania szerokość"},
            {"Zakres wyświetlania podział"},
            {"Zmienna nieaktywna"},
        };

    }
}
