﻿using SchemeEditor.Attributes;
using SchemeEditor.ViewModels.BaseViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeEditor.ViewModels
{
    [AutoViewMapper("InitView")]
    public class InitViewModel : BaseViewModel
    {
        public InitViewModel()
        {

        }
    }
}
