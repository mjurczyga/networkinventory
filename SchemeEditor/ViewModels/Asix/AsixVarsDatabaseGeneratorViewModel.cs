﻿using Autofac;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using SchemeControlsLib.Services;
using SchemeEditor.Attributes;
using SchemeEditor.Helpers;
using SchemeEditor.Services.Asix;
using SchemeEditor.Services.Asix.Models;
using SchemeEditor.Services.Asix.StaticElemnts;
using SchemeEditor.ViewModels.BaseViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SchemeEditor.ViewModels.Asix
{
    [AutoViewMapper("AsixVarsDatabaseGeneratorView")]
    public class AsixVarsDatabaseGeneratorViewModel : BaseViewModel
    {
        private readonly IComponentContext _componentContext;
        private readonly IAsixService _asixService;
        private readonly ISchemeService _schemeService;

        public ICommand GenerateAsixArchitektVars { get; private set; }
        public ICommand GenerateEvoVars { get; private set; }

        public List<ComputeFunction> ComputeFunctions { get { return ComboBoxes.ComputeFunctions; } }
        public ComputeFunction ComputeFunction { get; set; }
        public string CanalName { get; set; }

        public int StartAddress { get; set; }

        public AsixVarsDatabaseGeneratorViewModel(IComponentContext componentContext, IAsixService asixService, ISchemeService schemeService)
        {
            _componentContext = componentContext;
            _asixService = asixService;
            _schemeService = schemeService;

            GenerateAsixArchitektVars = new RelayCommand(OnGenerateAsixArchitektVars);
            GenerateEvoVars = new RelayCommand(OnGenerateEvoVars);
        }

        private void OnGenerateEvoVars()
        {
            var propsDict = new Dictionary<string, string>();
            propsDict.Add("Kanał", CanalName);

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Plik xml|*.xml";
            saveFileDialog.Title = "Zapisz wygenerowane zmienne do xml";
            saveFileDialog.ShowDialog();

            //using (var fs = new FileStream(saveFileDialog.FileName, FileMode.Create, FileAccess.Write))
            //{
                var varbase = _asixService.GenerateVarsToXMLFile(propsDict);
                varbase.SerializeXMLData(saveFileDialog.FileName);
            //}
        }

        private void OnGenerateAsixArchitektVars()
        {
            var propsDict = new Dictionary<string, string>();
            propsDict.Add("Liczba elementów", "1");
            propsDict.Add("Okres próbkowania", "1");
            propsDict.Add("Funkcja przeliczająca", ComputeFunction != null ? ComputeFunction.Name : "INT_NIC");
            propsDict.Add("Kanał", CanalName);
            propsDict.Add("Sprawdzanie zakresu pomiarowego", "0");
            propsDict.Add("Rejestracja sterowania", "0");
            propsDict.Add("Zmienna nieaktywna", "0");


            // Displays a SaveFileDialog so the user can save the Image
            // assigned to Button2.
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Plik xls|*.xls";
            saveFileDialog.Title = "Zapisz wygenerowane zmienne do xls";
            saveFileDialog.ShowDialog();

            using (var fs = new FileStream(saveFileDialog.FileName, FileMode.Create, FileAccess.Write))
            {
                var workbbok =_asixService.GenerateVarsToXLSFile(this.StartAddress, ComputeFunction != null ? ComputeFunction.ObjectSize : 1, propsDict, out List<string> beckhoffModbusVarDefinition);
                workbbok.Write(fs);
            }

        }
    }
}
