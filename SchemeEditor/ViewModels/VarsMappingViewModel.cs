﻿using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Helpers;
using SchemeControlsLib.Models;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using SchemeEditor.Attributes;
using SchemeEditor.Models;
using SchemeEditor.Services.EditorServices;
using SchemeEditor.ViewModels.BaseViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SchemeEditor.ViewModels
{
    [AutoViewMapper("VarsMappingView")]
    public class VarsMappingViewModel : BaseViewModel
    {
        private IVarsService _varsService;

        private List<VarMappingModel> _DataGridItemSource = new List<VarMappingModel>();
        public List<VarMappingModel> DataGridItemSource { get { return _DataGridItemSource; } set { _DataGridItemSource = value; } }

        private SchemeCanvas _SchemeCanvas = null;
        public SchemeCanvas SchemeCanvas { get { return _SchemeCanvas; } set { _SchemeCanvas = value; if (value != null) this.LoadDataFromCanvas(); } }

        private DataGrid _DataGrid = null;

        public ICommand DataGridLoadedCommand { get; set; }

        public ICommand LoadVarsDictionary { get; set; }

        public IEnumerable<string> CanvasAddedControlsName {get; set;}

        private VarMappingModel _SelectedItem;
        public VarMappingModel SelectedItem { get { return _SelectedItem; } set { _SelectedItem = value; OnSelectedItemChanged(_SelectedItem); } }

        public double EditableColumnMinWidth { get; set; } = 200;

        public ObservableCollection<string> VarsDictionary
        {
            get
            {
                return _varsService.VarsDictionary;
            }
        }

        private ObservableCollection<string> _ControlNamesDictionary = new ObservableCollection<string>();
        public ObservableCollection<string> ControlNamesDictionary
        {
            get
            {
                return _ControlNamesDictionary;
            }
            set
            {
                if (value != _ControlNamesDictionary)
                {
                    _ControlNamesDictionary = value;
                    RaisePropertyChanged("ControlNamesDictionary");
                }

            }
        }

        private void OnSelectedItemChanged(VarMappingModel selectedItem)
        {
            if (SchemeCanvas != null)
            {
                var schemeElement = SchemeCanvas.Children.OfType<ActiveElement>().Where(x => x.Name == selectedItem.ControlName).FirstOrDefault();
                SchemeCanvas.SelectElement(schemeElement);
            }
        }

        public VarsMappingViewModel(IVarsService varsService)
        {
            _varsService = varsService;
            LoadDataFromCanvas();
            DataGridLoadedCommand = new RelayCommand<DataGrid>(OnDataGridLoadedCommand);
            LoadVarsDictionary = new RelayCommand(OnLoadVarsDictionary);
        }

        private void OnSelectedItemChangedCommand(RoutedEventArgs e)
        {
            
        }

        private void OnLoadVarsDictionary()
        {
            if(!_varsService.IsDictionaryLoaded)
                _varsService.LoadDictionary();            

            /*OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "CSV Files (*.csv)|*.csv";
            openFileDialog.FilterIndex = 1;
            if (openFileDialog.ShowDialog() == true)
            {
                VarsDictionary.Clear();
                var readed = File.ReadAllText(openFileDialog.FileName);
                var list = new CSVHelper(readed);

                foreach (var varText in list)
                {
                    VarsDictionary.Add(varText[0].ToString().Replace(";", ""));
                    RaisePropertyChanged("VarsDictionary");
                }

                var longestString = VarsDictionary.Aggregate("", (max, cur) => max.Length > cur.Length ? max : cur);
                EditableColumnMinWidth = longestString.ComputeTextSize(12).Width;
                RaisePropertyChanged("EditableColumnMinWidth");

            }*/
        }

        private void OnDataGridLoadedCommand(DataGrid obj)
        {
            _DataGrid = obj;
            _DataGrid.CellEditEnding += OnCellEditEnding;
            _DataGrid.BeginningEdit += OnBeginningEdit;
        }

        private VarMappingModel _currentVarMappingModel = null;
        private VarMappingModel _prevVarMappingModel = null;
        public VarMappingModel PrevVarMappingModel { get { return _prevVarMappingModel; } set { _prevVarMappingModel = value; } }
        private void OnBeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            if (e.Row.Item is VarMappingModel)
            {
                PrevVarMappingModel = new VarMappingModel(e.Row.Item as VarMappingModel);
            }

            CanvasAddedControlsName = new List<string>(SchemeCanvas.Children.OfType<FrameworkElement>().Select(x => x.Name));
            //TODO Przemyśleć czy filtrować po typie łącznika 
            //if (_prevVarMappingModel.TypeName.ToUpper().Contains("ŁĄCZNIK"))
            //{
            //    var enumNames = Enum.GetNames(typeof(SwitchTypes));
            //    ControlNamesDictionary = VarsDictionary.Where(x => x.Contains(_prevVarMappingModel.))
            //        }
        }

        private void OnCellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            _currentVarMappingModel = e.Row.Item as VarMappingModel;
            if (PrevVarMappingModel == null)
                PrevVarMappingModel = _currentVarMappingModel;

            var schemeElement = SchemeCanvas.Children.OfType<ActiveElement>().Where(x => x.Name == PrevVarMappingModel.ControlName).FirstOrDefault();

            if (!schemeElement.SetSpecificPropdpValue(_currentVarMappingModel.StateVarNameProperty.PropertyName, _currentVarMappingModel.VarsName))
            {
                var datagrid = (DataGrid)sender;
                e.Cancel = true;
            }

            if (_currentVarMappingModel != null && PrevVarMappingModel != null && _currentVarMappingModel.ControlName != PrevVarMappingModel.ControlName)
            {
                if (schemeElement != null && schemeElement.SetName(_currentVarMappingModel.ControlName))
                {
                    CanvasAddedControlsName = new List<string>(SchemeCanvas.Children.OfType<FrameworkElement>().Select(x => x.Name));
                    var nameVar = VarsDictionary.Where(x => x.Contains(_currentVarMappingModel.ControlName)).FirstOrDefault();
                    if (schemeElement.SetSpecificPropdpValue(_currentVarMappingModel.StateVarNameProperty.PropertyName, nameVar))
                        _currentVarMappingModel.VarsName = nameVar;

                    PrevVarMappingModel = new VarMappingModel(_currentVarMappingModel);

                }
                else
                {
                    var datagrid = (DataGrid)sender;
                    e.Cancel = true;
                }

                _currentVarMappingModel = null;
            }       
            
        }

        private void OnDataGridSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            VarMappingModel varMappingModel = (VarMappingModel)e.Source;
        }

        private void LoadDataFromCanvas()
        {
            if (SchemeCanvas != null)
            {
                var activeElements = SchemeCanvas.Children.OfType<ActiveElement>();

                foreach(var activeElement in activeElements)
                {
                    var className = activeElement.GetType().GetCustomAttribute<EditorType>(false);
                    var stateProperties = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(activeElement, ElementPropertiesModels.GetPropertiesByElementType(activeElement)).ToDictionary(x => x.Key as DependencyProperty, x => x.Value).Where(x => x.Key.Name.Contains("BindingVarName"));

                    foreach (var stateProperty in stateProperties)
                    {
                        DataGridItemSource.Add(
                            new VarMappingModel(className.Name, 
                            activeElement.Name, 
                            new PropertyClass(stateProperty.Key.Name, stateProperty.Value), 
                            activeElement.GetValue(stateProperty.Key) != null ? activeElement.GetValue(stateProperty.Key).ToString() : "", 
                            activeElement.ClickCommand));
                    }
                }
            }
        }
    }
}
