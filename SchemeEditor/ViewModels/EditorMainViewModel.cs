﻿using Autofac;
using GalaSoft.MvvmLight.Command;
using SchemeEditor.Attributes;
using SchemeEditor.Services;
using SchemeEditor.ViewModels.BaseViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SchemeEditor.ViewModels
{
    [AutoViewMapper("EditorMainView")]
    public class EditorMainViewModel :BaseViewModel
    {
        private IRegionManager _regionManager;
        private IComponentContext _componentContext;

        public ICommand SchemeEditorCommand { get; private set; }

        public EditorMainViewModel(IComponentContext componentContext, IRegionManager regionManager)
        {
            _regionManager = regionManager;
            _componentContext = componentContext;

            this.InitCommands();
        }

        private void InitCommands()
        {
            SchemeEditorCommand = new RelayCommand(OnSchemeEditorCommand);
        }

        private void OnSchemeEditorCommand()
        {           
            _regionManager.SwitchRegion("SchemeEditorRegion", _componentContext.Resolve<MainViewModel>());
        }
    }
}
