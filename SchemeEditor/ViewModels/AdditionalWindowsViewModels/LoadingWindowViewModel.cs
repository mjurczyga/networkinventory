﻿using SchemeControlsLib.Services;
using SchemeEditor.Attributes;
using SchemeEditor.ViewModels.BaseViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeEditor.ViewModels.AdditionalWindowsViewModels
{
    [AutoViewMapper("LoadingView")]
    public class LoadingWindowViewModel : BaseViewModel
    {
        public string Text { get; set; }

        public LoadingWindowViewModel()
        { }
        
    }
}
