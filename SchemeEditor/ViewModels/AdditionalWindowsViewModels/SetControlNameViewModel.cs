﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeEditor.Attributes;
using SchemeEditor.Helpers;
using SchemeControlsLib.Helpers.ValidationRules;
using SchemeEditor.ViewModels.BaseViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SchemeEditor.ViewModels.AdditionalWindowsViewModels
{
    [AutoViewMapper("SetControlNameView")]
    public class SetControlNameViewModel : BaseViewModel
    {
        public FrameworkElement Control;

        private string _ControlName;
        public string ControlName { get { return _ControlName; } set { _ControlName = value; RaisePropertyChanged("ControlName"); }  }

        private string _NameValidationInfo;
        public string NameValidationInfo
        {
            get { return _NameValidationInfo; }
            set { _NameValidationInfo = value; RaisePropertyChanged("NameValidationInfo"); }
        }

        public ICommand SetControlNameCommand { get; private set; }

        public SetControlNameViewModel(FrameworkElement control) : base()
        {
            Control = control;
            ControlName = Control.Name;
            InitSetCommands();
        }

        private void InitSetCommands()
        {
            SetControlNameCommand = new RelayCommand<object>(OnSetControlNameCommand);
        }

        private void OnSetControlNameCommand(object obj)
        {
            var x = WindowHeplers.GetParentWindow(obj as DependencyObject);

            var nameValidator = new FrameworkElementlNameValidationRule();
            var validationResult = nameValidator.Validate(ControlName, null);
            if(!validationResult.IsValid)          
                NameValidationInfo = validationResult.ErrorContent as string;          
            else
            {
                NameValidationInfo = validationResult.ErrorContent as string;
                Control.SetValue(FrameworkElement.NameProperty, ControlName);
                x.Close();
            }
        }
    }
}
