﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeEditor.Attributes
{
    public class ContainerRegistration : Attribute
    {
        public Type RegisterAs { get; private set; } = null;
        public bool SingleInstance { get; private set; } = false;

        public ContainerRegistration(Type registerAs = null, bool singleInstance = false)
        {
            RegisterAs = registerAs;
            SingleInstance = singleInstance;
        }

        public ContainerRegistration(bool singleInstance = false)
        {
            SingleInstance = singleInstance;
        }

        public ContainerRegistration(Type registerAs = null)
        {
            RegisterAs = registerAs;
        }

        public ContainerRegistration()
        {

        }
    }
}
