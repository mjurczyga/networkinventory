﻿using System;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace SchemeEditor.Helpers
{
    public class ResizingAdorner : Adorner
    {
        private readonly Thumb[] _thumbs;
        private readonly VisualCollection _visualCollection;
        private const float ThumbSize = 10;

        public ResizingAdorner(UIElement adornedElement) : base(adornedElement)
        {
            _visualCollection = new VisualCollection(this);

            _thumbs = new Thumb[4];
            _thumbs[0] = CreateThumb(TopLeftThumbDrag, Cursors.SizeNWSE);
            _thumbs[1] = CreateThumb(TopRightThumbDrag, Cursors.SizeNESW);
            _thumbs[2] = CreateThumb(BottomLeftThumbDrag, Cursors.SizeNESW);
            _thumbs[3] = CreateThumb(BottomRightThumbDrag, Cursors.SizeNWSE);
        }
        private Thumb CreateThumb(DragDeltaEventHandler dragDeltaEventHandler, Cursor cursor)
        {
            Thumb thumb = new Thumb();
            thumb.Width = thumb.Height = ThumbSize;
            thumb.BorderThickness = new Thickness(1);
            thumb.BorderBrush = Brushes.Black;
            thumb.DragDelta += dragDeltaEventHandler;
            thumb.Cursor = cursor;

            _visualCollection.Add(thumb);

            return thumb;
        }
        protected override Visual GetVisualChild(int index)
        {
            return _visualCollection[index];
        }
        protected override int VisualChildrenCount
        {
            get { return _visualCollection.Count; }
        }
        protected override Size ArrangeOverride(Size finalSize)
        {
            double controlWidth = AdornedElement.DesiredSize.Width;
            double controlHeight = AdornedElement.DesiredSize.Height;

            _thumbs[0].Arrange(new Rect(-ThumbSize / 2, -ThumbSize / 2, ThumbSize, ThumbSize));
            _thumbs[1].Arrange(new Rect(controlWidth - ThumbSize / 2, -ThumbSize / 2, ThumbSize, ThumbSize));
            _thumbs[2].Arrange(new Rect(-ThumbSize / 2, controlHeight - ThumbSize / 2, ThumbSize, ThumbSize));
            _thumbs[3].Arrange(new Rect(controlWidth - ThumbSize / 2, controlHeight - ThumbSize / 2, ThumbSize, ThumbSize));

            return finalSize;
        }
        void BottomLeftThumbDrag(object sender, DragDeltaEventArgs args)
        {
            FrameworkElement adornedElement = (FrameworkElement)AdornedElement;

            adornedElement.Width = Math.Max(0, adornedElement.Width - args.HorizontalChange);
            adornedElement.Height = Math.Max(0, args.VerticalChange + adornedElement.Height);
        }
        void BottomRightThumbDrag(object sender, DragDeltaEventArgs args)
        {
            FrameworkElement adornedElement = (FrameworkElement)AdornedElement;

            adornedElement.Width = Math.Max(0, adornedElement.Width + args.HorizontalChange);
            adornedElement.Height = Math.Max(0, args.VerticalChange + adornedElement.Height);
        }
        void TopLeftThumbDrag(object sender, DragDeltaEventArgs args)
        {
            FrameworkElement adornedElement = (FrameworkElement)AdornedElement;

            adornedElement.Width = Math.Max(0, adornedElement.Width - args.HorizontalChange);
            adornedElement.Height = Math.Max(0, adornedElement.Height - args.VerticalChange);
        }
        void TopRightThumbDrag(object sender, DragDeltaEventArgs args)
        {
            FrameworkElement adornedElement = (FrameworkElement)AdornedElement;

            adornedElement.Width = Math.Max(0, adornedElement.Width + args.HorizontalChange);
            adornedElement.Height = Math.Max(0, adornedElement.Height - args.VerticalChange);
        }
    }
}
