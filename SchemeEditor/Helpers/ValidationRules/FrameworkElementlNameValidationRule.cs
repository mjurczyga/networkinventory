﻿using SchemeControlsLib.Helpers;
using SchemeEditor.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SchemeEditor.Helpers.ValidationRules
{
    internal class FrameworkElementlNameValidationRule : ValidationRule
    {
        public IEnumerableProperty IEnumerableProperty { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value is string)
            {


                var name = (string)value;
                if (StringHelpers.CheckIfIsNullOrEpmpty(name))
                    return new ValidationResult(false, "Nazwa nie może być pusta");
                else if(StringHelpers.CheckIfNumber(name[0].ToString()))
                    return new ValidationResult(false, "Nazwa nie może zaczynać się od liczby");
                else if (StringHelpers.CheckIfStringIsSymbol(name, '_'))
                    return new ValidationResult(false, "Nazwa nie może zawierać znaków specjalnych (wyjątek '_')");
                else if (StringHelpers.CheckIfSpaces(name))
                    return new ValidationResult(false, "Nazwa nie może zawierać spacji");
                else if (IEnumerableProperty.Value != null && IEnumerableProperty.Value.Contains(name))
                    return new ValidationResult(false, "Nazwa jest już używana");
                else
                    return new ValidationResult(true, null);
            }
            return new ValidationResult(false, "Sprawdzana nazwa musi być typu string");
        }
    }
}
