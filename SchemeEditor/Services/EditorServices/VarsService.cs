﻿using Microsoft.Win32;
using SchemeControlsLib.Utilities;
using SchemeEditor.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeEditor.Services.EditorServices
{
    public class VarsService : ServiceBase, IVarsService
    {
        public bool IsDictionaryLoaded { get { return VarsDictionary.Count > 0 ? true : false; } }

        private ObservableCollection<string> _VarsDictionary = new ObservableCollection<string>();
        public ObservableCollection<string> VarsDictionary
        {
            get
            {
                return _VarsDictionary;
            }
        }

        /// <summary>
        /// ładuje zmienne z pliku csv
        /// </summary>
        public void LoadDictionary()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "CSV Files (*.csv)|*.csv";
            openFileDialog.FilterIndex = 1;
            if (openFileDialog.ShowDialog() == true)
            {
                VarsDictionary.Clear();
                var readed = File.ReadAllText(openFileDialog.FileName);
                var list = new CSVHelper(readed);

                foreach (var varText in list)
                {
                    VarsDictionary.Add(varText[0].ToString().Replace(";", ""));
                }

                var longestString = VarsDictionary.Aggregate("", (max, cur) => max.Length > cur.Length ? max : cur);
            }
        }


    }
}
