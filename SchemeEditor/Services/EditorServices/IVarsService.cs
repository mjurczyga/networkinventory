﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeEditor.Services.EditorServices
{
    public interface IVarsService
    {
        bool IsDictionaryLoaded { get; }
        ObservableCollection<string> VarsDictionary { get; }
        void LoadDictionary();
    }
}
