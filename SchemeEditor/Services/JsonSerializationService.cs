﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeEditor.Services
{
    public static class JsonSerializationService
    {
        public static string ApplicationSettingsFile;

        private static JsonSerializer _jsonSerializer;

        static JsonSerializationService()
        {
            _jsonSerializer = new JsonSerializer();
            _jsonSerializer.NullValueHandling = NullValueHandling.Include;
            //var config = GlobalConfiguration.Configuration;
            //config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
        }

        public static void Serialize<T>(T configuration, string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                throw new NullReferenceException("Wartość parametru 'fileName' - NULL");

            using (StreamWriter streamWriter = new StreamWriter(fileName, false))
            using (JsonWriter jsonWriter = new JsonTextWriter(streamWriter))
            {
                _jsonSerializer.Serialize(jsonWriter, configuration);
            }
        }

        public static T Deserialize<T>(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                throw new NullReferenceException("Wartość parametru 'fileName' - NULL");

            object deserialized = null;

            T result = (T)deserialized;

            if (File.Exists(fileName))
            {
                try
                {
                    string jsonObject = File.ReadAllText(fileName);
                    deserialized = JsonConvert.DeserializeObject<T>(jsonObject);
                    result = (T)Convert.ChangeType(deserialized, typeof(T));
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }
    }
}
