﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SchemeEditor.Services
{
    public interface IRegionManager
    {
        Dictionary<string, ContentControl> RegionsByName { get; }
        Type LastContentType { get; }
        void RegisterRegion(ContentControl region);
        void SwitchRegion(string regionUid, Type type);
        void SwitchRegion(string regionUid, object instance);
        object SwitchRegionAndGetInstance(string regionUid, Type type);
        void ShowNewWindow(Type type, bool sizeToContent, bool showDialog = false, string title = "", WindowStyle windowStyle = WindowStyle.None, ResizeMode resizeMode = ResizeMode.NoResize);
        void ShowNewWindow(object instance, bool sizeToContent, bool showDialog = false, string title = "", WindowStyle windowStyle = WindowStyle.None, ResizeMode resizeMode = ResizeMode.NoResize, bool topmost = false);
        void CloseRegion(string regionUid);
        void CloseWindow();
        void HideRegion(string regionUid);
        void ShowRegion(string regionUid);
        object ShowRegionAndGetInstance(string regionUid);
    }
}
