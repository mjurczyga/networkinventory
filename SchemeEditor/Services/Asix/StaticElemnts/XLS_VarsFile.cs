﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeEditor.Services.Asix.StaticElemnts
{
    /// <summary>
    /// Klasa zawiera elementy pliku dla AsixArchitect
    /// </summary>
    public static class XLS_VarsFile
    {
        private static List<string> _Headers = new List<string>()
        {
            {"Nazwa"},
            {"Opis"},
            {"Adres"},
            {"Liczba elementów"},
            {"Okres próbkowania"},
            {"Funkcja przeliczająca"},
            {"Parametry archiwizacji"},
            {"Archiwum"},
            {"Grupa1"},
            {"Kanał"},
            {"Zakres surowy od"},
            {"Zakres surowy do"},
            {"Zakres pomiarowy od"},
            {"Zakres pomiarowy do"},
            {"Sprawdzanie zakresu pomiarowego"},
            {"Zakres wyświetlania od"},
            {"Zakres wyświetlania do"},
            {"Położenie suwaka"},
            {"Zakres sterowania od"},
            {"Zakres sterowania do"},
            {"Rejestracja sterowania"},
            {"Format"},
            {"Dokładność"},
            {"Baza słupka"},
            {"Zakres słupka od"},
            {"Zakres słupka do"},
            {"Jednostka"},
            {"Minimum krytyczne"},
            {"Minimum"},
            {"Maksimum"},
            {"Maksimum krytyczne"},
            {"Zmienna kontrolna"},
            {"Maska zmiennej kontrolnej"},
            {"Nazwy stanów"},
            {"Zestaw stanów"},
            {"Wartość stanu"},
            {"Tytuł wykresu"},
            {"Zakres wyświetlania krok"},
            {"Zakres wyświetlania szerokość"},
            {"Zakres wyświetlania podział"},
            {"Zmienna nieaktywna"},
        };
        
        public static List<string> Headers { get { return _Headers; } }
    }
}
