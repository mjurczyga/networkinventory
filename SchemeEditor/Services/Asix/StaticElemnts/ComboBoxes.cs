﻿using SchemeEditor.Services.Asix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeEditor.Services.Asix.StaticElemnts
{
    public static class ComboBoxes
    {

        private static List<ComputeFunction> _ComputeFunctions = new List<ComputeFunction>()
        {
            { new ComputeFunction("NIC_BYTE", 1) },
            { new ComputeFunction("NIC_INT", 1) },
        }; 
        public static List<ComputeFunction> ComputeFunctions { get { return _ComputeFunctions; } }
    }
}
