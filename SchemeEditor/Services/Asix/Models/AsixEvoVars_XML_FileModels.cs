﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SchemeEditor.Services.Asix.Models
{
    public static class EvoModelsExt
    {
        public static Variable GetVariable(string varName, string driver, string valueType)
        {
            Variable variable = new Variable()
            {
                Name = varName,
                Attribute = new List<XML_Attribute>()
                {
                    new XML_Attribute() { Name = "Description", LocalText = new LocalText() {LangId = 21, Text = varName } },
                    new XML_Attribute() { Name = "ValueType", Text = valueType },
                    new XML_Attribute() { Name = "Address", Text = "0" },
                    new XML_Attribute() { Name = "SampleRate", Text = "1" },
                    new XML_Attribute() { Name = "DisplayRangeFrom", Text = "0" },
                    new XML_Attribute() { Name = "DisplayRangeTo", Text = "2" }
                }
            };

            return variable;
        }
    }


    // using System.Xml.Serialization;
    // XmlSerializer serializer = new XmlSerializer(typeof(VariableBase));
    // using (StringReader reader = new StringReader(xml))
    // {
    //    var test = (VariableBase)serializer.Deserialize(reader);
    // }

    [XmlRoot(ElementName = "Languages")]
    public class Languages
    {

        [XmlElement(ElementName = "Language")]
        public List<int> Language { get; set; }
    }

    [XmlRoot(ElementName = "LocalName")]
    public class LocalName
    {

        [XmlAttribute(AttributeName = "LangId")]
        public int LangId { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Attribute")]
    public class XML_Attribute
    {

        [XmlElement(ElementName = "LocalName")]
        public List<LocalName> LocalName { get; set; }

        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }

        [XmlText]
        public string Text { get; set; }

        [XmlElement(ElementName = "LocalText")]
        public LocalText LocalText { get; set; }
        //public List<Attribute> Attributes { get; internal set; }
    }

    [XmlRoot(ElementName = "Attributes")]
    public class XML_Attributes
    {

        [XmlElement(ElementName = "Attribute")]
        public List<XML_Attribute> Attribute { get; set; }

        [XmlAttribute(AttributeName = "Count")]
        public int Count { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "LocalText")]
    public class LocalText
    {

        [XmlAttribute(AttributeName = "LangId")]
        public int LangId { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Variable")]
    public class Variable
    {

        [XmlElement(ElementName = "Attribute")]
        public List<XML_Attribute> Attribute { get; set; }

        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }

        [XmlText]
        public string Text { get; set; }

        public Variable()
        {

        }

        public Variable(string varName, string driver, string valueType)
        {

            Name = varName;
            Attribute = new List<XML_Attribute>()
                {
                    new XML_Attribute() { Name = "Description", LocalText = new LocalText() {LangId = 21, Text = varName } },
                    new XML_Attribute() { Name = "ValueType", Text = valueType },
                    new XML_Attribute() { Name = "Channel", Text = driver },
                    new XML_Attribute() { Name = "Address", Text = "0" },
                    new XML_Attribute() { Name = "SampleRate", Text = "1" },
                    new XML_Attribute() { Name = "DisplayRangeFrom", Text = "0" },
                    new XML_Attribute() { Name = "DisplayRangeTo", Text = "2" }
                };        
        }
    }

    [XmlRoot(ElementName = "Variables")]
    public class Variables
    {

        [XmlElement(ElementName = "Variable")]
        public List<Variable> Variable { get; set; }

        [XmlAttribute(AttributeName = "Count")]
        public int Count { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "VariableBase")]
    public class VariableBase
    {

        [XmlElement(ElementName = "Languages")]
        public Languages Languages { get; set; }

        [XmlElement(ElementName = "Attributes")]
        public XML_Attributes Attributes { get; set; }

        [XmlElement(ElementName = "Variables")]
        public Variables Variables { get; set; }
    }


}
