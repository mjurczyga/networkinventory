﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SchemeEditor.Services.Asix.Models
{
    public class AsixArchitect_XLS_Cell_Attribute : System.Attribute, INotifyPropertyChanged
    {
        private int _Index;
        public int Index
        {
            get { return _Index; }
            set { _Index = value; OnPropertyChanged("Index"); }
        }

        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; OnPropertyChanged("Name"); }
        }



        #region konstruktory
        public AsixArchitect_XLS_Cell_Attribute(int index, string name)
        {
            Index = index;
            Name = name;

        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
