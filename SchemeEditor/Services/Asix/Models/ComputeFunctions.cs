﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SchemeEditor.Services.Asix.Models
{
    public class ComputeFunction : INotifyPropertyChanged
    {
        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; OnPropertyChanged("Name"); }
        }

        private int _ObjectSize;
        public int ObjectSize
        {
            get { return _ObjectSize; }
            set { _ObjectSize = value; OnPropertyChanged("ObjectSize"); }
        }

        public ComputeFunction() { }

        public ComputeFunction(string name, int mb_size)
        {
            Name = name;
            mb_size = ObjectSize;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
