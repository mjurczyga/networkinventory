﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SchemeEditor.Services.Asix.Models
{
    public class AsixArchitect_XLS_CellModel : INotifyPropertyChanged
    {
        private int _Index;
        public int Index
        {
            get { return _Index; }
            private set { _Index = value; NotifyPropertyChanged("Index"); }
        }

        private string _CellName;
        public string CellName
        {
            get { return _CellName; }
            private set { _CellName = value; NotifyPropertyChanged("CellName"); }
        }

        private string _Value;
        public string Value
        {
            get { return _Value; }
            set { _Value = value; NotifyPropertyChanged("Value"); }
        }


        public AsixArchitect_XLS_CellModel(string cellName, int index)
        {
            _CellName = cellName;
            _Index = index;
        }

        public AsixArchitect_XLS_CellModel()
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
