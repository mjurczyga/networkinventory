﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SchemeEditor.Services.Asix.Models
{
    public class AsixArchitect_XLS_FileModel : INotifyPropertyChanged
    {
        private AsixArchitect_XLS_CellModel _Name;
        [AsixArchitect_XLS_Cell_Attribute(0, "Nazwa")]
        public AsixArchitect_XLS_CellModel Name
        {
            get { return _Name; }
            set { _Name = value; NotifyPropertyChanged("Name"); }
        }

        private AsixArchitect_XLS_CellModel _Description;
        [AsixArchitect_XLS_Cell_Attribute(1, "Opis")]
        public AsixArchitect_XLS_CellModel Description
        {
            get { return _Description; }
            set { _Description = value; NotifyPropertyChanged("Description"); }
        }

        private AsixArchitect_XLS_CellModel _Address;
        [AsixArchitect_XLS_Cell_Attribute(2, "Adres")]
        public AsixArchitect_XLS_CellModel Address
        {
            get { return _Address; }
            set { _Address = value; NotifyPropertyChanged("Address"); }
        }

        private AsixArchitect_XLS_CellModel _ItemsCount;
        [AsixArchitect_XLS_Cell_Attribute(3, "Liczba elementów")]
        public AsixArchitect_XLS_CellModel ItemsCount
        {
            get { return _ItemsCount; }
            set { _ItemsCount = value; NotifyPropertyChanged("ItemsCount"); }
        }

        private AsixArchitect_XLS_CellModel _SamplingPeriod;
        [AsixArchitect_XLS_Cell_Attribute(4, "Okres próbkowania")]
        public AsixArchitect_XLS_CellModel SamplingPeriod
        {
            get { return _SamplingPeriod; }
            set { _SamplingPeriod = value; NotifyPropertyChanged("SamplingPeriod"); }
        }

        private AsixArchitect_XLS_CellModel _ConversionFunction;
        [AsixArchitect_XLS_Cell_Attribute(5, "Funkcja przeliczająca")]
        public AsixArchitect_XLS_CellModel ConversionFunction
        {
            get { return _ConversionFunction; }
            set { _ConversionFunction = value; NotifyPropertyChanged("ConversionFunction"); }
        }

        private AsixArchitect_XLS_CellModel _ArchivingParameters;
        [AsixArchitect_XLS_Cell_Attribute(6, "Parametry archiwizacji")]
        public AsixArchitect_XLS_CellModel ArchivingParameters
        {
            get { return _ArchivingParameters; }
            set { _ArchivingParameters = value; NotifyPropertyChanged("ArchivingParameters"); }
        }

        private AsixArchitect_XLS_CellModel _Archives;
        [AsixArchitect_XLS_Cell_Attribute(7, "Archiwum")]
        public AsixArchitect_XLS_CellModel Archives
        {
            get { return _Archives; }
            set { _Archives = value; NotifyPropertyChanged("Archives"); }
        }

        private AsixArchitect_XLS_CellModel _Group1;
        [AsixArchitect_XLS_Cell_Attribute(8, "Grupa1")]
        public AsixArchitect_XLS_CellModel Group1
        {
            get { return _Group1; }
            set { _Group1 = value; NotifyPropertyChanged("Group1"); }
        }

        private AsixArchitect_XLS_CellModel _Canal;
        [AsixArchitect_XLS_Cell_Attribute(9, "Kanał")]
        public AsixArchitect_XLS_CellModel Canal
        {
            get { return _Canal; }
            set { _Canal = value; NotifyPropertyChanged("Canal"); }
        }

        private AsixArchitect_XLS_CellModel _RawRangeFrom;
        [AsixArchitect_XLS_Cell_Attribute(10, "Zakres surowy od")]
        public AsixArchitect_XLS_CellModel RawRangeFrom
        {
            get { return _RawRangeFrom; }
            set { _RawRangeFrom = value; NotifyPropertyChanged("RawRangeFrom"); }
        }

        private AsixArchitect_XLS_CellModel _RawRangeTo;
        [AsixArchitect_XLS_Cell_Attribute(11, "Zakres surowy do")]
        public AsixArchitect_XLS_CellModel RawRangeTo
        {
            get { return _RawRangeTo; }
            set { _RawRangeTo = value; NotifyPropertyChanged("RawRangeTo"); }
        }

        private AsixArchitect_XLS_CellModel _MeasuringRangeFrom;
        [AsixArchitect_XLS_Cell_Attribute(12, "Zakres pomiarowy od")]
        public AsixArchitect_XLS_CellModel MeasuringRangeFrom
        {
            get { return _MeasuringRangeFrom; }
            set { _MeasuringRangeFrom = value; NotifyPropertyChanged("MeasuringRangeFrom"); }
        }

        private AsixArchitect_XLS_CellModel _MeasuringRangeUpTo;
        [AsixArchitect_XLS_Cell_Attribute(13, "Zakres pomiarowy do")]
        public AsixArchitect_XLS_CellModel MeasuringRangeUpTo
        {
            get { return _MeasuringRangeUpTo; }
            set { _MeasuringRangeUpTo = value; NotifyPropertyChanged("MeasuringRangeUpTo"); }
        }

        private AsixArchitect_XLS_CellModel _CheckingTheMeasuringRange;
        [AsixArchitect_XLS_Cell_Attribute(14, "Sprawdzanie zakresu pomiarowego")]
        public AsixArchitect_XLS_CellModel CheckingTheMeasuringRange
        {
            get { return _CheckingTheMeasuringRange; }
            set { _CheckingTheMeasuringRange = value; NotifyPropertyChanged("CheckingTheMeasuringRange"); }
        }

        private AsixArchitect_XLS_CellModel _DisplayRangeFrom;
        [AsixArchitect_XLS_Cell_Attribute(15, "Zakres wyświetlania od")]
        public AsixArchitect_XLS_CellModel DisplayRangeFrom
        {
            get { return _DisplayRangeFrom; }
            set { _DisplayRangeFrom = value; NotifyPropertyChanged("DisplayRangeFrom"); }
        }

        private AsixArchitect_XLS_CellModel _DisplayRangeTo;
        [AsixArchitect_XLS_Cell_Attribute(16, "Zakres wyświetlania do")]
        public AsixArchitect_XLS_CellModel DisplayRangeTo
        {
            get { return _DisplayRangeTo; }
            set { _DisplayRangeTo = value; NotifyPropertyChanged("DisplayRangeTo"); }
        }

        private AsixArchitect_XLS_CellModel _PositionOfTheSlider;
        [AsixArchitect_XLS_Cell_Attribute(17, "Położenie suwaka")]
        public AsixArchitect_XLS_CellModel PositionOfTheSlider
        {
            get { return _PositionOfTheSlider; }
            set { _PositionOfTheSlider = value; NotifyPropertyChanged("PositionOfTheSlider"); }
        }

        private AsixArchitect_XLS_CellModel _ControlRangeFrom;
        [AsixArchitect_XLS_Cell_Attribute(18, "Zakres sterowania od")]
        public AsixArchitect_XLS_CellModel ControlRangeFrom
        {
            get { return _ControlRangeFrom; }
            set { _ControlRangeFrom = value; NotifyPropertyChanged("ControlRangeFrom"); }
        }

        private AsixArchitect_XLS_CellModel _ControlRangeTo;
        [AsixArchitect_XLS_Cell_Attribute(19, "Zakres sterowania do")]
        public AsixArchitect_XLS_CellModel ControlRangeTo
        {
            get { return _ControlRangeTo; }
            set { _ControlRangeTo = value; NotifyPropertyChanged("ControlRangeTo"); }
        }

        private AsixArchitect_XLS_CellModel _ControlRegistration;
        [AsixArchitect_XLS_Cell_Attribute(20, "Rejestracja sterowania")]
        public AsixArchitect_XLS_CellModel ControlRegistration
        {
            get { return _ControlRegistration; }
            set { _ControlRegistration = value; NotifyPropertyChanged("ControlRegistration"); }
        }

        private AsixArchitect_XLS_CellModel _Format;
        [AsixArchitect_XLS_Cell_Attribute(21, "Format")]
        public AsixArchitect_XLS_CellModel Format
        {
            get { return _Format; }
            set { _Format = value; NotifyPropertyChanged("Format"); }
        }

        private AsixArchitect_XLS_CellModel _Accuracy;
        [AsixArchitect_XLS_Cell_Attribute(22, "Dokładność")]
        public AsixArchitect_XLS_CellModel Accuracy
        {
            get { return _Accuracy; }
            set { _Accuracy = value; NotifyPropertyChanged("Accuracy"); }
        }

        private AsixArchitect_XLS_CellModel _BaseOfThePost;
        [AsixArchitect_XLS_Cell_Attribute(23, "Baza słupka")]
        public AsixArchitect_XLS_CellModel BaseOfThePost
        {
            get { return _BaseOfThePost; }
            set { _BaseOfThePost = value; NotifyPropertyChanged("BaseOfThePost"); }
        }

        private AsixArchitect_XLS_CellModel _BarRangeFrom;
        [AsixArchitect_XLS_Cell_Attribute(24, "Zakres słupka od")]
        public AsixArchitect_XLS_CellModel BarRangeFrom
        {
            get { return _BarRangeFrom; }
            set { _BarRangeFrom = value; NotifyPropertyChanged("BarRangeFrom"); }
        }

        private AsixArchitect_XLS_CellModel _BarRangeTo;
        [AsixArchitect_XLS_Cell_Attribute(25, "Zakres słupka do")]
        public AsixArchitect_XLS_CellModel BarRangeTo
        {
            get { return _BarRangeTo; }
            set { _BarRangeTo = value; NotifyPropertyChanged("BarRangeTo"); }
        }

        private AsixArchitect_XLS_CellModel _Unit;
        [AsixArchitect_XLS_Cell_Attribute(26, "Jednostka")]
        public AsixArchitect_XLS_CellModel Unit
        {
            get { return _Unit; }
            set { _Unit = value; NotifyPropertyChanged("Unit"); }
        }

        private AsixArchitect_XLS_CellModel _CriticalMinimum;
        [AsixArchitect_XLS_Cell_Attribute(27, "Minimum krytyczne")]
        public AsixArchitect_XLS_CellModel CriticalMinimum
        {
            get { return _CriticalMinimum; }
            set { _CriticalMinimum = value; NotifyPropertyChanged("CriticalMinimum"); }
        }

        private AsixArchitect_XLS_CellModel _Minimum;
        [AsixArchitect_XLS_Cell_Attribute(28, "Minimum")]
        public AsixArchitect_XLS_CellModel Minimum
        {
            get { return _Minimum; }
            set { _Minimum = value; NotifyPropertyChanged("Minimum"); }
        }

        private AsixArchitect_XLS_CellModel _Maximum;
        [AsixArchitect_XLS_Cell_Attribute(29, "Maksimum")]
        public AsixArchitect_XLS_CellModel Maximum
        {
            get { return _Maximum; }
            set { _Maximum = value; NotifyPropertyChanged("Maximum"); }
        }

        private AsixArchitect_XLS_CellModel _CriticalMaximum;
        [AsixArchitect_XLS_Cell_Attribute(30, "Maksimum krytyczne")]
        public AsixArchitect_XLS_CellModel CriticalMaximum
        {
            get { return _CriticalMaximum; }
            set { _CriticalMaximum = value; NotifyPropertyChanged("CriticalMaximum"); }
        }

        private AsixArchitect_XLS_CellModel _ControlVariable;
        [AsixArchitect_XLS_Cell_Attribute(31, "Zmienna kontrolna")]
        public AsixArchitect_XLS_CellModel ControlVariable
        {
            get { return _ControlVariable; }
            set { _ControlVariable = value; NotifyPropertyChanged("ControlVariable"); }
        }

        private AsixArchitect_XLS_CellModel _TheMaskOfTheControlVariable;
        [AsixArchitect_XLS_Cell_Attribute(32, "Maska zmiennej kontrolnej")]
        public AsixArchitect_XLS_CellModel TheMaskOfTheControlVariable
        {
            get { return _TheMaskOfTheControlVariable; }
            set { _TheMaskOfTheControlVariable = value; NotifyPropertyChanged("TheMaskOfTheControlVariable"); }
        }

        private AsixArchitect_XLS_CellModel _StateNames;
        [AsixArchitect_XLS_Cell_Attribute(33, "Nazwy stanów")]
        public AsixArchitect_XLS_CellModel StateNames
        {
            get { return _StateNames; }
            set { _StateNames = value; NotifyPropertyChanged("StateNames"); }
        }

        private AsixArchitect_XLS_CellModel _SetOfStates;
        [AsixArchitect_XLS_Cell_Attribute(34, "Zestaw stanów")]
        public AsixArchitect_XLS_CellModel SetOfStates
        {
            get { return _SetOfStates; }
            set { _SetOfStates = value; NotifyPropertyChanged("SetOfStates"); }
        }

        private AsixArchitect_XLS_CellModel _StateValue;
        [AsixArchitect_XLS_Cell_Attribute(35, "Wartość stanu")]
        public AsixArchitect_XLS_CellModel StateValue
        {
            get { return _StateValue; }
            set { _StateValue = value; NotifyPropertyChanged("StateValue"); }
        }

        private AsixArchitect_XLS_CellModel _ChartTitle;
        [AsixArchitect_XLS_Cell_Attribute(36, "Tytuł wykresu")]
        public AsixArchitect_XLS_CellModel ChartTitle
        {
            get { return _ChartTitle; }
            set { _ChartTitle = value; NotifyPropertyChanged("ChartTitle"); }
        }

        private AsixArchitect_XLS_CellModel _DisplayRangeStep;
        [AsixArchitect_XLS_Cell_Attribute(37, "Zakres wyświetlania krok")]
        public AsixArchitect_XLS_CellModel DisplayRangeStep
        {
            get { return _DisplayRangeStep; }
            set { _DisplayRangeStep = value; NotifyPropertyChanged("DisplayRangeStep"); }
        }

        private AsixArchitect_XLS_CellModel _DisplayRangeWidth;
        [AsixArchitect_XLS_Cell_Attribute(38, "Zakres wyświetlania szerokość")]
        public AsixArchitect_XLS_CellModel DisplayRangeWidth
        {
            get { return _DisplayRangeWidth; }
            set { _DisplayRangeWidth = value; NotifyPropertyChanged("DisplayRangeWidth"); }
        }

        private AsixArchitect_XLS_CellModel _DivisionDisplayRange;
        [AsixArchitect_XLS_Cell_Attribute(39, "Zakres wyświetlania podział")]
        public AsixArchitect_XLS_CellModel DivisionDisplayRange
        {
            get { return _DivisionDisplayRange; }
            set { _DivisionDisplayRange = value; NotifyPropertyChanged("DivisionDisplayRange"); }
        }

        private AsixArchitect_XLS_CellModel _VariableIsInactive;
        [AsixArchitect_XLS_Cell_Attribute(40, "Zmienna nieaktywna")]
        public AsixArchitect_XLS_CellModel VariableIsInactive
        {
            get { return _VariableIsInactive; }
            set { _VariableIsInactive = value; NotifyPropertyChanged("VariableIsInactive"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public AsixArchitect_XLS_FileModel()
        {
            var props = this.GetType().GetProperties();
            foreach(var prop in props)
            {
                var attributes = prop.GetCustomAttributes(true).OfType<AsixArchitect_XLS_Cell_Attribute>();

                foreach(var attribute in attributes)
                {
                    prop.SetValue(this, new AsixArchitect_XLS_CellModel(attribute.Name, attribute.Index));
                }
            }
        }

        static AsixArchitect_XLS_FileModel()
        {
            var props = typeof(AsixArchitect_XLS_FileModel).GetProperties();
            foreach (var prop in props)
            {
                var attributes = prop.GetCustomAttributes(true).OfType<AsixArchitect_XLS_Cell_Attribute>();

                foreach (var attribute in attributes)
                {
                    Headers.Add(new AsixArchitect_XLS_Cell_Attribute(attribute.Index, attribute.Name));
                }
            }
        }

        public static List<AsixArchitect_XLS_Cell_Attribute> Headers = new List<AsixArchitect_XLS_Cell_Attribute>();

    }
}
