﻿using NPOI.SS.UserModel;
using SchemeEditor.Services.Asix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeEditor.Services.Asix
{
    public interface IAsixService
    {
        IWorkbook GenerateVarsToXLSFile(int startIndex, int objectSizeInBytes, Dictionary<string, string> settedProperties, out List<string> beckhoffModbusVarsDefinition);
        VariableBase GenerateVarsToXMLFile(Dictionary<string, string> settedProperties);
    }
}
