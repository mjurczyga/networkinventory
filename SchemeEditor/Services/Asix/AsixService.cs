﻿using Microsoft.Win32;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Models;
using SchemeControlsLib.Services;
using SchemeControlsLib.Utilities;
using SchemeEditor.Attributes;
using SchemeEditor.Helpers;
using SchemeEditor.Services.Asix;
using SchemeEditor.Services.Asix.Models;
using SchemeEditor.Services.Asix.StaticElemnts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeEditor.Services.Asix
{
    [ContainerRegistration(typeof(IAsixService), true)]
    public class AsixService : ServiceBase, IAsixService
    {
        private readonly ISchemeService _schemeService;

      
        public AsixService(ISchemeService schemeService)
        {
            _schemeService = schemeService;
        }

        // zmienne do architekta
        public IWorkbook GenerateVarsToXLSFile(int startIndex, int objectSizeInBytes, Dictionary<string, string> settedProperties, out List<string> beckhoffModbusVarsDefinition)
        {
            IWorkbook workbook = null;
            beckhoffModbusVarsDefinition = new List<string>();
            if (_schemeService.SchemeCanvas != null)
            {

                int MBNumber = startIndex;

                workbook = new HSSFWorkbook();

                ISheet sheet1 = workbook.CreateSheet("Zmienne");
                var rowIndex = 0;
                IRow row = sheet1.CreateRow(rowIndex);

                int i = 0;
                foreach (var header in AsixArchitect_XLS_FileModel.Headers)
                {
                    var cell = row.CreateCell(i);
                    cell.SetCellValue(header.Name);
                    i++;
                }
                rowIndex++;

                if (_schemeService.SchemeCanvas != null)
                {
                    var activeElements = _schemeService.SchemeCanvas.Children.OfType<ActiveElement>();

                    List<Variable> variableList = new List<Variable>();
                    foreach (var activeElement in activeElements)
                    {
                        var stateProperties = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(activeElement, ElementPropertiesModels.GetPropertiesByElementType(activeElement)).ToDictionary(x => x.Key as DependencyProperty, x => x.Value).Where(x => x.Key.Name.Contains("BindingVarName"));


                        foreach (var stateProperty in stateProperties)
                        {
                            var schemeElementVarName = activeElement.GetValue(stateProperty.Key)?.ToString();
                            if (!string.IsNullOrEmpty(schemeElementVarName))
                            {
                                IRow varRow = sheet1.CreateRow(rowIndex);
                                this.SetCell(ref varRow, "Nazwa", schemeElementVarName);
                                this.SetCell(ref varRow, "Adres", "HR" + (startIndex + rowIndex).ToString());
                                this.SetCell(ref varRow, "Liczba elementów", settedProperties);
                                this.SetCell(ref varRow, "Okres próbkowania", settedProperties);
                                this.SetCell(ref varRow, "Funkcja przeliczająca", settedProperties);
                                this.SetCell(ref varRow, "Kanał", settedProperties);
                                this.SetCell(ref varRow, "Sprawdzanie zakresu pomiarowego", settedProperties);
                                this.SetCell(ref varRow, "Rejestracja sterowania", settedProperties);
                                this.SetCell(ref varRow, "Zmienna nieaktywna", settedProperties);

                                beckhoffModbusVarsDefinition.Add($"{schemeElementVarName} AT%MB{MBNumber.ToString()} : INT; (* {schemeElementVarName} *)");
                                MBNumber += 2;
                                rowIndex++;
                            }

                        }
                    }

                }
            }
            return workbook;
        }

        private void SetCell(ref IRow row, string cellHeader, Dictionary<string, string> cellValues)
        {
            if (row != null && AsixArchitect_XLS_FileModel.Headers.Any(x => x.Name == cellHeader))
            {
                if (cellValues.TryGetValue(cellHeader, out string value))
                    SetCell(ref row, cellHeader, value);
                else
                    SetCell(ref row, cellHeader, cellValue: null);

            }

        }

        private void SetCell(ref IRow row, string cellHeader, string cellValue)
        {
            var cellName = AsixArchitect_XLS_FileModel.Headers.Where(x => x.Name == cellHeader).FirstOrDefault();

            if (row != null && cellName != null)
            {
                if (double.TryParse(cellValue, out double dblValue))
                    row.CreateCell(cellName.Index).SetCellValue(dblValue);
                else if (bool.TryParse(cellValue, out bool bValue))
                    row.CreateCell(cellName.Index).SetCellValue(bValue);
                else
                    row.CreateCell(cellName.Index).SetCellValue(cellValue);
            }
        }

        public VariableBase GenerateVarsToXMLFile(Dictionary<string, string> settedProperties)
        {
            VariableBase variableBase = new VariableBase();
            variableBase.Languages = new Languages() { Language = new List<int>() { 21 } };

            List<Models.XML_Attribute> attribute = new List<Models.XML_Attribute>();
            attribute.Add(new Models.XML_Attribute()
            {
                Name = "Name",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Nazwa" },
                    new LocalName() { LangId = 9, Text = "Name" }
                }
            });
            attribute.Add(new Models.XML_Attribute()
            {
                Name = "Description",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Opis" },
                    new LocalName() { LangId = 9, Text = "Description" }
                }
            });
            attribute.Add(new Models.XML_Attribute()
            {
                Name = "ValueType",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Typ wartości" },
                    new LocalName() { LangId = 9, Text = "Value type" }
                }
            });
            attribute.Add(new Models.XML_Attribute()
            {
                Name = "Name",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Format" },
                    new LocalName() { LangId = 9, Text = "Format" }
                }
            });
            attribute.Add(new Models.XML_Attribute()
            {
                Name = "Channel",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Kanał" },
                    new LocalName() { LangId = 9, Text = "Channel" }
                }
            });
            attribute.Add(new Models.XML_Attribute()
            {
                Name = "Address",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Adres" },
                    new LocalName() { LangId = 9, Text = "Address" }
                }
            });
            attribute.Add(new Models.XML_Attribute()
            {
                Name = "SampleRate",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Częstość odświeżania" },
                    new LocalName() { LangId = 9, Text = "Sampling Period" }
                }
            });
            attribute.Add(new Models.XML_Attribute()
            {
                Name = "ConversionFunction",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Funkcja przeliczająca" },
                    new LocalName() { LangId = 9, Text = "Converting Function" }
                }
            });
            attribute.Add(new Models.XML_Attribute()
            {
                Name = "ReadConvertingFunction",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Funkcja przeliczająca odczytu" },
                    new LocalName() { LangId = 9, Text = "Read Converting Function" }
                }
            });
            attribute.Add(new Models.XML_Attribute()
            {
                Name = "WriteConvertingFunction",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Funkcja przeliczająca zapisu" },
                    new LocalName() { LangId = 9, Text = "Write Converting Function" }
                }
            });
            attribute.Add(new Models.XML_Attribute()
            {
                Name = "ItemNotActive",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Nieaktywna" },
                    new LocalName() { LangId = 9, Text = "Not active" }
                }
            });
            attribute.Add(new Models.XML_Attribute()
            {
                Name = "DisplayRangeFrom",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Zakres wyświetlania od" },
                    new LocalName() { LangId = 9, Text = "Minimum Display Value" }
                }
            });
            attribute.Add(new Models.XML_Attribute()
            {
                Name = "DisplayRangeTo",
                LocalName = new List<LocalName>()
                {
                    new LocalName() { LangId = 21, Text = "Zakres wyświetlania do" },
                    new LocalName() { LangId = 9, Text = "Maximum Display Value" }
                }
            });

            variableBase.Attributes = new Models.XML_Attributes() { Attribute = attribute, Count = attribute.Count() };


            if (_schemeService.SchemeCanvas != null)
            {
                var activeElements = _schemeService.SchemeCanvas.Children.OfType<ActiveElement>();

                List<Variable> variableList = new List<Variable>();
                foreach (var activeElement in activeElements)
                {
                    var className = activeElement.GetType().GetCustomAttribute<EditorType>(false);
                    var stateProperties = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(activeElement, ElementPropertiesModels.GetPropertiesByElementType(activeElement)).ToDictionary(x => x.Key as DependencyProperty, x => x.Value).Where(x => x.Key.Name.Contains("BindingVarName"));

                    foreach (var stateProperty in stateProperties)
                    {
                        var schemeElementVarName = activeElement.GetValue(stateProperty.Key)?.ToString();
                        if (!string.IsNullOrEmpty(schemeElementVarName) && settedProperties.TryGetValue("Kanał", out string value))
                            variableList.Add(new Variable(schemeElementVarName, value, "Int16"));
                    }
                }
                variableBase.Variables = new Variables() { Count = variableList.Count(), Variable = variableList };
            }


            return variableBase;
        }

    }
}
