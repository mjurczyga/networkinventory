﻿using Autofac;
using SchemeEditor.Attributes;
using SchemeEditor.ViewModels.BaseViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SchemeEditor.Services
{
    [ContainerRegistration(typeof(IRegionManager), true)]
    public class RegionManager : ServiceBase, IRegionManager
    {
        private readonly IComponentContext _componentContext;

        private Window window = null;

        private Dictionary<string, ContentControl> _regionsByName = new Dictionary<string, ContentControl>();

        public Dictionary<string, ContentControl> RegionsByName
        {
            get
            {
                return _regionsByName;
            }
        }

        public Type LastContentType { get; private set; }

        public RegionManager(IComponentContext componentContext) : base()
        {
            _componentContext = componentContext;
        }

        public void RegisterRegion(ContentControl region)
        {
            if (IsRegionControlValid(region))
            {
                if (_regionsByName.ContainsKey(region.Uid))
                {
                    //Log.Debug($"Region o Uid: '{region.Uid}' jest już zarejestrowany.");
                    throw new ArgumentException($"Region o nazwie {region.Uid} został już zarejestrowany.", "region");
                }
                else
                {
                    _regionsByName.Add(region.Uid, region);
                    //Log.Debug($"Region o Uid: '{region.Uid}' został zarejestrowany poprawnie.");
                }
            }
        }

        public void ShowNewWindow(Type type, bool sizeToContent, bool showDialog = false, string title = "", WindowStyle windowStyle = WindowStyle.None, ResizeMode resizeMode = ResizeMode.NoResize)
        {
            object instance = null;

            instance = _componentContext.Resolve(type);

            ShowNewWindow(instance, sizeToContent, showDialog, title, windowStyle, resizeMode);
        }

        public void ShowNewWindow(object instance, bool sizeToContent, bool showDialog = false, string title = "", WindowStyle windowStyle = WindowStyle.None, ResizeMode resizeMode = ResizeMode.NoResize, bool topmost = false)
        {
            window = new Window();
            if (instance is IBaseViewModel)
            {
                (instance as IBaseViewModel).OnLoaded();
                window.Closed += (x, y) => (instance as IBaseViewModel).OnUnloaded();
            }
            window.Content = instance;
            window.Topmost = topmost;
            window.Title = title;
            window.WindowStyle = windowStyle;
            window.ResizeMode = resizeMode;
            window.SizeToContent = sizeToContent ? SizeToContent.WidthAndHeight : SizeToContent.Manual;
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            /*window.MouseDown += (s, e) =>
            {
                var keypadService = ServiceLocator.Current.GetInstance<IKeypadService>();
                if (keypadService.IsKeypadVisible)
                    keypadService.HideScreenKeypad();
            };*/


            if (showDialog)

                window.ShowDialog();
            else
                window.Show();

        }

        public void CloseWindow()
        {
            if (window != null)
            {
                window.Close();
            }
        }

        public void SwitchRegion(string regionUid, Type type)
        {
            object instance = null;

            instance = _componentContext.Resolve(type);

            SwitchRegion(regionUid, instance);
        }

        public object SwitchRegionAndGetInstance(string regionUid, Type type)
        {
            object instance = null;

            instance = _componentContext.Resolve(type);

            SwitchRegion(regionUid, instance);

            return instance;
        }

        public void SwitchRegion(string regionUid, object instance)
        {
            if (!string.IsNullOrEmpty(regionUid))
            {
                if (_regionsByName.ContainsKey(regionUid))
                {
                    if (instance != null)
                    {
                        if (_regionsByName[regionUid].Content != null)
                        {
                            LastContentType = _regionsByName[regionUid].Content.GetType();
                            if (_regionsByName[regionUid].Content is IBaseViewModel)
                                (_regionsByName[regionUid].Content as IBaseViewModel).OnUnloaded();
                        }

                        if (instance is IBaseViewModel)
                            (instance as IBaseViewModel).OnLoaded();

                        _regionsByName[regionUid].SetValue(ContentControl.ContentProperty, instance);
                        _regionsByName[regionUid].Visibility = System.Windows.Visibility.Visible;
                        //Log.Debug($"Wczytano {instance.GetType().Name} do regionu {regionUid}.");
                    }
                }
                else
                {
                    //Log.Error($"Nie znaleziono regionu o Uid: '{regionUid}'.");
                    throw new ArgumentException($"Nie znaleziono regionu o Uid: '{regionUid}'.", "regionUid");
                }
            }
            else
            {
                //Log.Error($"Nie ustawiony identyfikator Uid.");
                throw new ArgumentException($"Nie ustawiony identyfikator Uid.", "regionUid");
            }
        }

        public void CloseRegion(string regionUid)
        {
            if (!string.IsNullOrEmpty(regionUid))
            {
                if (_regionsByName.ContainsKey(regionUid))
                {
                    if (_regionsByName[regionUid].Content is IBaseViewModel)
                        (_regionsByName[regionUid].Content as IBaseViewModel).OnUnloaded();
                    _regionsByName[regionUid].Content = null;
                    _regionsByName[regionUid].Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

        public void HideRegion(string regionUid)
        {
            if (!string.IsNullOrEmpty(regionUid))
            {
                if (_regionsByName.ContainsKey(regionUid))
                {
                    _regionsByName[regionUid].Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

        public void ShowRegion(string regionUid)
        {
            if (!string.IsNullOrEmpty(regionUid))
            {
                if (_regionsByName.ContainsKey(regionUid))
                {
                    _regionsByName[regionUid].Visibility = System.Windows.Visibility.Visible;
                }
            }
        }

        public object ShowRegionAndGetInstance(string regionUid)
        {
            if (!string.IsNullOrEmpty(regionUid))
            {
                if (_regionsByName.ContainsKey(regionUid))
                {
                    _regionsByName[regionUid].Visibility = System.Windows.Visibility.Visible;
                    return _regionsByName[regionUid].Content;
                }
            }
            return null;
        }

        private bool IsRegionControlValid(ContentControl region)
        {
            if (string.IsNullOrEmpty(region.Uid))
            {
                //Log.Error("Nie ustawiony identyfikator Uid dla rejestrowanego regionu.");
                throw new ArgumentException("Nie ustawiony identyfikator 'Uid' dla rejestrowanego regionu.", "region");
            }
            return true;
        }
    }
}
