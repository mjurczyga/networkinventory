﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Models
{
    #region wersja (2.0.0.0)
    public class V2000_SerializableObject
    {
        public Type ControlType;

        public Dictionary<string, object> Properties;

        public List<V2000_ConnectionModel> Connections;
    }

    public class V2000_ConnectionModel
    {
        public Point OutputNode1_CenterPoint;

        public Point OutputNode2_CenterPoint;

        public Dictionary<string, object> SchemeLineProperties;
    }
    #endregion wersja (2.0.0.0)

    #region wersja (3.0.0.0)
    /// <summary>
    /// Model obiektów specyficznych dla danej biblioteki (kontrolek schematów)
    /// </summary>
    public class SerializableObject : SerializableCommonObject
    {
        public List<OutputNodeModel> ElementOutputs;

        public List<ConnectionModel> Connections;
    }

    /// <summary>
    /// Model obiektów wspólnych do serializacji - nie posiadających wejść/wyjść np. Pole tekstowe, wentylator itd
    /// </summary>
    public class SerializableCommonObject
    {
        public Type ControlType;

        public Dictionary<string, object> Properties;
    }

    /// <summary>
    /// Model połączenia (z jakiego punktu do jakiego punktu + właściwości linii)
    /// </summary>
    public class ConnectionModel
    {
        public Point OutputNode1_CenterPoint;

        public Point OutputNode2_CenterPoint;

        public Dictionary<string, object> SchemeLineProperties;
    }

    /// <summary>
    /// Model wyjścia 
    /// </summary>
    public class OutputNodeModel
    {
        public Point CenterPoint;

        public bool CheckBounds;

        public Dictionary<string, object> Properties;
    }
    #endregion wersja (3.0.0.0)
}
