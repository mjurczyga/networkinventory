﻿using SchemeControlsLib.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Models
{
    public class ResultMessage : ObservableObject
    {
        private string _Message;
        public string Message
        {
            get { return _Message; }
            set { _Message = value; NotifyPropertyChanged("Message"); }
        }

        private bool _HasError;
        public bool HasError
        {
            get { return _HasError; }
            set { _HasError = value; NotifyPropertyChanged("HasError"); }
        }

        public ResultMessage() { }

        public ResultMessage(bool hasError, string message = null)
        {
            HasError = hasError;
            Message = message;
        }
    }
}
