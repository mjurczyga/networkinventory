﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Models.PLCCodeGeneratorModels
{
    public class PLCConnectionModel
    {
        string PLCStructName { get; set; }
        int OutputId { get; set; }
    }
}
