﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CommonControls;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.ElectricControls;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Models
{
    public static class ElementPropertiesModels
    {      
        public static Dictionary<string, string> OutputNodesProperties = new Dictionary<string, string>()
        {
            { "OutputNodesCollection", "Wyjścia kontrolki" },
        };

        public static Dictionary<string, string> OutputNodesProperties1 = new Dictionary<string, string>()
        {
            { "OutputNodes", "Wyjścia kontrolki" },
        };

        public static Dictionary<string, EditorProperty> GetPropertiesByElementType(DependencyObject element, bool toSerialize = false)
        {
            Dictionary<string, EditorProperty> result = new Dictionary<string, EditorProperty>();
            var elementTypeProperties = element.GetType().GetProperties();

            foreach (PropertyInfo prop in elementTypeProperties)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    EditorProperty editorProperty = attr as EditorProperty;
                    if (editorProperty != null && (!toSerialize || (toSerialize && editorProperty.Serialize)))
                    {
                        result.Add(prop.Name, editorProperty);
                    }
                }
            }

            return result;
        }

        public static bool HasCommonProperties(DependencyObject control)
        {
            if (control != null)
            {
                var properties = ElementPropertiesModels.GetPropertiesByElementType(control);
                return properties.Any(x => x.Value.CommonProperty);
            }
            return false;
        }

        private static List<Type> missingTypes = new List<Type>();

        
    }
}
