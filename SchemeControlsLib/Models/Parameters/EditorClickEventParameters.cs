﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SchemeControlsLib.Models.Parameters
{
    public class EditorClickEventParameters
    {
        private string _commandPropertiesString = "";

        public string GetPropertyValue(string propertyName)
        {
            if (_commandPropertiesString.ToUpper().Contains(propertyName.ToUpper()) && !string.IsNullOrEmpty(propertyName))
            {
                var splittedProps = _commandPropertiesString.Replace("\r\n", "").Split(';');
                foreach (var splittedProp in splittedProps)
                {
                    if (splittedProp.ToUpper().StartsWith(propertyName.ToUpper()))
                        return Regex.Match(splittedProp, @"<(.+?)>").Groups[1].Value;
                }
            }

            return null;
        }

        public EditorClickEventParameters(string windowProperties)
        {
            _commandPropertiesString = windowProperties;
        }


    }
}
