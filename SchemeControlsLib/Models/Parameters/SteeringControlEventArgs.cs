﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Models.Parameters
{
    public delegate void SteeringControlEventDelegate(object obj, SteeringControlEventArgs args);

    public class SteeringControlEventArgs : EventArgs
    {
        public string VarName { get; set; }
        public string Value { get; set; }
    }
}
