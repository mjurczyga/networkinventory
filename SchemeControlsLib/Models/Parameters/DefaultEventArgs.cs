﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Models.Parameters
{
    public delegate void DefaultEventDelegate(object obj, DefaultEventArgs args);

    public class DefaultEventArgs : EventArgs
    {
        private object _Source;
        public object Source
        {
            get { return _Source; }
            set { _Source = value; }
        }


        private object _OldValue;
        public object OldValue
        {
            get { return _OldValue; }
            set { _OldValue = value; }
        }

        private object _NewValue;
        public object NewValue
        {
            get { return _NewValue; }
            set { _NewValue = value; }
        }

        public DefaultEventArgs(object source, object oldValue, object newValue)
        {
            Source = source;
            OldValue = oldValue;
            NewValue = newValue;
        }


    }
}
