﻿using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.ElectricControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Models
{
    public static class ElementConnectionModels
    {
        private static Dictionary<string, string> ElectricElementBaseConnections = new Dictionary<string, string>()
        {
            { "OutputNodes", "Wyjścia kontrolki" },
        };

        private static Dictionary<string, string> SourceConnections = new Dictionary<string, string>()
        {
            { "MultiOutputNode", "Multi wyjscie z źródła" },
        };

        public static Dictionary<string, string> GetReferencedPropertiesByElementType(UIElement element)
        {
            if (element is ElectricElementBase)
                return ElectricElementBaseConnections;

            return null;
        }
    }
}
