﻿using SchemeControlsLib.Controls.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Models
{
    public class OLD_SchemeFileModel 
    {
        public string EditorVersion { get; set; }

        public int SchemeType { get; set; }

        public List<V2000_SerializableObject> SchemeElements { get; set; }

        public OLD_SchemeFileModel()
        {

        }

        public OLD_SchemeFileModel(string editorVersion, int schemeType, List<V2000_SerializableObject> schemeElements)
        {
            EditorVersion = editorVersion;
            SchemeType = schemeType;
            SchemeElements = schemeElements;
        }
    }

    public class SchemeFileModel
    {
        public string EditorVersion { get; set; }

        public int SchemeType { get; set; }

        public Dictionary<string, string> SchemeProperties { get; set; }

        public List<SerializableObject> SchemeElements { get; set; }

        public List<SerializableCommonObject> SchemeCommonControls { get; set; }

        public SchemeFileModel()
        {

        }

        public SchemeFileModel(string editorVersion, int schemeType, List<SerializableObject> schemeElements, List<SerializableCommonObject> schemeCommonControls, Dictionary<string, string> schemeProperties)
        {
            EditorVersion = editorVersion;
            SchemeType = schemeType;
            SchemeElements = schemeElements;
            SchemeCommonControls = schemeCommonControls;
            SchemeProperties = schemeProperties;
        }
    }

}
