﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Models
{
    public class VarMappingModel : ObservableObject
    {
        private string _TypeName;
        public string TypeName { get { return _TypeName; } set { _TypeName = value; RaisePropertyChanged(); } }

        private string _ControlName;
        public string ControlName
        {
            get { return _ControlName; }
            set { _ControlName = value; RaisePropertyChanged(); }
        }

        private PropertyClass _StateVarNameProperty;
        public PropertyClass StateVarNameProperty { get { return _StateVarNameProperty; } set { _StateVarNameProperty = value; RaisePropertyChanged(() => StateVarNameProperty); } }

        private string _VarsName;
        public string VarsName
        {
            get { return _VarsName; }
            set { _VarsName = value; RaisePropertyChanged(); }
        }

        public VarMappingModel()
        {

        }

        public VarMappingModel(VarMappingModel varMappingModel)
        {
            TypeName = varMappingModel.TypeName;
            ControlName = varMappingModel.ControlName;
            StateVarNameProperty = varMappingModel.StateVarNameProperty;
            VarsName = varMappingModel.VarsName;
        }

        public VarMappingModel(string typeName, string controlName, PropertyClass stateVarNameProperty, string varsName)
        {
            TypeName = typeName;
            ControlName = controlName;
            StateVarNameProperty = stateVarNameProperty;
            VarsName = varsName;
        }

        public override bool Equals(object obj)
        {
            if (obj is VarMappingModel)
            {
                var compared = (VarMappingModel)obj;
                if (this.TypeName == compared.TypeName && this.ControlName == compared.ControlName && this.StateVarNameProperty == compared.StateVarNameProperty)
                    return true;
            }
            return false;
        }

    }

    public class PropertyClass : ObservableObject
    {
        private string _PropertyName;
        public string PropertyName { get { return _PropertyName; } set { _PropertyName = value; RaisePropertyChanged("PropertyName"); } }

        private string _PropertyText;
        public string PropertyText { get { return _PropertyText; } set { _PropertyText = value; RaisePropertyChanged("PropertyText"); } }

        public PropertyClass()
        {
        }

        public PropertyClass(string propertyName, string propertyText)
        {
            PropertyName = propertyName;
            PropertyText = propertyText;
        }
    }
}
