﻿using SchemeControlsLib.Base;
using SchemeControlsLib.Services;
using SchemeControlsLib.Services.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.ViewModels
{
    public class InfoWindowViewModel : ObservableObject
    {
        public string Text { get; set; }

        public LoadingProgress LoadingProgress { get; private set; }

        public InfoWindowViewModel()
        {
            LoadingProgress = LoadingProgress.Instance;
        }

    }
}
