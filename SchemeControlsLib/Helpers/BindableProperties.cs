﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Helpers
{
    public class IEnumerableProperty : DependencyObject
    {
        public IEnumerable<string> Value
        {
            get { return (IEnumerable<string>)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
            nameof(Value),
            typeof(IEnumerable<string>),
            typeof(IEnumerableProperty),
            new PropertyMetadata(null));
    }

    public class StringProperty : DependencyObject
    {
        public string Value
        {
            get { return (string)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
            nameof(Value),
            typeof(string),
            typeof(StringProperty),
            new PropertyMetadata(null));
    }
}
