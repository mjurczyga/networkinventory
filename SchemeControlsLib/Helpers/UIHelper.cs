﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Helpers
{
    public static class UIHelper
    {
        public static T FindControl<T>(this UIElement parent, string ControlName) where T : FrameworkElement
        {

            if (parent == null)
                return null;

            if (parent.GetType() == typeof(T) && ((T)parent).Name == ControlName)
            {
                return (T)parent;
            }
            T result = null;
            int count = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < count; i++)
            {
                UIElement child = (UIElement)VisualTreeHelper.GetChild(parent, i);

                if (FindControl<T>(child, ControlName) != null)
                {
                    result = FindControl<T>(child, ControlName);
                    break;
                }
            }
            return result;
        }

        public static T FindUserControl<T>(this UIElement child) where T : FrameworkElement
        {
            if (child == null)
                return null;

            var parent = VisualTreeHelper.GetParent(child);

            if (parent == null)
            {
                return (T)child;
            }

            //UIElement parent = (UIElement)VisualTreeHelper.GetParent(child);

            //if ((UIElement)VisualTreeHelper.GetParent(child) == null)
            //{
            //    return (T)child;
            //}

            T result = null;
            if (parent is UIElement)
            {
                if (FindUserControl<T>((UIElement)parent) != null)
                {
                    result = FindUserControl<T>((UIElement)parent);
                }
            }

            return result;
        }
    }
}
