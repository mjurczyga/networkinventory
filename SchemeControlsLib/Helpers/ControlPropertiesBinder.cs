﻿using SchemeControlsLib.Controls;
using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Converters;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Models;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace SchemeControlsLib.Helpers
{
    public static class ControlPropertiesBinder
    {
        public static List<string> DoSchemeControlsBindings(UIElementCollection elementCollection, string bindingPrefix, string bindingSuffix)
        {
            var resultList = new List<string>();

            // bindowanie właściwości zawierajace w nazwie BindingVarName (tylko kontrolki aktywne) do właściwości stanu - state 
            var activeElements = elementCollection.OfType<ActiveElement>();
            foreach (var activeElement in activeElements)
                DoActiveControlPropertiesBinding(activeElement, resultList, bindingPrefix, bindingSuffix);


            var bindingControls = elementCollection.OfType<IBindingControl>();
            foreach (var bindingControl in bindingControls)
                DoIBindingControlPropertiesBinding(bindingControl, resultList, bindingPrefix, bindingSuffix);

            return resultList;
        }


        private static void DoActiveControlPropertiesBinding(ActiveElement activeControl, List<string> varList, string bindingPrefix = null, string bindingSuffix = null)
        {
            var className = activeControl.GetType().GetCustomAttribute<EditorType>(false);
            var stateBindingVarNameProperties = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(activeControl, ElementPropertiesModels.GetPropertiesByElementType(activeControl)).ToDictionary(x => x.Key as DependencyProperty, x => x.Value).Where(x => x.Key.Name.Contains("BindingVarName")); //BindingVarName

            var stateProperties = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(activeControl, ElementPropertiesModels.GetPropertiesByElementType(activeControl)).ToDictionary(x => x.Key as DependencyProperty, x => x.Value).Where(x => x.Key.Name.EndsWith("State"));

            foreach (var stateProperty in stateProperties)
            {
                var stateVarNameProperty = (string)activeControl.GetValue(stateBindingVarNameProperties.Where(x => x.Key.Name == stateProperty.Key.Name + "BindingVarName").FirstOrDefault().Key);
                if (!string.IsNullOrEmpty(stateVarNameProperty))
                {

                    var binding = new Binding(bindingPrefix + stateVarNameProperty + bindingSuffix);
                    binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                    activeControl.SetBinding(stateProperty.Key, binding);

                    // dodanie do listy zmiennych wykorzystanych na schemacie
                    AddVarToList(varList, stateVarNameProperty);
                }
            }
        }

        private static void DoIBindingControlPropertiesBinding(IBindingControl bindingControl, List<string> varList, string bindingPrefix = null, string bindingSuffix = null)
        {

            if (bindingControl is FrameworkElement bindingElement)
            {
                #region bindowanie właściwości string - przyszłosciowo do wyrzucenia - wszystkie właściwości do bindowania powinny być typu OBJECT
                var properties = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(bindingElement, ElementPropertiesModels.GetPropertiesByElementType(bindingElement)).ToDictionary(x => x.Key as DependencyProperty, x => x.Value).Where(x => x.Key.PropertyType == typeof(string));
                foreach (var property in properties)
                {
                    var value = bindingElement.GetValue(property.Key);


                    if (value is string strValue && strValue.StartsWith("[") && strValue.EndsWith("]"))
                    { 

                        string varName = strValue.Replace("[", "").Replace("]", "").Trim();
                        var binding = new Binding(bindingPrefix + varName + bindingSuffix);
                        bindingElement.SetBinding(property.Key, binding);

                        // dodanie do listy zmiennych wykorzystanych na schemacie
                        if (!varList.Contains(varName))
                            varList.Add(varName);
                    }
                }
                #endregion bindowanie właściwości string - przyszłosciowo do wyrzucenia - wszystkie właściwości do bindowania powinny być typu OBJECT

                // dla zmiennych do zbindowania które są typu object
                var objProperties = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(bindingElement, ElementPropertiesModels.GetPropertiesByElementType(bindingElement)).ToDictionary(x => x.Key as DependencyProperty, x => x.Value).Where(x => x.Key.PropertyType == typeof(object));
                foreach (var property in objProperties)
                {
                    var value = bindingElement.GetValue(property.Key);
                    if (value is string strValue && (strValue).StartsWith("[") && (strValue).EndsWith("]"))
                    {

                        // bindowanie po podaniu ElementName
                        if (strValue.Contains("ElementName="))
                        {
                            var trimSpaces = strValue.Split(' ');
                            var elementName = trimSpaces.Where(x => string.IsNullOrEmpty(x.ToString()) && x.ToString().Contains("ElementName=")).FirstOrDefault().ToString().Replace("ElementName=", "");

                            var path = trimSpaces.Where(x => string.IsNullOrEmpty(x.ToString()) && x.ToString().Contains("Path=")).FirstOrDefault();
                            if (string.IsNullOrEmpty(path.ToString()))
                                path = path.Replace("Path=", "");
                            else
                                path = property.Key.Name;

                            var binding = new Binding(path) { ElementName = elementName };
                            bindingElement.SetBinding(property.Key, binding);

                        }
                        // bindowanie command i właściwości z datacontext
                        else if (strValue.ToUpper().Contains("COMMAND") || strValue.Contains("BaseProperty "))
                        {
                            var binding = new Binding((strValue).Replace("[", "").Replace("]", "").Replace("BaseProperty ", "").Trim());
                            bindingElement.SetBinding(property.Key, binding);
                        }
                        // bindowanie zmiennych
                        else
                        {
                            var vars = (strValue).Replace("[", "").Replace("]", "").Trim().Split(';');

                           

                            if (vars.Count() == 1)
                            {
                                if (vars[0].Split('.').Count() != 1)
                                {
                                    string bindingString = bindingPrefix + vars[0];
                                    if (Settings.IsITSApp)
                                        bindingString += bindingSuffix;

                                    

                                    var binding = new Binding(bindingString);  //TODO: ZMIANA DO ZMIANY XD! (usunąć suffix, nie pamiętam dlaczego ale to ważne - asix miał dodany)
                                    bindingElement.SetBinding(property.Key, binding);

                                    // dodanie do listy zmiennych wykorzystanych na schemacie
                                    var varToAdd = vars[0].Split('.')[0];
                                    AddVarToList(varList, varToAdd);
                                }
                                else
                                {
                                    var binding = new Binding(bindingPrefix + vars[0] + bindingSuffix);
                                    bindingElement.SetBinding(property.Key, binding);
                                    AddVarToList(varList, vars[0]);
                                }
                            }
                            else if (vars.Count() > 1)
                            {
                                var multiBinding = new MultiBinding() { Converter = new MultiStateToBoolAndConverter() };
                                foreach (var varName in vars)
                                {
                                    multiBinding.Bindings.Add(new Binding(bindingPrefix + varName + bindingSuffix));
                                    AddVarToList(varList, varName);
                                }
                                bindingElement.SetBinding(property.Key, multiBinding);
                            }
                        }
                    }
                }
            }
        }

        private static void AddVarToList(List<string> varList, string var)
        {
            if (!varList.Contains(var))
                varList.Add(var);
        }
    }
}
