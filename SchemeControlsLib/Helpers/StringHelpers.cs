﻿using SchemeControlsLib.Controls.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Helpers
{
    public static class TextExt
    {
        public static double ComputeFontSize(this string str, Size avaiableSpace)
        {
            if (!str.CheckIfIsNullOrEpmpty())
            {
                double resultFontSize = 1;
                double width = avaiableSpace.Width;
                Size resultTextSize = new Size(0, 0);
                while (resultTextSize.Width <= avaiableSpace.Width && resultTextSize.Height <= avaiableSpace.Height)
                {
                    // tekst kontrolki      
                    Typeface typeFace = new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Normal, FontStretches.Normal);
                    FormattedText formattedText = new FormattedText(str, CultureInfo.CurrentUICulture, FlowDirection.LeftToRight, typeFace, resultFontSize, Brushes.Black);

                    resultTextSize.Width = formattedText.Width;
                    resultTextSize.Height = formattedText.Height;
                    resultFontSize += 0.5;
                }

                return resultFontSize;
            }
            else return 0;
        }

        public static Size ComputeTextSize(this string str, double fontSize)
        {
            if (!str.CheckIfIsNullOrEpmpty())
            {
                Typeface typeFace = new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Normal, FontStretches.Normal);
                FormattedText formattedText = new FormattedText(str, CultureInfo.CurrentUICulture, FlowDirection.LeftToRight, typeFace, fontSize, Brushes.Black);
                return new Size(formattedText.Width, formattedText.Height);
            }

            return new Size(0, 0); ;
        }

        public static Size ComputeTextSize(this string str, double fontSize, FontFamily fontFamily, FontStyle fontStyle, FontWeight fontWeight, FontStretch fontStretche)
        {
            if (!str.CheckIfIsNullOrEpmpty())
            {
                Typeface typeFace = new Typeface(fontFamily, fontStyle, fontWeight, fontStretche);
                FormattedText formattedText = new FormattedText(str, CultureInfo.CurrentUICulture, FlowDirection.LeftToRight, typeFace, fontSize, Brushes.Black);
                return new Size(formattedText.Width, formattedText.Height);
            }

            return new Size(0, 0); ;
        }


        public static bool CheckIfIsNullOrEpmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static bool CheckIfStringIsSymbol(string str, char exceptionChar)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if (!Char.IsDigit(str[i]) && !((str[i] >= 'A' && str[i] <= 'Z') || (str[i] >= 'a' && str[i] <= 'z')) && str[i] != ' ')
                {
                    if (str[i] != exceptionChar)
                        return true;
                }
            }
            return false;
        }

        public static bool CheckIfAlphabetic(string str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if (!Char.IsLetter(str[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool CheckIfNumber(string str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if (!Char.IsDigit(str[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool CheckIfSpaces(string str)
        {
            return str.Contains(' ');
        }

        public static string GetNumberAndSpecial(string str)
        {
            StringBuilder outString =
                     new StringBuilder();

            for (int i = 0; i < str.Length; i++)
            {
                if (Char.IsDigit(str[i]) || !((str[i] >= 'A' && str[i] <= 'Z') || (str[i] >= 'a' && str[i] <= 'z')))
                    outString.Append(str[i]);
            }

            return outString.ToString();
        }

        public static string GetAlphabetic(string str)
        {
            StringBuilder outString =
                     new StringBuilder();

            for (int i = 0; i < str.Length; i++)
            {
                if ((str[i] >= 'A' && str[i] <= 'Z') || (str[i] >= 'a' && str[i] <= 'z'))
                    outString.Append(str[i]);
            }

            return outString.ToString();
        }

        public static string GetAlphabeticToFirstNumber(string str)
        {
            StringBuilder outString =
                     new StringBuilder();

            for (int i = 0; i < str.Length; i++)
            {
                if (Char.IsDigit(str[i]))
                    break;

                if ((str[i] >= 'A' && str[i] <= 'Z') || (str[i] >= 'a' && str[i] <= 'z'))
                    outString.Append(str[i]);
            }

            return outString.ToString();
        }

        public static string GetNumberAndSpecialWithAlphabeticAfterNumeric(string str)
        {
            StringBuilder outString =
                     new StringBuilder();
            bool containsNumeric = false;

            for (int i = 0; i < str.Length; i++)
            {
                if (Char.IsDigit(str[i]) || (((str[i] >= 'A' && str[i] <= 'Z') || (str[i] >= 'a' && str[i] <= 'z')) && containsNumeric))
                {
                    outString.Append(str[i]);
                    containsNumeric = true;
                }
            }

            return outString.ToString();
        }

        public static bool CheckIfIsControlName(this string name)
        {
            if (!string.IsNullOrEmpty(name) && !CheckIfIsNullOrEpmpty(name) && !CheckIfStringIsSymbol(name, '_') && !CheckIfSpaces(name))
                return true;

            return false;
        }

        /// <summary>
        /// Finding maximal length (width) of text when using new line mark (\n)
        /// </summary>
        /// <param name="label">text</param>
        /// <returns>length - letters </returns>
        public static int GetMaxLength(this string label)
        {
            int length = label.Length;
            if (label.Contains("\n"))
            {
                string[] split = label.Split(new string[] { "\n" }, StringSplitOptions.None);
                int maxLength = 0;
                for (int i = 0; i < split.Length; i++)
                {
                    if (maxLength < split[i].Length)
                        maxLength = split[i].Length + 2;
                }
                length = maxLength;
            }

            return length;
        }


        #region Crypto
        /// <summary>
        /// Sprawdza czy zaszyfrowany plik zawiera wskazany nymer wersji biblioteki
        /// </summary>
        /// <param name="inputString">zaszyfrowany ciąg</param>
        /// <param name="versionComparer">numer wersji do porównania</param>
        /// <returns></returns>
        public static bool IsDecryptedVersion(this string inputString, string versionComparer, out string jsonObject)
        {
            var decrypted = Decrypt(inputString);
            if(decrypted != null && decrypted.Contains($"\"EditorVersion\":\"{versionComparer}\""))
            {
                jsonObject = decrypted;
                return true;
            }
            else
            {
                jsonObject = "";
                return false;
            }
        }

        private static byte[] key = new byte[8] { 1, 2, 3, 4, 5, 6, 7, 8 };
        private static byte[] iv = new byte[8] { 1, 2, 3, 4, 5, 6, 7, 8 };

        public static string Crypt(this string text)
        {
            text = text.Compress();
            SymmetricAlgorithm algorithm = DES.Create();
            ICryptoTransform transform = algorithm.CreateEncryptor(key, iv);
            byte[] inputbuffer = Encoding.Unicode.GetBytes(text);
            byte[] outputBuffer = transform.TransformFinalBlock(inputbuffer, 0, inputbuffer.Length);
            return Convert.ToBase64String(outputBuffer);
        }

        public static string Decrypt(this string text)
        {
            try
            {
                SymmetricAlgorithm algorithm = DES.Create();
                ICryptoTransform transform = algorithm.CreateDecryptor(key, iv);
                byte[] inputbuffer = Convert.FromBase64String(text);
                byte[] outputBuffer = transform.TransformFinalBlock(inputbuffer, 0, inputbuffer.Length);


                var decompressed = Encoding.Unicode.GetString(outputBuffer).Decompress();
                return decompressed;
            }
            catch(Exception e)
            {
                return "";
            }
        }
        #endregion Crypto

        #region Compressor

        public static string Compress(this string uncompressedString)
        {
            byte[] compressedBytes;

            using (var uncompressedStream = new MemoryStream(Encoding.UTF8.GetBytes(uncompressedString)))
            {
                using (var compressedStream = new MemoryStream())
                {
                    using (var compressorStream = new DeflateStream(compressedStream, CompressionLevel.Optimal, true))
                    {
                        uncompressedStream.CopyTo(compressorStream);
                    }

                    compressedBytes = compressedStream.ToArray();
                }
            }

            return Convert.ToBase64String(compressedBytes);
        }

        public static string Decompress(this string compressedString)
        {
            byte[] decompressedBytes;

            var compressedStream = new MemoryStream(Convert.FromBase64String(compressedString));

            using (var decompressorStream = new DeflateStream(compressedStream, CompressionMode.Decompress))
            {
                using (var decompressedStream = new MemoryStream())
                {
                    decompressorStream.CopyTo(decompressedStream);

                    decompressedBytes = decompressedStream.ToArray();
                }
            }

            return Encoding.UTF8.GetString(decompressedBytes);
        }

        #endregion Compressor
    }
}
