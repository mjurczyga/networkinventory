﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Helpers
{
    public static class MathHelpers
    {
        public static double NearestRound(double x, double delX)
        {
            if (delX < 1)
            {
                double i = (double)Math.Floor(x);
                double x2 = i;
                while ((x2 += delX) < x) ;
                double x1 = x2 - delX;
                return (Math.Abs(x - x1) < Math.Abs(x - x2)) ? x1 : x2;
            }
            else
            {
                return (float)Math.Round(x / delX, MidpointRounding.AwayFromZero) * delX;
            }
        }

    }
}
