﻿using SchemeControlsLib.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SchemeControlsLib.Helpers.ValidationRules
{
    public class FrameworkElementlNameValidationRule : ValidationRule
    {
        public IEnumerableProperty IEnumerableProperty { get; set; }

        public StringProperty StringProperty { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value is string)
            {
                var name = (string)value;
                if (TextExt.CheckIfIsNullOrEpmpty(name))
                    return new ValidationResult(false, "Nazwa nie może być pusta");
                else if(TextExt.CheckIfNumber(name[0].ToString()))
                    return new ValidationResult(false, "Nazwa nie może zaczynać się od liczby");
                else if (TextExt.CheckIfStringIsSymbol(name, '_'))
                    return new ValidationResult(false, "Nazwa nie może zawierać znaków specjalnych (wyjątek '_')");
                else if (TextExt.CheckIfSpaces(name))
                    return new ValidationResult(false, "Nazwa nie może zawierać spacji");
                else if (IEnumerableProperty?.Value != null && IEnumerableProperty.Value.Contains(name) && name != StringProperty?.Value)
                    return new ValidationResult(false, "Nazwa jest już używana");
                else
                    return new ValidationResult(true, null);
            }
            return new ValidationResult(false, "Sprawdzana nazwa musi być typu string");
        }
    }
}
