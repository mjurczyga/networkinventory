﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace SchemeControlsLib.Converters
{
    public class FillLevelToHeightConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (double.TryParse(value?.ToString(), out double dblValue) && double.TryParse(parameter?.ToString(), out double dblParameter))
            {
                var grBrusch = new LinearGradientBrush() { StartPoint = new System.Windows.Point(0, 0), EndPoint = new System.Windows.Point(0, 1) };

                var div = 1.0;
                if (dblParameter != 0)
                    div = dblParameter;

                var fillLevel = 1 - dblValue / div;

                grBrusch.GradientStops.Add(new GradientStop(Brushes.Gray.Color, fillLevel));
                grBrusch.GradientStops.Add(new GradientStop(Brushes.Black.Color, fillLevel));

                return grBrusch;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
