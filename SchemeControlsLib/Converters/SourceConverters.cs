﻿using SchemeControlsLib.Controls.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace SchemeControlsLib.Converters
{
    public class IntSourceToSourceBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (int.TryParse(value?.ToString(), out int intValue) && parameter is IEnumerable<ISource> sources)
            {
                return sources.Where(x => x.Name.Contains(intValue.ToString())).FirstOrDefault()?.SourceBrush;
            }

            return Brushes.Transparent;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class MultiIntSourceToSourceBrushConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Count() == 2 && int.TryParse(values[0]?.ToString(), out int intValue) && values[1] is IEnumerable<ISource> sources)
            {
                return sources.Where(x => x.Name.Contains(intValue.ToString())).FirstOrDefault()?.SourceBrush;
            }

            return Brushes.Transparent;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IntSourceToSourceNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (int.TryParse(value?.ToString(), out int intValue) && parameter is IEnumerable<ISource> sources)
            {
                return sources.Where(x => x.Name.Contains(intValue.ToString())).FirstOrDefault()?.Name;
            }

            return "---";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IntSourceToSourceDescriptionConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Count() == 2 && int.TryParse(values[0]?.ToString(), out int intValue) && values[1] is IEnumerable<ISource> sources)
            {
                return sources.Where(x => x.Name.Contains(intValue.ToString())).FirstOrDefault()?.SourceDescription;
            }

            return "---";
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
