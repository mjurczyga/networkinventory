﻿using SchemeControlsLib.Base;
using SchemeControlsLib.SchemeControlTypes.TypeConverters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.SchemeControlTypes
{
    [TypeConverter(typeof(SpecificPlaceTypeConverter))]
    public struct SpecificPlace : IEquatable<SpecificPlace>
    {
        public double Top { get; set; }

        public double Left { get; set; }

        public double Scale { get; set; }

        public SpecificPlace(double top, double left, double scale)
        {
            this.Top = top;
            this.Left = left;
            this.Scale = scale;
        }

        public SpecificPlace(double value)
        {
            this.Top = value;
            this.Left = value;
            this.Scale = value;
        }

        public bool Equals(SpecificPlace other)
        {
            if (other == null)
                return false;

            if (this.Top == other.Top && this.Left == other.Left && this.Scale == other.Scale)
                return true;

            return false;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
         
            if (obj is SpecificPlace)
                return Equals((SpecificPlace)obj);
            else
                return Equals(obj);
        }

        public override int GetHashCode()
        {
            return this.Scale.GetHashCode();
        }

        public static bool operator ==(SpecificPlace place1, SpecificPlace place2)
        {
            if (((object)place1) == null || ((object)place2) == null)
                return Object.Equals(place1, place2);

            return place1.Equals(place2);
        }

        public static bool operator !=(SpecificPlace place1, SpecificPlace place2)
        {
            if (((object)place1) == null || ((object)place2) == null)
                return !Object.Equals(place1, place2);

            return !(place1.Equals(place2));
        }

        public void CutSpecificPlaceToWidthAndHeight(double width, double height)
        {
            if (!double.IsNaN(width) && !double.IsNaN(height))
            {
                Size zoomedFrameSize = new Size(Math.Abs(width / this.Scale), Math.Abs(height / this.Scale));

                if (this.Left < 0)
                    this.Left = 0;
                else if (this.Left + zoomedFrameSize.Width > width)
                    this.Left = width - zoomedFrameSize.Width;

                if (this.Top < 0)
                    this.Top = 0;
                else if (this.Top + zoomedFrameSize.Height > height)
                    this.Top = height - zoomedFrameSize.Height;
            }
        }
    }
}
