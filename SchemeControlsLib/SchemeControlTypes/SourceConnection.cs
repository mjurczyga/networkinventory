﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.SchemeControlTypes
{
    public class SourceConnection
    {
        public bool HasConnection { get; set; }

        public Brush SourceBrush { get; set; } 

        public SourceConnection(bool hasDirectConnetionToSource, Brush directConnectedSourceBrush)
        {
            HasConnection = hasDirectConnetionToSource;
            SourceBrush = directConnectedSourceBrush;
        }
    }
}
