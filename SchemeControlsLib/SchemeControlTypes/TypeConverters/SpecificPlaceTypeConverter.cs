﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.SchemeControlTypes.TypeConverters
{
    public class SpecificPlaceTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var casted = value as string;
            if (casted != null)
            {
                var castedArray = casted.Split(' ');
                if (castedArray.Count() == 1 && double.TryParse(castedArray[0].ToString(), out double result))
                    return new SpecificPlace(result);
                if (castedArray.Count() == 3 && double.TryParse(castedArray[0].ToString(), out double result0) && double.TryParse(castedArray[1].ToString(), out double result1) && double.TryParse(castedArray[2].ToString(), out double result2))
                    return new SpecificPlace(result0, result1, result2);
            }


            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (value is SpecificPlace)
            {
                var casted = (SpecificPlace)value;
                 if(destinationType == typeof(string))
                    return casted.Left + " " + casted.Top + " " + casted.Scale;

            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
