﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.EnumTypes
{ 
    public enum NodeType
    {
        IO,
        I,
        O,
    }

    public enum SwitchStatuses
    {
        None,
        Open,
        Closed
    }

    public enum SwitchTypes
    {
        Odlacznik = 0,
        Przelacznik = 1,
        Rozlacznik = 2,
        Stycznik = 3,
        Wylacznik = 4
    }

    public enum TrolleyTypes
    {
        Przejscie,
        Pomiar,
        Bezpiecznik
    }

    public enum SwitchPlacement
    {
        Switch = 0,
        Trolley = 1,
    }

    public enum StateTypes
    {
        None = 0,
        Open = 1,
        Closed = 2,
    }


    public enum SchemeTypes
    {
        Common = 0,
        SynopticScheme = 1,
        NetworkScheme = 2,
        CoalProcessingScheme = 3,
    }

    public enum OrientationExt
    {
        LeftTop,
        LeftBottom,
        RightTop,
        RightBottom,
    }
}
