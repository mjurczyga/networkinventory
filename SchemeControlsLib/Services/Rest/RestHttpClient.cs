﻿using SchemeControlsLib.Services.Rest.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Services.Rest
{
    public static class RestHttpClient
    {
        static readonly HttpClient httpClient = new HttpClient();

        static RestHttpClient()
        {
            SetConnection(RestApiSettings.Settings.Ip, RestApiSettings.Settings.Port);
        }

        public static void SetConnection(string ip, string port)
        {
            httpClient.BaseAddress = new Uri($"http://{ip}:{port}/");
        }

        public static async Task<bool> GetDeviceStatus(string ip)
        {
            bool result = false;
            try
            {
                HttpResponseMessage response = await httpClient.GetAsync($"{RestApiSettings.PingPath}?ipAddress={ip}");
                result = response.IsSuccessStatusCode;
            }
            catch (Exception e)
            {
                var a = e.Message;
            }

            return result;
        }
    }
}
