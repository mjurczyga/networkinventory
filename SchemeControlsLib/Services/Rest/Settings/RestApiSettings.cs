﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Services.Rest.Settings
{
    public static class RestApiSettings
    {
        public static RestApiSettingsModel Settings { get; set; } = new RestApiSettingsModel("192.168.1.119", "7779");

        public static string PingPath = "/api/ping/ping";
    }
}
