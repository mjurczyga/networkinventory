﻿using SchemeControlsLib.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Services.Rest.Settings
{
    public class RestApiSettingsModel : ObservableObject
    {
        private string _Ip;
        public string Ip
        {
            get { return _Ip; }
            set { _Ip = value; NotifyPropertyChanged("Ip"); }
        }

        private string _Port;
        public string Port
        {
            get { return _Port; }
            set { _Port = value; NotifyPropertyChanged("Port"); }
        }

        public RestApiSettingsModel()
        {

        }

        public RestApiSettingsModel(string ip, string port)
        {
            Ip = ip;
            Port = port;
        }
    }
}
