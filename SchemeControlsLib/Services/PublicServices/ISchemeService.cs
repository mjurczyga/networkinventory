﻿using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Services
{
    public interface ISchemeService
    {
        IEnumerable<ISource> SchemeSources { get; }
        SchemeCanvas SchemeCanvas { get; }

        void SetSchemeCanvas(SchemeCanvas schemeCanvas);
    }
}
