﻿using SchemeControlsLib.Controls;
using SchemeControlsLib.Controls.ElectricControls;
using SchemeControlsLib.Controls.ElectricControls._Core.Interfaces;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Services
{
    public class SchemeService : ISchemeService
    {
        public event EventHandler ServiceInitializedEvent;

        private void _evoObjectExtension_EvoObjectPropertyChangedEvent(object sender, EventArgs evoObjectPropertyChangedEventArgs)
        {
            ServiceInitializedEvent?.Invoke(sender, evoObjectPropertyChangedEventArgs);
        }

        public SchemeCanvas SchemeCanvas { get; private set; }

        public virtual void ActiveElementClickEvent(string steringVarName, string steeringValue)
        {

        }

        public IEnumerable<ISource> SchemeSources
        {
            get
            {
                if (SchemeCanvas?.Children != null)
                {
                    var sources = SchemeCanvas.Children.OfType<ISource>().Where(x => x.GetType() != typeof(Ground));
                    return sources;
                }
                return null;
            }
        }

        public void SetSchemeCanvas(SchemeCanvas schemeCanvas)
        {
            SchemeCanvas = schemeCanvas;
            //if(Settings.IsITSApp && schemeCanvas != null)
            //{
            //    foreach(var steeringControl in schemeCanvas.Children.OfType<ISteeringControl>())
            //    {
            //        if(!string.IsNullOrEmpty(steeringControl.SteeringValue) && !steeringControl)
            //    }
            //}
        }
    }
}
