﻿using Newtonsoft.Json;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.Controls.ElectricControls;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.Models;
using SchemeControlsLib.Services.Mapping;
using SchemeControlsLib.Services.Deserializers;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using SchemeControlsLib.Controls.CoalProcessing.Types;
using System.Security.Cryptography;
using System.Text;
using SchemeControlsLib.Helpers;
using SchemeControlsLib.Controls;

namespace SchemeControlsLib.Services
{
    public static class SchemeSerializationService
    {
        private static Dispatcher _dispatcher = Dispatcher.CurrentDispatcher; //Application.Current.Dispatcher;

        public static string ApplicationSettingsFile;

        private static JsonSerializer _jsonSerializer;

        static SchemeSerializationService()
        {
            _jsonSerializer = new JsonSerializer();
            _jsonSerializer.NullValueHandling = NullValueHandling.Include;
            _jsonSerializer.Formatting = Formatting.Indented;
        }

        public static void Serialize<T>(T configuration, string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                throw new NullReferenceException("Wartość parametru 'fileName' - NULL");

            using (StreamWriter streamWriter = new StreamWriter(fileName, false))
            using (JsonWriter jsonWriter = new JsonTextWriter(streamWriter))
            {
                _jsonSerializer.Serialize(jsonWriter, configuration);
            }
        }

        public static T Deserialize<T>(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                throw new NullReferenceException("Wartość parametru 'fileName' - NULL");

            object deserialized = null;

            T result = (T)deserialized;

            if (File.Exists(fileName))
            {
                try
                {
                    string jsonObject = File.ReadAllText(fileName);
                    result = DeserializeText<T>(jsonObject);
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public static T DeserializeText<T>(string jsonObject)
        {
            if (string.IsNullOrEmpty(jsonObject))
                throw new NullReferenceException("Wartość parametru 'fileName' - NULL");

            object deserialized = null;

            T result = (T)deserialized;

            try
            {
                deserialized = JsonConvert.DeserializeObject<T>(jsonObject);
                result = (T)Convert.ChangeType(deserialized, typeof(T));
            }
            catch (Exception ex)
            {

            }


            return result;
        }

        /// <summary>
        /// Metoda wykonująca serializację elementów schematu
        /// </summary>
        /// <param name="configuration">Lista elemetów zawartych w SchemeCanvas</param>
        /// <param name="fileName">Nazwa pliku do zapisu</param>
        public static void SerializeSchemeComponents(List<ObservableControl> configuration, string fileName, int schemeType)
        {
            if (string.IsNullOrEmpty(fileName))
                throw new NullReferenceException("Wartość parametru 'fileName' - NULL");

            // kopia schematu
            if (File.Exists(fileName))
            {
                var currentFileFolder = fileName.Substring(0, fileName.LastIndexOf("\\"));
                var schemeName = fileName.Replace(currentFileFolder + "\\", "");
                var newDirectory = Path.Combine(currentFileFolder, "Backups");

                if (!Directory.Exists(Path.Combine(newDirectory)))
                    Directory.CreateDirectory(newDirectory);

                var newFilePath = Path.Combine(newDirectory, schemeName + $"_{DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss")}");
                File.Copy(fileName, newFilePath);
            }



            #region Schematy elektryczne
            // serializacja elementów klasy electric element base
            List<SerializableObject> schemeControls = PrepareElectricElementBaseToSerialize(configuration.OfType<ElectricElementBase>().ToList());
            //serializacja źródeł
            List<SerializableObject> sourceToSerialize = PrepareSourcesToSerialize(configuration.OfType<Source>().ToList());
            ListExt.AddListToList(schemeControls, sourceToSerialize);
            #endregion Schematy elektryczne

            #region Common control
            // wspolne kontrolki     
            List<SerializableCommonObject> commonControls = PrepareCommonControlBaseToSerialize(configuration.OfType<CommonControlBase>().ToList());
            //ListExt.AddListToList(toSerialize, commonControls);
            #endregion Common control

            #region Network controls
            List<SerializableObject> controlsWithOutputs = PrepareNetworkControlsToSerialize(configuration.OfType<IControlWithOutputs>().ToList());
            ListExt.AddListToList(schemeControls, controlsWithOutputs);
            #endregion Network controls

            var schemeProperties = new Dictionary<string, string>();
            schemeProperties.Add("STATUS_0_BRUSH", LibSettings.STATUS_0_BRUSH.ToString());
            schemeProperties.Add("STATUS_1_BRUSH", LibSettings.STATUS_1_BRUSH.ToString());
            schemeProperties.Add("STATUS_2_BRUSH", LibSettings.STATUS_2_BRUSH.ToString());
            schemeProperties.Add("STATUS_DEFAULT_BRUSH", LibSettings.STATUS_DEFAULT_BRUSH.ToString());
            var fileModel = new SchemeFileModel(Assembly.GetAssembly(typeof(SerializableObject)).GetName().Version.ToString(), schemeType, schemeControls, commonControls, schemeProperties);

            //using (StreamWriter streamWriter = new StreamWriter(fileName, false))
            //using (JsonWriter jsonWriter = new JsonTextWriter(streamWriter))
            //{
            //    _jsonSerializer.Serialize(jsonWriter, fileModel);
            //}


            //_jsonSerializer.Serialize(jsonWriter, fileModel);

            using (StreamWriter streamWriter = new StreamWriter(fileName, false))
            {
                string components = JsonConvert.SerializeObject(fileModel);
                components = components.Crypt();
                streamWriter.Write(components);
            }


        }

        /// <summary>
        /// Metoda wykonuje deserializację pliku z elementami biblioteki. Po deserializacji elementy zostają dodane do schematu. 
        /// </summary>
        /// <param name="fileName">Nazwa pliku z elementami</param>
        /// <param name="schemeCanvas">Canvas do którego elementy mają być odane</param>
        /// <param name="elementMouseDownAction">akcja na kliknięcie w element lewym przyciskiem myszy</param>
        /// <param name="elementContextMenu">akcja na kliknięcie w element prawym przyciskiem myszy</param>
        /// <param name="lineContextMenu">akcja na kliknięcie w polączenie (linie) prawym przyciskiem myszy</param>
        public static void DeserializeSchemeComponents(string fileName, SchemeCanvas schemeCanvas, MouseButtonEventHandler elementMouseDownAction = null, ContextMenu elementContextMenu = null)
        {

            var fileAllText = "";
            try
            {
                fileAllText = File.ReadAllText(fileName);
                LibMessagerService.ShowMessage(fileName, LibMessageTypes.INFO, SchemeControlsLibResources.GetIconBrushByKey("CurrentFile"));
            }
            catch
            {
                LibMessagerService.ShowMessage("Błąd otwarcia pliku schematu", LibMessageTypes.ALERT, SchemeControlsLibResources.GetIconBrushByKey("DocumentError")); 
            }


            // Deserializer dla wersji 3.0.0.0
            if (fileAllText.IsDecryptedVersion("3.1.0.0", out string jsonObject))
            {
                V3100_Deserialize.DeserializeSchemeComponents(jsonObject, schemeCanvas, elementMouseDownAction, elementContextMenu, fileAllText);
            }
            else if (fileAllText.Contains("\"EditorVersion\": \"3.0.0.0\""))
                V3000_Deserialize.DeserializeSchemeComponents(schemeCanvas, elementMouseDownAction, elementContextMenu, fileAllText);
            else if (fileAllText.Contains("ControlType"))
                V2000_Deserialize.DeserializeSchemeComponents(fileName, fileAllText, schemeCanvas, elementMouseDownAction, elementContextMenu, fileAllText);
            else
            {
                LibMessagerService.ShowMessage("Błąd podczas ładowania pliku schematu", LibMessageTypes.ALERT, SchemeControlsLibResources.GetIconBrushByKey("DocumentError"));
            }
        }

        #region Serializer private methods
        public static List<string> KR2_Switchboards = new List<string>()
        {
            { "ZPZ" },
            { "ZP2X400V" },
            { "PAC" },
            { "KR2" },
        };

        /// <summary>
        /// Przygotowuje elementy schematu klasy ElectricElementBase do serializacji
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        private static List<SerializableObject> PrepareElectricElementBaseToSerialize(List<ElectricElementBase> configuration)
        {
            List<SerializableObject> toSerialize = new List<SerializableObject>();
            foreach (var element in configuration)
            {
                var properties = ElementPropertiesModels.GetPropertiesByElementType(element);
                var elementCurrentProperties = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(element, properties, true);
                var elementDependencyProperties = elementCurrentProperties.Where(x => x.Key is DependencyProperty).Select(x => x.Key as DependencyProperty).ToList();
                var elementNormalProperties = elementCurrentProperties.Where(x => x.Key is PropertyInfo).Select(x => x.Key as PropertyInfo).ToList();

                var elementProperties = new Dictionary<string, object>();
                foreach (var property in elementDependencyProperties)
                {
                    //################DODANE ŻEBY POPRAWIĆ SUBSKRYPCJE ZMIENNYCH (viewVars)
                    //var elementValue = element.GetValue(property);

                    //if (element is ActiveElement && property.Name.Contains("BindingVarName") && !string.IsNullOrEmpty(elementValue?.ToString()))
                    //{

                    //    if (KR2_Switchboards.Any(x => elementValue.ToString().ToUpper().Contains("CUK_VISU")))
                    //        elementValue =  elementValue.ToString().Replace("CUK_VISU", "PLC_VISU");
                    //    //else
                    //    //{
                    //    //    if (!elementValue.ToString().Contains("CUK"))
                    //    //        elementValue = "CUK_VISU_Synoptyka_" + elementValue;
                    //    //}
                    //}

                    //elementProperties.Add(property.Name, elementValue);
                    elementProperties.Add(property.Name, element.GetValue(property));
                }


                List<ConnectionModel> elementConnections = null;
                foreach (var property in elementNormalProperties)
                {
                    var propertyVal = Convert.ChangeType(property.GetValue(element), property.PropertyType) as IEnumerable<OutputNode>;
                    if (propertyVal != null)
                    {
                        elementConnections = new List<ConnectionModel>();
                        foreach (var output in propertyVal)
                        {
                            if (output.OutputLine != null)
                            {
                                var outputLineCurrentProperties = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(output.OutputLine, ElementPropertiesModels.GetPropertiesByElementType(output.OutputLine));
                                var outputLineDependencyProperties = outputLineCurrentProperties.Where(x => x.Key is DependencyProperty).Select(x => x.Key as DependencyProperty).ToList();
                                var outputLineProperties = new Dictionary<string, object>();
                                foreach (var outputLineDependencyProperty in outputLineDependencyProperties)
                                {
                                    outputLineProperties.Add(outputLineDependencyProperty.Name, output.OutputLine.GetValue(outputLineDependencyProperty));
                                }

                                var elementConnection = new ConnectionModel()
                                {
                                    OutputNode1_CenterPoint = output.OutputLine.OutputNode1.CenterPoint,
                                    OutputNode2_CenterPoint = output.OutputLine.OutputNode2.CenterPoint,
                                    SchemeLineProperties = outputLineProperties,
                                };
                                elementConnections.Add(elementConnection);
                            }
                        }
                    }
                }

                var serializableObject = new SerializableObject() { ControlType = element.GetType(), Properties = elementProperties, Connections = elementConnections, ElementOutputs = null };
                toSerialize.Add(serializableObject);
            }
            return toSerialize;
        }

        /// <summary>
        /// Przygotowuje elementy klasy Source do serializacji. Osobna metoda bo zawiera MultiOutputSource
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        private static List<SerializableObject> PrepareSourcesToSerialize(List<Source> configuration)
        {
            List<SerializableObject> toSerialize = new List<SerializableObject>();
            foreach (var element in configuration)
            {
                var properties = ElementPropertiesModels.GetPropertiesByElementType(element);
                var elementCurrentProperties = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(element, properties);
                var elementDependencyProperties = elementCurrentProperties.Where(x => x.Key is DependencyProperty).Select(x => x.Key as DependencyProperty).ToList();

                var elementProperties = new Dictionary<string, object>();
                foreach (var property in elementDependencyProperties)
                {
                    elementProperties.Add(property.Name, element.GetValue(property));
                }

                List<ConnectionModel> elementConnections = null;
                if (element.MultiOutputNode != null)
                {
                    elementConnections = new List<ConnectionModel>();
                    foreach (var output in element.MultiOutputNode.OutputNodes)
                    {
                        if (output != null)
                        {
                            if (output.OutputLine != null)
                            {
                                var outputLineCurrentProperties = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(output.OutputLine, ElementPropertiesModels.GetPropertiesByElementType(output.OutputLine));
                                var outputLineDependencyProperties = outputLineCurrentProperties.Where(x => x.Key is DependencyProperty).Select(x => x.Key as DependencyProperty).ToList();
                                var outputLineProperties = new Dictionary<string, object>();
                                foreach (var outputLineDependencyProperty in outputLineDependencyProperties)
                                {
                                    outputLineProperties.Add(outputLineDependencyProperty.Name, output.OutputLine.GetValue(outputLineDependencyProperty));
                                }

                                var elementConnection = new ConnectionModel()
                                {
                                    OutputNode1_CenterPoint = PointExt.ComparePoints(output.CenterPoint, output.OutputLine.OutputNode1.CenterPoint) ? output.OutputLine.OutputNode2.CenterPoint : output.OutputLine.OutputNode1.CenterPoint,
                                    SchemeLineProperties = outputLineProperties,
                                };
                                elementConnections.Add(elementConnection);
                            }
                        }
                    }
                }

                var serializableObject = new SerializableObject() { ControlType = element.GetType(), Properties = elementProperties, Connections = elementConnections, ElementOutputs = null };
                toSerialize.Add(serializableObject);
            }
            return toSerialize;
        }

        /// <summary>
        /// Przygotowuje elementy schematu klasy CommonControlBase (wspólne kontrolki) do serializacji
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        private static List<SerializableCommonObject> PrepareCommonControlBaseToSerialize(List<CommonControlBase> configuration)
        {
            List<SerializableCommonObject> toSerialize = new List<SerializableCommonObject>();
            foreach (var element in configuration)
            {
                var properties = ElementPropertiesModels.GetPropertiesByElementType(element);
                var elementCurrentProperties = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(element, properties, true);
                var elementDependencyProperties = elementCurrentProperties.Where(x => x.Key is DependencyProperty).Select(x => x.Key as DependencyProperty).ToList();
                var elementNormalProperties = elementCurrentProperties.Where(x => x.Key is PropertyInfo).Select(x => x.Key as PropertyInfo).ToList();

                var serializableObject = GetSerializeCommonObject(element, elementDependencyProperties);
                toSerialize.Add(serializableObject);
            }
            return toSerialize;
        }

        /// <summary>
        /// Przygotowuje kontrolki sieci oraz przeróbki do serializacji
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        private static List<SerializableObject> PrepareNetworkControlsToSerialize(List<IControlWithOutputs> configuration)
        {
            List<SerializableObject> toSerialize = new List<SerializableObject>();
            foreach (var element in configuration)
            {
                if (element is UIElement uIElement)
                {
                    List<ConnectionModel> elementConnections = null;

                    var properties = ElementPropertiesModels.GetPropertiesByElementType(uIElement, true);
                    var elementCurrentProperties = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(element, properties, true);
                    var elementDependencyProperties = elementCurrentProperties.Where(x => x.Key is DependencyProperty).Select(x => x.Key as DependencyProperty).ToList();
                    var elementNormalProperties = elementCurrentProperties.Where(x => x.Key is PropertyInfo).Select(x => x.Key as PropertyInfo).ToList();

                    var serializableObject = GetSerializeObject(uIElement, elementDependencyProperties);

                    foreach (var property in elementNormalProperties)
                    {
                        var propertyVal = Convert.ChangeType(property.GetValue(element), property.PropertyType) as IEnumerable<IOutputWithLines>;
                        if (propertyVal != null)
                        {
                            elementConnections = new List<ConnectionModel>();
                            foreach (var output in propertyVal)
                            {
                                if (output.ConnectedLines != null)
                                {
                                    foreach (var line in output.ConnectedLines)
                                    {
                                        if (line is UIElement lineElement)
                                        {
                                            var outputLineCurrentProperties = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(lineElement, ElementPropertiesModels.GetPropertiesByElementType(lineElement));
                                            var outputLineDependencyProperties = outputLineCurrentProperties.Where(x => x.Key is DependencyProperty).Select(x => x.Key as DependencyProperty).ToList();
                                            var outputLineProperties = new Dictionary<string, object>();
                                            foreach (var outputLineDependencyProperty in outputLineDependencyProperties)
                                            {
                                                outputLineProperties.Add(outputLineDependencyProperty.Name, lineElement.GetValue(outputLineDependencyProperty));
                                            }

                                            var elementConnection = new ConnectionModel()
                                            {
                                                OutputNode1_CenterPoint = line.OutputNode1.CenterPoint,
                                                OutputNode2_CenterPoint = line.OutputNode2.CenterPoint,
                                                SchemeLineProperties = outputLineProperties,
                                            };
                                            elementConnections.Add(elementConnection);
                                        }
                                    }
                                }
                            }
                            serializableObject.Connections = elementConnections;
                        }
                    }

                    toSerialize.Add(serializableObject);
                }
            }

            return toSerialize;
        }

        private static OutputNodeModel GetOutputNodeModelInstance(IOutputNode outputNode)
        {
            var outputNode_Properties = new Dictionary<string, object>();
            if (outputNode is FrameworkElement outputNodeFrameworkElement)
            {
                var outputNode_CurrentProperties = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(outputNodeFrameworkElement, ElementPropertiesModels.GetPropertiesByElementType(outputNodeFrameworkElement));
                var outputNode_DependencyProperties = outputNode_CurrentProperties.Where(x => x.Key is DependencyProperty).Select(x => x.Key as DependencyProperty).ToList();
                foreach (var outputNode_DependencyProperty in outputNode_DependencyProperties)
                {
                    outputNode_Properties.Add(outputNode_DependencyProperty.Name, outputNodeFrameworkElement.GetValue(outputNode_DependencyProperty));
                }
            }
            return new OutputNodeModel() { CenterPoint = outputNode.CenterPoint, Properties = outputNode_Properties };
        }
        
        #region SerializeObject
        private static SerializableObject GetSerializeObject(UIElement uIElement, List<DependencyProperty> elementDependencyProperties)
        {
            var elementProperties = new Dictionary<string, object>();
            List<OutputNodeModel> outputNodeModels = null;
            foreach (var property in elementDependencyProperties)
            {
                if (property.PropertyType == typeof(FontWeight))
                    elementProperties.Add(property.Name, ((FontWeight)uIElement.GetValue(property)).ToOpenTypeWeight());
                else
                {
                    elementProperties.Add(property.Name, uIElement.GetValue(property));
                    var a = uIElement.GetValue(property);
                }
            }

            if(uIElement is EditableControlBase editableControlBase)
            {
                outputNodeModels = new List<OutputNodeModel>();
                foreach (var outuptNode in editableControlBase.OutputNodes)
                {
                    outputNodeModels.Add(GetOutputNodeModelInstance(outuptNode));
                }
            }

            return new SerializableObject() { ControlType = uIElement.GetType(), Properties = elementProperties, Connections = null, ElementOutputs = outputNodeModels };
        }

        private static SerializableCommonObject GetSerializeCommonObject(UIElement uIElement, List<DependencyProperty> elementDependencyProperties)
        {
            var elementProperties = new Dictionary<string, object>();
            foreach (var property in elementDependencyProperties)
            {
                if (property.PropertyType == typeof(FontWeight))
                    elementProperties.Add(property.Name, ((FontWeight)uIElement.GetValue(property)).ToOpenTypeWeight());
                else
                {
                    elementProperties.Add(property.Name, uIElement.GetValue(property));
                }
            }

            return new SerializableCommonObject() { ControlType = uIElement.GetType(), Properties = elementProperties };
        }
        #endregion SerializeObject
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="field"></param>
        /// <param name="property"></param>
        /// <param name="newElement"></param>
        /// <returns></returns>
        public static int SetElementProperty(DependencyProperty field, KeyValuePair<string, object> property, FrameworkElement newElement, int newElements)
        {
            int result = newElements;

           
            if (field != null)
            {
                var typeConverter = field.PropertyType.CustomAttributes.Where(x => x.AttributeType == typeof(TypeConverterAttribute))?.FirstOrDefault()?.ConstructorArguments?.FirstOrDefault().Value as Type;

                if (field.PropertyType.IsEnum)
                    newElement.SetValue(field, Enum.Parse(field.PropertyType, property.Value.ToString()));
                else if (property.Key.ToString().ToUpper() == "NAME" && string.IsNullOrEmpty(property.Value?.ToString()))
                {
                    newElement.SetValue(field, Convert.ChangeType("NoName" + newElements, field.PropertyType));
                    newElements++;
                }
                else if (field.PropertyType == typeof(Thickness))
                    newElement.SetValue(field, ThicknessConv.FromString(((string)property.Value).Replace(".", ",")));
                else if (field.PropertyType == typeof(CornerRadius))
                    newElement.SetValue(field, CornerRadiusConv.FromString(((string)property.Value).Replace(".", ",")));
                else if (field.PropertyType == typeof(FontWeight) && int.TryParse(property.Value?.ToString(), out int intFontWeight))
                    newElement.SetValue(field, FontWeight.FromOpenTypeWeight(intFontWeight));
                else if(typeConverter != null && property.Value != null)
                {
                    var typeConverterInstance = Activator.CreateInstance(typeConverter);
                    if (typeConverterInstance is TypeConverter typeConverterI)                   
                        newElement.SetValue(field, typeConverterI.ConvertFrom(property.Value));
                }
                else
                    newElement.SetValue(field, Convert.ChangeType(property.Value, field.PropertyType));
            }

            return result;
        }

        #region do usunięcia
        private static List<OutputNodeModel> PrepareOutpuyNodesToSerialize(List<IOutputNode> outputNodes)
        {
            var result = new List<OutputNodeModel>();

            foreach (var outputNode in outputNodes)
            {
                var outpuyNodeModelInstance = GetOutputNodeModelInstance(outputNode);
                if (outpuyNodeModelInstance != null)
                    result.Add(GetOutputNodeModelInstance(outputNode));
            }
            return result;
        }
        #endregion do usunięcia

    }
}

