﻿using SchemeControlsLib.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Services.Mapping
{
    public sealed class LoadingProgress : ObservableObject
    {
        private static readonly LoadingProgress instance = new LoadingProgress();
        private LoadingProgress() { }

        public Window LoadingWindow { get; set; }

        public static LoadingProgress Instance
        {
            get
            {
                return instance;
            }
        }

        private string _LoadingProgersString { get; set; }
        public string LoadingProgersString
        {
            get { return _LoadingProgersString; }
            set
            {
                _LoadingProgersString = value;
                NotifyPropertyChanged("LoadingProgersString");
            }
        }

        public void CloseLoadingWindow()
        {
            if(LoadingWindow != null)
            {
                LoadingWindow.Close();
                LoadingWindow = null;
            }
        }
    }
}
