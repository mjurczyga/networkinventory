﻿using Newtonsoft.Json;
using SchemeControlsLib.Controls;
using SchemeControlsLib.Controls.CoalProcessing;
using SchemeControlsLib.Controls.CoalProcessing.Interfaces;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.Controls.ElectricControls;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.NetworkControls;
using SchemeControlsLib.Controls.NetworkControls.Interfaces;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.Models;
using SchemeControlsLib.Services.Mapping;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace SchemeControlsLib.Services.Deserializers
{
    public static class V2000_Deserialize
    {
        /// <summary>
        /// Metoda wykonuje deserializację pliku z elementami biblioteki. Po deserializacji elementy zostają dodane do schematu. 
        /// </summary>
        /// <param name="fileName">Nazwa pliku z elementami</param>
        /// <param name="schemeCanvas">Canvas do którego elementy mają być odane</param>
        /// <param name="elementMouseDownAction">akcja na kliknięcie w element lewym przyciskiem myszy</param>
        /// <param name="elementContextMenu">akcja na kliknięcie w element prawym przyciskiem myszy</param>
        /// <param name="lineContextMenu">akcja na kliknięcie w polączenie (linie) prawym przyciskiem myszy</param>
        public static void DeserializeSchemeComponents(string fileName, string jsonObject, SchemeCanvas schemeCanvas, MouseButtonEventHandler elementMouseDownAction = null, ContextMenu elementContextMenu = null, string fileAllText = null)
        {
            var elements = DeserializeSchemeComponents(fileName, jsonObject);
            SetLoadedEventParameters(schemeCanvas, elementContextMenu, elements);
            if (schemeCanvas.IsEditMode)
                schemeCanvas.Children.Clear();

            // dodanie elementów do schematu
            int loadedElements = 0;
            int elementsQuantity = elements.Count;
            foreach (var element in elements)
            {
                schemeCanvas.AddSchemeElement((ObservableControl)element.Key, false);
                element.Key.Loaded += V2000_Deserialize.ElementLoaded;
                _dispatcher.Invoke(() =>
                {
                    LoadingProgress.Instance.LoadingProgersString = $"Załadowano {loadedElements} z {elementsQuantity} kontrolek.";
                }, DispatcherPriority.Render);
                loadedElements++;
            }

            //DeserializeElectricSchemeElements(elements, schemeCanvas, elementContextMenu);

            //DeserializeNetworkSchemeConnections(elements, schemeCanvas, elementContextMenu);
        }

        #region Deserialize private methods
        /// <summary>
        /// Funkcja która tworzy elementy z deserializowanego pliku dla wersji 2.0.0.0 i starszej.
        /// </summary>
        /// <param name="fileName">nazwa pliku</param>
        /// <returns>Słownik: element schematu; połączenia do tego elementu schematu</returns>
        private static Dictionary<FrameworkElement, List<V2000_ConnectionModel>> DeserializeSchemeComponents(string fileName, string fileAllText = null)
        {
            Dictionary<FrameworkElement, List<V2000_ConnectionModel>> result = new Dictionary<FrameworkElement, List<V2000_ConnectionModel>>();

            #region Tłumaczenie starych schematów na nowe (zamiana właściowści Canvas na wrapper) 
            if (string.IsNullOrEmpty(fileAllText))
            {
                fileAllText = File.ReadAllText(fileName);
            }
            #region wersje z canvas Left i Top
            if (!fileAllText.Contains("EditorVersion") && fileAllText.Contains("\"Left\""))
            {
                var currentFileFolder = fileName.Substring(0, fileName.LastIndexOf("\\"));
                var schemeName = fileName.Replace(currentFileFolder + "\\", "");

                var newDirectory = Path.Combine(currentFileFolder, DateTime.Now.ToString("yyyy_MM_dd") + "_BackuspSchemes");

                if (!Directory.Exists(newDirectory))
                    Directory.CreateDirectory(newDirectory);


                var newFilePath = Path.Combine(currentFileFolder, DateTime.Now.ToString("yyyy_MM_dd") + "_BackuspSchemes", schemeName);
                if (File.Exists(newFilePath))
                    newFilePath = newFilePath + DateTime.Now.ToString("yyyy_MM_dd__H_mm_ss");

                File.Copy(fileName, newFilePath);


                fileAllText = fileAllText.Replace("Left", "X_Position");
                fileAllText = fileAllText.Replace("Top", "Y_Position");
                File.WriteAllText(fileName, fileAllText);

                var tempRead = SchemeSerializationService.Deserialize<List<V2000_SerializableObject>>(fileName);

                var fileModel = new OLD_SchemeFileModel(Assembly.GetAssembly(typeof(V2000_SerializableObject)).GetName().Version.ToString(), 1, tempRead);
                SchemeSerializationService.Serialize<OLD_SchemeFileModel>(fileModel, fileName);
                //using (StreamWriter streamWriter = new StreamWriter(fileName, false))
                //using (JsonWriter jsonWriter = new JsonTextWriter(streamWriter))
                //{
                //    _jsonSerializer.Serialize(jsonWriter, fileModel);
                //}
            }
            #endregion wersje z canvas Left i Top


            #endregion Tłumaczenie starych schematów na nowe (zamiana właściowści Canvas na wrapper) 
            // czytanie wersji 2.0.0.0
            var readed = SchemeSerializationService.DeserializeText<OLD_SchemeFileModel>(fileAllText);
            var readedElements = readed?.SchemeElements;
            int i = 0;
            if (readedElements != null)
            {
                foreach (var element in readedElements)
                {
                    //try
                    //{
                    FrameworkElement newElement = null;
                    newElement = Activator.CreateInstance(element.ControlType) as Control;
                    if (newElement is IControlLoadedEvent controlLoadedEvent)
                        controlLoadedEvent.AddLoadedEvent();

                    var fields = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(newElement, ElementPropertiesModels.GetPropertiesByElementType(newElement)).Select(x => x.Key as DependencyProperty).ToList();

                    // ustawienie włąsciwosci
                    foreach (var property in element.Properties)
                    {
                        var field = fields.Find(x => x.Name == property.Key);
                        try
                        {
                            i = SchemeSerializationService.SetElementProperty(field, property, newElement, i);
                        }
                        catch (Exception e)
                        {
                            File.AppendAllText("Log11.txt", $"Błąd przy ładowaniu elementu schematu: { element.ControlType.ToString()}, {e.Message} -- {field.Name } -- {property.Key} -- {property.Value }\r\n");

                            Console.WriteLine($"Błąd przy ładowaniu elementu schematu: {element.ControlType.ToString()}");
                        }
                    }

                    result.Add(newElement, element.Connections);
                }
            }
            return result;
        }
        #endregion Deserialize private methods

        public static void SetLoadedEventParameters(SchemeCanvas schemeCanvas, ContextMenu elementContextMenu, Dictionary<FrameworkElement, List<V2000_ConnectionModel>> elements)
        {
            _schemeCanvas = schemeCanvas;
            _elementContextMenu = elementContextMenu;
            _elements = elements;
            _LoadedCounter = 0;
        }

        private static ContextMenu _elementContextMenu;
        private static Dictionary<FrameworkElement, List<V2000_ConnectionModel>> _elements;
        private static Dispatcher _dispatcher = Dispatcher.CurrentDispatcher; //Application.Current.Dispatcher;
        private static SchemeCanvas _schemeCanvas;
        private static int _LoadedCounter = 0;
        public static void ElementLoaded(object sender, RoutedEventArgs e)
        {
            int controlsQuantity = _elements.Count;
            if (sender is FrameworkElement frameworkElement)
            {
                frameworkElement.Loaded -= ElementLoaded;
                _LoadedCounter++;
                //odświeżenie widoku kontrolki po załadowaniu (dodanie labela)
                if (frameworkElement is ILabeledControl labeledControl)
                {
                    var tmpLabel = labeledControl.LabelText;
                    labeledControl.LabelText = null;
                    labeledControl.LabelText = tmpLabel;
                }

                // po załadowaniu wszystkich kontrolek wykonaj połączenia
                if (_LoadedCounter == controlsQuantity)
                {
                    _dispatcher.Invoke(() =>
                    {
                        LoadingProgress.Instance.LoadingProgersString = "Trwa ładowanie połączeń między kontrolkami";
                    }, DispatcherPriority.Render);
                    DeserializeElectricSchemeElements(_elements, _schemeCanvas, _elementContextMenu);
                    DeserializeNetworkSchemeConnections(_elements, _schemeCanvas, _elementContextMenu);
                    //LoadingProgress.Instance.CloseLoadingWindow();
                }
            }
        }

        #region OutputNode - schematy elektryczne
        private static void DeserializeElectricSchemeElements(Dictionary<FrameworkElement, List<V2000_ConnectionModel>> elements, SchemeCanvas schemeCanvas, ContextMenu elementContextMenu = null)
        {
            var sources = elements.Where(x => x.Key.GetType() == typeof(Source)).ToDictionary(x => x.Key as Source, x => x.Value);
            var otherElements = elements.Where(x => x.Key.GetType() != typeof(Source)).ToDictionary(x => x.Key, x => x.Value);

            // dodanie reszty połączeń między kontrolkami
            var canvasOutputs = schemeCanvas?.Children?.OfType<OutputNode>();

            foreach (var element in otherElements)
            {
                if (element.Value != null)
                {
                    foreach (var connectionPop in element.Value)
                    {

                        // znalezienie wyjść (po punktach)
                        var output1 = canvasOutputs.Where(x => PointExt.ComparePoints(x.CenterPoint, connectionPop.OutputNode1_CenterPoint)).FirstOrDefault();
                        if (output1?.ControlParent is LineConnector && output1.OutputLine != null)
                            output1 = ((LineConnector)output1.ControlParent).GetOpposedOutputNode(output1);


                        var output2 = canvasOutputs.Where(x => PointExt.ComparePoints(x.CenterPoint, connectionPop.OutputNode2_CenterPoint)).FirstOrDefault();
                        if (output2?.ControlParent is LineConnector && output2.OutputLine != null)
                            output2 = ((LineConnector)output2.ControlParent).GetOpposedOutputNode(output2);


                        if (output1 != null && output1.OutputLine == null && output2 != null && output2.OutputLine == null)
                        {
                            // tworzenie nowej linii
                            var schemeLine = new SchemeLine() { Stroke = Settings.DefaultLineColor, StrokeThickness = Settings.LinesStrokeThickness };
                            var fields = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(schemeLine, ElementPropertiesModels.GetPropertiesByElementType(schemeLine)).Select(x => x.Key as DependencyProperty).ToList();
                            foreach (var schemeLineProperty in connectionPop.SchemeLineProperties)
                            {
                                var field = fields.Find(x => x.Name == schemeLineProperty.Key);
                                if (field != null)
                                {
                                    schemeLine.SetValue(field, Convert.ChangeType(schemeLineProperty.Value, field.PropertyType));
                                }
                            }
                            // wykonanie połączenia na lini i dodanie jej do schematu
                            schemeLine.Connect(output1, output2);
                            if (elementContextMenu != null)
                                schemeLine.ContextMenu = elementContextMenu;
                            schemeCanvas.Children.Add(schemeLine);
                        }
                    }
                }
            }

            // najpierw dodaje połączenia ze źródła źródła
            foreach (var source in sources)
            {
                foreach (var connectionPop in source.Value)
                {
                    // przy serializacji zapisane jest tylko jedno źródło (docelowe) bo drugie dodaje samo źródło w metodzie Connect
                    var output1 = canvasOutputs.Where(x => PointExt.ComparePoints(x.CenterPoint, connectionPop.OutputNode1_CenterPoint)).FirstOrDefault();
                    if (output1 != null && output1.OutputLine == null)
                    {
                        // Wykonanie połączenia
                        source.Key.Connect(output1);
                        // nie trzeba tworzyć linii bo źródło samo ją tworzy, do ustawienie właściowści brana jest linia z wyjsci docelowego (nie ze źródła!)
                        var schemeLine = output1.OutputLine;

                        // ustawienie właściwosci na dodanej lini
                        var fields = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(schemeLine, ElementPropertiesModels.GetPropertiesByElementType(schemeLine)).Select(x => x.Key as DependencyProperty).ToList();
                        foreach (var schemeLineProperty in connectionPop.SchemeLineProperties)
                        {
                            var field = fields.Find(x => x.Name == schemeLineProperty.Key);
                            if (field != null)
                            {
                                schemeLine.SetValue(field, Convert.ChangeType(schemeLineProperty.Value, field.PropertyType));
                            }
                        }
                    }
                }
            }
        }
        #endregion OutputNode - schematy elektryczne

        #region OutputNode - schematy sieci
        private static void DeserializeNetworkSchemeConnections(Dictionary<FrameworkElement, List<V2000_ConnectionModel>> elements, SchemeCanvas schemeCanvas, ContextMenu elementContextMenu = null)
        {
            var networkControls = elements.Where(x => x.Key.GetType() != typeof(Source)).ToDictionary(x => x.Key, x => x.Value);

            // dodanie reszty połączeń między kontrolkami
            var editableNetworkControls = schemeCanvas?.Children?.OfType<EditableControlBase>(); // NetworkControlBase

            foreach (var element in networkControls)
            {
                if (element.Value != null)
                {
                    foreach (var connectionPop in element.Value)
                    {
                        // znalezienie kontrolek (po punktach wyjść)
                        var networkControl1 = editableNetworkControls.Where(
                            x => x.OutputBounds.IsInBounds(connectionPop.OutputNode1_CenterPoint)
                            || x.OutputNodes.Any(y => y.CenterPoint == connectionPop.OutputNode1_CenterPoint)).FirstOrDefault();
                        var networkControl2 = editableNetworkControls.Where(
                            x => x.OutputBounds.IsInBounds(connectionPop.OutputNode2_CenterPoint)
                            || x.OutputNodes.Any(y => y.CenterPoint == connectionPop.OutputNode2_CenterPoint)).FirstOrDefault();

                        if (networkControl1 != null && networkControl2 != null)
                        {
                            var networkControlOutput1 = networkControl1.OutputNodes.Where(x => PointExt.ComparePoints(x.CenterPoint, connectionPop.OutputNode1_CenterPoint)).FirstOrDefault();
                            if (networkControlOutput1 == null)
                                networkControlOutput1 = networkControl1.AddOutput(connectionPop.OutputNode1_CenterPoint.X, connectionPop.OutputNode1_CenterPoint.Y);

                            var networkControlOutput2 = networkControl2.OutputNodes.Where(x => PointExt.ComparePoints(x.CenterPoint, connectionPop.OutputNode2_CenterPoint)).FirstOrDefault();
                            if (networkControlOutput2 == null)
                                networkControlOutput2 = networkControl2.AddOutput(connectionPop.OutputNode2_CenterPoint.X, connectionPop.OutputNode2_CenterPoint.Y);

                            // tworzenie nowej linii (sprawdzenie czy już czasem nie połączono)
                            if (networkControlOutput1 != null && networkControlOutput2 != null && !networkControlOutput1.ConnectedLines.Any(x => networkControlOutput2.ConnectedLines.Contains(x)))
                            {
                                FrameworkElement schemeLine = null;
                                if (networkControlOutput1 is INetworkElement && networkControlOutput2 is INetworkElement)
                                    schemeLine = new NetworkSchemeLine();
                                else if (networkControlOutput1 is ICoalProcessingElement && networkControlOutput2 is ICoalProcessingElement)
                                    schemeLine = new CoalProcessingLine();

                                if (schemeLine != null)
                                {
                                    var fields = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(schemeLine, ElementPropertiesModels.GetPropertiesByElementType(schemeLine)).Select(x => x.Key as DependencyProperty).ToList();

                                    if (schemeLine is ILine line)
                                        line.Connect(networkControlOutput1, networkControlOutput2);

                                    foreach (var schemeLineProperty in connectionPop.SchemeLineProperties)
                                    {
                                        var field = fields.Find(x => x.Name == schemeLineProperty.Key);
                                        SchemeSerializationService.SetElementProperty(field, schemeLineProperty, schemeLine as FrameworkElement, 0);
                                    }
                                    // wykonanie połączenia na lini i dodanie jej do schematu
                                    if (elementContextMenu != null)
                                        schemeLine.ContextMenu = elementContextMenu;
                                    schemeCanvas.Children.Add(schemeLine);
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion OutputNode - schematy sieci
    }
}
