﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Services.PLC
{
    public static class ValuesComparer
    {
        public static double WartoscZDupy = -9999.999;

        public static string ProcessObjValue(object objValue, string stringFormat = null) //ZMIENIONO  "00.0"
        {
            if(double.TryParse(objValue?.ToString()?.Replace('.',','), out double dblValue))
            {
                if (dblValue == WartoscZDupy)
                    return "------";
                else
                    return dblValue.ToString(stringFormat);
            }

            return objValue?.ToString();
        }
    }
}
