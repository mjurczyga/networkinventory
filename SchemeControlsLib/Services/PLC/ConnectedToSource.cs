﻿using SchemeControlsLib.Controls.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Services.PLC
{
    public class ConnectedToSource
    {
        public string SourceName { get; set; }
        public bool IsConnectedToSource { get; set; }

        public ISource Source { get; set; }

        public ConnectedToSource()
        {

        }

        public ConnectedToSource(ISource source)
        {
            Source = source;
            SourceName = source.Name;
            IsConnectedToSource = true;
        }

        public ConnectedToSource(bool isConnectedToSource, string sourceName)
        {
            SourceName = sourceName;
            IsConnectedToSource = isConnectedToSource;
        }
    }
}
