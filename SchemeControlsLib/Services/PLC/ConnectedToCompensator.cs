﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Services.PLC
{
    public class ConnectedToCompensator
    {
        public bool IsConnectedToCompensator { get; set; }
        public string CompensatorName { get; set; }
        public string ConnectorName { get; set; }

        public ConnectedToCompensator() { }

        public ConnectedToCompensator(bool isConnectedToCompensator, string compensatorName, string connectorName)
        {
            IsConnectedToCompensator = isConnectedToCompensator;
            CompensatorName = compensatorName;
            ConnectorName = connectorName;
        }


    }
}
