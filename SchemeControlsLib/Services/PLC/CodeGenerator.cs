﻿using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.ElectricControls;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.Interfaces.ElementsInterfaces;
using SchemeControlsLib.Controls.LibControls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Services.PLC
{
    public class CodeGenerator
    {
        private SchemeCanvas _schemeCanvas;

        public void GeneratePLCCode(SchemeCanvas schemeCanvas, string filePath)
        {
            _schemeCanvas = schemeCanvas;
            var activeElements = schemeCanvas.Children.OfType<ActiveElement>();

            var switchesDict = this.PrepareActiveElementsDefinitions(activeElements, filePath);
            var xD_Islands = this.CreateIsland(activeElements, filePath);
            this.TranslateConnections(xD_Islands, switchesDict);

            this.SaveConnectionsFile(xD_Islands, filePath);
        }


        /// <summary>
        /// Robi pliki definicji zmiennych (dla wizu) i przypisanie stanów łącznika tj. Lacznik[XXX].Stan := ZMIENNA;
        /// </summary>
        /// <param name="activeElements"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private Dictionary<int, SwitchModel> PrepareActiveElementsDefinitions(IEnumerable<ActiveElement> activeElements, string filePath)
        {
            Dictionary<int, SwitchModel> switchesDict = new Dictionary<int, SwitchModel>();
            var counter = 1;
            foreach (var element in activeElements)
            {
                if (element is ActiveElement activeElement)
                {
                    if (activeElement is ITrolley trolley)
                    {
                        if (!string.IsNullOrEmpty(trolley.TrolleyStateBindingVarName))
                        {
                            switchesDict.Add(counter + 1, new SwitchModel(trolley.TrolleyStateBindingVarName, counter));
                            counter++;
                        }
                    }

                    if (activeElement is ILacznik lacznik)
                    {
                        if (!string.IsNullOrEmpty(lacznik.SwitchStateBindingVarName))
                        {
                            switchesDict.Add(counter + 1, new SwitchModel(lacznik.SwitchStateBindingVarName, counter));
                            counter++;
                        }
                    }
                }
            }

            int startAddress = 3000;
            using (StreamWriter file = new StreamWriter(filePath + @"\\PLC_StateDefinitions.txt"))
            using (StreamWriter fileDef = new StreamWriter(filePath + @"\\PLC_Definitions.txt"))
            {
                file.WriteLine("(* -----ŁĄCZNIKI PRZYPISANIE STANÓW ----- *)");
                file.WriteLine($"(* ILOŚĆ ELEMENTÓW: {switchesDict.Count()} *)");

                fileDef.WriteLine("(* -----DEFINICJE ZMIENNYCH ----- *)");
                fileDef.WriteLine($"(* ILOŚĆ ELEMENTÓW: {switchesDict.Count()} *)");

                foreach (var switchElement in switchesDict)
                {
                    file.WriteLine($"{switchElement.Value.PLCTableName}.Stan := {switchElement.Value.SwitchCleanedName};");
                    fileDef.WriteLine($"{switchElement.Value.SwitchCleanedName} : INT; (* {switchElement.Value.PLCTableName} *)"); //AT%MB{startAddress}
                    startAddress += 2;
                }
            }

            return switchesDict;
        }

        private void SaveConnectionsFile(List<XD_Island> xD_Islands, string filePath)
        {
            using (StreamWriter file = new StreamWriter(filePath + @"\\PLC_Connections.txt"))
            {
                file.WriteLine("(* -----POŁĄCZANIA KONTROLEK ----- *)");
                file.WriteLine($"(* ILOŚĆ WYSP: {xD_Islands.Count()} *)");

                var sortedIslands = xD_Islands.GroupBy(x => x.ConectedActiveElements.Count()).OrderByDescending(x => x.Key).FirstOrDefault();
                var topIslands = new KeyValuePair<int, List<XD_Island>>(sortedIslands.Key, sortedIslands.ToList());
                if (topIslands.Value != null)
                {
                    if (topIslands.Value.Count() == 1) 
                        file.WriteLine($"(* WYSPA Z NAJWIĘKSZA ILOSCIĄ PRZYŁĄCZY: {topIslands.Value[0].PLC_IslandName} - {topIslands.Key} *)");
                    else
                    {
                        file.WriteLine($"(* WYSPAY Z NAJWIĘKSZA ILOSCIĄ PRZYŁĄCZY: *)");
                        foreach (var island in topIslands.Value)
                        {
                            file.WriteLine($"(* {island.PLC_IslandName} - {island.ConectedActiveElements.Count} *)");
                        }
                    }
                }

                var sourceIslands = xD_Islands.Where(x => x.ConnectedToSource != null && x.ConnectedToSource.IsConnectedToSource);
                if (sourceIslands != null && sourceIslands.Count() != 0)
                {
                    file.WriteLine($"(* ----------ŹRÓDŁA---------- *)");
                    foreach (var sourceIsland in sourceIslands)
                    {
                        file.WriteLine($"(* {sourceIsland.PLC_IslandName} - {sourceIsland.ConnectedToSource.SourceName} *)");
                    }
                }

                var compensatorsIslands = xD_Islands.Where(x => x.ConnectedToCompensator != null && x.ConnectedToCompensator.IsConnectedToCompensator);
                if (compensatorsIslands != null && compensatorsIslands.Count() != 0)
                {
                    file.WriteLine($"(* ---------- KOMPENSATORY ---------- *)");
                    foreach (var compensatorsIsland in compensatorsIslands)
                    {
                        file.WriteLine($"(* {compensatorsIsland.PLC_IslandName} - {compensatorsIsland.ConnectedToCompensator.CompensatorName} *)");
                    }
                }

                foreach (var xD_Island in xD_Islands)
                {
                    var list = xD_Island.GetPLCConnectionsList();
                    foreach (var conenction in list)
                    {
                        file.WriteLine(conenction);
                    }
                }
            }
        }

        /// <summary>
        /// Robi wyspy łaczników
        /// </summary>
        private List<XD_Island> CreateIsland(IEnumerable<ActiveElement> activeElements, string filePath)
        {
            var XD_Islands = new List<XD_Island>();
            int XD_IslandsIndex = 1;

            foreach(var activeElement in activeElements)
            {
                foreach(var outputNode in activeElement.OutputNodesCollection)
                {
                    var connections = CheckOutputNodeToActiveElementConnections(outputNode, out ConnectedToSource connectedToSource, out ConnectedToCompensator connectedToCompensator);
                    if (connections != null && connections.Count != 0)
                    {
                        var newIsland = new XD_Island(XD_IslandsIndex, connections);
                        newIsland.ConnectedToCompensator = connectedToCompensator;
                        newIsland.ConnectedToSource = connectedToSource;

                        XD_Islands.Add(newIsland);
                        XD_IslandsIndex++;
                    }
                }

                // połącznie wozków lacznik wyjscie 1 z wozkiem wyjcie 0
                if(activeElement is ILacznik lacznik && activeElement is ITrolley trolley)
                {
                    List<ElementNameWithOutputIndex> lacznikWozkowy = new List<ElementNameWithOutputIndex>();
                    lacznikWozkowy.Add(new ElementNameWithOutputIndex(lacznik.SwitchStateBindingVarName, 1));
                    lacznikWozkowy.Add(new ElementNameWithOutputIndex(trolley.TrolleyStateBindingVarName, 0));
                    XD_Islands.Add(new XD_Island(XD_IslandsIndex, lacznikWozkowy));
                    XD_IslandsIndex++;
                }
            }



            return XD_Islands;
        }

        private List<OutputNode> _CheckedOutputs = new List<OutputNode>();
        /// <summary>
        /// Sprawdza połączenia pomiędzy wyjściem z aktywnej kontrolki do wyjść/wjeść aktywnych kontrolek
        /// </summary>
        /// <param name="outputNode">wyjście aktywnej kontrolki od której należy zacząć przeszukiwanie</param>
        /// <returns>Lista połącznonych ze sobą kontolek wraz z numerem wyjścia/wejścia</returns>
        private List<ElementNameWithOutputIndex> CheckOutputNodeToActiveElementConnections(OutputNode outputNode, out ConnectedToSource connectedToSource, out ConnectedToCompensator connectedToCompensator)
        {
            List<ElementNameWithOutputIndex> elements = new List<ElementNameWithOutputIndex>();
            connectedToSource = new ConnectedToSource(false, "");
            connectedToCompensator = new ConnectedToCompensator(false, "", "");

            if (!_CheckedOutputs.Contains(outputNode) && this.TryGetNextOutputFromShemeLine(outputNode, out OutputNode nextOutputNode) && !_CheckedOutputs.Contains(nextOutputNode))
            {
                if(this.TryGetElementNameWithOutput(outputNode, out ElementNameWithOutputIndex connection))
                    elements.Add(connection);

                _CheckedOutputs.Add(outputNode);
                _CheckedOutputs.Add(nextOutputNode);

                // jeśli jest aktywnym elementym
                if (nextOutputNode.ControlParent is ActiveElement activeElement)
                {
                    // łączniki wózkowe - gdy wyjście ma index 0 to stan łącznika, gdy 1 to stan wózka
                    if (this.TryGetElementNameWithOutput(nextOutputNode, out ElementNameWithOutputIndex connectionNext))
                        elements.Add(connectionNext);
                }
                else if (nextOutputNode.ControlParent is PassiveElement passiveElement)
                {

                    foreach (var passiveOutputNode in passiveElement.OutputNodesCollection)
                    {
                        if (passiveOutputNode != nextOutputNode)
                        {
                            var list = CheckOutputNodeToActiveElementConnections(passiveOutputNode, out ConnectedToSource connectedToSourceNext, out ConnectedToCompensator connectedToCompensatorNext);
                            elements.AddRange(list);
                            connectedToSource = connectedToSourceNext;
                            connectedToCompensator = connectedToCompensatorNext;
                        }
                    }

                    if (passiveElement.IsCompensator)
                    {
                        connectedToCompensator.IsConnectedToCompensator = passiveElement.IsCompensator;
                        connectedToCompensator.CompensatorName = passiveElement.Name;
                    }
                }
                else if (nextOutputNode.ControlParent is MultiOutputNode sourceNode)
                {
                    if (sourceNode.ControlParent is ISource source)
                        connectedToSource = new ConnectedToSource(source);
                }
            }

            if (connectedToCompensator.IsConnectedToCompensator)
                connectedToCompensator.ConnectorName = elements.FirstOrDefault()?.TranslatedName;

            return elements;
        }

        /// <summary>
        /// /// "Oddaje" dugie wyjście z linni wyjścia tj. 
        ///  Paramert wejściowy> ON1--------------------ON2 <parametrWyjściowy
        /// </summary>
        /// <param name="outputNode">ON1</param>
        /// <param name="nextOutputNode">ON2</param>
        /// <returns>Czy znaleziono</returns>
        private bool TryGetNextOutputFromShemeLine(OutputNode outputNode, out OutputNode nextOutputNode)
        {
            var schemeLine = outputNode.OutputLine;
            if (schemeLine != null)
            {
                if (schemeLine.OutputNode1 != outputNode)
                {
                    nextOutputNode = schemeLine.OutputNode1 as OutputNode;
                    return true;
                }
                else if (schemeLine.OutputNode2 != outputNode)
                {
                    nextOutputNode = schemeLine.OutputNode2 as OutputNode;
                    return true;
                }
            }
            nextOutputNode = null;
            return false;
        }

        /// <summary>
        /// Tłumaczy nazwe zmiennej wizu na nazwe w tablicy laczników
        /// </summary>
        /// <param name="xD_Islands"></param>
        /// <param name="switches"></param>
        private void TranslateConnections(List<XD_Island> xD_Islands, Dictionary<int, SwitchModel> switches)
        {
            foreach(var xD_Island in xD_Islands)
            {
                foreach(var connection in xD_Island.ConectedActiveElements)
                {
                    connection.Translate(switches);
                }
            }
        }

        private bool TryGetElementNameWithOutput(OutputNode outputNode, out ElementNameWithOutputIndex elementNameWithOutputIndex)
        {
            if (outputNode.ControlParent is ActiveElement activeElement)
            {
                // łączniki wózkowe - gdy wyjście ma index 0 to stan łącznika, gdy 1 to stan wózka
                if (activeElement is ILacznik lacznik && activeElement is ITrolley trolley)
                {
                    int outputIndex = activeElement.OutputNodesCollection.IndexOf(outputNode);
                    if (outputIndex == 0)
                    {
                        elementNameWithOutputIndex = new ElementNameWithOutputIndex(lacznik.SwitchStateBindingVarName, outputIndex);
                        return true;
                    }
                    else if (outputIndex == 1)
                    {
                        elementNameWithOutputIndex = new ElementNameWithOutputIndex(trolley.TrolleyStateBindingVarName, outputIndex);
                        return true;
                    }
                }
                else if (activeElement is ILacznik aloneLacznik)
                {
                    int outputIndex = activeElement.OutputNodesCollection.IndexOf(outputNode);
                    elementNameWithOutputIndex = new ElementNameWithOutputIndex(aloneLacznik.SwitchStateBindingVarName, outputIndex);
                    return true;
                }
                else if (activeElement is ITrolley alonetrolley)
                {
                    int outputIndex = activeElement.OutputNodesCollection.IndexOf(outputNode);
                    elementNameWithOutputIndex = new ElementNameWithOutputIndex(alonetrolley.TrolleyStateBindingVarName, outputIndex);
                    return true;
                }
            }
            elementNameWithOutputIndex = null;
            return false;
        }
    }
}
