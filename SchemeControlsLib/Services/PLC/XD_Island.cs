﻿using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces.ElementsInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Services.PLC
{
    public class XD_Island
    {
        public int Index { get; set; }

        public string Comment { get; set; }

        public ConnectedToSource ConnectedToSource { get; set; }

        public ConnectedToCompensator ConnectedToCompensator { get; set; }

        public string PLC_IslandName
        {
            get
            {
                return $"Wyspa[{Index}]";
            }

        }

        public List<ElementNameWithOutputIndex> ConectedActiveElements { get; set; } = new List<ElementNameWithOutputIndex>();

        private List<ActiveElement> _ConnectedActiveElements { get; set; } = new List<ActiveElement>();

        public void AddActiveElemet(ActiveElement activeElement, int outputNodeIndex)
        {
            if (!_ConnectedActiveElements.Contains(activeElement))
            {
                _ConnectedActiveElements.Add(activeElement);
                if (activeElement is ILacznik lacznik)
                    ConectedActiveElements.Add(new ElementNameWithOutputIndex(lacznik.SwitchStateBindingVarName, outputNodeIndex));
                else if (activeElement is ITrolley trolley)
                    ConectedActiveElements.Add(new ElementNameWithOutputIndex(trolley.TrolleyStateBindingVarName, outputNodeIndex));
            }
        }

        public List<string> GetPLCConnectionsList()
        {
            List<string> result = new List<string>();
            for (int i = 0; i < ConectedActiveElements.Count; i++)
            {
                var conectedActiveElement = ConectedActiveElements[i];
                if (!string.IsNullOrEmpty(conectedActiveElement.TranslatedName))
                {
                    var comment = ConnectedToSource != null && ConnectedToSource.IsConnectedToSource ? ConnectedToSource.SourceName : null;
                    result.Add($"POLACZ(ADR(Wyspa[{Index}]), {i + 1}, ADR({conectedActiveElement.TranslatedName}), {conectedActiveElement.OutputIndex}); (* {conectedActiveElement.ActiveElementCleanedName} {comment} *) ");
                }
            }

            return result;
        }

        public XD_Island(int index)
        {
            Index = index;
        }

        public XD_Island(int index, List<ElementNameWithOutputIndex> conectedActiveElements)
        {
            Index = index;
            ConectedActiveElements = conectedActiveElements;
        }
    }
}
