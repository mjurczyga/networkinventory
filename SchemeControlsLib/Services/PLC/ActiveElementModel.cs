﻿using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces.ElementsInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Services.PLC
{
    public class SwitchModel
    {
        public int SwitchId { get; set; }
        public string SwitchFullName { get; set; }
        public string SwitchCleanedName
        {
            get
            {
                return ClearSwitchName(SwitchFullName);
            }
        }

        public string PLCTableName
        {
            get
            {
                return $"Lacznik[{SwitchId}]";
            }
        }

        public Dictionary<int, string> ConnectedControls { get; set; } = new Dictionary<int, string>();

        public bool TryGetClearSwitchName(out string cleanedName)
        {
            var retult = TryClearSwitchName(SwitchFullName, out string cleanedNameTmp);
            cleanedName = cleanedNameTmp;
            return retult;
        }

        private bool TryClearSwitchName(string name, out string cleanedName)
        {
            cleanedName = ClearSwitchName(name);
            return !string.IsNullOrEmpty(cleanedName);
        }

        private string ClearSwitchName(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                if (name.Contains("CUK_VISU_Synoptyka_"))
                    return name.Replace("CUK_VISU_Synoptyka_", "");
                else if (name.Contains("KR2_VISU_Synoptyka_"))
                    return name.Replace("KR2_VISU_Synoptyka_", "");
                else
                    return name;
            }
            else return null;
        }

        public SwitchModel(string fullName, int switchId)
        {
            SwitchFullName = fullName;
            SwitchId = switchId;
        }
    }
}
