﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Services.PLC
{
    public class ElementNameWithOutputIndex
    {
        public int OutputIndex { get; set; }
        public string ActiveElementName { get; set; }
        public string TranslatedName { get; set; } // Tłumaczenie na numer łącznika z listy
        public string ActiveElementCleanedName { get; set; } // Tłumaczenie na numer łącznika z listy

        public void Translate(Dictionary<int, SwitchModel> switches)
        {
            var lacznik = switches.Where(x => x.Value.SwitchFullName == ActiveElementName).FirstOrDefault().Value;
            if(lacznik != null)
            {
                TranslatedName = lacznik.PLCTableName;
                ActiveElementCleanedName = lacznik.SwitchCleanedName;
            }
        }

        public ElementNameWithOutputIndex(string conectedActiveElementName, int outputIndex)
        {
            ActiveElementName = conectedActiveElementName;
            OutputIndex = outputIndex + 1;
        }

        public override bool Equals(object obj)
        {
            return obj is ElementNameWithOutputIndex && this == (ElementNameWithOutputIndex)obj;
        }

        public override int GetHashCode()
        {
            return Tuple.Create(OutputIndex, ActiveElementName).GetHashCode();
        }

        public static bool operator ==(ElementNameWithOutputIndex x, ElementNameWithOutputIndex y)
        {
            if (x == null || y == null)
                return false;

            return x.OutputIndex == y.OutputIndex && x.ActiveElementName == y.ActiveElementName;
        }

        public static bool operator !=(ElementNameWithOutputIndex x, ElementNameWithOutputIndex y)
        {
            return !(x == y);
        }
    }
}
