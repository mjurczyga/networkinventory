﻿using SchemeControlsLib.SchemeControlTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Services
{
    public sealed class SchemeMapManager
    {
        private static SchemeMapManager m_oInstance = null;
        private static readonly object m_oPadLock = new object();

        public static SchemeMapManager Instance
        {
            get
            {
                lock (m_oPadLock)
                {
                    if (m_oInstance == null)
                    {
                        m_oInstance = new SchemeMapManager();
                    }
                    return m_oInstance;
                }
            }
        }

        public SourceConnection CheckConnectionToSource()
        {
            SourceConnection result = null;
            return result;
        }
    }
}
