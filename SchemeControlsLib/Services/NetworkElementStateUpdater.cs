﻿using SchemeControlsLib.Base;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.NetworkControls.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Threading;

namespace SchemeControlsLib.Services
{
    public class NetworkElementStateUpdater : ObservableObject
    {
        private Dispatcher _dispatcher = Dispatcher.CurrentDispatcher;
        private Timer _StatesRefreshTimer;
        private int _TimerInterval_ms = 0;

        private SchemeCanvas _SchemeCanvas;
        private ObservableCollection<IEthernetControl> _ethernetControls;

        private bool _IsRunning;
        public bool IsRunning
        {
            get { return _IsRunning; }
            set { _IsRunning = value; NotifyPropertyChanged("IsRunning"); }
        }

        private bool _IsStopped = true;
        public bool IsStopped
        {
            get { return _IsStopped; }
            set { _IsStopped = value; NotifyPropertyChanged("IsStopped"); }
        }

        public NetworkElementStateUpdater(SchemeCanvas schemeCanvas, int timerInterval_ms)
        {
            _SchemeCanvas = schemeCanvas;
            _TimerInterval_ms = timerInterval_ms;
            this.InitTimer();
        }

        private void InitTimer()
        {
            _StatesRefreshTimer = new Timer(_TimerInterval_ms);
            _StatesRefreshTimer.Elapsed += _StatesRefreshTimer_Elapsed;
            _StatesRefreshTimer.Enabled = true;
            _StatesRefreshTimer.AutoReset = true;
        }

        private void _StatesRefreshTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if(_SchemeCanvas != null)
            {
                _StatesRefreshTimer.Stop();
                var tasks = new List<Task>();
                foreach(var networkControl in _ethernetControls)
                {
                    tasks.Add(networkControl.RefreshState());
                }

                Task.WaitAll(tasks.ToArray(), 500);

                _StatesRefreshTimer.Start();
            }
        }

        public void Start()
        {
            _ethernetControls = new ObservableCollection<IEthernetControl>(_SchemeCanvas.Children.OfType<IEthernetControl>());

            if (_StatesRefreshTimer == null)
                this.InitTimer();

            _StatesRefreshTimer?.Start();
            IsRunning = true;
            IsStopped = false;
        }

        public void Stop()
        {
            if (_StatesRefreshTimer != null)
                _StatesRefreshTimer.Stop();

            IsRunning = false;
            IsStopped = true;
        }

        public void Clean()
        {
            if (_StatesRefreshTimer != null)
            {
                _StatesRefreshTimer.Stop();
                _StatesRefreshTimer.Elapsed -= _StatesRefreshTimer_Elapsed;

                foreach (var networkControl in _ethernetControls)
                {
                    networkControl.SetState(0);
                }

                _StatesRefreshTimer.Dispose();
                _StatesRefreshTimer = null;
            }
        }


    }
}
