﻿using System;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Media;
using SchemeControlsLib.ViewModels;
using SchemeControlsLib.Views;
using SchemeControlsLib.Services.Mapping;

namespace SchemeControlsLib.Services
{
    public static class WindowManager
    {
        public static void ShowActionExecutingWindow(Action action, string text, bool autoClose = true)
        {
            InfoWindowView loadingWindow = new InfoWindowView();
            InfoWindowViewModel infoWindowViewModel = new InfoWindowViewModel() { Text = text };
            loadingWindow.DataContext = infoWindowViewModel;
            Window window = new Window() { Content = loadingWindow, AllowsTransparency = true, Background = Brushes.Transparent, WindowStyle = WindowStyle.None, WindowState = WindowState.Maximized };

            window.Dispatcher.Invoke(delegate { window.Show(); }, System.Windows.Threading.DispatcherPriority.Render);
            LoadingProgress.Instance.LoadingWindow = window;

            try
            {
                Dispatcher.CurrentDispatcher.Invoke(action, System.Windows.Threading.DispatcherPriority.Render);
            }
            catch(Exception e)
            {
                //if (autoClose)
                    LoadingProgress.Instance.CloseLoadingWindow();
            }
            finally
            {
                //if(autoClose)
                    LoadingProgress.Instance.CloseLoadingWindow();
            }
        }

        public static Window ShowContentInNewWindow(object content, string windowTitle, Brush foreground, WindowStyle windowStyle = WindowStyle.SingleBorderWindow, WindowState windowState = WindowState.Normal)
        {
            Window window = new Window() { Content = content, WindowStyle = windowStyle, WindowState = WindowState.Normal, Title = windowTitle, Foreground = foreground, SizeToContent = SizeToContent.WidthAndHeight };
            window.Show();

            return window;
        }

        public static Window ShowContentInNewDialogWindow(object content, string windowTitle, Brush foreground, WindowStyle windowStyle = WindowStyle.SingleBorderWindow, WindowState windowState = WindowState.Normal)
        {
            Window window = new Window() { Content = content, WindowStyle = windowStyle, WindowState = WindowState.Normal, Title = windowTitle, Foreground = foreground, SizeToContent = SizeToContent.WidthAndHeight };
            window.ShowDialog();

            return window;
        }
    }
}
