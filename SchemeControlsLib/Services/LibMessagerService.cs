﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace SchemeControlsLib.Services
{
    public enum LibMessageTypes
    {
        INFO,
        WARNING,
        ALERT,
    }

    public sealed class LibMessage : INotifyPropertyChanged
    {
        private string _Message;
        public string Message
        {
            get { return _Message; }
            set { _Message = value; NotifyPropertyChanged("Message"); NotifyPropertyChanged("FullMessage"); }
        }

        private LibMessageTypes _MessageType;
        public LibMessageTypes MessageType
        {
            get { return _MessageType; }
            set { _MessageType = value; NotifyPropertyChanged("MessageType"); NotifyPropertyChanged("FullMessage"); NotifyPropertyChanged("TypeBrush"); }
        }

        private DateTime _DateTime;
        public DateTime DateTime
        {
            get { return _DateTime; }
            set { _DateTime = value; NotifyPropertyChanged("DateTime"); NotifyPropertyChanged("FullMessage"); }
        }

        public string FullMessage
        {
            get
            {
                return $"[{DateTime.ToString("yyyy-MM-dd h:mm:ss")}] {Enum.GetName(MessageType.GetType(), MessageType)}  {Message}";
            }
        }

        public Brush TypeBrush
        {
            get
            {
                Brush brushResult = null;

                switch (MessageType)
                {
                    case LibMessageTypes.INFO:
                        brushResult = Brushes.Transparent;
                        break;
                    case LibMessageTypes.WARNING:
                        brushResult = (SolidColorBrush)new BrushConverter().ConvertFrom("#FFFFCC00"); 
                        break;
                    case LibMessageTypes.ALERT:
                        brushResult = (SolidColorBrush)new BrushConverter().ConvertFrom("#BE0A0A");
                        break;

                }

                return brushResult;
            }
        }

        private Brush _Icon;
        public Brush Icon
        {
            get { return _Icon; }
            set { _Icon = value; NotifyPropertyChanged("Icon"); }
        }




        public LibMessage()
        {

        }

        public LibMessage(string message, LibMessageTypes messageType, DateTime dateTime, Brush icon = null)
        {
            Message = message;
            MessageType = messageType;
            DateTime = dateTime;
            Icon = icon;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        private void NotifyProperties()
        {
            NotifyPropertyChanged("TypeBrush");
            NotifyPropertyChanged("FullMessage");
        }

    }

    // SINGLETON!
    public sealed class LibMessagerService : INotifyPropertyChanged
    {
        private List<LibMessage> _MessageBuffer = new List<LibMessage>();

        private LibMessage _CurrentMessage = new LibMessage();
        public LibMessage CurrentMessage
        {
            get { return _CurrentMessage; }
            set { _CurrentMessage = value; NotifyPropertyChanged("CurrentMessage"); }
        }

        private LibMessagerService() { }

        private static LibMessagerService _instance;

        static LibMessagerService()
        {
            LibMessagerService.GetInstance();
        }


        public static LibMessagerService GetInstance()
        {
            if (_instance == null)
            {
                _instance = new LibMessagerService();
            }
            return _instance;
        }

        public static void ShowMessage(string message, LibMessageTypes messageType, Brush icon = null)
        {
            if (_instance != null && _instance.CurrentMessage != null && _instance._MessageBuffer != null)
            {
                var libMessage = new LibMessage(message, messageType, DateTime.Now, icon);
                _instance._CurrentMessage.Message = message;
                _instance._CurrentMessage.MessageType = messageType;
                _instance._CurrentMessage.DateTime = DateTime.Now;
                _instance._CurrentMessage.Icon = icon;
                _instance._MessageBuffer.Add(libMessage);
            }
            //_instance._CurrentMessage.NotifyProperties();
        }



        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
