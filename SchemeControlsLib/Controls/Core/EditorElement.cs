﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace SchemeControlsLib.Controls.Core
{
    /// <summary>
    /// Klasa bazowa elementu edytora. Można ustawić położenie w Canvas poprzez wrapper na Canvas.Left i Canvas.Top
    /// </summary>
    public abstract class EditorElement : ObservableControl, IEditorElement
    {
        protected Dispatcher _Dispatcher = Application.Current.Dispatcher;

        private bool _risePositionChangedEvent = true;

        #region Control Position - Canvas wrapper
        #region X property
        /// <summary>
        /// Y postition in scheme, wrapped on Canvas.Top attached property
        /// </summary>
        [EditorProperty("Y")]
        public double Y_Position
        {
            get { return (double)GetValue(Y_PositionProperty); }
            set { SetValue(Y_PositionProperty, value); }
        }

        public static readonly DependencyProperty Y_PositionProperty =
            DependencyProperty.Register("Y_Position", typeof(double), typeof(EditorElement), new PropertyMetadata(Y_Position_PropertyChanged));

        private static void Y_Position_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is EditorElement editorElement)
            {
                if (double.TryParse(e.NewValue?.ToString(), out double doubleVal))
                {
                    Canvas.SetTop(editorElement, doubleVal);
                    editorElement.Y_Position_Changed();

                    if (editorElement._risePositionChangedEvent)
                        editorElement.ControlPosition_Changed();
                }
            }
        }

        protected virtual void Y_Position_Changed() { }
        #endregion Y property

        #region X property
        /// <summary>
        /// X postition in scheme, wrapped on Canvas.Left attached property
        /// </summary>
        [EditorProperty("X")]
        public double X_Position
        {
            get { return (double)GetValue(X_PositionProperty); }
            set { SetValue(X_PositionProperty, value); }
        }

        public static readonly DependencyProperty X_PositionProperty =
            DependencyProperty.Register("X_Position", typeof(double), typeof(EditorElement), new PropertyMetadata(X_Position_PropertyChanged));

        private static void X_Position_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is EditorElement editorElement)
            {
                if (double.TryParse(e.NewValue?.ToString(), out double doubleVal))
                {
                    Canvas.SetLeft(editorElement, doubleVal);
                    editorElement.X_Position_Changed();

                    if (editorElement._risePositionChangedEvent)
                        editorElement.ControlPosition_Changed();
                }
            }
        }

        protected virtual void X_Position_Changed() { }
        #endregion X property

        protected virtual void ControlPosition_Changed() { }

        /// <summary>
        /// Sets x and y control position in scheme (with bounds validation)
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        /// <returns>True if both properties are setted</returns>
        public bool SetPosition(double x, double y)
        {
            bool result = true;
            this._risePositionChangedEvent = false;

            this.X_Position = x;
            this.Y_Position = y;

            ControlPosition_Changed();
            this._risePositionChangedEvent = true;
            return result;
        }
        #endregion Control Position - Canvas wrapper
    }
}
