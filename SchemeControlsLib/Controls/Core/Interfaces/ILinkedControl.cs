﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Controls.Interfaces.Core
{
    public interface ILinkedControl
    {
        FrameworkElement ControlParent { get; }
    }
}
