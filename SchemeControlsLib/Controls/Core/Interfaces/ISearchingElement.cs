﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Core.Interfaces
{
    public interface ISearchingElement
    {
        string SearchingText { get; }
        void BlinkElement(Brush colorToBlink, int blinksQuantity, int blinkIntervalMS);
    }
}
