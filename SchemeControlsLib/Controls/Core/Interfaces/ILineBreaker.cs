﻿using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;

namespace SchemeControlsLib.Controls.Core.Interfaces
{
    public interface ILineBreaker
    {
        void BreakTheLine(ILine schemeLine, SchemeCanvas schemeCanvas);

        /// <summary>
        /// Określa reguły przejścia stanu przez taki "łamacz" ("miesza" stany)
        /// </summary>
        /// <param name="schemeLine">Linnia która doszedł stan</param>
        /// <param name="outputNode">wyjście które pobydziło do zmiany stanu (równie dobrze wyjście może pobudzić)</param>
        /// <param name="state">Przekazany stan</param>
        void UpdateBreakerState(ILine schemeLine, IOutputNode outputNode, int state);

        bool IsStateUpdating { get; }
    }
}
