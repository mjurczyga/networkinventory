﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.Interfaces
{
    interface IChangeableControl
    {
        /// <summary>
        /// Ustawia nazwę kontrolki z walidacją
        /// </summary>
        /// <param name="name">nazwa</param>
        /// <returns>tru jeśli ustawiło</returns>
        bool SetName(string name);

        /// <summary>
        /// wpisuje wartość do podanej właściwości
        /// </summary>
        /// <param name="propertyName">nazwa właściwosci</param>
        /// <param name="value">wartość</param>
        /// <returns></returns>
        bool SetSpecificPropdpValue(string propertyName, object value);
    }
}
