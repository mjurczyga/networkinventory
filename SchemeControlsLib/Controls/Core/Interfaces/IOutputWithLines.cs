﻿using SchemeControlsLib.Controls.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.Core.Interfaces
{
    public interface IOutputWithLines : IOutputNode
    {
        List<ILine> ConnectedLines { get; }
        int LinesCount { get; }

        void AddLine(ILine line);
        void RemoveLine(ILine line);
    }
}
