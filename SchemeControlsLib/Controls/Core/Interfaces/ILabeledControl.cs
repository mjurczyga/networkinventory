﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Core.Interfaces
{
    public interface ILabeledControl
    {
        void RemoveLabel();

        string LabelText { get; set; }
        double LabelFontSize { get; }
        Brush LabelForeground { get; }

        double LabelStrokeThickness { get; }
        Brush LabelStroke { get; }
        
    }
}
