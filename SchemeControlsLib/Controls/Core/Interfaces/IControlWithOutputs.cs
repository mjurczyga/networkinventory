﻿using SchemeControlsLib.Controls.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.Core.Interfaces
{
    public interface IControlWithOutputs
    {
        object DeviceState { get; set; }
        RectangleBounds OutputBounds { get; set; }
        List<IOutputWithLines> OutputNodes { get; }

        void RemoveOutput(IOutputWithLines networkOutput);
        IOutputWithLines AddOutput(double x_Position, double y_Position, bool checkBounds = true);
    }
}
