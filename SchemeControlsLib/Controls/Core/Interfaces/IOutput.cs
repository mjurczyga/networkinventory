﻿using SchemeControlsLib.Controls.Interfaces.Core;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Controls.Interfaces
{
    public interface IOutputNode : ILinkedControl
    {
        Point CenterPoint { get; }

        int ParentState { get; set; }

        NodeType NodeType { get; set; }

    }
}
