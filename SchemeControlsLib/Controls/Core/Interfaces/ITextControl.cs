﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Core.Interfaces
{
    public interface ITextControl
    {
        string Description { get; set; }
        double FontSize { get; set; }
        Brush Foreground { get; set; }
        FontWeight FontWeight { get; set; }
        FontStyle FontStyle { get; set; }
        TextDecorationCollection TextDecorationCollection { get; set; }
    }
}
