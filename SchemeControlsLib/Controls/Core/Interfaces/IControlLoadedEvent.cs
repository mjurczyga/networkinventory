﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.Core.Interfaces
{
    public interface IControlLoadedEvent
    {
        void AddLoadedEvent();
    }
}
