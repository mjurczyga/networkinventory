﻿using SchemeControlsLib.Controls.LibControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Interfaces
{
    public interface ILine
    {
        double StrokeThickness { get; set; }
        Brush Stroke { get; set; }
        bool IsDashed { get; set; }
        bool IsArrowed { get; set; }

        void Connect(IOutputNode iOutput1, IOutputNode iOutput2);
        void Disconnect(bool remove = false);
        void UpdateConnection();

        void BreakLine(Point menuOpenedPoint, SchemeCanvas schemeCanvas);

        IOutputNode OutputNode1 { get; }
        IOutputNode OutputNode2 { get; }

        /// <summary>
        /// zmienia stan kontrolki w wyjściu przeciwnym do podanego w parametrze
        /// </summary>
        /// <param name="outputNode"></param>
        /// <param name="state"></param>
        void ChangeState(IOutputNode outputNode, int state);

        bool ReverseDrawing { get; set; }

        double X1 { get; set; }
        double Y1 { get; set; }

        double X2 { get; set; }
        double Y2 { get; set; }
    }
}
