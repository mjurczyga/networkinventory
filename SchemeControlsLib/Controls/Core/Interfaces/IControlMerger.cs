﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.Core.Interfaces
{
    public interface IControlMerger
    {
        IMergingControl MergingControl { get; set; }

        int ControlQuantity { get; set; }
    }
}
