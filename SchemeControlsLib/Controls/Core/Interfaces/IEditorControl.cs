﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.Interfaces
{
    public interface IEditableControl
    {
        /// <summary>
        /// Remove element from canvas
        /// </summary>
        void Remove();

        /// <summary>
        /// Copy element
        /// </summary>
        object Copy();

        /// <summary>
        /// clone elemen
        /// </summary>
        object Clone();

        bool IsEditMode { get; set; }
    }
}
