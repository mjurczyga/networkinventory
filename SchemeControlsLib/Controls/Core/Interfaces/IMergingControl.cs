﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Controls.Core.Interfaces
{
    public interface IMergingControl
    {
        int ControlQuantity { get; set; }

        List<EditableControlBase> MergedControls { get; }

        void AddControl(EditableControlBase editableControlBase, string controlName);
        void Remove();
        void Remove(EditableControlBase editableControlBase);

        void MoveGroup(EditableControlBase editableControlBase, Point oldPosition);
    }
}
