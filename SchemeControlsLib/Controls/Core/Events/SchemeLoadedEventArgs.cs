﻿using SchemeControlsLib.Controls.LibControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SchemeControlsLib.Controls.Events
{
    public enum SchemeStates
    {
        Loading,
        Loaded,
        Saving,
        Saved,
    }


    public class SchemeLoadedEventArgs : EventArgs
    {
        public SchemeLoadedEventArgs(SchemeCanvas schemeCanvas, UIElementCollection children)
        {
            Children = children;
            SchemeCanvas = schemeCanvas;
        }

        private SchemeCanvas _schemeCanvas;
        public SchemeCanvas SchemeCanvas
        {
            get { return _schemeCanvas; }
            set { _schemeCanvas = value; }
        }


        private UIElementCollection _children;
        public UIElementCollection Children
        {
            get { return _children; }
            set { _children = value; }
        }
    }

    public class SchemeStateChangedEventArgs : EventArgs
    {
        public SchemeStateChangedEventArgs(SchemeStates schemeState)
        {
            SchemeState = schemeState;
        }

        private SchemeStates _SchemeState;
        public SchemeStates SchemeState
        {
            get { return _SchemeState; }
            set { _SchemeState = value; }
        }
    }

    public class VarsBindingsDoneEventArgs : EventArgs
    {
        public VarsBindingsDoneEventArgs(SchemeCanvas schemeCanvas, IEnumerable<string> varsName)
        {
            VarsNames = varsName;
            SchemeCanvas = schemeCanvas;
        }

        private IEnumerable<string> _VarsNames;
        public IEnumerable<string> VarsNames
        {
            get { return _VarsNames; }
            set { _VarsNames = value; }
        }

        private SchemeCanvas _SchemeCanvas;
        public SchemeCanvas SchemeCanvas
        {
            get { return _SchemeCanvas; }
            set { _SchemeCanvas = value; }
        }
    }
}
