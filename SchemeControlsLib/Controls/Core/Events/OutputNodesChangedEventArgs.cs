﻿using SchemeControlsLib.Controls.ElectricControls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Events
{
    public class OutputNodesChangedEventArgs : EventArgs
    {
        public OutputNodesChangedEventArgs(ObservableCollection<OutputNode> oldValue, ObservableCollection<OutputNode> newValue)
        {
            _OldValue = oldValue;
            _NewValue = newValue;
        }  
        
        private ObservableCollection<OutputNode> _OldValue;
        public ObservableCollection<OutputNode> OldValue
        {
            get { return _OldValue; }
            set { _OldValue = value; }
        }

        private ObservableCollection<OutputNode> _NewValue;
        public ObservableCollection<OutputNode> NewValue
        {
            get { return _NewValue; }
            set { _NewValue = value; }
        }
    }
}
