﻿using SchemeControlsLib.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Controls.Core.Models
{
    public class RectangleBounds : ObservableObject
    {
        private double _Left;
        public double Left
        {
            get { return _Left; }
            set { _Left = value; NotifyPropertyChanged("Left"); }
        }

        private double _Top;
        public double Top
        {
            get { return _Top; }
            set { _Top = value; NotifyPropertyChanged("Top"); }
        }

        private double _Right;
        public double Right
        {
            get { return _Right; }
            set { _Right = value; NotifyPropertyChanged("Right"); }
        }

        private double _Bottom;
        public double Bottom
        {
            get { return _Bottom; }
            set { _Bottom = value; NotifyPropertyChanged("Bottom"); }
        }

        public RectangleBounds()
        {

        }

        public RectangleBounds(double left, double top, double right, double bottom)
        {
            Left = left;
            Top = top;
            Right = right;
            Bottom = bottom;
        }


        public static bool IsInBounds(Point pointToCheck, double left, double top, double right, double bottom)
        {
            if (pointToCheck.X >= left && pointToCheck.X <= right && pointToCheck.Y <= bottom && pointToCheck.Y >= top)
                return true;
            return false;
        }

        public bool IsInBounds(Point point)
        {
            return IsInBounds(point, this.Left, this.Top, this.Right, this.Bottom);
        }

        public bool IsInBounds(double x_Position, double y_Position)
        {
            return IsInBounds(new Point(x_Position, y_Position), this.Left, this.Top, this.Right, this.Bottom);
        }
    }
}
