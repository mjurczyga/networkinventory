﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace SchemeControlsLib.Controls.Core
{
    public class EditorControlBase : ObservableControl, IEditorElement
    {
        protected Dispatcher _Dispatcher = Dispatcher.CurrentDispatcher;

        private bool _risePositionChangedEvent = true;

        #region Z INDEX
        [EditorProperty("Numer warstwy")]
        public int LayerNumber
        {
            get { return (int)GetValue(LayerNumberProperty); }
            set { SetValue(LayerNumberProperty, value); }
        }

        public static readonly DependencyProperty LayerNumberProperty =
            DependencyProperty.Register("LayerNumber", typeof(int), typeof(EditorControlBase), new PropertyMetadata(LayerNumber_PropertyChanged));

        private static void LayerNumber_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is EditorControlBase editorControlBase && int.TryParse(e.NewValue?.ToString(), out int iNewValue))
            {
                Canvas.SetZIndex(editorControlBase, iNewValue);
            }
        }
        #endregion Z INDEX

        #region Control Position - Canvas wrapper
        #region Y property
        /// <summary>
        /// Y postition in scheme, wrapped on Canvas.Top attached property
        /// </summary>
        [EditorProperty("Y", "Układ")]
        public double Y_Position
        {
            get { return (double)GetValue(Y_PositionProperty); }
            set { SetValue(Y_PositionProperty, value); }
        }

        public static readonly DependencyProperty Y_PositionProperty =
            DependencyProperty.Register("Y_Position", typeof(double), typeof(EditorControlBase), new PropertyMetadata(Y_Position_PropertyChanged));

        private static void Y_Position_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is EditorControlBase editorControlBase)
            {
                if (double.TryParse(e.NewValue?.ToString(), out double doubleVal) && double.TryParse(e.OldValue?.ToString(), out double oldValue))
                {
                    Canvas.SetTop(editorControlBase, doubleVal);
                    editorControlBase.Y_Position_Changed();

                    if (editorControlBase._risePositionChangedEvent)
                        editorControlBase.ControlPosition_Changed(editorControlBase.X_Position, oldValue);
                }
            }
        }

        protected virtual void Y_Position_Changed() { }
        #endregion Y property

        #region X property
        /// <summary>
        /// X postition in scheme, wrapped on Canvas.Left attached property
        /// </summary>
        [EditorProperty("X", "Układ")]
        public double X_Position
        {
            get { return (double)GetValue(X_PositionProperty); }
            set { SetValue(X_PositionProperty, value); }
        }

        public static readonly DependencyProperty X_PositionProperty =
            DependencyProperty.Register("X_Position", typeof(double), typeof(EditorControlBase), new PropertyMetadata(X_Position_PropertyChanged));

        private static void X_Position_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is EditorControlBase editorControlBase)
            {
                if (double.TryParse(e.NewValue?.ToString(), out double doubleVal) && double.TryParse(e.OldValue?.ToString(), out double oldValue))
                {
                    Canvas.SetLeft(editorControlBase, doubleVal);
                    editorControlBase.X_Position_Changed();

                    if (editorControlBase._risePositionChangedEvent)
                        editorControlBase.ControlPosition_Changed(oldValue, editorControlBase.Y_Position);
                }
            }
        }

        protected virtual void X_Position_Changed() { }
        #endregion X property

        #region Ingerited properties
        [EditorProperty("Szerokość", "Układ")]
        public new double Width { get { return base.Width; } set { base.Width = value; } }

        [EditorProperty("Wysokość", "Układ")]
        public new double Height { get { return base.Height; } set { base.Height = value; } }

        [EditorProperty("Nazwa", "Wspólne")]
        public new string Name { get { return base.Name; } set { base.Name = value; } }
        #endregion

        protected virtual void ControlPosition_Changed(double oldX_Position, double oldY_Position) { }

        /// <summary>
        /// Sets x and y control position in scheme (with bounds validation)
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        /// <returns>True if both properties are setted</returns>
        public virtual bool SetPosition(double x, double y)
        {
            bool result = true;
            this._risePositionChangedEvent = false;
            var oldX_Position = this.X_Position;
            var oldY_Position = this.Y_Position;

            this.X_Position = Math.Round(x,1);
            this.Y_Position = Math.Round(y, 1);

            ControlPosition_Changed(oldX_Position, oldY_Position);
            this._risePositionChangedEvent = true;
            this.NotifyCenterPointChanged();
            return result;
        }
        #endregion Control Position - Canvas wrapper

        /// <summary>
        /// Koordynaty środka wyjścia w canvasie
        /// </summary>
        private Point _CenterPoint = new Point();
        public Point CenterPoint
        {
            get
            {
                _CenterPoint.X = this.X_Position;
                _CenterPoint.Y = this.Y_Position;
                if (!double.IsNaN(this.Width))
                    _CenterPoint.X += this.Width / 2.0;
                if (!double.IsNaN(this.Height))
                    _CenterPoint.Y += this.Height / 2.0;
                return _CenterPoint;
            }
        }

        private Point _RelativeCenterPoint = new Point();
        public Point RelativeCenterPoint
        {
            get
            {
                _RelativeCenterPoint.X = this.Width / 2.0; 
                _RelativeCenterPoint.Y = this.Height / 2.0;
                return _CenterPoint;
            }
        }

        private void NotifyCenterPointChanged()
        {
            NotifyPropertyChanged("CenterPoint");
            NotifyPropertyChanged("RelativeCenterPoint");
        }

        public EditorControlBase()
        {
            DependencyPropertyDescriptor.FromProperty(EditorControlBase.HeightProperty, typeof(EditorControlBase)).AddValueChanged(this, CarrierType_PropertyChanged);
            DependencyPropertyDescriptor.FromProperty(EditorControlBase.WidthProperty, typeof(EditorControlBase)).AddValueChanged(this, EditorControlBase_WidthProperty_PropertyChanged);
        }

        private void CarrierType_PropertyChanged(object sender, EventArgs e)
        {
            this.NotifyCenterPointChanged();
        }

        private void EditorControlBase_WidthProperty_PropertyChanged(object sender, EventArgs e)
        {
            this.NotifyCenterPointChanged();
        }
    }
}
