﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.NetworkControls.Interfaces;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Core
{
    public abstract class OutputBase : EditorControlBase, ISelectableElement, IMovableElement, IHideableElement, IEditableControl, IOutputWithLines
    {
        #region Properties wrapper
        public new double Width { get { return base.Width; } set { base.Width = value; } }
        public new double Height { get { return base.Height; } set { base.Height = value; } }
        public new double FontSize { get { return base.FontSize; } set { base.FontSize = value; } }
        public new Brush BorderBrush { get { return base.BorderBrush; } set { base.BorderBrush = value; } }
        #endregion Properties wrapper

        private bool _CheckBounds = true;
        public bool CheckBounds
        {
            get { return _CheckBounds; }
            set { _CheckBounds = value; NotifyPropertyChanged("CheckBounds"); }
        }

        #region IEditableControl
        public bool IsEditMode
        {
            get { return (bool)GetValue(IsEditModeProperty); }
            set { SetValue(IsEditModeProperty, value); }
        }

        public static readonly DependencyProperty IsEditModeProperty =
            DependencyProperty.Register("IsEditMode", typeof(bool), typeof(OutputBase), new PropertyMetadata(false, IsEditMode_PropertyChanged));

        private static void IsEditMode_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is OutputBase outputBase)
                outputBase.ChangeEditMode((bool)e.NewValue);
        }

        /// <summary>
        /// actions for changed Edit mode
        /// </summary>
        /// <param name="isEditMode"></param>
        protected virtual void ChangeEditMode(bool isEditMode)
        {

        }

        /// <summary>
        /// metoda usuwająca kontroklę z UI(SchemeCanvas)
        /// </summary>
        public void Remove()
        {
            if (this.Parent is SchemeCanvas schemeCanvas)
            {
                if (this.ControlParent is IControlWithOutputs controlWithOutputs && controlWithOutputs.OutputNodes.Contains(this))
                {
                    controlWithOutputs.OutputNodes.Remove(this);
                }

                schemeCanvas.RemoveElementFromScheme(this);
                foreach (var line in ConnectedLines)
                {
                    if (line is ObservableControl observableControl)
                        schemeCanvas.RemoveElementFromScheme(observableControl);
                }
            }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public object Copy()
        {
            var copiedElement = Activator.CreateInstance(this.GetType()) as EditorControlBase;

            var thisDependencyProperties = DependencyPropertyFinder.GetAllDependencyProperties(this);
            foreach (var property in thisDependencyProperties)
            {
                if (!property.ReadOnly && property.Name.ToUpper() != "NAME")
                    copiedElement.SetValue(property, this.GetValue(property));
                else if (!property.ReadOnly && property.Name.ToUpper() != "NAME" && this.Parent is Canvas canvas)
                    copiedElement.SetValue(property, CanvasUtilities.GetNextNewElementName(canvas));
            }

            var eventsFields = DependencyPropertyFinder.GetAllRoutedEvents(this);

            foreach (var eventField in eventsFields)
            {
                copiedElement.AddHandler((RoutedEvent)eventField.Key.GetValue(this), eventField.Value.Handler);
            }

            copiedElement.SetPosition(this.X_Position, this.Y_Position);

            return copiedElement;
        }
        #endregion IEditableControl

        #region IHideableElement
        public bool HideElement
        {
            get { return (bool)GetValue(HideElementProperty); }
            set { SetValue(HideElementProperty, value); }
        }

        public static readonly DependencyProperty HideElementProperty =
            DependencyProperty.Register("HideElement", typeof(bool), typeof(OutputBase), new PropertyMetadata(false, HideElement_PropertyChanged));

        private static void HideElement_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is OutputBase outputBase)
            {
                outputBase.SetControlVisibility(!(bool)e.NewValue);
            }
        }
        #endregion IHideableElement

        #region overrides
        /// <summary>
        /// ustawia pozycję
        /// </summary>
        /// <param name="x">Lewe przesuniecie wyjścia</param>
        /// <param name="y">Prawe przesunięcie wyjścia</param>
        /// <returns></returns>
        public override bool SetPosition(double x, double y)
        {
            if (this.ControlParent is IControlWithOutputs networkControl && CheckBounds)
            {
                if (x + this.Width / 2.0 < networkControl.OutputBounds.Left)
                    x = networkControl.OutputBounds.Left - this.Width / 2.0;
                else if (x + this.Width / 2.0 > networkControl.OutputBounds.Right)
                    x = networkControl.OutputBounds.Right - this.Width / 2.0;
                if (y + this.Height / 2.0 < networkControl.OutputBounds.Top)
                    y = networkControl.OutputBounds.Top - this.Height / 2.0;
                else if (y + this.Height / 2.0 > networkControl.OutputBounds.Bottom)
                    y = networkControl.OutputBounds.Bottom - this.Height / 2.0;
            }

            return base.SetPosition(x, y);
        }

        protected override void ControlPosition_Changed(double oldX_Position, double oldY_Position)
        {
            base.ControlPosition_Changed(oldX_Position, oldY_Position);
            foreach (var connectedLine in ConnectedLines)
            {
                connectedLine.UpdateConnection();
            }
        }
        #endregion overrides

        #region IOutputNode
        [EditorProperty("Typ węzła")]
        public NodeType NodeType
        {
            get { return (NodeType)GetValue(NodeTypeProperty); }
            set { SetValue(NodeTypeProperty, value); }
        }

        public static readonly DependencyProperty NodeTypeProperty =
            DependencyProperty.Register("NodeType", typeof(NodeType), typeof(OutputBase), new PropertyMetadata(NodeType.IO, NodeType_PropertyChanged));

        private static void NodeType_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is OutputBase outputBase)
            {
                outputBase.UpdateConnectedLinesState(outputBase.ParentState);
                outputBase.UpdateNodeBackground();
            }
        }

        private Brush _NodeBackground = Brushes.Purple;
        public Brush NodeBackground
        {
            get { return _NodeBackground; }
            private set { _NodeBackground = value; NotifyPropertyChanged("NodeBackground"); }
        }

        private void UpdateNodeBackground()
        {
            if (NodeType == NodeType.I)
                NodeBackground = Brushes.Purple;
            else if (NodeType == NodeType.O)
                NodeBackground = Brushes.Lime;
            else if (NodeType == NodeType.IO)
            {
                LinearGradientBrush gradient = new LinearGradientBrush();
                gradient.StartPoint = new Point(0, 0);
                gradient.EndPoint = new Point(1, 1);
                GradientStop firstColor = new GradientStop();
                firstColor.Color = Colors.Purple;
                firstColor.Offset = 0.5;
                gradient.GradientStops.Add(firstColor);

                GradientStop secondColor = new GradientStop();
                secondColor.Color = Colors.Lime;
                secondColor.Offset = 1.0;
                gradient.GradientStops.Add(secondColor);

                NodeBackground = gradient;
            }
            else
                NodeBackground = Brushes.Pink;
        }

        private int _ParentState;
        public int ParentState
        {
            get { return _ParentState; }
            set { _ParentState = value; UpdateConnectedLinesState(value); NotifyPropertyChanged("ParentState"); }
        }

        protected virtual void UpdateConnectedLinesState(int state)
        {
           foreach(var line in this.ConnectedLines)
            {
                line.ChangeState(this, state);
            }
        }      
        
        public FrameworkElement ControlParent { get; private set; }

        public List<ILine> ConnectedLines { get; private set; } = new List<ILine>();

        private int _LinesCount;
        public int LinesCount
        {
            get { return _LinesCount; }
            private set { _LinesCount = value; NotifyPropertyChanged("LinesCount"); }
        }


        public void AddLine(ILine line)
        {
            if (!ConnectedLines.Contains(line))
                ConnectedLines.Add(line);

            LinesCount = ConnectedLines.Count();
        }

        public void RemoveLine(ILine line)
        {
            if (ConnectedLines.Contains(line))
                ConnectedLines.Remove(line);

            LinesCount = ConnectedLines.Count();
        }
        #endregion IOutputNode

        private void SetControlVisibility(bool isVisible)
        {
            if (isVisible)
                this.Visibility = Visibility.Visible;
            else
                this.Visibility = Visibility.Collapsed;
        }

        static OutputBase()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(OutputBase), new FrameworkPropertyMetadata(typeof(OutputBase)));
        }

        public OutputBase(FrameworkElement controlParent) : base()
        {
            ControlParent = controlParent;
            this.Width = 10;
            this.Height = 10;

            this.ConstructorMethods();
        }
        public OutputBase()
        {
            this.ConstructorMethods();
        }

        private void ConstructorMethods()
        {
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.UpdateNodeBackground();
        }
    }
}
