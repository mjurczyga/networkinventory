﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.Core.Models;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;

namespace SchemeControlsLib.Controls.Core
{
    public abstract class EditableControlBase : ControlBase, IControlWithOutputs, ISelectableElement, IGroupSelectableElement, IMovableElement, IMovableSelectionElement, IEditableControl
    {
        #region IsSelectionMoving
        private bool _IsSelectionMoving;
        public bool IsSelectionMoving
        {
            get { return _IsSelectionMoving; }
            set { _IsSelectionMoving = value; NotifyPropertyChanged("IsSelectionMoving"); }
        }
        #endregion IsSelectionMoving

        #region IEditableControl
        [EditorProperty("Tryb edycji", "Układ", serialize: false)]
        public bool IsEditMode
        {
            get { return (bool)GetValue(IsEditModeProperty); }
            set { SetValue(IsEditModeProperty, value); }
        }

        public static readonly DependencyProperty IsEditModeProperty =
            DependencyProperty.Register("IsEditMode", typeof(bool), typeof(EditableControlBase), new PropertyMetadata(false, IsEditMode_PropertyChanged));

        private static void IsEditMode_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            EditableControlBase editableControlBase = (EditableControlBase)d;
            editableControlBase.ChangeEditMode((bool)e.NewValue);
        }

        /// <summary>
        /// actions for changed Edit mode
        /// </summary>
        /// <param name="isEditMode"></param>
        protected virtual void ChangeEditMode(bool isEditMode)
        {

        }

        /// <summary>
        /// metoda usuwająca kontroklę z UI(SchemeCanvas)
        /// </summary>
        public virtual void Remove()
        {
            if (this.Parent is SchemeCanvas)
            {
                var schemeCanvas = (SchemeCanvas)this.Parent;

                if (OutputNodes != null)
                {
                    //kopia zrobiona po to żeby ni było kolizji z IOutputWithLines.Remove()
                    var outputNodesCopy = new List<IOutputWithLines>(OutputNodes);
                    foreach (var outputNode in outputNodesCopy)
                    {
                        if (outputNode is IEditableControl networkOutput)
                        {
                            networkOutput.Remove();
                        }
                   }

                }

                schemeCanvas.RemoveElementFromScheme(this);
            }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public virtual object Copy()
        {
            var copiedElement = Activator.CreateInstance(this.GetType()) as ControlBase;

            var thisDependencyProperties = DependencyPropertyFinder.GetAllDependencyProperties(this);
            foreach (var property in thisDependencyProperties)
            {
                if (!property.ReadOnly && property.Name.ToUpper() != "NAME")
                    copiedElement.SetValue(property, this.GetValue(property));
                else if (!property.ReadOnly && property.Name.ToUpper() != "NAME" && this.Parent is Canvas)
                    copiedElement.SetValue(property, CanvasUtilities.GetNextNewElementName((Canvas)this.Parent));
            }

            var eventsFields = DependencyPropertyFinder.GetAllRoutedEvents(this);

            foreach (var eventField in eventsFields)
            {
                copiedElement.AddHandler((RoutedEvent)eventField.Key.GetValue(this), eventField.Value.Handler);
            }

            copiedElement.SetPosition(this.X_Position, this.Y_Position);

            return copiedElement;
        }
        #endregion IEditableControl

        #region Outputs
        #region bounds
        private RectangleBounds _OutputBounds;
        public RectangleBounds OutputBounds
        {
            get { return _OutputBounds; }
            set { _OutputBounds = value; NotifyPropertyChanged("OutputBounds"); }
        }

        protected override void ControlPosition_Changed(double oldX_Position, double oldY_Position)
        {
            base.ControlPosition_Changed(oldX_Position, oldY_Position);
            OutputBounds = new RectangleBounds(this.X_Position, this.Y_Position, this.X_Position + this.ActualWidth, this.Y_Position + this.ActualHeight);

            var delay = new Point(oldX_Position - X_Position, oldY_Position - Y_Position);
            foreach (var outputNode in OutputNodes)
            {
                if (outputNode is IEditorElement editorElement)
                    editorElement.SetPosition(editorElement.X_Position - delay.X, editorElement.Y_Position - delay.Y);
            }
        }

        private void RefreshBounds()
        {
            OutputBounds = new RectangleBounds(this.X_Position, this.Y_Position, this.X_Position + this.Width, this.Y_Position + this.Height);
        }
        #endregion

        public List<IOutputWithLines> OutputNodes { get; private set; } = new List<IOutputWithLines>();
        public abstract IOutputWithLines AddOutput(double x_Position, double y_Position, bool checkBounds = true);

        /// <summary>
        /// Remove output from OutputNodes current control list (not from scheme!!)
        /// </summary>
        /// <param name="networkOutput">OutputNode to remove</param>
        public virtual void RemoveOutput(IOutputWithLines networkOutput)
        {
            if (networkOutput is IEditableControl editableControl)
                editableControl.Remove();
        }

        private void UpdateOutpustState(int state)
        {
            foreach (var outputNode in OutputNodes)
            {
                outputNode.ParentState = state;
            }
        }
        #endregion

        protected override void StateChanged(int state)
        {
            base.StateChanged(state);
            this.UpdateOutpustState(state);
        }

        protected override Size MeasureOverride(Size constraint)
        {
            return base.MeasureOverride(constraint);
        }

        public EditableControlBase()
        {
            DependencyPropertyDescriptor.FromProperty(EditableControlBase.HeightProperty, typeof(EditableControlBase)).AddValueChanged(this, HeightProperty_PropertyChanged);
            DependencyPropertyDescriptor.FromProperty(EditableControlBase.WidthProperty, typeof(EditableControlBase)).AddValueChanged(this, WidthProperty_PropertyChanged);
        }

        private void WidthProperty_PropertyChanged(object sender, EventArgs e)
        {
            if (sender is EditableControlBase editableControlBase)
            {
                editableControlBase.RefreshBounds();
            }
        }

        private void HeightProperty_PropertyChanged(object sender, EventArgs e)
        {
            if (sender is EditableControlBase editableControlBase)
            {
                editableControlBase.RefreshBounds();
            }
        }
    }
}
