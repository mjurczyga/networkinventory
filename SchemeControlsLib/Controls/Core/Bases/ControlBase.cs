﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Core
{
    public abstract class ControlBase : EditorControlBase, IActiveElement, IBindingControl
    {
        #region Inherited properties wrapper
        [EditorProperty("Kolor ramki", "Układ")]
        public new Brush BorderBrush { get { return base.BorderBrush; } set { base.BorderBrush = value; } }

        [EditorProperty("Rozmiar tekstu", "Text")]
        public new double FontSize { get { return base.FontSize; } set { base.FontSize = value; } }
        #endregion Inherited properties wrapper

        #region Control Name
        /// <summary>
        /// Set control name with "Name" validation and "dubled names" validation 
        /// </summary>
        /// <param name="name">new control name</param>
        /// <returns>True if setted correctly</returns>
        public bool SetName(string name)
        {
            if (this.Parent is SchemeCanvas && name.CheckIfIsControlName() && !((SchemeCanvas)this.Parent).CheckIfNameExist(name))
            {
                this.Name = name;
                return true;
            }
            return false;
        }
        #endregion Control Name

        #region Device status
        [EditorProperty("Status urządzenia", "Wspólne")]
        public object DeviceState
        {
            get { return (object)GetValue(DeviceStateProperty); }
            set { SetValue(DeviceStateProperty, value); }
        }

        public static readonly DependencyProperty DeviceStateProperty =
            DependencyProperty.Register("DeviceState", typeof(object), typeof(ControlBase), new PropertyMetadata(0, DeviceStatus_PropertyChanged));

        private static void DeviceStatus_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ControlBase controlBase)
            {
                if (int.TryParse(e.NewValue?.ToString(), out int intResult))
                    controlBase.State = intResult;
                else if (bool.TryParse(e.NewValue?.ToString(), out bool boolResult))
                {
                    if (boolResult)
                        controlBase.State = 2;
                    else
                        controlBase.State = 1;
                }
                else
                    controlBase.State = 0;
            }
        }
        #endregion Device status

        #region Device name
        [EditorProperty("Nazwa urządzenia", "Wspólne")]
        public string DeviceName
        {
            get { return (string)GetValue(DeviceNameProperty); }
            set { SetValue(DeviceNameProperty, value); }
        }

        public static readonly DependencyProperty DeviceNameProperty =
            DependencyProperty.Register("DeviceName", typeof(string), typeof(ControlBase));

        #endregion

        #region ClickCommand
        [EditorProperty("Zdarzenie na kliknięcie", "Zdarzenia")]
        public object ClickCommand
        {
            get { return (object)GetValue(ClickCommandProperty); }
            set { SetValue(ClickCommandProperty, value); }
        }

        public static readonly DependencyProperty ClickCommandProperty =
            DependencyProperty.Register("ClickCommand", typeof(object), typeof(ControlBase), new PropertyMetadata(ClickCommand_PropertyChanged));

        private static void ClickCommand_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ControlBase controlBase)
            {
                if (e.NewValue is ICommand command)
                {
                    controlBase.InternalClickCommand = command;
                }
                if (e.NewValue != null)
                    controlBase.Cursor = Cursors.Hand;
                else
                    controlBase.Cursor = Cursors.Arrow;

            }
        }

        private ICommand InternalClickCommand;

        [EditorProperty("Parametr zdarzenia na kliknięcie", "Zdarzenia")]
        public object ClickCommandParameter
        {
            get { return (object)GetValue(ClickCommandParameterProperty); }
            set { SetValue(ClickCommandParameterProperty, value); }
        }

        public static readonly DependencyProperty ClickCommandParameterProperty =
            DependencyProperty.Register("ClickCommandParameter", typeof(object), typeof(ControlBase));
        #endregion ClickCommand

        #region layout
        private Brush _InternalBackgroundBrush = Brushes.LightGray;
        public Brush InternalBackgroundBrush
        {
            get { return _InternalBackgroundBrush; }
            protected set { _InternalBackgroundBrush = value; NotifyPropertyChanged("InternalBackgroundBrush"); }
        }

        public abstract double ElementStrokeThickness { get;  protected set; }
        public abstract Brush ElementStroke { get; protected set; }

        /// <summary>
        /// instrukcje jaki kolor powinna przyjąć kontrolka w zależności od tego jaki jest jej stan
        /// </summary>
        /// <param name="value"></param>
        protected virtual void UpdateBackgroundBrush(int value)
        {
            switch (value)
            {
                case 0:
                    InternalBackgroundBrush = Settings.StatusInvalidBrush;
                    break;
                case 1:
                    InternalBackgroundBrush = Settings.StatusOffBrush;
                    break;
                case 2:
                    InternalBackgroundBrush = Settings.StatusOnBrush;
                    break;
                default:
                    ElementStroke = Brushes.Pink;
                    break;
            }
        }
        #endregion layout

        #region overrides
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            if (InternalClickCommand != null)
            {
                if (ClickCommandParameter != null)
                    InternalClickCommand.Execute(ClickCommandParameter);
                else
                    InternalClickCommand.Execute(this);
            }
        }
        #endregion overrides

        #region Interface inplementation
        #region IActiveElement
        protected bool _RiseUpdateStateEvent = true;

        private int _State;
        public int State
        {
            get { return _State; }
            set
            {
                _State = value;
                if (_RiseUpdateStateEvent)
                    this.StateChanged(_State);
                UpdateBackgroundBrush(_State);
                NotifyPropertyChanged("State");
            }
        }

        protected virtual void StateChanged(int state)
        {

        }
        #endregion IActiveElement
        #endregion Interface inplementation

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
        }
    }
}
