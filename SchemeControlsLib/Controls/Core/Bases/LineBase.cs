﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Core
{
    public abstract class LineBase : ControlBase, ILine
    {
        private bool _AddingConenction = false;

        #region Properties wrapper
        public new double Width { get { return base.Width; } set { base.Width = value; } }
        public new double Height { get { return base.Height; } set { base.Height = value; } }
        //public new object DeviceState { get { return base.DeviceState; } set { base.DeviceState = value; } }
        public new double X_Position { get { return base.X_Position; } set { base.X_Position = value; } }
        public new double Y_Position { get { return base.Y_Position; } set { base.Y_Position = value; } }
        public new string DeviceName { get { return base.DeviceName; } set { base.DeviceName = value; } }
        public new object ClickCommand { get { return base.ClickCommand; } set { base.ClickCommand = value; } }
        public new object ClickCommandParameter { get { return base.ClickCommandParameter; } set { base.ClickCommandParameter = value; } }
        public new double FontSize { get { return base.FontSize; } set { base.FontSize = value; } }
        public new Brush BorderBrush { get { return base.BorderBrush; } set { base.BorderBrush = value; } }
        #endregion Properties wrapper

        #region Dependency properties
        #region Linia kreskowana
        [EditorProperty("Kreskowana?", "Linia" )]
        public bool IsDashed
        {
            get { return (bool)GetValue(IsDashedProperty); }
            set { SetValue(IsDashedProperty, value); }
        }

        public static readonly DependencyProperty IsDashedProperty =
            DependencyProperty.Register("IsDashed", typeof(bool), typeof(LineBase), new PropertyMetadata(false, OnDashed_PropertyChanged));

        private static void OnDashed_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is LineBase lineBase && bool.TryParse(e.NewValue?.ToString(), out bool boolVal))
            {
                if (boolVal)
                    lineBase.StrokeDashArray = new DoubleCollection() { 3, 3 };
                else
                    lineBase.StrokeDashArray = null;
            }
        }

        private DoubleCollection _StrokeDashArray;
        public DoubleCollection StrokeDashArray
        {
            get { return _StrokeDashArray; }
            set { _StrokeDashArray = value; NotifyPropertyChanged("StrokeDashArray"); }
        }
        #endregion Linia kreskowana

        #region Arrows on lines
        [EditorProperty("Strzałki?", "Linia")]
        public bool IsArrowed
        {
            get { return (bool)GetValue(IsArrowedProperty); }
            set { SetValue(IsArrowedProperty, value); }
        }

        public static readonly DependencyProperty IsArrowedProperty =
            DependencyProperty.Register("IsArrowed", typeof(bool), typeof(LineBase), new PropertyMetadata(false));

        #endregion Arrows on lines

        #region ReverseDraving
        [EditorProperty("Odwrócone rysowanie", "Linia")]
        public bool ReverseDrawing
        {
            get { return (bool)GetValue(ReverseDrawingProperty); }
            set { SetValue(ReverseDrawingProperty, value); }
        }

        public static readonly DependencyProperty ReverseDrawingProperty =
            DependencyProperty.Register("ReverseDrawing", typeof(bool), typeof(LineBase), new PropertyMetadata(false, ReverseDrawing_PropertyChanged));

        private static void ReverseDrawing_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is LineBase lineBase)
            {
                lineBase.UpdateConnection();
            }
        }
        #endregion ReverseDraving

        private Brush _Stroke;
        public Brush Stroke
        {
            get { return _Stroke; }
            set { _Stroke = value; }
        }

        public double StrokeThickness
        {
            get { return (double)GetValue(StrokeThicknessProperty); }
            set { SetValue(StrokeThicknessProperty, value); }
        }


        public static readonly DependencyProperty StrokeThicknessProperty =
            DependencyProperty.Register("StrokeThickness", typeof(double), typeof(LineBase));

        public double X1
        {
            get { return (double)GetValue(X1Property); }
            set { SetValue(X1Property, value); }
        }

        public static readonly DependencyProperty X1Property =
            DependencyProperty.Register("X1", typeof(double), typeof(LineBase), new PropertyMetadata(0.0));

        public double Y1
        {
            get { return (double)GetValue(Y1Property); }
            set { SetValue(Y1Property, value); }
        }

        public static readonly DependencyProperty Y1Property =
            DependencyProperty.Register("Y1", typeof(double), typeof(LineBase), new PropertyMetadata(0.0));

        public double X2
        {
            get { return (double)GetValue(X2Property); }
            set { SetValue(X2Property, value); }
        }

        public static readonly DependencyProperty X2Property =
            DependencyProperty.Register("X2", typeof(double), typeof(LineBase), new PropertyMetadata(0.0));

        public double Y2
        {
            get { return (double)GetValue(Y2Property); }
            set { SetValue(Y2Property, value); }
        }

        public static readonly DependencyProperty Y2Property =
            DependencyProperty.Register("Y2", typeof(double), typeof(LineBase), new PropertyMetadata(0.0));

        #endregion ILine props

        #region INetworkLine props
        private UIElement _Lines;
        public UIElement Lines
        {
            get { return _Lines; }
            set { _Lines = value; NotifyPropertyChanged("Lines"); }
        }
        #endregion INetworkLine

        #region Control Drawing 
        public Geometry LinesGeometry { get; protected set; }

        public IOutputNode OutputNode1 { get; protected set; }

        public IOutputNode OutputNode2 { get; protected set; }

        private double _ElementStrokeThickness = Settings.Network_ElementStrokeThickness;
        public override double ElementStrokeThickness
        {
            get
            {
                return _ElementStrokeThickness;
            }
            protected set
            {
                _ElementStrokeThickness = value; NotifyPropertyChanged("ElementStrokeThickness");
            }
        }
        private Brush _ElementStroke = Settings.Network_ElementStroke;
        public override Brush ElementStroke {
            get { return _ElementStroke; }
            protected set { _ElementStroke = value; NotifyPropertyChanged("ElementStroke"); } }

        public virtual void Connect(IOutputNode outputNode, IOutputNode outputNode1)
        {
            if (outputNode is OutputBase outputBase1 && outputNode1 is OutputBase outputBase2)
            {

                bool result = DependencyPropertyFinder.CopyParentClickHandler(outputBase1, this);
                if (!result)
                    DependencyPropertyFinder.CopyParentClickHandler(outputBase2, this);

                outputBase1.AddLine(this);
                outputBase2.AddLine(this);

                OutputNode1 = outputBase1;
                OutputNode2 = outputBase2;

                this.SetValue(FrameworkElement.NameProperty, outputBase1.Name + "_TO_" + outputBase2.Name);

                this.UpdateConnection(outputNode, outputNode1);
            }
        }

        public void UpdateConnection()
        {
            if (OutputNode1 != null && OutputNode2 != null)
            {
                this.UpdateConnection(OutputNode1, OutputNode2);
            }
        }

        protected void UpdateConnection(IOutputNode output1, IOutputNode output2)
        {
            _AddingConenction = true;
            if (output1.CenterPoint.X >= output2.CenterPoint.X)
            {
                this.X_Position = output2.CenterPoint.X;
                this.X1 = output1.CenterPoint.X - output2.CenterPoint.X;
                this.X2 = 0;
            }
            else
            {
                this.X_Position = output1.CenterPoint.X;
                this.X1 = 0;
                this.X2 = output2.CenterPoint.X - output1.CenterPoint.X;
            }

            if (output1.CenterPoint.Y >= output2.CenterPoint.Y)
            {
                this.Y_Position = output2.CenterPoint.Y;
                this.Y1 = output1.CenterPoint.Y - output2.CenterPoint.Y;
                this.Y2 = 0;
            }
            else
            {
                this.Y_Position = output1.CenterPoint.Y;
                this.Y1 = 0;
                this.Y2 = output2.CenterPoint.Y - output1.CenterPoint.Y;
            }

            _AddingConenction = false;
        }

        public virtual void Disconnect(bool remove = false)
        {
            if (this.Parent is SchemeCanvas schemeCanvas && this.OutputNode1 is OutputBase outputBase1 && this.OutputNode2 is OutputBase outputBase2)
            {
                //jeśli linia jest połączona z NetworkLineBreaker, to usuń linie, breaker i połącz pozostałe elementy
                //var no1_CP = networkOutput1.ControlParent;
                //var no2_CP = networkOutput2.ControlParent;

                //if (no1_CP is NetworkLineBreaker networkLineBreaker1 && (!(no2_CP is NetworkLineBreaker) || no2_CP is BaseNetworkControl baseNetworkControl2))
                //{
                //}

                outputBase1.RemoveLine(this);
                outputBase2.RemoveLine(this);
                schemeCanvas.RemoveElementFromScheme(this);
            }
        }

        public virtual void BreakLine(Point menuOpenedPoint, SchemeCanvas schemeCanvas)
        {
            //var networkLineBreaker = new NetworkLineBreaker() { NetworkType = this.NetworkType };
            //schemeCanvas.AddSchemeElement(networkLineBreaker, true);

            //networkLineBreaker.SetPosition(menuOpenedPoint.X - networkLineBreaker.Width / 2.0, menuOpenedPoint.Y - networkLineBreaker.Height / 2.0);
            //networkLineBreaker.BreakTheLine(this, schemeCanvas);
        }
        #endregion Control Drawing 

        protected override void StateChanged(int state)
        {
            UpdateState(OutputNode1, state);
            UpdateState(OutputNode2, state);
        }

        /// <summary>
        /// zmienia stan kontrolki w wyjściu przeciwnym do podanego w parametrze
        /// </summary>
        /// <param name="outputNode"></param>
        /// <param name="state"></param>
        public void ChangeState(IOutputNode outputNode, int state)
        {
            _RiseUpdateStateEvent = true;
            
            if (OutputNode1 == outputNode)
            {
                this.DeviceState = state;
                UpdateState(OutputNode2, state);
                // zmień kolor
            }

            // jeśli są strzałki z kierunkiem przepływu to nie koloruj w przeciwną stronę
            if (OutputNode2 == outputNode && !IsArrowed)
            {
                this.DeviceState = state;
                UpdateState(OutputNode1, state);
            }

            _RiseUpdateStateEvent = true;

        }

        /// <summary>
        /// zmienia stan kontrlki dla wyjścia podanego w parametrze
        /// </summary>
        /// <param name="outputNode"></param>
        /// <param name="state"></param>
        protected void UpdateState(IOutputNode outputNode, int state)
        {
            if (outputNode.ControlParent is ILineBreaker lineBreaker && outputNode.ParentState != state && (outputNode.NodeType == NodeType.IO || outputNode.NodeType == NodeType.O))
            {
                lineBreaker.UpdateBreakerState(this, outputNode, state);

                //outputNode.ParentState = state;
            }

            if ((outputNode.NodeType == NodeType.I || outputNode.NodeType == NodeType.IO) && outputNode.ControlParent is IActuator actuator)
            {
                actuator.UpdateState(state);
            }
        }
    }
}
