﻿using SchemeControlsLib.Base;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SchemeControlsLib.Controls.Core.Controls
{
    public class MergingControl : ObservableObject, IMergingControl
    {
        private bool _isRemoving = false;
        private bool _isAdding = false;
        private SchemeCanvas _schemeCanvas = null;
        private EditableControlBase _controlParent;

        private readonly object _MergedControls_Locker = new object();
        public List<EditableControlBase> MergedControls { get; private set; } = new List<EditableControlBase>();

        public MergingControl(EditableControlBase controlParent)
        {
            _controlParent = controlParent;
            MergedControls.Add(controlParent);
        }

        static MergingControl()
        {

        }

        private int _Old_ControlQuantity;
        private int _ControlQuantity = 1;
        public int ControlQuantity
        {
            get { return _ControlQuantity; }
            set
            {
                _Old_ControlQuantity = _ControlQuantity;
                _ControlQuantity = value;
                NotifyPropertyChanged("ControlQuantity");
                UpdateControls();
            }
        }

        public void UpdateControls()
        {
            if (_schemeCanvas == null && _controlParent.Parent is SchemeCanvas schemeCanvas)
            {
                _schemeCanvas = schemeCanvas;
            }

            if (ControlQuantity != 0)
            {
                // Usunięcie kontrolek
                if (_Old_ControlQuantity > ControlQuantity)
                {
                    var quantityToDelete = _Old_ControlQuantity - ControlQuantity;
                    var controlsToDelete = new List<EditableControlBase>(MergedControls.Skip(Math.Max(0, MergedControls.Count() - quantityToDelete)));
                    foreach (var controlToDelete in controlsToDelete)
                    {
                        this.Remove(controlToDelete);
                    }
                }
                // Dodanie kontrolek
                else if (_Old_ControlQuantity < ControlQuantity)
                {
                    _isAdding = true; // flaga blokująca
                    if (_controlParent != null)
                    {
                        var quantityToAdd = ControlQuantity - _Old_ControlQuantity;
                        for (int i = 0; i < quantityToAdd; i++)
                        {
                            var ctorParameters = new List<object>();
                            ctorParameters.Add(this);
                            var control = ActivatorUtilities.CreateInstance(_controlParent.GetType(), ctorParameters) as EditableControlBase;
                            if (control is IControlLoadedEvent controlLoadedEvent)
                                controlLoadedEvent.AddLoadedEvent();
                            this.AddControl(control, _controlParent.Name + "_" + quantityToAdd + i + 1);
                            control.SetPosition(_controlParent.X_Position + _controlParent.Width * (i + _Old_ControlQuantity), _controlParent.Y_Position);
                            control.Width = _controlParent.Width;
                            control.Height = _controlParent.Height;
                        }
                    }
                    _isAdding = false;
                }

                lock (_MergedControls_Locker)
                {
                    foreach (var mergedControl in MergedControls)
                    {
                        if (mergedControl is IControlMerger controlMerger && controlMerger.ControlQuantity != ControlQuantity)
                            controlMerger.ControlQuantity = ControlQuantity;
                    }
                }
            }
        }    

        public void AddControl(EditableControlBase editableControlBase, string controlName)
        {
            if(_schemeCanvas != null)
            {
                lock(_MergedControls_Locker)
                {
                    if(!MergedControls.Contains(editableControlBase))
                        MergedControls.Add(editableControlBase);

                    _schemeCanvas.AddSchemeElement(editableControlBase, true, controlName);
                }
            }
        }

        public void Remove()
        {
            if (!_isRemoving)
            {
                _isRemoving = true;
                lock (_MergedControls_Locker)
                {
                    foreach (var editableControlBase in MergedControls)
                        editableControlBase.Remove();
                }
                MergedControls.Clear();
                _isRemoving = false;
            }
        }

        public void Remove(EditableControlBase editableControlBase)
        {
            if (!_isRemoving)
            {
                _isRemoving = true;
                lock (_MergedControls_Locker)
                {
                    if (MergedControls.Contains(editableControlBase))
                        MergedControls.Remove(editableControlBase);
                    editableControlBase.Remove();
                }
                _isRemoving = false;
            }
        }

        private bool _isMoving = false;
        public void MoveGroup(EditableControlBase editableControlBase, Point oldPosition)
        {
            if (!_isMoving && !_isRemoving && !_isAdding)
            {
                _isMoving = true;
                lock (_MergedControls_Locker)
                {
                    if (MergedControls.Contains(editableControlBase))
                    {
                        var movingControlIndex = MergedControls.IndexOf(editableControlBase);
                        var movingControl_X_Position_Shift = editableControlBase.X_Position - oldPosition.X;
                        var movingControl_Y_Position_Shift = editableControlBase.Y_Position - oldPosition.Y;

                        foreach (var mergedControl in MergedControls)
                        {
                            if (mergedControl != editableControlBase)
                                mergedControl.SetPosition(mergedControl.X_Position + movingControl_X_Position_Shift, mergedControl.Y_Position + movingControl_Y_Position_Shift);

                        }

                    }
                }
                _isMoving = false;
            }
        }
    }
}
