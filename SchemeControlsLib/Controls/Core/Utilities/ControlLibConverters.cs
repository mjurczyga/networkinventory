﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Utilities
{
    public static class ControlLibConverters
    {
        public static Brush StateToColorBrushConverter(int state, Brush sourceColor)
        {
            switch (state)
            {
                case 0:
                    return Settings.UniknowStateColor;
                case 1:
                    return Settings.OffStateColor;
                case 2:
                    return sourceColor;
                default:
                    return Brushes.Black;
            }
        }

        public static void StateToColorsConverter<T>(ref ObservableCollection<T> outSourceColors, int state, ObservableCollection<T> sourceColors) where T : struct
        {
            var result = new ObservableCollection<T>(outSourceColors);
            switch (state)
            {
                case 0:
                    AddDefaultColorIfNotExists<T>(ref outSourceColors, Settings.UniknowStateColor);
                    break;
                case 1:
                    AddDefaultColorIfNotExists<T>(ref outSourceColors, Settings.OffStateColor);
                    break;
                case 2:
                    outSourceColors = sourceColors;
                    break;
                default:
                    AddDefaultColorIfNotExists<T>(ref outSourceColors, Brushes.Black);
                    break;
            }
        }


        private static void AddDefaultColorIfNotExists<T>(ref ObservableCollection<T> outSourceColors, Brush defaultColor) where T : struct
        {
            if (outSourceColors != null && !outSourceColors.Contains(((T)(object)((SolidColorBrush)defaultColor).Color)))
            {
                var result = new ObservableCollection<T>();
                result.Add((T)(object)((SolidColorBrush)Settings.UniknowStateColor).Color);
                outSourceColors = result;
            }
        }

        public static int TrolleyConnectorToStateConverter(int trolleyState, int connectorState)
        {
            if (trolleyState == 2 && connectorState == 2)
                return 2;
            else if (trolleyState == 0 || connectorState == 0)
                return 0;
            else
                return 1;
        }
    }
}
