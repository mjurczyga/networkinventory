﻿using SchemeControlsLib.Controls.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Controls.Utilities
{
    static public class PointExt
    {
        public static bool ComparePoints(this Point point1, Point point2)
        {
            if (Math.Abs(Math.Round(point1.X, 2) - Math.Round(point2.X, 2)) < 0.5 && Math.Abs(Math.Round(point1.Y, 2) - Math.Round(point2.Y, 2)) < 0.5)
                return true;

            return false;
        }

        public static bool CompareWithDeviation(this Point point, Point pointWithDeviation, double deviation)
        {
            if (Math.Abs(point.X - pointWithDeviation.X) < deviation && Math.Abs(point.Y - pointWithDeviation.Y) < deviation)
                return true;
            else
                return false;
        }

        public static bool IsInBounds(this Point point, double left, double top, double right, double bottom)
        {
            return RectangleBounds.IsInBounds(point, left, top, right, bottom);
        }

        public static bool IsInBounds(this Point point, RectangleBounds rectangleBounds)
        {
            return RectangleBounds.IsInBounds(point, rectangleBounds.Left, rectangleBounds.Top, rectangleBounds.Right, rectangleBounds.Bottom);
        }
    }
}
