﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.Utilities
{
    public static class ValueChecker
    {
        /// <summary>
        /// Check is double value NaN, Infinity or 0
        /// </summary>
        /// <param name="value">value to check</param>
        /// <returns></returns>
        public static bool CheckDoubleNanInfOrZero(double value)
        {
            if (double.IsNaN(value) || double.IsInfinity(value) || value == 0)
                return true;

            return false;
        }
    }
}
