﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Utilities
{
    public static class MixColors
    {
        public static Brush GenerateTwoColorBrush(Brush color1, Brush color2, double ratio = 0.5)
        {
            GradientStopCollection collection = new GradientStopCollection();

            collection.Add(new GradientStop(((SolidColorBrush)color1).Color, 0));
            collection.Add(new GradientStop(((SolidColorBrush)color1).Color, ratio));
            collection.Add(new GradientStop(((SolidColorBrush)color2).Color, ratio));
            collection.Add(new GradientStop(((SolidColorBrush)color2).Color, 1.0));

            LinearGradientBrush brush = new LinearGradientBrush(collection);
            return brush;
        }

        public static Brush GenerateTwoColorBrushWithLength(IList<Brush> ColorsCollection, double length, bool computeRatio = false)
        { 
            LinearGradientBrush brush;
            GradientStopCollection collection = new GradientStopCollection();
            if (ColorsCollection != null && ColorsCollection.Count > 1 && length > 0)
            {
                double ratio = computeRatio ? 1.0 / ColorsCollection.Count : (Settings.ColorLineLength / length); // obliczenie przeskoku dla pojedynczego koloru
                double loopRatio = 0, mainLoopRatio = 0; int colorNr = 0;

                while (mainLoopRatio + ratio < 1.0)
                {
                    loopRatio = 0;
                    // przejscie po kolorach
                    for (int i = 0; i < ColorsCollection.Count; i++)
                    {
                        colorNr = i;
                        // wyjscie jesli nastepny przeskok wyjdzie poza dlugosc linii
                        if (mainLoopRatio + ratio * i + ratio > 1.0)
                            break;

                        collection.Add(new GradientStop(((SolidColorBrush)ColorsCollection[i]).Color, loopRatio + mainLoopRatio));
                        collection.Add(new GradientStop(((SolidColorBrush)ColorsCollection[i]).Color, loopRatio + mainLoopRatio + ratio));
                        // dodanie przeskoku dla pojedynczego koloru
                        loopRatio = ratio * i + ratio;
                    }
                    // dodanie do glownego licznika przeskokow dla wszystkich kolorow
                    mainLoopRatio += loopRatio;
                }
                // zmiana koloru koncowego
                colorNr = mainLoopRatio + ratio > 1.0 ? colorNr + 1 : colorNr;
                colorNr = (colorNr + 1 > ColorsCollection.Count ? 0 : colorNr);

                collection.Add(new GradientStop(((SolidColorBrush)ColorsCollection[colorNr]).Color, mainLoopRatio));
                collection.Add(new GradientStop(((SolidColorBrush)ColorsCollection[colorNr]).Color, 1.0));
            }
            else if (ColorsCollection.Count == 1)
            {
                collection.Add(new GradientStop(((SolidColorBrush)ColorsCollection[0]).Color, 0));
                collection.Add(new GradientStop(((SolidColorBrush)ColorsCollection[0]).Color, 1.0));
            }

            brush = new LinearGradientBrush(collection);
            return brush;
        }

        public static Brush GenerateTwoColorBrushWithLength(IList<Brush> ColorsCollection, Point p1, Point p2, bool computeRatio = false)
        {
            return GenerateTwoColorBrushWithLength(ColorsCollection, ComputeLineLength(p1, p2), computeRatio);
        }

        private static double ComputeLineLength(Point p1, Point p2)
        {
            return Math.Sqrt((p2.X - p1.X) * (p2.X - p1.X) + (p2.Y - p1.Y) * (p2.Y - p1.Y));
        }

        /// <summary>
        /// Porównuje dwie zawartości kolekcji nie zwracając uwagi na kolejność elementów
        /// </summary>
        /// <param name="firstCollection"></param>
        /// <param name="secondCollection"></param>
        /// <returns>Zwraca true jeśli zawartosci są takie same</returns>
        public static bool CompareTwoBrushes(IList<Brush> firstCollection, IList<Brush> secondCollection)
        {
            bool result = false;
            if (firstCollection != null && secondCollection != null && firstCollection.Count == secondCollection.Count)
                result = !firstCollection.Except(secondCollection).Any();            

            return result;
        }

        /// <summary>
        /// Sprawdza czy kolekcja pierwsza zawiera kolekcję drugą
        /// </summary>
        /// <param name="collectionToCheck"></param>
        /// <param name="brushes"></param>
        /// <returns>swraca true gdy któryś z elementów kolekcji drugiej jest w kolekcji pierwszej</returns>
        public static bool CheckIfBrushesContainsAnotherBrushes(IList<Brush> collectionToCheck, IList<Brush> brushes)
        {
            if (brushes.Any() && collectionToCheck.Any())
            {
                foreach(var brush in brushes)
                {
                    if (collectionToCheck.Contains(brush))
                        return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Sprawdza czy kolekcja pierwsza nie zawiera kolekcji drugiej
        /// </summary>
        /// <param name="collectionToCheck"></param>
        /// <param name="brushes"></param>
        /// <returns>swraca true gdy któryś z elementów kolekcji drugiej jest w kolekcji pierwszej</returns>
        public static bool CheckIfBrushesNotContainsAnotherBrushes(IList<Brush> collectionToCheck, IList<Brush> brushes)
        {
            if (brushes.Any() && collectionToCheck.Any())
            {
                foreach (var brush in brushes)
                {
                    if (!collectionToCheck.Contains(brush))
                        return true;
                }
            }

            return false;
        }


    }
}
