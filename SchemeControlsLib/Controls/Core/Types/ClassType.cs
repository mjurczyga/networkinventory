﻿using SchemeControlsLib.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.Types
{
    public abstract class ClassType : ObservableObject, IClassType
    {
        private int _TypeNumber;
        public int TypeNumber
        {
            get { return _TypeNumber; }
            private set { _TypeNumber = value; NotifyPropertyChanged("TypeNumber"); }
        }

        private string _TypeName;
        public string TypeName
        {
            get { return _TypeName; }
            private set { _TypeName = value; NotifyPropertyChanged("TypeName"); }
        }

        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; NotifyPropertyChanged("Name"); }
        }


        private string _ToolTip;
        public string ToolTip
        {
            get { return _ToolTip; }
            set { _ToolTip = value; NotifyPropertyChanged("ToolTip"); }
        }

        public abstract List<ClassType> GetClassTypes();

        public ClassType(int typeNumber, string typeName, string name, string toolTip)
        {
            TypeNumber = typeNumber;
            TypeName = typeName;
            Name = name;
            ToolTip = toolTip;
        }
    }
}
