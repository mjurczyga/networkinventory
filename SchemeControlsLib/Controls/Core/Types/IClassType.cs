﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.Types
{
    public interface IClassType
    {
        int TypeNumber { get; }
        string TypeName { get; }
        string Name { get; }
        string ToolTip { get; }
        List<ClassType> GetClassTypes();
    }
}
