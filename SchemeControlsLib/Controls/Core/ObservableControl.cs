﻿using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SchemeControlsLib.Controls.Core
{
    public abstract class ObservableControl : Control, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged; 

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            NotifyPropertyChangedExplicit(propertyName);
        }

        protected void NotifyPropertyChanged<TProperty>(Expression<Func<TProperty>> projection)
        {
            var memberExpression = (MemberExpression)projection.Body;
            NotifyPropertyChangedExplicit(memberExpression.Member.Name);
        }

        void NotifyPropertyChangedExplicit(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }
    }
}
