﻿using SchemeControlsLib.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.Core
{
    public class CollectionWithNotification<T> : ObservableObject
    {
        private readonly ObservableCollection<T> _Collection;

        public ObservableCollection<T> Collection
        {
            get { return _Collection; }
        }

        public CollectionWithNotification()
        {
            _Collection = new ObservableCollection<T>();
            _Collection.CollectionChanged += IncludeFolders_CollectionChanged;
        }

        private void IncludeFolders_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            NotifyPropertyChanged("Collection");
        }

        /// <summary>
        /// Dodaje element do kolekcji
        /// </summary>
        /// <param name="outputNode">Element do dodania</param>
        public void Add(T value)
        {
            Collection.Add(value);
        }

        public void Clear()
        {
            Collection.Clear();
        }

        /// <summary>
        /// Usuwa element z listy
        /// </summary>
        /// <param name="outputNode">Element do usunięcia</param>
        public void Delete(T value)
        {
            Collection.Remove(value);
        }
    }
}
