﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Interfaces;
using SchemeControlsLib.Controls.CoalProcessing.Types;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(SchemeTypes.CoalProcessingScheme, false, "Wyjście kontrolki - zakład przeróbczy")]
    public class CoalProcessingOutput : OutputBase, ICoalProcessingElement
    {
        #region ICoalProcessingControl
        [EditorProperty("Typ nośnika", "Zakład przeróbczy", "Typ nośnika")]
        public CarrierType CarrierType
        {
            get { return (CarrierType)GetValue(CarrierTypeProperty); }
            set { SetValue(CarrierTypeProperty, value); }
        }

        public static readonly DependencyProperty CarrierTypeProperty =
            DependencyProperty.Register("CarrierType", typeof(CarrierType), typeof(CoalProcessingOutput), new PropertyMetadata(CarrierType_PropertyChanged));

        private static void CarrierType_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is CoalProcessingOutput coalProcessingOutputNode)
            {

            }
        }
        #endregion ICoalProcessingControl

        static CoalProcessingOutput()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CoalProcessingOutput), new FrameworkPropertyMetadata(typeof(CoalProcessingOutput)));
        }

        public CoalProcessingOutput(FrameworkElement controlParent) : base(controlParent)
        {
            if (controlParent is CoalProcessingControlBase coalProcessingControlBase)
                this.CarrierType = coalProcessingControlBase.CarrierType;

            this.ConstructorMethods();
        }
        public CoalProcessingOutput()
        {
            this.ConstructorMethods();
        }

        private void ConstructorMethods()
        {
            DependencyPropertyDescriptor.FromProperty(CoalProcessingOutput.CarrierTypeProperty, typeof(CoalProcessingOutput)).AddValueChanged(this, CarrierType_PropertyChanged);
        }

        private void CarrierType_PropertyChanged(object sender, EventArgs e)
        {
            if(sender is CoalProcessingOutput coalProcessingOutput)
            {
                coalProcessingOutput.UpdateConnectedLinesType();
            }
        }

        protected override void UpdateConnectedLinesState(int state)
        {
            //base.UpdateConnectedLinesState(state);
            if (NodeType == NodeType.IO || NodeType == NodeType.O)
            {
                foreach (var line in this.ConnectedLines)
                {
                    if (!(this.ControlParent is ILineBreaker lineBreaker))
                    {
                        if (line is IActiveElement activeElement)
                        {
                            // dodane żeby uaktualniało stan zgodnie ze strzałkami na schemacie (strzałki od OutputNode1 do OutputNode2) 
                            if (line.OutputNode1 == this)
                                line.ChangeState(this, state);
                        }
                    }
                    //kolory przechodzą tylko przez załamanie linii
                    else if (lineBreaker != null && !lineBreaker.IsStateUpdating)
                    {
                        lineBreaker.UpdateBreakerState(line, this, state);
                    }
                }
            }

        }

        private void UpdateConnectedLinesType()
        {
            foreach(var line in ConnectedLines)
            {
                if (line is ICoalProcessingElement coalProcessingElement)
                    coalProcessingElement.CarrierType = this.CarrierType;
            }
        }
    }
}
