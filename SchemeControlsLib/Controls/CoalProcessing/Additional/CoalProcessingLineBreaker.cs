﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Interfaces;
using SchemeControlsLib.Controls.CoalProcessing.Types;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(SchemeTypes.CoalProcessingScheme, true, "Załamanie lini - zakład przeróbczy")]
    public sealed class CoalProcessingLineBreaker : CoalProcessingControlBase, IHideableElement, ILineBreaker
    {
        #region Properties wrapper
        public new double Width { get { return base.Width; } set { base.Width = value; } }
        public new double Height { get { return base.Height; } set { base.Height = value; } }
        public new string DeviceName { get { return base.DeviceName; } set { base.DeviceName = value; } }
        public new object ClickCommand { get { return base.ClickCommand; } set { base.ClickCommand = value; } }
        public new object ClickCommandParameter { get { return base.ClickCommandParameter; } set { base.ClickCommandParameter = value; } }
        public new double FontSize { get { return base.FontSize; } set { base.FontSize = value; } }
        public new Brush BorderBrush { get { return base.BorderBrush; } set { base.BorderBrush = value; } }
        public new string LabelText { get { return base.LabelText; } set { base.LabelText = value; } }
        public new Brush LabelForeground { get { return base.LabelForeground; } set { base.LabelForeground = value; } }
        public new double LabelFontSize { get { return base.LabelFontSize; } set { base.LabelFontSize = value; } }
        public new Brush LabelStroke { get { return base.LabelStroke; } set { base.LabelStroke = value; } }
        public new double LabelStrokeThickness { get { return base.LabelStrokeThickness; } set { base.LabelStrokeThickness = value; } }
        public new double LabelAngle { get { return base.LabelAngle; } set { base.LabelAngle = value; } }
        #endregion Properties wrapper

        #region dependency properties
        public bool HideElement
        {
            get { return (bool)GetValue(IsElementHiddenProperty); }
            set { SetValue(IsElementHiddenProperty, value); }
        }

        public static readonly DependencyProperty IsElementHiddenProperty =
            DependencyProperty.Register("HideElement", typeof(bool), typeof(CoalProcessingLineBreaker), new PropertyMetadata(false, HideElement_PropertyChanged));

        private static void HideElement_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is CoalProcessingLineBreaker coalProcessingLineBreaker)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal))
                {
                    if (boolVal)
                        coalProcessingLineBreaker.Visibility = Visibility.Hidden;
                    else
                        coalProcessingLineBreaker.Visibility = Visibility.Visible;
                }
            }
        }
        #endregion

        #region ILineBreaker
        private bool _IsStateUpdating = false;
        public bool IsStateUpdating
        {
            get { return _IsStateUpdating; }
            private set { _IsStateUpdating = value; NotifyPropertyChanged("IsStateUpdating"); }
        }


        public void BreakTheLine(ILine schemeLine, SchemeCanvas schemeCanvas)
        {
            if (schemeCanvas != null)
            {
                this.SetIsHitTestVisibleToOutputs(true);

                var on1 = schemeLine.OutputNode1;
                var on2 = schemeLine.OutputNode2;

                if (on1 is CoalProcessingOutput coalProcessingOutput1 && on2 is CoalProcessingOutput coalProcessingOutput2)
                {
                    var carrierType = CarrierTypes.None;
                    if (coalProcessingOutput1.CarrierType == coalProcessingOutput2.CarrierType)
                    {
                        carrierType = coalProcessingOutput1.CarrierType;
                    }
                    schemeLine.Disconnect();

                    this.HideElement = !schemeCanvas.IsEditMode;

                    //pierwsza linia                            
                    var newSchemeLine1 = new CoalProcessingLine() { CarrierType = carrierType };
                    newSchemeLine1.IsArrowed = schemeLine.IsArrowed;
                    newSchemeLine1.IsDashed = schemeLine.IsDashed;
                    var output1 = this.AddOutput(this.CenterPoint.X, this.CenterPoint.Y) as CoalProcessingOutput;
                    schemeCanvas.AddSchemeElement(newSchemeLine1, false, $"{output1?.Name}_TO_{coalProcessingOutput1?.Name}");
                    newSchemeLine1.Connect(on1, output1);

                    //druga linia
                    var newSchemeLine2 = new CoalProcessingLine() { CarrierType = carrierType };
                    newSchemeLine2.IsArrowed = schemeLine.IsArrowed;
                    newSchemeLine2.IsDashed = schemeLine.IsDashed;
                    schemeCanvas.AddSchemeElement(newSchemeLine2, false, $"{coalProcessingOutput2?.Name}_TO_{output1?.Name}");
                    newSchemeLine2.Connect(output1, on2);

                }
            }
        }


        private static Dictionary<string, int> _UpdatesDict = new Dictionary<string, int>();
        
        public void UpdateBreakerState(ILine schemeLine, IOutputNode outputNode, int state)
        {
            this.IsStateUpdating = true;
            _RiseUpdateStateEvent = false;          
            var thisName = this.Name;          

            // 2 - zawsze ma priorytet, jesli któraś z linii wchodzi do węzła z 2 a nowy stan to 1 to nie można wykonać zmiany stanu węzła
            bool updateNodeState = true;
            if (state != 2)            
                updateNodeState = !this.CheckIfAnyLineIncomingWith_2_State();
            

            // Zmiana stanu jeśli nowy stan to 2 lub gdy można zmienić na inny (jeśli do wężła nie wchodzą stany 2)
            if (updateNodeState)
            {
                this.DeviceState = state;
                foreach (var controlOutputNode in OutputNodes)
                {
                    controlOutputNode.ParentState = state;
                    foreach (var line in controlOutputNode.ConnectedLines)
                    {
                        if (line != schemeLine)
                        {
                            line.ChangeState(controlOutputNode, state);
                        }
                    }
                }
            }

            _RiseUpdateStateEvent = true;
            this.IsStateUpdating = false;
        }

        protected override void StateChanged(int state)
        {
            base.StateChanged(state);
        }
        #endregion ILineBreaker

        private void SetIsHitTestVisibleToOutputs(bool isHitTestVisible)
        {
            foreach (var output in OutputNodes)
            {
                if (output is Control control)
                    control.IsHitTestVisible = isHitTestVisible;
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
        }

        static CoalProcessingLineBreaker()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CoalProcessingLineBreaker), new FrameworkPropertyMetadata(typeof(CoalProcessingLineBreaker)));
        }

        public CoalProcessingLineBreaker()
        {
            this.Width = 16;
            this.Height = 16;


            DependencyPropertyDescriptor.FromProperty(CarrierTypeProperty, typeof(CoalProcessingLineBreaker)).AddValueChanged(this, CarrierType_PropertyChanged);
        }

        private void CarrierType_PropertyChanged(object sender, EventArgs e)
        {
            if (sender is CoalProcessingLineBreaker coalProcessingLineBreaker)
            {
                lock (coalProcessingLineBreaker.OutputNodes)
                {
                    foreach (var outputNode in coalProcessingLineBreaker.OutputNodes)
                    {
                        if (outputNode is ICoalProcessingElement coalProcessingElement)
                            coalProcessingElement.CarrierType = coalProcessingLineBreaker.CarrierType;
                    }
                }
            }
        }

        private bool CheckIfAnyLineIncomingWith_2_State()
        {
            var result = false;
            var name = this.Name;
            foreach(var outputNode in OutputNodes)
            {
                if(outputNode.NodeType == NodeType.I || outputNode.NodeType == NodeType.IO)
                {
                    foreach(var outputLine in outputNode.ConnectedLines)
                    {
                        if(outputLine.OutputNode1 != outputNode && outputLine.OutputNode1.ParentState == 2)
                        {
                            result = true;
                        }
                    }
                }
            }

            return result;
        }
    }
}
