﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing;
using SchemeControlsLib.Controls.CoalProcessing.Interfaces;
using SchemeControlsLib.Controls.CoalProcessing.Types;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(SchemeTypes.CoalProcessingScheme, false, "Linia przeróbki")]
    public class CoalProcessingLine : LineBase, ICoalProcessingElement
    {
        #region ICoalProcessingControl
        protected Brush ON_ElementStroke = CarrierTypes.None.Brush;
        protected Brush OFF_ElementStroke = Settings.StatusInvalidBrush;

        [EditorProperty("Typ nośnika", "Zakład przeróbczy")]
        public CarrierType CarrierType
        {
            get { return (CarrierType)GetValue(CarrierTypeProperty); }
            set { SetValue(CarrierTypeProperty, value); }
        }

        public static readonly DependencyProperty CarrierTypeProperty =
            DependencyProperty.Register("CarrierType", typeof(CarrierType), typeof(CoalProcessingLine), new PropertyMetadata(CarrierTypes.None, CarrierType_PropertyChanged));

        private static void CarrierType_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is CoalProcessingLine coalProcessingLine && e.NewValue is CarrierType carrierType)
            {
                coalProcessingLine.ON_ElementStroke = carrierType.Brush;
                coalProcessingLine.IsArrowed = carrierType.IsArrowed;
                coalProcessingLine.IsDashed = carrierType.IsDashed;
                coalProcessingLine.ElementStrokeThickness = carrierType.StrokeThickness;

                coalProcessingLine.UpdateElementStroke(coalProcessingLine.State);
            }
        }
        #endregion ICoalProcessingControl

        #region ILine
        public override void Connect(IOutputNode outputNode, IOutputNode outputNode1)
        {
            if (outputNode is CoalProcessingOutput networkOutput1 && outputNode1 is CoalProcessingOutput networkOutput2)
            {
                var networkType = CarrierTypes.None;
                if (networkOutput1.CarrierType == networkOutput2.CarrierType)
                {
                    networkType = networkOutput2.CarrierType;
                }
                this.CarrierType = networkType;

                bool result = DependencyPropertyFinder.CopyParentClickHandler(networkOutput1, this);
                if (!result)
                    DependencyPropertyFinder.CopyParentClickHandler(networkOutput2, this);

                networkOutput1.AddLine(this);
                networkOutput2.AddLine(this);

                this.OutputNode1 = networkOutput1;
                this.OutputNode2 = networkOutput2;

                this.SetValue(FrameworkElement.NameProperty, networkOutput1.Name + "_TO_" + networkOutput2.Name);

                this.UpdateConnection(outputNode, outputNode1);
            }
        }

        public override void Disconnect(bool remove = false)
        {
            if (this.Parent is SchemeCanvas schemeCanvas && this.OutputNode1 is CoalProcessingOutput networkOutput1 && this.OutputNode2 is CoalProcessingOutput networkOutput2)
            {
                //TODO: Dopisać połączeie linni 

                //jeśli linia jest połączona z NetworkLineBreaker, to usuń linie, breaker i połącz pozostałe elementy
                //var no1_CP = networkOutput1.ControlParent;
                //var no2_CP = networkOutput2.ControlParent;

                //if (no1_CP is NetworkLineBreaker networkLineBreaker1 && (!(no2_CP is NetworkLineBreaker) || no2_CP is BaseNetworkControl baseNetworkControl2))
                //{
                //}

                networkOutput1.RemoveLine(this);
                networkOutput2.RemoveLine(this);
                schemeCanvas.RemoveElementFromScheme(this);
            }
        }

        public override void BreakLine(Point menuOpenedPoint, SchemeCanvas schemeCanvas)
        {
            var coalProcessingLineBreaker = new CoalProcessingLineBreaker() { CarrierType = this.CarrierType };
            schemeCanvas.AddSchemeElement(coalProcessingLineBreaker, true);

            coalProcessingLineBreaker.SetPosition(menuOpenedPoint.X - coalProcessingLineBreaker.Width / 2.0, menuOpenedPoint.Y - coalProcessingLineBreaker.Height / 2.0);
            coalProcessingLineBreaker.BreakTheLine(this, schemeCanvas);
        }

        protected override void UpdateBackgroundBrush(int value)
        {
            if (value == 0 || value == 1)
            {
                InternalBackgroundBrush = Settings.StatusOffBrush;
            }
            else
            {
                InternalBackgroundBrush = CarrierType == null ? Brushes.GreenYellow : CarrierType.Brush;
            }
            this.UpdateElementStroke(value);
        }
        #endregion ILine 

        public bool ChangeArrows
        {
            get { return (bool)GetValue(ChangeArrowsProperty); }
            set { SetValue(ChangeArrowsProperty, value); }
        }

        public static readonly DependencyProperty ChangeArrowsProperty =
            DependencyProperty.Register("ChangeArrows", typeof(bool), typeof(CoalProcessingLine), new PropertyMetadata(false));

        private void UpdateElementStroke(int state)
        {
            switch (state)
            {
                case 0:
                    ElementStroke = Brushes.LightGray;
                    break;
                case 1:
                    ElementStroke = OFF_ElementStroke;
                    break;
                case 2:
                    ElementStroke = ON_ElementStroke;
                    break;
                default:
                    ElementStroke = Brushes.Transparent;
                    break;
            }
        }

        public CoalProcessingLine()
        {
            this.StrokeThickness = SchemeControlsLib.Controls.Settings.LinesStrokeThickness;
            this.IsArrowed = CarrierTypes.None.IsArrowed;
            this.IsDashed = CarrierTypes.None.IsDashed;
            ElementStroke = Brushes.White;
        }

        static CoalProcessingLine()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CoalProcessingLine), new FrameworkPropertyMetadata(typeof(CoalProcessingLine)));
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.UpdateElementStroke(this.State);
        }

        protected override void StateChanged(int state)
        {
            UpdateState(OutputNode2, state);
        }
    }
}
