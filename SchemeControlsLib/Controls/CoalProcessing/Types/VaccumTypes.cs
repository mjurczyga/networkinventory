﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.CoalProcessing.Types
{
    public static class VaccumTypes
    {
        private static List<VaccumType> _Sources;

        public static VaccumType Drum = new VaccumType(1, "Drum", "Bębnowa", "");
        public static VaccumType Disc = new VaccumType(2, "Disc", "Tarczowa", "");

        static VaccumTypes()
        {
            _Sources = typeof(VaccumTypes).GetFields(BindingFlags.Public | BindingFlags.Static).
            Where(f => f.FieldType == typeof(VaccumType)).
            Select(f => f.GetValue(null)).
            Cast<VaccumType>().ToList();
        }

        public static List<VaccumType> GetList()
        {
            return _Sources;
        }

        public static VaccumType GetElementByType(string typeName)
        {
            return _Sources?.Where(x => x.TypeName == typeName).FirstOrDefault();
        }
    }
}
