﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.CoalProcessing.Types.TypeConverters
{
    public class QuantityTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                if (context != null && context.Instance != null)
                {
                    if (!(context.Instance is QuantityType))
                    {
                        throw new ArgumentException("General_Expected_Type  QuantityType", "context");
                    }
                    QuantityType quantityType = (QuantityType)context.Instance;
                    return quantityType.CanSerializeToString();
                }
                return true;
            }
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value == null)
            {
                throw GetConvertFromException(value);
            }
            string text = value as string;
            if (text != null)
            {
                return QuantityType.Parse(text, context);
            }
            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType != null && value is QuantityType)
            {
                QuantityType quantityType = (QuantityType)value;
                if (destinationType == typeof(string))
                {
                    if (context != null && context.Instance != null && !quantityType.CanSerializeToString())
                    {
                        throw new NotSupportedException("Converter_ConvertToNotSupported");
                    }
                    return quantityType.ConvertToString(null, culture);
                }
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        public QuantityTypeConverter()
        {
        }
    }
}
