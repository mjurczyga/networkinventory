﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.CoalProcessing.Types
{
    public class QuantityTypes
    {
        private static List<QuantityType> _Sources;

        public static QuantityType SingleControl = new QuantityType(1, "SingleControl", "Pojedyńcze", "");
        public static QuantityType DoubleControl = new QuantityType(2, "DoubleControl", "Podwójne", "");
        public static QuantityType TrippleControl = new QuantityType(3, "TrippleControl", "Potrójne", "");

        static QuantityTypes()
        {
            _Sources = typeof(QuantityTypes).GetFields(BindingFlags.Public | BindingFlags.Static).
            Where(f => f.FieldType == typeof(QuantityType)).
            Select(f => f.GetValue(null)).
            Cast<QuantityType>().ToList();
        }

        public static List<QuantityType> GetList()
        {
            return _Sources;
        }

        public static QuantityType GetElementByType(string typeName)
        {
            return _Sources?.Where(x => x.TypeName == typeName).FirstOrDefault();
        }
    }
}
