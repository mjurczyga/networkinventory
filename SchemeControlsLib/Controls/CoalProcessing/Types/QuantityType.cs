﻿using SchemeControlsLib.Controls.CoalProcessing.Types.TypeConverters;
using SchemeControlsLib.Controls.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.CoalProcessing.Types
{
    [TypeConverter(typeof(QuantityTypeConverter))]
    public class QuantityType : ClassType
    {
        public override List<ClassType> GetClassTypes()
        {
            return QuantityTypes.GetList().ToList<ClassType>();
        }

        public QuantityType(int typeNumber, string typeName, string name, string toolTip) : base(typeNumber, typeName, name, toolTip)
        {
        }

        public override string ToString()
        {
            return this.TypeName;
        }

        public virtual bool CanSerializeToString()
        {
            return !string.IsNullOrEmpty(this.TypeName);
        }

        internal static QuantityType Parse(string value, ITypeDescriptorContext context)
        {
            return QuantityTypes.GetList().Where(x => x.ToString() == value).FirstOrDefault();
        }

        internal virtual string ConvertToString(string format, IFormatProvider provider)
        {
            return this.ToString();
        }
    }
}
