﻿using SchemeControlsLib.Controls.CoalProcessing.Types.TypeConverters;
using SchemeControlsLib.Controls.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.CoalProcessing.Types
{
    [TypeConverter(typeof(LineStyleConverter))]
    public class LineStyle : ClassType
    {
        public DoubleCollection StrokeDashArray { get; private set; }

        public override List<ClassType> GetClassTypes()
        {
            return LineStyles.GetList().ToList<ClassType>();
        }

        public LineStyle(int typeNumber, string typeName, string name, DoubleCollection strokeDashArray, string toolTip) : base(typeNumber, typeName, name, toolTip)
        {
            StrokeDashArray = strokeDashArray;
        }

        public override string ToString()
        {
            return this.TypeName;
        }

        public virtual bool CanSerializeToString()
        {
            return !string.IsNullOrEmpty(this.TypeName);
        }

        internal static LineStyle Parse(string value, ITypeDescriptorContext context)
        {
            return LineStyles.GetList().Where(x => x.ToString() == value).FirstOrDefault();
        }

        internal virtual string ConvertToString(string format, IFormatProvider provider)
        {
            return this.ToString();
        }
    }
}
