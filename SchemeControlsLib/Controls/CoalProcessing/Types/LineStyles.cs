﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.CoalProcessing.Types
{
    public static class LineStyles
    {
        private static List<LineStyle> _Sources;

        public static LineStyle Off = new LineStyle(0, "Off", "Wyłączone", null, "Brak");
        public static LineStyle Dashed = new LineStyle(1, "Dashed", "Przerywana", null, "Brak");
        public static LineStyle Solid = new LineStyle(2, "Solid", "Ciągła", null, "Brak");


        static LineStyles()
        {
            _Sources = typeof(LineStyles).GetFields(BindingFlags.Public | BindingFlags.Static).
            Where(f => f.FieldType == typeof(LineStyle)).
            Select(f => f.GetValue(null)).
            Cast<LineStyle>().ToList();
        }

        public static List<LineStyle> GetList()
        {
            return _Sources;
        }

        public static LineStyle GetElementByType(string typeName)
        {
            return _Sources?.Where(x => x.TypeName == typeName).FirstOrDefault();
        }
    }
}
