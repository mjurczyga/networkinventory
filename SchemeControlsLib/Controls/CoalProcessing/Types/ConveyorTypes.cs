﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.CoalProcessing.Types
{
    public static class ConveyorTypes
    {
        private static List<ConveyorType> _Sources; 

        public static ConveyorType None = new ConveyorType(0, "None", "Brak", "");  
        public static ConveyorType Balance = new ConveyorType(1, "Balance", "Waga", "");  
        public static ConveyorType PickingBelt = new ConveyorType(2, "PickingBelt", "Taśma przebiercza", "");  
        public static ConveyorType Scraper = new ConveyorType(3, "Scraper", "Przenośnik zgrzebłowy", "");

        static ConveyorTypes()
        {
            _Sources = typeof(ConveyorTypes).GetFields(BindingFlags.Public | BindingFlags.Static).
            Where(f => f.FieldType == typeof(ConveyorType)).
            Select(f => f.GetValue(null)).
            Cast<ConveyorType>().ToList();
        }

        public static List<ConveyorType> GetList()
        {
            return _Sources;
        }

        public static ConveyorType GetElementByType(string typeName)
        {
            return _Sources?.Where(x => x.TypeName == typeName).FirstOrDefault();
        }
    }
}
