﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.CoalProcessing.Types
{
    public static class CarrierTypes
    {
        private static List<CarrierType> _Sources;

        public static CarrierType None = new CarrierType(0, "None", "Brak", Brushes.Black, Settings.Coal_ElementStrokeThickness, false, true, "None");
        public static CarrierType Coal = new CarrierType(1, "Coal", "Węgiel", new SolidColorBrush((Color)ColorConverter.ConvertFromString("#05FF00")), Settings.Coal_ElementStrokeThickness, false, true, "Węgiel");
        public static CarrierType Sludge = new CarrierType(2, "Sludge", "Odpady", Brushes.SandyBrown, Settings.Coal_ElementStrokeThickness, false, true, "Odpady");
        public static CarrierType Water = new CarrierType(3, "Water", "Woda", Brushes.Cyan, Settings.Coal_ElementStrokeThickness, true, true, "Woda");
        public static CarrierType Air = new CarrierType(4, "Air", "Powietrze", Brushes.SkyBlue, Settings.Coal_ElementStrokeThickness, true, true, "Powietrze");
        public static CarrierType Mule = new CarrierType(4, "Mule", "Muł", Brushes.BlanchedAlmond, Settings.Coal_ElementStrokeThickness, false, true, "Muł");
        public static CarrierType RedColor = new CarrierType(5, "RedColor", "Czerwony", Brushes.DarkRed, Settings.Coal_ElementStrokeThickness, false, true, "Czerwony");


        static CarrierTypes()
        {
            _Sources = typeof(CarrierTypes).GetFields(BindingFlags.Public | BindingFlags.Static).
            Where(f => f.FieldType == typeof(CarrierType)).
            Select(f => f.GetValue(null)).
            Cast<CarrierType>().ToList();
        }

        public static List<CarrierType> GetList()
        {
            return _Sources;
        }

        public static CarrierType GetElementByType(string typeName)
        {
            return _Sources?.Where(x => x.TypeName == typeName).FirstOrDefault();
        }
    }
}
