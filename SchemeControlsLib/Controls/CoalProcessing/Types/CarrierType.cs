﻿using SchemeControlsLib.Controls.CoalProcessing.Types.TypeConverters;
using SchemeControlsLib.Controls.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.CoalProcessing.Types
{
    [TypeConverter(typeof(CarrierTypeConverter))]
    public class CarrierType : ClassType
    {
        public Brush Brush { get; private set; }

        public double StrokeThickness { get; private set; }

        public bool IsDashed { get; private set; }

        public bool IsArrowed { get; private set; }

        public override List<ClassType> GetClassTypes()
        {
            return CarrierTypes.GetList().ToList<ClassType>();
        }

        public CarrierType(int typeNumber, string typeName, string name, Brush brush, double strokeThickess, bool isDasched, bool isArrowed, string toolTip) : base(typeNumber, typeName, name, toolTip)
        {
            Brush = brush;
            StrokeThickness = strokeThickess;
            IsDashed = isDasched;
            IsArrowed = isArrowed;
        }

        public override string ToString()
        {
            return this.TypeName;
        }

        public virtual bool CanSerializeToString()
        {
            return !string.IsNullOrEmpty(this.TypeName);
        }

        internal static CarrierType Parse(string value, ITypeDescriptorContext context)
        {
            return CarrierTypes.GetList().Where(x => x.ToString() == value).FirstOrDefault();
        }

        internal virtual string ConvertToString(string format, IFormatProvider provider)
        {
            return this.ToString();
        }
    }
}
