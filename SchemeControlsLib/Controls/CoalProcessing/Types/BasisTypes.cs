﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.CoalProcessing.Types
{
    public static class BasisTypes
    {
        private static List<BasisType> _Sources;

        public static BasisType None = new BasisType(0, "None", "Brak", "");
        public static BasisType Rails = new BasisType(1, "Rails", "Tory", "");
        public static BasisType Balance = new BasisType(2, "Balance", "Waga", "");

        static BasisTypes()
        {
            _Sources = typeof(BasisTypes).GetFields(BindingFlags.Public | BindingFlags.Static).
            Where(f => f.FieldType == typeof(BasisType)).
            Select(f => f.GetValue(null)).
            Cast<BasisType>().ToList();
        }

        public static List<BasisType> GetList()
        {
            return _Sources;
        }

        public static BasisType GetElementByType(string typeName)
        {
            return _Sources?.Where(x => x.TypeName == typeName).FirstOrDefault();
        }
    }
}
