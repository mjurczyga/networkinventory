﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.CoalProcessing.Types
{
    public class CoalScreenCaps
    {
        private static List<CoalScreenCap> _Sources;

        public static CoalScreenCap SingleDeck = new CoalScreenCap(0, "SingleDeck", "Jednopokładowy", "Brak");
        public static CoalScreenCap DoubleDeck = new CoalScreenCap(1, "DoubleDeck", "Dwupokładowy", "Brak");
        public static CoalScreenCap DoubleDeckDewatering = new CoalScreenCap(2, "DoubleDeckDewatering", "Dwupokładowy odwadniający", "Brak");


        static CoalScreenCaps()
        {
            _Sources = typeof(CoalScreenCaps).GetFields(BindingFlags.Public | BindingFlags.Static).
            Where(f => f.FieldType == typeof(CoalScreenCap)).
            Select(f => f.GetValue(null)).
            Cast<CoalScreenCap>().ToList();
        }

        public static List<CoalScreenCap> GetList()
        {
            return _Sources;
        }

        public static CoalScreenCap GetElementByType(string typeName)
        {
            return _Sources?.Where(x => x.TypeName == typeName).FirstOrDefault();
        }
    }
}
