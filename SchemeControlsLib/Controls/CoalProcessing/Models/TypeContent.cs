﻿using SchemeControlsLib.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.CoalProcessing.Models
{
    public class TypeContent : ObservableObject
    {
        private object _Content;
        public object Content
        {
            get { return _Content; }
            set { _Content = value; NotifyPropertyChanged("Content"); }
        }

        private double _Left;
        public double Left
        {
            get { return _Left; }
            set { _Left = value; NotifyPropertyChanged("Left"); }
        }

        private double _Top;
        public double Top
        {
            get { return _Top; }
            set { _Top = value; NotifyPropertyChanged("Top"); }
        }

        public TypeContent() { }

        public TypeContent(object content, double left, double top)
        {
            Content = content;
            Left = left;
            Top = top;
        }
    }
}
