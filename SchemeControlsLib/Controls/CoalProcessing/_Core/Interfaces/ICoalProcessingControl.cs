﻿using SchemeControlsLib.Controls.CoalProcessing.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.CoalProcessing.Interfaces
{
    public interface ICoalProcessingElement
    {
        CarrierType CarrierType { get; set; }
    }
}
