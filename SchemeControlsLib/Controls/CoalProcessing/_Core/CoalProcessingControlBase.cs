﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Interfaces;
using SchemeControlsLib.Controls.CoalProcessing.Types;
using SchemeControlsLib.Controls.CommonControls;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using SchemeControlsLib.Base;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    public abstract class CoalProcessingControlBase : EditableControlBase, ICoalProcessingElement, ILabeledControl, IControlLoadedEvent, ISearchingElement
    {
        #region Inherited properties wrapper
        public new Brush BorderBrush { get { return base.BorderBrush; } set { base.BorderBrush = value; } }
        #endregion Inherited properties wrapper

        #region ISearchingElement
        [EditorProperty("Tekst wyszukiwania")]
        public string SearchingText
        {
            get { return (string)GetValue(SearchingTextProperty); }
            set { SetValue(SearchingTextProperty, value); }
        }

        public static readonly DependencyProperty SearchingTextProperty =
            DependencyProperty.Register("SearchingText", typeof(string), typeof(EditableControlBase), new PropertyMetadata(null));

        private Brush _SavedBrush;
        private Brush _AnimationBrush;
        private int _blinksQuantity;
        private int _blinksCounter;
        private bool _isAnimating;
        public void BlinkElement(Brush colorToBlink, int blinksQuantity, int blinkIntervalMS)
        {
            _blinksCounter = 0;
            if (!_isAnimating)
            {
                _SavedBrush = InternalBackgroundBrush;
                _AnimationBrush = colorToBlink;
                _blinksQuantity = blinksQuantity;
                _animationTimer.Animate(blinkIntervalMS, OnTimerElapsed, this.Name);
                _isAnimating = true;
            }

        }

        private AnimationTimer _animationTimer = new AnimationTimer();
        protected virtual void OnTimerElapsed()
        {
            if (_blinksCounter < _blinksQuantity)
            {
                _blinksCounter++;
                if (InternalBackgroundBrush == _AnimationBrush)
                    InternalBackgroundBrush = _SavedBrush;
                else
                    InternalBackgroundBrush = _AnimationBrush;
            }
            else
            {
                _animationTimer.CleanTimer();
                InternalBackgroundBrush = _SavedBrush;
                _isAnimating = false;
                if(this.Parent is SchemeCanvas schemeCanvas)
                {
                    schemeCanvas.RemoveFromSelectionDict(this);
                }
            }
        }
        #endregion ISearchingElement

        #region Label control base
        private LabelControl _labelControl;

        #region LabelText propdp
        [EditorProperty("Tekst odnośnika", "Label")]
        public string LabelText
        {
            get { return (string)GetValue(LabelTextProperty); }
            set { SetValue(LabelTextProperty, value); }
        }

        public static readonly DependencyProperty LabelTextProperty =
            DependencyProperty.Register("LabelText", typeof(string), typeof(CoalProcessingControlBase), new PropertyMetadata(LabelText_PropertyChanged));

        private static void LabelText_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is CoalProcessingControlBase coalProcessingControlBase)
            {
                if (!string.IsNullOrEmpty(e.NewValue?.ToString()))
                    coalProcessingControlBase.AddLabel();
                else
                    coalProcessingControlBase.RemoveLabel();

                if (!string.IsNullOrEmpty(e.NewValue?.ToString()))
                    coalProcessingControlBase.SearchingText = e.NewValue.ToString();
            }
        }
        #endregion LabelText propdp

        #region Label propdps
        [EditorProperty("Kolor tekstu odnośnika", "Label")]
        public Brush LabelForeground
        {
            get { return (Brush)GetValue(LabelForegroundProperty); }
            set { SetValue(LabelForegroundProperty, value); }
        }

        public static readonly DependencyProperty LabelForegroundProperty =
            DependencyProperty.Register("LabelForeground", typeof(Brush), typeof(CoalProcessingControlBase), new PropertyMetadata(Settings.TextForeground));

        [EditorProperty("Rozmiar tekstu odnośnika", "Label")]
        public double LabelFontSize
        {
            get { return (double)GetValue(LabelFontSizeProperty); }
            set { SetValue(LabelFontSizeProperty, value); }
        }

        public static readonly DependencyProperty LabelFontSizeProperty =
            DependencyProperty.Register("LabelFontSize", typeof(double), typeof(CoalProcessingControlBase), new PropertyMetadata(12.0, LabelFontSize_PropertyChanged));

        private static void LabelFontSize_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is CoalProcessingControlBase coalProcessingControlBase)
            {
                coalProcessingControlBase.UpdateLabelDistance();
            }
        }

        [EditorProperty("Kolor linii odnośnika", "Label")]
        public Brush LabelStroke
        {
            get { return (Brush)GetValue(LabelStrokeProperty); }
            set { SetValue(LabelStrokeProperty, value); }
        }

        public static readonly DependencyProperty LabelStrokeProperty =
            DependencyProperty.Register("LabelStroke", typeof(Brush), typeof(CoalProcessingControlBase), new PropertyMetadata(Brushes.White));

        [EditorProperty("Grubość linii odnośnika", "Lable")]
        public double LabelStrokeThickness
        {
            get { return (double)GetValue(LabelStrokeThicknessProperty); }
            set { SetValue(LabelStrokeThicknessProperty, value); }
        }

        public static readonly DependencyProperty LabelStrokeThicknessProperty =
            DependencyProperty.Register("LabelStrokeThickness", typeof(double), typeof(CoalProcessingControlBase), new PropertyMetadata(Settings.Coal_ElementStrokeThickness));

        [EditorProperty("Kąt odnośnika", "Label")]
        public double LabelAngle
        {
            get { return (double)GetValue(LabelAngleProperty); }
            set { SetValue(LabelAngleProperty, value); }
        }

        public static readonly DependencyProperty LabelAngleProperty =
            DependencyProperty.Register("LabelAngle", typeof(double), typeof(CoalProcessingControlBase), new PropertyMetadata(300.0, LabelAngle_PropertyChanged));

        private static void LabelAngle_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is CoalProcessingControlBase coalProcessingControlBase)
            {
                coalProcessingControlBase.UpdateLabelDistance();
            }
        }
        #endregion Label propdps

        #region private label methods
        private void AddLabel()
        {

            if (this.Parent is SchemeCanvas schemeCanvas && _labelControl == null)
            {
                var labeldistance = GetLabelDistance(this.LabelAngle);

                _labelControl = new LabelControl(labeldistance);
                _labelControl.SetBinding(LabelControl.TextProperty, BindingUtilities.GetBinding("LabelText", this, null));
                _labelControl.SetBinding(LabelControl.FontSizeProperty, BindingUtilities.GetBinding("LabelFontSize", this, null));
                _labelControl.SetBinding(LabelControl.ForegroundProperty, BindingUtilities.GetBinding("LabelForeground", this, null));
                _labelControl.SetBinding(LabelControl.StrokeThicknessProperty, BindingUtilities.GetBinding("LabelStrokeThickness", this, null));
                _labelControl.SetBinding(LabelControl.StrokeProperty, BindingUtilities.GetBinding("LabelStroke", this, null));
                _labelControl.SetBinding(LabelControl.LineAngleProperty, BindingUtilities.GetBinding("LabelAngle", this, null));
                _labelControl.SetBinding(LabelControl.LineStartPointProperty, BindingUtilities.GetBinding("CenterPoint", this, null, BindingMode.OneWay));

                schemeCanvas.AddSchemeElement(_labelControl, false, this.Name + "_Label");

            }
        }

        public void RemoveLabel()
        {
            if (this.Parent is SchemeCanvas schemeCanvas && _labelControl != null)
            {
                BindingUtilities.ClearBindings(_labelControl);
                schemeCanvas.RemoveElementFromScheme(_labelControl);
            }
        }

        protected virtual void UpdateLabelDistance()
        {
            if (_labelControl != null)
            {
                _labelControl.LabelDistance = GetLabelDistance(this.LabelAngle);
            }
        }

        protected virtual double GetLabelDistance(double labelAngle)
        {
            if (MathExt.IsSinusSignNegative(labelAngle))
                return this.Height / 2.0 + 0.5 * LabelFontSize;
            else
                return this.Height / 2.0 + 1.5 * LabelFontSize;
        }
        #endregion private label methods

        #endregion Label control

        #region Control parts
        protected static readonly string _controlGraphic_PART = "_controlGraphic_PART";
        protected Grid _controlGraphic_PART_Grid;
        #endregion Control parts

        #region control protected 
        protected bool _LockDimensionChanged = false;
        #endregion control protected 

        #region ICoalProcessingControl
        protected Brush ON_ElementStroke;
        protected Brush OFF_ElementStroke;

        [EditorProperty("Typ nośnika", "Właściwości podstawowe")]
        public CarrierType CarrierType
        {
            get { return (CarrierType)GetValue(CarrierTypeProperty); }
            set { SetValue(CarrierTypeProperty, value); }
        }

        public static readonly DependencyProperty CarrierTypeProperty =
            DependencyProperty.Register("CarrierType", typeof(CarrierType), typeof(CoalProcessingControlBase), new PropertyMetadata(CarrierTypes.None, CarrierType_PropertyChanged));

        private static void CarrierType_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is CoalProcessingControlBase coalProcessingControlBase && e.NewValue is CarrierType carrierType)
            {
                coalProcessingControlBase.ON_ElementStroke = carrierType.Brush;
                coalProcessingControlBase.ElementStrokeThickness = carrierType.StrokeThickness;

                coalProcessingControlBase.UpdateElementStroke(coalProcessingControlBase.State);
            }
        }
        #endregion ICoalProcessingControl

        /// <summary>
        /// metoda na wrór invalidateVisual. Rysuje na nowo całą kontrolkę - w motywie kontrolki musi być grid o nazwie "_controlGraphic_PART"
        /// </summary>
        public virtual void UpdateGraphic()
        {
            if (_controlGraphic_PART_Grid != null)
            {
                BindingUtilities.ClearBindings(_controlGraphic_PART_Grid);
                _controlGraphic_PART_Grid.Children.Clear();
            }
            this.UpdateLabelDistance();

            this.UpdateDefaultOutputsPosition();
        }

        #region base class overrides
        /// <summary>
        /// Metoda dodająca wyjscie z kontrolki
        /// </summary>
        /// <param name="x_Position"></param>
        /// <param name="y_Position"></param>
        /// <returns></returns>
        public override IOutputWithLines AddOutput(double x_Position, double y_Position, bool checkBounds = true)
        {
            CoalProcessingOutput result = null;
            if (OutputBounds != null && !OutputBounds.IsInBounds(x_Position, y_Position) && checkBounds)
            {
                //INFO - Wyjście nie jest w granicy kontrolki
            }
            else
            {
                if (this.Parent is SchemeCanvas schemeCanvas)
                {
                    result = new CoalProcessingOutput(this);
                    result.CheckBounds = checkBounds;
                    result.SetPosition(x_Position - result.Width / 2.0, y_Position - result.Height / 2.0);
                    //result.SetPosition(x_Position, y_Position);
                    OutputNodes.Add(result);
                    schemeCanvas.AddSchemeElement(result, false, this.Name + "_X" + OutputNodes.Count());
                    //result.CheckBounds = true;

                }
            }
            return result;
        }

        public override void RemoveOutput(IOutputWithLines networkOutput)
        {
            base.RemoveOutput(networkOutput);

            if (_DefaultOutputs.ContainsKey(networkOutput))
                _DefaultOutputs.Remove(networkOutput);
        }

        private double _ElementStrokeThickness = Settings.Coal_ElementStrokeThickness;
        public override double ElementStrokeThickness
        {
            get { return _ElementStrokeThickness; }
            protected set { _ElementStrokeThickness = value; NotifyPropertyChanged("ElementStrokeThickness"); }
        }
        private Brush _ElementStroke = Settings.Coal_ElementStroke;
        public override Brush ElementStroke
        {
            get { return _ElementStroke; }
            protected set { _ElementStroke = value; NotifyPropertyChanged("ElementStroke"); }
        }
        #endregion base class overrides

        public CoalProcessingControlBase()
        {
            this.Constructor();
        }

        public void AddLoadedEvent()
        {
            this.Loaded += CoalProcessingControlBase_Loaded;
        }

        private void Constructor()
        {
            this.ElementStrokeThickness = Settings.Coal_ElementStrokeThickness;
            this.ElementStroke = Settings.Coal_ElementStroke;

            this.OFF_ElementStroke = Settings.Coal_ElementStroke;
            this.ON_ElementStroke = CarrierTypes.None.Brush;

            DependencyPropertyDescriptor.FromProperty(CoalProcessingControlBase.HeightProperty, typeof(CoalProcessingControlBase)).AddValueChanged(this, HeightProperty_PropertyChanged);
            DependencyPropertyDescriptor.FromProperty(CoalProcessingControlBase.WidthProperty, typeof(CoalProcessingControlBase)).AddValueChanged(this, WidthProperty_PropertyChanged);
        }

        #region property changed methods
        private void WidthProperty_PropertyChanged(object sender, EventArgs e)
        {

            if (sender is CoalProcessingControlBase coalProcessingControlBase)
            {
                if (!coalProcessingControlBase._LockDimensionChanged)
                {
                    coalProcessingControlBase.ControlSizeChanged();
                    coalProcessingControlBase.UpdateGraphic();
                    coalProcessingControlBase.UpdateDefaultOutputsPosition();
                }
            }
        }

        private void HeightProperty_PropertyChanged(object sender, EventArgs e)
        {
            if (sender is CoalProcessingControlBase coalProcessingControlBase)
            {
                if (!coalProcessingControlBase._LockDimensionChanged)
                {
                    coalProcessingControlBase.ControlSizeChanged();
                    coalProcessingControlBase.UpdateGraphic();
                    coalProcessingControlBase.UpdateDefaultOutputsPosition();
                }
            }
        }

        protected virtual void ControlSizeChanged() { }
        #endregion property changed methods

        #region State methods
        protected override void UpdateBackgroundBrush(int value)
        {
            base.UpdateBackgroundBrush(value);
            this.UpdateElementStroke(value);
        }

        private void UpdateElementStroke(int state)
        {
            switch (state)
            {
                case 0:
                case 1:
                    ElementStroke = OFF_ElementStroke;
                    break;
                case 2:
                    ElementStroke = ON_ElementStroke;
                    break;
                default:
                    ElementStroke = Brushes.Transparent;
                    break;
            }
        }
        #endregion State methods

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _controlGraphic_PART_Grid = this.GetTemplateChild(_controlGraphic_PART) as Grid;

            this.UpdateGraphic();
        }

        #region IEditable overrides
        public override void Remove()
        {
            this.RemoveLabel();
            base.Remove();
            _DefaultOutputs.Clear();
        }
        #endregion IEditable overrides

        #region default outputs
        /// <summary>
        /// Lista domyślnych wyjść kontrolki, Func<Point> - wzór matematyczny na wyliczenie pozycji
        /// </summary>
        protected Dictionary<IOutputWithLines, Func<Point>> _DefaultOutputs = new Dictionary<IOutputWithLines, Func<Point>>();

        protected virtual void UpdateDefaultOutputsPosition()
        {
            foreach (var defaultOutput in _DefaultOutputs)
            {
                if (defaultOutput.Key is OutputBase outputBase)
                {
                    var point = defaultOutput.Value.Invoke();

                    outputBase.SetPosition(point.X - outputBase.Width / 2.0, point.Y - outputBase.Height / 2.0);
                }

            }
        }

        private void CoalProcessingControlBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.AddDefaultOutputs();
        }

        protected virtual void AddDefaultOutputs()
        { }

        public IOutputWithLines AddDefaultOutput(Func<Point> outputCoordinateMathFormula, NodeType nodeType, bool checkBounds = true)
        {
            if (!_DefaultOutputs.ContainsValue(outputCoordinateMathFormula))
            {
                var outputCoordinate = outputCoordinateMathFormula();
                var output = this.AddOutput(outputCoordinate.X, outputCoordinate.Y, checkBounds);
                if (output != null)
                {
                    output.NodeType = nodeType;
                    _DefaultOutputs.Add(output, outputCoordinateMathFormula);
                    return output;
                }
            }
            return null;
        }

        public void RemoveDefaultOutputByMathFormula(Func<Point> outputCoordinateMathFormula)
        {
            if (_DefaultOutputs.ContainsValue(outputCoordinateMathFormula))
            {
                IOutputWithLines outputNode = _DefaultOutputs.Where(x => x.Value == outputCoordinateMathFormula).FirstOrDefault().Key;
                this.RemoveOutput(outputNode);
            }
        }
        #endregion default outputs       
    }
}

