﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Types;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(EnumTypes.SchemeTypes.CoalProcessingScheme, true, "Zbiornik cieczy")]
    public class Tank : CoalProcessingControlBase
    {
        #region private statics
        private static double _rectangleHeightRatio = 10.0 / 40.0;
        #endregion

        #region IsBigTank
        [EditorProperty("Duży zbiornik?", "Układ")]
        public bool IsBigTank
        {
            get { return (bool)GetValue(IsBigTankProperty); }
            set { SetValue(IsBigTankProperty, value); }
        }

        public static readonly DependencyProperty IsBigTankProperty =
            DependencyProperty.Register("IsBigTank", typeof(bool), typeof(Tank), new PropertyMetadata(false, IsBigTank_PropertyChanged));

        private static void IsBigTank_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is Tank tank)
            {
                tank.UpdateGraphic();
            }
        }
        #endregion IsBigTank

        static Tank()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Tank), new FrameworkPropertyMetadata(typeof(Tank)));
        }

        public Tank()
        {
            CarrierType = CarrierTypes.Water;
        }

        #region Graphic
        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic());
            }
        }

        private FrameworkElement GetGraphic()
        {
            Canvas canvas = new Canvas();
            double height = this.Height;
            double width = this.Width;
            double _controlStrokeThickness = this.ElementStrokeThickness;

            double rectangleLength = _rectangleHeightRatio * height;
            if(IsBigTank)
            {
                rectangleLength = Math.Min(height, 2 * rectangleLength);
            }

            PointCollection points = new PointCollection();
            points.Add(new Point(_controlStrokeThickness / 2, 0));
            points.Add(new Point(_controlStrokeThickness / 2, rectangleLength));
            points.Add(new Point(width / 2, height - _controlStrokeThickness / 2));
            points.Add(new Point(width - _controlStrokeThickness / 2, rectangleLength));
            points.Add(new Point(width - _controlStrokeThickness / 2, 0));

            Polyline polyline = new Polyline { StrokeThickness = _controlStrokeThickness, Points = points };
            polyline.SetBinding(Polyline.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            PointCollection pointsFill = new PointCollection();
            pointsFill.Add(new Point(0, height / rectangleLength));
            pointsFill.Add(new Point(0, rectangleLength));
            pointsFill.Add(new Point(width / 2, height));
            pointsFill.Add(new Point(width, rectangleLength));
            pointsFill.Add(new Point(width, height / rectangleLength));

            Polygon polygon = new Polygon { StrokeThickness = _controlStrokeThickness, Points = pointsFill };
            polygon.SetBinding(Polygon.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
            canvas.Children.Add(polygon);


            canvas.Children.Add(polyline);
            return canvas;
        }
        #endregion Graphic

        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(OutputMathFormula, NodeType.O, true);
            AddDefaultOutput(Output1MathFormula, NodeType.O, true);

            AddDefaultOutput(InputMathFormula, NodeType.I, true);
            AddDefaultOutput(Input1MathFormula, NodeType.I, true);
        }

        private Point InputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + 4 * this.ElementStrokeThickness, 2), Math.Round(this.Y_Position, 2));
        }

        private Point Input1MathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width - 4 * this.ElementStrokeThickness, 2), Math.Round(this.Y_Position, 2));
        }

        private Point OutputMathFormula()
        {
            Point outPoint = new Point();

            double height = this.Height;
            double width = this.Width;

            double rectangleLength = _rectangleHeightRatio * height; // straight line length
            if (IsBigTank)
            {
                rectangleLength = Math.Min(height, 2 * rectangleLength);
            }

            double b = height - rectangleLength;  // vertical triangle line length
            double a = width / 2.0;   // horizontal triangle line length
            double tg = a / b;  // tangens

            double c = width / 6.0;   // small triangle line length

            outPoint.X = this.X_Position + (width / 2.0) - c;   // x coordinate (is left)
            outPoint.Y = this.Y_Position + height - c / tg;    // y coordinate
            return outPoint;
        }


        private Point Output1MathFormula()
        {
            Point outPoint = new Point();

            double height = this.Height;
            double width = this.Width;

            double rectangleLength = _rectangleHeightRatio * height; // straight line length
            if (IsBigTank)
            {
                rectangleLength = Math.Min(height, 2 * rectangleLength);
            }

            double b = height - rectangleLength;  // vertical triangle line length
            double a = width / 2.0;   // horizontal triangle line length
            double tg = a / b;  // tangens

            double c = width / 6.0;   // small triangle line length

            outPoint.X = this.X_Position + (width / 2) + c;   // x coordinate (right)
            outPoint.Y = this.Y_Position + height - c / tg;    // y coordinate
            return outPoint;
        }
        #endregion default IO
    }
}
