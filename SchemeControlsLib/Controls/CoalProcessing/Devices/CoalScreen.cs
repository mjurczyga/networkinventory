﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Types;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Shapes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(SchemeTypes.CoalProcessingScheme, true, "Przesiewacz")]
    public class CoalScreen : CoalProcessingControlBase
    {
        #region privte static
        private static double _triangleHeightRatio = 25.0 / 40.0;
        private static double _rectangleHeightRatio = 15.0 / 40.0;
        private static double _doubleCapHeightPercent = ((_triangleHeightRatio + 2.0 * _rectangleHeightRatio) * 100.0) / (_triangleHeightRatio + _rectangleHeightRatio); 
        // procent kontrolki z podwójnym "daszkiem" (że jest to jakieś 133% rozmiaru wysokości pojedyńczej kontrolki)
        #endregion

        #region ScreenCap 
        [EditorProperty("Pokłady", "Układ")]
        public CoalScreenCap CoalScreenCap
        {
            get { return (CoalScreenCap)GetValue(CoalScreenCapProperty); }
            set { SetValue(CoalScreenCapProperty, value); }
        }

        public static readonly DependencyProperty CoalScreenCapProperty =
            DependencyProperty.Register("CoalScreenCap", typeof(CoalScreenCap), typeof(CoalScreen), new PropertyMetadata(CoalScreenCaps.SingleDeck, CoalScreenCap_PropertyChanged));

        private static void CoalScreenCap_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is CoalScreen coalScreen && e.NewValue is CoalScreenCap newCoalScreenCap && e.OldValue is CoalScreenCap oldCoalScreenCap)
            {
                coalScreen.SetDefaultSize(coalScreen.CoalScreenType, oldCoalScreenCap);
                coalScreen.UpdateGraphic();
                coalScreen.AddDefaultOutputs();
            }
        }
        #endregion ScreenCap

        #region Typ przesiewacza
        [EditorProperty("Typ przesiewacza", "Układ")]
        public QuantityType CoalScreenType
        {
            get { return (QuantityType)GetValue(CoalScreenTypeProperty); }
            set { SetValue(CoalScreenTypeProperty, value); }
        }

        public static readonly DependencyProperty CoalScreenTypeProperty =
            DependencyProperty.Register("CoalScreenType", typeof(QuantityType), typeof(CoalScreen), new PropertyMetadata(QuantityTypes.SingleControl, CoalScreenType_PropertyChanged));

        private static void CoalScreenType_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is CoalScreen coalScreen && e.NewValue is QuantityType newQuantityType && e.OldValue is QuantityType oldQuantityType)
            {
                coalScreen.SetDefaultSize(oldQuantityType, coalScreen.CoalScreenCap);
                coalScreen.UpdateGraphic();
                coalScreen.AddDefaultOutputs();
            }
        }
        #endregion Typ przesiewacza

        static CoalScreen()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CoalScreen), new FrameworkPropertyMetadata(typeof(CoalScreen)));
        }

        public CoalScreen()
        {

        }

        public override void OnApplyTemplate()
        {
            this.SetDefaultSize(this.CoalScreenType, this.CoalScreenCap);
            base.OnApplyTemplate();
        }

        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic(_DefaultSize));
            }
        }

        #region control UI methods
        #region Control size
        protected override void ControlSizeChanged()
        {
            base.ControlSizeChanged();
            this.SetDefaultSize(this.CoalScreenType, this.CoalScreenCap);
        }

        private Size _DefaultSize = new Size();
        private void SetDefaultSize(QuantityType oldQuantityType, CoalScreenCap oldCoalScreenCap)
        {
            double width = this.Width / oldQuantityType.TypeNumber;

            double height = this.Height;
            if (oldCoalScreenCap != CoalScreenCaps.SingleDeck)
            {
                height += this.ElementStrokeThickness;
                height = (height * 100.0) / _doubleCapHeightPercent;
            }

            _DefaultSize = new Size(width, height);
        }

        private void SetControlSize(Size size)
        {
            if (this.Width != size.Width)
                this.Width = size.Width;
            if (this.Height != size.Height)
                this.Height = size.Height;
        }
        #endregion Control size

        #region Control drawing
        private FrameworkElement GetGraphic(Size singleControlSize)
        {
            Canvas shapeCanvas = new Canvas();

            var elementStrokeThickness = this.ElementStrokeThickness;

            double computedDefaultHeight = singleControlSize.Height;
            double computedDefaultWidth = singleControlSize.Width;

            double rectangleHeight = singleControlSize.Height * _rectangleHeightRatio;
            double secondRectangleCanvsTop = 0;
            if (CoalScreenCap != CoalScreenCaps.SingleDeck)
                secondRectangleCanvsTop = singleControlSize.Height * _rectangleHeightRatio - elementStrokeThickness;
            double triangleHeight = singleControlSize.Height * _triangleHeightRatio;
            double triangleCanvasTop = singleControlSize.Height * _rectangleHeightRatio + secondRectangleCanvsTop;

            Triangle triangle = new Triangle() { Width = singleControlSize.Width, Height = triangleHeight, StrokeThickness = elementStrokeThickness, TriangleOrientation = Triangle.Orientation.S };
            triangle.SetBinding(Triangle.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            triangle.SetBinding(Triangle.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
            Canvas.SetTop(triangle, triangleCanvasTop);
            shapeCanvas.Children.Add(triangle);


            if (CoalScreenType == QuantityTypes.DoubleControl || CoalScreenType == QuantityTypes.TrippleControl)
            {
                Triangle triangle1 = new Triangle() { Width = singleControlSize.Width, Height = triangleHeight, StrokeThickness = elementStrokeThickness, TriangleOrientation = Triangle.Orientation.S };
                triangle1.SetBinding(Triangle.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
                triangle1.SetBinding(Triangle.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
                Canvas.SetTop(triangle1, triangleCanvasTop);
                Canvas.SetLeft(triangle1, singleControlSize.Width);
                shapeCanvas.Children.Add(triangle1);
                computedDefaultWidth += singleControlSize.Width;
            }

            if (CoalScreenType == QuantityTypes.TrippleControl)
            {
                Triangle triangle1 = new Triangle() { Width = singleControlSize.Width, Height = triangleHeight, StrokeThickness = elementStrokeThickness, TriangleOrientation = Triangle.Orientation.S };
                triangle1.SetBinding(Triangle.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
                triangle1.SetBinding(Triangle.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
                Canvas.SetTop(triangle1, triangleCanvasTop);
                Canvas.SetLeft(triangle1, 2 * singleControlSize.Width);
                shapeCanvas.Children.Add(triangle1);
                computedDefaultWidth += singleControlSize.Width;
            }

            shapeCanvas.Children.Add(this.DrawRectangle(new Size(computedDefaultWidth, rectangleHeight), elementStrokeThickness));
            if (CoalScreenCap == CoalScreenCaps.DoubleDeck)
            {
                var secondRectangle = this.DrawRectangle(new Size(computedDefaultWidth, rectangleHeight), elementStrokeThickness);
                Canvas.SetTop(secondRectangle, secondRectangleCanvsTop);
                shapeCanvas.Children.Add(secondRectangle);
                computedDefaultHeight += rectangleHeight - elementStrokeThickness;
            }
            else if(CoalScreenCap == CoalScreenCaps.DoubleDeckDewatering)
            {
                var secondRectangle = this.DrawRectangleWithTriangle(new Size(computedDefaultWidth, rectangleHeight), elementStrokeThickness);
                Canvas.SetTop(secondRectangle, secondRectangleCanvsTop);
                shapeCanvas.Children.Add(secondRectangle);
                computedDefaultHeight += rectangleHeight - elementStrokeThickness;
            }


            SetControlSize(new Size(computedDefaultWidth, computedDefaultHeight));

            return shapeCanvas;
        }

        private FrameworkElement DrawRectangle(Size rectangleSize, double elementStrokeThickness)
        {
            Canvas rectangleCanvas = new Canvas();

            Rectangle rectangle = new Rectangle();
            rectangle.Height = rectangleSize.Height;
            rectangle.Width = rectangleSize.Width;
            rectangle.StrokeThickness = elementStrokeThickness;
            rectangle.SetBinding(Rectangle.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            rectangle.SetBinding(Rectangle.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
            rectangleCanvas.Children.Add(rectangle);

            Line line = new Line();
            line.X1 = 0; line.Y1 = rectangleSize.Height / 2.0;
            line.X2 = rectangleSize.Width; line.Y2 = rectangleSize.Height / 2.0;
            line.StrokeDashArray = new DoubleCollection() { 3, 3 };
            line.StrokeThickness = elementStrokeThickness / 2.0;
            line.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            rectangleCanvas.Children.Add(line);

            return rectangleCanvas;
        }

        private FrameworkElement DrawRectangleWithTriangle(Size rectangleSize, double elementStrokeThickness)
        {
            Canvas rectangleCanvas = new Canvas();
            double triangleStrokeThickness = elementStrokeThickness / 2.0;

            Rectangle rectangle = new Rectangle() { Height = rectangleSize.Height, Width = rectangleSize.Width, StrokeThickness = elementStrokeThickness };
            rectangle.SetBinding(Rectangle.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            rectangle.SetBinding(Rectangle.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
            rectangleCanvas.Children.Add(rectangle);

            double triangleH = Math.Abs(rectangleSize.Height - 5.0 * elementStrokeThickness);
            double triangleA = (2.0 * triangleH) / Math.Sqrt(3);
            double distanceBetweenTriangles = triangleA / 2.0;

            int trianglesQuantity = (int)Math.Floor((rectangleSize.Width - 2 * elementStrokeThickness) / (triangleA + distanceBetweenTriangles));
            double leftCanvas = (rectangleSize.Width - ((trianglesQuantity * (triangleA + distanceBetweenTriangles)) - distanceBetweenTriangles)) / 2.0;
            double topCanvas = 2.5 * elementStrokeThickness;


            for (int i = 0; i <= trianglesQuantity - 1; i++)
            {
                var triangle = new Triangle { Fill = Brushes.Black, Width = triangleA, Height = triangleH, TriangleOrientation = Triangle.Orientation.S };
                triangle.SetBinding(Rectangle.FillProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
                Canvas.SetLeft(triangle, leftCanvas);
                Canvas.SetTop(triangle, topCanvas);
                rectangleCanvas.Children.Add(triangle);
                leftCanvas += triangleA + distanceBetweenTriangles;
            }

            return rectangleCanvas;
        }
        #endregion Control drawing
        #endregion control UI methods
        protected override void AddDefaultOutputs()
        {
            if (CoalScreenType == QuantityTypes.SingleControl)
            {
                AddDefaultOutput(OutputMathFormula, NodeType.O, true);
                RemoveDefaultOutputByMathFormula(Output1MathFormula);
                RemoveDefaultOutputByMathFormula(Output2MathFormula);
            }
            else if (CoalScreenType == QuantityTypes.DoubleControl)
            {
                AddDefaultOutput(Output1MathFormula, NodeType.O, true);
                AddDefaultOutput(Output1MathFormula, NodeType.O, true);
                RemoveDefaultOutputByMathFormula(Output2MathFormula);
            }
            else if (CoalScreenType == QuantityTypes.TrippleControl)
            {
                AddDefaultOutput(Output1MathFormula, NodeType.O, true);
                AddDefaultOutput(Output1MathFormula, NodeType.O, true);
                AddDefaultOutput(Output2MathFormula, NodeType.O, true);
            }


            // Dodanie wejść
            AddDefaultOutput(InputMathFormula, NodeType.I, true);
            if (CoalScreenCap == CoalScreenCaps.SingleDeck)
            {
                AddDefaultOutput(Input1MathFormula, NodeType.I, true);
                AddDefaultOutput(Input2MathFormula, NodeType.I, true);
                RemoveDefaultOutputByMathFormula(Input3MathFormula);
                RemoveDefaultOutputByMathFormula(Input4MathFormula);
            }
            if (CoalScreenCap != CoalScreenCaps.SingleDeck)
            {
                AddDefaultOutput(Input1MathFormula, NodeType.I, true);
                AddDefaultOutput(Input2MathFormula, NodeType.I, true);
                AddDefaultOutput(Input3MathFormula, NodeType.I, true);
                AddDefaultOutput(Input4MathFormula, NodeType.I, true);
            }
        }

        #region math formulas
        #region output formulas
        // wyjście 1
        private Point OutputMathFormula()
        {
            if(CoalScreenType == QuantityTypes.SingleControl)
                return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position + this.Height));
            else if (CoalScreenType == QuantityTypes.DoubleControl)
                return new Point(Math.Round(this.X_Position + this.Width / 4.0, 2), Math.Round(this.Y_Position + this.Height));
            else
                return new Point(Math.Round(this.X_Position + this.Width / 6.0, 2), Math.Round(this.Y_Position + this.Height));
        }

        private Point Output1MathFormula()
        {
            if (CoalScreenType == QuantityTypes.DoubleControl)
                return new Point(Math.Round(this.X_Position +  3 * this.Width / 4.0, 2), Math.Round(this.Y_Position + this.Height));
            else
                return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position + this.Height));
        }

        private Point Output2MathFormula()
        {
                return new Point(Math.Round(this.X_Position + 5.0 * this.Width / 6.0, 2), Math.Round(this.Y_Position + this.Height));
        }
        #endregion output formulas

        #region input formulas
        private Point InputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + 4 * this.ElementStrokeThickness, 2), Math.Round(this.Y_Position, 2));
        }

        private Point Input1MathFormula()
        {
            var rectangleHeight = _DefaultSize.Height * _rectangleHeightRatio;

            return new Point(Math.Round(this.X_Position, 2), Math.Round(this.Y_Position + rectangleHeight / 2.0, 2));
        }

        private Point Input2MathFormula()
        {
            var rectangleHeight = _DefaultSize.Height * _rectangleHeightRatio;

            return new Point(Math.Round(this.X_Position + this.Width, 2), Math.Round(this.Y_Position + rectangleHeight / 2.0, 2));
        }

        private Point Input3MathFormula()
        {
            var rectangleHeight = _DefaultSize.Height * _rectangleHeightRatio;

            return new Point(Math.Round(this.X_Position, 2), Math.Round(this.Y_Position + 3.0 * rectangleHeight / 2.0, 2));
        }

        private Point Input4MathFormula()
        {
            var rectangleHeight = _DefaultSize.Height * _rectangleHeightRatio;

            return new Point(Math.Round(this.X_Position + this.Width, 2), Math.Round(this.Y_Position + 3.0 * rectangleHeight / 2.0, 2));
        }
        #endregion input formulas
        #endregion math formulas
    }

}
