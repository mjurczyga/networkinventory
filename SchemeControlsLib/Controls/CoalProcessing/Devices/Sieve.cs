﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Shapes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(SchemeTypes.CoalProcessingScheme, true, "Sito łukowe")]
    public class Sieve : CoalProcessingControlBase
    {
        static Sieve()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Sieve), new FrameworkPropertyMetadata(typeof(Sieve)));
        }

        public Sieve()
        {
        }

        #region Graphic
        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic());
            }
        }

        private FrameworkElement GetGraphic()
        {
            var _controlStrokeThickness = this.ElementStrokeThickness;
            var _width = this.Width;
            var _height = this.Height;

            PointCollection points = new PointCollection();
            points.Add(new Point(_controlStrokeThickness / 2, _controlStrokeThickness / 2));
            points.Add(new Point(_controlStrokeThickness / 2, _height / 2 - _controlStrokeThickness / 2));
            points.Add(new Point(2 * _width / 3, _height - _controlStrokeThickness / 2));
            points.Add(new Point(_width, _height / 2 - _controlStrokeThickness / 2));
            points.Add(new Point(_controlStrokeThickness / 2, _controlStrokeThickness / 2));
            Polygon polygon = new Polygon { StrokeThickness = _controlStrokeThickness, Points = points };

            polygon.SetBinding(Polygon.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            polygon.SetBinding(Polygon.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));


            return polygon;
        }
        #endregion Graphic
    }
}
