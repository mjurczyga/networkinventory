﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Types;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(EnumTypes.SchemeTypes.CoalProcessingScheme, true, "Pompa wodna")]
    public class Pump : CoalProcessingControlBase
    {
        static Pump()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Pump), new FrameworkPropertyMetadata(typeof(Pump)));
        }

        public Pump()
        {
            CarrierType = CarrierTypes.Water;
        }

        #region graphic
        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.DrawGraphic());
            }
        }

        private UIElement DrawGraphic()
        {
            double _width = this.Width;
            double _height = this.Height;
            double _controlStrokeThickness = this.ElementStrokeThickness;

            Canvas canvas = new Canvas { Width = _width, Height = _height };

            Ellipse ellipse = new Ellipse { StrokeThickness = _controlStrokeThickness, Height = _height, Width = _width };
            ellipse.SetBinding(Ellipse.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            ellipse.SetBinding(Ellipse.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
            canvas.Children.Add(ellipse);

            return canvas;
        }
        #endregion graphic

        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(InputMathFormula, NodeType.I, true);

            AddDefaultOutput(OutputMathFormula, NodeType.O, true);
        }

        private Point InputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width, 2), Math.Round(this.Y_Position + this.Height / 2.0, 2));
        }

        private Point OutputMathFormula()
        {
            return new Point(Math.Round(this.X_Position, 2), Math.Round(this.Y_Position + this.Height / 2.0, 2));
        }
        #endregion default IO
    }
}
