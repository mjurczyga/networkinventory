﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(EnumTypes.SchemeTypes.CoalProcessingScheme, true, "Kruszarka walcowa")]
    public class RollerCrusher : CoalProcessingControlBase
    {
        static RollerCrusher()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(RollerCrusher), new FrameworkPropertyMetadata(typeof(RollerCrusher)));
        }

        public RollerCrusher()
        {
        }

        #region Graphic
        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic());
            }
        }

        private FrameworkElement GetGraphic()
        {
            var _controlStrokeThickness = this.ElementStrokeThickness;
            var _halfControlStrokeThickness = this.ElementStrokeThickness / 1.5;
            var _width = this.Width;
            var _height = this.Height;

            Canvas canvas = new Canvas();

            Rectangle rectangle = new Rectangle { Width = _width, Height = _height, StrokeThickness = _controlStrokeThickness };
            rectangle.SetBinding(Rectangle.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            rectangle.SetBinding(Rectangle.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
            canvas.Children.Add(rectangle);

            Ellipse leftEllipse = new Ellipse { StrokeThickness = _halfControlStrokeThickness, Width = _height - 4 * _halfControlStrokeThickness, Height = _height - 4 * _halfControlStrokeThickness, Margin = new Thickness(2 * _halfControlStrokeThickness, 2 * _halfControlStrokeThickness, 0, 0) };
            leftEllipse.SetBinding(Ellipse.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Ellipse rightEllipse = new Ellipse { StrokeThickness = _halfControlStrokeThickness, Width = _height - 4 * _halfControlStrokeThickness, Height = _height - 4 * _halfControlStrokeThickness, Margin = new Thickness(_width / 2 + 2 * _halfControlStrokeThickness, 2 * _halfControlStrokeThickness, 0, 0) };
            rightEllipse.SetBinding(Ellipse.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            AddArcSegment(canvas, new Point(_width / 2 - 4.0 * _halfControlStrokeThickness, _height / 2), new Point(_width / 4, _height - 4.0 * _halfControlStrokeThickness), SweepDirection.Counterclockwise);
            AddArcSegment(canvas, new Point(_width / 2 + 4.0 * _halfControlStrokeThickness, _height / 2), new Point((3 * _width) / 4, _height - 4.0 * _halfControlStrokeThickness), SweepDirection.Clockwise);
            canvas.Children.Add(leftEllipse);
            canvas.Children.Add(rightEllipse);

            return canvas;
        }

        private void AddArcSegment(Canvas canvas, Point startPoint, Point endPoint, SweepDirection sweep)
        {
            var _controlStrokeThickness = this.ElementStrokeThickness;
            var _width = this.Width;
            var _height = this.Height;

            var arcSegment = new StreamGeometry();

            using (var gc = arcSegment.Open())
            {
                gc.BeginFigure(
                    startPoint: new Point(startPoint.X, startPoint.Y),
                    isFilled: false,
                    isClosed: false);

                gc.ArcTo(
                    point: new Point(endPoint.X, endPoint.Y),
                    size: new Size(Math.Abs(endPoint.X - startPoint.X), Math.Abs(endPoint.X - startPoint.X)),
                    rotationAngle: 0d,
                    isLargeArc: true,
                    sweepDirection: sweep,
                    isStroked: true,
                    isSmoothJoin: false);
            }

            var arcSegmentPath = new Path { StrokeThickness = _controlStrokeThickness / 4.0, Data = arcSegment };
            arcSegmentPath.SetBinding(Path.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            PointCollection points = new PointCollection();
            points.Add(new Point(startPoint.X, startPoint.Y + _controlStrokeThickness));
            points.Add(new Point(startPoint.X + _controlStrokeThickness / 3.0, startPoint.Y - (sweep == SweepDirection.Clockwise ? _controlStrokeThickness / 3.0 : _controlStrokeThickness)));
            points.Add(new Point(startPoint.X - _controlStrokeThickness / 3.0, startPoint.Y - (sweep == SweepDirection.Counterclockwise ? _controlStrokeThickness / 3.0 : _controlStrokeThickness)));
            points.Add(new Point(startPoint.X, startPoint.Y + _controlStrokeThickness));
            Polygon arrow = new Polygon { StrokeThickness = 0.1, Points = points };
            arrow.SetBinding(Polygon.FillProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            canvas.Children.Add(arcSegmentPath);
            canvas.Children.Add(arrow);        
        }
        #endregion Graphic


        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(InputMathFormula, NodeType.I, true);

            AddDefaultOutput(OutputMathFormula, NodeType.O, true);
        }

        private Point InputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position, 2));
        }

        private Point OutputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position + this.Height, 2));
        }
        #endregion default IO
    }
}
