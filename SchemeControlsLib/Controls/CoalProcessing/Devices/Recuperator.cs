﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(EnumTypes.SchemeTypes.CoalProcessingScheme, true, "Rekuperator")]
    public class Recuperator : CoalProcessingControlBase
    {

        static Recuperator()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Recuperator), new FrameworkPropertyMetadata(typeof(Recuperator)));
        }

        public Recuperator()
        {
        }

        #region graphic
        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic());
            }
        }

        private UIElement GetGraphic()
        {
            var _controlStrokeThickness = this.ElementStrokeThickness;
            double width = this.Width;
            double height = this.Height;

            Canvas canvas = new Canvas();
            double circleDiameter = 2 * width / 5;
            Ellipse ellipse = new Ellipse { StrokeThickness = _controlStrokeThickness, Width = circleDiameter, Height = circleDiameter, Margin = new Thickness(2 * width / 5, 0, 0, 0) };
            ellipse.SetBinding(Ellipse.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            ellipse.SetBinding(Ellipse.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));

            PointCollection points = new PointCollection();
            points.Add(new Point(_controlStrokeThickness / 2, circleDiameter / 2));
            points.Add(new Point(_controlStrokeThickness / 2, circleDiameter));
            points.Add(new Point(width / 5, circleDiameter));
            points.Add(new Point(2 * width / 5, height - _controlStrokeThickness / 2));
            points.Add(new Point(3 * width / 5, 3 * height / 4));
            points.Add(new Point(4 * width / 5, height - _controlStrokeThickness / 2));
            points.Add(new Point(width - _controlStrokeThickness / 2, circleDiameter));
            points.Add(new Point(width - _controlStrokeThickness / 2, circleDiameter / 2));

            Polygon polygon = new Polygon { StrokeThickness = _controlStrokeThickness, Points = points };
            polygon.SetBinding(Polygon.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            polygon.SetBinding(Polygon.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));

            canvas.Children.Add(polygon);
            canvas.Children.Add(ellipse);

            return canvas;
        }
        #endregion graphic

        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(OutputMathFormula, NodeType.O, true);
            AddDefaultOutput(Output1MathFormula, NodeType.O, true);
            AddDefaultOutput(Output2MathFormula, NodeType.O, true);

            AddDefaultOutput(InputMathFormula, NodeType.I, true);
        }

        private Point OutputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + 2.0 * this.Width / 5.0, 2), Math.Round(this.Y_Position + this.Height, 2));
        }

        private Point Output1MathFormula()
        {
            return new Point(Math.Round(this.X_Position + 4.0 * this.Width / 5.0, 2), Math.Round(this.Y_Position + this.Height, 2));
        }

        private Point Output2MathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width - this.ElementStrokeThickness, 2), Math.Round(this.Y_Position + this.Width / 5 - this.ElementStrokeThickness, 2));
        }

        private Point InputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width - this.ElementStrokeThickness, 2), Math.Round(this.Y_Position + this.Width / 5 - this.ElementStrokeThickness, 2));
        }
        #endregion default IO
    }
}
