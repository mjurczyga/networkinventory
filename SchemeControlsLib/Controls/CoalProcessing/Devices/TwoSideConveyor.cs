﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Shapes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Shapes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(EnumTypes.SchemeTypes.CoalProcessingScheme, true, "Taśma dwukierunkowa")]
    public class TwoSideConveyor : CoalProcessingControlBase
    {
        #region properties
        #region IsMotorRightWorking
        [EditorProperty("Praca taśmy w prawo", "Układ", "Typu bool lub int (można bindować)")]
        public object IsMotorRightWorking
        {
            get { return (object)GetValue(IsMotorRightWorkingProperty); }
            set { SetValue(IsMotorRightWorkingProperty, value); }
        }

        public static readonly DependencyProperty IsMotorRightWorkingProperty =
            DependencyProperty.Register("IsMotorRightWorking", typeof(object), typeof(TwoSideConveyor), new PropertyMetadata(false, IsMotorRightWorking_PropertyChanged));

        private static void IsMotorRightWorking_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is TwoSideConveyor twoSideConveyor )
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool newValue))
                {
                    if (newValue)
                        twoSideConveyor.MotorRight_Visiblity = Visibility.Visible;
                    else
                        twoSideConveyor.MotorRight_Visiblity = Visibility.Collapsed;
                }
                else if (int.TryParse(e.NewValue?.ToString(), out int intNewValue))
                {
                    if(intNewValue == 0)
                        twoSideConveyor.MotorRight_Visiblity = Visibility.Collapsed;
                    else
                        twoSideConveyor.MotorRight_Visiblity = Visibility.Visible;
                }
            }
        }

        private Visibility _MotorRight_Visiblity = Visibility.Collapsed;
        public Visibility MotorRight_Visiblity
        {
            get { return _MotorRight_Visiblity; }
            set { _MotorRight_Visiblity = value; NotifyPropertyChanged("MotorRight_Visiblity"); }
        }
        #endregion IsMotorRightWorking

        #region IsMotorLeftWorking
        [EditorProperty("Praca taśmy w lewo", "Układ", "Typu bool lub int (można bindować)")]
        public object IsMotorLeftWorking
        {
            get { return (object)GetValue(IsMotorLeftWorkingProperty); }
            set { SetValue(IsMotorLeftWorkingProperty, value); }
        }

        public static readonly DependencyProperty IsMotorLeftWorkingProperty =
            DependencyProperty.Register("IsMotorLeftWorking", typeof(object), typeof(TwoSideConveyor), new PropertyMetadata(false, IsMotorLeftWorking_PropertyChanged));

        private static void IsMotorLeftWorking_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is TwoSideConveyor twoSideConveyor)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool newValue))
                {
                    if (newValue)
                        twoSideConveyor.MotorLeft_Visiblity = Visibility.Visible;
                    else
                        twoSideConveyor.MotorLeft_Visiblity = Visibility.Collapsed;
                }
                else if (int.TryParse(e.NewValue?.ToString(), out int intNewValue))
                {
                    if (intNewValue == 0)
                        twoSideConveyor.MotorLeft_Visiblity = Visibility.Collapsed;
                    else
                        twoSideConveyor.MotorLeft_Visiblity = Visibility.Visible;
                }
            }
        }

        private Visibility _MotorLeft_Visiblity = Visibility.Collapsed;
        public Visibility MotorLeft_Visiblity
        {
            get { return _MotorLeft_Visiblity; }
            set { _MotorLeft_Visiblity = value; NotifyPropertyChanged("MotorLeft_Visiblity"); }
        }
        #endregion IsMotorLeftWorking
        #endregion properties

        static TwoSideConveyor()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TwoSideConveyor), new FrameworkPropertyMetadata(typeof(TwoSideConveyor)));
        }

        public TwoSideConveyor()
        {
        }

        #region Control drawings
        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.DrawConveyor());
            }
        }

        private FrameworkElement DrawConveyor()
        {
            Canvas canvas = new Canvas(); 
            ConveyorShape conveyor = new ConveyorShape()
            {
                Stroke = ElementStroke,
                StrokeThickness = ElementStrokeThickness,
                Fill = InternalBackgroundBrush,
                Width = Width,
                Height = Height,
            };
            conveyor.SetBinding(ConveyorShape.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            conveyor.SetBinding(ConveyorShape.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));

            double lineLength = Width * 3.0 / 10.0;
            Point centerOfControl = new Point(Width / 2.0, Height / 2.0);

            Ellipse ellipse = new Ellipse() { Width = 1.5 * ElementStrokeThickness, Height = 1.5 * ElementStrokeThickness };
            ellipse.SetBinding(Ellipse.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            ellipse.SetBinding(Ellipse.FillProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            Canvas.SetLeft(ellipse, centerOfControl.X - 1.5 * ElementStrokeThickness / 2.0);
            Canvas.SetTop(ellipse, centerOfControl.Y - 1.5 * ElementStrokeThickness / 2.0);

            RightAngleLine rightAngleLine_Left = new RightAngleLine()
            {
                X1 = centerOfControl.X, Y1 = centerOfControl.Y,
                X2 = centerOfControl.X - lineLength, Y2 = centerOfControl.Y,
                StrokeThickness = ElementStrokeThickness / 2.0, IsArrowOnSecondLineEnd = true
            };
            rightAngleLine_Left.SetBinding(RightAngleLine.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            rightAngleLine_Left.SetBinding(RightAngleLine.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
            rightAngleLine_Left.SetBinding(RightAngleLine.VisibilityProperty, BindingUtilities.GetBinding("MotorLeft_Visiblity", this, this.MotorLeft_Visiblity));

            RightAngleLine rightAngleLine_Right = new RightAngleLine()
            {
                X1 = centerOfControl.X, Y1 = centerOfControl.Y,
                X2 = centerOfControl.X + lineLength, Y2 = centerOfControl.Y,
                StrokeThickness = ElementStrokeThickness / 2.0, IsArrowOnSecondLineEnd = true
            };
            rightAngleLine_Right.SetBinding(RightAngleLine.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            rightAngleLine_Right.SetBinding(RightAngleLine.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
            rightAngleLine_Right.SetBinding(RightAngleLine.VisibilityProperty, BindingUtilities.GetBinding("MotorRight_Visiblity", this, this.MotorRight_Visiblity));

            canvas.Children.Add(conveyor);
            canvas.Children.Add(ellipse);
            canvas.Children.Add(rightAngleLine_Left);
            canvas.Children.Add(rightAngleLine_Right);

            return canvas;
        }
        #endregion Control drawings

        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(OutputMathFormula, NodeType.I, false);
            AddDefaultOutput(Output1MathFormula, NodeType.O, false);
            AddDefaultOutput(Output2MathFormula, NodeType.O, false);
        }

        private Point OutputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position, 2));
        }

        private Point Output1MathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Height, 2), Math.Round(this.Y_Position - 2 * this.ElementStrokeThickness, 2));
        }
        private Point Output2MathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width - this.Height, 2), Math.Round(this.Y_Position - 2 * this.ElementStrokeThickness, 2));
        }
        #endregion default IO
    }
}
