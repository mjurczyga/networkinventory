﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;
using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Models;
using SchemeControlsLib.Controls.CoalProcessing.Shapes;
using SchemeControlsLib.Controls.CoalProcessing.Types;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Shapes;
using SchemeControlsLib.Utilities;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(SchemeTypes.CoalProcessingScheme, true, "Przenośnik taśmowy")]
    public class Conveyor : CoalProcessingControlBase
    {
        #region Dependency properties
        #region ConveyorType propdp
        [EditorProperty("Typ przenośnika", "Układ")]
        public ConveyorType ConveyorType
        {
            get { return (ConveyorType)GetValue(ConveyorTypeProperty); }
            set { SetValue(ConveyorTypeProperty, value); }
        }

        public static readonly DependencyProperty ConveyorTypeProperty =
            DependencyProperty.Register("ConveyorType", typeof(ConveyorType), typeof(CoalProcessingControlBase), new PropertyMetadata(ConveyorTypes.None, ConveyorType_PropertyChanged));

        private static void ConveyorType_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Conveyor conveyor && e.NewValue is ConveyorType conveyorType)
            {
                conveyor.UpdateGraphic();
            }
        }
        #endregion ConveyorType propdp

        #region orientation
        [EditorProperty("Orientacja", "Układ")]
        public Orientation Orientation
        {       
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("Orientation", typeof(Orientation), typeof(Conveyor), new PropertyMetadata(Orientation_PropertyChanged));

        private static void Orientation_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is Conveyor conveyor)
            {
                conveyor._LockDimensionChanged = true;
                double tempHeight = conveyor.Height;
                double tempWidth = conveyor.Width;

                conveyor.Width = tempHeight;
                conveyor.Height = tempWidth;

                conveyor.UpdateGraphic();
                conveyor._LockDimensionChanged = false;
            }
        }
        #endregion orientation
        #endregion Dependency properties

        static Conveyor()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Conveyor), new FrameworkPropertyMetadata(typeof(Conveyor)));
        }

        public Conveyor()
        {
        }

        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {             
                _controlGraphic_PART_Grid.Children.Add(this.DrawConveyor());

                if (ConveyorType == ConveyorTypes.Balance)
                {
                    _controlGraphic_PART_Grid.Children.Add(this.DrawBalance());
                }
                else if (ConveyorType == ConveyorTypes.PickingBelt)
                {
                    _controlGraphic_PART_Grid.Children.Add(this.DrawSludge());
                }
                else if (ConveyorType == ConveyorTypes.Scraper)
                {
                    _controlGraphic_PART_Grid.Children.Add(this.DrawBeltFeeder());
                }
            }
        }

        #region private type drawing methods
        private FrameworkElement DrawConveyor()
        {
            ConveyorShape conveyor = new ConveyorShape()
            {
                Stroke = ElementStroke,
                StrokeThickness = ElementStrokeThickness,
                Fill = InternalBackgroundBrush,
                Width = Width,
                Height = Height,
                Orientation = Orientation,
            };
            conveyor.SetBinding(ConveyorShape.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            conveyor.SetBinding(ConveyorShape.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));

            return conveyor;
        }

        private FrameworkElement DrawSludge()
        {
            Canvas canvas = new Canvas();
            double strokeThickness = ElementStrokeThickness * 0.5;
            double triangleLength = this.Width / 5; double triangleHeight = triangleLength / Math.Sqrt(2);
            Point stPoint = new Point(this.Width / 2 - 5 * triangleLength / 4, 3 * this.Height / 5);
            Point lineStPoint = new Point(stPoint.X + triangleLength / 2, stPoint.Y + triangleHeight);
            Point lineEndPoint = new Point(stPoint.X + triangleLength / 2, stPoint.Y + triangleHeight + triangleLength);
            PointCollection points = new PointCollection();
            points.Add(stPoint);
            points.Add(new Point(stPoint.X + triangleLength, stPoint.Y));
            points.Add(new Point(lineStPoint.X, lineStPoint.Y));
            points.Add(stPoint);
            //first triangle
            Polygon triangle = new Polygon { StrokeThickness = strokeThickness, Points = points, Fill = (SolidColorBrush)(new BrushConverter().ConvertFrom("#7A7D80")) };
            triangle.SetBinding(Polygon.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            RightAngleLine line = new RightAngleLine { X1 = lineStPoint.X, Y1 = lineStPoint.Y, X2 = lineEndPoint.X, Y2 = lineEndPoint.Y, StrokeThickness = strokeThickness, IsArrowOnFirstLineEnd = true  };
            line.SetBinding(RightAngleLine.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            PointCollection points1 = new PointCollection();
            Point stPoint1 = new Point(stPoint.X + 6 * triangleLength / 4, stPoint.Y);
            Point line1StPoint = new Point(stPoint1.X + triangleLength / 2, stPoint1.Y + triangleHeight);
            Point line1EndPoint = new Point(stPoint1.X + triangleLength / 2, stPoint1.Y + triangleHeight + triangleLength);

            points1.Add(stPoint1);
            points1.Add(new Point(stPoint1.X + triangleLength, stPoint1.Y));
            points1.Add(new Point(line1StPoint.X, line1StPoint.Y));
            points1.Add(stPoint1);
            //second triangle
            Polygon triangle1 = new Polygon { StrokeThickness = strokeThickness, Points = points1, Fill = (SolidColorBrush)(new BrushConverter().ConvertFrom("#7A7D80")) };
            triangle1.SetBinding(Polygon.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            RightAngleLine line1 = new RightAngleLine { X1 = line1StPoint.X, Y1 = line1StPoint.Y, X2 = line1EndPoint.X, Y2 = line1EndPoint.Y, StrokeThickness = strokeThickness, IsArrowOnFirstLineEnd = true };
            line1.SetBinding(RightAngleLine.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            TextBlock textBlock = new TextBlock { FontSize = 6, Foreground = Brushes.White, TextDecorations = TextDecorations.Underline, Text = "ODPADY", Margin = new Thickness(this.Width / 2 - 12, lineEndPoint.Y, 0, 0), Padding = new Thickness(0, 0, 0, 0) };

            canvas.Children.Add(triangle);
            canvas.Children.Add(line);
            canvas.Children.Add(triangle1);
            canvas.Children.Add(line1);
            canvas.Children.Add(textBlock);

            return canvas;
        }

        private FrameworkElement DrawBalance()
        {
            var result = new Canvas();

            double triangleHeight = (4 * this.Height / 7) / Math.Sqrt(2);
            double maxWidth = this.Width - this.ElementStrokeThickness;
            double maxHeight = this.Height - this.ElementStrokeThickness;

            double strokeThickness = ElementStrokeThickness * 0.5;

            Line line = new Line { StrokeThickness = strokeThickness, X1 = maxWidth / 4, Y1 = 2 * maxHeight / 7 + strokeThickness, X2 = 3 * maxWidth / 4, Y2 = 2 * maxHeight / 7 + strokeThickness };
            line.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            result.Children.Add(line);

            PointCollection points = new PointCollection();
            points.Add(new Point(maxWidth / 2, 2 * maxHeight / 7 + 2 * strokeThickness));
            points.Add(new Point(maxWidth / 2 + triangleHeight / 2, triangleHeight + 2 * maxHeight / 7 + strokeThickness));
            points.Add(new Point(maxWidth / 2 - triangleHeight / 2, triangleHeight + 2 * maxHeight / 7 + strokeThickness));
            points.Add(new Point(maxWidth / 2, 2 * maxHeight / 7 + 2 * strokeThickness));
            Polygon polyline = new Polygon { StrokeThickness = strokeThickness, Points = points };
            polyline.SetBinding(Polygon.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            result.Children.Add(polyline);
            return result;
        }

        private FrameworkElement DrawBeltFeeder()
        {
            Canvas canvas = new Canvas();
           
            double _controlStrokeThickness = ElementStrokeThickness;
            double maxWidth = this.Width;
            double maxHeight = this.Height;

            double circleDiameter = this.Height;
            double basisLength = (maxWidth - circleDiameter);
            // right/top circle center
            Point rtCircleCenter = new Point(maxWidth - circleDiameter / 2, circleDiameter / 2);

            Point startTopPoint = new Point(maxHeight / 2, 0);
            Point endTopPoint = new Point(maxHeight / 2, -_controlStrokeThickness * 2);
            Point startBottomPoint = new Point(maxHeight / 2, maxHeight);
            Point endBottomPoint = new Point(maxHeight / 2, maxHeight + _controlStrokeThickness * 2);


            double circleLineDistanceAngle = 30.0;
            // Left circle angle parameters
            double leftLineAngle = Math.PI * (90.0 + circleLineDistanceAngle) / 180.0;
            // right circle angle parameters
            double rightLineAngle = Math.PI * (270.0 + circleLineDistanceAngle) / 180.0;

            if (this.Orientation == Orientation.Vertical)
            {
                circleDiameter = this.Width;
                startTopPoint.Y = endTopPoint.Y = circleDiameter / 2;
                startTopPoint.X = 0; endTopPoint.X = -_controlStrokeThickness * 2;
                startBottomPoint.Y = endBottomPoint.Y = circleDiameter / 2;
                startBottomPoint.X = maxWidth; endBottomPoint.X = maxWidth + _controlStrokeThickness * 2;

                rtCircleCenter.Y = maxHeight - circleDiameter / 2;
                rtCircleCenter.X = circleDiameter / 2;

                leftLineAngle = Math.PI * (180.0 + circleLineDistanceAngle) / 180.0;
                rightLineAngle = Math.PI * (0 + circleLineDistanceAngle) / 180.0;

                basisLength = maxHeight - circleDiameter;
            }
            //left circle points parameters
            Point leftCircleStartPoint = new Point(circleDiameter / 2 + circleDiameter / 2 * Math.Cos(leftLineAngle), 
                                                   circleDiameter / 2 + circleDiameter / 2 * Math.Sin(leftLineAngle));
            Point leftCircleEndPoint = new Point(circleDiameter / 2 + (circleDiameter / 2 + 2 * _controlStrokeThickness) * Math.Cos(leftLineAngle), 
                                                 circleDiameter / 2 + (circleDiameter / 2 + 2 * _controlStrokeThickness) * Math.Sin(leftLineAngle));
            // right circle points parameters
            Point rightCircleStartPoint = new Point(rtCircleCenter.X + circleDiameter / 2 * Math.Cos(rightLineAngle),
                                                    rtCircleCenter.Y + circleDiameter / 2 * Math.Sin(rightLineAngle));
            Point rightCircleEndPoint = new Point(rtCircleCenter.X + (circleDiameter / 2 + 2 * _controlStrokeThickness) * Math.Cos(rightLineAngle),
                                                  rtCircleCenter.Y + (circleDiameter / 2 + 2 * _controlStrokeThickness) * Math.Sin(rightLineAngle));


            int lineQuantity = (int)(basisLength / (3 * _controlStrokeThickness));
            double distance = basisLength / lineQuantity;
            for (int i = 0; i <= lineQuantity; i++)
            {
                Line line = new Line { StrokeThickness = _controlStrokeThickness, X1 = startTopPoint.X, Y1 = startTopPoint.Y, X2 = endTopPoint.X, Y2 = endTopPoint.Y };
                line.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

                if (this.Orientation == Orientation.Vertical)
                    startTopPoint.Y = endTopPoint.Y += distance;
                else
                    startTopPoint.X = endTopPoint.X += distance;
                canvas.Children.Add(line);
            }
            for (int i = 0; i <= lineQuantity; i++)
            {
                Line line = new Line { StrokeThickness = _controlStrokeThickness, X1 = startBottomPoint.X, Y1 = startBottomPoint.Y, X2 = endBottomPoint.X, Y2 = endBottomPoint.Y };
                line.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

                if (this.Orientation == Orientation.Vertical)
                    startBottomPoint.Y = endBottomPoint.Y += distance;
                else
                    startBottomPoint.X = endBottomPoint.X += distance;

                canvas.Children.Add(line);
            }
            // left circle drawing
            for (int i = 0; i <= 4; i++)
            {
                Line line = new Line { StrokeThickness = _controlStrokeThickness, X1 = leftCircleStartPoint.X, Y1 = leftCircleStartPoint.Y, X2 = leftCircleEndPoint.X, Y2 = leftCircleEndPoint.Y };
                line.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

                leftLineAngle += Math.PI * circleLineDistanceAngle / 180.0;
                var cosRightLineAngle = Math.Cos(leftLineAngle);
                var sinRightLineAngle = Math.Sin(leftLineAngle);

                leftCircleStartPoint.X = circleDiameter / 2 + circleDiameter / 2 * cosRightLineAngle;
                leftCircleStartPoint.Y = circleDiameter / 2 + circleDiameter / 2 * sinRightLineAngle;

                leftCircleEndPoint.X = circleDiameter / 2 + (circleDiameter / 2 + 2 * _controlStrokeThickness) * cosRightLineAngle;
                leftCircleEndPoint.Y = circleDiameter / 2 + (circleDiameter / 2 + 2 * _controlStrokeThickness) * sinRightLineAngle;
                canvas.Children.Add(line);
            }
            // right circle drawing
            for (int i = 0; i <= 4; i++)
            {
                Line line = new Line { StrokeThickness = _controlStrokeThickness, X1 = rightCircleStartPoint.X, Y1 = rightCircleStartPoint.Y, X2 = rightCircleEndPoint.X, Y2 = rightCircleEndPoint.Y };
                line.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

                rightLineAngle += Math.PI * circleLineDistanceAngle / 180.0;
                var cosRightLineAngle = Math.Cos(rightLineAngle);
                var sinRightLineAngle = Math.Sin(rightLineAngle);

                rightCircleStartPoint.X = rtCircleCenter.X + circleDiameter / 2 * cosRightLineAngle;
                rightCircleStartPoint.Y = rtCircleCenter.Y + circleDiameter / 2 * sinRightLineAngle;

                rightCircleEndPoint.X = rtCircleCenter.X + (circleDiameter / 2 + 2 * _controlStrokeThickness) * cosRightLineAngle;
                rightCircleEndPoint.Y = rtCircleCenter.Y + (circleDiameter / 2 + 2 * _controlStrokeThickness) * sinRightLineAngle;

                canvas.Children.Add(line);
            }

            return canvas;
        }
        #endregion private type drawing methods

        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(OutputMathFormula, NodeType.I, false);
            AddDefaultOutput(Output1MathFormula, NodeType.O, false);
        }

        private Point OutputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Height, 2), Math.Round(this.Y_Position - 1.5 * this.ElementStrokeThickness, 2));
        }

        private Point Output1MathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width - this.Height, 2), Math.Round(this.Y_Position - 1.5 * this.ElementStrokeThickness, 2));
        }
    }
}
