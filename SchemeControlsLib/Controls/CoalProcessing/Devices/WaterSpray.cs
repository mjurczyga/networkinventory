﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Types;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(EnumTypes.SchemeTypes.CoalProcessingScheme, true, "Natrysk")]
    public class WaterSpray : CoalProcessingControlBase, IActuator
    {
        static WaterSpray()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(WaterSpray), new FrameworkPropertyMetadata(typeof(WaterSpray)));
        }

        public WaterSpray()
        {
            CarrierType = CarrierTypes.Water;
        }

        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic());
            }
        }

        private FrameworkElement GetGraphic()
        {
            double width = this.Width;
            double height = this.Height;
            double controlStrokeThickness = this.ElementStrokeThickness;
            double circleDiameter = width;

            Ellipse ellipse = new Ellipse { StrokeThickness = controlStrokeThickness, Width = circleDiameter, Height = circleDiameter };
            ellipse.SetBinding(Ellipse.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            ellipse.SetBinding(Ellipse.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));


            Line line = new Line { StrokeThickness = controlStrokeThickness, X1 = width * 0.5, Y1 = circleDiameter, X2 = width * 0.5, Y2 = height };
            line.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            double circleRadius = 0.5 * circleDiameter;
            double xT = circleRadius * 0.5;
            double yT = circleRadius * Math.Sqrt(3) * 0.5;
            double xT1 = (height - circleDiameter) * 0.5;
            double yT1 = (height - circleDiameter) * Math.Sqrt(3) * 0.5;

            Line line1 = new Line { StrokeThickness = controlStrokeThickness, X1 = 0.5 * width - xT, Y1 = circleRadius + yT, X2 = 0.5 * width - xT - xT1, Y2 = circleRadius + yT + yT1 };
            line1.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            Line line2 = new Line { StrokeThickness = controlStrokeThickness, X1 = 0.5 * width + xT, Y1 = circleRadius + yT, X2 = 0.5 * width + xT + xT1, Y2 = circleRadius + yT + yT1 };
            line2.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Canvas canvas = new Canvas();
            canvas.Width = width;
            canvas.Height = height;
            canvas.Children.Add(ellipse);
            canvas.Children.Add(line);
            canvas.Children.Add(line1);
            canvas.Children.Add(line2);

            return canvas;
        }

        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(InputMathFormula, NodeType.I, true);
        }

        private Point InputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position, 2));
        }
        #endregion default IO

        public void UpdateState(int state)
        {
            _RiseUpdateStateEvent = false;

            this.State = state;

            _RiseUpdateStateEvent = true;
        }
    }
}
