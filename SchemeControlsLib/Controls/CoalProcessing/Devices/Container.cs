﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Shapes;
using SchemeControlsLib.Controls.Core.Controls;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.Converters;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(EnumTypes.SchemeTypes.CoalProcessingScheme, true, "Kontener/Zbiornik")]
    public class Container : CoalProcessingControlBase, IControlMerger
    {
        #region Poziom zbiornika

        [EditorProperty("Sygnalizować poziom zbiornika", "Zbiornik", "true = poziom, false = stan")]
        public bool IsTankLevel
        {
            get { return (bool)GetValue(IsTankLevelProperty); }
            set { SetValue(IsTankLevelProperty, value); }
        }

        public static readonly DependencyProperty IsTankLevelProperty =
            DependencyProperty.Register("IsTankLevel", typeof(bool), typeof(Container), new PropertyMetadata(false, IsTankLevel_PropertyChanged));

        private static void IsTankLevel_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Container container)
            {
                container.UpdateGraphic();
            }
        }

        [EditorProperty("Maks pojemność", "Zbiornik")]
        public double MaxLevel
        {
            get { return (double)GetValue(MaxLevelProperty); }
            set { SetValue(MaxLevelProperty, value); }
        }

        public static readonly DependencyProperty MaxLevelProperty =
            DependencyProperty.Register("MaxLevel", typeof(double), typeof(Container), new PropertyMetadata(100.0));

        [EditorProperty("Aktualny poziom", "Zbiornik")]
        public object TankLevel
        {
            get { return (object)GetValue(TankLevelProperty); }
            set { SetValue(TankLevelProperty, value); }
        }

        public static readonly DependencyProperty TankLevelProperty =
            DependencyProperty.Register("TankLevel", typeof(object), typeof(Container), new PropertyMetadata(0, TankLevel_PropertyChanged));

        private static void TankLevel_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is Container container)
            {
                if(double.TryParse(e.NewValue?.ToString(), out double dblVal))
                {
                    if (container.IsTankLevel)
                    {
                        if (dblVal != 0)
                            container.DeviceState = 2;
                        else
                            container.DeviceState = 1;
                    }
                    container.DoubleTankLevel = dblVal;
                }
            }
        }

        private double _DoubleTankLevel;
        public double DoubleTankLevel
        {
            get { return _DoubleTankLevel; }
            private set { _DoubleTankLevel = value; NotifyPropertyChanged("DoubleTankLevel"); }
        }

        #endregion Poziom zbiornika

        #region ContainerQuantity
        [EditorProperty("Ilość zbiorników", "Układ", serialize: false)]
        public int ControlQuantity
        {
            get { return (int)GetValue(ControlQuantityProperty); }
            set { SetValue(ControlQuantityProperty, value); }
        }

        public static readonly DependencyProperty ControlQuantityProperty =
            DependencyProperty.Register("ControlQuantity", typeof(int), typeof(Container), new PropertyMetadata(1, ContainerQuantity_PropertyChanged));

        private static void ContainerQuantity_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Container container && int.TryParse(e.NewValue?.ToString(), out int newValue) && int.TryParse(e.OldValue?.ToString(), out int oldValue))
            {
                if(container.MergingControl != null && container.MergingControl.ControlQuantity != newValue)
                {
                    container.MergingControl.ControlQuantity = newValue;
                }
            }
        }
        #endregion ContainerQuantity

        public IMergingControl MergingControl { get; set; }

        public override void UpdateGraphic()
        {
            base.UpdateGraphic();
            if (_controlGraphic_PART_Grid != null)
            {

                //DODAĆ MAXLEVEL
                ContainerShape containerShape = new ContainerShape() { StrokeThickness = this.ElementStrokeThickness, Width= this.Width, Height = this.Height };
                containerShape.SetBinding(ContainerShape.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
                if(IsTankLevel)
                    containerShape.SetBinding(ContainerShape.FillProperty, BindingUtilities.GetBinding("DoubleTankLevel", this, this.InternalBackgroundBrush, converter: new FillLevelToHeightConverter(), converterParameter: MaxLevel));
                else
                    containerShape.SetBinding(ContainerShape.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
                _controlGraphic_PART_Grid.Children.Add(containerShape);
            }
        }

        static Container()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Container), new FrameworkPropertyMetadata(typeof(Container)));
        }

        public Container(MergingControl mergingControl)
        {
            MergingControl = mergingControl;
        }

        public Container()
        {
            MergingControl = new MergingControl(this);
        }

        public override void Remove()
        {
            MergingControl?.Remove();
            base.Remove();
        }

        protected override void ControlPosition_Changed(double oldX_Position, double oldY_Position)
        {
            base.ControlPosition_Changed(oldX_Position, oldY_Position);
            if(!IsSelectionMoving)
                MergingControl?.MoveGroup(this, new Point(oldX_Position, oldY_Position));
        }

        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(OutputMathFormula, NodeType.O, true);
            AddDefaultOutput(InputMathFormula, NodeType.I, true);
        }

        private Point OutputMathFormula()
        {
            return new Point(this.X_Position + this.Width / 2.0, this.Y_Position + this.Height);
        }

        private Point InputMathFormula()
        {
            return new Point(this.X_Position + this.Width / 2.0, this.Y_Position);
        }
    }
}
