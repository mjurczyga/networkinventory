﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Shapes;
using SchemeControlsLib.Controls.CoalProcessing.Types;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(EnumTypes.SchemeTypes.CoalProcessingScheme, true, "Ciężarówka")]
    public class Truck : CoalProcessingControlBase
    {
        private static double _balancePerceent = 100.0 * 30.0 / 25.0; // 5 - wysokośćwagi, 25 - wysokość kontrolki

        #region BasisType
        [EditorProperty("Typ podstawy", "Układ")]
        public BasisType BasisType
        {
            get { return (BasisType)GetValue(BasisTypeProperty); }
            set { SetValue(BasisTypeProperty, value); }
        }

        public static readonly DependencyProperty BasisTypeProperty =
            DependencyProperty.Register("BasisType", typeof(BasisType), typeof(Truck), new PropertyMetadata(BasisTypes.None, BasisType_PropertyChanged));

        private static void BasisType_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Truck truck && e.OldValue is BasisType oldBasisType)
            {
                truck.SetDefaultSize(oldBasisType);
                truck.UpdateGraphic();
                //truck.AddDefaultOutputs();
            }
        }
        #endregion BasisType

        static Truck()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Truck), new FrameworkPropertyMetadata(typeof(Truck)));
        }

        public Truck()
        {
        }

        protected override void ControlSizeChanged()
        {
            base.ControlSizeChanged();
            this.SetDefaultSize(this.BasisType);
        }

        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic(_DefaultSize));
            }
        }

        private UIElement GetGraphic(Size defaultSize)
        {
            Canvas canvas = new Canvas();
            var _controlStrokeThickness = this.ElementStrokeThickness;

            TruckShape truckShape = new TruckShape { StrokeThickness = _controlStrokeThickness, Width = defaultSize.Width, Height = defaultSize.Height };
            truckShape.SetBinding(TruckShape.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            truckShape.SetBinding(TruckShape.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
            canvas.Children.Add(truckShape);


            var basis = DrawBalanceOrRail(defaultSize);
            canvas.Children.Add(basis);
            Canvas.SetTop(basis, defaultSize.Height);

            return canvas;
        }

        /// <summary>
        /// Draw basis on control bottom
        /// </summary>
        /// <param name="stackPanel">Panel from control shape</param>
        /// <param name="binding">Stroke binding</param>
        /// <param name="controlWidth">Control width</param>
        /// <param name="controlHeight">Control height</param>
        /// <param name="basisType">basis type</param>
        protected FrameworkElement DrawBalanceOrRail(Size singleControlSize)
        {
            var _controlStrokeThickness = this.ElementStrokeThickness;
            var width = singleControlSize.Width;
            var height = singleControlSize.Height;
            double computedHeight = 0;
            Canvas canvas = new Canvas();
            if (BasisType != BasisTypes.None)
            {

                Line line = new Line { StrokeThickness = _controlStrokeThickness };
                line.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
                line.SetBinding(Line.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
                line.X1 = 0; line.Y1 = 0;
                line.X2 = singleControlSize.Width; line.Y2 = 0;
                canvas.Children.Add(line);
                computedHeight = _controlStrokeThickness;

                if (BasisType == BasisTypes.Balance)
                {
                    computedHeight = _controlStrokeThickness + ((_balancePerceent / 100.0) * height) - height;
                    PointCollection points = new PointCollection();
                    points.Add(new Point(singleControlSize.Width / 2, _controlStrokeThickness));
                    points.Add(new Point(singleControlSize.Width / 2 + singleControlSize.Height / 10, computedHeight));
                    points.Add(new Point(singleControlSize.Width / 2 - singleControlSize.Height / 10, computedHeight));
                    points.Add(new Point(singleControlSize.Width / 2, _controlStrokeThickness));
                    Polygon polyline = new Polygon { StrokeThickness = _controlStrokeThickness, Points = points };
                    polyline.SetBinding(Polygon.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
                    polyline.SetBinding(Polygon.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
                    canvas.Children.Add(polyline);
                }
            }

            SetControlSize(new Size(singleControlSize.Width, singleControlSize.Height + computedHeight));
            return canvas;
        }

        private Size _DefaultSize = new Size();
        private void SetDefaultSize(BasisType basisType)
        {
            double width = this.Width;

            double height = this.Height;
            if (basisType == BasisTypes.Rails)
            {
                height -= this.ElementStrokeThickness;
            }
            else if (basisType == BasisTypes.Balance)
            {
                height = ((height - this.ElementStrokeThickness) * 100) / _balancePerceent;
            }

            _DefaultSize = new Size(Math.Max(0.0, width), Math.Max(0.0, height));
        }

        private void SetControlSize(Size size)
        {
            if (this.Width != size.Width)
                this.Width = size.Width;
            if (this.Height != size.Height)
                this.Height = size.Height;
        }

        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(InputMathFormula, NodeType.I, true);
        }

        private Point InputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position, 2));
        }
        #endregion default IO
    }
}
