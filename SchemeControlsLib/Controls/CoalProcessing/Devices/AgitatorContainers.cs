﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(EnumTypes.SchemeTypes.CoalProcessingScheme, true, "Pojemnik mieszadła")]
    public class AgitatorContainers : CoalProcessingControlBase
    {
        static AgitatorContainers()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(AgitatorContainers), new FrameworkPropertyMetadata(typeof(AgitatorContainers)));
        }

        public AgitatorContainers()
        {
        }

        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.DrawControl());
            }
        }

        private FrameworkElement DrawControl()
        {
            var controlStrokeThickness = this.ElementStrokeThickness;
            var width = this.Width;
            var height = this.Height;

            PointCollection points = new PointCollection();
            points.Add(new Point(controlStrokeThickness / 2, 0));
            points.Add(new Point(controlStrokeThickness / 2, height - controlStrokeThickness / 2));
            points.Add(new Point(width - controlStrokeThickness / 2, height - controlStrokeThickness / 2));
            points.Add(new Point(width - controlStrokeThickness / 2, 0));

            Polyline polyline = new Polyline { StrokeThickness = controlStrokeThickness, Points = points };
            polyline.SetBinding(Polyline.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));


            return polyline;
        }

        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(OutputMathFormula, NodeType.O, true);
            AddDefaultOutput(InputMathFormula, NodeType.I, true);
        }

        private Point OutputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position, 2));
        }

        private Point InputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position + this.Height, 2));
        }
        #endregion default IO
    }
}
