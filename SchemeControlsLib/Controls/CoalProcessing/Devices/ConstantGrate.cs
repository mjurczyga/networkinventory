﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(SchemeTypes.CoalProcessingScheme, true, "Ruszt stały")]
    public class ConstantGrate : CoalProcessingControlBase
    {
        double _circleDiameter = 6;

        static ConstantGrate()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ConstantGrate), new FrameworkPropertyMetadata(typeof(ConstantGrate)));
        }

        public ConstantGrate()
        {
        }

        #region Graphic
        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic());
            }
        }

        private FrameworkElement GetGraphic()
        {
            var _controlStrokeThickness = this.ElementStrokeThickness;
            var _width = this.Width;
            var _height = this.Height;

            Canvas canvas = new Canvas();

            Ellipse ellipse = new Ellipse { StrokeThickness = _controlStrokeThickness, Width = _circleDiameter, Height = _circleDiameter };
            ellipse.SetBinding(Ellipse.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            ellipse.SetBinding(Ellipse.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
            PointCollection points = new PointCollection();
            points.Add(new Point(0, _circleDiameter / 2));
            points.Add(new Point(_width / 2, _height));
            points.Add(new Point(_width, 3 * _height / 4));
            points.Add(new Point(_circleDiameter, _circleDiameter / 2));
            Polygon polygon = new Polygon { Points = points };
            //polygon.SetBinding(Polygon.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            polygon.SetBinding(Polygon.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
            canvas.Children.Add(polygon);


            Line line = new Line { StrokeThickness = _controlStrokeThickness, X1 = _controlStrokeThickness / 2, Y1 = _circleDiameter / 2, X2 = _width / 2, Y2 = _height };
            line.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Line line1 = new Line { StrokeThickness = _controlStrokeThickness, X1 = _width / 2, Y1 = _height - _controlStrokeThickness / 2, X2 = _width - _controlStrokeThickness / 2, Y2 = 3 * _height / 4 };
            line1.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Line line2 = new Line
            {
                StrokeThickness = _controlStrokeThickness,
                X1 = _width,
                Y1 = 3 * _height / 4,
                X2 = _circleDiameter,
                Y2 = _circleDiameter / 2,
                StrokeDashArray = new DoubleCollection() { 3 }
            };
            line2.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));


            canvas.Children.Add(ellipse);
            canvas.Children.Add(line);
            canvas.Children.Add(line1);
            canvas.Children.Add(line2);


            return canvas;
        }
        #endregion Graphic
    }
}
