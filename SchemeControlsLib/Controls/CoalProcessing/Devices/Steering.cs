﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(SchemeTypes.CoalProcessingScheme, true, "Zwrotnica")]
    public class Steering : CoalProcessingControlBase, IActuator
    {
        [EditorProperty("Orientacja grafiki", "Wygląd")]
        public OrientationExt GraphicsPlace
        {
            get { return (OrientationExt)GetValue(GraphicsPlaceProperty); }
            set { SetValue(GraphicsPlaceProperty, value); }
        }

        public static readonly DependencyProperty GraphicsPlaceProperty =
            DependencyProperty.Register("GraphicsPlace", typeof(OrientationExt), typeof(Steering), new PropertyMetadata(GraphicsPlace_PropertyChanged));

        private static void GraphicsPlace_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is Steering steering)
            {
                steering.UpdateGraphic();
            }
        }

        static Steering()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Steering), new FrameworkPropertyMetadata(typeof(Steering)));
        }

        public Steering()
        {
        }

        #region Graphic
        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic());
            }
        }

        private FrameworkElement GetGraphic()
        {
            var _controlStrokeThickness = this.ElementStrokeThickness;

            string data = "";
            Path path = new Path { StrokeThickness = _controlStrokeThickness, Fill = Brushes.Gray, VerticalAlignment = VerticalAlignment.Bottom, HorizontalAlignment = HorizontalAlignment.Center, Stretch = Stretch.Uniform };

            if (GraphicsPlace == OrientationExt.LeftTop)
            {
                data = "m 0.17687054,287.177 9.64629776,9.64613 M 0.25,296.75 a 9.5,9.5 0 0 1 9.5000002,-9.5 l -2e-7,9.5 z";
            }
            else if (GraphicsPlace == OrientationExt.RightTop)
            {
                data = "M 0.17685211,296.82315 9.8231478,287.17694 M 0.25000019,287.25 A 9.5,9.5 0 0 1 9.75,296.75 h -9.5 z";
            }
            else if (GraphicsPlace == OrientationExt.LeftBottom)
            {
                data = "M 0.17685211,296.82315 9.8231479,287.17694 M 9.7500003,296.75 A 9.5,9.5 0 0 1 3.0324855,293.96751 9.5,9.5 0 0 1 0.25,287.25 h 9.5 z";
            }
            else if (GraphicsPlace == OrientationExt.RightBottom)
            {
                data = "m 0.17687054,287.177 9.64629766,9.64613 M 9.75,287.25 a 9.5,9.5 0 0 1 -9.49999975,9.5 L 0.25,287.25 Z";
            }

            path.Data = Geometry.Parse(data);

            path.SetBinding(Path.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            path.SetBinding(Path.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));


            return path;
        }

        public void UpdateState(int state)
        {
            this.DeviceState = state;
        }
        #endregion Graphic
    }
}