﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(EnumTypes.SchemeTypes.CoalProcessingScheme, true, "Przesiewacz obrócony")]
    public class RotatedCoalScreen : CoalProcessingControlBase
    {
        private readonly double _rectangleWH = 3;


        public override void UpdateGraphic()
        {
            base.UpdateGraphic();
            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.DrawShape());
            }
        }

        private FrameworkElement DrawShape()
        {
            Canvas shapeCanvas = new Canvas();
            Point stPoint = new Point();

            var controlHeight = this.Height;
            var controlWidth = this.Width;

            var controlStrokeThickness = this.ElementStrokeThickness;
            var rectangleWH = this.ElementStrokeThickness * _rectangleWH;

            stPoint.X = 0; ; stPoint.Y = controlHeight - rectangleWH;

            PointCollection points = new PointCollection();
            points.Add(stPoint);
            points.Add(new Point(rectangleWH, controlHeight));
            points.Add(new Point(controlWidth, controlHeight));
            points.Add(new Point(controlWidth, rectangleWH));
            points.Add(new Point(controlWidth - rectangleWH, 0));
            points.Add(stPoint);
            Polygon polygon = new Polygon();
            polygon.Points = points;
            polygon.StrokeThickness = controlStrokeThickness;
            polygon.SetBinding(Polygon.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            polygon.SetBinding(Polygon.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));

            Line line = new Line();
            line.X1 = rectangleWH; line.Y1 = controlHeight;
            line.X2 = controlWidth; line.Y2 = rectangleWH;
            line.StrokeThickness = controlStrokeThickness;
            line.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Line line1 = new Line();
            line1.X1 = rectangleWH / 2; line1.Y1 = controlHeight - (rectangleWH / 2);
            line1.X2 = controlWidth - (rectangleWH / 2); line1.Y2 = rectangleWH / 2;
            line1.StrokeThickness = controlStrokeThickness;
            line1.StrokeDashArray = new DoubleCollection() { 5 };
            line1.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            shapeCanvas.Width = controlWidth;
            shapeCanvas.Height = controlHeight;

            shapeCanvas.Children.Add(polygon);
            shapeCanvas.Children.Add(line);
            shapeCanvas.Children.Add(line1);

            return shapeCanvas;
        }

        static RotatedCoalScreen()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(RotatedCoalScreen), new FrameworkPropertyMetadata(typeof(RotatedCoalScreen)));
        }

        public RotatedCoalScreen()
        {
        }

        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(InputMathFormula, NodeType.I, true);
            AddDefaultOutput(Output1MathFormula, NodeType.O, true);
            AddDefaultOutput(Output2MathFormula, NodeType.O, true);
        }

        private Point InputMathFormula()
        {
            var controlStrokeThickness = this.ElementStrokeThickness;
            var rectangleWH = this.ElementStrokeThickness * _rectangleWH;
            return new Point(Math.Round(this.X_Position + this.Width - rectangleWH / 2.0, 2), Math.Round(this.Y_Position + rectangleWH / 2, 2));
        }

        private Point Output1MathFormula()
        {
            var controlStrokeThickness = this.ElementStrokeThickness;
            var rectangleWH = this.ElementStrokeThickness * _rectangleWH;
            return new Point(Math.Round(this.X_Position + rectangleWH / 2.0, 2), Math.Round(this.Y_Position + this.Height - (rectangleWH / 2.0), 2));
        }
        private Point Output2MathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width, 2), Math.Round(this.Y_Position + this.Height, 2));
        }
        #endregion default IO

    }
}
