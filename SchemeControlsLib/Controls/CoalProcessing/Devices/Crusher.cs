﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Shapes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(EnumTypes.SchemeTypes.CoalProcessingScheme, true, "Kruszarka")]
    public class Crusher : CoalProcessingControlBase
    {
        static Crusher()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Crusher), new FrameworkPropertyMetadata(typeof(Crusher)));
        }

        public Crusher()
        {
        }

        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.DrawControl());
            }
        }

        private FrameworkElement DrawControl()
        {
            Canvas canvas = new Canvas();
            var width = this.Width;
            var height = this.Height;

            CrusherShape ellipse = new CrusherShape() { Width = width, Height = height, StrokeThickness = this.ElementStrokeThickness };
            ellipse.SetBinding(CrusherShape.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            ellipse.SetBinding(CrusherShape.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
            canvas.Children.Add(ellipse);
            return canvas;
        }

        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(OutputMathFormula, NodeType.O, true);
            AddDefaultOutput(InputMathFormula, NodeType.I, true);
        }

        private Point OutputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position, 2));
        }

        private Point InputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position + this.Height, 2));
        }
        #endregion default IO

    }
}
