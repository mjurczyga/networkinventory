﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Types;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(EnumTypes.SchemeTypes.CoalProcessingScheme, true, "Filtr próżniowy")]
    public class VaccumFilter : CoalProcessingControlBase
    {
        #region VaccumType
        [EditorProperty("Typ pompy", "Układ")]
        public VaccumType VaccumType
        {
            get { return (VaccumType)GetValue(VaccumTypeProperty); }
            set { SetValue(VaccumTypeProperty, value); }
        }

        public static readonly DependencyProperty VaccumTypeProperty =
            DependencyProperty.Register("VaccumType", typeof(VaccumType), typeof(VaccumFilter), new PropertyMetadata(VaccumTypes.Disc, VaccumType_PropertyChanged));

        private static void VaccumType_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is VaccumFilter vaccumFilter)
            {
                vaccumFilter.UpdateGraphic();
            }
        }
        #endregion VaccumType

        static VaccumFilter()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(VaccumFilter), new FrameworkPropertyMetadata(typeof(VaccumFilter)));
        }

        public VaccumFilter()
        {
        }

        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.DrawGraphic());
            }
        }

        private UIElement DrawGraphic()
        {
            double _width = this.Width;
            double _height = this.Height;
            double _controlStrokeThickness = this.ElementStrokeThickness;

            Canvas canvas = new Canvas { Width = _width, Height = _height };

            Ellipse ellipse = new Ellipse { StrokeThickness = _controlStrokeThickness, Height = _height, Width = _width };
            ellipse.SetBinding(Ellipse.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            ellipse.SetBinding(Ellipse.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
            canvas.Children.Add(ellipse);
            if (VaccumType == VaccumTypes.Disc)
            {
                Line line = new Line { StrokeThickness = _controlStrokeThickness, X1 = .5 * _width - 0.5 * (_width / Math.Sqrt(2)), Y1 = .5 * _height - 0.5 * (_height / Math.Sqrt(2)), X2 = .5 * _width + 0.5 * (_width / Math.Sqrt(2)), Y2 = .5 * _height + 0.5 * (_height / Math.Sqrt(2)) };
                line.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

                Line line1 = new Line { StrokeThickness = _controlStrokeThickness, X1 = .5 * _width + 0.5 * (_width / Math.Sqrt(2)), Y1 = .5 * _height - 0.5 * (_height / Math.Sqrt(2)), X2 = .5 * _width - 0.5 * (_width / Math.Sqrt(2)), Y2 = .5 * _height + 0.5 * (_height / Math.Sqrt(2)) };
                line1.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
                canvas.Children.Add(line);
                canvas.Children.Add(line1);
            }

            return canvas;
        }

        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(InputMathFormula, NodeType.I, true);

            AddDefaultOutput(OutputMathFormula, NodeType.O, true);
        }

        private Point InputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position, 2));
        }

        private Point OutputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position + this.Height, 2));
        }
        #endregion default IO
    }
}
