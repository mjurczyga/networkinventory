﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Shapes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(SchemeTypes.CoalProcessingScheme, true, "Rozdzielacz")]
    public sealed class Distributor : CoalProcessingControlBase
    {
        static Distributor()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Distributor), new FrameworkPropertyMetadata(typeof(Distributor)));
        }

        public Distributor()
        {

        }

        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.DrawShape());
            }

        }


        private FrameworkElement DrawShape()
        {
            Canvas distributorCanvas = new Canvas();
            var _width = this.Width;
            var _height = this.Height;
            var _controlStrokeThickness = this.ElementStrokeThickness;

            DistributorShape distributorShape = new DistributorShape { StrokeThickness = _controlStrokeThickness, Width = _width, Height = _height };
            distributorShape.SetBinding(DistributorShape.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            distributorShape.SetBinding(DistributorShape.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));

            Ellipse ellipse = new Ellipse { StrokeThickness = _controlStrokeThickness, Width = _width, Height = _height };
            ellipse.SetBinding(Ellipse.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            ellipse.SetBinding(Ellipse.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));

            distributorCanvas.Children.Add(ellipse);
            distributorCanvas.Children.Add(distributorShape);
            return distributorCanvas;
        }

        #region private add outputs
        protected override void AddDefaultOutputs()
        {
            this.AddDefaultOutput(OutputMathFormula, NodeType.I);
            this.AddDefaultOutput(Output1MathFormula, NodeType.O);
            this.AddDefaultOutput(Output2MathFormula, NodeType.O);
        }

        private Point OutputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0), Math.Round(this.Y_Position));
        }

        private Point Output1MathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0 - (Math.Sqrt(3) * (this.Width / 4.0)), 2), Math.Round(this.Y_Position + (3.0 * this.Height) / 4.0, 2));
        }

        private Point Output2MathFormula()
        {
            return new Point(Math.Round(this.X_Position + Math.Sqrt(3) * (this.Width / 4.0) + this.Width / 2.0, 2), Math.Round(this.Y_Position + (3.0 * this.Height) / 4.0, 2));
        }
        #endregion private add outputs
    }
}
