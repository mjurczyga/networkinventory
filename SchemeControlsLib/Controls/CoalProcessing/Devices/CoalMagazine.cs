﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(EnumTypes.SchemeTypes.CoalProcessingScheme, true, "Magazyn węgla")]
    public class CoalMagazine : CoalProcessingControlBase, ITextControl
    {
        #region ITextControl
        [EditorProperty("Opis", "Tekst")]
        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(CoalMagazine));

        //[EditorProperty("Dekoracja opisu", "Tekst")]
        public TextDecorationCollection TextDecorationCollection
        {
            get { return (TextDecorationCollection)GetValue(TextDecorationCollectionProperty); }
            set { SetValue(TextDecorationCollectionProperty, value); }
        }

        public static readonly DependencyProperty TextDecorationCollectionProperty =
            DependencyProperty.Register("TextDecorationCollection", typeof(TextDecorationCollection), typeof(CoalMagazine), new PropertyMetadata(TextDecorations.Underline));

        [EditorProperty("Rozmiar opisu", "Tekst")]
        public new double FontSize { get { return base.FontSize; } set { base.FontSize = value; } }

        [EditorProperty("Kolor opisu", "Tekst")]
        public new Brush Foreground { get { return base.Foreground; } set { base.Foreground = value; } }

        [EditorProperty("Grubość opisu", "Tekst")]
        public new FontWeight FontWeight { get { return base.FontWeight; } set { base.FontWeight = value; } }

        [EditorProperty("Styl opisu", "Tekst")]
        public new FontStyle FontStyle { get { return base.FontStyle; } set { base.FontStyle = value; } }
        #endregion ITextControl

        static CoalMagazine()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CoalMagazine), new FrameworkPropertyMetadata(typeof(CoalMagazine)));
        }

        public CoalMagazine()
        {
        }

        #region graphic
        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic());
            }
        }

        private UIElement GetGraphic()
        {
            var _controlStrokeThickness = this.ElementStrokeThickness;
            double width = this.Width;
            double height = this.Height;

            PointCollection points = new PointCollection();
            points.Add(new Point(width / 6, _controlStrokeThickness * 0.5));
            points.Add(new Point(5 * width / 6, _controlStrokeThickness * 0.5));
            points.Add(new Point(width, height + _controlStrokeThickness * 0.5));
            points.Add(new Point(0, height + _controlStrokeThickness * 0.5));
            points.Add(new Point(width / 6, _controlStrokeThickness * 0.5));

            Polygon polygon = new Polygon { StrokeThickness = _controlStrokeThickness, Points = points };
            polygon.SetBinding(Polygon.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            polygon.SetBinding(Polygon.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));

            return polygon;
        }
        #endregion graphic

        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(OutputMathFormula, NodeType.O, true);
            AddDefaultOutput(Output1MathFormula, NodeType.O, true);
            AddDefaultOutput(Output2MathFormula, NodeType.O, true);

            AddDefaultOutput(InputMathFormula, NodeType.I, true);
            AddDefaultOutput(Input1MathFormula, NodeType.I, true);
            AddDefaultOutput(Input2MathFormula, NodeType.I, true);
        }

        private Point OutputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + 3 * this.Width / 8.0, 2), Math.Round(this.Y_Position + this.Height, 2));
        }

        private Point Output1MathFormula()
        {
            return new Point(Math.Round(this.X_Position + 5 * this.Width / 8.0, 2), Math.Round(this.Y_Position + this.Height, 2));
        }

        private Point Output2MathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position + this.Height, 2));
        }

        private Point InputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0 - this.Width / 4.0, 2), Math.Round(this.Y_Position, 2));
        }

        private Point Input1MathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position, 2));
        }

        private Point Input2MathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0 + this.Width / 4.0, 2), Math.Round(this.Y_Position, 2));
        }
        #endregion default IO
    }
}
