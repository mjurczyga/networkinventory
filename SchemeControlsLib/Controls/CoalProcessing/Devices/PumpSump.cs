﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(SchemeTypes.CoalProcessingScheme, true, "Rząpie pompowe z przelewem")]
    public class PumpSump : CoalProcessingControlBase
    {
        static PumpSump()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PumpSump), new FrameworkPropertyMetadata(typeof(PumpSump)));
        }

        public PumpSump()
        {
        }

        #region Graphic
        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic());
            }
        }

        private FrameworkElement GetGraphic()
        {
            var _controlStrokeThickness = this.ElementStrokeThickness;

            Canvas canvas = new Canvas();
            var _width = this.Width;
            var _height = this.Height;

            PointCollection points = new PointCollection();
            points.Add(new Point(_controlStrokeThickness / 2, 0));
            points.Add(new Point(_controlStrokeThickness / 2, _height / 3));
            points.Add(new Point(_width - _controlStrokeThickness / 2, _height));
            points.Add(new Point(_width - _controlStrokeThickness / 2, 0));

            Polyline polyline = new Polyline { StrokeThickness = _controlStrokeThickness, Points = points };
            polyline.SetBinding(Polyline.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            polyline.SetBinding(Polyline.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));

            PointCollection fillPoints = new PointCollection();
            fillPoints.Add(new Point(_controlStrokeThickness / 2, 3 * _controlStrokeThickness));
            fillPoints.Add(new Point(_controlStrokeThickness / 2, _height / 3));
            fillPoints.Add(new Point(_width - _controlStrokeThickness / 2, _height));
            fillPoints.Add(new Point(_width - _controlStrokeThickness / 2, 3 * _controlStrokeThickness));

            Polygon polygon = new Polygon { StrokeThickness = _controlStrokeThickness, Points = fillPoints };
            polygon.SetBinding(Polygon.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            polygon.SetBinding(Polygon.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
            canvas.Children.Add(polygon);

            canvas.Children.Add(polyline);


            return canvas;
        }
        #endregion Graphic
    }
}
