﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(EnumTypes.SchemeTypes.CoalProcessingScheme, true, "Płuczka")]
    public class DenseMediumSeparator : CoalProcessingControlBase
    {
        #region ITextControl
        [EditorProperty("Opis", "Tekst")]
        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(DenseMediumSeparator));

        //[EditorProperty("Dekoracja opisu", "Tekst")]
        public TextDecorationCollection TextDecorationCollection
        {
            get { return (TextDecorationCollection)GetValue(TextDecorationCollectionProperty); }
            set { SetValue(TextDecorationCollectionProperty, value); }
        }

        public static readonly DependencyProperty TextDecorationCollectionProperty =
            DependencyProperty.Register("TextDecorationCollection", typeof(TextDecorationCollection), typeof(DenseMediumSeparator), new PropertyMetadata(TextDecorations.Underline));

        [EditorProperty("Rozmiar opisu", "Tekst")]
        public new double FontSize { get { return base.FontSize; } set { base.FontSize = value; } }

        [EditorProperty("Kolor opisu", "Tekst")]
        public new Brush Foreground { get { return base.Foreground; } set { base.Foreground = value; } }

        [EditorProperty("Grubość opisu", "Tekst")]
        public new FontWeight FontWeight { get { return base.FontWeight; } set { base.FontWeight = value; } }

        [EditorProperty("Styl opisu", "Tekst")]
        public new FontStyle FontStyle { get { return base.FontStyle; } set { base.FontStyle = value; } }
        #endregion ITextControl

        static DenseMediumSeparator()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DenseMediumSeparator), new FrameworkPropertyMetadata(typeof(DenseMediumSeparator)));
        }

        public DenseMediumSeparator()
        {
        }

        #region Graphic
        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic());
            }
        }

        private UIElement GetGraphic()
        {
            var width = this.Width;
            var height = this.Height;
            var controlStrokeThickness = this.ElementStrokeThickness;

            Canvas canvas = new Canvas();

            Rectangle rectangle = new Rectangle { Width = width, Height = height, StrokeThickness = controlStrokeThickness };
            rectangle.SetBinding(Rectangle.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            rectangle.SetBinding(Rectangle.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));

            Ellipse ellipse = new Ellipse { StrokeThickness = controlStrokeThickness, Width = width, Height = height };
            ellipse.SetBinding(Ellipse.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Line line = new Line { X1 = width / 6, Y1 = height / 2, X2 = 5 * width / 6, Y2 = height / 2, StrokeThickness = controlStrokeThickness };
            line.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            canvas.Children.Add(rectangle);
            canvas.Children.Add(ellipse);
            canvas.Children.Add(line);

            return canvas;
        }
        #endregion Graphic

        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(InputMathFormula, NodeType.I, true);
            AddDefaultOutput(Input1MathFormula, NodeType.I, true);
            AddDefaultOutput(OutputMathFormula, NodeType.O, true);
            AddDefaultOutput(Output1MathFormula, NodeType.O, true);
        }

        private Point InputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position, 2));
        }

        private Point Input1MathFormula()
        {
            return new Point(Math.Round(this.X_Position, 2), Math.Round(this.Y_Position + this.Height / 2.0, 2));
        }

        private Point OutputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position + this.Height, 2));
        }

        private Point Output1MathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width, 2), Math.Round(this.Y_Position + this.Height / 2.0, 2));
        }
        #endregion default IO
    }
}
