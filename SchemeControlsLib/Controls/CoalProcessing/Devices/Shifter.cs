﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(EnumTypes.SchemeTypes.CoalProcessingScheme, true, "Przestawiak")]
    public class Shifter : CoalProcessingControlBase
    {
        #region CloseShifter
        [EditorProperty("Przestawiak zamknięty", "Przestawiak")]
        public object CloseShifter
        {
            get { return (object)GetValue(CloseShifterProperty); }
            set { SetValue(CloseShifterProperty, value); }
        }

        public static readonly DependencyProperty CloseShifterProperty =
            DependencyProperty.Register("CloseShifter", typeof(object), typeof(Shifter), new PropertyMetadata(false, CloseShifter_PropertyChanged));

        private static void CloseShifter_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is Shifter shifter)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal))
                    shifter.BoolCloseShifter = boolVal;
                else if(int.TryParse(e.NewValue?.ToString(), out int intVal))
                {
                    if (intVal > 0)
                        shifter.BoolCloseShifter = true;
                    else
                        shifter.BoolCloseShifter = false;
                }
            }
        }

        private bool _BoolCloseShifter;
        public bool BoolCloseShifter
        {
            get { return _BoolCloseShifter; }
            set { _BoolCloseShifter = value; NotifyPropertyChanged("BoolCloseShifter"); }
        }
        #endregion CloseShifter

        #region OpenShifter
        [EditorProperty("Przestawiak otwarty", "Przestawiak")]
        public object OpenShifter
        {
            get { return (object)GetValue(OpenShifterProperty); }
            set { SetValue(OpenShifterProperty, value); }
        }

        public static readonly DependencyProperty OpenShifterProperty =
            DependencyProperty.Register("OpenShifter", typeof(object), typeof(Shifter), new PropertyMetadata(false, OpenShifter_PropertyChanged));

        private static void OpenShifter_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Shifter shifter)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal))
                    shifter.BoolOpenShifter = boolVal;
                else if (int.TryParse(e.NewValue?.ToString(), out int intVal))
                {
                    if (intVal > 0)
                        shifter.BoolOpenShifter = true;
                    else
                        shifter.BoolOpenShifter = false;
                }
            }
        }

        private bool _BoolOpenShifter;
        public bool BoolOpenShifter
        {
            get { return _BoolOpenShifter; }
            set { _BoolOpenShifter = value; NotifyPropertyChanged("BoolOpenShifter"); }
        }
        #endregion CloseShifter


        static Shifter()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Shifter), new FrameworkPropertyMetadata(typeof(Shifter)));
        }

        public Shifter()
        {
        }

        #region Graphic
        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic());
            }
        }

        private FrameworkElement GetGraphic()
        {
            var _controlStrokeThickness = this.ElementStrokeThickness;
            var _width = this.Width;
            var _height = this.Height;

            Rectangle rectangle = new Rectangle { Width = _width, Height = _height, StrokeThickness = _controlStrokeThickness };
            rectangle.SetBinding(Rectangle.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            rectangle.SetBinding(Rectangle.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));

            Line line1 = new Line { StrokeThickness = _controlStrokeThickness / 2, X1 = _width / 2, Y1 = _height / 2, X2 = _width / 2, Y2 = _height };
            line1.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));


            Line lineO = new Line { StrokeThickness = _controlStrokeThickness / 2, X1 = _width / 2, Y1 = 0, X2 = _width - _controlStrokeThickness * 1.5, Y2 = _height / 2 };
            lineO.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            lineO.SetBinding(Line.VisibilityProperty, BindingUtilities.GetBinding("BoolOpenShifter", this, this.BoolOpenShifter, converter: new BooleanToVisibilityConverter()));

            Line lineC = new Line { StrokeThickness = _controlStrokeThickness / 2, X1 = _width / 2, Y1 = 0, X2 = _width / 2, Y2 = _height / 2 };
            lineC.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            lineC.SetBinding(Line.VisibilityProperty, BindingUtilities.GetBinding("BoolCloseShifter", this, this.BoolCloseShifter, converter: new BooleanToVisibilityConverter()));

            Canvas canvas = new Canvas();
            canvas.Children.Add(rectangle);
            canvas.Children.Add(lineO);
            canvas.Children.Add(lineC);
            canvas.Children.Add(line1);

            return canvas;
        }
        #endregion Graphic


        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(InputMathFormula, NodeType.I, true);

            AddDefaultOutput(OutputMathFormula, NodeType.O, true);
        }

        private Point InputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position, 2));
        }

        private Point OutputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position + this.Height, 2));
        }
        #endregion default IO
    }
}
