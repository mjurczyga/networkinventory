﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(SchemeTypes.CoalProcessingScheme, true, "Mieszalnik")]
    public class Mixer : CoalProcessingControlBase
    {
        static Mixer()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Mixer), new FrameworkPropertyMetadata(typeof(Mixer)));
        }

        public Mixer()
        {
        }

        #region Graphic
        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic());
            }
        }

        private FrameworkElement GetGraphic()
        {
            var _controlStrokeThickness = this.ElementStrokeThickness;
            var width = this.Width;
            var height = this.Height;

            Canvas canvas = new Canvas { Width = width, Height = height };
            PointCollection points = new PointCollection();
            points.Add(new Point(0, 0));
            points.Add(new Point(0, height));
            points.Add(new Point(width, height));
            points.Add(new Point(width, 0));
            Polyline polyline = new Polyline { StrokeThickness = _controlStrokeThickness, Points = points };
            polyline.SetBinding(Polyline.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            PointCollection pointsFill = new PointCollection();
            pointsFill.Add(new Point(0, height / 4));
            pointsFill.Add(new Point(0, height));
            pointsFill.Add(new Point(width, height));
            pointsFill.Add(new Point(width, height / 4));
            Polygon polygon = new Polygon { Points = pointsFill };
            polygon.SetBinding(Polygon.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
            canvas.Children.Add(polygon);
            canvas.Children.Add(polyline);

            return canvas;
        }
        #endregion Graphic

        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(InputMathFormula, NodeType.I, true);

            AddDefaultOutput(OutputMathFormula, NodeType.O, true);
        }

        private Point InputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position, 2));
        }

        private Point OutputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position + this.Height, 2));
        }
        #endregion default IO
    }
}
