﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(EnumTypes.SchemeTypes.CoalProcessingScheme, true, "Wyciąg")]
    public class Extractor : CoalProcessingControlBase
    {
        private double _rectangleHeightPercent =  4 / 15;

        static Extractor()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Extractor), new FrameworkPropertyMetadata(typeof(Extractor)));
        }

        public Extractor()
        {
        }

        #region graphic
        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic());
            }
        }

        private UIElement GetGraphic()
        {
            var _controlStrokeThickness = this.ElementStrokeThickness;
            var _linesStrokeThickness = this.ElementStrokeThickness / 2.0;
            double width = this.Width;
            double height = this.Height;
            double rectangeHeight = _controlStrokeThickness * 3.0;

            Canvas canvas = new Canvas();
            Line line = new Line { StrokeThickness = _controlStrokeThickness, X1 = _controlStrokeThickness / 2, Y1 = 0, X2 = _controlStrokeThickness / 2, Y2 = height / 2 - rectangeHeight / 2 };
            line.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Line line1 = new Line { StrokeThickness = _controlStrokeThickness, X1 = _controlStrokeThickness / 2, Y1 = height / 2 + rectangeHeight / 2, X2 = _controlStrokeThickness / 2, Y2 = height };
            line1.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Line line2 = new Line { StrokeThickness = _controlStrokeThickness, X1 = width - _controlStrokeThickness / 2, Y1 = 0, X2 = width - _controlStrokeThickness / 2, Y2 = height };
            line2.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            PointCollection points = new PointCollection();
            points.Add(new Point(2 * _controlStrokeThickness, height / 2 - rectangeHeight / 2));
            points.Add(new Point(2 * _controlStrokeThickness, height / 2 + rectangeHeight / 2));
            points.Add(new Point(width - 2 * _controlStrokeThickness, height / 2));
            points.Add(new Point(width - 2 * _controlStrokeThickness, height / 2 - rectangeHeight));
            Polygon polygon = new Polygon { StrokeThickness = _linesStrokeThickness, Points = points };
            polygon.SetBinding(Polygon.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            polygon.SetBinding(Polygon.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));

            Line line3 = new Line { StrokeThickness = _linesStrokeThickness,
                X1 = 4 * width / 10, Y1 = height / 2 + 3 * rectangeHeight / 4 - 1 * _linesStrokeThickness,
                X2 = 4 * width / 10, Y2 = height - rectangeHeight / 4 - 1 * _linesStrokeThickness
            };
            line3.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Line line4 = new Line { StrokeThickness = _linesStrokeThickness,
                X1 = 6 * width / 10, Y1 = height / 2 + 3 * rectangeHeight / 4,
                X2 = 6 * width / 10, Y2 = height - rectangeHeight / 4 };
            line4.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            PointCollection arrowPoints = new PointCollection();
            arrowPoints.Add(new Point(4 * width / 10, height - rectangeHeight / 4));
            arrowPoints.Add(new Point(4 * width / 10 - _linesStrokeThickness / 2.0, height - rectangeHeight / 4 - 1 * _linesStrokeThickness));
            arrowPoints.Add(new Point(4 * width / 10 + _linesStrokeThickness / 2.0, height - rectangeHeight / 4 - 1 * _linesStrokeThickness));
            Polygon arrowPolygon = new Polygon { Points = arrowPoints, StrokeThickness = _linesStrokeThickness };
            arrowPolygon.SetBinding(Polygon.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            PointCollection arrowPoints1 = new PointCollection();
            arrowPoints1.Add(new Point(6 * width / 10, height - height / 2 + 3 * rectangeHeight / 4 - 1 * _linesStrokeThickness));
            arrowPoints1.Add(new Point(6 * width / 10 - _linesStrokeThickness / 2.0, height / 2 + 3 * rectangeHeight / 4));
            arrowPoints1.Add(new Point(6 * width / 10 + _linesStrokeThickness / 2.0, height / 2 + 3 * rectangeHeight / 4));
            Polygon arrowPolygon1 = new Polygon { Points = arrowPoints1, StrokeThickness = _linesStrokeThickness };
            arrowPolygon1.SetBinding(Polygon.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            canvas.Children.Add(line);
            canvas.Children.Add(line1);
            canvas.Children.Add(line2);
            canvas.Children.Add(polygon);
            canvas.Children.Add(line3);
            canvas.Children.Add(line4);
            canvas.Children.Add(arrowPolygon);
            canvas.Children.Add(arrowPolygon1);

            return canvas;
        }
        #endregion graphic

        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(OutputMathFormula, NodeType.O, true);

        }

        private Point OutputMathFormula()
        {
            return new Point(Math.Round(this.X_Position, 2), Math.Round(this.Y_Position + this.Height / 2.0, 2));
        }
        #endregion default IO
    }
}
