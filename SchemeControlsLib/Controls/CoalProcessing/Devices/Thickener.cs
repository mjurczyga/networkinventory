﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(SchemeTypes.CoalProcessingScheme, true, "Zagęszczacz promieniowy")]
    public class Thickener : CoalProcessingControlBase
    {
        static Thickener()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Thickener), new FrameworkPropertyMetadata(typeof(Thickener)));
        }

        public Thickener()
        {
        }

        #region Graphic
        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic());
            }
        }

        private FrameworkElement GetGraphic()
        {
            var _controlStrokeThickness = this.ElementStrokeThickness;

            Canvas canvas = new Canvas();
            var width = this.Width;
            var height = this.Height;

            PointCollection leftPoints = new PointCollection();
            leftPoints.Add(new Point(+_controlStrokeThickness / 2, 0));
            leftPoints.Add(new Point(+_controlStrokeThickness / 2, height / 6));
            leftPoints.Add(new Point(height / 6, height / 6));

            Polyline leftPolyline = new Polyline { StrokeThickness = _controlStrokeThickness, Points = leftPoints };
            leftPolyline.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            PointCollection rightPoints = new PointCollection();
            rightPoints.Add(new Point(width - _controlStrokeThickness / 2, 0));
            rightPoints.Add(new Point(width - _controlStrokeThickness / 2, height / 6));
            rightPoints.Add(new Point(width - height / 6, height / 6));

            Polyline rightPolyline = new Polyline { StrokeThickness = _controlStrokeThickness, Points = rightPoints };
            rightPolyline.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            PointCollection centerPoints = new PointCollection();
            centerPoints.Add(new Point(height / 6 + _controlStrokeThickness / 2, 0));
            centerPoints.Add(new Point(height / 6 + _controlStrokeThickness / 2, height / 2));
            centerPoints.Add(new Point(width / 2, height));
            centerPoints.Add(new Point(width - height / 6 - _controlStrokeThickness / 2, height / 2));
            centerPoints.Add(new Point(width - height / 6 - _controlStrokeThickness / 2, 0));

            Polyline centerPolyline = new Polyline { StrokeThickness = _controlStrokeThickness, Points = centerPoints };
            centerPolyline.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            PointCollection insidePoints = new PointCollection();
            insidePoints.Add(new Point(height / 6 + (((width - height / 3) / 2) * 4 * _controlStrokeThickness) / (.5 * height), height / 2));
            insidePoints.Add(new Point(width / 2, height - 4 * _controlStrokeThickness));
            insidePoints.Add(new Point(width - height / 6 - (((width - height / 3) / 2) * 4 * _controlStrokeThickness) / (.5 * height), height / 2));

            Polyline insidePolyline = new Polyline { StrokeThickness = _controlStrokeThickness, Points = insidePoints };
            insidePolyline.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Line line = new Line { StrokeThickness = _controlStrokeThickness, X1 = width / 2, Y1 = height / 6, X2 = width / 2, Y2 = height - 4 * _controlStrokeThickness };
            line.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            PointCollection fillPoints = new PointCollection();
            fillPoints.Add(new Point(height / 6 + _controlStrokeThickness / 2, height / 4));
            fillPoints.Add(new Point(height / 6 + _controlStrokeThickness / 2, height / 2));
            fillPoints.Add(new Point(width / 2, height));
            fillPoints.Add(new Point(width - height / 6 - _controlStrokeThickness / 2, height / 2));
            fillPoints.Add(new Point(width - height / 6 - _controlStrokeThickness / 2, height / 4));

            Polygon polygon = new Polygon { StrokeThickness = _controlStrokeThickness, Points = fillPoints };
            //polygon.SetBinding(Polygon.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            polygon.SetBinding(Polygon.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));
            canvas.Children.Add(polygon);

            canvas.Children.Add(leftPolyline);
            canvas.Children.Add(rightPolyline);
            canvas.Children.Add(centerPolyline);
            canvas.Children.Add(insidePolyline);
            canvas.Children.Add(line);


            return canvas;
        }
        #endregion Graphic
    }
}
