﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Shapes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(SchemeTypes.CoalProcessingScheme, true, "Zwałowarka")]
    public class Stacker : CoalProcessingControlBase
    {
        static Stacker()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Stacker), new FrameworkPropertyMetadata(typeof(Stacker)));
        }

        public Stacker()
        {
        }

        #region Graphic
        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic());
            }
        }

        private FrameworkElement GetGraphic()
        {
            var _controlStrokeThickness = this.ElementStrokeThickness;

            StackerShape stackerShape = new StackerShape { StrokeThickness = _controlStrokeThickness };

            stackerShape.SetBinding(StackerShape.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            stackerShape.SetBinding(StackerShape.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));


            return stackerShape;
        }
        #endregion Graphic
    }
}
