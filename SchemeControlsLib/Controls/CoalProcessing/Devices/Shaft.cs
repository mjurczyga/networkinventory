﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Shapes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(SchemeTypes.CoalProcessingScheme, true, "Szyb")]
    public sealed class Shaft : CoalProcessingControlBase
    {
        static Shaft()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Shaft), new FrameworkPropertyMetadata(typeof(Shaft)));
        }

        public Shaft()
        {

        }

        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.DrawShape());
            }

        }


        private FrameworkElement DrawShape()
        {
            var _width = this.Width;
            var _height = this.Height;
            var _controlStrokeThickness = this.ElementStrokeThickness;

            Ellipse ellipse = new Ellipse { StrokeThickness = _controlStrokeThickness, Width = _width, Height = _height };
            ellipse.SetBinding(Ellipse.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            ellipse.SetBinding(Ellipse.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));

            HammersShape hammersShape = new HammersShape { StrokeThickness = _controlStrokeThickness / 2.0, Margin = new Thickness(2 * _controlStrokeThickness),
                Width = Math.Max(0.0, _width - 4 * _controlStrokeThickness), Height = Math.Max(0.0, _height - 4 * _controlStrokeThickness) };
            hammersShape.SetBinding(HammersShape.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            hammersShape.SetBinding(HammersShape.FillProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Canvas canvas = new Canvas();
            canvas.Children.Add(ellipse);
            canvas.Children.Add(hammersShape);

            return canvas;
        }

        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(OutputMathFormula, NodeType.O);
        }

        private Point OutputMathFormula()
        {
            return new Point(this.X_Position + this.Width / 2.0, this.Y_Position + this.Height);
        }

    }
}
