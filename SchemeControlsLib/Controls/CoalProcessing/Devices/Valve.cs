﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(EnumTypes.SchemeTypes.CoalProcessingScheme, true, "Zawór")]
    public class Valve : CoalProcessingControlBase
    {
        static Valve()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Valve), new FrameworkPropertyMetadata(typeof(Valve)));
        }

        public Valve()
        {
            //CarrierType = CarrierTypes.Water;
        }

        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic());
            }
        }

        private UIElement GetGraphic()
        {
            double width = this.Width;
            double height = this.Height;
            double _controlStrokeThickness = this.ElementStrokeThickness;

            Canvas canvas = new Canvas { Width = width, Height = height };
            double shortLine = _controlStrokeThickness;//(width / 15) / Scale;
            double lineStrokeThickness = _controlStrokeThickness / 2.0;
            Ellipse ellipse = new Ellipse { Width = width, Height = height, StrokeThickness = _controlStrokeThickness };
            ellipse.SetBinding(Ellipse.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            ellipse.SetBinding(Ellipse.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));

            Line line = new Line { StrokeThickness = lineStrokeThickness, X1 = width / 4 + lineStrokeThickness, Y1 = 3 * height / 4 - lineStrokeThickness, X2 = 3 * width / 4 - lineStrokeThickness, Y2 = height / 4 + lineStrokeThickness };
            line.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Line line_1 = new Line { StrokeThickness = lineStrokeThickness, X1 = width / 4 - shortLine + lineStrokeThickness, Y1 = 3 * height / 4 - shortLine - lineStrokeThickness, X2 = width / 4 + shortLine + lineStrokeThickness, Y2 = 3 * height / 4 + shortLine - lineStrokeThickness };
            line_1.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Line line_2 = new Line { StrokeThickness = lineStrokeThickness, X1 = 3 * width / 4 - shortLine - lineStrokeThickness, Y1 = height / 4 - shortLine + lineStrokeThickness, X2 = 3 * width / 4 + shortLine - lineStrokeThickness, Y2 = height / 4 + shortLine + lineStrokeThickness };
            line_2.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Line line1 = new Line { StrokeThickness = lineStrokeThickness, X1 = width / 4 + lineStrokeThickness, Y1 = height / 4 + lineStrokeThickness, X2 = 3 * width / 4 - lineStrokeThickness, Y2 = 3 * height / 4 - lineStrokeThickness };
            line1.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Line line11 = new Line { StrokeThickness = lineStrokeThickness, X1 = width / 4 - shortLine + lineStrokeThickness, Y1 = height / 4 + shortLine + lineStrokeThickness, X2 = width / 4 + shortLine + lineStrokeThickness, Y2 = height / 4 - shortLine + lineStrokeThickness };
            line11.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Line line12 = new Line { StrokeThickness = lineStrokeThickness, X1 = 3 * width / 4 - shortLine - lineStrokeThickness, Y1 = 3 * height / 4 + shortLine - lineStrokeThickness, X2 = 3 * width / 4 + shortLine - lineStrokeThickness, Y2 = 3 * height / 4 - shortLine - lineStrokeThickness };
            line12.SetBinding(Line.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            canvas.Children.Add(ellipse);
            canvas.Children.Add(line);
            canvas.Children.Add(line_1);
            canvas.Children.Add(line_2);
            canvas.Children.Add(line1);
            canvas.Children.Add(line11);
            canvas.Children.Add(line12);
            return canvas;
        }

        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(InputMathFormula, NodeType.I, true);

            AddDefaultOutput(OutputMathFormula, NodeType.O, true);
        }

        private Point InputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position, 2));
        }

        private Point OutputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position + this.Height, 2));
        }
        #endregion default IO
    }
}
