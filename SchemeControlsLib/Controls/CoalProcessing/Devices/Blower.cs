﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(EnumTypes.SchemeTypes.CoalProcessingScheme, true, "Dmuchawa")]
    public class Blower : CoalProcessingControlBase
    {
        static Blower()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Blower), new FrameworkPropertyMetadata(typeof(Blower)));
        }

        public Blower()
        {
        }

        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.DrawControl());
            }
        }

        private FrameworkElement DrawControl()
        {
            var _controlStrokeThickness = this.ElementStrokeThickness;
            var _width = this.Width;
            var _height = this.Height;

            Rectangle rectangle = new Rectangle { StrokeThickness = _controlStrokeThickness, Width = _width, Height = _height };
            rectangle.SetBinding(Rectangle.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            rectangle.SetBinding(Rectangle.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));

            return rectangle;
        }

        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(OutputMathFormula, NodeType.O, true);
        }

        private Point OutputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width, 2), Math.Round(this.Y_Position + this.Height / 2.0, 2));
        }
        #endregion default IO
    }
}
