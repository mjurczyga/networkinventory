﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Shapes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(SchemeTypes.CoalProcessingScheme, true, "Przenośnik z wózkiem zrzutowym")]
    public class ConveyorWithDumpTrolley : CoalProcessingControlBase
    {
        static ConveyorWithDumpTrolley()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ConveyorWithDumpTrolley), new FrameworkPropertyMetadata(typeof(ConveyorWithDumpTrolley)));
        }

        public ConveyorWithDumpTrolley()
        {
        }

        #region Graphic
        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic());
            }
        }

        private FrameworkElement GetGraphic()
        {
            Canvas shapeCanvas = new Canvas();
            var _width = this.Width;
            var _height = this.Height;
            var _controlStrokeThickness = this.ElementStrokeThickness;
            var _circleDiameter = Math.Ceiling(_height / 3.0);

            ConveyorWithDumpTrolleyShape conveyor = new ConveyorWithDumpTrolleyShape { Width = this.Width, Height = this.Height, StrokeThickness = _controlStrokeThickness };
            conveyor.SetBinding(ConveyorWithDumpTrolleyShape.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            conveyor.SetBinding(ConveyorWithDumpTrolleyShape.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));

            Ellipse e1 = new Ellipse { StrokeThickness = _controlStrokeThickness, Margin = new Thickness(0, _height - _circleDiameter, 0, 0), Height = _circleDiameter, Width = _circleDiameter };
            e1.SetBinding(Ellipse.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Ellipse e2 = new Ellipse { StrokeThickness = _controlStrokeThickness, Margin = new Thickness((_width - _circleDiameter) / 2, _circleDiameter - _controlStrokeThickness / 2, 0, 0), Height = _circleDiameter, Width = _circleDiameter };
            e2.SetBinding(Ellipse.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Ellipse e3 = new Ellipse { StrokeThickness = _controlStrokeThickness, Margin = new Thickness(_width - _circleDiameter, _height - _circleDiameter, 0, 0), Height = _circleDiameter, Width = _circleDiameter };
            e3.SetBinding(Ellipse.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            Ellipse e4 = new Ellipse { StrokeThickness = _controlStrokeThickness, Margin = new Thickness((_width + _circleDiameter * 2) / 2, 0, 0, 0), Height = _circleDiameter, Width = _circleDiameter };
            e4.SetBinding(Ellipse.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));

            shapeCanvas.Children.Add(conveyor);
           // shapeCanvas.Children.Add(e1);
            //shapeCanvas.Children.Add(e2);
            //shapeCanvas.Children.Add(e3);
            //shapeCanvas.Children.Add(e4);



            return shapeCanvas;
        }
        #endregion Graphic
    }
}
