﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing
{
    [EditorType(SchemeTypes.CoalProcessingScheme, true, "Podajnik")]
    public class Feeder : CoalProcessingControlBase
    {
        static Feeder()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Feeder), new FrameworkPropertyMetadata(typeof(Feeder)));
        }

        public Feeder()
        {
        }

        #region Graphic
        public override void UpdateGraphic()
        {
            base.UpdateGraphic();

            if (_controlGraphic_PART_Grid != null)
            {
                _controlGraphic_PART_Grid.Children.Add(this.GetGraphic());
            }
        }

        private FrameworkElement GetGraphic()
        {
            var _controlStrokeThickness = this.ElementStrokeThickness;
            var width = this.Width;
            var height = this.Height;

            PointCollection points = new PointCollection();
            points.Add(new Point(_controlStrokeThickness * 0.5, _controlStrokeThickness * 0.5));
            points.Add(new Point(width - _controlStrokeThickness * 0.5, (height - _controlStrokeThickness) * 0.5));
            points.Add(new Point(width - _controlStrokeThickness * 0.5, height - _controlStrokeThickness * 0.5));
            points.Add(new Point(_controlStrokeThickness * 0.5, height - _controlStrokeThickness * 0.5));
            points.Add(new Point(_controlStrokeThickness * 0.5, _controlStrokeThickness * 0.5));

            //points.Add(new Point(0, 0));
            //points.Add(new Point(width, height * 0.5));
            //points.Add(new Point(width, height));
            //points.Add(new Point(0, height));
            //points.Add(new Point(0, 0));
            Polygon polygon = new Polygon { StrokeThickness = _controlStrokeThickness, Points = points };

            polygon.SetBinding(Polygon.StrokeProperty, BindingUtilities.GetBinding("ElementStroke", this, this.ElementStroke));
            polygon.SetBinding(Polygon.FillProperty, BindingUtilities.GetBinding("InternalBackgroundBrush", this, this.InternalBackgroundBrush));




            return polygon;
        }
        #endregion Graphic

        #region default IO
        protected override void AddDefaultOutputs()
        {
            AddDefaultOutput(OutputMathFormula, NodeType.O, true);
            AddDefaultOutput(InputMathFormula, NodeType.I, true);
        }

        private Point OutputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position, 2));
        }

        private Point InputMathFormula()
        {
            return new Point(Math.Round(this.X_Position + this.Width / 2.0, 2), Math.Round(this.Y_Position + this.Height, 2));
        }
        #endregion default IO
    }
}
