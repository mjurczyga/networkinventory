﻿using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing.Shapes
{
    public class ContainerShape : Shape
    {
        private double _rectangleRatio = 2.0 / 3.0;
        private double _rectangleRatio1 = 1.0 / 6.0;

        public ContainerShape() : base()
        {
            this.Stretch = System.Windows.Media.Stretch.Uniform;
            //this.StrokeLineJoin = PenLineJoin.Round;

            DependencyPropertyDescriptor.FromProperty(ContainerShape.StrokeThicknessProperty, typeof(ContainerShape)).AddValueChanged(this, StrokeThickness_PropertyChanged); ;
        }

        private void StrokeThickness_PropertyChanged(object sender, EventArgs e)
        {
            if (sender is ContainerShape conveyorShape)
            {
                conveyorShape.InvalidateVisual();
            }
        }

        protected override Geometry DefiningGeometry { get { return GetGeometry(); } }

        private Geometry GetGeometry()
        {
            GeometryGroup geometryGroup = new GeometryGroup() { FillRule = FillRule.Nonzero };

            double maxWidth = Math.Max(0.0, this.Width - StrokeThickness);
            double maxHeight = Math.Max(0.0, this.Height - StrokeThickness);
            double rectangleRatioHeight = maxHeight * _rectangleRatio;
            double rectangleRatio1Height = maxHeight * _rectangleRatio1;

            LineSegment topLine = new LineSegment(new Point(maxWidth, 0), true);
            LineSegment rightLine = new LineSegment(new Point(maxWidth, rectangleRatioHeight), true);
            LineSegment triangleRightLine = new LineSegment(new Point(maxWidth / 2.0, maxHeight), true);
            LineSegment triangleLeftLine = new LineSegment(new Point(0, rectangleRatioHeight), true);

            List<PathSegment> pathSegments = new List<PathSegment>();
            pathSegments.Add(topLine);
            pathSegments.Add(rightLine);
            pathSegments.Add(triangleRightLine);
            pathSegments.Add(triangleLeftLine);

            PathFigure pathFigure = new PathFigure(new Point(0, 0), pathSegments, true);
            PathGeometry pathGeometry = new PathGeometry();
            pathGeometry.Figures.Add(pathFigure);        
            geometryGroup.Children.Add(pathGeometry);

            LineGeometry line = new LineGeometry(new Point(0, rectangleRatio1Height), new Point(maxWidth, rectangleRatio1Height));
            geometryGroup.Children.Add(line);

            return geometryGroup;
        }
    }
}
