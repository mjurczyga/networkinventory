﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing.Shapes
{
    public class DistributorShape : Shape
    {
        public DistributorShape()
        {
            this.Stretch = Stretch.Uniform;
            this.StrokeLineJoin = PenLineJoin.Round;
        }
        protected override Geometry DefiningGeometry
        {
            get { return GetGeometry(); }
        }

        private Geometry GetGeometry()
        {
            PathGeometry data = new PathGeometry();

            Geometry distributor = Geometry.Parse("m 1.7037638,291.939 6.2632888,-3.61612 m 4.3539344,-0.0576 6.26329,3.6161 M 9.9999998,277.29032 v 7.23223 M 12.43746,287 A 2.4374601,2.437461 0 0 1 10.000001,289.43746 2.4374601,2.437461 0 0 1 7.5625404,287 2.4374601,2.437461 0 0 1 10.000001,284.56254 2.4374601,2.437461 0 0 1 12.43746,287 Z m 7.31238,0 A 9.7498402,9.7498442 0 0 1 10.000001,296.74984 9.7498402,9.7498442 0 0 1 0.25016004,287 9.7498402,9.7498442 0 0 1 10.000001,277.25016 9.7498402,9.7498442 0 0 1 19.74984,287 Z");

            return distributor;
        }
    }
}
