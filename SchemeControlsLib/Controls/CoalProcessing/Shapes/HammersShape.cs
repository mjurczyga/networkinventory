﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing.Shapes
{
    public class HammersShape : Shape
    {
        public HammersShape()
        {
            this.Stretch = Stretch.Uniform;
            this.StrokeLineJoin = PenLineJoin.Round;
        }
        protected override Geometry DefiningGeometry
        {
            get { return GetGeometry(); }
        }

        private Geometry GetGeometry()
        {
            Geometry hammers = Geometry.Parse("M 138.85156 71.927734 L 34.388672 176.25781 L 71.742188 213.64062 L 113.72852 171.70898 L 167.89062 225.91602 L 109.30859 284.54492 L 130.82617 306.03516 L 189.38477 247.42773 L 251.16797 309.26172 L 272.68555 287.77148 L 210.88086 225.91602 L 264.63477 172.11719 L 306.21094 213.64062 L 343.56445 176.25781 L 301.98633 134.73438 L 301.99414 134.72656 L 280.47656 113.23633 L 280.46875 113.24414 L 239.10156 71.927734 L 201.74805 109.3125 L 243.11719 150.62695 L 189.38477 204.40234 L 135.24609 150.21875 L 176.20508 109.3125 L 138.85156 71.927734 z ");

            return hammers;
        }
    }
}
