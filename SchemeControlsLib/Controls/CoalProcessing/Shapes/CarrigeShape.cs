﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing.Shapes
{
    public class CarrigeShape : Shape
    {
        public CarrigeShape()
        {
           this.Stretch = Stretch.Fill;
        }

        protected override Geometry DefiningGeometry
        {
            get { return GetGeometry(); }
        }

        private Geometry GetGeometry()
        {
            double maxWidth = Math.Max(0.0, this.Width - StrokeThickness);
            double maxHeight = Math.Max(0.0, this.Height - StrokeThickness);
            double halfStrokeThickness = StrokeThickness / 2.0;


            double rectangleHeight = (4.0 * maxHeight) / 5.0;
            double circleDiameter = maxHeight / 5.0 + halfStrokeThickness;
            double circleRadius = circleDiameter / 2.0;

            LineSegment topLine = new LineSegment(new Point(maxWidth, 0), true);
            LineSegment rightLine = new LineSegment(new Point(maxWidth, rectangleHeight - circleDiameter), true);

            ArcSegment arcSegment = new ArcSegment(
                new Point(maxWidth - circleDiameter, rectangleHeight),  
                new Size(circleDiameter, circleDiameter), 90.0, false, SweepDirection.Clockwise, true);

            LineSegment rightBottomLine = new LineSegment(new Point(circleDiameter, rectangleHeight), true);
            ArcSegment arcSegment1 = new ArcSegment(new Point(0, rectangleHeight - circleDiameter),  new Size(circleDiameter, circleDiameter), 90.0, false, SweepDirection.Clockwise, true);


            List<PathSegment> pathSegments = new List<PathSegment>();
            pathSegments.Add(topLine);
            pathSegments.Add(rightLine);
            pathSegments.Add(arcSegment);
            pathSegments.Add(rightBottomLine);
            pathSegments.Add(arcSegment1);

            PathFigure pathFigure = new PathFigure(new Point(0, 0), pathSegments, true);
            PathGeometry pathGeometry = new PathGeometry();
            pathGeometry.Figures.Add(pathFigure);

            EllipseGeometry leftWheel = new EllipseGeometry()
            {
                Center = new Point(circleDiameter * 2, rectangleHeight + circleRadius),
                RadiusX = circleRadius, RadiusY = circleRadius
            };
            EllipseGeometry rightWheel = new EllipseGeometry()
            {
                Center = new Point(maxWidth - circleDiameter * 2, rectangleHeight + circleRadius),
                RadiusX = circleRadius, RadiusY = circleRadius
            };

            GeometryGroup geometryGroup = new GeometryGroup();

            geometryGroup.Children.Add(pathGeometry);
            geometryGroup.Children.Add(leftWheel);
            geometryGroup.Children.Add(rightWheel);

            return geometryGroup;
        }
    }
}
