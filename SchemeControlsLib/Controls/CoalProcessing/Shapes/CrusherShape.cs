﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing.Shapes
{
    public class CrusherShape : Shape
    {
        public CrusherShape()
        {
            this.Stretch = Stretch.Uniform;
            this.StrokeLineJoin = PenLineJoin.Round;
        }
        protected override Geometry DefiningGeometry
        {
            get { return GetGeometry(); }
        }

        private Geometry GetGeometry()
        {
            string data;
            data = "m 16.425614,281.63626 -6.371265,9.55691 M 4.7449597,280.04345 c 7.4331443,15.32138 7.4331443,15.39723 7.4331443,15.39723 m 7.57192,-8.44069 A 9.750025,9.750033 0 0 1 10,296.75002 9.750025,9.750033 0 0 1 0.24997541,286.99999 9.750025,9.750033 0 0 1 10,277.24997 a 9.750025,9.750033 0 0 1 9.750024,9.75002 z";

            return Geometry.Parse(data);
        }
    }
}
