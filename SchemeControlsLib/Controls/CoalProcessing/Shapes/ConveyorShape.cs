﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing.Shapes
{
    public class ConveyorShape : Shape
    {
        public ConveyorShape() : base()
        {
            this.Stretch = System.Windows.Media.Stretch.Uniform;
            this.StrokeLineJoin = PenLineJoin.Round;

            DependencyPropertyDescriptor.FromProperty(ConveyorShape.StrokeThicknessProperty, typeof(ConveyorShape)).AddValueChanged(this, StrokeThickness_PropertyChanged); ;
        }

        private void StrokeThickness_PropertyChanged(object sender, EventArgs e)
        {
            if(sender is ConveyorShape conveyorShape)
            {
                
            }
        }

        public Orientation Orientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("Orientation", typeof(Orientation), typeof(ConveyorShape), new PropertyMetadata(Orientation.Horizontal, Orientation_PropertyChanged));

        private static void Orientation_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is ConveyorShape conveyorShape)
            {
                conveyorShape.InvalidateMeasure();
                conveyorShape.InvalidateVisual();
            }
        }


        protected override Geometry DefiningGeometry { get { return GetGeometry(); } }


        private Geometry GetGeometry()
        {
            GeometryGroup data = new GeometryGroup() { FillRule = FillRule.Nonzero };

            double maxWidth = Math.Max(0.0, this.Width - StrokeThickness);
            double maxHeight = Math.Max(0.0, this.Height - StrokeThickness);

            if (Orientation == Orientation.Horizontal)
            {
                var circleRadius = maxHeight / 2.0;
                var circleDiameter = circleRadius * 2.0;

                if (maxWidth - circleRadius >= 0)
                {
                    LineSegment topLine = new LineSegment(new Point(maxWidth - circleRadius, 0), true);
                    LineSegment rightLine = new LineSegment(new Point(maxWidth - circleRadius, maxHeight), false);
                    LineSegment bottomLine = new LineSegment(new Point(circleRadius, maxHeight), true);
                    LineSegment leftLine = new LineSegment(new Point(circleRadius, 0), false);

                    List<PathSegment> pathSegments = new List<PathSegment>();
                    pathSegments.Add(topLine);
                    pathSegments.Add(rightLine);
                    pathSegments.Add(bottomLine);
                    pathSegments.Add(leftLine);

                    PathFigure pathFigure = new PathFigure(new Point(circleRadius, 0), pathSegments, true);
                    PathGeometry pathGeometry = new PathGeometry();
                    pathGeometry.Figures.Add(pathFigure);

                    EllipseGeometry leftCircle = new EllipseGeometry() { Center = new Point(circleRadius, circleRadius), RadiusX = circleRadius, RadiusY = circleRadius };
                    EllipseGeometry rightCircle = new EllipseGeometry() { Center = new Point(maxWidth - circleRadius, circleRadius), RadiusX = circleRadius, RadiusY = circleRadius };

                    data.Children.Add(pathGeometry);
                    data.Children.Add(leftCircle);
                    data.Children.Add(rightCircle);
                }
                else
                {
                    FormattedText text = new FormattedText("SIZE ERROR", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Segoe UI"), Math.Abs(this.Height - 1), Brushes.Red);
                    Geometry geometry = text.BuildGeometry(new Point(1, 1));
                    geometry.Transform = new RotateTransform(90);
                    data.Children.Add(geometry);
                }
            }
            else if (Orientation == Orientation.Vertical)
            {
                var circleRadius = maxWidth / 2.0;
                var circleDiameter = circleRadius * 2.0;

                if (maxHeight - circleRadius >= 0)
                {
                    LineSegment leftLine = new LineSegment(new Point(0, maxHeight - circleRadius), true);
                    ArcSegment topArc = new ArcSegment(new Point(maxWidth, maxHeight - circleRadius), new Size(circleRadius, circleRadius), 180, false, SweepDirection.Clockwise, false);
                    LineSegment rightLine = new LineSegment(new Point(maxWidth, circleRadius), true);
                    ArcSegment bottomArc = new ArcSegment(new Point(0, circleRadius), new Size(circleRadius, circleRadius), 180, false, SweepDirection.Clockwise, false);

                    List<PathSegment> pathSegments = new List<PathSegment>();
                    pathSegments.Add(leftLine);
                    pathSegments.Add(topArc);
                    pathSegments.Add(rightLine);
                    pathSegments.Add(bottomArc);

                    PathFigure pathFigure = new PathFigure(new Point(0, circleRadius), pathSegments, true);
                    PathGeometry pathGeometry = new PathGeometry();
                    pathGeometry.Figures.Add(pathFigure);

                    EllipseGeometry topCircle = new EllipseGeometry() { Center = new Point(circleRadius, circleRadius), RadiusX = circleRadius, RadiusY = circleRadius };
                    EllipseGeometry bottomCircle = new EllipseGeometry() { Center = new Point(circleRadius, maxHeight - circleRadius), RadiusX = circleRadius, RadiusY = circleRadius };

                    data.Children.Add(pathGeometry);
                    data.Children.Add(topCircle);
                    data.Children.Add(bottomCircle);
                }
                else
                {
                    FormattedText text = new FormattedText("SIZE ERROR", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Segoe UI"), Math.Abs(this.Width - 1), Brushes.Red);
                    Geometry geometry = text.BuildGeometry(new Point(1, 1));
                    data.Children.Add(geometry);
                }
            }
            return data;
        }
    }
}
