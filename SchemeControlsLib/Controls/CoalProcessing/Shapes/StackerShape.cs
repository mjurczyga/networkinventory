﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing.Shapes
{
    public class StackerShape : Shape
    {
        public StackerShape()
        {
            this.Stretch = Stretch.Uniform;
        }

        protected override Geometry DefiningGeometry
        {
            get { return GetGeometry(); }
        }

        private Geometry GetGeometry()
        {
            Geometry data = Geometry.Parse("M 0.25007528,277.25006 H 6.2499246 v 2.49985 H 0.25007528 Z m 5.99990092,-9e-5 H 18.750025 v 13.50005 H 6.2499762 Z m 0.9999365,13.49994 H 17.750088 v 2.50017 H 7.2499127 Z m -1.9998302,2.50018 H 19.749917 v 3.49984 H 5.2500825 Z");

            return data;
        }
    }
}
