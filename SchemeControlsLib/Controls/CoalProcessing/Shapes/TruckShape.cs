﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing.Shapes
{
    public class TruckShape : Shape
    {
        public TruckShape()
        {
            this.Stretch = Stretch.Uniform;
            this.StrokeLineJoin = PenLineJoin.Round;
        }
        protected override Geometry DefiningGeometry
        {
            get { return GetGeometry(); }
        }

        private Geometry GetGeometry()
        {
            PathGeometry data = new PathGeometry();

            Geometry distributor = Geometry.Parse("m 32.58,285 v 6.75 m 17.165525,-6.67246 v 6.66798 H 32.560426 0.25447558 v -6.66798 H 32.57516 v -12.82307 h 7.070151 z M 42.194016,294.25 a 2.4440165,2.4440124 0 0 1 -2.444017,2.44402 2.4440165,2.4440124 0 0 1 -2.444017,-2.44402 2.4440165,2.4440124 0 0 1 2.444017,-2.44402 2.4440165,2.4440124 0 0 1 2.444017,2.44402 z m -28.999999,0 A 2.444017,2.444018 0 0 1 10.75,296.69401 2.444017,2.444018 0 0 1 8.3059833,294.25 2.444017,2.444018 0 0 1 10.75,291.80598 2.444017,2.444018 0 0 1 13.194017,294.25 Z");

            return distributor;
        }
    }
}
