﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CoalProcessing.Shapes
{
    public class SludgeConveyorShape : Shape
    {
        private bool _updateSize = true;
        private double _defaultHeight = 22;
        private double _defaultWidth = 70;
        private double _defaultWhellDiameter = 12;

        private Dictionary<Point, double> _HeightDictionary = new Dictionary<Point, double>()
        {
            { new Point(0,0.75), 20 },
            { new Point(0.75,1.5), 21 },
            { new Point(1.505,2.5), 22 },
            { new Point(2.505,3.5), 24 },
            { new Point(3.505,99), 26 },
        };

        public SludgeConveyorShape() : base()
        {
            this.Stretch = System.Windows.Media.Stretch.Uniform;
            this.StrokeLineJoin = PenLineJoin.Round;

            if (_updateSize)
            {
                this.Width = _defaultWidth;
                this.Height = _defaultHeight;
                this.WheelDiameter = _defaultWhellDiameter;
            }

            //DependencyPropertyDescriptor.FromProperty(SludgeConveyorShape.WidthProperty, typeof(SludgeConveyorShape)).AddValueChanged(this, WidthProperty_PropertyChanged);
            //DependencyPropertyDescriptor.FromProperty(SludgeConveyorShape.HeightProperty, typeof(SludgeConveyorShape)).AddValueChanged(this, HeightProperty_PropertyChanged);
        }

        private void HeightProperty_PropertyChanged(object sender, EventArgs e)
        {
            if(sender is SludgeConveyorShape conveyorShape)
            {
                if (conveyorShape._updateSize)
                {
                    conveyorShape._updateSize = false;
                    var ratio = Height / conveyorShape._defaultHeight;
                    conveyorShape.Width = ratio * conveyorShape._defaultWidth;
                    conveyorShape.WheelDiameter = ratio * conveyorShape._defaultWhellDiameter;
                    conveyorShape._updateSize = true;
                }
            }
        }

        private void WidthProperty_PropertyChanged(object sender, EventArgs e)
        {
            if (sender is SludgeConveyorShape conveyorShape)
            {
                if (conveyorShape._updateSize)
                {
                    conveyorShape._updateSize = false;
                    var ratio = Width / conveyorShape._defaultWidth;
                    conveyorShape.Height = ratio * conveyorShape._defaultHeight;
                    conveyorShape.WheelDiameter = ratio * conveyorShape._defaultWhellDiameter;
                    conveyorShape._updateSize = true;
                }
            }
        }

        public double WheelDiameter
        {
            get { return (double)GetValue(WheelDiameterProperty); }
            set { SetValue(WheelDiameterProperty, value); }
        }

        public static readonly DependencyProperty WheelDiameterProperty =
            DependencyProperty.Register("WheelDiameter", typeof(double), typeof(SludgeConveyorShape), new PropertyMetadata(12.0, WheelDiameter_PropertyChanged));

        private static void WheelDiameter_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SludgeConveyorShape conveyorShape && double.TryParse(e.NewValue?.ToString(), out double newVal))
            {
                if (conveyorShape._updateSize)
                {
                    //conveyorShape._updateSize = false;
                    //var ratio = newVal / conveyorShape._defaultWhellDiameter;
                    //conveyorShape.Width = ratio * conveyorShape._defaultWidth;
                    //conveyorShape.Height = ratio * conveyorShape._defaultHeight;
                    //conveyorShape._updateSize = true;
                }

                conveyorShape.InvalidateMeasure();
                conveyorShape.InvalidateVisual();
            }
        }

        public Orientation Orientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("Orientation", typeof(Orientation), typeof(SludgeConveyorShape), new PropertyMetadata(Orientation.Horizontal, Orientation_PropertyChanged));

        private static void Orientation_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is SludgeConveyorShape conveyorShape)
            {
                conveyorShape.InvalidateMeasure();
                conveyorShape.InvalidateVisual();
            }
        }


        protected override Geometry DefiningGeometry { get { return GetGeometry(); } }


        private Geometry GetGeometry()
        {
            GeometryGroup data = new GeometryGroup() { FillRule = FillRule.Nonzero };

            double maxWidth = Math.Max(0.0, this.Width - StrokeThickness);
            double maxConveyorHeight = Math.Max(0.0, this.WheelDiameter - StrokeThickness);

            if (Orientation == Orientation.Horizontal)
            {
                var circleRadius = maxConveyorHeight / 2.0;
                var circleDiameter = circleRadius * 2.0;

                if (maxWidth - circleRadius >= 0)
                {
                    var triangleOffset = (2.0 * circleRadius) / 3.0;
                    var triangleV = (circleRadius) / Math.Sqrt(3);
                    var triangleA = maxWidth / 2.0 - circleDiameter - 2.0 * triangleOffset;
                    var triangleH = triangleA * Math.Sqrt(3) / 2.0;

                    LineSegment topLine = new LineSegment(new Point(maxWidth - circleRadius, 0), true);
                    LineSegment rightLine = new LineSegment(new Point(maxWidth - circleRadius, maxConveyorHeight), false);

                    LineSegment rightSludgeLine1 = new LineSegment(new Point(maxWidth - circleDiameter - triangleOffset - triangleV, maxConveyorHeight), true);
                    LineSegment rightSludgeLine2 = new LineSegment(new Point((maxWidth + triangleA) / 2.0 + triangleOffset, triangleH + circleRadius), true);
                    LineSegment rightSludgeLine3 = new LineSegment(new Point(maxWidth / 2.0 + triangleOffset, circleRadius), true);
                    LineSegment rightSludgeLine4 = new LineSegment(new Point(maxWidth - circleDiameter - triangleOffset, circleRadius), true);
                    LineSegment rightSludgeLine5 = new LineSegment(new Point(maxWidth - circleDiameter - triangleOffset - triangleV, maxConveyorHeight), true);
                    LineSegment rightSludgeLine6 = new LineSegment(new Point(maxWidth / 2.0 + triangleOffset + triangleV, maxConveyorHeight), false);

                    LineSegment conveyorCenterLine = new LineSegment(new Point(maxWidth / 2.0 - triangleOffset - triangleV, maxConveyorHeight), true);
                    LineSegment leftSludgeLine1 = new LineSegment(new Point((maxWidth - triangleA) / 2.0 - triangleOffset, triangleH + circleRadius), true);
                    LineSegment leftSludgeLine2 = new LineSegment(new Point(maxWidth / 2.0 - triangleOffset - triangleA, circleRadius), true);
                    LineSegment leftSludgeLine3 = new LineSegment(new Point(maxWidth / 2.0 - triangleOffset, circleRadius), true);
                    LineSegment leftSludgeLine4 = new LineSegment(new Point(maxWidth / 2.0 - triangleOffset - triangleV, maxConveyorHeight), true);
                    LineSegment leftSludgeLine5 = new LineSegment(new Point(maxWidth / 2.0 - triangleA - triangleOffset + triangleV, maxConveyorHeight), false);

                    LineSegment conveyorLeftBottomLine = new LineSegment(new Point(circleRadius, maxConveyorHeight), true);
                    LineSegment leftLine = new LineSegment(new Point(circleRadius, 0), false);

                    List<PathSegment> pathSegments = new List<PathSegment>();
                    pathSegments.Add(topLine);
                    pathSegments.Add(rightLine);
                    pathSegments.Add(rightSludgeLine1);
                    pathSegments.Add(rightSludgeLine2);
                    pathSegments.Add(rightSludgeLine3);
                    pathSegments.Add(rightSludgeLine4);
                    pathSegments.Add(rightSludgeLine5);
                    pathSegments.Add(rightSludgeLine6);
                    pathSegments.Add(conveyorCenterLine);
                    pathSegments.Add(leftSludgeLine1);
                    pathSegments.Add(leftSludgeLine2);
                    pathSegments.Add(leftSludgeLine3);
                    pathSegments.Add(leftSludgeLine4);
                    pathSegments.Add(leftSludgeLine5);

                    pathSegments.Add(conveyorLeftBottomLine);
                    pathSegments.Add(leftLine);

                    PathFigure pathFigure = new PathFigure(new Point(circleRadius, 0), pathSegments, true);
                    PathGeometry pathGeometry = new PathGeometry();
                    pathGeometry.Figures.Add(pathFigure);

                    EllipseGeometry leftCircle = new EllipseGeometry() { Center = new Point(circleRadius, circleRadius), RadiusX = circleRadius, RadiusY = circleRadius };
                    EllipseGeometry rightCircle = new EllipseGeometry() { Center = new Point(maxWidth - circleRadius, circleRadius), RadiusX = circleRadius, RadiusY = circleRadius };

                    data.Children.Add(pathGeometry);
                    data.Children.Add(leftCircle);
                    data.Children.Add(rightCircle);
                }
                else
                {
                    FormattedText text = new FormattedText("SIZE ERROR", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Segoe UI"), Math.Abs(this.Height - 1), Brushes.Red);
                    Geometry geometry = text.BuildGeometry(new Point(1, 1));
                    geometry.Transform = new RotateTransform(90);
                    data.Children.Add(geometry);
                }
            }

            else if (Orientation == Orientation.Vertical)
            {

                    FormattedText text = new FormattedText("NOT FOUND", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Segoe UI"), Math.Abs(this.Width - 1), Brushes.Red);
                    Geometry geometry = text.BuildGeometry(new Point(1, 1));
                    data.Children.Add(geometry);
                
            }
            return data;
        }
    }
}
