﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Converters
{
    public class StateToElementBackgroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value.GetType() == typeof(int))
            {
                switch (value)
                {
                    case 0: return Brushes.SandyBrown;
                    case 1: return Brushes.Green;
                    case 2: return Brushes.Red;
                    case 3: return Brushes.Black;
                    default: return Brushes.Black;

                }
            }
            return Brushes.Transparent;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
