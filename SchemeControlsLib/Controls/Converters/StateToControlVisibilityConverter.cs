﻿using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace SchemeControlsLib.Controls.Converters
{
    class StateToControlVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value.GetType() == typeof(int))
            {
                switch (value)
                {
                    case 0:
                        if (parameter.GetType() == typeof(SwitchStatuses))
                        {
                            SwitchStatuses switchTypes = (SwitchStatuses)parameter;
                            if (switchTypes == SwitchStatuses.None)
                                return Visibility.Visible;
                            else if (switchTypes == SwitchStatuses.Open)
                                return Visibility.Hidden;
                            else
                                return Visibility.Hidden;
                        }
                        else
                            return Visibility.Hidden;

                    case 1:

                        if (parameter.GetType() == typeof(SwitchStatuses))
                        {
                            SwitchStatuses switchTypes = (SwitchStatuses)parameter;
                            if (switchTypes == SwitchStatuses.None)
                                return Visibility.Hidden;
                            else if (switchTypes == SwitchStatuses.Open)
                                return Visibility.Visible;
                            else
                                return Visibility.Hidden;
                        }
                        else
                            return Visibility.Hidden;

                    case 2:
                        if (parameter.GetType() == typeof(SwitchStatuses))
                        {
                            SwitchStatuses switchTypes = (SwitchStatuses)parameter;
                            if (switchTypes == SwitchStatuses.None)
                                return Visibility.Hidden;
                            else if (switchTypes == SwitchStatuses.Open)
                                return Visibility.Hidden;
                            else
                                return Visibility.Visible;
                        }
                        else
                            return Visibility.Hidden;

                    default: return Visibility.Hidden;

                }
            }
            return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
