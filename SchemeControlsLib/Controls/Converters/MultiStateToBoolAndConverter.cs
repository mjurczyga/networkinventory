﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SchemeControlsLib.Controls.Converters
{
    public class MultiStateToBoolAndConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.All(x => int.TryParse(x?.ToString(), out int val)))
            {
                foreach(var value in values)
                {
                    if(int.TryParse(value.ToString(), out int val))
                        if (val != 2)
                            return 0;
                }
                return 2;
            }
            return 0;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
