﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace SchemeControlsLib.Controls.Converters
{
    public class BoolenToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (bool.TryParse(value?.ToString(), out bool bVal))
            {
                bool parameterValue = false;
                if (!string.IsNullOrEmpty(parameter?.ToString()))
                {
                    parameterValue = true;
                }

                if (bVal || parameterValue && parameter.ToString().ToUpper() == bVal.ToString().ToUpper())
                    return Visibility.Visible;
            }

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
