﻿using SchemeControlsLib.Controls.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls
{
    public class BrushChangedEventArgs : EventArgs
    {
        public BrushChangedEventArgs(Brush newBrush)
        {
            NewBrush = newBrush;
        }

        private Brush _NewBrush;
        public Brush NewBrush
        {
            get { return _NewBrush; }
            set { _NewBrush = value; }
        }
    }

    public class LibSettingsSetter : DependencyObject
    {
        [EditorProperty("Kolor statusu \"Brak danych - '0'\"")]
        public Brush STATUS_0_BRUSH
        {
            get { return (Brush)GetValue(STATUS_0_BRUSHProperty); }
            set { SetValue(STATUS_0_BRUSHProperty, value); }
        }

        public static readonly DependencyProperty STATUS_0_BRUSHProperty =
            DependencyProperty.Register("STATUS_0_BRUSH", typeof(Brush), typeof(LibSettingsSetter), new PropertyMetadata(LibSettings.STATUS_0_BRUSH, STATUS_0_BRUSH_PropertyChanged));

        private static void STATUS_0_BRUSH_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is LibSettingsSetter libSettingsSetter && e.NewValue is Brush brush)
            {
                LibSettings.STATUS_0_BRUSH = brush;
                libSettingsSetter.OnBrushChanged(new BrushChangedEventArgs(brush));
            }
        }

        [EditorProperty("Kolor statusu \"Niekatywny - '1'\"")]
        public Brush STATUS_1_BRUSH
        {
            get { return (Brush)GetValue(STATUS_1_BRUSHProperty); }
            set { SetValue(STATUS_1_BRUSHProperty, value); }
        }

        public static readonly DependencyProperty STATUS_1_BRUSHProperty =
            DependencyProperty.Register("STATUS_1_BRUSH", typeof(Brush), typeof(LibSettingsSetter), new PropertyMetadata(LibSettings.STATUS_1_BRUSH, STATUS_1_BRUSH_PropertyChanged));

        private static void STATUS_1_BRUSH_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is LibSettingsSetter libSettingsSetter && e.NewValue is Brush brush)
            {
                LibSettings.STATUS_1_BRUSH = brush;
                libSettingsSetter.OnBrushChanged(new BrushChangedEventArgs(brush));
            }
        }

        [EditorProperty("Kolor statusu \"Aktywny - '2'\"")]
        public Brush STATUS_2_BRUSH
        {
            get { return (Brush)GetValue(STATUS_2_BRUSHProperty); }
            set { SetValue(STATUS_2_BRUSHProperty, value); }
        }

        public static readonly DependencyProperty STATUS_2_BRUSHProperty =
            DependencyProperty.Register("STATUS_2_BRUSH", typeof(Brush), typeof(LibSettingsSetter), new PropertyMetadata(LibSettings.STATUS_2_BRUSH, STATUS_2_BRUSH_PropertyChanged));

        private static void STATUS_2_BRUSH_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is LibSettingsSetter libSettingsSetter && e.NewValue is Brush brush)
            {
                LibSettings.STATUS_2_BRUSH = brush;
                libSettingsSetter.OnBrushChanged(new BrushChangedEventArgs(brush));
            }
        }

        [EditorProperty("Kolor statusu \"Poza zakresem\"")]
        public Brush STATUS_DEFAULT_BRUSH
        {
            get { return (Brush)GetValue(STATUS_DEFAULT_BRUSHProperty); }
            set { SetValue(STATUS_DEFAULT_BRUSHProperty, value); }
        }

        public static readonly DependencyProperty STATUS_DEFAULT_BRUSHProperty =
            DependencyProperty.Register("STATUS_DEFAULT_BRUSH", typeof(Brush), typeof(LibSettingsSetter), new PropertyMetadata(LibSettings.STATUS_DEFAULT_BRUSH, STATUS_DEFAULT_BRUSH_PropertyChanged));

        private static void STATUS_DEFAULT_BRUSH_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is LibSettingsSetter libSettingsSetter && e.NewValue is Brush brush)
            {
                LibSettings.STATUS_DEFAULT_BRUSH = brush;
                libSettingsSetter.OnBrushChanged(new BrushChangedEventArgs(brush));
            }
        }

        public event EventHandler<BrushChangedEventArgs> BrushChanged;

        protected virtual void OnBrushChanged(BrushChangedEventArgs e)
        {
            EventHandler<BrushChangedEventArgs> handler = this.BrushChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public void RefreshProperties()
        {
            STATUS_0_BRUSH = LibSettings.STATUS_0_BRUSH;
            STATUS_1_BRUSH = LibSettings.STATUS_1_BRUSH;
            STATUS_2_BRUSH = LibSettings.STATUS_2_BRUSH;
            STATUS_DEFAULT_BRUSH = LibSettings.STATUS_DEFAULT_BRUSH;
        }
    }

    public static class LibSettings
    {
        public static Brush STATUS_0_BRUSH { get; set; }
        public static Brush STATUS_1_BRUSH { get; set; }
        public static Brush STATUS_2_BRUSH { get; set; }
        public static Brush STATUS_DEFAULT_BRUSH { get; set; }

        static LibSettings()
        {
            STATUS_0_BRUSH = Brushes.SandyBrown;
            STATUS_1_BRUSH = Brushes.Green;
            STATUS_2_BRUSH = Brushes.Red;
            STATUS_DEFAULT_BRUSH = Brushes.Black;
        }

        public static void SetProperties(Dictionary<string, string> keyValuePairs)
        {
            if(keyValuePairs != null)
            {
                if (keyValuePairs.ContainsKey(nameof(STATUS_0_BRUSH)))
                {
                    STATUS_0_BRUSH = new BrushConverter().ConvertFromString(keyValuePairs["STATUS_0_BRUSH"]) as SolidColorBrush;
                    STATUS_0_BRUSH.Freeze();
                }
                if (keyValuePairs.ContainsKey(nameof(STATUS_1_BRUSH)))
                {
                    STATUS_1_BRUSH = new BrushConverter().ConvertFromString(keyValuePairs["STATUS_1_BRUSH"]) as SolidColorBrush;
                    STATUS_1_BRUSH.Freeze();
                }
                if (keyValuePairs.ContainsKey(nameof(STATUS_2_BRUSH)))
                {
                    STATUS_2_BRUSH = new BrushConverter().ConvertFromString(keyValuePairs["STATUS_2_BRUSH"]) as SolidColorBrush;
                    STATUS_2_BRUSH.Freeze();
                }
                if (keyValuePairs.ContainsKey(nameof(STATUS_DEFAULT_BRUSH)))
                {
                    STATUS_DEFAULT_BRUSH = new BrushConverter().ConvertFromString(keyValuePairs["STATUS_DEFAULT_BRUSH"]) as SolidColorBrush;
                    STATUS_DEFAULT_BRUSH.Freeze();
                }

            }
        }

        //public static FrameworkElement GetPropertiesChngedElement()
        //{
        //    var result = new StackPanel();

            
        //}



    }
}
