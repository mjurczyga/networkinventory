﻿using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Helpers;
using SchemeControlsLib.SchemeControlTypes;
using SchemeControlsLib.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SchemeControlsLib.Controls.LibControls
{
    public class SchemeCanvasInfobar : ObservableControl
    {
        public LibMessage Message
        {
            get { return (LibMessage)GetValue(MyPropertyProperty); }
            set { SetValue(MyPropertyProperty, value); }
        }

        public static readonly DependencyProperty MyPropertyProperty =
            DependencyProperty.Register("Message", typeof(LibMessage), typeof(SchemeCanvasInfobar), new PropertyMetadata(null));



        public string SchemeCanvasName
        {
            get { return (string)GetValue(SchemeCanvasNameProperty); }
            set { SetValue(SchemeCanvasNameProperty, value); }
        }

        public static readonly DependencyProperty SchemeCanvasNameProperty =
            DependencyProperty.Register("SchemeCanvasName", typeof(string), typeof(SchemeCanvasInfobar), new PropertyMetadata(null, SchemeCanvasName_PropertyChanged));

        private static void SchemeCanvasName_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SchemeCanvasInfobar schemeCanvasNavigationBar = d as SchemeCanvasInfobar;
            if (schemeCanvasNavigationBar != null)
                schemeCanvasNavigationBar.FindSchemeCanvas(e.NewValue.ToString());
        }

        public SpecificPlace AcctualZoom
        {
            get { return (SpecificPlace)GetValue(AcctualZoomProperty); }
            set { SetValue(AcctualZoomProperty, value); }
        }

        public static readonly DependencyProperty AcctualZoomProperty =
            DependencyProperty.Register("AcctualZoom", typeof(SpecificPlace), typeof(SchemeCanvasInfobar), new PropertyMetadata(new SpecificPlace(0), AcctualZoom_PropertyChanged));

        private static void AcctualZoom_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        private Visibility _ActualZoomVisibility;
        public Visibility ActualZoomVisibility
        {
            get { return _ActualZoomVisibility; }
            set { _ActualZoomVisibility = value; NotifyPropertyChanged("ActualZoomVisibility"); }
        }


        private SchemeCanvas _EditorSchemeCanvas = null;
        public SchemeCanvas EditorSchemeCanvas
        {
            get { return _EditorSchemeCanvas; }
            private set { _EditorSchemeCanvas = value; }
        }     


        private void FindSchemeCanvas(string name)
        {
            if (TextExt.CheckIfIsControlName(name))
            {
                FrameworkElement element = UIHelper.FindUserControl<FrameworkElement>(this);
                if (element != null)
                    EditorSchemeCanvas = element.FindName(name) as SchemeCanvas;
            }          
        }

        static SchemeCanvasInfobar()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SchemeCanvasInfobar), new FrameworkPropertyMetadata(typeof(SchemeCanvasInfobar)));
        }

        public SchemeCanvasInfobar()
        {
            Message = LibMessagerService.GetInstance().CurrentMessage;
        }
    }
}
