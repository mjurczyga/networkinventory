﻿using GalaSoft.MvvmLight.Command;
using SchemeControlsLib.Models;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using VISU.Controls;

namespace SchemeControlsLib.Controls.LibControls
{
    public class ElementCommonPropertiesControl : NotifyControl
    {
        private FrameworkElement _FrameworkElement = null;
        private SchemeCanvas _SchemeCanvas;

        private object _ControlContext;
        public object ControlContext
        {
            get { return _ControlContext; }
            private set { _ControlContext = value; NotifyPropertyChanged("ControlContext"); }
        }

        private ICommand _ChangeForAllCommand;
        public ICommand ChangeForAllCommand
        {
            get { return _ChangeForAllCommand; }
            private set { _ChangeForAllCommand = value; NotifyPropertyChanged("ChangeForAllCommand"); }
        }



        public Brush ButtonBrush
        {
            get { return (Brush)GetValue(ButtonBrushProperty); }
            set { SetValue(ButtonBrushProperty, value); }
        }

        public static readonly DependencyProperty ButtonBrushProperty =
            DependencyProperty.Register("ButtonBrush", typeof(Brush), typeof(ElementCommonPropertiesControl));



        static ElementCommonPropertiesControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ElementCommonPropertiesControl), new FrameworkPropertyMetadata(typeof(ElementCommonPropertiesControl)));
        }

        public ElementCommonPropertiesControl(SchemeCanvas schemeCanvas, FrameworkElement frameworkElement)
        {
            _FrameworkElement = frameworkElement;
            _SchemeCanvas = schemeCanvas;
            InitCommands();
        }

        private void InitCommands()
        {
            ChangeForAllCommand = new RelayCommand(OnChangeForAllCommand);
        }

        private void OnChangeForAllCommand()
        {
            if(_FrameworkElement != null && _SchemeCanvas != null)
            {
                var commonElementProperties = ElementPropertiesModels.GetPropertiesByElementType(_FrameworkElement).Where(x => x.Value.CommonProperty == true).ToDictionary(x => x.Key, y => y.Value);
                foreach(var child in _SchemeCanvas.Children)
                {
                    if (child is FrameworkElement frameworkElement)
                    {
                        var commonChildProperties = ElementPropertiesModels.GetPropertiesByElementType(frameworkElement).Where(x => x.Value.CommonProperty == true).ToDictionary(x => x.Key, y => y.Value);
                        foreach (var commonElementProperty in commonChildProperties)
                        {
                            var commonChildProperty = commonChildProperties.Where(x => x.Key == commonElementProperty.Key).FirstOrDefault();
                            if(commonChildProperty.Key != null)
                            {
                                var elementProdpd = DependencyPropertyFinder.GetControlSpecificDependencyProperty(_FrameworkElement, commonChildProperty.Key, commonChildProperty.Value).ToDictionary(x => x.Key as DependencyProperty, x => x.Value).FirstOrDefault();

                                var childProdpd = DependencyPropertyFinder.GetControlSpecificDependencyProperty(frameworkElement, commonChildProperty.Key, commonChildProperty.Value).ToDictionary(x => x.Key as DependencyProperty, x => x.Value).FirstOrDefault();

                                if (elementProdpd.Key != null && childProdpd.Key != null)
                                {
                                    var newValue = _FrameworkElement.GetValue(elementProdpd.Key);
                                    frameworkElement.SetValue(childProdpd.Key, newValue);
                                }
                            }

                        }
                    }
                }
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.GenerateContext();
        }

        private void GenerateContext()
        {
            if(_FrameworkElement != null)
            {
                ControlContext = new ElementPropertiesControl(_FrameworkElement, true) { Foreground = this.Foreground };
            }

        }
    }
}
