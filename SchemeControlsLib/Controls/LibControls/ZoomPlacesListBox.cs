﻿using GalaSoft.MvvmLight.Command;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces.CoreElementsInterfaces;
using SchemeControlsLib.Helpers;
using SchemeControlsLib.SchemeControlTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.LibControls
{
    public class ZoomPlacesListBox : ObservableControl
    {

        #region SchemeCanvas load specific places (ItemsSource)
        public object  SchemeCanvas
        {
            get { return (object)GetValue(SchemeCanvasProperty); }
            set { SetValue(SchemeCanvasProperty, value); }
        }

        public static readonly DependencyProperty SchemeCanvasProperty =
            DependencyProperty.Register("SchemeCanvas", typeof(object), typeof(ZoomPlacesListBox), new PropertyMetadata(null, SchemeCanvas_PropertyChanged));

        private static void SchemeCanvas_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ZoomPlacesListBox zoomPlacesListBox = d as ZoomPlacesListBox;
            if (zoomPlacesListBox != null)
            {
                if (e.NewValue is SchemeCanvas)
                {
                    zoomPlacesListBox.AddSchemeLoadedEvent((SchemeCanvas)e.NewValue);
                }

                if (e.OldValue is SchemeCanvas)
                {
                    zoomPlacesListBox.RemoveSchemeLoadedEvent((SchemeCanvas)e.OldValue);
                }
            }
        }

        private void AddSchemeLoadedEvent(SchemeCanvas schemeCanvas)
        {
            schemeCanvas.SchemeLoaded += Canvas_SchemeLoaded;
        }

        private void RemoveSchemeLoadedEvent(SchemeCanvas schemeCanvas)
        {
            schemeCanvas.SchemeLoaded -= Canvas_SchemeLoaded;
        }

        private void Canvas_SchemeLoaded(object sender, Events.SchemeLoadedEventArgs e)
        {
            var specZoomPlaces = e.Children.OfType<ISpecificZoomPlace>();
            Dictionary<string, SpecificPlace> specificPlaces = new Dictionary<string, SpecificPlace>();
            specificPlaces.Add("Całość", new SpecificPlace(0, 0, 1));
            foreach (var specZoomPlace in specZoomPlaces)
            {
                if (!specificPlaces.ContainsKey(specZoomPlace.PlaceName))
                    specificPlaces.Add(specZoomPlace.PlaceName, specZoomPlace.SpecificPlace);
            }

            if(SpecificPlacesDict == null || SpecificPlacesDict.Count == 0)
            {
                SpecificPlacesDict = specificPlaces;
            }
        }
        #endregion

        public KeyValuePair<string, SpecificPlace> SelectedItem
        {
            get { return (KeyValuePair<string, SpecificPlace>)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(KeyValuePair<string, SpecificPlace>), typeof(ZoomPlacesListBox));

        public IDictionary<string, SpecificPlace> SpecificPlacesDict
        {
            get { return (IDictionary<string, SpecificPlace>)GetValue(SpecificPlacesDictProperty); }
            set { SetValue(SpecificPlacesDictProperty, value); }
        }

        public static readonly DependencyProperty SpecificPlacesDictProperty =
            DependencyProperty.Register("SpecificPlacesDict", typeof(IDictionary<string, SpecificPlace>), typeof(ZoomPlacesListBox));

        public Thickness TextPadding
        {
            get { return (Thickness)GetValue(TextPaddingProperty); }
            set { SetValue(TextPaddingProperty, value); }
        }

        public static readonly DependencyProperty TextPaddingProperty =
            DependencyProperty.Register("TextPadding", typeof(Thickness), typeof(ZoomPlacesListBox), new PropertyMetadata(new Thickness(5)));

        #region Header
        public string Header
        {
            get { return (string)GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }

        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register("Header", typeof(string), typeof(ZoomPlacesListBox), new PropertyMetadata("ZoomListBox"));


        public double HeaderFontSize
        {
            get { return (double)GetValue(HeaderFontSizeProperty); }
            set { SetValue(HeaderFontSizeProperty, value); }
        }

        public static readonly DependencyProperty HeaderFontSizeProperty =
            DependencyProperty.Register("HeaderFontSize", typeof(double), typeof(ZoomPlacesListBox), new PropertyMetadata(15.0, OnHeaderFontSize_PropertyChanged));

        private static void OnHeaderFontSize_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ZoomPlacesListBox zoomPlacesListBox = (ZoomPlacesListBox)d;
            zoomPlacesListBox.MarkFontSize = (double)e.NewValue + 4.0;
        }

        private double _MarkFontSize;
        public double MarkFontSize
        {
            get { return _MarkFontSize; }
            private set { _MarkFontSize = value; NotifyPropertyChanged("MarkFontSize"); }
        }
        #endregion

        #region ListBoxProps
        public Brush ListBoxBackground
        {
            get { return (Brush)GetValue(ListBoxBackgroundProperty); }
            set { SetValue(ListBoxBackgroundProperty, value); }
        }

        public static readonly DependencyProperty ListBoxBackgroundProperty =
            DependencyProperty.Register("ListBoxBackground", typeof(Brush), typeof(ZoomPlacesListBox));

        public Brush ListBoxForeground
        {
            get { return (Brush)GetValue(ListBoxForegroundProperty); }
            set { SetValue(ListBoxForegroundProperty, value); }
        }
        public static readonly DependencyProperty ListBoxForegroundProperty =
            DependencyProperty.Register("ListBoxForeground", typeof(Brush), typeof(ZoomPlacesListBox));

        public double ListBoxFontSize
        {
            get { return (double)GetValue(ListBoxFontSizeProperty); }
            set { SetValue(ListBoxFontSizeProperty, value); }
        }

        public static readonly DependencyProperty ListBoxFontSizeProperty =
            DependencyProperty.Register("ListBoxFontSize", typeof(double), typeof(ZoomPlacesListBox), new PropertyMetadata(12.0));
        #endregion

        private Visibility _ListVisibility = Visibility.Collapsed;
        public Visibility ListVisibility { get { return _ListVisibility; } private set { _ListVisibility = value; NotifyPropertyChanged("ListVisibility"); } }

        static ZoomPlacesListBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ZoomPlacesListBox), new FrameworkPropertyMetadata(typeof(ZoomPlacesListBox)));
        }

        public ZoomPlacesListBox()
        {
            this.MouseLeftButtonDown += ZoomPlacesListBox_MouseLeftButtonDown;
        }

        private void ZoomPlacesListBox_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                if (ListVisibility == Visibility.Visible)
                    ListVisibility = Visibility.Collapsed;
                else if (ListVisibility == Visibility.Collapsed)
                    ListVisibility = Visibility.Visible;
            }
        }
    }
}
