﻿using SchemeControlsLib.Base;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.LibControls
{
    [TemplatePart(Name = "PART_MainTextBox", Type = typeof(TextBox))]
    [TemplatePart(Name = "PART_Button", Type = typeof(Viewbox))]
    [TemplatePart(Name = "PART_Popup_TextBox", Type = typeof(TextBox))]
    public class TextFrame : ObservableControl
    {
        private const string PART_MainTextBox = "PART_MainTextBox";
        private const string PART_Button = "PART_Button";
        private const string PART_Popup_TextBox = "PART_Popup_TextBox";
        private TextBox _PART_MainTextBox;
        private TextBox _PART_Popup_TextBox;
        private Viewbox _PART_Button;

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(TextFrame), new PropertyMetadata(null));

        #region IsOpenProperty
        public bool IsOpen
        {
            get { return (bool)GetValue(IsOpenProperty); }
            set { SetValue(IsOpenProperty, value); }
        }

        public static readonly DependencyProperty IsOpenProperty =
            DependencyProperty.Register("IsOpen", typeof(bool), typeof(TextFrame), new PropertyMetadata(IsOpen_PropertyChanged));

        private static void IsOpen_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is TextFrame textFrame)
            {

            }
        }
        #endregion IsOpenProperty

        #region TextAligment
        public TextAlignment TextAlignment
        {
            get { return (TextAlignment)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }

        public static readonly DependencyProperty TextAlignmentProperty =
            DependencyProperty.Register("TextAlignment", typeof(TextAlignment), typeof(TextFrame));
        #endregion TextAligment


        #region popup dimensions
        public double PopupWidth
        {
            get { return (double)GetValue(PopupWidthProperty); }
            set { SetValue(PopupWidthProperty, value); }
        }

        public static readonly DependencyProperty PopupWidthProperty =
            DependencyProperty.Register("PopupWidth", typeof(double), typeof(TextFrame), new PropertyMetadata(150.0));

        public double PopupHeight
        {
            get { return (double)GetValue(PopupHeightProperty); }
            set { SetValue(PopupHeightProperty, value); }
        }

        public static readonly DependencyProperty PopupHeightProperty =
            DependencyProperty.Register("PopupHeight", typeof(double), typeof(TextFrame), new PropertyMetadata(200.0));
        #endregion popup dimensions


        public TextFrame()
        {

        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            _PART_MainTextBox = GetTemplateChild(PART_MainTextBox) as TextBox;
            _PART_Popup_TextBox = GetTemplateChild(PART_Popup_TextBox) as TextBox;
            _PART_Button = GetTemplateChild(PART_Button) as Viewbox;

            if (_PART_MainTextBox != null)
                AttacheEnterEvent(_PART_MainTextBox);
            if (_PART_Popup_TextBox != null)
                AttacheEnterEvent(_PART_Popup_TextBox);

            if (_PART_Button != null)
                _PART_Button.Child = this.GenerateButton();

            this.MouseDown += TextFrame_MouseDown;


        }

        private void AttacheEnterEvent(TextBox textBox)
        {
            if(textBox != null)
            {
                textBox.KeyDown += TextBox_KeyDown;
            }
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(sender is TextBox textBox && e.Key == Key.Enter)
            {
                textBox.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                IsOpen = false;
            }
        }

        private Border _PART_ShadowBorder;
        private Grid GenerateButton()
        {
            DrawingGroup drawingGroup = new DrawingGroup();
            drawingGroup.Children.Add(new GeometryDrawing(Brushes.White, null, Geometry.Parse("F1M16,16L0,16 0,0 16,0z")));
            drawingGroup.Children.Add(new GeometryDrawing(Brushes.Black, null, Geometry.Parse("m5,6.5 h6 c-3,3 -3,3 -3,3 0,0 0,0 -3,-3z")));

            var colorBoder = new Border() { Name = "_colorBoder", Background = new DrawingBrush(drawingGroup), BorderThickness = new Thickness(0.5), BorderBrush = Brushes.Black, Width = 16, Height = 16 };
            colorBoder.Margin = new Thickness(1);
            var shadowBorder = new Border() { Name = "_SchadowBorder", Background = Brushes.Black, Opacity = 0.4, Visibility = Visibility.Collapsed };
            colorBoder.Margin = new Thickness(1);
            _PART_ShadowBorder = shadowBorder;

            var resultGrid = new Grid() { Width = 17, Height = 17 };
            resultGrid.MouseEnter += ColorBoder_MouseEnter;
            resultGrid.MouseLeave += ColorBoder_MouseLeave;
            resultGrid.Children.Add(colorBoder);
            resultGrid.Children.Add(shadowBorder);

            return resultGrid;
        }

        private void ColorBoder_MouseLeave(object sender, MouseEventArgs e)
        {
            if (_PART_ShadowBorder != null)
            {
                _PART_ShadowBorder.Visibility = Visibility.Collapsed;
            }
        }

        private void ColorBoder_MouseEnter(object sender, MouseEventArgs e)
        {
            if (_PART_ShadowBorder != null)
            {
                _PART_ShadowBorder.Visibility = Visibility.Visible;
            }
        }

        private void TextFrame_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            IsOpen = !IsOpen;
        }


        static TextFrame()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TextFrame), new FrameworkPropertyMetadata(typeof(TextFrame)));
        }

        private Binding GetBinding(string path)
        {
            var result = new Binding(path);
            result.Source = this;
            return result;
        }
    }
}
