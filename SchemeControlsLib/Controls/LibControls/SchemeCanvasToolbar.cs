﻿using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.ElectricControls;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Helpers;
using SchemeControlsLib.Inputs;
using SchemeControlsLib.Services;
using SchemeControlsLib.Services.PLC;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;

namespace SchemeControlsLib.Controls.LibControls
{
    public class SchemeCanvasToolbar : ObservableControl
    {
        private Dispatcher _dispatcher = Dispatcher.CurrentDispatcher;

        public Visibility ITS_Visibility
        {
            get { return Settings.IsITSApp ? Visibility.Collapsed : Visibility.Visible; }
        }

        public string SchemeCanvasName
        {
            get { return (string)GetValue(SchemeCanvasNameProperty); }
            set { SetValue(SchemeCanvasNameProperty, value); }
        }

        public static readonly DependencyProperty SchemeCanvasNameProperty =
            DependencyProperty.Register("SchemeCanvasName", typeof(string), typeof(SchemeCanvasToolbar), new PropertyMetadata(null, SchemeCanvasName_PropertyChanged));

        private static void SchemeCanvasName_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SchemeCanvasToolbar schemeCanvasNavigationBar = d as SchemeCanvasToolbar;
            if(schemeCanvasNavigationBar != null)
                schemeCanvasNavigationBar.FindSchemeCanvas(e.NewValue.ToString());
        }

        private double _SliderValue;
        public double SliderValue
        {
            get { return _SliderValue; }
            set { _SliderValue = value; SetLinesStrokeThickness(_SliderValue);  NotifyPropertyChanged("SliderValue"); }
        }

        public ICommand GenerateAsixEvoVarsCommand
        {
            get { return (ICommand)GetValue(GenerateAsixEvoVarsCommandProperty); }
            set { SetValue(GenerateAsixEvoVarsCommandProperty, value); }
        }

        public static readonly DependencyProperty GenerateAsixEvoVarsCommandProperty =
            DependencyProperty.Register("GenerateAsixEvoVarsCommand", typeof(ICommand), typeof(SchemeCanvasToolbar), new PropertyMetadata(null, GenerateAsixEvoVarsCommand_PropertyChanged));

        private static void GenerateAsixEvoVarsCommand_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is SchemeCanvasToolbar schemeCanvasToolbar)
            {
                if (e.NewValue is ICommand)
                    schemeCanvasToolbar.GenerateAsixEvoVarsButtonVisibility = Visibility.Visible;
                else
                    schemeCanvasToolbar.GenerateAsixEvoVarsButtonVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _GenerateAsixEvoVarsButtonVisibility = Visibility.Collapsed;
        public Visibility GenerateAsixEvoVarsButtonVisibility
        {
            get { return _GenerateAsixEvoVarsButtonVisibility; }
            set { _GenerateAsixEvoVarsButtonVisibility = value; NotifyPropertyChanged("GenerateAsixEvoVarsButtonVisibility"); }
        }

        private string _SaveToolTip = "Zapisz";
        public string SaveToolTip
        {
            get { return _SaveToolTip; }
            set { _SaveToolTip = value; NotifyPropertyChanged("SaveToolTip"); }
        }



        public ICommand FitToTopCommand { get; set; }
        public ICommand FitToBottomCommand { get; set; }
        public ICommand FitToLeftCommand { get; set; }
        public ICommand FitToRightCommand { get; set; }
        public ICommand SetHorizontalDistanceCommand { get; set; }
        public ICommand SetVarticalDistanceCommand { get; set; }
        public ICommand SaveFileAsCommand { get; set; }
        public ICommand SaveFileCommand { get; set; }
        public ICommand LoadFileCommand { get; set; }
        public ICommand SavePLCCode { get; set; }
        public ICommand SchearchElement { get; set; }
        public ICommand LayerUpCommand { get; set; }
        public ICommand LayerDownCommand { get; set; }


        public ICommand StartNetworkUpdater { get; set; }
        public ICommand StopNetworkUpdater { get; set; }

        private SchemeCanvas _EditorSchemeCanvas = null;
        public SchemeCanvas EditorSchemeCanvas
        {
            get { return _EditorSchemeCanvas; }
            private set
            {
                _EditorSchemeCanvas = value;
                if (_EditorSchemeCanvas != null)
                {
                    _EditorSchemeCanvas.LoadedFileUriChangedEvent += _EditorSchemeCanvas_SchemePathChangedEvent;

                    var stackpanel = new StackPanel();

                    ElementPropertiesControl schemePropertiesDataContent = new ElementPropertiesControl(_EditorSchemeCanvas.LibSettingsSetter);
                    schemePropertiesDataContent.SetBinding(ElementPropertiesControl.ForegroundProperty, new Binding("Foreground") { Source = this });
                    schemePropertiesDataContent.SetBinding(ElementPropertiesControl.BackgroundProperty, new Binding("Background") { Source = this });
                    schemePropertiesDataContent.ElementPropertiesChangedEvent += OnElementPropertiesChanged;

                    var propertiesContent = new ElementPropertiesControl(_EditorSchemeCanvas) { TextMargin = new Thickness(0, 0, 5, 0) };
                    propertiesContent.SetBinding(ElementPropertiesControl.ForegroundProperty, new Binding("Foreground") { Source = this });
                    propertiesContent.SetBinding(ElementPropertiesControl.BackgroundProperty, new Binding("Background") { Source = this });

                    stackpanel.Children.Add(schemePropertiesDataContent);
                    stackpanel.Children.Add(propertiesContent);
                    PropertiesContent = stackpanel;
                }
            }
        }

        private void _EditorSchemeCanvas_SchemePathChangedEvent(object sender, Models.Parameters.DefaultEventArgs e)
        {
            if (e.NewValue != null && e.NewValue is string newValue)
                SaveToolTip = "Zapisz w lokalizacji: " + newValue;
            else
                SaveToolTip = "Zapisz";
        }

        private void OnElementPropertiesChanged(object obj, PropertiesChangedEventArgs args)
        {
            if (_EditorSchemeCanvas != null)
            {
                _EditorSchemeCanvas.InvalidateVisual();
            }
        }

        #region IP checker
        public bool IpCheckerIsRunning
        {
            get { return NetworkElementStateUpdater != null ? NetworkElementStateUpdater.IsRunning : false; }
        }

        public bool IpCheckerIsStopped
        {
            get { return NetworkElementStateUpdater != null ? NetworkElementStateUpdater.IsStopped : true; }
        }

        private NetworkElementStateUpdater NetworkElementStateUpdater;
        #endregion IP checker

        public object PropertiesContent { get; set; }

        static SchemeCanvasToolbar()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SchemeCanvasToolbar), new FrameworkPropertyMetadata(typeof(SchemeCanvasToolbar)));
        }

        public SchemeCanvasToolbar()
        {
            this.InitCommands();
            SliderValue = Settings.LinesStrokeThickness; //this.GetCurrentLinesStrokeThickness();
        }

        public void InitCommands()
        {
            FitToTopCommand = new RelayCommand(OnFitToTopCommand);
            FitToBottomCommand = new RelayCommand(OnFitToBottomCommand);
            FitToLeftCommand = new RelayCommand(OnFitToLeftCommand);
            FitToRightCommand = new RelayCommand(OnFitToRightCommand);
            SetHorizontalDistanceCommand = new RelayCommand(OnSetHorizontalDistanceCommand);
            SetVarticalDistanceCommand = new RelayCommand(OnSetVarticalDistanceCommand);
            SaveFileAsCommand = new RelayCommand(OnSaveFileAsCommand);
            SaveFileCommand = new RelayCommand(OnSaveFileCommand);
            LoadFileCommand = new RelayCommand(OnLoadFileCommand);
            SavePLCCode = new RelayCommand(OnSavePLCCode);
            SchearchElement = new RelayCommand(OnSchearchElement);
            LayerUpCommand = new RelayCommand(OnLayerUpCommand);
            LayerDownCommand = new RelayCommand(OnLayerDownCommand);


            StartNetworkUpdater = new RelayCommand(OnStartNetworkUpdater);
            StopNetworkUpdater = new RelayCommand(OnStopNetworkUpdater);
        }

        private void OnLayerDownCommand()
        {
            if(_EditorSchemeCanvas != null)
            {
                _EditorSchemeCanvas.LayerMoveDownSelection();
            }
        }

        private void OnLayerUpCommand()
        {
            if (_EditorSchemeCanvas != null)
            {
                _EditorSchemeCanvas.LayerMoveUpSelection();
            }
        }

        private void OnStopNetworkUpdater()
        {
            EditorSchemeCanvas?.StopNetworkSchemeUpdater();
            NotifyPropertyChanged("IpCheckerIsRunning");
            NotifyPropertyChanged("IpCheckerIsStopped");
        }

        private void OnStartNetworkUpdater()
        {
            NetworkElementStateUpdater = EditorSchemeCanvas?.StartNetworkSchemeUpdater();
            NotifyPropertyChanged("IpCheckerIsRunning");
            NotifyPropertyChanged("IpCheckerIsStopped");
        }

        #region search engine
        private string _SearchingPrase;
        public string SearchingPrase
        {
            get { return _SearchingPrase; }
            set { _SearchingPrase = value; NotifyPropertyChanged("SearchingPrase"); }
        }

        private void OnSchearchElement()
        {
            _EditorSchemeCanvas.SearchElement(SearchingPrase);
        }
        #endregion search engine

        private void OnSavePLCCode()
        {
            using (var fbd = new System.Windows.Forms.FolderBrowserDialog())
            {
                System.Windows.Forms.DialogResult result = fbd.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    CodeGenerator codeGenerator = new CodeGenerator();
                    codeGenerator.GeneratePLCCode(EditorSchemeCanvas, fbd.SelectedPath);
                }
            }
        }

        private void OnLoadFileCommand()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                //_EditorSchemeCanvas.LoadedFileUri = openFileDialog.FileName;
                WindowManager.ShowActionExecutingWindow(() => { _EditorSchemeCanvas.LoadScheme(openFileDialog.FileName); }, "Wczytywanie schematu...", false);
            }
        }

        private void OnSaveFileCommand()
        {
            if (string.IsNullOrEmpty(_EditorSchemeCanvas.LoadedFileUri))
            {
                SaveFileDialog openFileDialog = new SaveFileDialog();
                if (openFileDialog.ShowDialog() == true)
                {
                    WindowManager.ShowActionExecutingWindow(() => { _EditorSchemeCanvas.SaveScheme(openFileDialog.FileName); }, "Zapisywanie schematu...");
                    LibMessagerService.ShowMessage("Zapisano schemat w lokalizacji: " + openFileDialog.FileName, LibMessageTypes.INFO, SchemeControlsLibResources.GetIconBrushByKey("Saved"));
                }
            }
            else
            {
                WindowManager.ShowActionExecutingWindow(() => { _EditorSchemeCanvas.SaveScheme(_EditorSchemeCanvas.LoadedFileUri); }, "Zapisywanie schematu...");
                LibMessagerService.ShowMessage("Zapisano schemat w lokalizacji: " + _EditorSchemeCanvas.LoadedFileUri, LibMessageTypes.INFO, SchemeControlsLibResources.GetIconBrushByKey("Saved"));
            }
        }

        private void OnSaveFileAsCommand()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            if (saveFileDialog.ShowDialog() == true)
            {
                //_EditorSchemeCanvas.LoadedFileUri = saveFileDialog.FileName;
                WindowManager.ShowActionExecutingWindow(() => { _EditorSchemeCanvas.SaveScheme(saveFileDialog.FileName); }, "Zapisywanie schematu...");
                LibMessagerService.ShowMessage("Zapisano schemat w lokalizacji: " + saveFileDialog.FileName, LibMessageTypes.INFO, SchemeControlsLibResources.GetIconBrushByKey("Saved"));
            }
        }

        private void OnSetVarticalDistanceCommand()
        {
            if (EditorSchemeCanvas != null)
                EditorSchemeCanvas.SetVarticalDistance();
        }

        private void OnSetHorizontalDistanceCommand()
        {
            if (EditorSchemeCanvas != null)
                EditorSchemeCanvas.SetHorizontalDistance();
        }

        private void OnFitToRightCommand()
        {
            if (EditorSchemeCanvas != null)
                EditorSchemeCanvas.FitSelectionToRight();
        }

        private void OnFitToBottomCommand()
        {
            if (EditorSchemeCanvas != null)
                EditorSchemeCanvas.FitSelectionToBottom();
        }

        private void OnFitToLeftCommand()
        {
            if (EditorSchemeCanvas != null)
                EditorSchemeCanvas.FitSelectionToLeft();
        }

        private void OnFitToTopCommand()
        {
            if (EditorSchemeCanvas != null)
                EditorSchemeCanvas.FitSelectionToTop();
        }

        private void FindSchemeCanvas(string name)
        {
            if(TextExt.CheckIfIsControlName(name))
            {
                FrameworkElement element = UIHelper.FindUserControl<FrameworkElement>(this);
                if (element != null)
                    EditorSchemeCanvas = element.FindName(name) as SchemeCanvas;
            }
        }
        // TODO: ???
        private double GetCurrentLinesStrokeThickness()
        {
            if (EditorSchemeCanvas != null && EditorSchemeCanvas.Children != null)
            {
                var lines = EditorSchemeCanvas.Children.OfType<ILine>();
                var currentLine = lines.FirstOrDefault();
                if (currentLine != null)
                    return currentLine.StrokeThickness;
            }


            var line = new LineElement();
            return line.StrokeThickness;

        }

        private void SetLinesStrokeThickness(double strokeThickness)
        {
            if (EditorSchemeCanvas != null && EditorSchemeCanvas.Children != null)
            {
                var lines = EditorSchemeCanvas.Children.OfType<ILine>();
                var currentLineStroke = lines.FirstOrDefault()?.StrokeThickness;

                if (strokeThickness != currentLineStroke)
                {
                    foreach (var line in lines)
                    {
                        line.StrokeThickness = strokeThickness;
                    }
                }
            }
        }
    }
}
