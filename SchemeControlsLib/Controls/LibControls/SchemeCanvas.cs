﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing.Interfaces;
using SchemeControlsLib.Controls.CommonControls;
using SchemeControlsLib.Controls.Converters;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.Controls.ElectricControls;
using SchemeControlsLib.Controls.Events;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibElementsControls;
using SchemeControlsLib.Controls.NetworkControls.Interfaces;
using SchemeControlsLib.Helpers;
using SchemeControlsLib.Inputs;
using SchemeControlsLib.Models;
using SchemeControlsLib.Models.Parameters;
using SchemeControlsLib.SchemeControlTypes;
using SchemeControlsLib.Services;
using SchemeControlsLib.Services.Mapping;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SchemeControlsLib.Controls.LibControls
{
    public class SchemeCanvas : Canvas, INotifyPropertyChanged
    {
        public LibSettingsSetter LibSettingsSetter { get; set; } = new LibSettingsSetter();  

        #region Events
        public event EventHandler<SchemeLoadedEventArgs> SchemeLoaded;

        protected virtual void OnSchemeLoaded(SchemeLoadedEventArgs e)
        {
            EventHandler<SchemeLoadedEventArgs> handler = this.SchemeLoaded;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<SchemeStateChangedEventArgs> SchemeStateChanged;

        protected virtual void OnSchemeStateChanged(SchemeStateChangedEventArgs e)
        {
            EventHandler<SchemeStateChangedEventArgs> handler = this.SchemeStateChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<VarsBindingsDoneEventArgs> VarsBindingsDone;

        protected virtual void OnVarsBindingsDone(VarsBindingsDoneEventArgs e)
        {
            EventHandler<VarsBindingsDoneEventArgs> handler = this.VarsBindingsDone;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<DefaultEventArgs> LoadedFileUriChangedEvent;

        protected virtual void OnLoadedFileUriChangedEvent(DefaultEventArgs e)
        {
            EventHandler<DefaultEventArgs> handler = this.LoadedFileUriChangedEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion

        #region Ingerited properties
        [EditorProperty("Tło")]
        public new Brush Background { get { return base.Background; } set { base.Background = value; } }

        [EditorProperty("Szerokość")]
        public new double Width { get { return base.Width; } set { base.Width = value; } }

        [EditorProperty("Wysokość")]
        public new double Height { get { return base.Height; } set { base.Height = value; } }
        #endregion

        #region Dependency properties
        #region AutoSize
        [EditorProperty("Dopasuj wymiary")]
        public bool AutoSize
        {
            get { return (bool)GetValue(AutoSizeProperty); }
            set { SetValue(AutoSizeProperty, value); }
        }

        public static readonly DependencyProperty AutoSizeProperty =
            DependencyProperty.Register("AutoSize", typeof(bool), typeof(SchemeCanvas), new PropertyMetadata(false, AutoSize_PropertyChanged));

        private static void AutoSize_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SchemeCanvas schemeCanvas = (SchemeCanvas)d;
            schemeCanvas.DoAutoSize((bool)e.NewValue);

        }

        #endregion

        #region auto binding properties
        public bool IsAutoBinding
        {
            get { return (bool)GetValue(IsAutoBindingProperty); }
            set { SetValue(IsAutoBindingProperty, value); }
        }

        public static readonly DependencyProperty IsAutoBindingProperty =
            DependencyProperty.Register("IsAutoBinding", typeof(bool), typeof(SchemeCanvas), new PropertyMetadata(false));

        public string BindingSuffix
        {
            get { return (string)GetValue(BindingSuffixProperty); }
            set { SetValue(BindingSuffixProperty, value); }
        }

        public static readonly DependencyProperty BindingSuffixProperty =
            DependencyProperty.Register("BindingSuffix", typeof(string), typeof(SchemeCanvas), new PropertyMetadata(""));

        public string BindingPrefix
        {
            get { return (string)GetValue(BindingPrefixProperty); }
            set { SetValue(BindingPrefixProperty, value); }
        }

        public static readonly DependencyProperty BindingPrefixProperty =
            DependencyProperty.Register("BindingPrefix", typeof(string), typeof(SchemeCanvas), new PropertyMetadata(""));
        #endregion

        #region Scheme Loading properties
        public string SchemeFilePath
        {
            get { return (string)GetValue(SchemeFilePathProperty); }
            set { SetValue(SchemeFilePathProperty, value); }
        }

        public static readonly DependencyProperty SchemeFilePathProperty =
            DependencyProperty.Register("SchemeFilePath", typeof(string), typeof(SchemeCanvas), new PropertyMetadata("", SchemeFilePath_PropertyChanged));

        private static void SchemeFilePath_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SchemeCanvas schemeCanvas && e.NewValue is string strNewValue && schemeCanvas._isControlLoaded)
            {
                    schemeCanvas.LoadAndBind(strNewValue);         
            }
        }
        #endregion

        [EditorProperty("Tryb edycji")]
        public bool IsEditMode
        {
            get { return (bool)GetValue(IsEditModeProperty); }
            set { SetValue(IsEditModeProperty, value); }
        }

        public static readonly DependencyProperty IsEditModeProperty =
            DependencyProperty.Register("IsEditMode", typeof(bool), typeof(SchemeCanvas), new PropertyMetadata(false, IsEditMode_PropertyChanged));

        private static void IsEditMode_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SchemeCanvas schemeCanvas = (SchemeCanvas)d;
            schemeCanvas.ShowOrHideControls((bool)e.NewValue);
            if ((bool)e.NewValue)
            {
                schemeCanvas.AddMouseClickEventHandlerFromControls();
                schemeCanvas.AddContextMenuToControls();
                schemeCanvas.AddSchemeCanvasEvents();
            }
            else
            {
                schemeCanvas.RemoveMouseClickEventHandlerFromControls();
                schemeCanvas.RemoveContextMenuFromControls();
                schemeCanvas.RemoveSchemeCanvasEvents();
                if (schemeCanvas.SelectedElement != null)
                    schemeCanvas.UnselectElement();
                if (schemeCanvas.SchemeElements.Count > 0)
                {
                    schemeCanvas.ClearSelectionDict();
                    schemeCanvas.SetStartPoint();
                }
            }

        }

        #region Siatka
        public double GridLinesFrequency
        {
            get { return (double)GetValue(GridLinesFrequencyProperty); }
            set { SetValue(GridLinesFrequencyProperty, value); }
        }

        public static readonly DependencyProperty GridLinesFrequencyProperty =
            DependencyProperty.Register("GridLinesFrequency", typeof(double), typeof(SchemeCanvas), new PropertyMetadata(1.0, GridLinesFrequency_PropertyChanged));

        private static void GridLinesFrequency_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SchemeCanvas schemeCanvas = (SchemeCanvas)d;
            CanvasUtilities.RemoveGridLines(schemeCanvas);
            CanvasUtilities.DrawGridLines(Convert.ToInt32(e.NewValue), Convert.ToInt32(e.NewValue), schemeCanvas);
        }

        public bool IsGridVisible
        {
            get { return (bool)GetValue(IsGridVisibleProperty); }
            set { SetValue(IsGridVisibleProperty, value); }
        }

        public static readonly DependencyProperty IsGridVisibleProperty =
            DependencyProperty.Register("IsGridVisible", typeof(bool), typeof(SchemeCanvas), new PropertyMetadata(false, ShowGridLines_PropertyChanged));

        private static void ShowGridLines_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SchemeCanvas schemeCanvas = (SchemeCanvas)d;
            CanvasUtilities.RemoveGridLines(schemeCanvas);
            if ((bool)e.NewValue)
                CanvasUtilities.DrawGridLines(Convert.ToInt32(schemeCanvas.GridLinesFrequency), Convert.ToInt32(schemeCanvas.GridLinesFrequency), schemeCanvas);
        }
        #endregion
        #endregion

        #region public fields
        //private bool _IsSchemeLoading;
        //public bool IsSchemeLoading
        //{
        //    get { return _IsSchemeLoading; }
        //    private set { _IsSchemeLoading = value; NotifyPropertyChanged("IsSchemeLoading"); }
        //}

        //private bool _IsSchemeSaving;
        //public bool IsSchemeSaving
        //{
        //    get { return _IsSchemeSaving; }
        //    private set { _IsSchemeSaving = value; NotifyPropertyChanged("IsSchemeSaving"); }
        //}

        public List<string> ChildrenNames
        {
            get { return this.Children.OfType<FrameworkElement>().Select(x => x.Name).ToList(); }
        }

        private string _LoadedFileUri = null;
        public string LoadedFileUri { get { return _LoadedFileUri; }
            private set
            {
                var oldValue = _LoadedFileUri;
                _LoadedFileUri = value;
                NotifyPropertyChanged("LoadedFileUri");
                OnLoadedFileUriChangedEvent(new DefaultEventArgs(this, oldValue, _LoadedFileUri)); } }
        #endregion

        #region Private values
        // marginesy ramki zaznaczonego elementu
        private const int SELECTED_BORDER_PADDING = 5;

        private Dispatcher _dispatcher = Dispatcher.CurrentDispatcher;
        // ramka na zaznaczony obiekt
        private Rectangle _selectedBorder;
        // czy element się "rusza" (można przestawić go)
        private bool _mooving = false;
        // czy element jest nowy
        private bool _newElement = false;
        private double xoffset;
        private double yoffset;
        #endregion

        #region właściwosci zaznaczenia/kopii
        public bool PasteMenuItemIsEnabled { get { return _CopiedElement != null || _CopyOfSelectionDict_List.Any() ? true : false; } }

        private ObservableControl _CopiedElement = null;
        public ObservableControl CopiedElement
        {
            get { return _CopiedElement; }
            set
            {
                _CopiedElement = value;
                NotifyPropertyChanged("PasteMenuItemIsEnabled");
            }
        }

        public SelectedElementChangedDelegate SelectedElementChangedEvent = null;

        public ObservableControl _SelectedElement = null;
        public ObservableControl SelectedElement
        {
            get
            {
                return _SelectedElement;
            }
            private set
            {
                _SelectedElement = value;
                if (SelectedElementChangedEvent != null)
                    SelectedElementChangedEvent(this, new SelectedElementChangedEventArgs(_SelectedElement));
                NotifyPropertyChanged("SelectedElementPosition");
            }
        }

        public Point SelectedElementPosition
        {
            get
            {
                if (SelectedElement != null)
                {
                    double left, top;
                    if (SelectedElement is IEditorElement editorElement)
                    {
                        top = editorElement.Y_Position;
                        left = editorElement.X_Position;
                    }
                    else
                    {
                        top = Canvas.GetTop(SelectedElement);
                        left = Canvas.GetLeft(SelectedElement);
                    }
                    return new Point(left, top);
                }
                else
                {
                    return new Point();
                }
            }
        }
        #endregion

        #region zarządzanie zawartoscią schemecanvas
        public bool CheckIfNameExist(string name)
        {
            return this.Children.OfType<FrameworkElement>().Any(x => x.Name == name);
        }

        /// <summary>
        /// Slownik dodanych elementów biblioteki (nazwa, referencja do obiektu)
        /// </summary>
        public Dictionary<string, MultiSourceBrushControl> SchemeElements { get; set; } = new Dictionary<string, MultiSourceBrushControl>();

        protected override Size MeasureOverride(Size constraint)
        {

                if (this.AutoSize)
                {
                    if (base.InternalChildren.Count > 0)
                    {
                        base.MeasureOverride(constraint);
                        double width = base
                            .InternalChildren
                            .OfType<UIElement>()
                            .Where(i => i.GetValue(Canvas.LeftProperty) != null && i.GetType() != typeof(Border))
                            .Max(i => i.DesiredSize.Width + (double)i.GetValue(Canvas.LeftProperty) + 5);

                        if (Double.IsNaN(width))
                        {
                            width = 0;
                        }

                        double height = base
                            .InternalChildren
                            .OfType<UIElement>()
                            .Where(i => i.GetValue(Canvas.TopProperty) != null && i.GetType() != typeof(Border))
                            .Max(i => i.DesiredSize.Height + (double)i.GetValue(Canvas.TopProperty) + 5);

                        if (Double.IsNaN(height))
                        {
                            height = 0;
                        }

                        return new Size(width, height);
                    }
                    if (!base.InternalChildren.OfType<UIElement>().Any()) return new System.Windows.Size(1, 1);

                }
            
            return base.MeasureOverride(constraint);
        }


        public int CanvasChildrenCount { get; private set; }
        /// <summary>
        /// Nadpisana żeby uaktualiać słownik elementów
        /// </summary>
        /// <param name="visualAdded"></param>
        /// <param name="visualRemoved"></param>
        protected override void OnVisualChildrenChanged(DependencyObject visualAdded, DependencyObject visualRemoved)
        {
            base.OnVisualChildrenChanged(visualAdded, visualRemoved);

            if (visualAdded is MultiSourceBrushControl)
            {
                var addedControl = (MultiSourceBrushControl)visualAdded;
                if (!SchemeElements.ContainsKey(addedControl.Name))
                    SchemeElements.Add(addedControl.Name, addedControl);
                NotifyPropertyChanged(() => SchemeElements);
            }
            if (visualRemoved is MultiSourceBrushControl)
            {
                var removedControl = (MultiSourceBrushControl)visualRemoved;
                if (SchemeElements.ContainsKey(removedControl.Name))
                    SchemeElements.Remove(removedControl.Name);
                NotifyPropertyChanged(() => SchemeElements);
            }

            CanvasChildrenCount = this.Children.OfType<ObservableControl>().Count();
            NotifyPropertyChanged("CanvasChildrenCount");

        }

        /// <summary>
        /// Zapisuje elementy biblioteki zawarte w children
        /// </summary>
        /// <param name="fileName">Nazwa pliku</param>
        public void SaveScheme(string fileName)
        {
            //kopia schematu do backups
            this.OnSchemeStateChanged(new SchemeStateChangedEventArgs(SchemeStates.Saving));
            var valToSave = new List<ObservableControl>(this.Children?.OfType<ObservableControl>());
            foreach (var element in valToSave)
            {
                this.Children.Remove(element);
            }

            SchemeSerializationService.SerializeSchemeComponents(valToSave, fileName, 1);

            foreach (var element in valToSave)
            {
                this.Children.Add(element);
            }
            this.OnSchemeStateChanged(new SchemeStateChangedEventArgs(SchemeStates.Saved));
        }

        /// <summary>
        /// Wczytuje elementy zawarte na schemacie do pliku
        /// </summary>
        /// <param name="fileName">nazwa pliku</param>
        public void LoadScheme(string fileName)
        {
            this.OnSchemeStateChanged(new SchemeStateChangedEventArgs(SchemeStates.Loading));

            ContextMenu contextMenu = null;
            MouseButtonEventHandler mouseButtonEventHandler = null;

            if (this.IsEditMode)
            {
                contextMenu = new SchemeElementContextMenu();
                mouseButtonEventHandler = SchemeElement_MouseDown;
            }


            Dispatcher.Invoke(() =>
              {

                  if (File.Exists(fileName))
                  {
                      SchemeSerializationService.DeserializeSchemeComponents(fileName, this, mouseButtonEventHandler,
                      contextMenu);
                  }
              });


            this.SetWidthAndHeightProperties(this.DesiredSize);
            this.ApplyTemplate();
            this.ShowOrHideControls(this.IsEditMode);

            // podniesienie zdarzenia
            this.OnSchemeStateChanged(new SchemeStateChangedEventArgs(SchemeStates.Loaded));
            this.OnSchemeLoaded(new SchemeLoadedEventArgs(this, this.Children));

            if (!this.IsEditMode)
                this.StartNetworkSchemeUpdater();

            LoadedFileUri = fileName;
        }

        public void ClearScheme()
        {
            this.Children.Clear();
        }


        /// <summary>
        /// Metoda dodająca nowy element do schematu z odświeżeniem UI
        /// </summary>
        /// <param name="schemeElement">typ elementu do dodania</param>
        public void AddSchemeElement(Type schemeElementType)
        {

            if (schemeElementType != null && schemeElementType.IsSubclassOf(typeof(EditorControlBase))) // || schemeElementType.IsSubclassOf(typeof(ControlBase))
            {
                var element = Activator.CreateInstance(schemeElementType) as FrameworkElement;
                if (element is IControlLoadedEvent controlLoadedEvent)
                    controlLoadedEvent.AddLoadedEvent();

                
                var mousePosition = Mouse.GetPosition(this);
                if(Application.Current != null)
                {
                    mousePosition = Mouse.GetPosition(Application.Current.MainWindow);
                }
                // ustawienie pozycji elementu

                if (element is IEditorElement editorElement)
                    editorElement.SetPosition(mousePosition.X, mousePosition.Y);
                else
                {
                    Canvas.SetLeft(element, mousePosition.X);
                    Canvas.SetTop(element, mousePosition.Y);
                }

                if (this.IsEditMode)
                {
                    element.MouseDown += SchemeElement_MouseDown;
                    element.ContextMenu = new SchemeElementContextMenu();
                }
                element.SetValue(FrameworkElement.NameProperty, CanvasUtilities.GetNextNewElementName(this));

                this.Children.Add(element);

                this.UpdateLayout();
                if (element is IEditableControl editableControl)
                    editableControl.IsEditMode = this.IsEditMode;

                _newElement = true;
                // zaznaczenie kontrolki po dodaniu
                SelectedElement = element as ObservableControl;
                // można przenoscić kontrolkę
                _mooving = true;
            }
        }

        /// <summary>
        /// Dodaje nowy element do schematu z odświeżeniem UI
        /// </summary>
        /// <param name="element">element do dodania</param>
        public void AddSchemeElement(ObservableControl element, bool addNewControlName, string byParentName = null)
        {
            if (this.IsEditMode)
            {
                if (element is ISelectableElement)
                {
                    element.MouseDown += SchemeElement_MouseDown;
                }

                if (element is IEditorElement)
                {
                    element.ContextMenu = new SchemeElementContextMenu();
                }

                if (element is IEditableControl editableControl)
                {
                    editableControl.IsEditMode = this.IsEditMode;
                }

                if (element is IHideableElement)
                {
                    ((IHideableElement)element).HideElement = !this.IsEditMode;
                }
            }
            if (addNewControlName && string.IsNullOrEmpty(byParentName))
                element.Name = CanvasUtilities.GetNextNewElementName(this);
            else if (!addNewControlName && !string.IsNullOrEmpty(byParentName))
                element.Name = byParentName;

            this.Children.Add(element);
            this.UpdateLayout();
        }
        private int nodeCounter = 0;

        public void RemoveElementFromScheme(ObservableControl element)
        {
            if (this.Children.Contains(element))
            {
                element.MouseDown -= SchemeElement_MouseDown;
                element.ContextMenu = null;
                this.Children.Remove(element);
                if (SelectedElement != null)
                {
                    this.UnselectElement();
                }
                else
                    this.RemoveSelectionMask();
            }
        }
        #endregion

        #region konstruktory/ inicjalizacja
        public SchemeCanvas()
        {
            this.Loaded += SchemeCanvas_Loaded;
            this.Unloaded += SchemeCanvas_Unloaded;

            if(LibSettingsSetter != null)
                LibSettingsSetter.BrushChanged += LibSettingsSetter_BrushChanged;

        }

        private void LibSettingsSetter_BrushChanged(object sender, BrushChangedEventArgs e)
        {
            this.InvalidateVisual();
            var childrens = this.Children.OfType<ActiveElement>();
            foreach(var a in childrens)
            {
                a.InvalidateVisual();
            }

        }

        private void SchemeCanvas_Unloaded(object sender, RoutedEventArgs e)
        {
            StopNetworkSchemeUpdater();
        }

        private bool _isControlLoaded = false;
        private void SchemeCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            this.LoadAndBind(this.SchemeFilePath);
            _isControlLoaded = true;
        }

        public void LoadAndBind(string schemeFilePath)
        {
            this.Focus();
            if (!string.IsNullOrEmpty(schemeFilePath))
            {
                this.LoadScheme(schemeFilePath);

            }

            if (this.IsAutoBinding)
                this.DoVarsBinding();
        }

        public override void OnApplyTemplate()
        {

            base.OnApplyTemplate();
            this.InitializeSchemeCanvas();
        }

        private void InitializeSchemeCanvas()
        {
            this.ShowOrHideControls((bool)this.IsEditMode);
        }
        #endregion

        #region eventy
        private void SchemeElement_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && Keyboard.Modifiers != ModifierKeys.Control)
            {
                if (!SelectionDict.Any())
                {
                    if (sender is IMovableElement)
                    {
                        SelectElement((ObservableControl)sender);
                        _mooving = true;
                        xoffset = e.GetPosition(SelectedElement).X;
                        yoffset = e.GetPosition(SelectedElement).Y;
                    }
                    else if (sender is IOutputNode || sender is ILine)
                        SelectElement((FrameworkElement)sender);
                }
                else
                    _moovingSelection = true;

            }
            else if (e.LeftButton == MouseButtonState.Pressed && Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (SelectionDict.ContainsKey((FrameworkElement)sender))
                    this.RemoveFromSelectionDict((FrameworkElement)sender);
                else
                    this.AddToSelectionDict((FrameworkElement)sender, AddSelectionMask((FrameworkElement)sender));
            }

        }

        #region SchemeCanvas events
        private void SchemeCanvas_MouseLeave(object sender, MouseEventArgs e)
        {
            this.SetStartPoint();
        }

        private void SchemeCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && Keyboard.Modifiers != ModifierKeys.Control)
            {
                if (_newElement && SelectedElement != null)
                {
                    _newElement = false;
                }
                UnselectElement();

                // usuń zaznaczenia jeśli nie jest przenoszona zawartosć zaznaczenia
                if (!this.IsMouseOverOnElementInSelectionDict())
                {
                    ClearSelectionDict();
                    SetStartPoint();
                }
            }
        }

        private void SchemeCanvas_ContextMenuPaste(object sender, RoutedEventArgs e)
        {

            this.PasteSelectedChild();

        }

        private void SchemeCanvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (_mooving)
                _mooving = false;

            if (_moovingSelection)
                _moovingSelection = false;

            xoffset = 0;
            yoffset = 0;

            // usuń zaznaczenia jeśli nie jest przenoszona zawartosć zaznaczenia
            //if(_selectionMask != null && !_selectionMask.IsMouseOver)
            this.SetStartPoint();

        }

        // przetrzymuje wartość wcześniejszej pozycji myszki
        public Point MousePosition
        {
            get
            {
                return new Point(Math.Round(Mouse.GetPosition(this).X, 2), Math.Round(Mouse.GetPosition(this).Y, 2));
            }
        }
        private static Point _mouseOldPosition = new Point();
        private void SchemeCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            NotifyPropertyChanged("MousePosition");

            var acctualMousePos = e.GetPosition(this);
            if (e.LeftButton == MouseButtonState.Pressed && _moovingSelection)
            {
                this.MoveSelection(acctualMousePos);
            }
            else if (SelectedElement != null && _selectedBorder != null && _mooving && e.LeftButton == MouseButtonState.Pressed)
            {
                if (SelectedElement is IEditorElement editorElement)
                {
                    editorElement.SetPosition(MathHelpers.NearestRound(acctualMousePos.X - xoffset, GridLinesFrequency), MathHelpers.NearestRound(acctualMousePos.Y - yoffset, GridLinesFrequency));

                    Canvas.SetLeft(_selectedBorder, editorElement.X_Position - SELECTED_BORDER_PADDING);
                    Canvas.SetTop(_selectedBorder, editorElement.Y_Position - SELECTED_BORDER_PADDING);
                }
                else
                {
                    //POZYCJA
                    Canvas.SetLeft(SelectedElement, MathHelpers.NearestRound(acctualMousePos.X - xoffset, GridLinesFrequency));
                    Canvas.SetTop(SelectedElement, MathHelpers.NearestRound(acctualMousePos.Y - yoffset, GridLinesFrequency));


                    Canvas.SetLeft(_selectedBorder, Canvas.GetLeft(SelectedElement) - SELECTED_BORDER_PADDING);
                    Canvas.SetTop(_selectedBorder, Canvas.GetTop(SelectedElement) - SELECTED_BORDER_PADDING);
                }

                NotifyPropertyChanged(() => SelectedElementPosition);
            }
            else if (SelectedElement != null && _selectedBorder == null && _mooving && _newElement == true)
            {
                if (SelectedElement is IEditorElement editorElement)
                    editorElement.SetPosition(MathHelpers.NearestRound(acctualMousePos.X - xoffset, GridLinesFrequency), MathHelpers.NearestRound(acctualMousePos.Y - yoffset, GridLinesFrequency));
                else
                {
                    //POZYCJA
                    Canvas.SetLeft(SelectedElement, MathHelpers.NearestRound(acctualMousePos.X - xoffset, GridLinesFrequency));
                    Canvas.SetTop(SelectedElement, MathHelpers.NearestRound(acctualMousePos.Y - yoffset, GridLinesFrequency));
                }

                NotifyPropertyChanged(() => SelectedElementPosition);
            }

            if (!_moovingSelection)
            {
                if (_selectionStartPoint != new Point(0, 0) && e.LeftButton == MouseButtonState.Pressed && SelectedElement == null)
                    DrawSelectionRectangle(_selectionStartPoint);
                else
                    this.SetStartPoint();
            }

            _mouseOldPosition = acctualMousePos;
        }
        #endregion
        #endregion

        /// <summary>
        /// Pokazuje/Ukrywa kontrolki "Edytora" (źródła, wyjścia itd)
        /// </summary>
        /// <param name="isEditMode">Czy tryb edycji</param>
        private void ShowOrHideControls(bool isEditMode)
        {
            var hideableElements = this?.Children?.OfType<IHideableElement>();
            foreach (var hideableElement in hideableElements)
            {
                hideableElement.HideElement = !isEditMode;
                //output.SetValue(OutputNode.HideElementProperty, !isEditMode);
            }

            var sources = this?.Children?.OfType<Source>();
            foreach (var source in sources)
            {
                source.SetValue(Source.IsGraphicsVisibleProperty, isEditMode);
            }
        }

        #region obsługa zaznaczenia
        private Rectangle AddSelectionMask(FrameworkElement element)
        {
            RemoveSelectionMask();
            if (element != null)
            {
                var resultBorder = new Rectangle();
                resultBorder.Stroke = Brushes.Red;
                resultBorder.StrokeThickness = 2;
                resultBorder.Width = (element as FrameworkElement).ActualWidth + (SELECTED_BORDER_PADDING * 2);
                resultBorder.Height = (element as FrameworkElement).ActualHeight + (SELECTED_BORDER_PADDING * 2);

                //POZYCJA RECTANGLE
                Canvas.SetLeft(resultBorder, Canvas.GetLeft(element) - SELECTED_BORDER_PADDING);
                Canvas.SetTop(resultBorder, Canvas.GetTop(element) - SELECTED_BORDER_PADDING);
                return resultBorder;
            }
            return null;
        }

        private void RemoveSelectionMask()
        {
            if (this.Children.Contains(_selectedBorder))
                this.Children.Remove(_selectedBorder);
            _selectedBorder = null;
        }

        public void SelectElement(FrameworkElement element)
        {
            if (element is ObservableControl)
            {
                var observableControl = (ObservableControl)element;
                SelectedElement = observableControl;
                _selectedBorder = AddSelectionMask(SelectedElement);
                if (_selectedBorder != null)
                    this.Children.Add(_selectedBorder);
            }
        }

        public void UnselectElement()
        {
            if (SelectedElement != null)
                SelectedElement = null;
            if (_mooving)
                _mooving = false;

            RemoveSelectionMask();
        }
        #endregion

        #region Multiselect

        #region Fields
        private Dictionary<FrameworkElement, Rectangle> _SelectionDict = new Dictionary<FrameworkElement, Rectangle>();
        public Dictionary<FrameworkElement, Rectangle> SelectionDict { get { return _SelectionDict; } private set { _SelectionDict = value; } }

        private List<FrameworkElement> _CopyOfSelectionDict_List = new List<FrameworkElement>();

        private int _SelectedElementCounter;
        public int SelectefElementCounter { get { return _SelectedElementCounter; } set { _SelectedElementCounter = value; NotifyPropertyChanged("SelectefElementCounter"); } }

        private Rectangle _selectionBorder = null;
        private Rectangle _selectionMask = null;
        private Point _selectionStartPoint = new Point(0, 0);

        private bool _moovingSelection = false;
        #endregion

        #region public methods
        /// <summary>
        /// Wyrównuje zaznaczone kontroli do kontrolki najbardziej wysuniętej do góry
        /// </summary>
        public void FitSelectionToTop()
        {
            if (SelectionDict.Any())
            {
                double top = Canvas.GetTop(SelectionDict.First().Key);
                foreach (var element in SelectionDict)
                {
                    double tempTop = Canvas.GetTop(element.Key);
                    if (tempTop < top)
                        top = tempTop;
                }
                SetTopInSelection(top);
            }
        }

        /// <summary>
        /// Wyrównuje zaznaczone kontroli do kontrolki najbardziej wysuniętej do dołu
        /// </summary>
        public void FitSelectionToBottom()
        {
            if (SelectionDict.Any())
            {
                double top = Canvas.GetTop(SelectionDict.First().Key);
                foreach (var element in SelectionDict)
                {
                    double tempTop = Canvas.GetTop(element.Key);
                    if (tempTop > top)
                        top = tempTop;
                }
                SetTopInSelection(top);
            }
        }

        /// <summary>
        /// Wyrównuje zaznaczone kontroli do kontrolki najbardziej wysuniętej do lewej
        /// </summary>
        public void FitSelectionToLeft()
        {
            if (SelectionDict.Any())
            {
                double left = Canvas.GetLeft(SelectionDict.First().Key);
                foreach (var element in SelectionDict)
                {
                    double tempLeft = Canvas.GetLeft(element.Key);
                    if (tempLeft < left)
                        left = tempLeft;
                }
                SetLeftInSelection(left);
            }
        }

        /// <summary>
        /// Wyrównuje zaznaczone kontroli do kontrolki najbardziej wysuniętej do prawej
        /// </summary>
        public void FitSelectionToRight()
        {
            if (SelectionDict.Any())
            {
                double left = Canvas.GetLeft(SelectionDict.First().Key);
                foreach (var element in SelectionDict)
                {
                    double tempLeft = Canvas.GetLeft(element.Key);
                    if (tempLeft > left)
                        left = tempLeft;
                }
                SetLeftInSelection(left);
            }
        }

        /// <summary>
        /// Wyrównuje dystans poziomy pomiędzy kontrolkami (ustawia taki jaki jest pomiędzy pierwszą i drugą)
        /// </summary>
        public void SetHorizontalDistance()
        {
            if (SelectionDict.Count > 1)
            {
                var sortedDict = (from pair in SelectionDict
                                  orderby Canvas.GetLeft(pair.Key) ascending
                                  select pair).ToDictionary(x => x.Key, x => x.Value);

                double firstLeft = Canvas.GetLeft(sortedDict.ElementAt(0).Key);
                double secondLeft = Canvas.GetLeft(sortedDict.ElementAt(1).Key);
                double controlWidth = sortedDict.First().Key.ActualWidth;

                double distance = Math.Abs(secondLeft - firstLeft) - controlWidth;

                double canvasLeftValueToSet = firstLeft;

                for (int i = 0; i < sortedDict.Count; i++)
                {
                    this.SetLeftAtElement(sortedDict.ElementAt(i), canvasLeftValueToSet);
                    canvasLeftValueToSet = canvasLeftValueToSet + sortedDict.ElementAt(i).Key.ActualWidth + distance;
                }
                //AddOrRemoveSelectionDictMask();
            }
        }

        /// <summary>
        /// Wyrównuje dystans pionowy pomiędzy kontrolkami (ustawia taki jaki jest pomiędzy pierwszą i drugą)
        /// </summary>
        public void SetVarticalDistance()
        {
            if (SelectionDict.Count > 1)
            {
                var sortedDict = (from pair in SelectionDict
                                  orderby Canvas.GetTop(pair.Key) ascending
                                  select pair).ToDictionary(x => x.Key, x => x.Value);

                double firstTop = Canvas.GetTop(sortedDict.ElementAt(0).Key);
                double secondTop = Canvas.GetTop(sortedDict.ElementAt(1).Key);
                double controlHeight = sortedDict.First().Key.ActualHeight;

                double distance = Math.Abs(secondTop - firstTop) - controlHeight;

                double canvasLeftValueToSet = firstTop;

                for (int i = 0; i < sortedDict.Count; i++)
                {
                    this.SetTopAtElement(sortedDict.ElementAt(i), canvasLeftValueToSet);
                    canvasLeftValueToSet = canvasLeftValueToSet + sortedDict.ElementAt(i).Key.ActualHeight + distance;
                }
            }
        }

        /// <summary>
        /// Usuwa wszystkie zaznaczone elementy z UI
        /// </summary>
        public void DeleteSelectedItems()
        {
            if (SelectionDict.Any())
            {
                var selectionDictCopy = new Dictionary<FrameworkElement, Rectangle>(SelectionDict);
                selectionDictCopy.ToList().ForEach(x => this.RemoveFromUI(x.Key));
            }
        }

        private Point _selectionCopyOffset = new Point();
        /// <summary>
        /// kopiuje wszystkie zaznaczony elementy elementy 
        /// </summary>
        public void CopySelection()
        {
            if (!_CopyOfSelectionDict_List.Any())
            {
                foreach (var element in SelectionDict)
                    _CopyOfSelectionDict_List.Add((FrameworkElement)((IEditableControl)element.Key).Copy());
                NotifyPropertyChanged("PasteMenuItemIsEnabled");
                _selectionCopyOffset = Mouse.GetPosition(this);
            }
        }

        /// <summary>
        ///  metoda wklejajaca skopiowane zaznaczenie
        /// </summary>
        public void PasteSelectionCopy()
        {
            if (_CopyOfSelectionDict_List.Any())
            {
                this.ClearSelectionDict();
                var currMousePosition = Mouse.GetPosition(this);
                foreach (var element in _CopyOfSelectionDict_List)
                {
                    if (element is IEditorElement editorElement)
                        editorElement.SetPosition(Canvas.GetLeft(element) + currMousePosition.X - _selectionCopyOffset.X, Canvas.GetTop(element) + currMousePosition.Y - _selectionCopyOffset.Y);
                    else
                    {
                        //POZYCJA
                        Canvas.SetLeft(element, Canvas.GetLeft(element) + currMousePosition.X - _selectionCopyOffset.X);
                        Canvas.SetTop(element, Canvas.GetTop(element) + currMousePosition.Y - _selectionCopyOffset.Y);
                    }
                    this.Children.Add(element);
                    this.UpdateLayout();
                    this.AddToSelectionDict(element, AddSelectionMask(element));

                }
                this._CopyOfSelectionDict_List.Clear();
            }
        }
        #endregion

        #region private
        /// <summary>
        /// Ustawia taką samą wartość canvas.top na całej kolekcji zaznaczenia
        /// </summary>
        /// <param name="top"></param>
        private void SetTopInSelection(double top)
        {
            if (SelectionDict.Any())
            {
                for (int i = 0; i < SelectionDict.Count; i++)
                {
                    SetTopAtElement(SelectionDict.ElementAt(i), top);
                }
                //AddOrRemoveSelectionDictMask();
            }
        }

        /// <summary>
        /// Ustawia canvas.top na elemencie z zachowaniem maski (ramki zaznaczenia) na elemencie
        /// </summary>
        /// <param name="selection">Klucz - zaznaczony element; wartość - jego ramka</param>
        /// <param name="top">wartość do ustawienia</param>
        private void SetTopAtElement(KeyValuePair<FrameworkElement, Rectangle> selection, double top)
        {
            if (this.Children.Contains(selection.Value))
                this.Children.Remove(selection.Value);

            if (selection.Key is IEditorElement editorElement)
                editorElement.Y_Position = top;
            else
            {
                //POZYCJA TOP
                Canvas.SetTop(selection.Key, top);
            }
            SelectionDict[selection.Key] = AddSelectionMask(selection.Key);
            this.Children.Add(SelectionDict[selection.Key]);
        }

        /// <summary>
        /// Ustawia taką samą wartość canvas.left na całej kolekcji zaznaczenia
        /// </summary>
        /// <param name="left"></param>
        private void SetLeftInSelection(double left)
        {
            if (SelectionDict.Any())
            {
                for (int i = 0; i < SelectionDict.Count; i++)
                {
                    SetLeftAtElement(SelectionDict.ElementAt(i), left);
                }
                //AddOrRemoveSelectionDictMask();
            }
        }

        /// <summary>
        /// Ustawia canvas.left na elemencie z zachowaniem maski (ramki zaznaczenia) na elemencie
        /// </summary>
        /// <param name="selection">Klucz - zaznaczony element; wartość - jego ramka</param>
        /// <param name="left">wartość do ustawienia</param>
        private void SetLeftAtElement(KeyValuePair<FrameworkElement, Rectangle> selection, double left)
        {
            if (this.Children.Contains(selection.Value))
                this.Children.Remove(selection.Value);

            if (selection.Key is IEditorElement editorElement)
                editorElement.X_Position = left;
            else
            {
                //POZYCJA LEFT
                Canvas.SetLeft(selection.Key, left);
            }
            SelectionDict[selection.Key] = AddSelectionMask(selection.Key);
            this.Children.Add(SelectionDict[selection.Key]);
        }

        /// <summary>
        /// Ustawia punkt startu rysowania ramki zaznaczenia
        /// </summary>
        private void SetStartPoint()
        {
            if (_selectionStartPoint == new Point(0, 0))
            {
                var mousePosition = Mouse.GetPosition(this);
                _selectionStartPoint.X = mousePosition.X;
                _selectionStartPoint.Y = mousePosition.Y;
            }
            else
            {
                this.RemoveSelectionRectangle();
                _selectionStartPoint.X = 0;
                _selectionStartPoint.Y = 0;
            }
        }

        /// <summary>
        /// metoda rysująca ramkę zaznaczenia
        /// </summary>
        /// <param name="startPoint"></param>
        private void DrawSelectionRectangle(Point startPoint)
        {
            this.RemoveSelectionRectangle();

            _selectionBorder = new Rectangle();

            var mousePosition = Mouse.GetPosition(this);

            var offsetX = -1;
            var canvasLeft = startPoint.X;
            if (mousePosition.X < startPoint.X)
            {
                canvasLeft = mousePosition.X + 1;
                offsetX = 0;
            }

            var offsetY = -1;
            var canvasTop = startPoint.Y;
            if (mousePosition.Y < startPoint.Y)
            {
                canvasTop = mousePosition.Y + 1;
                offsetY = 0;
            }

            mousePosition.X += offsetX;
            mousePosition.Y += offsetY;

            var borderWidth = Math.Abs(mousePosition.X - startPoint.X);
            var borderHeigh = Math.Abs(mousePosition.Y - startPoint.Y);

            _selectionBorder.StrokeThickness = 2;
            _selectionBorder.Stroke = Brushes.LightBlue;
            _selectionBorder.Width = borderWidth;
            _selectionBorder.Height = borderHeigh;
            this.Children.Add(_selectionBorder);

            //POZYCJA RECTANGLE
            Canvas.SetLeft(_selectionBorder, canvasLeft);
            Canvas.SetTop(_selectionBorder, canvasTop);

            this.AddSelected(new Size(borderWidth, borderHeigh), canvasTop, canvasLeft);
        }

        /// <summary>
        /// metoda usuwająca ramkę zaznaczenia
        /// </summary>
        private void RemoveSelectionRectangle()
        {
            if (_selectionBorder != null && this.Children.Contains(_selectionBorder))
            {
                this.Children.Remove(_selectionBorder);
            }
        }

        /// <summary>
        /// dodanie do kolekcji zaznaczenia jeśli mieści się w ramce
        /// </summary>
        /// <param name="borderSize"></param>
        /// <param name="canvasTop"></param>
        /// <param name="canvasLeft"></param>
        private void AddSelected(Size borderSize, double canvasTop, double canvasLeft)
        {
            double rectW = borderSize.Width;
            double rectH = borderSize.Height;
            double rectT = canvasTop;
            double rectL = canvasLeft;

            var selectableElements = new List<FrameworkElement>(this.Children.OfType<IEditorElement>().Cast<FrameworkElement>());
            foreach (var selectableElement in selectableElements)
            {
                double elementT = Canvas.GetTop(selectableElement);
                double elementL = Canvas.GetLeft(selectableElement);
                if (elementL <= rectL + rectW && elementL >= rectL && elementT <= rectT + rectH && elementT >= rectT)
                {
                    AddToSelectionDict(selectableElement, AddSelectionMask(selectableElement));

                }
                else
                    RemoveFromSelectionDict(selectableElement);
            }
            SelectefElementCounter = SelectionDict.Count;
        }

        /// <summary>
        /// dodanie elementu do słownika zaznaczonych elementów
        /// </summary>
        /// <param name="itemToAdd"></param>
        /// <param name="itemMask"></param>
        private void AddToSelectionDict(FrameworkElement itemToAdd, Rectangle itemMask)
        {
            if (this.SelectionDict != null && itemToAdd is ISelectableElement && itemToAdd is IGroupSelectableElement && !this.SelectionDict.ContainsKey(itemToAdd))
            {
                this.SelectionDict.Add(itemToAdd, itemMask);
                this.Children.Add(itemMask);
                this.NotifyPropertyChanged(() => this.SelectionDict);
                //this.AddOrRemoveSelectionDictMask();
            }
        }

        /// <summary>
        /// usuwa element z słownika zaznaczonych elementów
        /// </summary>
        /// <param name="itemToRemove"></param>
        public void RemoveFromSelectionDict(FrameworkElement itemToRemove)
        {
            if (this.SelectionDict != null && this.SelectionDict.ContainsKey(itemToRemove))
            {
                if (this.Children.Contains(SelectionDict[itemToRemove]))
                    this.Children.Remove(SelectionDict[itemToRemove]);

                this.SelectionDict.Remove(itemToRemove);
                this.NotifyPropertyChanged(() => this.SelectionDict);
                //this.AddOrRemoveSelectionDictMask();
            }
        }

        /// <summary>
        ///  usuwa element wraz z zaznaczeniem z UI
        /// </summary>
        /// <param name="frameworkElement"></param>
        private void RemoveFromUI(FrameworkElement frameworkElement)
        {
            if (SelectionDict.ContainsKey(frameworkElement) && frameworkElement is IEditableControl)
            {
                this.RemoveFromSelectionDict(frameworkElement);
                ((IEditableControl)frameworkElement).Remove();
            }
        }

        /// <summary>
        /// czyści słownik zaznaczonych elementów. Usuwa ramki zaznaczenia z UI
        /// </summary>
        private void ClearSelectionDict()
        {
            foreach (var element in SelectionDict)
            {
                if (this.Children.Contains(element.Value))
                    this.Children.Remove(element.Value);
            }
            this.SelectionDict.Clear();
            this.SelectefElementCounter = SelectionDict.Count;
            //this.AddOrRemoveSelectionDictMask();

        }

        /// <summary>
        /// przenosi zaznaczone elementy
        /// </summary>
        /// <param name="mousePosition"></param>
        private void MoveSelection(Point mousePosition)
        {
            var distanceX = mousePosition.X - _mouseOldPosition.X;
            var distanceY = mousePosition.Y - _mouseOldPosition.Y;

            // przesuwanie w jednej osi
            if (Keyboard.Modifiers == ModifierKeys.Shift)
            {
                if (Math.Abs(distanceX) < Math.Abs(distanceY))
                {
                    for (int i = 0; i < SelectionDict.Count; i++)
                    {
                        var element = SelectionDict.ElementAt(i);
                        if (element.Key is IEditorElement editorElement && element.Key is IMovableSelectionElement movableSelectionElement)
                        {
                            movableSelectionElement.IsSelectionMoving = true;
                            editorElement.Y_Position = MathHelpers.NearestRound(editorElement.Y_Position + distanceY, GridLinesFrequency);
                            movableSelectionElement.IsSelectionMoving = false;
                        }
                        else if (!(element.Key is IEditorElement))
                        {
                            //POZYCJA TOP
                            Canvas.SetTop(element.Key, MathHelpers.NearestRound(Canvas.GetTop(element.Key) + distanceY, GridLinesFrequency));
                        }
                        Canvas.SetTop(element.Value, Canvas.GetTop(element.Key) - SELECTED_BORDER_PADDING);
                    }
                }
                else if (Math.Abs(distanceX) > Math.Abs(distanceY))
                {
                    for (int i = 0; i < SelectionDict.Count; i++)
                    {
                        var element = SelectionDict.ElementAt(i);

                        if (element.Key is IEditorElement editorElement && element.Key is IMovableSelectionElement movableSelectionElement)
                        {
                            movableSelectionElement.IsSelectionMoving = true;
                            editorElement.X_Position = MathHelpers.NearestRound(editorElement.X_Position + distanceX, GridLinesFrequency);
                            movableSelectionElement.IsSelectionMoving = false;
                        }
                        else if (!(element.Key is IEditorElement))
                        {
                            //POZYCJA LEFT
                            Canvas.SetLeft(element.Key, MathHelpers.NearestRound(Canvas.GetLeft(element.Key) + distanceX, GridLinesFrequency));
                        }
                        Canvas.SetLeft(element.Value, Canvas.GetLeft(element.Key) - SELECTED_BORDER_PADDING);
                    }
                }
            }
            else
            {
                for (int i = 0; i < SelectionDict.Count; i++)
                {
                    var element = SelectionDict.ElementAt(i);

                    if (element.Key is IEditorElement editorElement && element.Key is IMovableSelectionElement movableSelectionElement)
                    {
                        movableSelectionElement.IsSelectionMoving = true;
                        editorElement.SetPosition(MathHelpers.NearestRound(editorElement.X_Position + distanceX, GridLinesFrequency), MathHelpers.NearestRound(editorElement.Y_Position + distanceY, GridLinesFrequency));
                        movableSelectionElement.IsSelectionMoving = false;
                    }
                    else if (!(element.Key is IEditorElement))
                    {
                        //POZYCJA
                        Canvas.SetLeft(element.Key, MathHelpers.NearestRound(Canvas.GetLeft(element.Key) + distanceX, GridLinesFrequency));
                        Canvas.SetTop(element.Key, MathHelpers.NearestRound(Canvas.GetTop(element.Key) + distanceY, GridLinesFrequency));
                    }

                    //POZYCJA RECTANGLE
                    Canvas.SetLeft(element.Value, Canvas.GetLeft(element.Key) - SELECTED_BORDER_PADDING);
                    Canvas.SetTop(element.Value, Canvas.GetTop(element.Key) - SELECTED_BORDER_PADDING);
                }
            }
            //AddOrRemoveSelectionDictMask();
        }

        /// <summary>
        /// dodaje maskę zaznaczenia grupowego
        /// </summary>
        private void AddOrRemoveSelectionDictMask()
        {
            if (_selectionMask != null && this.Children.Contains(_selectionMask))
            {
                this.Children.Remove(_selectionMask);
            }
            if (SelectionDict.Any())
            {
                Point startPoint = new Point(Canvas.GetLeft(SelectionDict.FirstOrDefault().Value), Canvas.GetTop(SelectionDict.FirstOrDefault().Value));
                Point endPoint = new Point(startPoint.X, startPoint.Y);
                foreach (var element in SelectionDict)
                {
                    var top = Canvas.GetTop(element.Value);
                    var left = Canvas.GetLeft(element.Value);
                    if (left < startPoint.X)
                        startPoint.X = left;
                    if (left + element.Value.Width > endPoint.X)
                        endPoint.X = left + element.Value.Width;
                    if (top < startPoint.Y)
                        startPoint.Y = top;
                    if (top + element.Value.Height > endPoint.Y)
                        endPoint.Y = top + element.Value.Height;
                }
                this._selectionMask = new Rectangle() { Stroke = Brushes.Red, StrokeThickness = 2, Width = endPoint.X - startPoint.X + (SELECTED_BORDER_PADDING * 2), Height = endPoint.Y - startPoint.Y + (SELECTED_BORDER_PADDING * 2) };
                //POZYCJA MASKA
                Canvas.SetLeft(_selectionMask, startPoint.X - SELECTED_BORDER_PADDING);
                Canvas.SetTop(_selectionMask, startPoint.Y - SELECTED_BORDER_PADDING);
                this.Children.Add(_selectionMask);
            }
        }

        /// <summary>
        /// sprawdza czy myszka jest na elemencie ze słownika zaznaczenia
        /// </summary>
        /// <returns></returns>
        private bool IsMouseOverOnElementInSelectionDict()
        {
            return SelectionDict.Any(x => x.Key.IsMouseOver == true);
        }
        #endregion

        #endregion

        #region Powiadomienia zmian właściowści
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            NotifyPropertyChangedExplicit(propertyName);
        }

        protected void NotifyPropertyChanged<TProperty>(Expression<Func<TProperty>> projection)
        {
            var memberExpression = (MemberExpression)projection.Body;
            NotifyPropertyChangedExplicit(memberExpression.Member.Name);
        }

        void NotifyPropertyChangedExplicit(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }
        #endregion

        #region editMode private methods
        private void RemoveMouseClickEventHandlerFromControls()
        {
            var movableElements = this?.Children?.OfType<ObservableControl>();
            foreach (var element in movableElements)
            {
                element.MouseDown -= SchemeElement_MouseDown;
            }
        }

        private void AddMouseClickEventHandlerFromControls()
        {
            var movableElements = this?.Children?.OfType<ObservableControl>();
            foreach (var element in movableElements)
            {
                element.MouseDown += SchemeElement_MouseDown;
            }
        }

        private void AddContextMenuToControls()
        {
            var movableElements = this?.Children?.OfType<ObservableControl>();
            foreach (var element in movableElements)
            {
                element.ContextMenu = new SchemeElementContextMenu();
            }

        }

        private void RemoveContextMenuFromControls()
        {
            var movableElements = this?.Children?.OfType<ObservableControl>();
            foreach (var element in movableElements)
            {
                element.ContextMenu = null;
            }
        }

        private void AddSchemeCanvasEvents()
        {
            this.MouseMove += SchemeCanvas_MouseMove;
            this.MouseUp += SchemeCanvas_MouseUp;
            this.PreviewMouseDown += SchemeCanvas_MouseDown;
            this.MouseLeave += SchemeCanvas_MouseLeave;

            var canvasMenuContext = new ContextMenu();
            var contextMenuItems = new List<FrameworkElement>();
            var pasteMenuItem = new MenuItem() { Header = "Wklej", Icon = SchemeControlsLibResources.GetIconByKey("Paste") };
            pasteMenuItem.Click += SchemeCanvas_ContextMenuPaste;

            Binding pasteMenuItemIsEnabledBinding = new Binding("PasteMenuItemIsEnabled");
            pasteMenuItemIsEnabledBinding.Source = this;
            pasteMenuItemIsEnabledBinding.Mode = BindingMode.OneWay;
            pasteMenuItemIsEnabledBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            pasteMenuItem.SetBinding(MenuItem.IsEnabledProperty, pasteMenuItemIsEnabledBinding);
            contextMenuItems.Add(pasteMenuItem);
            canvasMenuContext.ItemsSource = contextMenuItems;
            this.ContextMenu = canvasMenuContext;


            var window = Application.Current?.MainWindow;

            window.CommandBindings.Add(new CommandBinding(ApplicationCommands.Copy, (sender, args) => 
            {
                this.CopySelectedChild();
            }, (sender, args) => { args.CanExecute = true; }));
            window.CommandBindings.Add(new CommandBinding(ApplicationCommands.Paste, (sender, args) => { this.PasteSelectedChild(); }, (sender, args) => args.CanExecute = true));
            window.CommandBindings.Add(new CommandBinding(ApplicationCommands.Delete, (sender, args) => { this.DeleteSelectedChild(); }, (sender, args) => args.CanExecute = true));

            this.Focusable = true;
            this.Focus();

        }

        private void RemoveSchemeCanvasEvents()
        {
            this.MouseMove -= SchemeCanvas_MouseMove;
            this.MouseUp -= SchemeCanvas_MouseUp;
            this.PreviewMouseDown -= SchemeCanvas_MouseDown;
            this.MouseLeave -= SchemeCanvas_MouseLeave;

            this.CommandBindings.Clear();
            this.InputBindings.Clear();

            var canvasMenuContext = this?.ContextMenu.ItemsSource as List<FrameworkElement>;
            if (canvasMenuContext != null)
            {
                foreach (var pasteMenuItem in canvasMenuContext)
                {
                    if (pasteMenuItem is MenuItem)
                    {
                        var menuItem = pasteMenuItem as MenuItem;
                        BindingOperations.ClearAllBindings(menuItem);
                        menuItem.Click -= SchemeCanvas_ContextMenuPaste;
                    }
                }

                this.ContextMenu = null;
            }
        }
        #endregion

        /// <summary>
        /// Metoda wykonująca atobindowanie
        /// </summary>
        private void DoVarsBinding()
        {
            List<string> bindingVarsNames = ControlPropertiesBinder.DoSchemeControlsBindings(this.Children, this.BindingPrefix, this.BindingSuffix);
            BindingVarsNames = bindingVarsNames;
            this.OnVarsBindingsDone(new VarsBindingsDoneEventArgs(this, bindingVarsNames));
        }

        public List<string> BindingVarsNames { get; private set; }

        private Size _savedCanvasSize = new Size();
        // Wykonuje autosize (dopasowywuje wymiar do granic elementów)
        private void DoAutoSize(bool value)
        {
            if (value)
            {
                this._savedCanvasSize.Width = (double)this.GetValue(WidthProperty);
                this._savedCanvasSize.Height = (double)this.GetValue(HeightProperty);
                this.SetValue(WidthProperty, DependencyProperty.UnsetValue);
                this.SetValue(HeightProperty, DependencyProperty.UnsetValue);
                this.InvalidateMeasure();
            }
            else
            {
                this.SetWidthAndHeightProperties(_savedCanvasSize);
            }
        }

        private void SetWidthAndHeightProperties(Size canvasSize)
        {
            this.SetValue(WidthProperty, canvasSize.Width);
            this.SetValue(HeightProperty, canvasSize.Height);
        }

        #region networkContols      
        private NetworkElementStateUpdater NetworkElementStateUpdater;

        public NetworkElementStateUpdater StartNetworkSchemeUpdater()
        {
            if (this.Children.OfType<IEthernetControl>().Any())
            {
                if (NetworkElementStateUpdater == null)
                    NetworkElementStateUpdater = new NetworkElementStateUpdater(this, 5000);

                NetworkElementStateUpdater.Start();
                return NetworkElementStateUpdater;
            }
            return null;
        }

        public void StopNetworkSchemeUpdater()
        {
            if (NetworkElementStateUpdater != null)
            {
                NetworkElementStateUpdater.Stop();
                NetworkElementStateUpdater.Clean();
            }
        }
        #endregion networkContols

        #region search engine
        public void SearchElement(string searchPhrase)
        {
            var searchingElements = this.Children.OfType<ISearchingElement>().Where(x=> x.SearchingText != null && x.SearchingText.ToUpper().Split(';').Where(y => y == searchPhrase.ToUpper()).Count() != 0).ToList();
            if (searchingElements.Count == 1 && this.Parent is ZoomBorder zoomBorder)
            {
                var element = searchingElements.FirstOrDefault();
                if (element is EditorControlBase editorControlBase)
                {
                    var borderNewWidth = zoomBorder.ActualWidth / 8.0;
                    var borderNewHeight = zoomBorder.ActualHeight / 8.0;
                    SpecificPlace specificPlace = new SpecificPlace(editorControlBase.CenterPoint.Y - borderNewHeight, editorControlBase.CenterPoint.X - borderNewWidth, 4);
                    zoomBorder.SetSpecyficArea(specificPlace);
                    // gdyby przybliżenie miało być animowane - ale do poprawy
                    //zoomBorder.SetZoomAnimationToCenterPoint(4, editorControlBase.CenterPoint, 5);
                    element.BlinkElement(Brushes.Blue, 20, 250);
                }
            }
            else
            {
                foreach (var element in searchingElements)
                {
                    element.BlinkElement(Brushes.Blue, 20, 250);
                    if (element is FrameworkElement frameworkElement)
                        this.AddToSelectionDict(frameworkElement, AddSelectionMask(frameworkElement));
                }
            }
        }
        #endregion search engine

        #region IEditable methods
        public void CopySelectedChild()
        {
            if (!this.SelectionDict.Any())
            {
                if(SelectedElement is IEditableControl editableControl)
                this.CopiedElement = editableControl.Copy() as ObservableControl;
            }
            else
                this.CopySelection();
        }

        public void PasteSelectedChild()
        {
            if (CopiedElement != null)
            {
                var mousePosition = Mouse.GetPosition(this);

                CopiedElement.SetValue(FrameworkElement.NameProperty, CanvasUtilities.GetNextNewElementName(this));

                if (CopiedElement is IEditorElement editorElement)
                    editorElement.SetPosition(mousePosition.X, mousePosition.Y);
                else
                {
                    Canvas.SetLeft(CopiedElement, mousePosition.X);
                    Canvas.SetTop(CopiedElement, mousePosition.Y);
                }
                this.Children.Add(CopiedElement);
                CopiedElement = null;
            }

            if (this._CopyOfSelectionDict_List.Any())
            {
                this.PasteSelectionCopy();
            }
        }

        public void DeleteSelectedChild()
        {

            if (this.SelectedElement != null)
            {
                if (!(SelectedElement is LineConnector) && !(this.SelectedElement is ILine) && this.SelectedElement is IEditableControl editableControl)
                    editableControl.Remove();
                else if (this.SelectedElement is ILine line)
                {
                    line.Disconnect(true);
                }
                else if (SelectedElement is LineConnector lineConnector)
                {
                    lineConnector.RemoveTheLinieBreak();
                }
            }
            else if (this.SelectionDict != null && this.SelectionDict.Any())
            {
                this.DeleteSelectedItems();
            }
        }
        #endregion IEditable methods


        #region Layers
        public void LayerMoveDownSelection()
        {
            if (!this.SelectionDict.Any())
            {
                if (SelectedElement is IEditorElement editorElement)
                {
                    editorElement.LayerNumber -= 1;
                }
            }
            else
            {
                foreach(var element in this.SelectionDict)
                {
                    if (element.Key is IEditorElement editorElement)
                    {
                        editorElement.LayerNumber -= 1;
                    }
                }
            }

        }

        public void LayerMoveUpSelection()
        {
            if (!this.SelectionDict.Any())
            {
                if (SelectedElement is IEditorElement editorElement)
                {
                    editorElement.LayerNumber += 1;
                }
            }
            else
            {
                foreach (var element in this.SelectionDict)
                {
                    if (element.Key is IEditorElement editorElement)
                    {
                        editorElement.LayerNumber += 1;
                    }
                }
            }

        }
        #endregion Layers
    }
}
