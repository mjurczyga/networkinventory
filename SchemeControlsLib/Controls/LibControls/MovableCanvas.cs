﻿using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SchemeControlsLib.Controls.LibControls
{
    public class MovableCanvas : Canvas, INotifyPropertyChanged
    {
        #region properties
        #region private properties
        private FrameworkElement _selectedElement = null;
        private bool _moving = false;
        private double _xOffset;
        private double _yOffset;
        #endregion

        #region public properties
        public Point SelectedElementPosition
        {
            get
            {
                if (_selectedElement != null)
                {
                    double left, top;
                    if (_selectedElement is IEditorElement editorElement)
                    {
                        top = editorElement.Y_Position;
                        left = editorElement.X_Position;
                    }
                    else
                    {
                        top = Canvas.GetTop(_selectedElement);
                        left = Canvas.GetLeft(_selectedElement);
                    }
                    return new Point(left, top);
                }
                else
                {
                    return new Point();
                }
            }
        }
        #endregion
        #endregion

        #region ElementsEvents
        private void CanvasElement_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && Keyboard.Modifiers != ModifierKeys.Control)
            {
                if (sender is FrameworkElement)
                {
                    SelectElement((FrameworkElement)sender);
                    _moving = true;
                    _xOffset = e.GetPosition(_selectedElement).X;
                    _yOffset = e.GetPosition(_selectedElement).Y;
                }
            }
        }
        #endregion


        #region canvas events
        private void MovableCanvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (_moving)
                _moving = false;

            _xOffset = 0;
            _yOffset = 0;
        }

        private void MovableCanvas_MouseMove(object sender, MouseEventArgs e)
        {

            var acctualMousePos = e.GetPosition(this);

            if (_selectedElement != null && _moving && e.LeftButton == MouseButtonState.Pressed)
            {
                if (_selectedElement is IEditorElement editorElement)
                    editorElement.SetPosition(MathHelpers.NearestRound(acctualMousePos.X - _xOffset, 1), MathHelpers.NearestRound(acctualMousePos.Y - _yOffset, 1));
                //POZYCJA
                else
                {
                    Canvas.SetLeft(_selectedElement, MathHelpers.NearestRound(acctualMousePos.X - _xOffset, 1));
                    Canvas.SetTop(_selectedElement, MathHelpers.NearestRound(acctualMousePos.Y - _yOffset, 1));
                }

                NotifyPropertyChanged("SelectedElementPosition");
            }
        }
        #endregion

        public MovableCanvas()
        {
            this.MouseMove += MovableCanvas_MouseMove;
            this.MouseUp += MovableCanvas_MouseUp;
        }

        #region canvas overides
        protected override void OnVisualChildrenChanged(DependencyObject visualAdded, DependencyObject visualRemoved)
        {
            base.OnVisualChildrenChanged(visualAdded, visualRemoved);

            if(visualAdded is FrameworkElement)
            {
                ((FrameworkElement)visualAdded).MouseDown += CanvasElement_MouseDown;
            }
            if(visualRemoved is FrameworkElement)
            {
                ((FrameworkElement)visualRemoved).MouseDown -= CanvasElement_MouseDown;
            }

        }
        #endregion

        #region public methods
        public void SelectElement(FrameworkElement element)
        {
            _selectedElement = element;
        }
        #endregion

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
