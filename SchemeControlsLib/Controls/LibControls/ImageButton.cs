﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.LibControls
{
    public class ImageButton : Button, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #region Icon
        public DrawingBrush Drawing
        {
            get { return (DrawingBrush)GetValue(DrawingProperty); }
            set { SetValue(DrawingProperty, value); }
        }

        public static readonly DependencyProperty DrawingProperty =
            DependencyProperty.Register("Drawing", typeof(DrawingBrush), typeof(ImageButton), new PropertyMetadata(null, Drawing_PropertyChanged));

        private static void Drawing_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ImageButton imageButton)
            {
                if (e.NewValue != null)
                    imageButton.IconVisibility = Visibility.Visible;
                else
                    imageButton.IconVisibility = Visibility.Collapsed;
            }
        }



        public Visibility IconVisibility
        {
            get { return (Visibility)GetValue(IconVisibilityProperty); }
            set { SetValue(IconVisibilityProperty, value); }
        }

        public static readonly DependencyProperty IconVisibilityProperty =
            DependencyProperty.Register("IconVisibility", typeof(Visibility), typeof(ImageButton), new PropertyMetadata(Visibility.Collapsed));



        //private Visibility _IconVisibility = Visibility.Collapsed;
        //public Visibility IconVisibility
        //{
        //    get { return _IconVisibility; }
        //    private set { _IconVisibility = value; NotifyPropertyChanged("IconVisibility"); }
        //}
        #endregion icon

        #region DrawingContent
        public UIElement DrawingContent
        {
            get { return (UIElement)GetValue(DrawingContentProperty); }
            set { SetValue(DrawingContentProperty, value); }
        }

        public static readonly DependencyProperty DrawingContentProperty =
            DependencyProperty.Register("DrawingContent", typeof(UIElement), typeof(ImageButton));
        #endregion DrawingContent

        #region button text
        public TextAlignment TextAlignment
        {
            get { return (TextAlignment)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }

        public static readonly DependencyProperty TextAlignmentProperty =
            DependencyProperty.Register("TextAlignment", typeof(TextAlignment), typeof(ImageButton));

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(ImageButton), new PropertyMetadata(null, Text_PropertyChanged));

        private static void Text_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ImageButton imageButton)
            {
                if (e.NewValue != null)
                    imageButton.TextVisibility = Visibility.Visible;
                else
                    imageButton.TextVisibility = Visibility.Collapsed;
            }
        }

        public Visibility TextVisibility
        {
            get { return (Visibility)GetValue(TextVisibilityProperty); }
            set { SetValue(TextVisibilityProperty, value); }
        }

        public static readonly DependencyProperty TextVisibilityProperty =
            DependencyProperty.Register("TextVisibility", typeof(Visibility), typeof(ImageButton), new PropertyMetadata(Visibility.Collapsed));

        //private Visibility _TextVisibility = Visibility.Collapsed;
        //public Visibility TextVisibility
        //{
        //    get { return _TextVisibility; }
        //    set { _TextVisibility = value; NotifyPropertyChanged("TextVisibility"); }
        //}

        #endregion


        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(ImageButton));

        public Thickness TextPadding
        {
            get { return (Thickness)GetValue(TextPaddingProperty); }
            set { SetValue(TextPaddingProperty, value); }
        }

        public static readonly DependencyProperty TextPaddingProperty =
            DependencyProperty.Register("TextPadding", typeof(Thickness), typeof(ImageButton));

        public Thickness DrawingMargin
        {
            get { return (Thickness)GetValue(DrawingMarginProperty); }
            set { SetValue(DrawingMarginProperty, value); }
        }

        public static readonly DependencyProperty DrawingMarginProperty =
            DependencyProperty.Register("DrawingMargin", typeof(Thickness), typeof(ImageButton), new PropertyMetadata(new Thickness(15)));

        public ICommand MouseDownCommand
        {
            get { return (ICommand)GetValue(MouseDownCommandProperty); }
            set { SetValue(MouseDownCommandProperty, value); }
        }

        public static readonly DependencyProperty MouseDownCommandProperty =
            DependencyProperty.Register("MouseDownCommand", typeof(ICommand), typeof(ImageButton), new PropertyMetadata(MouseDownCommand_PropertyChanged));

        public object BaseVar
        {
            get { return (object)GetValue(BaseVarProperty); }
            set { SetValue(BaseVarProperty, value); }
        }

        public static readonly DependencyProperty BaseVarProperty =
            DependencyProperty.Register("BaseVar", typeof(object), typeof(ImageButton));

        public object MouseDownCommandParameter
        {
            get { return (object)GetValue(MouseDownCommandValueProperty); }
            set { SetValue(MouseDownCommandValueProperty, value); }
        }

        public static readonly DependencyProperty MouseDownCommandValueProperty =
            DependencyProperty.Register("MouseDownCommandValue", typeof(object), typeof(ImageButton));

        #region Background/select element
        public Brush AccentColor
        {
            get { return (Brush)GetValue(AccentColorProperty); }
            set { SetValue(AccentColorProperty, value); }
        }
        public static readonly DependencyProperty AccentColorProperty =
            DependencyProperty.Register("AccentColor", typeof(Brush), typeof(ImageButton));


        private bool _IsSelected = false;
        public bool IsSelected
        {
            get { return _IsSelected; }
            private set { _IsSelected = value; NotifyPropertyChanged("IsSelected"); }
        }

        public void SelectElement()
        {
            IsSelected = true;
            this.UpdateBackground();
        }

        public void UnselectElement()
        {
            IsSelected = false;
            this.UpdateBackground();
        }

        private Brush _InternalBackground;
        public Brush InternalBackground
        {
            get { return _InternalBackground; }
            set { _InternalBackground = value; NotifyPropertyChanged("InternalBackground"); }
        }


        private void UpdateBackground()
        {
            if (IsSelected)
                this.InternalBackground = AccentColor;
            else
                this.InternalBackground = Background;
        }
        #endregion

        private static void MouseDownCommand_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ImageButton imageButton = (ImageButton)d;
            if (e.NewValue != null)
                imageButton.Cursor = Cursors.Hand;
            else
                imageButton.Cursor = Cursors.Arrow;

        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            if (MouseDownCommand != null)
            {
                if (BaseVar != null)
                    MouseDownCommand.Execute(new KeyValuePair<object, object>(BaseVar, MouseDownCommandParameter));
                else
                    MouseDownCommand.Execute(MouseDownCommandParameter);
            }
        }

        static ImageButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ImageButton), new FrameworkPropertyMetadata(typeof(ImageButton)));
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.UpdateBackground();
        }
    }
}