﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Helpers;
using SchemeControlsLib.Inputs;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SchemeControlsLib.Controls.LibControls
{
    public class SchemeElementsToolbox : ObservableControl
    {
        private Visibility _ITS_Unvisible;
        public Visibility ITS_Unvisible
        {
            get { return Settings.IsITSApp ? Visibility.Collapsed : Visibility.Visible; }
        }


        #region Dependency properties
        public ICommand MouseButtonDoubleClickOnElementCommand
        {
            get { return (ICommand)GetValue(MouseButtonDoubleClickOnElementCommandProperty); }
            set { SetValue(MouseButtonDoubleClickOnElementCommandProperty, value); }
        }

        public static readonly DependencyProperty MouseButtonDoubleClickOnElementCommandProperty =
            DependencyProperty.Register("MouseButtonDoubleClickOnElementCommand", typeof(ICommand), typeof(SchemeElementsToolbox), new PropertyMetadata(null));


        public KeyValuePair<string, Type> SelectedItem
        {
            get { return (KeyValuePair<string, Type>)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(KeyValuePair<string, Type>), typeof(SchemeElementsToolbox), new PropertyMetadata(null));

        public string SchemeCanvasName
        {
            get { return (string)GetValue(SchemeCanvasNameProperty); }
            set { SetValue(SchemeCanvasNameProperty, value); }
        }

        public static readonly DependencyProperty SchemeCanvasNameProperty =
            DependencyProperty.Register("SchemeCanvasName", typeof(string), typeof(SchemeElementsToolbox), new PropertyMetadata("", SchemeCanvasName_PropertyChanged));

        private static void SchemeCanvasName_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SchemeElementsToolbox schemeCanvasNavigationBar = d as SchemeElementsToolbox;
            if (schemeCanvasNavigationBar != null)
                schemeCanvasNavigationBar.FindSchemeCanvas(e.NewValue.ToString());
        }
        #endregion

        private SchemeCanvas _EditorSchemeCanvas = null;

        public Dictionary<string, Type> LibElements { get; private set; } = new Dictionary<string, Type>();

        public Dictionary<string, Type> CommonElements { get; private set; } = new Dictionary<string, Type>();

        public Dictionary<string, Type> NetworkElements { get; private set; } = new Dictionary<string, Type>();

        public Dictionary<string, Type> CoalProcessingElements { get; private set; } = new Dictionary<string, Type>();

        public SchemeElementsToolbox()
        {

        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            InitControl();
        }

        private void InitControl()
        {
            try
            {
                FindSchemeControlsLibElements();
            }
            catch (Exception e)
            {
                Console.WriteLine("Słownik toolboxa zostanie wczytany w runtime");
            }

            if (this.Template != null)
            {
                var libControls = this.Template.FindName("_LibControls", this) as ListBox;
                var commonElements = this.Template.FindName("_commonElements", this) as ListBox;
                var networkElements = this.Template.FindName("_nommonElements", this) as ListBox;
                var coalProcessingElements = this.Template.FindName("_CoalProcessingElements", this) as ListBox;
                var tabControl = this.Template.FindName("_TabControl", this) as TabControl;

                tabControl.SelectionChanged += TabControl_SelectionChanged;

                if (libControls != null)
                {
                    libControls.MouseDoubleClick += OnListboxMouseButtonDoubleClick;
                    libControls.SelectionChanged += OnListboxSelectionChanged;
                }

                if (commonElements != null)
                {
                    commonElements.MouseDoubleClick += OnListboxMouseButtonDoubleClick;
                    commonElements.SelectionChanged += OnListboxSelectionChanged;
                }

                if (networkElements != null)
                {
                    networkElements.MouseDoubleClick += OnListboxMouseButtonDoubleClick;
                    networkElements.SelectionChanged += OnListboxSelectionChanged;
                }
                if (coalProcessingElements != null)
                {
                    coalProcessingElements.MouseDoubleClick += OnListboxMouseButtonDoubleClick;
                    coalProcessingElements.SelectionChanged += OnListboxSelectionChanged;
                }
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl tabControl && tabControl.SelectedItem is TabItem tabItem && tabItem.Content is ListBox listBox && listBox.SelectedItem is KeyValuePair<string, Type> keyValue)
            {
                SelectedItem = keyValue;
            }
        }

        private void OnListboxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var x = (KeyValuePair<string, Type>)((ListBox)e.Source).SelectedItem;
            SelectedItem = x;
        }

        private void OnListboxMouseButtonDoubleClick(object sender, MouseButtonEventArgs e)
        {

            if(_EditorSchemeCanvas != null)
            {
                _EditorSchemeCanvas.UnselectElement();
                _EditorSchemeCanvas.AddSchemeElement(SelectedItem.Value);
            }
            else if (this.MouseButtonDoubleClickOnElementCommand != null)
            {
                MouseButtonDoubleClickOnElementCommand.Execute(e);
            }

        }


        static SchemeElementsToolbox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SchemeElementsToolbox), new FrameworkPropertyMetadata(typeof(SchemeElementsToolbox)));
        }

        private void FindSchemeControlsLibElements()
        {
            var schemeLibControls = SchemeElementsFinder.GetSchemeControls();
            foreach (var control in schemeLibControls)
            {
                if (control.Key.Flag == true)
                {
                    if(!(Settings.IsITSApp && (control.Key.Name == "Logo eFOLZON" || control.Key.Name == "Kontrolka pomiarowa" || control.Key.Name == "Jaworek")))

                    if (control.Key.SchemeType == SchemeTypes.SynopticScheme)
                        LibElements.Add(control.Key.Name, control.Value);
                    else if (control.Key.SchemeType == SchemeTypes.Common)
                        CommonElements.Add(control.Key.Name, control.Value);
                    else if (control.Key.SchemeType == SchemeTypes.NetworkScheme)
                        NetworkElements.Add(control.Key.Name, control.Value);
                    else if (control.Key.SchemeType == SchemeTypes.CoalProcessingScheme)
                        CoalProcessingElements.Add(control.Key.Name, control.Value);
                }
            }
            NotifyPropertyChanged(() => LibElements);
            NotifyPropertyChanged(() => CommonElements);
            NotifyPropertyChanged(() => NetworkElements);
            NotifyPropertyChanged(() => CoalProcessingElements);

        }

        private void FindSchemeCanvas(string name)
        {
            if (TextExt.CheckIfIsControlName(name))
            {
                FrameworkElement element = UIHelper.FindUserControl<FrameworkElement>(this);
                if (element != null)
                    _EditorSchemeCanvas = element.FindName(name) as SchemeCanvas;
            }
        }

    }
}
