﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.LibControls.Converters;
using SchemeControlsLib.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Media;
using VISU.Controls;

namespace SchemeControlsLib.Controls.LibControls
{
    [TemplatePart(Name = "PART_MainGrid", Type = typeof(Grid))]
    public class LocalColorPicker : NotifyControl
    {
        private const string PART_MainGrid = "PART_MainGrid";
        private Grid _MainGrid;

        #region dependencyProperties
        #region events
        #region SelectedColorChanged
        public static readonly RoutedEvent SelectedColorChangedEvent = EventManager.RegisterRoutedEvent(
            "SelectedColorChanged", RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<Color?>), typeof(LocalColorPicker));

        public event RoutedPropertyChangedEventHandler<Color?> SelectedColorChanged
        {
            add { AddHandler(SelectedColorChangedEvent, value); }
            remove { RemoveHandler(SelectedColorChangedEvent, value); }
        }

        void RaiseSelectedColorChangedEvent(Color? oldValue, Color? newValue)
        {
            RoutedPropertyChangedEventArgs<Color?> newEventArgs = new RoutedPropertyChangedEventArgs<Color?>(oldValue, newValue);
            newEventArgs.RoutedEvent = SelectedColorChangedEvent;
            RaiseEvent(newEventArgs);
        }
        #endregion SelectedColorChanged

        #region OpenedEvent
        public static readonly RoutedEvent OpenedEvent = EventManager.RegisterRoutedEvent(
            "Opened", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(LocalColorPicker));

        public event RoutedEventHandler Opened
        {
            add { AddHandler(OpenedEvent, value); }
            remove { RemoveHandler(OpenedEvent, value); }
        }

        void RaiseOpenedEvent()
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(LocalColorPicker.OpenedEvent);
            RaiseEvent(newEventArgs);
        }
        #endregion OpenedEvent

        #region ClosedEvent
        public static readonly RoutedEvent ClosedEvent = EventManager.RegisterRoutedEvent(
            "Closed", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(LocalColorPicker));

        public event RoutedEventHandler Closed
        {
            add { AddHandler(ClosedEvent, value); }
            remove { RemoveHandler(ClosedEvent, value); }
        }

        void RaiseClosedEvent()
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(LocalColorPicker.ClosedEvent);
            RaiseEvent(newEventArgs);
        }
        #endregion ClosedEvent
        #endregion events

        #region IsOpenProperty
        public bool IsOpen
        {
            get { return (bool)GetValue(IsOpenProperty); }
            set { SetValue(IsOpenProperty, value); }
        }

        public static readonly DependencyProperty IsOpenProperty =
            DependencyProperty.Register("IsOpen", typeof(bool), typeof(LocalColorPicker), new PropertyMetadata(IsOpen_PropertyChanged));

        private static void IsOpen_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is LocalColorPicker localColorPicker)
            {

            }
        }
        #endregion IsOpenProperty

        #region SelectedColor
        public Color SelectedColor
        {
            get { return (Color)GetValue(SelectedColorProperty); }
            set { SetValue(SelectedColorProperty, value); }
        }

        public static readonly DependencyProperty SelectedColorProperty =
            DependencyProperty.Register("SelectedColor", typeof(Color), typeof(LocalColorPicker), new PropertyMetadata(SelectedColor_PropertyChanged));

        private static void SelectedColor_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is LocalColorPicker localColorPicker)
            {
                if (e.OldValue != e.NewValue)
                {
                    localColorPicker.RaiseSelectedColorChangedEvent((Color?)e.OldValue, (Color?)e.NewValue);
                    localColorPicker.SelectedBrush = new SolidColorBrush((Color)e.NewValue);
                }
            }
        }

        private Brush _SelectedBrush;
        public Brush SelectedBrush
        {
            get { return _SelectedBrush; }
            private set { _SelectedBrush = value; NotifyPropertyChanged("SelectedBrush"); }
        }

        #endregion SelectedColor

        #region AvailableColors
        public ObservableCollection<ColorElement> AvailableColors
        {
            get { return (ObservableCollection<ColorElement>)GetValue(AvailableColorsProperty); }
            set { SetValue(AvailableColorsProperty, value); }
        }

        public static readonly DependencyProperty AvailableColorsProperty =
            DependencyProperty.Register("AvailableColors", typeof(ObservableCollection<ColorElement>), typeof(LocalColorPicker), new PropertyMetadata(AvailableColors_PropertyChanged));

        private static void AvailableColors_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is LocalColorPicker localColorPicker)
            {
                
            }
        }

        public bool ShowAvailableColors
        {
            get { return (bool)GetValue(ShowAvailableColorsProperty); }
            set { SetValue(ShowAvailableColorsProperty, value); }
        }

        public static readonly DependencyProperty ShowAvailableColorsProperty =
            DependencyProperty.Register("ShowAvailableColors", typeof(bool), typeof(LocalColorPicker), new PropertyMetadata(ShowAvailableColors_PeoprertyChanged));

        private static void ShowAvailableColors_PeoprertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is LocalColorPicker localColorPicker)
            {

            }
        }




        #endregion AvailableColors

        public double HeadersFontSize
        {
            get { return (double)GetValue(HeadersFontSizeProperty); }
            set { SetValue(HeadersFontSizeProperty, value); }
        }

        public static readonly DependencyProperty HeadersFontSizeProperty =
            DependencyProperty.Register("HeadersFontSize", typeof(double), typeof(LocalColorPicker), new PropertyMetadata(12.0));



        public Brush HeadersForeground
        {
            get { return (Brush)GetValue(HeadersForegroundProperty); }
            set { SetValue(HeadersForegroundProperty, value); }
        }

        public static readonly DependencyProperty HeadersForegroundProperty =
            DependencyProperty.Register("HeadersForeground", typeof(Brush), typeof(LocalColorPicker), new PropertyMetadata(Brushes.Black));



        #endregion dependencyProperties

        #region Private methods
        private ObservableCollection<ColorElement> GetAvailableBrushesList()
        {
            var result = new ObservableCollection<ColorElement>();
            var brushesProps = typeof(Brushes).GetProperties();
            if(brushesProps != null)
            {
                foreach(var brushProp in brushesProps)
                {
                    var propValue = (SolidColorBrush)brushProp.GetValue(null);
                    if(propValue != null && brushProp.Name != "Transparent")
                    {
                        result.Add(new ColorElement(propValue.Color, brushProp.Name));
                    }
                }
            }

            return result;
        }

        private ObservableCollection<ColorElement> GetStandardBrushesList()
        {
            var result = new ObservableCollection<ColorElement>();
            result.Add(new ColorElement(Brushes.Transparent.Color, "Transparent"));
            result.Add(new ColorElement(Brushes.White.Color, "White"));
            result.Add(new ColorElement(Brushes.Gray.Color, "Gray"));
            result.Add(new ColorElement(Brushes.Black.Color, "Black"));
            result.Add(new ColorElement(Brushes.Red.Color, "Red"));
            result.Add(new ColorElement(Brushes.Green.Color, "Green"));
            result.Add(new ColorElement(Brushes.Blue.Color, "Blue"));
            result.Add(new ColorElement(Brushes.Yellow.Color, "Yellow"));
            result.Add(new ColorElement(Brushes.Orange.Color, "Orange"));
            result.Add(new ColorElement(Brushes.Pink.Color, "Pink"));

            return result;
        }

        #endregion Private methods

        #region Advenced
        private FrameworkElement GenerateAdvancedTabContent()
        {
            var gradientElement = ColorGradientCanvas();
            return gradientElement;
        }

        private FrameworkElement ColorGradientCanvas()
        {
            var gradientCanvas = new Canvas();
            var binding = new Binding("SelectedColor");
            binding.Source = this;
            binding.Converter = new ColorToGradientConverter();
            gradientCanvas.SetBinding(Canvas.BackgroundProperty, binding);

            var linearGradientBrush = new LinearGradientBrush() { StartPoint = new Point(0.5, 0), EndPoint = new Point(0.5, 1) };
            linearGradientBrush.GradientStops.Add(new GradientStop() { Color = Colors.Transparent, Offset = 0 });
            linearGradientBrush.GradientStops.Add(new GradientStop() { Color = Colors.Black, Offset = 1 });
            var secondGradientCanvas = new Canvas() { Background = linearGradientBrush };

            Grid grid = new Grid();
            grid.Children.Add(gradientCanvas);
            grid.Children.Add(secondGradientCanvas);

            return grid;
        }
        #endregion Advenced

        #region Standard
        private FrameworkElement _StandardColorsContent = null;
        private FrameworkElement GenerateStandardTabContent()
        {
            var colors = GetAvailableBrushesList();
            UniformGrid availableColorsGrid = new UniformGrid() { Columns = 11, Margin = new Thickness(5) };
            if (colors != null)
            {
                var sortedColors = colors.OrderBy(x => x.Name).ToList();
                foreach (var sortedColor in sortedColors)
                {
                    if (sortedColor.Color is Color color)
                    {
                        var colorBoder = GetColorBorder(color, sortedColor.Name);
                        availableColorsGrid.Children.Add(colorBoder);
                    }
                }
            }
            TextBlock availableColorsHeader = new TextBlock() { Text = "Dostępne kolory"};
            availableColorsHeader.SetBinding(FontSizeProperty, GetBinding("HeadersFontSize"));
            availableColorsHeader.SetBinding(ForegroundProperty, GetBinding("HeadersForeground"));

            StackPanel standardColorsStackPanel = new StackPanel();
            standardColorsStackPanel.Children.Add(availableColorsHeader);
            standardColorsStackPanel.Children.Add(availableColorsGrid);

            UniformGrid standardColorsGrid = new UniformGrid() { Columns = 11, Margin = new Thickness(5) };
            var standardColors = GetStandardBrushesList();
            if (standardColors != null)
            {
                foreach (var standardColor in standardColors)
                {
                    if (standardColor.Color is Color color)
                    {
                        var colorBoder = GetColorBorder(color, standardColor.Name);
                        standardColorsGrid.Children.Add(colorBoder);
                    }
                }
            }
            TextBlock standardColorsHeader = new TextBlock() { Text = "Standardowe kolory" };
            standardColorsHeader.SetBinding(FontSizeProperty, GetBinding("HeadersFontSize"));
            standardColorsHeader.SetBinding(ForegroundProperty, GetBinding("HeadersForeground"));

            standardColorsStackPanel.Children.Add(standardColorsHeader);
            standardColorsStackPanel.Children.Add(standardColorsGrid);

            Border result = new Border() { Background = Brushes.White };
            result.Child = standardColorsStackPanel;

            return result;
        }

        private void ColorBoder_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (sender is Grid grid)
            {
                var _SchadowBorder = grid.FindControl<Border>("_colorBoder") as Border;
                if (_SchadowBorder != null)
                {
                    SelectedColor = ((SolidColorBrush)_SchadowBorder.Background).Color;
                }
            }
        }

        /// <summary>
        /// zwraca kontrolke z bindowaniem koloru i cieniowaniem
        /// </summary>
        /// <param name="color"></param>
        /// <param name="colorName"></param>
        /// <returns></returns>
        private FrameworkElement GetColorBorder(Color color, string colorName)
        {

            var colorBoder = new Border() { Name= "_colorBoder", Background = new SolidColorBrush(color), ToolTip = colorName, BorderThickness = new Thickness(0.5), BorderBrush = Brushes.Black, };
            colorBoder.Margin = new Thickness(1);
            var shadowBorder = new Border() {Name = "_SchadowBorder", Background = Brushes.Black, ToolTip = colorName, Opacity = 0.4, Visibility = Visibility.Collapsed };
            colorBoder.Margin = new Thickness(1);


            var resultGrid = new Grid() { Width = 17, Height = 17 };
            resultGrid.MouseDown += ColorBoder_MouseDown;
            resultGrid.MouseEnter += ColorBoder_MouseEnter;
            resultGrid.MouseLeave += ColorBoder_MouseLeave;
            resultGrid.Children.Add(colorBoder);
            resultGrid.Children.Add(shadowBorder);

            return resultGrid;
        }

        private void ColorBoder_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if(sender is Grid grid)
            {
                var _SchadowBorder = grid.FindControl<Border>("_SchadowBorder") as Border;
                if(_SchadowBorder != null)
                {
                    _SchadowBorder.Visibility = Visibility.Collapsed;
                }

            }
        }

        private void ColorBoder_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (sender is Grid grid)
            {
                var _SchadowBorder = grid.FindControl<Border>("_SchadowBorder");
                if (_SchadowBorder != null)
                {
                    _SchadowBorder.Visibility = Visibility.Visible;
                }

            }
        }
        #endregion Standard

        private void GenerateControl()
        {
            if (_MainGrid != null)
            {
                _MainGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                _MainGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });

                var colorBorder = new Border(); // new SolidColorBrush(SelectedColor)
                colorBorder.SetBinding(Border.BackgroundProperty, this.GetBinding("SelectedBrush"));
                Grid.SetColumn(colorBorder, 0);
                DrawingGroup drawingGroup = new DrawingGroup();
                drawingGroup.Children.Add(new GeometryDrawing(Brushes.White, null, Geometry.Parse("F1M16,16L0,16 0,0 16,0z")));
                drawingGroup.Children.Add(new GeometryDrawing(Brushes.Black, null, Geometry.Parse("m5,6.5 h6 c-3,3 -3,3 -3,3 0,0 0,0 -3,-3z")));

                var colorBoder = new Border() { Name = "_colorBoder", Background = new DrawingBrush(drawingGroup), BorderThickness = new Thickness(0.5), BorderBrush = Brushes.Black, Width = 16, Height = 16 };
                colorBoder.Margin = new Thickness(1);
                var shadowBorder = new Border() { Name = "_SchadowBorder", Background = Brushes.Black, Opacity = 0.4, Visibility = Visibility.Collapsed };
                colorBoder.Margin = new Thickness(1);

                var resultGrid = new Grid() { Width = 17, Height = 17 };
                resultGrid.MouseEnter += ColorBoder_MouseEnter;
                resultGrid.MouseLeave += ColorBoder_MouseLeave;
                resultGrid.Children.Add(colorBoder);
                resultGrid.Children.Add(shadowBorder);


                var viewBox = new Viewbox() { Stretch = Stretch.UniformToFill };
                viewBox.Child = resultGrid;
                Grid.SetColumn(viewBox, 1);
                _MainGrid.Children.Add(colorBorder);
                _MainGrid.Children.Add(viewBox);


                this.MouseDown += LocalColorPicker_MouseDown;

                var popup = new Popup() { PlacementTarget = this, Placement = PlacementMode.Left, StaysOpen = true };
                popup.SetBinding(Popup.IsOpenProperty, GetBinding("IsOpen"));
                popup.Child = GenerateStandardTabContent();
                _MainGrid.Children.Add(popup);

            }
        }

        private void LocalColorPicker_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            IsOpen = !IsOpen;
        }

        public LocalColorPicker()
        {
            
        }

        static LocalColorPicker()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(LocalColorPicker), new FrameworkPropertyMetadata(typeof(LocalColorPicker)));
        }

        private object _ContentControl;
        public object ContentControl
        {
            get { return _ContentControl; }
            set { _ContentControl = value; NotifyPropertyChanged("ContentControl"); }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            _MainGrid = GetTemplateChild(PART_MainGrid) as Grid;


            GenerateControl();
        }


        private Binding GetBinding(string path)
        {
            var result = new Binding(path);
            result.Source = this;
            return result;
        }

    }
}
