﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.LibControls.Converters
{
    public class ColorToGradientConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is Color color)
            {
                var linearGradientBrush = new LinearGradientBrush() { StartPoint = new Point(0, 0.5), EndPoint = new Point(1, 0.5) };
                linearGradientBrush.GradientStops.Add(new GradientStop() { Color = Colors.White, Offset = 0 });
                linearGradientBrush.GradientStops.Add(new GradientStop() { Color = color, Offset = 1 });
                return linearGradientBrush;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
