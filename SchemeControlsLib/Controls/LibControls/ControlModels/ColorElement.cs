﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.LibControls
{
    public class ColorElement : IEquatable<ColorElement>, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private Color? _Color;
        public Color? Color
        {
            get { return _Color; }
            set { _Color = value; NotifyPropertyChanged("Color"); }
        }

        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; NotifyPropertyChanged("Name"); }
        }

        public ColorElement(Color? color, string name)
        {
            Color = color;
            Name = name;
        }

        public bool Equals(ColorElement other)
        {
            if (Color == other.Color && Name == other.Name)
                return true;
            else
                return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
