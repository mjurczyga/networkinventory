﻿using SchemeControlsLib.Base;
using SchemeControlsLib.Controls.Interfaces.CoreElementsInterfaces;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.SchemeControlTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace SchemeControlsLib.Controls.LibControls
{
    public class ZoomBorder : Border
    {
        #region private vars
        private UIElement child = null;
        private Point origin;
        private Point start;
        #endregion

        #region dependency properties    
        public bool EnableScrolling
        {
            get { return (bool)GetValue(EnableScrollingProperty); }
            set { SetValue(EnableScrollingProperty, value); }
        }

        public static readonly DependencyProperty EnableScrollingProperty =
            DependencyProperty.Register("EnableScrolling", typeof(bool), typeof(ZoomBorder), new PropertyMetadata(true));

        public bool IsUsingCtrlKeyToMove
        {
            get { return (bool)GetValue(IsUsingCtrlKeyToMoveProperty); }
            set { SetValue(IsUsingCtrlKeyToMoveProperty, value); }
        }

        public static readonly DependencyProperty IsUsingCtrlKeyToMoveProperty =
            DependencyProperty.Register("IsUsingCtrlKeyToMove", typeof(bool), typeof(ZoomBorder), new PropertyMetadata(false));

        public SpecificPlace ZoomToSpecyficPlace
        {
            get { return (SpecificPlace)GetValue(ZoomToSpecyficPlaceProperty); }
            set { SetValue(ZoomToSpecyficPlaceProperty, value); }
        }

        public static readonly DependencyProperty ZoomToSpecyficPlaceProperty =
            DependencyProperty.Register("ZoomToSpecyficPlace", typeof(SpecificPlace), typeof(ZoomBorder), new PropertyMetadata(new SpecificPlace(0,0,1.0), ZoomToSpecyficPlace_PropertyChanged));
        

        private static void ZoomToSpecyficPlace_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ZoomBorder zoomBorder = d as ZoomBorder;
            SpecificPlace specificPlace = (SpecificPlace)e.NewValue;

            if (specificPlace == null || specificPlace == new SpecificPlace(0))
                zoomBorder.Reset();
            else
                zoomBorder.SetSpecyficArea(specificPlace);
        }

        public SpecificPlace ActualZoom
        {
            get { return (SpecificPlace)GetValue(ActualZoomProperty); }
            set { SetValue(ActualZoomProperty, value); }
        }

        public static readonly DependencyProperty ActualZoomProperty =
            DependencyProperty.Register("ActualZoom", typeof(SpecificPlace), typeof(ZoomBorder), new PropertyMetadata(new SpecificPlace(0, 0, 1.0), ActualZoom_PropertyChanged));

        private static void ActualZoom_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ZoomBorder zoomBorder = (ZoomBorder)d;
            zoomBorder.child_HideZoomDependencyElements();
        }

        public double MaxZoom
        {
            get { return (double)GetValue(MaxZoomProperty); }
            set { SetValue(MaxZoomProperty, value); }
        }

        public static readonly DependencyProperty MaxZoomProperty =
            DependencyProperty.Register("MaxZoom", typeof(double), typeof(ZoomBorder), new PropertyMetadata(3.0));
        #endregion

        public override UIElement Child
        {
            get { return base.Child; }
            set
            {
                if (value != null && value != this.Child)
                    this.Initialize(value);
                base.Child = value;
            }           
        }

        public void Initialize(UIElement element)
        {
            this.child = element;
            if (child != null)
            {
                TransformGroup group = new TransformGroup();
                ScaleTransform st = new ScaleTransform();
                group.Children.Add(st);
                TranslateTransform tt = new TranslateTransform();
                group.Children.Add(tt);
                child.RenderTransform = group;
                child.RenderTransformOrigin = new Point(0.0, 0.0);
                this.MouseWheel += child_MouseWheel;
                this.MouseDown += child_MouseDown;
                this.MouseRightButtonDown += child_MouseRightButtonDown;
                this.MouseRightButtonUp += child_MouseRightButtonUp;
                this.MouseMove += child_MouseMove;
            }
        }

        private void child_HideZoomDependencyElements()
        {
            if(this.child is SchemeCanvas && !((SchemeCanvas)this.child).IsEditMode)
            {
                var zoomHideableElements = ((SchemeCanvas)this.child).Children.OfType<ISpecificPlaceElement>();
                var specificZoomPlaces = ((SchemeCanvas)this.child).Children.OfType<ISpecificZoomPlace>();

                var specificZoomPlace = specificZoomPlaces.Where(x => x.SpecificPlace == ActualZoom).FirstOrDefault();

                if(specificZoomPlace is FrameworkElement)
                {
                    var zoomPlace = (FrameworkElement)specificZoomPlace;

                    var placeLeftLow = Canvas.GetLeft(zoomPlace);
                    var placeLeftHigh = placeLeftLow + zoomPlace.Width;
                    var placeTopLow = Canvas.GetTop(zoomPlace);
                    var placeTopHigh = placeTopLow + zoomPlace.Height;

                    foreach (var zoomHideableElement in zoomHideableElements)
                    {
                        if(zoomHideableElement.SpecificPlaceElement == true && zoomHideableElement is FrameworkElement)
                        {
                            var element = (FrameworkElement)zoomHideableElement;
                            var elementLeft = Canvas.GetLeft(element);
                            var elementTop = Canvas.GetTop(element);
                            if(elementLeft >= placeLeftLow && elementLeft <= placeLeftHigh && elementTop >= placeTopLow && elementTop <= placeTopHigh)
                            {
                                element.Visibility = Visibility.Visible;
                            }
                            else
                                element.Visibility = Visibility.Collapsed;
                        }
                    }
                }
                else
                {
                    foreach (var zoomHideableElement in zoomHideableElements)
                    {
                        if (zoomHideableElement.SpecificPlaceElement && zoomHideableElement is FrameworkElement)
                            ((FrameworkElement)zoomHideableElement).Visibility = Visibility.Collapsed;
                    }
                }
            }
            else if(this.child is SchemeCanvas && ((SchemeCanvas)this.child).IsEditMode)
            {
                var zoomHideableElements = ((SchemeCanvas)this.child).Children.OfType<ISpecificPlaceElement>();
                foreach(var zoomHideableElement in zoomHideableElements)
                {
                    if (zoomHideableElement is FrameworkElement)
                        ((FrameworkElement)zoomHideableElement).Visibility = Visibility.Visible;
                }
            }
        }

        private void child_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.MiddleButton == MouseButtonState.Pressed)
                if (EnableScrolling)
                {
                    this.Reset();
                    return;
                }
        }

        public void Reset()
        {
            if (child != null)
            {
                // reset zoom
                var st = GetScaleTransform(child);
                st.ScaleX = 1.0;
                st.ScaleY = 1.0;

                // reset pan
                var tt = GetTranslateTransform(child);
                tt.X = 0.0;
                tt.Y = 0.0;
            }
        }

        #region Child Events
        private TranslateTransform GetTranslateTransform(UIElement element)
        {
            return (TranslateTransform)((TransformGroup)element.RenderTransform)
              .Children.First(tr => tr is TranslateTransform);
        }

        private ScaleTransform GetScaleTransform(UIElement element)
        {
            return (ScaleTransform)((TransformGroup)element.RenderTransform)
              .Children.First(tr => tr is ScaleTransform);
        }

        // do poprawy jeśli za dużo animacji (coś z tym zrobić)
        public void SetZoomAnimationToCenterPoint(double zoom, Point centerPoint, int zoomInterval)
        {
            _zoomScaleToSet = zoom;
            _zoomCenterPoint = centerPoint;
            _zoomStep = (zoom - Math.Abs(ActualZoom.Scale)) / _animationQuantity;
            _animationTimer.Animate(zoomInterval, OnTimerElapsed, "ZoomBorder");
        }

        #region ZoomAnimation
        private Dispatcher _Dispatcher = Dispatcher.CurrentDispatcher; //Application.Current.Dispatcher;
        private AnimationTimer _animationTimer = new AnimationTimer();
        private double _zoomScaleToSet = 1.0;
        private double _zoomStep = 0;
        private Point _zoomCenterPoint;
        private int _animationQuantity = 60;

        protected virtual void OnTimerElapsed()
        {
            Point currentFrameCenterPoint = new Point(ActualZoom.Left + (this.ActualWidth / ActualZoom.Scale) / 2.0, ActualZoom.Top + (this.ActualHeight / ActualZoom.Scale) / 2.0);

            if (Math.Abs(ActualZoom.Scale) <= _zoomScaleToSet || !currentFrameCenterPoint.CompareWithDeviation(_zoomCenterPoint, 2))
            {
                var newZoom = ActualZoom.Scale;
                var borderNewWidth = this.ActualWidth / (newZoom * 2.0);
                var borderNewHeight = this.ActualHeight / (newZoom * 2.0);
                if (Math.Abs(ActualZoom.Scale) <= _zoomScaleToSet)
                {
                    newZoom = ActualZoom.Scale + _zoomStep;
                    borderNewWidth = this.ActualWidth / (newZoom * 2.0);
                    borderNewHeight = this.ActualHeight / (newZoom * 2.0);
                }
                SpecificPlace specificPlace = new SpecificPlace(_zoomCenterPoint.Y - borderNewHeight, _zoomCenterPoint.X - borderNewWidth, newZoom);
                SetSpecyficArea(specificPlace);
            }
            else
            {
                _animationTimer.CleanTimer();
            }
        }
        #endregion ZoomAnimation


        public void SetSpecyficArea(SpecificPlace specificPlace)
        {
            if (child != null)
            {
                specificPlace.CutSpecificPlaceToWidthAndHeight(this.ActualWidth, this.ActualHeight);
                var st = GetScaleTransform(child);
                var tt = GetTranslateTransform(child);

                tt.X = -specificPlace.Left * specificPlace.Scale;
                tt.Y = -specificPlace.Top * specificPlace.Scale;

                st.ScaleX = specificPlace.Scale;
                st.ScaleY = specificPlace.Scale;


                ActualZoom = new SpecificPlace(-Math.Round(tt.Y / st.ScaleY, 1), - Math.Round(tt.X / st.ScaleX, 1), st.ScaleX);
            }
        }

        private void child_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (child != null && EnableScrolling)
            {
                if (e.MiddleButton == MouseButtonState.Pressed)
                    if (EnableScrolling)
                    {
                        this.Reset();
                        return;
                    }

                var st = GetScaleTransform(child);
                var tt = GetTranslateTransform(child);
                double zoom = e.Delta > 0 ? .4 : -.4;

                if ((st.ScaleX <= MaxZoom && st.ScaleY <= MaxZoom) || zoom < 0)
                {

                    if (!(e.Delta > 0) && (st.ScaleX < .4 || st.ScaleY < .4))
                        return;

                    Point relative = e.GetPosition(child);
                    double abosuluteX;
                    double abosuluteY;

                    abosuluteX = relative.X * st.ScaleX + tt.X;
                    abosuluteY = relative.Y * st.ScaleY + tt.Y;


                    st.ScaleX += zoom;
                    st.ScaleY += zoom;

                    var height = (double)child.GetValue(HeightProperty) * st.ScaleY;
                    var width = (double)child.GetValue(WidthProperty) * st.ScaleX;
                    if (abosuluteX - relative.X * st.ScaleX < -width + width / st.ScaleX)
                        tt.X = -width + width / st.ScaleX;
                    else if (abosuluteX - relative.X * st.ScaleX > 0)
                        tt.X = 0;
                    else
                        tt.X = abosuluteX - relative.X * st.ScaleX;

                    if (abosuluteY - relative.Y * st.ScaleY < -height + height / st.ScaleY)
                        tt.Y = -height + height / st.ScaleY;
                    else if (abosuluteY - relative.Y * st.ScaleY > 0)
                        tt.Y = 0;
                    else
                        tt.Y = abosuluteY - relative.Y * st.ScaleY;

                    if (st.ScaleX < 1.0 || st.ScaleY < 1.0)
                        this.Reset();

                    ActualZoom = new SpecificPlace(-Math.Round(tt.Y/ st.ScaleY ,1), -Math.Round(tt.X / st.ScaleX, 1), st.ScaleX);
                }
            }
        }

        private void child_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (child != null && EnableScrolling)
            {
                if (Keyboard.Modifiers == ModifierKeys.Control)
                {
                    var tt = GetTranslateTransform(child);
                    var st = GetScaleTransform(child);
                    if (st.ScaleX > 1.0 || st.ScaleY > 1.0)
                    {
                        start = e.GetPosition(this);
                        origin = new Point(tt.X, tt.Y);
                        this.Cursor = Cursors.Hand;
                        child.CaptureMouse();
                    }
                }
            }
        }

        private void child_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (child != null)
            {
                child.ReleaseMouseCapture();
                this.Cursor = Cursors.Arrow;
            }
        }

        private void child_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (child != null && EnableScrolling)
            {
                if (IsUsingCtrlKeyToMove && Keyboard.Modifiers == ModifierKeys.Control || !IsUsingCtrlKeyToMove)
                {
                    var tt = GetTranslateTransform(child);
                    var st = GetScaleTransform(child);
                    if (st.ScaleX > 1.0 || st.ScaleY > 1.0)
                    {
                        start = e.GetPosition(this);
                        origin = new Point(tt.X, tt.Y);
                        this.Cursor = Cursors.Hand;
                        child.CaptureMouse();
                    }
                }
            }
        }

        private void child_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (child != null)
            {
                child.ReleaseMouseCapture();
                this.Cursor = Cursors.Arrow;
            }
        }

        private void child_MouseMove(object sender, MouseEventArgs e)
        {
            if (child != null && EnableScrolling)
            {
                if (IsUsingCtrlKeyToMove && Keyboard.Modifiers == ModifierKeys.Control || !IsUsingCtrlKeyToMove)
                {
                    if (child.IsMouseCaptured)
                    {
                        var tt = GetTranslateTransform(child);
                        var st = GetScaleTransform(child);

                        var height = (double)child.GetValue(ActualHeightProperty) * st.ScaleY;
                        var width = (double)child.GetValue(ActualWidthProperty) * st.ScaleX;
                        Vector v = start - e.GetPosition(this);

                        if (tt.X >= 0 && v.X < 0)
                            tt.X = 0;
                        else if (tt.X <= -width + width / st.ScaleX && v.X > 0)
                            tt.X = -width + width / st.ScaleX;
                        else
                            tt.X = origin.X - v.X;

                        if (tt.Y <= 0 && origin.Y - v.Y > 0)
                            tt.Y = 0;
                        else if (tt.Y <= -height + height / st.ScaleY && v.Y > 0)
                            tt.Y = -height + height / st.ScaleY;
                        else
                            tt.Y = origin.Y - v.Y;

                        ActualZoom = new SpecificPlace(-Math.Round(tt.Y / st.ScaleY, 1), -Math.Round(tt.X / st.ScaleX, 1), st.ScaleX);
                    }

                }
            }
        }
        #endregion
    }
}

