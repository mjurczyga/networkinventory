﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.LibElementsControls;
using SchemeControlsLib.Controls.NetworkControls.Types;
using SchemeControlsLib.Controls.Types;
using SchemeControlsLib.Converters;
using SchemeControlsLib.Helpers;
using SchemeControlsLib.Helpers.ValidationRules;
using SchemeControlsLib.Inputs;
using SchemeControlsLib.Models;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.LibControls
{
    public class ElementPropertiesControl : ObservableControl
    {
        public ICommand StackPanelLoadedCommand { get; private set; }

        private Dictionary<Type, Type> _ComboboxTypes = new Dictionary<Type, Type>()
        {
            { typeof(FontWeight), typeof(FontWeights) },
            { typeof(FontStyle), typeof(FontStyles) },
            //{ typeof(TextDecorationCollection), typeof(TextDecorations) },

        };

        #region dependency properties

        public Thickness TextMargin
        {
            get { return (Thickness)GetValue(TextMarginProperty); }
            set { SetValue(TextMarginProperty, value); }
        }

        public static readonly DependencyProperty TextMarginProperty =
            DependencyProperty.Register("TextMargin", typeof(Thickness), typeof(ElementPropertiesControl), new PropertyMetadata(new Thickness(0)));



        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(ElementPropertiesControl), new PropertyMetadata(null, Text_PropertyChanged));

        private static void Text_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is ElementPropertiesControl elementPropertiesControl)
            {
                if (string.IsNullOrEmpty(e.NewValue?.ToString()))
                    elementPropertiesControl.TextVisibility = Visibility.Collapsed;
                else
                    elementPropertiesControl.TextVisibility = Visibility.Visible;
            }
        }

        private Visibility _TextVisibility = Visibility.Collapsed;
        public Visibility TextVisibility
        {
            get { return _TextVisibility; }
            set { _TextVisibility = value; NotifyPropertyChanged("TextVisibility"); }
        }

        #endregion

        private void LoadPropertiesFromControl(bool isCommon)
        {
            if (DependencyObject != null)
            {
                var properties = ElementPropertiesModels.GetPropertiesByElementType(DependencyObject);
                if (isCommon)
                    properties = properties.Where(x => x.Value.CommonProperty == true).ToDictionary(x => x.Key, y => y.Value);

                _elementProperties = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesWithAttributesDict(DependencyObject, properties).ToDictionary(x => x.Key as DependencyProperty, x => x.Value);
            }
        }

        private Dictionary<DependencyProperty, EditorProperty> _elementProperties;


        private DependencyObject _DependencyObject;
        public DependencyObject DependencyObject
        {
            get { return _DependencyObject; }
            set { _DependencyObject = value; NotifyPropertyChanged("DependencyObject"); }
        }

        private object _PropertiesDataContext;
        public object PropertiesDataContext { get { return _PropertiesDataContext; } set { _PropertiesDataContext = value; NotifyPropertyChanged("PropertiesDataContext"); } }

        public event PropertiesChangedDelegate ElementPropertiesChangedEvent;

        static ElementPropertiesControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ElementPropertiesControl), new FrameworkPropertyMetadata(typeof(ElementPropertiesControl)));
        }

        public ElementPropertiesControl(DependencyObject multiSourceBrushControl, bool isCommon = false)
        {
            DependencyObject = multiSourceBrushControl;
            LoadPropertiesFromControl(isCommon);
        }



        //private string TryUpdateName(string name)
        //{
        //    if (TextExt.CheckIfIsNullOrEpmpty(name))
        //        return "Nazwa nie może być pusta.";
        //    else if (TextExt.CheckIfStringIsSymbol(name, '_'))
        //        return "Nazwa nie może zawierać znaków specjalnych (wyjątek '_').";
        //    else if (TextExt.CheckIfSpaces(name))
        //        return "Nazwa nie może zawierać spacji.";
        //    else
        //    {
        //        FrameworkElement?.SetValue(FrameworkElement.NameProperty, name);
        //        RiseUpdateVisualPropertyChanged();
        //        return "";
        //    }
        //}

        private string TryUpdateDoubleValue(DependencyProperty dp, string value)
        {
            var parsingResult = double.TryParse(value, out double parsingResultValue);
            if (parsingResult && DependencyObject != null)
            {
                DependencyObject.SetValue(dp, parsingResultValue);
                RiseUpdateVisualPropertyChanged();
                return "";
            }
            else
                return "Podana wartość nie jest typu double.";
        }

        private void RiseUpdateVisualPropertyChanged()
        {
            if (ElementPropertiesChangedEvent != null)
            {
                if(DependencyObject is FrameworkElement frameworkElement)
                    ElementPropertiesChangedEvent(this, new PropertiesChangedEventArgs(frameworkElement));
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            OnControlLoadedLoadedCommand();
        }

        private void OnControlLoadedLoadedCommand()
        {
            ScrollViewer scrollViewer = new ScrollViewer() { MaxHeight = 400, CanContentScroll = true, Padding = new Thickness(5), VerticalScrollBarVisibility = ScrollBarVisibility.Auto, Foreground = this.Foreground };
            StackPanel stackPanel = new StackPanel();
            CreateStackPanelChildren(stackPanel);

            scrollViewer.Content = stackPanel;

            PropertiesDataContext = scrollViewer;
        }

        private void CreateStackPanelChildren(StackPanel stackPanel)
        {
            if (_elementProperties != null)
            {
                foreach (var property in _elementProperties)
                {
                    var toolTip = property.Value.Description;
                    if (!string.IsNullOrEmpty(property.Value.ToolTip))
                        toolTip = property.Value.ToolTip;

                    if (property.Key.PropertyType.IsEnum || property.Key.PropertyType.GetInterfaces().Any(x => x == typeof(IClassType)) || _ComboboxTypes.ContainsKey(property.Key.PropertyType))
                    {
                        var control = CreateComboBoxControl(property.Key, DependencyObject);
                        control.ToolTip = toolTip;

                        stackPanel.Children.Add(CreatePropertyControl(property.Value, control));
                    }
                    else if (property.Key.PropertyType == typeof(bool))
                    {
                        var control = CreateCheckBoxControl(property.Key, DependencyObject);
                        control.ToolTip = toolTip;

                        stackPanel.Children.Add(CreatePropertyControl(property.Value, control));
                    }
                    else if (property.Key.PropertyType == typeof(Brush))
                    {
                        var control = CreateLocalColorPickerControl(property.Key, DependencyObject);
                        control.ToolTip = toolTip;

                        stackPanel.Children.Add(CreatePropertyControl(property.Value, control));
                    }
                    else
                    {
                        var control = CreateTextFrame(property.Key, DependencyObject);
                        control.ToolTip = toolTip;

                        stackPanel.Children.Add(CreatePropertyControl(property.Value, control));
                    }
                }
            }

        }

        private PropertyControl CreatePropertyControl(EditorProperty propertyName, UIElement propertyContent)
        {
            var propertyControl = new PropertyControl() { PropertyName = propertyName.Description, PropertyValueContent = propertyContent, PropertyNameForeground = this.Foreground, Margin = new Thickness(0, 2, 0, 0) };
            propertyControl.SetBinding(PropertyControl.TextMarginProperty, new Binding("TextMargin") { Source = this });

            return propertyControl;
        }

        #region metody dodające kontrolki
        private TextBox CreateTextBoxControl(DependencyProperty property, FrameworkElement sourceElement)
        {
            var resultsTextBox = new TextBox() { Foreground = this.Foreground, Background = this.BorderBrush, VerticalContentAlignment = VerticalAlignment.Center };
            Type propertyType = property.PropertyType;

            Binding binding = new Binding();
            binding.Path = new PropertyPath(property);
            binding.Source = sourceElement;
            if (property.Name == "Name")
            {
                var nameValidation = new FrameworkElementlNameValidationRule();
                binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                binding.ValidatesOnDataErrors = true;

                Binding bindingValue = new Binding("ChildrenNames");
                bindingValue.Source = sourceElement.Parent;
                bindingValue.Mode = BindingMode.OneWay;
                var enumerableProperty = new IEnumerableProperty();

                BindingOperations.SetBinding(enumerableProperty, IEnumerableProperty.ValueProperty, bindingValue);
                nameValidation.IEnumerableProperty = enumerableProperty;
                nameValidation.StringProperty = new StringProperty() { Value = sourceElement.Name };
                binding.ValidationRules.Add(nameValidation);
            }
            else if (property.Name == "Text" || property.Name == "ClickCommandParameter")
            {
                resultsTextBox.TextWrapping = TextWrapping.Wrap;
                resultsTextBox.AcceptsReturn = true;
                resultsTextBox.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
                resultsTextBox.MaxLines = 12;
            }
            resultsTextBox.SetBinding(TextBox.TextProperty, binding);

            return resultsTextBox;
        }

        private TextFrame CreateTextFrame(DependencyProperty property, DependencyObject sourceElement)
        {
            var result = new TextFrame() { Foreground = this.Foreground, FontSize = this.FontSize, Background = this.BorderBrush };
            Type propertyType = property.PropertyType;

            Binding binding = new Binding();
            binding.Path = new PropertyPath(property);
            binding.Source = sourceElement;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            binding.Mode = BindingMode.TwoWay;
            if (property.Name == "Name" && sourceElement is FrameworkElement frameworkElement)
            {
                var nameValidation = new FrameworkElementlNameValidationRule();
                binding.ValidatesOnDataErrors = true;

                Binding bindingValue = new Binding("ChildrenNames");
                bindingValue.Source = frameworkElement.Parent;
                bindingValue.Mode = BindingMode.OneWay;
                var enumerableProperty = new IEnumerableProperty();

                BindingOperations.SetBinding(enumerableProperty, IEnumerableProperty.ValueProperty, bindingValue);
                nameValidation.IEnumerableProperty = enumerableProperty;
                nameValidation.StringProperty = new StringProperty() { Value = frameworkElement.Name };
                binding.ValidationRules.Add(nameValidation);
            }
            //else if(property.Name == "Text" || property.Name == "ClickCommandParameter")
            //{

            //}
            result.SetBinding(TextFrame.TextProperty, binding);

            return result;
        }

        private ComboBox CreateComboBoxControl(DependencyProperty property, DependencyObject sourceElement)
        {
            Binding binding = new Binding(property.Name);
            binding.Source = sourceElement;

            ComboBox resultsComboBox = null;

            if (property.PropertyType.IsEnum)
            {
                resultsComboBox = new ComboBox() { Background = this.Background, BorderBrush = this.BorderBrush, SelectedItem = Enum.GetName(property.PropertyType, sourceElement.GetValue(property)) };
                resultsComboBox.ItemsSource = Enum.GetNames(property.PropertyType);

                resultsComboBox.SelectionChanged += delegate (object sender, SelectionChangedEventArgs e)
                {
                    sourceElement.SetValue(property, Enum.Parse(property.PropertyType, resultsComboBox.SelectedItem.ToString()));
                    this.RiseUpdateVisualPropertyChanged();

                };
            }
            else if (property.PropertyType.GetInterfaces().Any(x => x == typeof(IClassType)))
            {
                var settedValue = sourceElement.GetValue(property) as ClassType;

                resultsComboBox = new ComboBox() { Background = this.Background, BorderBrush = this.BorderBrush, SelectedItem = settedValue, SelectedIndex = -1,
                    SelectedValuePath = "Name", DisplayMemberPath = "Name" };
                resultsComboBox.ItemsSource = settedValue.GetClassTypes();

                resultsComboBox.SelectionChanged += delegate (object sender, SelectionChangedEventArgs e)
                {
                    sourceElement.SetValue(property, resultsComboBox.SelectedItem);
                    this.RiseUpdateVisualPropertyChanged();

                };
            }
            else if (_ComboboxTypes.ContainsKey(property.PropertyType))
            {
                var settedValue = sourceElement.GetValue(property);
                var comboboxTypes = _ComboboxTypes.Where(x => x.Key == property.PropertyType).FirstOrDefault();
                if (comboboxTypes.Key != null)
                {
                    resultsComboBox = new ComboBox()
                    { Background = this.Background, BorderBrush = this.BorderBrush, SelectedItem = settedValue, SelectedIndex = -1 };
                    resultsComboBox.ItemsSource = GetTypesDictFromTypes(comboboxTypes.Value, comboboxTypes.Key);

                    resultsComboBox.SelectionChanged += delegate (object sender, SelectionChangedEventArgs e)
                    {
                        sourceElement.SetValue(property, resultsComboBox.SelectedItem);
                        this.RiseUpdateVisualPropertyChanged();

                    };
                }
            }

            return resultsComboBox;
        }

        private CheckBox CreateCheckBoxControl(DependencyProperty property, object sourceElement)
        {
            Binding binding = new Binding(property.Name);
            binding.Source = sourceElement;
            var resultsCheckBox = new CheckBox() { Foreground = this.Foreground, VerticalContentAlignment = VerticalAlignment.Center };
            resultsCheckBox.SetBinding(CheckBox.IsCheckedProperty, binding);
            return resultsCheckBox;
        }

        //private ColorPicker CreateColorPickerControl(DependencyProperty property, UIElement sourceElement)
        //{
        //    var resultsColorPicker = new ColorPicker() { Foreground = Brushes.Black, TabForeground = Brushes.Black, HeaderForeground = Brushes.Black };
        //    Type propertyType = property.PropertyType;
        //    if (sourceElement.GetValue(property) != null)
        //        resultsColorPicker.SelectedColor = ((SolidColorBrush)sourceElement.GetValue(property)).Color;

        //    resultsColorPicker.SelectedColorChanged += delegate (object sender, RoutedPropertyChangedEventArgs<Color?> e)
        //    {
        //        if (e.NewValue != null)
        //        {
        //            var newColor = (Color)e.NewValue;
        //            sourceElement.SetValue(property, new SolidColorBrush(newColor));
        //        }
        //    };
        //    return resultsColorPicker;
        //}

        private LocalColorPicker CreateLocalColorPickerControl(DependencyProperty property, DependencyObject sourceElement)
        {
            var resultsColorPicker = new LocalColorPicker() { Foreground = Brushes.Black };
            Type propertyType = property.PropertyType;
            //if (sourceElement.GetValue(property) != null)
            //{
            //    resultsColorPicker.SelectedColor = ((SolidColorBrush)sourceElement.GetValue(property)).Color;
            //}
            //resultsColorPicker.Height = "ABC".ComputeTextSize(this.FontSize).Height;
            Binding binding = new Binding();
            binding.Path = new PropertyPath(property);
            binding.Source = sourceElement;
            //binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            binding.Converter = new BrushToColorConverter();
            //binding.Mode = BindingMode.OneWay;
            resultsColorPicker.SetBinding(LocalColorPicker.SelectedColorProperty, binding);

            resultsColorPicker.SelectedColorChanged += delegate (object sender, RoutedPropertyChangedEventArgs<Color?> e)
            {
                if (e.NewValue != null)
                {
                    var newBrush = new SolidColorBrush((Color)e.NewValue);
                    newBrush.Freeze();

                    sourceElement.SetValue(property, newBrush);
                }
            };
            return resultsColorPicker;
        }
        #endregion

        private List<object> GetTypesDictFromTypes(Type staticTypes, Type typeInStaticTypes)
        {
            var sources = staticTypes.GetProperties(BindingFlags.Public | BindingFlags.Static).
            Where(f => f.PropertyType == typeInStaticTypes).
            Select(f => f.GetValue(null)).ToList();

            return sources;
        }

    }
}
