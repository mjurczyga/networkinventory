﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.CommonControls.Interfaces
{
    public interface ILabelControl
    {
        string LabelText { get; set; }
        double FontSize { get; set; }
        Brush Foreground { get; set; }
        Point LineStartPoint { get; set; }
        double LabelDistance { get; set; }
        double LineAngle { get; set; }
        Brush Stroke { get; set; }
        double StrokeThickness { get; set; }

    }
}
