﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.Interfaces.CoreElementsInterfaces
{
    public interface ISpecificPlaceElement
    {
        bool SpecificPlaceElement { get; set; }
    }
}
