﻿using SchemeControlsLib.SchemeControlTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.Interfaces.CoreElementsInterfaces
{
    public interface ISpecificZoomPlace
    {
        string PlaceName { get; }

        SpecificPlace SpecificPlace { get; }
    }
}
