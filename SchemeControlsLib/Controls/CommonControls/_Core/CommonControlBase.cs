﻿using SchemeControlsLib.Base;
using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.Helpers;
using SchemeControlsLib.Models;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Core
{
    public abstract class CommonControlBase : EditorControlBase, IEditableControl, IChangeableControl, ISelectableElement, IGroupSelectableElement, ISearchingElement
    {
        protected abstract void DrawControl(DrawingContext drawingContext);

        #region publiczne metody/funkcje
        [EditorProperty("Tryb edycji")]
        public bool IsEditMode
        {
            get { return (bool)GetValue(IsEditModeProperty); }
            set { SetValue(IsEditModeProperty, value); }
        }

        public static readonly DependencyProperty IsEditModeProperty =
            DependencyProperty.Register("IsEditMode", typeof(bool), typeof(CommonControlBase), new PropertyMetadata(false, IsEditMode_PropertyChanged));

        private static void IsEditMode_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CommonControlBase controlBase = (CommonControlBase)d;
            controlBase.ChangeEditMode((bool)e.NewValue);
        }

        /// <summary>
        /// actions for changed Edit mode
        /// </summary>
        /// <param name="isEditMode"></param>
        protected virtual void ChangeEditMode(bool isEditMode)
        {

        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public object Copy()
        {
            var copiedElement = Activator.CreateInstance(this.GetType()) as ObservableControl;

            var thisDependencyProperties = DependencyPropertyFinder.GetAllDependencyProperties(this);
            foreach (var property in thisDependencyProperties)
            {
                if (!property.ReadOnly && property.Name.ToUpper() != "NAME" && this.GetValue(property) != null)
                    copiedElement.SetValue(property, this.GetValue(property));
                else if (!property.ReadOnly && property.Name.ToUpper() != "NAME" && this.GetValue(property) != null && this.Parent is Canvas)
                    copiedElement.SetValue(property, CanvasUtilities.GetNextNewElementName((Canvas)this.Parent));
            }

            var eventsFields = DependencyPropertyFinder.GetAllRoutedEvents(this);

            foreach (var eventField in eventsFields)
            {
                copiedElement.AddHandler((RoutedEvent)eventField.Key.GetValue(this), eventField.Value.Handler);
            }

            if (copiedElement is IEditorElement oldEditorElement)
                oldEditorElement.SetPosition(this.X_Position, this.Y_Position); 
            else
                Canvas.SetLeft(copiedElement, Canvas.GetLeft(this));
                Canvas.SetTop(copiedElement, Canvas.GetTop(this));

            return copiedElement;
        }

        public void Remove()
        {
            if (this.Parent is SchemeCanvas)
            {
                var schemeCanvas = (SchemeCanvas)this.Parent;
                schemeCanvas.RemoveElementFromScheme(this);
            }
        }

        /// <summary>
        /// Ustawia nazwę kontrolki z walidacją
        /// </summary>
        /// <param name="name">nazwa</param>
        /// <returns>tru jeśli ustawiło</returns>
        public bool SetName(string name)
        {
            if (this.Parent is SchemeCanvas && name.CheckIfIsControlName() && !((SchemeCanvas)this.Parent).CheckIfNameExist(name))
            {
                this.Name = name;
                return true;
            }
            return false;
        }

        /// <summary>
        /// wpisuje wartość do podanej właściwości
        /// </summary>
        /// <param name="propertyName">nazwa właściwosci</param>
        /// <param name="value">wartość</param>
        /// <returns></returns>
        public bool SetSpecificPropdpValue(string propertyName, object value)
        {
            var specificPropdp = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(this, ElementPropertiesModels.GetPropertiesByElementType(this)).Where(x => x.Key is DependencyProperty).Select(x => x.Key as DependencyProperty).ToList().Where(x => x.Name == propertyName).FirstOrDefault();
            if (value != null && specificPropdp.PropertyType == value.GetType())
            {
                this.SetValue(specificPropdp, value);
                return true;
            }
            return false;
        }
        #endregion

        #region overrides
        protected sealed override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);
            if (!ValueChecker.CheckDoubleNanInfOrZero(this.ActualWidth) && !ValueChecker.CheckDoubleNanInfOrZero(this.ActualHeight))
                DrawControl(drawingContext);
        }
        #endregion

        #region property descriptors
        /// <summary>
        /// Metoda wywoływana gdy zmieni się pozycja kontrolki w SchemeCanvas
        /// </summary>
        protected virtual void PositionInCanvasChanged(object sender, EventArgs e) { }

        DependencyPropertyDescriptor canvasTopPropertyDescriptor = DependencyPropertyDescriptor.FromProperty(Canvas.TopProperty, typeof(CommonControlBase));

        DependencyPropertyDescriptor canvasLeftPropertyDescriptor = DependencyPropertyDescriptor.FromProperty(Canvas.LeftProperty, typeof(CommonControlBase));
        #endregion

        #region ISearchingElement
        [EditorProperty("Tekst wyszukiwania")]
        public string SearchingText
        {
            get { return (string)GetValue(SearchingTextProperty); }
            set { SetValue(SearchingTextProperty, value); }
        }

        public static readonly DependencyProperty SearchingTextProperty =
            DependencyProperty.Register("SearchingText", typeof(string), typeof(CommonControlBase), new PropertyMetadata(null));

        private AnimationTimer _animationTimer = new AnimationTimer();
        private Brush _SavedBrush;
        private Brush _AnimationBrush;
        private int _blinksQuantity;
        private int _blinksCounter;
        private bool _isAnimating;
        public virtual void BlinkElement(Brush colorToBlink, int blinksQuantity, int blinkIntervalMS)
        {
            _blinksCounter = 0;
            if (!_isAnimating)
            {
                _SavedBrush = Foreground;
                _AnimationBrush = colorToBlink;
                _blinksQuantity = blinksQuantity;
                _animationTimer.Animate(blinkIntervalMS, OnTimerElapsed, this.Name);
                _isAnimating = true;
            }

        }

        protected virtual void OnTimerElapsed()
        {
            if (_blinksCounter < _blinksQuantity)
            {
                _blinksCounter++;
                if (Foreground == _AnimationBrush)
                    Foreground = _SavedBrush;
                else
                    Foreground = _AnimationBrush;
            }
            else
            {
                _animationTimer.CleanTimer();
                Foreground = _SavedBrush;
                _isAnimating = false;
                if (this.Parent is SchemeCanvas schemeCanvas)
                {
                    schemeCanvas.RemoveFromSelectionDict(this);
                }
            }
        }
        #endregion ISearchingElement


        public CommonControlBase()
        {
            //canvasTopPropertyDescriptor.AddValueChanged(this, PositionInCanvasChanged);
            //canvasLeftPropertyDescriptor.AddValueChanged(this, PositionInCanvasChanged);
        }
    }
}
