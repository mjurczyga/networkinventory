﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Helpers;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.CommonControls
{
    [EditorType(SchemeTypes.Common, true, "Przycisk")]
    public sealed class SchemeButton : CommonControlBase, IMovableElement, IMovableSelectionElement, IBindingControl
    {
        #region IsSelectionMoving
        private bool _IsSelectionMoving;
        public bool IsSelectionMoving
        {
            get { return _IsSelectionMoving; }
            set { _IsSelectionMoving = value; NotifyPropertyChanged("IsSelectionMoving"); }
        }
        #endregion IsSelectionMoving

        #region Ingerited properties
        [EditorProperty("Kolor tła")]
        public new Brush Background { get { return base.Background; } set { base.Background = value; } }

        [EditorProperty("Kolor ramki")]
        public new Brush BorderBrush { get { return base.BorderBrush; } set { base.BorderBrush = value; } }

        [EditorProperty("Rozmier tekstu")]
        public new double FontSize { get { return base.FontSize; } set { base.FontSize = value; } }

        [EditorProperty("Styl tekstu")]
        public new FontWeight FontWeight { get { return base.FontWeight; } set { base.FontWeight = value; } }

        [EditorProperty("Kolor tekstu")]
        public new Brush Foreground { get { return base.Foreground; } set { base.Foreground = value; } }

        [EditorProperty("Tag")]
        public new object Tag { get { return base.Tag; } set { base.Tag = value; } }

        [EditorProperty("Nazwa")]
        public new string Name { get { return base.Name; } set { base.Name = value; } }
        #endregion

        #region Button additionals
        [EditorProperty("Nazwa zmiennej urządzenia")]
        public string VarName
        {
            get { return (string)GetValue(VarNameProperty); }
            set { SetValue(VarNameProperty, value); }
        }

        public static readonly DependencyProperty VarNameProperty =
            DependencyProperty.Register("VarName", typeof(string), typeof(SchemeButton), new PropertyMetadata(null));

        /// <summary>
        /// Jeśli przycisk otwiera okno
        /// </summary>
        [EditorProperty("Tytuł okna")]
        public string WindowTitle
        {
            get { return (string)GetValue(WindowTitleProperty); }
            set { SetValue(WindowTitleProperty, value); }
        }

        public static readonly DependencyProperty WindowTitleProperty =
            DependencyProperty.Register("WindowTitle", typeof(string), typeof(SchemeButton), new PropertyMetadata(null));

        [EditorProperty("Nazwa pliku schematu")]
        public string SchemeFileName
        {
            get { return (string)GetValue(SchemeFileNameProperty); }
            set { SetValue(SchemeFileNameProperty, value); }
        }

        public static readonly DependencyProperty SchemeFileNameProperty =
            DependencyProperty.Register("SchemeFileName", typeof(string), typeof(SchemeButton), new PropertyMetadata(null));

        //Np LumelN14
        [EditorProperty("Nazwa typu urządzenia")]
        public string DeviceTypeName
        {
            get { return (string)GetValue(DeviceTypeNameProperty); }
            set { SetValue(DeviceTypeNameProperty, value); }
        }

        public static readonly DependencyProperty DeviceTypeNameProperty =
            DependencyProperty.Register("DeviceTypeName", typeof(string), typeof(SchemeButton), new PropertyMetadata(null));

        [EditorProperty("Prefix zmiennych pola")]
        public string FieldVarsNamePrefix
        {
            get { return (string)GetValue(FieldVarsNamePrefixProperty); }
            set { SetValue(FieldVarsNamePrefixProperty, value); }
        }

        public static readonly DependencyProperty FieldVarsNamePrefixProperty =
            DependencyProperty.Register("FieldVarsNamePrefix", typeof(string), typeof(SchemeButton), new PropertyMetadata(null));
        #endregion

        #region dependence properties

        //[EditorProperty("Akcja Asix.Evo na kliknięcie")]
        //public object AsixEvoClickAction
        //{
        //    get { return (object)GetValue(AsixEvoClickActionProperty); }
        //    set { SetValue(AsixEvoClickActionProperty, value); }
        //}

        //public static readonly DependencyProperty AsixEvoClickActionProperty =
        //    DependencyProperty.Register("AsixEvoClickAction", typeof(object), typeof(SchemeButton), new PropertyMetadata(null));



        [EditorProperty("Tekst")]
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(SchemeButton), new PropertyMetadata(""));

        [EditorProperty("Zaokrąglenia")]
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(SchemeButton), new PropertyMetadata(null));

        #region ClickCommand
        [EditorProperty("Zdarzenie na kliknięcie", toolTip: "Nazwę zdarzenia należy umieścić w nawiasach kwadratowych: [Zdarzenie]. Zdarzenie wywoływane na kliknięcie to [FieldClickCommand]")]
        public object ClickCommand
        {
            get { return (object)GetValue(ClickCommandProperty); }
            set { SetValue(ClickCommandProperty, value); }
        }

        public static readonly DependencyProperty ClickCommandProperty =
            DependencyProperty.Register("ClickCommand", typeof(object), typeof(SchemeButton), new PropertyMetadata(null, ClickCommand_PropertyChanged));

        
        private static void ClickCommand_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SchemeButton activeElement = (SchemeButton)d;
            if (e.NewValue != null)
                activeElement.Cursor = Cursors.Hand;
            else
                activeElement.Cursor = Cursors.Arrow;
        }


        [EditorProperty("Parametry kliknięcia", toolTip: "Obsługuje zdarzenia z Asix. Przykład: ^OpenDiagram(PARAMETRY)")]
        public object ClickCommandParameters
        {
            get { return (object)GetValue(ClickCommandParametersProperty); }
            set { SetValue(ClickCommandParametersProperty, value); }
        }

        public static readonly DependencyProperty ClickCommandParametersProperty =
            DependencyProperty.Register("ClickCommandParameters", typeof(object), typeof(SchemeButton), new PropertyMetadata(null));


        #endregion ClickCommand

        [EditorProperty("Zdarzenie na MouseOver")]
        public object MouseOverCommand
        {
            get { return (object)GetValue(MouseOverCommandProperty); }
            set { SetValue(MouseOverCommandProperty, value); }
        }

        public static readonly DependencyProperty MouseOverCommandProperty =
            DependencyProperty.Register("MouseEnterCommand", typeof(object), typeof(SchemeButton), new PropertyMetadata(null));
        #endregion

        #region public fields
        private double _ButtonOpacity;
        public double ButtonOpacity
        {
            get { return _ButtonOpacity; }
            set { _ButtonOpacity = value; NotifyPropertyChanged("ButtonOpacity"); }
        }

        private Brush _AccentBrush;
        public Brush AccentBrush
        {
            get { return _AccentBrush; }
            set { _AccentBrush = value; NotifyPropertyChanged("AccentBrush"); }
        }

        public bool IsClickCommandBinded
        {
            get { return this.ClickCommand is ICommand ? true : false; }
        }
        #endregion

        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            if (this.MouseOverCommand is ICommand command)
            {
                command.Execute(this);
            }
            if (IsClickCommandBinded)
            {
                this.ButtonOpacity = 0.8;
                this.AccentBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF3C7FB1"));
            }
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            if (this.MouseOverCommand is ICommand command)
            {
                command.Execute(this);
            }
            this.AccentBrush = Brushes.Transparent;
            this.ButtonOpacity = 0;
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            if (this.ClickCommand is ICommand command)
            {
                if (ClickCommandParameters == null)
                    command.Execute(this);
                else
                    command.Execute(ClickCommandParameters);

            }

        }

        private void UpdateWidthAndHeight()
        {
            this.Width = this.ActualWidth;
            this.Height = this.ActualHeight;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
        }

        protected override void PositionInCanvasChanged(object sender, EventArgs e)
        {

        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);

            if (sizeInfo.HeightChanged)
                this.Height = Math.Round(sizeInfo.NewSize.Height, 1, MidpointRounding.AwayFromZero);
            if (sizeInfo.WidthChanged)
                this.Width = Math.Round(sizeInfo.NewSize.Width, 1, MidpointRounding.AwayFromZero);

        }

        protected override void DrawControl(DrawingContext drawingContext)
        {
        }

        static SchemeButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SchemeButton), new FrameworkPropertyMetadata(typeof(SchemeButton)));
        }

        public SchemeButton()
        {
            this.Background = Brushes.Gray;
        }
    }
}
