﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.Interfaces.CoreElementsInterfaces;
using SchemeControlsLib.SchemeControlTypes;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.CommonControls
{
    [EditorType(SchemeTypes.Common, true, "Miejsce przybliżenia")]
    public class SpecificZoomPlace : CommonControlBase, IMovableElement, IMovableSelectionElement, ISpecificZoomPlace, IHideableElement
    {
        #region Ingerited properties
        [EditorProperty("Rozmiar tekstu")]
        public new double FontSize { get { return base.FontSize; } set { base.FontSize = value; } }

        [EditorProperty("Kolor tekstu")]
        public new Brush Foreground { get { return base.Foreground; } set { base.Foreground = value; } }

        [EditorProperty("Styl tekstu")]
        public new FontWeight FontWeight { get { return base.FontWeight; } set { base.FontWeight = value; } }

        [EditorProperty("Kolor ramki")]
        public new Brush BorderBrush { get { return base.BorderBrush; } set { base.BorderBrush = value; } }

        [EditorProperty("Grubość lini")]
        public new Thickness BorderThickness { get { return base.BorderThickness; } set { base.BorderThickness = value; } }
        #endregion

        #region IsSelectionMoving
        private bool _IsSelectionMoving;
        public bool IsSelectionMoving
        {
            get { return _IsSelectionMoving; }
            set { _IsSelectionMoving = value; NotifyPropertyChanged("IsSelectionMoving"); }
        }
        #endregion IsSelectionMoving

        #region Propdps
        [EditorProperty("Nazwa miejsca")]
        public string PlaceName
        {
            get { return (string)GetValue(PlaceNameProperty); }
            set { SetValue(PlaceNameProperty, value); }
        }

        public static readonly DependencyProperty PlaceNameProperty =
            DependencyProperty.Register("PlaceName", typeof(string), typeof(SpecificZoomPlace));

        public bool HideElement
        {
            get { return (bool)GetValue(HideElementProperty); }
            set { SetValue(HideElementProperty, value); }
        }

        public static readonly DependencyProperty HideElementProperty =
            DependencyProperty.Register("HideElement", typeof(bool), typeof(SpecificZoomPlace), new PropertyMetadata(HideElement_PropertyChanged));

        private static void HideElement_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SpecificZoomPlace specificZoomPlace = (SpecificZoomPlace)d;
            if (bool.TryParse(e.NewValue?.ToString(), out bool result))
            {
                if (result)
                    specificZoomPlace.Visibility = Visibility.Collapsed;
                else
                    specificZoomPlace.Visibility = Visibility.Visible;

            }
            else
            {
                specificZoomPlace.Visibility = Visibility.Visible;
            }
        }
        #endregion

        public SpecificPlace SpecificPlace
        {
            get { return ComputeSpecificPlace(); }
        }

        protected override Size MeasureOverride(Size constraint)
        {
            NotifyPropertyChanged("SpecificPlace");
            return base.MeasureOverride(constraint);
        }

        private SpecificPlace ComputeSpecificPlace()
        {
            if (Parent is Canvas)
            {
                var resultScale = 1.0;
                var parentCanvas = (Canvas)Parent;
                var top = Canvas.GetTop(this);
                var left = Canvas.GetLeft(this);
                var scaleX = Math.Round(parentCanvas.ActualWidth / this.Width, 2);
                var scaleY = Math.Round(parentCanvas.ActualHeight / this.Height, 2);

                if (scaleX < scaleY)
                {
                    resultScale = scaleX;
                    top = top - ((parentCanvas.ActualHeight / resultScale) - this.Height) / 2.0;
                }
                else
                {
                    resultScale = scaleY;
                    left = left - ((parentCanvas.ActualWidth / resultScale) - this.Width) / 2.0;
                }


                return new SpecificPlace(Math.Round(top), Math.Round(left), resultScale);
            }
            return new SpecificPlace(0);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
        }

        protected override void DrawControl(DrawingContext drawingContext)
        {
        }

        static SpecificZoomPlace()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SpecificZoomPlace), new FrameworkPropertyMetadata(typeof(SpecificZoomPlace)));
        }

    }
}
