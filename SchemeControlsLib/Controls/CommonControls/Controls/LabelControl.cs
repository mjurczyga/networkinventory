﻿using SchemeControlsLib.Controls.CommonControls.Interfaces;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Helpers;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.CommonControls
{

    public class LabelControl : EditorControlBase, IBindingControl, ILabelControl
    {
        private static readonly string _controlGraphic_PART = "_controlGraphic_PART";
        protected Grid _controlGraphic_PART_Grid;

        #region ILabelControl
        public string LabelText
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(LabelControl), new PropertyMetadata(Text_PropertyChanged));

        private static void Text_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is LabelControl labelControl)
            {
                labelControl.UpdateGraphic();
            }
        }

        public Point LineStartPoint
        {
            get { return (Point)GetValue(LineStartPointProperty); }
            set { SetValue(LineStartPointProperty, value); }
        }

        public static readonly DependencyProperty LineStartPointProperty =
            DependencyProperty.Register("LineStartPoint", typeof(Point), typeof(LabelControl), new PropertyMetadata(LineStartPoint_PropertyChanged));

        private static void LineStartPoint_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is LabelControl labelControl && e.NewValue is Point newPoint)
            {
                labelControl.SetPosition(newPoint.X, newPoint.Y);
            }
        }

        public double LabelDistance
        {
            get { return (double)GetValue(LabelDistanceProperty); }
            set { SetValue(LabelDistanceProperty, value); }
        }

        public static readonly DependencyProperty LabelDistanceProperty =
            DependencyProperty.Register("LabelDistance", typeof(double), typeof(LabelControl), new PropertyMetadata(LabelDistance_PropertyChanged));

        private static void LabelDistance_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is LabelControl labelControl)
            {
                labelControl.UpdateGraphic();
            }
        }

        public double LineAngle
        {
            get { return (double)GetValue(LineAngleProperty); }
            set { SetValue(LineAngleProperty, value); }
        }

        public static readonly DependencyProperty LineAngleProperty =
            DependencyProperty.Register("LineAngle", typeof(double), typeof(LabelControl), new PropertyMetadata(LineAngle_PropertyChanged));

        private static void LineAngle_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is LabelControl labelControl && double.TryParse(e.NewValue?.ToString(), out double dblval))
            {
                labelControl.LineAngleRad = Math.PI * dblval / 180.0;
            }
        }

        private double _LineAngleRad;
        public double LineAngleRad
        {
            get { return _LineAngleRad; }
            set { _LineAngleRad = value; NotifyPropertyChanged("LineAngleRad"); this.UpdateGraphic(); }
        }


        public Brush Stroke
        {
            get { return (Brush)GetValue(StrokeProperty); }
            set { SetValue(StrokeProperty, value); }
        }

        public static readonly DependencyProperty StrokeProperty =
            DependencyProperty.Register("Stroke", typeof(Brush), typeof(LabelControl), new PropertyMetadata(Brushes.Black, Stroke_PropertyChanged));

        private static void Stroke_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is LabelControl labelControl)
            {
                labelControl.UpdateGraphic();
            }
        }

        public double StrokeThickness
        {
            get { return (double)GetValue(StrokeThicknessProperty); }
            set { SetValue(StrokeThicknessProperty, value); }
        }

        public static readonly DependencyProperty StrokeThicknessProperty =
            DependencyProperty.Register("StrokeThickness", typeof(double), typeof(LabelControl), new PropertyMetadata(Settings.Coal_ElementStrokeThickness, StrokeThickness_PropertyChanged));

        private static void StrokeThickness_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is LabelControl labelControl)
            {
                labelControl.UpdateGraphic();
            }
        }
        #endregion ILabelControl

        public LabelControl()
        {

        }

        public LabelControl(double labelDistance)
        {
            LabelDistance = labelDistance;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            _controlGraphic_PART_Grid = GetTemplateChild(_controlGraphic_PART) as Grid;

            this.UpdateGraphic();
        }

        private Line DrawAngledLine(double angleRadians, double distance, Brush stroke, double strokeThickness, DoubleCollection strokeDashArray)
        {
            double sinLineAngleMark = Math.Sin(angleRadians).GetValueDblSign();
            double cosLineAngleMark = Math.Cos(angleRadians).GetValueDblSign();
            double tgLineAngle = Math.Round(Math.Abs(Math.Tan(angleRadians)), 5);
            double x2 = distance * cosLineAngleMark;
            if (tgLineAngle != 0)
            {
                x2 = Math.Round((distance / tgLineAngle) * cosLineAngleMark, 2);
            }
            double y2 = sinLineAngleMark * distance;
            if (tgLineAngle == 0)
            {
                y2 = 0.0;
            }


            return new Line() { X1 = 0, Y1 = 0, X2 = x2, Y2 = y2, Stroke = stroke, StrokeThickness = strokeThickness, StrokeDashArray = strokeDashArray };
        }

        public void UpdateGraphic()
        {
            if (_controlGraphic_PART_Grid != null)
            {                
                Canvas canvas = new Canvas();

                var angledLine = DrawAngledLine(this.LineAngleRad, this.LabelDistance, this.Stroke, this.StrokeThickness * 0.5, new DoubleCollection() { 2, 2 });
                var textSize = this.LabelText.ComputeTextSize(this.FontSize);
                var sinLineAngleMark = Math.Sin(this.LineAngleRad).GetValueDblSign(); // dodatni dla 0-180
                var cosLineAngleMark = Math.Cos(this.LineAngleRad).GetValueDblSign(); // ujemny dla 90-270
                var textBasisLine = new Line() { X1 = 0, Y1 = 0, X2 = cosLineAngleMark * textSize.Width, Y2 = 0, Stroke = this.Stroke, StrokeThickness = this.StrokeThickness };
                Canvas.SetLeft(textBasisLine, angledLine.X2);
                Canvas.SetTop(textBasisLine, angledLine.Y2);

                var textBlock = new TextBlock() { Text = LabelText, FontSize = base.FontSize, Foreground = Foreground };

                Canvas.SetTop(textBlock, angledLine.Y2 - textSize.Height - this.StrokeThickness * 0.5);
                if (cosLineAngleMark < 0)
                    Canvas.SetLeft(textBlock, angledLine.X2 - textSize.Width);
                else
                    Canvas.SetLeft(textBlock, angledLine.X2);

                canvas.Children.Add(angledLine);
                canvas.Children.Add(textBasisLine);
                canvas.Children.Add(textBlock);

                _controlGraphic_PART_Grid.Children.Clear();
                _controlGraphic_PART_Grid.Children.Add(canvas);
            }
        }

        static LabelControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(LabelControl), new FrameworkPropertyMetadata(typeof(LabelControl)));
        }
    }
}
