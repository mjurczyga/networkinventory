﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.CommonControls
{
    [EditorType(SchemeTypes.Common, true, "Logo eFOLZON")]
    public class Logo_eFOLZON : CommonControlBase, IMovableElement, IMovableSelectionElement, IBindingControl
    {
        [EditorProperty("Kolor tła", "Wygląd")]
        public Brush BackgroundBrush
        {
            get { return (Brush)GetValue(BackgroundBrushProperty); }
            set { SetValue(BackgroundBrushProperty, value); }
        }

        public static readonly DependencyProperty BackgroundBrushProperty =
            DependencyProperty.Register("BackgroundBrush", typeof(Brush), typeof(Logo_eFOLZON));


        [EditorProperty("Zaokrąglenie rogów", "Wygląd")]
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(Logo_eFOLZON), new PropertyMetadata(new CornerRadius(10)));


        [EditorProperty("Padding tekstu", "Wygląd")]
        public Thickness TextPadding
        {
            get { return (Thickness)GetValue(TextPaddingProperty); }
            set { SetValue(TextPaddingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TextPadding.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextPaddingProperty =
            DependencyProperty.Register("TextPadding", typeof(Thickness), typeof(Logo_eFOLZON), new PropertyMetadata(new Thickness(10)));



        #region IsSelectionMoving
        private bool _IsSelectionMoving;
        public bool IsSelectionMoving
        {
            get { return _IsSelectionMoving; }
            set { _IsSelectionMoving = value; NotifyPropertyChanged("IsSelectionMoving"); }
        }
        #endregion IsSelectionMoving

        protected override void DrawControl(DrawingContext drawingContext)
        {

        }

        static Logo_eFOLZON()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Logo_eFOLZON), new FrameworkPropertyMetadata(typeof(Logo_eFOLZON)));
        }
    }
}
