﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.Interfaces.CoreElementsInterfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Services.PLC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.CommonControls
{
    [EditorType(SchemeTypes.Common, true, "Kontrolka pomiarowa")]
    public class MeasureControl : CommonControlBase, IMovableElement, IMovableSelectionElement, IBindingControl, ISpecificPlaceElement, IHideableElement
    {
        #region IsbindedValue
        private bool _IsBindedValue;
        public bool IsBindedValue
        {
            get { return _IsBindedValue; }
            set { _IsBindedValue = value; SetelementsVisibility(value); NotifyPropertyChanged("IsBindedValue"); }
        }

        private void SetelementsVisibility(bool value)
        {
            if (IsEditMode && value)
            {
                ControlTextVisibilit = Visibility.Collapsed;
                BindedTextVisibilit = Visibility.Visible;
            }
            else
            {
                ControlTextVisibilit = Visibility.Visible;
                BindedTextVisibilit = Visibility.Collapsed;
            }
        }

        private Visibility _ControlTextVisibilit = Visibility.Visible;
        public Visibility ControlTextVisibilit
        {
            get { return _ControlTextVisibilit; }
            set { _ControlTextVisibilit = value; NotifyPropertyChanged("ControlTextVisibilit"); }
        }

        private Visibility _BindedTextVisibilit = Visibility.Collapsed;
        public Visibility BindedTextVisibilit
        {
            get { return _BindedTextVisibilit; }
            set { _BindedTextVisibilit = value; NotifyPropertyChanged("BindedTextVisibilit"); }
        }
        #endregion IsbindedValue

        #region IsSelectionMoving
        private bool _IsSelectionMoving;
        public bool IsSelectionMoving
        {
            get { return _IsSelectionMoving; }
            set { _IsSelectionMoving = value; NotifyPropertyChanged("IsSelectionMoving"); }
        }
        #endregion IsSelectionMoving

        #region Ingerited properties
        [EditorProperty("Kolor tła")]
        public new Brush Background { get { return base.Background; } set { base.Background = value; } }

        [EditorProperty("Rozmier tekstu")]
        public new double FontSize { get { return base.FontSize; } set { base.FontSize = value; } }

        [EditorProperty("Styl tekstu")]
        public new FontWeight FontWeight { get { return base.FontWeight; } set { base.FontWeight = value; } }

        [EditorProperty("Kolor tekstu")]
        public new Brush Foreground { get { return base.Foreground; } set { base.Foreground = value; } }

        [EditorProperty("Przeźroczystość")]
        public new double Opacity { get { return base.Opacity; } set { base.Opacity = value; } }

        [EditorProperty("Padding")]
        public new Thickness Padding { get { return base.Padding; } set { base.Padding = value; } }

        [EditorProperty("Kolor ramki")]
        public new Brush BorderBrush { get { return base.BorderBrush; } set { base.BorderBrush = value; } }

        [EditorProperty("Grubość ramki")]
        public new Thickness BorderThickness { get { return base.BorderThickness; } set { base.BorderThickness = value; } }
        #endregion

        #region dependency properties

        #region limit na warrning
        [EditorProperty("Aktywność ostrzeżenia")]
        public object WarrningActivity
        {
            get { return (object)GetValue(WarrningActivityProperty); }
            set { SetValue(WarrningActivityProperty, value); }
        }

        public static readonly DependencyProperty WarrningActivityProperty =
            DependencyProperty.Register("WarrningActivity", typeof(object), typeof(MeasureControl), new PropertyMetadata(null, WarrningActivity_PropertyChanged));

        private static void WarrningActivity_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is MeasureControl measureControl)
            {
                measureControl.UpdateLimitBrush();
            }
        }

        [EditorProperty("Granica ostrzeżenia")]
        public object WarrningLimit
        {
            get { return (object)GetValue(WarrningLimitProperty); }
            set { SetValue(WarrningLimitProperty, value); }
        }

        public static readonly DependencyProperty WarrningLimitProperty =
            DependencyProperty.Register("WarrningLimit", typeof(object), typeof(MeasureControl), new PropertyMetadata(null, WarrningLimit_PropertyChanged));

        private static void WarrningLimit_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is MeasureControl measureControl)
            {
                measureControl.UpdateLimitBrush();
            }
        }

        [EditorProperty("Kolor ostrzeżenia")]
        public Brush WarrningLimitBrush
        {
            get { return (Brush)GetValue(WarrningLimitBrushProperty); }
            set { SetValue(WarrningLimitBrushProperty, value); }
        }

        public static readonly DependencyProperty WarrningLimitBrushProperty =
            DependencyProperty.Register("WarrningLimitBrush", typeof(Brush), typeof(MeasureControl), new PropertyMetadata(null, WarrningLimitBrush_PropertyChanged));

        private static void WarrningLimitBrush_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is MeasureControl measureControl)
            {
                measureControl.UpdateLimitBrush();
            }
        }
        #endregion limit na warrning

        #region limit na alarm
        [EditorProperty("Aktywność alarmu")]
        public object AlarmActivity
        {
            get { return (object)GetValue(AlarmActivityProperty); }
            set { SetValue(AlarmActivityProperty, value); }
        }

        public static readonly DependencyProperty AlarmActivityProperty =
            DependencyProperty.Register("AlarmActivity", typeof(object), typeof(MeasureControl), new PropertyMetadata(null, AlarmActivity_PropertyChanged));

        private static void AlarmActivity_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is MeasureControl measureControl)
            {
                measureControl.UpdateLimitBrush();
            }
        }

        [EditorProperty("Granica alarmu")]
        public object AlarmLimit
        {
            get { return (object)GetValue(AlarmLimitProperty); }
            set { SetValue(AlarmLimitProperty, value); }
        }

        public static readonly DependencyProperty AlarmLimitProperty =
            DependencyProperty.Register("AlarmLimit", typeof(object), typeof(MeasureControl), new PropertyMetadata(null, AlarmLimit_PropertyChanged));

        private static void AlarmLimit_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is MeasureControl measureControl)
            {
                measureControl.UpdateLimitBrush();
            }
        }

        [EditorProperty("Kolor alarmu")]
        public Brush AlarmLimitBrush
        {
            get { return (Brush)GetValue(AlarmLimitBrushProperty); }
            set { SetValue(AlarmLimitBrushProperty, value); }
        }

        public static readonly DependencyProperty AlarmLimitBrushProperty =
            DependencyProperty.Register("AlarmLimitBrush", typeof(Brush), typeof(MeasureControl), new PropertyMetadata(null, AlarmLimitBrush_PropertyChanged));

        private static void AlarmLimitBrush_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is MeasureControl measureControl)
            {
                measureControl.UpdateLimitBrush();
            }
        }
        #endregion limit na alarm

        private void UpdateLimitBrush()
        {
            if (double.TryParse(Text?.ToString(), out double currentValue))
            {
                bool limitActive = false;


                if (bool.TryParse(WarrningActivity?.ToString(), out bool bWA) && bWA && double.TryParse(WarrningLimit?.ToString(), out double warrningValue) && currentValue >= warrningValue)
                {
                    InternalMeasurementBackgroundBrush = WarrningLimitBrush;
                    limitActive = true;
                }

                if (bool.TryParse(AlarmActivity?.ToString(), out bool bAA) && bAA && double.TryParse(AlarmLimit?.ToString(), out double alarmValue) && currentValue >= alarmValue)
                {
                    InternalMeasurementBackgroundBrush = AlarmLimitBrush;
                    limitActive = true;
                }


                if(!limitActive)
                    InternalMeasurementBackgroundBrush = Brushes.Transparent;
            }
            else
                InternalMeasurementBackgroundBrush = Brushes.Transparent;

        }

        private Brush _InternalMeasurementBackgroundBrush = Brushes.Transparent;
        public Brush InternalMeasurementBackgroundBrush
        {
            get { return _InternalMeasurementBackgroundBrush; }
            set { _InternalMeasurementBackgroundBrush = value; NotifyPropertyChanged("InternalMeasurementBackgroundBrush"); }
        }

        #region ClickCommand
        [EditorProperty("Zdarzenie na kliknięcie", "Zdarzenia")]
        public object ClickCommand
        {
            get { return (object)GetValue(ClickCommandProperty); }
            set { SetValue(ClickCommandProperty, value); }
        }

        public static readonly DependencyProperty ClickCommandProperty =
            DependencyProperty.Register("ClickCommand", typeof(object), typeof(CommonControlBase), new PropertyMetadata(ClickCommand_PropertyChanged));

        private static void ClickCommand_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is MeasureControl measureControl)
            {
                if (e.NewValue is ICommand command)
                {
                    measureControl.InternalClickCommand = command;
                }
                if (e.NewValue != null)
                    measureControl.Cursor = Cursors.Hand;
                else
                    measureControl.Cursor = Cursors.Arrow;

            }
        }

        private ICommand InternalClickCommand;

        [EditorProperty("Parametr zdarzenia na kliknięcie", "Zdarzenia")]
        public object ClickCommandParameter
        {
            get { return (object)GetValue(ClickCommandParameterProperty); }
            set { SetValue(ClickCommandParameterProperty, value); }
        }

        public static readonly DependencyProperty ClickCommandParameterProperty =
            DependencyProperty.Register("ClickCommandParameter", typeof(object), typeof(CommonControlBase));

        #region overrides
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            if (InternalClickCommand != null)
            {
                if (ClickCommandParameter != null)
                    InternalClickCommand.Execute(ClickCommandParameter);
                else
                    InternalClickCommand.Execute(this);
            }
        }
        #endregion overrides
        #endregion ClickCommand

        #region text properties
        [EditorProperty("Położenie tekstu")]
        public TextAlignment TextAlignment
        {
            get { return (TextAlignment)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }

        public static readonly DependencyProperty TextAlignmentProperty =
            DependencyProperty.Register("TextAlignment", typeof(TextAlignment), typeof(MeasureControl));

        #region Tekst
        [EditorProperty("Wartość")]
        public object Text
        {
            get { return (object)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(object), typeof(MeasureControl), new PropertyMetadata("TEXT", Text_PropertyChanged));

        private static void Text_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is MeasureControl measureControl)
            {
                if (!string.IsNullOrEmpty((string)e.NewValue) && e.NewValue.ToString().StartsWith("[") && e.NewValue.ToString().EndsWith("]"))
                    measureControl.TextString = "@";
                else
                {
                    measureControl.TextString = ValuesComparer.ProcessObjValue(e.NewValue);
                    measureControl.UpdateLimitBrush();
                }
            }
        }

        private object _TextString;
        public object TextString
        {
            get { return _TextString; }
            set { _TextString = value; NotifyPropertyChanged("TextString"); }
        }

        #endregion Tekst

        #region Opis
        [EditorProperty("Opis")]
        public object Description
        {
            get { return (object)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(object), typeof(MeasureControl), new PropertyMetadata(Description_PropertyChanged));

        private static void Description_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is MeasureControl measureControl)
            {
                measureControl.IsDescripctionVisible = string.IsNullOrEmpty((string)e.NewValue);

                if (!string.IsNullOrEmpty((string)e.NewValue) && e.NewValue.ToString().StartsWith("[") && e.NewValue.ToString().EndsWith("]"))
                    measureControl.DescriptionString = "@";
                else
                    measureControl.DescriptionString = e.NewValue;

            }
        }

        private object _DescriptionString = "TEXT";
        public object DescriptionString
        {
            get { return _DescriptionString; }
            set { _DescriptionString = value; NotifyPropertyChanged("DescriptionString"); }
        }
        #endregion Opis

        #region Unit
        [EditorProperty("Jednostka")]
        public object Unit
        {
            get { return (object)GetValue(UnitProperty); }
            set { SetValue(UnitProperty, value); }
        }

        public static readonly DependencyProperty UnitProperty =
            DependencyProperty.Register("Unit", typeof(object), typeof(MeasureControl), new PropertyMetadata(Unit_PropertyChanged));

        private static void Unit_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is MeasureControl measureControl)
            {
                measureControl.IsUnitVisible = string.IsNullOrEmpty((string)e.NewValue);

                if (!string.IsNullOrEmpty((string)e.NewValue) && e.NewValue.ToString().StartsWith("[") && e.NewValue.ToString().EndsWith("]"))
                    measureControl.UnitString = "[@]";
                else
                    measureControl.UnitString = e.NewValue;


            }

        }

        private object _UnitString;
        public object UnitString
        {
            get { return _UnitString; }
            set { _UnitString = value; NotifyPropertyChanged("UnitString"); }
        }
        #endregion Unit

        #endregion

        #region zoom props
        [EditorProperty("Specyficzny element przybliżenia")]
        public bool SpecificPlaceElement
        {
            get { return (bool)GetValue(SpecificPlaceElementProperty); }
            set { SetValue(SpecificPlaceElementProperty, value); }
        }

        public static readonly DependencyProperty SpecificPlaceElementProperty =
            DependencyProperty.Register("SpecificPlaceElement", typeof(bool), typeof(MeasureControl), new PropertyMetadata(false, SpecificPlaceElement_PropertyChanged));

        private static void SpecificPlaceElement_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MeasureControl measureControl = (MeasureControl)d;
            if (bool.TryParse(e.NewValue?.ToString(), out bool result) && result && measureControl?.Parent is SchemeCanvas && ((SchemeCanvas)measureControl.Parent).IsEditMode == false)
            {
                measureControl.Visibility = Visibility.Collapsed;
            }
        }

        public bool HideElement
        {
            get { return (bool)GetValue(HideElementProperty); }
            set { SetValue(HideElementProperty, value); }
        }

        public static readonly DependencyProperty HideElementProperty =
            DependencyProperty.Register("HideElement", typeof(bool), typeof(MeasureControl), new PropertyMetadata(false, HideElement_PropertyChanged));

        private static void HideElement_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MeasureControl measureControl = (MeasureControl)d;
            if (bool.TryParse(e.NewValue?.ToString(), out bool result) && result && measureControl.SpecificPlaceElement == true)
            {
                measureControl.Visibility = Visibility.Collapsed;
            }
            else
                measureControl.Visibility = Visibility.Visible;
        }


        #endregion

        #region ScrewTransform
        [EditorProperty("Kąt obrotu")]
        public double Angle
        {
            get { return (double)GetValue(AngleProperty); }
            set { SetValue(AngleProperty, value); }
        }

        public static readonly DependencyProperty AngleProperty =
            DependencyProperty.Register("Angle", typeof(double), typeof(MeasureControl), new PropertyMetadata(0.0, Angle_PropertyChanged));

        private static void Angle_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MeasureControl measureControl = (MeasureControl)d;
            if (double.TryParse(e.NewValue?.ToString(), out double result))
                measureControl.RenderTransform = new RotateTransform(result);
        }
        #endregion

        #region Description properties
        [EditorProperty("Tło opisu")]
        public Brush DescriptionBackground
        {
            get { return (Brush)GetValue(DescriptionBackgroundProperty); }
            set { SetValue(DescriptionBackgroundProperty, value); }
        }
        public static readonly DependencyProperty DescriptionBackgroundProperty =
            DependencyProperty.Register("DescriptionBackground", typeof(Brush), typeof(MeasureControl));


        #endregion Description properties

        [EditorProperty("Zaokrąglenia")]
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(MeasureControl));

        [EditorProperty("Orientacja")]
        public Orientation Orientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("Orientation", typeof(Orientation), typeof(MeasureControl));

        #region DashArray
        [EditorProperty("Kreskowana ramka")]
        public bool IsDasched
        {
            get { return (bool)GetValue(IsDaschedProperty); }
            set { SetValue(IsDaschedProperty, value); }
        }

        public static readonly DependencyProperty IsDaschedProperty =
            DependencyProperty.Register("IsDasched", typeof(bool), typeof(MeasureControl), new PropertyMetadata(IsDasched_PropertyChanged));

        private static void IsDasched_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is MeasureControl measureControl)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolValue) && boolValue)
                    measureControl.BorderStrokeDashArray = new DoubleCollection() { 5, 5 };
                else
                    measureControl.BorderStrokeDashArray = new DoubleCollection();

            }
        }

        private DoubleCollection _BorderStrokeDashArray = new DoubleCollection();
        public DoubleCollection BorderStrokeDashArray
        {
            get { return _BorderStrokeDashArray; }
            set { _BorderStrokeDashArray = value; NotifyPropertyChanged("BorderStrokeDashArray"); }
        }

        #endregion DashArray

        #endregion

        #region Public fields
        private bool _IsDescripctionVisible;
        public bool IsDescripctionVisible { get { return _IsDescripctionVisible; } set { _IsDescripctionVisible = value; NotifyPropertyChanged("IsDescripctionVisible"); } }

        private bool _IsUnitVisible;
        public bool IsUnitVisible { get { return _IsUnitVisible; } set { _IsUnitVisible = value; NotifyPropertyChanged("IsUnitVisible"); } }

        #endregion

        protected override void DrawControl(DrawingContext drawingContext)
        {
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.UpdateLimitBrush();
        }

        public MeasureControl()
        {
            this.Foreground = Settings.DefaultLineColor;
        }

        static MeasureControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MeasureControl), new FrameworkPropertyMetadata(typeof(MeasureControl)));
        }

    }
}