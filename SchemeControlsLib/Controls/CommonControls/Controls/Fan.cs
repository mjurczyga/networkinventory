﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.CommonControls
{
    [EditorType(SchemeTypes.Common, true, "Wiatrak")]
    public sealed class Fan : CommonControlBase, IMovableElement, IMovableSelectionElement, IBindingControl
    {
        #region IsSelectionMoving
        private bool _IsSelectionMoving;
        public bool IsSelectionMoving
        {
            get { return _IsSelectionMoving; }
            set { _IsSelectionMoving = value; NotifyPropertyChanged("IsSelectionMoving"); }
        }
        #endregion IsSelectionMoving

        #region SteeringControlVar TRYB STEROWANIA
        [EditorProperty("Zmienna trybu sterowania")]
        public object SteeringControlVar
        {
            get { return (object)GetValue(SteeringControlVarProperty); }
            set { SetValue(SteeringControlVarProperty, value); }
        }

        public static readonly DependencyProperty SteeringControlVarProperty =
            DependencyProperty.Register("SteeringControlVar", typeof(object), typeof(Fan), new PropertyMetadata(null, SteeringControlVar_PropertyChanged));

        private static void SteeringControlVar_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Fan fan)
            {
                fan.SteeringControlChanged(e.NewValue);
            }
        }

        private void SteeringControlChanged(object newValue)
        {
            if (int.TryParse(newValue?.ToString(), out int intValue))
            {
                if (intValue == 0)
                {
                    HandMode_IconVisibility = Visibility.Collapsed;
                    AutomaticMode_IconVisibility = Visibility.Collapsed;
                    SetAsideMode_IconVisibility = Visibility.Collapsed;
                }

                if (intValue == 1)
                {
                    HandMode_IconVisibility = Visibility.Visible;
                    AutomaticMode_IconVisibility = Visibility.Collapsed;
                    SetAsideMode_IconVisibility = Visibility.Collapsed;
                }

                if (intValue == 2)
                {
                    HandMode_IconVisibility = Visibility.Collapsed;
                    AutomaticMode_IconVisibility = Visibility.Visible;
                    SetAsideMode_IconVisibility = Visibility.Collapsed;
                }

            }
        }

        private Visibility _HandMode_IconVisibility = Visibility.Collapsed;
        public Visibility HandMode_IconVisibility
        {
            get { return _HandMode_IconVisibility; }
            set { _HandMode_IconVisibility = value; NotifyPropertyChanged("HandMode_IconVisibility"); }
        }

        private Visibility _AutomaticMode_IconVisibility = Visibility.Collapsed;
        public Visibility AutomaticMode_IconVisibility
        {
            get { return _AutomaticMode_IconVisibility; }
            set { _AutomaticMode_IconVisibility = value; NotifyPropertyChanged("AutomaticMode_IconVisibility"); }
        }

        private Visibility _SetAsideMode_IconVisibility = Visibility.Collapsed;
        public Visibility SetAsideMode_IconVisibility
        {
            get { return _SetAsideMode_IconVisibility; }
            set { _SetAsideMode_IconVisibility = value; NotifyPropertyChanged("SetAsideMode_IconVisibility"); }
        }

        #endregion SteeringControlVar


        #region dependency properties
        [EditorProperty("Wartość na rotacje")]
        public object Value
        {
            get { return (object)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(object), typeof(Fan), new PropertyMetadata(null, Value_PropertyChanged));

        private static void Value_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Fan fan = (Fan)d;
            if (bool.TryParse(e.NewValue?.ToString(), out bool bValue))
            {
                fan.IsRotating = bValue;
            }
            else if (int.TryParse(e.NewValue?.ToString(), out int iValue))
            {
                fan.IsRotating = iValue > 1 ? true : false;
            }
        }

        [EditorProperty("Rotacja?")]
        public bool IsRotating
        {
            get { return (bool)GetValue(IsRotatingProperty); }
            set { SetValue(IsRotatingProperty, value); }
        }

        public static readonly DependencyProperty IsRotatingProperty =
            DependencyProperty.Register("IsRotating", typeof(bool), typeof(Fan));

        [EditorProperty("Kolor wiatraka")]
        public Brush FanBrush
        {
            get { return (Brush)GetValue(FanBrushProperty); }
            set { SetValue(FanBrushProperty, value); }
        }

        public static readonly DependencyProperty FanBrushProperty =
            DependencyProperty.Register("FanBrush", typeof(Brush), typeof(Fan), new PropertyMetadata(Brushes.White));

        #endregion

        protected override void DrawControl(DrawingContext drawingContext)
        {
            
        }

        public Fan()
        {
            this.Width = 50;
            this.Height = 50;
        }

        static Fan()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Fan), new FrameworkPropertyMetadata(typeof(Fan)));
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            // wymuszenie kręcenia po załadowaniu
            IsRotating = !IsRotating;
            IsRotating = !IsRotating;
        }
    }
}
