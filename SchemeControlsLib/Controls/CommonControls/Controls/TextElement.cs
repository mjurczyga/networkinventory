﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.Interfaces.CoreElementsInterfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.Helpers;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.CommonControls
{
    [EditorType(SchemeTypes.Common, true, "Tekst")]
    public sealed class TextElement : CommonControlBase, IMovableElement, IMovableSelectionElement, IBindingControl, ISpecificPlaceElement, IHideableElement
    {
        #region IsbindedValue
        private bool _IsBindedValue;
        public bool IsBindedValue
        {
            get { return _IsBindedValue; }
            set { _IsBindedValue = value; SetelementsVisibility(value); NotifyPropertyChanged("IsBindedValue"); }
        }

        private void SetelementsVisibility(bool value)
        {
            if(IsEditMode && value)
            {
                ControlTextVisibilit = Visibility.Collapsed;
                BindedTextVisibilit = Visibility.Visible;
            }
            else
            {
                ControlTextVisibilit = Visibility.Visible;
                BindedTextVisibilit = Visibility.Collapsed;
            }
        }

        private Visibility _ControlTextVisibilit = Visibility.Visible;
        public Visibility ControlTextVisibilit
        {
            get { return _ControlTextVisibilit; }
            set { _ControlTextVisibilit = value; NotifyPropertyChanged("ControlTextVisibilit"); }
        }

        private Visibility _BindedTextVisibilit = Visibility.Collapsed;
        public Visibility BindedTextVisibilit
        {
            get { return _BindedTextVisibilit; }
            set { _BindedTextVisibilit = value; NotifyPropertyChanged("BindedTextVisibilit"); }
        }
        #endregion IsbindedValue

        #region IsSelectionMoving
        private bool _IsSelectionMoving;
        public bool IsSelectionMoving
        {
            get { return _IsSelectionMoving; }
            set { _IsSelectionMoving = value; NotifyPropertyChanged("IsSelectionMoving"); }
        }
        #endregion IsSelectionMoving

        #region Ingerited properties
        [EditorProperty("Kolor tła")]
        public new Brush Background { get { return base.Background; } set { base.Background = value; } }

        [EditorProperty("Rozmier tekstu")]
        public new double FontSize { get { return base.FontSize; } set { base.FontSize = value; } }

        [EditorProperty("Styl tekstu")]
        public new FontWeight FontWeight { get { return base.FontWeight; } set { base.FontWeight = value; } }

        [EditorProperty("Kolor tekstu")]
        public new Brush Foreground { get { return base.Foreground; } set { base.Foreground = value; } }

        [EditorProperty("Przeźroczystość")]
        public new double Opacity { get { return base.Opacity; } set { base.Opacity = value; } }

        [EditorProperty("Padding")]
        public new Thickness Padding { get { return base.Padding; } set { base.Padding = value; } }

        [EditorProperty("Kolor ramki")]
        public new Brush BorderBrush { get { return base.BorderBrush; } set { base.BorderBrush = value; } }

        [EditorProperty("Grubość ramki")]
        public new Thickness BorderThickness { get { return base.BorderThickness; } set { base.BorderThickness = value; } }
        #endregion

        #region dependency properties
        #region text properties
        [EditorProperty("Położenie tekstu")]
        public TextAlignment TextAlignment
        {
            get { return (TextAlignment)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }

        public static readonly DependencyProperty TextAlignmentProperty =
            DependencyProperty.Register("TextAlignment", typeof(TextAlignment), typeof(TextElement));

        #region Tekst
        [EditorProperty("Tekst")]
        public object Text
        {
            get { return (object)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(object), typeof(TextElement), new PropertyMetadata("TEXT", Text_PropertyChanged));

        private static void Text_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is TextElement textElement)
            {
                if (!string.IsNullOrEmpty(e.NewValue?.ToString()) && e.NewValue.ToString().StartsWith("[") && e.NewValue.ToString().EndsWith("]"))
                    textElement.IsBindedValue = true;
                else
                {
                    //if (string.IsNullOrEmpty((string)e.NewValue))
                        //textElement.Text = "TEXT";

                    textElement.IsBindedValue = false;
                }
            }
        }
        #endregion Tekst

        #region Opis
        [EditorProperty("Opis")]
        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(TextElement), new PropertyMetadata(Description_PropertyChanged));

        private static void Description_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is TextElement textElement)
            {
                textElement.IsDescripctionVisible = string.IsNullOrEmpty((string)e.NewValue);

                if (!string.IsNullOrEmpty(e.NewValue?.ToString()) && e.NewValue.ToString().StartsWith("[") && e.NewValue.ToString().EndsWith("]"))
                    textElement.IsBindedValue = true;
                else
                    textElement.IsBindedValue = false;

            }
        }
        #endregion Opis

        #region Unit
        [EditorProperty("Jednostka")]
        public string Unit
        {
            get { return (string)GetValue(UnitProperty); }
            set { SetValue(UnitProperty, value); }
        }

        public static readonly DependencyProperty UnitProperty =
            DependencyProperty.Register("Unit", typeof(string), typeof(TextElement), new PropertyMetadata(Unit_PropertyChanged));

        private static void Unit_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is TextElement textElement)
            {
                textElement.IsUnitVisible = string.IsNullOrEmpty(e.NewValue?.ToString());

                if (!string.IsNullOrEmpty(e.NewValue?.ToString()) && e.NewValue.ToString().StartsWith("[") && e.NewValue.ToString().EndsWith("]"))
                    textElement.IsBindedValue = true;
                else
                    textElement.IsBindedValue = false;
                

            }

        }
        #endregion Unit

        #endregion

        #region zoom props
        [EditorProperty("Specyficzny element przybliżenia")]
        public bool SpecificPlaceElement
        {
            get { return (bool)GetValue(SpecificPlaceElementProperty); }
            set { SetValue(SpecificPlaceElementProperty, value); }
        }

        public static readonly DependencyProperty SpecificPlaceElementProperty =
            DependencyProperty.Register("SpecificPlaceElement", typeof(bool), typeof(TextElement), new PropertyMetadata(false, SpecificPlaceElement_PropertyChanged));

        private static void SpecificPlaceElement_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextElement textElement = (TextElement)d;
            if(bool.TryParse(e.NewValue?.ToString(), out bool result) && result && textElement?.Parent is SchemeCanvas && ((SchemeCanvas)textElement.Parent).IsEditMode == false)
            {
                textElement.Visibility = Visibility.Collapsed;
            }
        }

        public bool HideElement
        {
            get { return (bool)GetValue(HideElementProperty); }
            set { SetValue(HideElementProperty, value); }
        }

        public static readonly DependencyProperty HideElementProperty =
            DependencyProperty.Register("HideElement", typeof(bool), typeof(TextElement), new PropertyMetadata(false, HideElement_PropertyChanged));

        private static void HideElement_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextElement textElement = (TextElement)d;
            if (bool.TryParse(e.NewValue?.ToString(), out bool result) && result && textElement.SpecificPlaceElement == true)
            {
                textElement.Visibility = Visibility.Collapsed;
            }
            else
                textElement.Visibility = Visibility.Visible;
        }


        #endregion

        #region ScrewTransform
        [EditorProperty("Kąt obrotu")]
        public double Angle
        {
            get { return (double)GetValue(AngleProperty); }
            set { SetValue(AngleProperty, value); }
        }

        public static readonly DependencyProperty AngleProperty =
            DependencyProperty.Register("Angle", typeof(double), typeof(TextElement), new PropertyMetadata(0.0, Angle_PropertyChanged));

        private static void Angle_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextElement textElement = (TextElement)d;
            if (double.TryParse(e.NewValue?.ToString(), out double result))
                textElement.RenderTransform = new RotateTransform(result);
        }
        #endregion

        [EditorProperty("Zaokrąglenia")]
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(TextElement));

        [EditorProperty("Orientacja")]
        public Orientation Orientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("Orientation", typeof(Orientation), typeof(TextElement));

        #region DashArray
        [EditorProperty("Kreskowana ramka")]
        public bool IsDasched
        {
            get { return (bool)GetValue(IsDaschedProperty); }
            set { SetValue(IsDaschedProperty, value); }
        }

        public static readonly DependencyProperty IsDaschedProperty =
            DependencyProperty.Register("IsDasched", typeof(bool), typeof(TextElement), new PropertyMetadata(IsDasched_PropertyChanged));

        private static void IsDasched_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is TextElement textElement)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolValue) && boolValue)
                    textElement.BorderStrokeDashArray = new DoubleCollection() { 5, 5 };
                else
                    textElement.BorderStrokeDashArray = new DoubleCollection();

            }
        }

        private DoubleCollection _BorderStrokeDashArray = new DoubleCollection();
        public DoubleCollection BorderStrokeDashArray
        {
            get { return _BorderStrokeDashArray; }
            set { _BorderStrokeDashArray = value; NotifyPropertyChanged("BorderStrokeDashArray"); }
        }

        #endregion DashArray

        #endregion

        #region Public fields
        private bool _IsDescripctionVisible;
        public bool IsDescripctionVisible { get { return _IsDescripctionVisible; } set { _IsDescripctionVisible = value; NotifyPropertyChanged("IsDescripctionVisible"); } }

        private bool _IsUnitVisible;
        public bool IsUnitVisible { get { return _IsUnitVisible; } set { _IsUnitVisible = value; NotifyPropertyChanged("IsUnitVisible"); } }

        #endregion

        protected override void DrawControl(DrawingContext drawingContext)
        {
        }

        public TextElement()
        {
            this.Foreground = Settings.DefaultLineColor;
        }

        static TextElement()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TextElement), new FrameworkPropertyMetadata(typeof(TextElement)));
        }

    }
}
