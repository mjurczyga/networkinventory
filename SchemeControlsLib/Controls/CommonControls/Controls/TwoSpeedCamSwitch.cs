﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.CommonControls
{
    [EditorType(EnumTypes.SchemeTypes.Common, true, "Jaworek")]
    public class TwoSpeedCamSwitch : CommonControlBase, IMovableElement, IMovableSelectionElement, IBindingControl
    {
        #region RadioButton 0
        public bool IsCheckedButton0Status
        {
            get { return (bool)GetValue(IsCheckedButton0StatusProperty); }
            set { SetValue(IsCheckedButton0StatusProperty, value); }
        }

        public static readonly DependencyProperty IsCheckedButton0StatusProperty =
            DependencyProperty.Register("IsCheckedButton0Status", typeof(bool), typeof(TwoSpeedCamSwitch), new FrameworkPropertyMetadata(propertyChangedCallback: OnIsCheckedButton0Status_PropertyChanged));

        private static void OnIsCheckedButton0Status_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d != null)
            {
                TwoSpeedCamSwitch control = d as TwoSpeedCamSwitch;
                if (control.IsCheckedButton0Status)
                {
                    control.SwitcherState = 0;
                    control.GraphicVBAngle = 0;
                }
            }
        }

        public bool IsChecked0
        {
            get { return (bool)GetValue(IsChecked0Property); }
            set { SetValue(IsChecked0Property, value); }
        }

        public static readonly DependencyProperty IsChecked0Property =
            DependencyProperty.Register("IsChecked0", typeof(bool), typeof(TwoSpeedCamSwitch), new FrameworkPropertyMetadata(propertyChangedCallback: OnIsChecked0_PropertyChanged));

        private static void OnIsChecked0_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d != null)
            {
                TwoSpeedCamSwitch control = d as TwoSpeedCamSwitch;
                if (control.IsChecked0)
                {
                    control.SwitchState = 0;
                    control.Button0Command?.Execute(control);
                }
            }
        }

        public string Button0Text
        {
            get { return (string)GetValue(Button0TextProperty); }
            set { SetValue(Button0TextProperty, value); }
        }

        public static readonly DependencyProperty Button0TextProperty =
            DependencyProperty.Register("Button0Text", typeof(string), typeof(TwoSpeedCamSwitch), new PropertyMetadata("Opis"));

        public ICommand Button0Command
        {
            get { return (ICommand)GetValue(Button0CommandProperty); }
            set { SetValue(Button0CommandProperty, value); }
        }

        public static readonly DependencyProperty Button0CommandProperty =
            DependencyProperty.Register("Button0Command", typeof(ICommand), typeof(TwoSpeedCamSwitch), new PropertyMetadata(null));

        public string Button0ToolTip
        {
            get { return (string)GetValue(Button0ToolTipProperty); }
            set { SetValue(Button0ToolTipProperty, value); }
        }

        public static readonly DependencyProperty Button0ToolTipProperty =
            DependencyProperty.Register("Button0ToolTip", typeof(string), typeof(TwoSpeedCamSwitch));
        #endregion

        #region RadioButton 1

        public bool IsCheckedButton1Status
        {
            get { return (bool)GetValue(IsCheckedButton1StatusProperty); }
            set { SetValue(IsCheckedButton1StatusProperty, value); }
        }

        public static readonly DependencyProperty IsCheckedButton1StatusProperty =
            DependencyProperty.Register("IsCheckedButton1Status", typeof(bool), typeof(TwoSpeedCamSwitch), new FrameworkPropertyMetadata(propertyChangedCallback: OnIsCheckedButton1Status_PropertyChanged));

        private static void OnIsCheckedButton1Status_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d != null)
            {
                TwoSpeedCamSwitch control = d as TwoSpeedCamSwitch;
                if (control.IsCheckedButton1Status)
                {
                    control.SwitcherState = 1;
                    control.GraphicVBAngle = -65;
                }
            }
        }

        public bool IsChecked1
        {
            get { return (bool)GetValue(IsChecked1Property); }
            set { SetValue(IsChecked1Property, value); }
        }

        public static readonly DependencyProperty IsChecked1Property =
            DependencyProperty.Register("IsChecked1", typeof(bool), typeof(TwoSpeedCamSwitch), new FrameworkPropertyMetadata(propertyChangedCallback: OnIsChecked1_PropertyChanged));

        private static void OnIsChecked1_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d != null)
            {
                TwoSpeedCamSwitch control = d as TwoSpeedCamSwitch;
                if (control.IsChecked1)
                {
                    control.SwitchState = 1;
                    control.Button1Command?.Execute(control);
                }
            }
        }

        public string Button1Text
        {
            get { return (string)GetValue(Button1TextProperty); }
            set { SetValue(Button1TextProperty, value); }
        }

        public static readonly DependencyProperty Button1TextProperty =
            DependencyProperty.Register("Button1Text", typeof(string), typeof(TwoSpeedCamSwitch), new PropertyMetadata("Opis"));

        public ICommand Button1Command
        {
            get { return (ICommand)GetValue(Button1CommandProperty); }
            set { SetValue(Button1CommandProperty, value); }
        }

        public static readonly DependencyProperty Button1CommandProperty =
            DependencyProperty.Register("Button1Command", typeof(ICommand), typeof(TwoSpeedCamSwitch), new PropertyMetadata(null));

        public string Button1ToolTip
        {
            get { return (string)GetValue(Button1ToolTipProperty); }
            set { SetValue(Button1ToolTipProperty, value); }
        }

        public static readonly DependencyProperty Button1ToolTipProperty =
            DependencyProperty.Register("Button1ToolTip", typeof(string), typeof(TwoSpeedCamSwitch));
        #endregion

        #region RadioButton 2
        public bool IsCheckedButton2Status
        {
            get { return (bool)GetValue(IsCheckedButton2StatusProperty); }
            set { SetValue(IsCheckedButton2StatusProperty, value); }
        }

        public static readonly DependencyProperty IsCheckedButton2StatusProperty =
            DependencyProperty.Register("IsCheckedButton2Status", typeof(bool), typeof(TwoSpeedCamSwitch), new FrameworkPropertyMetadata(propertyChangedCallback: OnIsCheckedButton2Status_PropertyChanged));

        private static void OnIsCheckedButton2Status_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d != null)
            {
                TwoSpeedCamSwitch control = d as TwoSpeedCamSwitch;
                if (control.IsCheckedButton2Status)
                {
                    control.SwitcherState = 2;
                    control.GraphicVBAngle = 65;
                }
            }
        }

        public bool IsChecked2
        {
            get { return (bool)GetValue(IsChecked2Property); }
            set { SetValue(IsChecked2Property, value); }
        }

        public static readonly DependencyProperty IsChecked2Property =
            DependencyProperty.Register("IsChecked2", typeof(bool), typeof(TwoSpeedCamSwitch), new FrameworkPropertyMetadata(propertyChangedCallback: OnIsChecked2_PropertyChanged));

        private static void OnIsChecked2_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d != null)
            {
                TwoSpeedCamSwitch control = d as TwoSpeedCamSwitch;
                if (control.IsChecked2)
                {
                    control.SwitchState = 2;
                    control.Button2Command?.Execute(control);
                }
            }
        }

        public string Button2Text
        {
            get { return (string)GetValue(Button2TextProperty); }
            set { SetValue(Button2TextProperty, value); }
        }

        public static readonly DependencyProperty Button2TextProperty =
            DependencyProperty.Register("Button2Text", typeof(string), typeof(TwoSpeedCamSwitch), new PropertyMetadata("Opis"));

        public ICommand Button2Command
        {
            get { return (ICommand)GetValue(Button2CommandProperty); }
            set { SetValue(Button2CommandProperty, value); }
        }

        public static readonly DependencyProperty Button2CommandProperty =
            DependencyProperty.Register("Button2Command", typeof(ICommand), typeof(TwoSpeedCamSwitch), new PropertyMetadata(null));

        public string Button2ToolTip
        {
            get { return (string)GetValue(Button2ToolTipProperty); }
            set { SetValue(Button2ToolTipProperty, value); }
        }

        public static readonly DependencyProperty Button2ToolTipProperty =
            DependencyProperty.Register("Button2ToolTip", typeof(string), typeof(TwoSpeedCamSwitch));
        #endregion

        private bool _PopupIsOpen = false;
        public bool PopupIsOpen { get { return _PopupIsOpen; } set { _PopupIsOpen = value; NotifyPropertyChanged(); } }

        private int _SwitcherState = 0;
        public int SwitcherState { get { return _SwitcherState; } set { _SwitcherState = value; NotifyPropertyChanged(); } }

        public int SwitchState
        {
            get { return (int)GetValue(SwitchStateProperty); }
            set { SetValue(SwitchStateProperty, value); }
        }

        public static readonly DependencyProperty SwitchStateProperty =
            DependencyProperty.Register("SwitchState", typeof(int), typeof(TwoSpeedCamSwitch), new PropertyMetadata(0));

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(TwoSpeedCamSwitch), new FrameworkPropertyMetadata(null, propertyChangedCallback: OnDescription_PropertyChnaged));

        private static void OnDescription_PropertyChnaged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d != null)
            {
                var obj = d as TwoSpeedCamSwitch;
                if (!string.IsNullOrEmpty(obj.Description))
                    obj.DescriptionVisibility = Visibility.Visible;
                else
                    obj.DescriptionVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _DescriptionVisibility = Visibility.Collapsed;
        public Visibility DescriptionVisibility
        {
            get { return _DescriptionVisibility; }
            set { _DescriptionVisibility = value; NotifyPropertyChanged(); }
        }


        private double _GraphicVBAngle;
        public double GraphicVBAngle { get { return _GraphicVBAngle; } set { _GraphicVBAngle = value; NotifyPropertyChanged(); } }

        private void UpdateSwitcherState()
        {
            if (IsChecked0)
                SwitcherState = 0;
            else if (IsChecked1)
                SwitcherState = 1;
            else if (IsChecked2)
                SwitcherState = 2;
            else
                SwitcherState = 9;
        }

        #region IsSelectionMoving
        private bool _IsSelectionMoving;
        public bool IsSelectionMoving
        {
            get { return _IsSelectionMoving; }
            set { _IsSelectionMoving = value; NotifyPropertyChanged("IsSelectionMoving"); }
        }
        #endregion IsSelectionMoving

        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            PopupIsOpen = true;
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            PopupIsOpen = false;
        }

        private void MoveTo_2_Completed(object sender, EventArgs e)
        {
            GraphicVBAngle = 65;

        }

        protected override void DrawControl(DrawingContext drawingContext)
        {
        }

        static TwoSpeedCamSwitch()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TwoSpeedCamSwitch), new FrameworkPropertyMetadata(typeof(TwoSpeedCamSwitch)));
        }
    }
}
