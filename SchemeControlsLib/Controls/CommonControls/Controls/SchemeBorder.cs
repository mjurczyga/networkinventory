﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.CommonControls
{
    [EditorType(SchemeTypes.Common, true, "Obramowanie")]
    public class SchemeBorder : CommonControlBase, IMovableElement, IMovableSelectionElement, IBindingControl
    {
        #region IsSelectionMoving
        private bool _IsSelectionMoving;
        public bool IsSelectionMoving
        {
            get { return _IsSelectionMoving; }
            set { _IsSelectionMoving = value; NotifyPropertyChanged("IsSelectionMoving"); }
        }
        #endregion IsSelectionMoving

        #region DependencyProperties
        #region Text properties
        [EditorProperty("Tekst")]
        public object  Text
        {
            get { return (object )GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(object ), typeof(CommonControlBase));

        #region property wrappers
        [EditorProperty("Rozmiar tekstu")]
        public new double FontSize { get { return base.FontSize; } set { base.FontSize = value; } }

        [EditorProperty("Kolor tekstu")]
        public new Brush Foreground { get { return base.Foreground; } set { base.Foreground = value; } }

        [EditorProperty("Typ tekstu")]
        public new FontWeight FontWeight { get { return base.FontWeight; } set { base.FontWeight = value; } }
        #endregion property wrappers
        #endregion Text properties


        [EditorProperty("Zaokrąglenia")]
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(SchemeBorder));

        #region VisibilityVar
        [EditorProperty("Zmienna na widoczność kontrolki")]
        public object VisibilityVar
        {
            get { return (object)GetValue(VisibilityVarProperty); }
            set { SetValue(VisibilityVarProperty, value); }
        }

        public static readonly DependencyProperty VisibilityVarProperty =
            DependencyProperty.Register("VisibilityVar", typeof(object), typeof(SchemeBorder), new PropertyMetadata(VisibilityVar_PropertyChanged));

        private static void VisibilityVar_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is SchemeBorder schemeBorder)
            {
                schemeBorder.SetVisibility(e.NewValue, schemeBorder.VisibilityVarComparer);
            }
        }

        [EditorProperty("Wartość przy której nie widoczne", toolTip: "Domyślnie 'true'. Można oddzielać ';'.")]
        public object VisibilityVarComparer
        {
            get { return (object)GetValue(VisibilityVarComparerProperty); }
            set { SetValue(VisibilityVarComparerProperty, value); }
        }

        public static readonly DependencyProperty VisibilityVarComparerProperty =
            DependencyProperty.Register("VisibilityVarComparer", typeof(object), typeof(SchemeBorder), new PropertyMetadata(VisibilityVarComparer_PropertyChanged));

        private static void VisibilityVarComparer_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SchemeBorder schemeBorder)
            {
                schemeBorder.SetVisibility(schemeBorder.VisibilityVar, e.NewValue);
            }
        }

        private void SetVisibility(object obj, object objComparer = null)
        {
            if (obj != null)
            {
                if (objComparer == null)
                {
                    if (bool.TryParse(obj?.ToString(), out bool boolObj))
                    {
                        if (boolObj)
                            Visibility = Visibility.Visible;
                        else
                            Visibility = Visibility.Collapsed;
                    }
                    else
                        Visibility = Visibility.Visible;
                }
                else
                {
                    if (obj.ToString().Contains('[') || obj.ToString().Contains(']'))
                        Visibility = Visibility.Visible;
                    else
                    {
                        var splitted = objComparer.ToString().Split(';');
                        if (splitted.Any(x => x == obj.ToString()))
                            Visibility = Visibility.Collapsed;
                        else
                            Visibility = Visibility.Visible;
                    }
                }
            }
            else
                Visibility = Visibility.Visible;

        }

        #endregion VisibilityVar



        #region property wrappers
        [EditorProperty("Kolor tła")]
        public new Brush Background { get { return base.Background; } set { base.Background = value; } }

        [EditorProperty("Kolor ramki")]
        public new Brush BorderBrush { get { return base.BorderBrush; } set { base.BorderBrush = value; } }

        [EditorProperty("Grubość obramowania")]
        public new Thickness BorderThickness { get { return base.BorderThickness; } set { base.BorderThickness = value; } }

        [EditorProperty("Przeźroczystość")]
        public new double Opacity { get { return base.Opacity; } set { base.Opacity = value; } }
        #endregion property wrappers
        #endregion DependencyProperties

        protected override void DrawControl(DrawingContext drawingContext)
        {

        }

        public SchemeBorder()
        {
            this.Width = 50;
            this.Height = 50;
            BorderBrush = Brushes.White;
            Background = Brushes.Gray;
        }

        static SchemeBorder()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SchemeBorder), new FrameworkPropertyMetadata(typeof(SchemeBorder)));
        }
    }
}
