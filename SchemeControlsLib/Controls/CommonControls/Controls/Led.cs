﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.Interfaces.CoreElementsInterfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.CommonControls
{
    [EditorType(SchemeTypes.Common, true, "Dioda led")]
    public sealed class Led : CommonControlBase, IMovableSelectionElement, IMovableElement, IBindingControl, ISpecificPlaceElement, IHideableElement
    {
        #region Ingerited properties
        [EditorProperty("Kolor tła")]
        public new Brush Background { get { return base.Background; } set { base.Background = value; } }

        [EditorProperty("Przeźroczystość")]
        public new double Opacity { get { return base.Opacity; } set { base.Opacity = value; } }

        [EditorProperty("Kolor ramki")]
        public new Brush BorderBrush { get { return base.BorderBrush; } set { base.BorderBrush = value; } }

        [EditorProperty("Grubość ramki")]
        public new Thickness BorderThickness { get { return base.BorderThickness; } set { base.BorderThickness = value; } }
        #endregion

        #region dependency properties
        #region Led props
        [EditorProperty("Zmienna stanu leda")]
        public object LedState
        {
            get { return (object)GetValue(LedStateProperty); }
            set { SetValue(LedStateProperty, value); }
        }

        public static readonly DependencyProperty LedStateProperty =
            DependencyProperty.Register("LedState", typeof(object), typeof(Led), new PropertyMetadata(false, LedState_PropertyChanged));

        private static void LedState_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is Led led)
            {
                led.SetBackground(e.NewValue);
            }
        }

        #region led Brush
        [EditorProperty("Kolor włączonej")]
        public Brush LedOnBrush
        {
            get { return (Brush)GetValue(LedOnBrushProperty); }
            set { SetValue(LedOnBrushProperty, value); }
        }

        public static readonly DependencyProperty LedOnBrushProperty =
            DependencyProperty.Register("LedOnBrush", typeof(Brush), typeof(Led), new PropertyMetadata(Brushes.Green, LedBrush_PropertyChanged));

        [EditorProperty("Kolor wyłączonej")]
        public Brush LedOffBrush
        {
            get { return (Brush)GetValue(LedOffBrushProperty); }
            set { SetValue(LedOffBrushProperty, value); }
        }

        public static readonly DependencyProperty LedOffBrushProperty =
            DependencyProperty.Register("LedOffBrush", typeof(Brush), typeof(Led), new PropertyMetadata(Brushes.Red, LedBrush_PropertyChanged));

        private static void LedBrush_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Led led)
            {
                led.SetBackground(led.LedState);
            }
        }
        #endregion led Brush
        [EditorProperty("Zaokrąglenia")]
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(Led));

        [EditorProperty("Opis (tool tip)")]
        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(Led), new PropertyMetadata(Description_PropertyChanged));

        private static void Description_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Led led = (Led)d;
            led.IsDescripctionVisible = string.IsNullOrEmpty((string)e.NewValue);
        }
        #endregion

        #region zoom props
        [EditorProperty("Specyficzny element przybliżenia")]
        public bool SpecificPlaceElement
        {
            get { return (bool)GetValue(SpecificPlaceElementProperty); }
            set { SetValue(SpecificPlaceElementProperty, value); }
        }

        public static readonly DependencyProperty SpecificPlaceElementProperty =
            DependencyProperty.Register("SpecificPlaceElement", typeof(bool), typeof(Led), new PropertyMetadata(false, SpecificPlaceElement_PropertyChanged));

        private static void SpecificPlaceElement_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Led led = (Led)d;
            if (bool.TryParse(e.NewValue?.ToString(), out bool result) && result && led?.Parent is SchemeCanvas && ((SchemeCanvas)led.Parent).IsEditMode == false)
            {
                led.Visibility = Visibility.Collapsed;
            }
        }

        public bool HideElement
        {
            get { return (bool)GetValue(HideElementProperty); }
            set { SetValue(HideElementProperty, value); }
        }

        public static readonly DependencyProperty HideElementProperty =
            DependencyProperty.Register("HideElement", typeof(bool), typeof(Led), new PropertyMetadata(false, HideElement_PropertyChanged));

        private static void HideElement_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Led led = (Led)d;
            if (bool.TryParse(e.NewValue?.ToString(), out bool result) && result && led.SpecificPlaceElement == true)
            {
                led.Visibility = Visibility.Collapsed;
            }
            else
                led.Visibility = Visibility.Visible;
        }


        #endregion

        #endregion

        #region IsSelectionMoving
        private bool _IsSelectionMoving;
        public bool IsSelectionMoving
        {
            get { return _IsSelectionMoving; }
            set { _IsSelectionMoving = value; NotifyPropertyChanged("IsSelectionMoving"); }
        }
        #endregion IsSelectionMoving

        #region Public fields
        private Brush _InternalBackground;
        public Brush InternalBackground
        {
            get { return _InternalBackground; }
            set { _InternalBackground = value; NotifyPropertyChanged("InternalBackground"); }
        }


        private bool _IsDescripctionVisible;
        public bool IsDescripctionVisible { get { return _IsDescripctionVisible; } set { _IsDescripctionVisible = value; NotifyPropertyChanged("IsDescripctionVisible"); } }

        private bool _IsUnitVisible;
        public bool IsUnitVisible { get { return _IsUnitVisible; } set { _IsUnitVisible = value; NotifyPropertyChanged("IsUnitVisible"); } }

        #endregion

        private void SetBackground(object value)
        {
            if(value != null && bool.TryParse(value?.ToString(), out bool boolVal))
            {
                if(boolVal)
                    this.InternalBackground = this.LedOnBrush;
                else
                    this.InternalBackground = this.LedOffBrush;
            }
            else
                this.InternalBackground = this.Background;
        }

        protected override void DrawControl(DrawingContext drawingContext)
        {
        }

        public Led()
        {
            this.Foreground = Settings.DefaultLineColor;
            this.Background = Brushes.Pink;
            SetBackground(this.LedState);
            this.Width = 20;
            this.Height = 20;
        }

        static Led()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Led), new FrameworkPropertyMetadata(typeof(Led)));
        }

    }
}
