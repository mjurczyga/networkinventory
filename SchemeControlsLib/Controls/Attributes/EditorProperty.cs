﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.Attributes
{
    public class EditorProperty : Attribute, INotifyPropertyChanged
    {
        private string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; OnPropertyChanged("Description"); }
        }

        private string _LongDescription;
        public string LongDescription
        {
            get { return _LongDescription; }
            set { _LongDescription = value; OnPropertyChanged("LongDescription"); }
        }

        private string _Group;
        public string Group
        {
            get { return _Group; }
            set { _Group = value; OnPropertyChanged("Group"); }
        }

        private string _ToolTip;
        public string ToolTip
        {
            get { return _ToolTip; }
            set { _ToolTip = value; OnPropertyChanged("ToolTip"); }
        }

        private bool _Serialize = true;
        public bool Serialize
        {
            get { return _Serialize; }
            set { _Serialize = value; OnPropertyChanged("Serialize"); }
        }

        private bool _CommonProperty;
        public bool CommonProperty
        {
            get { return _CommonProperty; }
            set { _CommonProperty = value; OnPropertyChanged("CommonProperty"); }
        }

        private bool _OnlyToSerialize;
        public bool OnlyToSerialize
        {
            get { return _OnlyToSerialize; }
            set { _OnlyToSerialize = value; OnPropertyChanged("OnlyToSerialize"); }
        }



        #region konstruktory
        public EditorProperty(string description, string group = null, string toolTip = null, string longDescription = null, bool serialize = true, bool groupChangeProperty = false)
        {
            Description = description;
            Group = group;
            ToolTip = toolTip;
            LongDescription = longDescription;
            Serialize = serialize;
            CommonProperty = false;
        }

        public EditorProperty(string description, bool groupChangeProperty, string group = null, string toolTip = null, string longDescription = null, bool serialize = true)
        {
            Description = description;
            Group = group;
            ToolTip = toolTip;
            LongDescription = longDescription;
            Serialize = serialize;
            CommonProperty = groupChangeProperty;
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
