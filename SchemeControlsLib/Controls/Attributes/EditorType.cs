﻿using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.Attributes
{

    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public sealed class EditorType : Attribute
    {
        public EditorType(SchemeTypes schemeType, bool flag, string name, bool visibleInEditor = true)
        {
            SchemeType = schemeType;
            Flag = flag;
            Name = name;
            visibleInEditor = visibleInEditor;
        }

        public EditorType()
        {
            SchemeType = SchemeTypes.SynopticScheme;
            Flag = false;
            Name = "";
            VisibleInEditor = true;
        }

        // czy jest elementem widzialnym w edytorze
        public bool Flag { get; set; }

        // nazwa kontrolki
        public string Name { get; set; }

        public SchemeTypes SchemeType { get; set; }

        public bool VisibleInEditor { get; set; }
    }
}
