﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces.ElementsInterfaces;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorElement(true, "Sekcja pomiarowa")]
    public sealed class MeasuringSection : ActiveElement, ITrolley
    {
        #region private
        private TrolleyBase _tB = new TrolleyBase();
        #endregion

        protected override double WidthToHeightControlRatio => 33.0 / 72.0;

        protected override double DefaultWidth => 33.0;

        private double TrolleyWidthToHeightRatio = 3.0;

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();
            this.OutputPointsCollection.Add(new Point(this.Width / 2, 0));
        }

        protected override void DrawControl(DrawingContext drawingContext)
        {
            if (drawingContext != null)
            {
                double width = this.Width; double height = this.Height;
                var brush = Settings.DefaultLineColor;

                if (SourceBrushes != null && SourceBrushes.Count > 0 && (StateTypes)this.State == StateTypes.Closed)
                {
                    brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, width, true);
                }
                var pen = new Pen(brush, Settings.ElementStrokeThickness);

                // switch
                double switchHeight; double switchWidth; Thickness switchMargin; double openTrolleyDistance = 0; double rectangleDistance;
                if (ControlOrientation == Orientation.Horizontal)
                {
                    if ((StateTypes)this.State == StateTypes.Open)
                        openTrolleyDistance = width * 0.3;

                    switchWidth = width;
                    switchHeight = height - width / TrolleyWidthToHeightRatio;
                    switchMargin = new Thickness(openTrolleyDistance, switchWidth / TrolleyWidthToHeightRatio, 0, 0);
                    rectangleDistance = width / 20;
                }

                else
                {
                    if ((StateTypes)this.State == StateTypes.Open)
                        openTrolleyDistance = height * 0.3;

                    switchHeight = height;
                    switchWidth = width - switchHeight / TrolleyWidthToHeightRatio;
                    switchMargin = new Thickness(switchHeight / TrolleyWidthToHeightRatio, openTrolleyDistance, 0, 0);
                    rectangleDistance = height / 20;
                }

                DrawTrolleyBackground(drawingContext, width, width / TrolleyWidthToHeightRatio, StateToBrushConverter(this.State), new CornerRadius(5));

                DrawTrolley(drawingContext, width, width / TrolleyWidthToHeightRatio, false, this.ControlOrientation, new Pen(Settings.DefaultLineColor, Settings.ElementStrokeThickness), new Thickness(0));

                DrawSwitchType(drawingContext, switchWidth, switchHeight, pen, switchMargin, rectangleDistance);
            }
        }

        private void DrawSwitchType(DrawingContext drawingContext, double width, double height, Pen pen, Thickness margin, double rectangleDistance)
        {
            var heightPomiar = (5 * height) / 6.0;

            double horizontalPosition = margin.Top - margin.Bottom;
            double verticalPosiotion = margin.Left - margin.Right;

            //Linia
            drawingContext.DrawLine(pen, new Point(width / 2 - rectangleDistance * 2 + verticalPosiotion, -rectangleDistance + 3 * horizontalPosition / 4), new Point(width / 2 + verticalPosiotion, 0 + 3 * horizontalPosition / 4));
            drawingContext.DrawLine(pen, new Point(width / 2 + rectangleDistance * 2 + verticalPosiotion, -rectangleDistance + 3 * horizontalPosition / 4), new Point(width / 2 + verticalPosiotion, 0 + 3 * horizontalPosition / 4));
            drawingContext.DrawLine(pen, new Point(width / 2 + verticalPosiotion, 0 + 3 * horizontalPosition / 4), new Point(width / 2 + verticalPosiotion, height / 12 + horizontalPosition));

            // fuse
            drawingContext.DrawLine(pen, new Point(width / 2.0 + verticalPosiotion, heightPomiar / 12.0 + horizontalPosition), new Point(width / 2.0 + verticalPosiotion, 2 * height / 4.0 + horizontalPosition));
            drawingContext.DrawRectangle(null, pen, new Rect(new Point(width / 3.0 + verticalPosiotion, height / 10.0 + horizontalPosition), new Size(width / 3.0, height / 3.0)));

            // trafo
            var circleWidth = 2 * heightPomiar / 6.0;
            var cirdleHeight = circleWidth;
            drawingContext.DrawEllipse(null, pen, new Point(width / 2 - heightPomiar / 6 + circleWidth / 2.0 + verticalPosiotion, 2 * height / 4 + cirdleHeight / 2.0 + horizontalPosition), circleWidth / 2.0, cirdleHeight / 2.0);
            drawingContext.DrawEllipse(null, pen, new Point(width / 2 - 1.75 * heightPomiar / 6 + circleWidth / 2.0 + verticalPosiotion, 2.75 * height / 4 + cirdleHeight / 2.0 + horizontalPosition), circleWidth / 2.0, cirdleHeight / 2.0);
            drawingContext.DrawEllipse(null, pen, new Point(1.75 * heightPomiar / 6 + circleWidth / 2.0 + verticalPosiotion, 2.75 * height / 4 + cirdleHeight / 2.0 + horizontalPosition), circleWidth / 2.0, cirdleHeight / 2.0);
        }

        #region interface implementation
        public void DrawTrolleyTop(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, double distance = 8.0)
        {
            _tB.DrawTrolleyTop(drawingContext, width, height, flippedGraphic, controlOrientation, pen);
        }

        public void DrawTrolleyBottom(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, double distance = 8.0)
        {
            _tB.DrawTrolleyBottom(drawingContext, width, height, flippedGraphic, controlOrientation, pen);
        }

        public void DrawTrolleyBackground(DrawingContext drawingContext, double width, double height, Brush stateBackgroundBrush, CornerRadius cornerRadius)
        {
            _tB.DrawTrolleyBackground(drawingContext, width, height, stateBackgroundBrush, cornerRadius);
        }

        public void DrawTrolley(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, Thickness margin)
        {
            _tB.DrawTrolley(drawingContext, width, height, flippedGraphic, controlOrientation, pen, margin);
        }
        #endregion
    }
}
