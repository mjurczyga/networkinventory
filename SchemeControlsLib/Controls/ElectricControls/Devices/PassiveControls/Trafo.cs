﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, true, "Transformator 2")]
    public sealed class Trafo : PassiveElement
    {
        [EditorProperty("Transformator trójfazowy")]
        public bool ThreePhaze
        {
            get { return (bool)GetValue(ThreePhazeProperty); }
            set { SetValue(ThreePhazeProperty, value); }
        }

        public static readonly DependencyProperty ThreePhazeProperty =
            DependencyProperty.Register("ThreePhaze", typeof(bool), typeof(Trafo), new PropertyMetadata(false, ThreePhaze_PropertyChanged));

        private static void ThreePhaze_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is Trafo trafo)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                {
                    trafo.WidthToHeightControlRatio = 1.001;
                    trafo.Height = ((trafo.DefaultWidth * 2.0) + 1.0) / trafo.WidthToHeightControlRatio;
                }
                else
                {
                    trafo.WidthToHeightControlRatio = 3.0 / 5.0;
                    trafo.Height = trafo.DefaultWidth / (3.0 / 5.0);
                }

                //trafo.InvalidateVisual();
            }
        }

        protected override double WidthToHeightControlRatio { get; set; } = 3.0 / 5.0;

        protected override double DefaultWidth => 19.0;

        private void DrawShape(DrawingContext drawingContext)
        {
            if (drawingContext != null)
            {
                var width = this.ActualWidth;
                var height = this.ActualHeight;
                Brush brush = Settings.DefaultLineColor;

                if (SourceBrushes != null && SourceBrushes.Count > 0)
                {
                    brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, width, true);
                }

                var pen = new Pen(brush, Settings.ElementStrokeThickness);
                pen.Freeze();

                double halfWidth = Math.Round(this.Width / 2.0);
                double halfHeignt = Math.Round(this.Height / 2.0);

                if (!this.ThreePhaze)
                {
                    if (ControlOrientation == Orientation.Vertical)
                    {

                        drawingContext.DrawEllipse(null, pen, new Point(halfHeignt, halfHeignt), halfHeignt, halfHeignt);
                        drawingContext.DrawEllipse(null, pen, new Point(width - halfHeignt, halfHeignt), halfHeignt, halfHeignt);
                    }
                    else
                    {
                        drawingContext.DrawEllipse(null, pen, new Point(halfWidth, halfWidth), halfWidth, halfWidth);
                        drawingContext.DrawEllipse(null, pen, new Point(halfWidth, height - halfWidth), halfWidth, halfWidth);
                    }
                }
                else
                {
                    if (FlipGraphics)
                    {
                        if(ControlOrientation == Orientation.Vertical)
                        {
                            drawingContext.DrawEllipse(null, pen,
                                new Point((halfWidth / 2.0) + Settings.ElementStrokeThickness, halfHeignt - Settings.ElementStrokeThickness / 2.0),
                                halfWidth / 2.0 + Settings.ElementStrokeThickness, halfWidth / 2.0 + Settings.ElementStrokeThickness);

                            drawingContext.DrawEllipse(null, pen,
                                new Point(((3.0 * width) / 4.0) - Settings.ElementStrokeThickness, halfHeignt / 2.0 + Settings.ElementStrokeThickness),
                                halfWidth / 2.0 + Settings.ElementStrokeThickness, halfWidth / 2.0 + Settings.ElementStrokeThickness);

                            drawingContext.DrawEllipse(null, pen,
                                new Point(((3.0 * width) / 4.0) - Settings.ElementStrokeThickness, ((3.0 * height) / 4.0) - Settings.ElementStrokeThickness),
                                halfWidth / 2.0 + Settings.ElementStrokeThickness, halfWidth / 2.0 + Settings.ElementStrokeThickness);
                        }
                        else
                        {
                            drawingContext.DrawEllipse(null, pen,
                                new Point(halfWidth - Settings.ElementStrokeThickness / 2.0, (halfHeignt / 2.0) + Settings.ElementStrokeThickness),
                                halfWidth / 2.0 + Settings.ElementStrokeThickness, halfWidth / 2.0 + Settings.ElementStrokeThickness);

                            drawingContext.DrawEllipse(null, pen,
                                new Point(halfWidth / 2.0 + Settings.ElementStrokeThickness, ((3.0 * height) / 4.0) - Settings.ElementStrokeThickness),
                                halfWidth / 2.0 + Settings.ElementStrokeThickness, halfWidth / 2.0 + Settings.ElementStrokeThickness);

                            drawingContext.DrawEllipse(null, pen,
                                new Point(((3.0 * width) / 4.0) - Settings.ElementStrokeThickness, ((3.0 * height) / 4.0) - Settings.ElementStrokeThickness),
                                halfWidth / 2.0 + Settings.ElementStrokeThickness, halfWidth / 2.0 + Settings.ElementStrokeThickness);
                        }
                    }
                    else
                    {
                        if (ControlOrientation == Orientation.Vertical)
                        {

                            drawingContext.DrawEllipse(null, pen,
                                new Point(((3.0 * width) / 4.0) - Settings.ElementStrokeThickness, halfWidth - Settings.ElementStrokeThickness / 2.0),
                                halfWidth / 2.0 + Settings.ElementStrokeThickness, halfWidth / 2.0 + Settings.ElementStrokeThickness);

                            drawingContext.DrawEllipse(null, pen,
                                new Point((halfWidth / 2.0) + Settings.ElementStrokeThickness, halfWidth / 2.0 + Settings.ElementStrokeThickness),
                                halfWidth / 2.0 + Settings.ElementStrokeThickness, halfWidth / 2.0 + Settings.ElementStrokeThickness);

                            drawingContext.DrawEllipse(null, pen,
                                new Point((halfWidth / 2.0) + Settings.ElementStrokeThickness, ((3.0 * width) / 4.0) - Settings.ElementStrokeThickness),
                                halfWidth / 2.0 + Settings.ElementStrokeThickness, halfWidth / 2.0 + Settings.ElementStrokeThickness);
                        }
                        else
                        {
                            drawingContext.DrawEllipse(null, pen,
                                new Point(halfWidth - Settings.ElementStrokeThickness / 2.0, ((3.0 * width) / 4.0) - Settings.ElementStrokeThickness),
                                halfWidth / 2.0 + Settings.ElementStrokeThickness, halfWidth / 2.0 + Settings.ElementStrokeThickness);

                            drawingContext.DrawEllipse(null, pen,
                                new Point(halfWidth / 2.0 + Settings.ElementStrokeThickness, (halfWidth / 2.0) + Settings.ElementStrokeThickness),
                                halfWidth / 2.0 + Settings.ElementStrokeThickness, halfWidth / 2.0 + Settings.ElementStrokeThickness);

                            drawingContext.DrawEllipse(null, pen,
                                new Point(((3.0 * width) / 4.0) - Settings.ElementStrokeThickness, (halfWidth / 2.0) + Settings.ElementStrokeThickness),
                                halfWidth / 2.0 + Settings.ElementStrokeThickness, halfWidth / 2.0 + Settings.ElementStrokeThickness);
                        }
                    }
                }
            }
        }

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();
            if (ControlOrientation == Orientation.Vertical)
            {
                OutputPointsCollection.Add(new Point(0.0, this.Height / 2.0));
                OutputPointsCollection.Add(new Point(this.Width, this.Height / 2.0));
            }
            else
            {
                OutputPointsCollection.Add(new Point(this.Width / 2.0, 0.0));
                OutputPointsCollection.Add(new Point(this.Width / 2.0, this.Height));
            }
        }

        protected override void DrawControl(DrawingContext drawingContext)
        {
            this.DrawShape(drawingContext);
        }

        static Trafo()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Trafo), new FrameworkPropertyMetadata(typeof(Trafo)));
        }

    }
}
