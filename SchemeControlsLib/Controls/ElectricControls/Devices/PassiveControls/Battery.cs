﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, true, "Bateria")]
    public class Battery : PassiveElement
    {
        public new bool FlipGraphics { get { return base.FlipGraphics; } set { base.FlipGraphics = value; } }

        protected override double WidthToHeightControlRatio => 0.25;

        protected override double DefaultWidth => 8;

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();
            if (ControlOrientation == Orientation.Horizontal)
            {
                OutputPointsCollection.Add(new Point(0, this.Height / 2.0));
                OutputPointsCollection.Add(new Point(this.Width, this.Height / 2.0));
            }
            else
            {
                OutputPointsCollection.Add(new Point(this.Width / 2.0, 0));
                OutputPointsCollection.Add(new Point(this.Width / 2.0, this.Height));
            }
        }

        protected override void DrawControl(DrawingContext drawingContext)
        {
            this.DrawShape(drawingContext);
        }

        private void DrawShape(DrawingContext drawingContext)
        {
            if (drawingContext != null)
            {
                var width = this.ActualWidth;
                var height = this.ActualHeight;
                Brush brush = Settings.DefaultLineColor;

                if (SourceBrushes != null && SourceBrushes.Count > 0)
                {
                    brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, width, true);
                }

                var pen = new Pen(brush, Settings.LinesStrokeThickness);
                pen.Freeze();

                double halfWidth = Math.Round(this.Width / 2.0);
                double halfHeignt = Math.Round(this.Height / 2.0);

                if (ControlOrientation == Orientation.Horizontal)
                {
                    drawingContext.DrawLine(pen,
                        new Point(0, 0),
                        new Point(0, height));

                    drawingContext.DrawLine(pen,
                        new Point(width, 0.0),
                        new Point(width, height));
                }
                else
                {
                    drawingContext.DrawLine(pen,
                        new Point(0, 0.0),
                        new Point(width, 0.0));

                    drawingContext.DrawLine(pen,
                        new Point(0.0, height),
                        new Point(width, height));

                }
            }
        }

        static Battery()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Battery), new FrameworkPropertyMetadata(typeof(Battery)));
        }
    }
}
