﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, true, "Węzeł")]
    public sealed class Node : PassiveElement, IPLCControl
    {
        public string PLCStructName => "ST_CONNECTOR";

        public new bool FlipGraphics { get { return base.FlipGraphics; } set { base.FlipGraphics = value; } }

        public new Orientation ControlOrientation { get { return base.ControlOrientation; } set { base.ControlOrientation = value; } }

        public new bool IsCompensator { get { return base.IsCompensator; } set { base.IsCompensator = value; } }

        protected override double DefaultWidth => 5.0;

        protected override double WidthToHeightControlRatio => 1.0;

        #region Dependency properties

        [EditorProperty("Widoczność węzła")]
        public bool IsGraphicsVisible
        {
            get { return (bool)GetValue(IsGraphicsVisibleProperty); }
            set { SetValue(IsGraphicsVisibleProperty, value); }
        }

        public static readonly DependencyProperty IsGraphicsVisibleProperty =
            DependencyProperty.Register("IsGraphicsVisible", typeof(bool), typeof(Node), new PropertyMetadata(true, IsGraphicsVisible_PropertyChanged));

        private static void IsGraphicsVisible_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Node source = (Node)d;
            source.InvalidateVisual();
        }
        #endregion

        protected override void DrawControl(DrawingContext drawingContext)
        {
            if (drawingContext != null && this.IsGraphicsVisible)
            {
                var width = this.ActualWidth;
                var height = this.ActualHeight;

                var brush = Settings.DefaultLineColor;

               
                if (SourceBrushes != null && SourceBrushes.Count > 0)
                {
                    brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, width);
                }

                var pen = new Pen(brush, Settings.ElementStrokeThickness);
                pen.Freeze(); 

                drawingContext.DrawEllipse(brush, pen, new Point(width / 2.0, height / 2.0), width / 2.0, height / 2.0);
            }
        }

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();

            OutputPointsCollection.Add(new Point(0, this.Height / 2.0));
            OutputPointsCollection.Add(new Point(this.Width, this.Height / 2.0));
            OutputPointsCollection.Add(new Point(this.Width / 2.0, 0));
            OutputPointsCollection.Add(new Point(this.Width / 2.0, this.Height));
        }

        static Node()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Node), new FrameworkPropertyMetadata(typeof(Node)));
        }
    }
}
