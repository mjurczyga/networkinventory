﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, true, "Uziemnik")]
    public sealed class Ground : ElectricElementBase, ISource
    {
        protected override double WidthToHeightControlRatio => 3.0 / 4.0;

        protected override double DefaultWidth => 25.0;

        #region dependency properties
        public string SourceDescription => "GROUND";

        public int SourceNumber => 999;

        [EditorProperty("Kolor uziemienia")]
        public Brush SourceBrush
        {
            get { return (Brush)GetValue(SourceBrushProperty); }
            set { SetValue(SourceBrushProperty, value); }
        }

        public static readonly DependencyProperty SourceBrushProperty =
            DependencyProperty.Register("SourceBrush", typeof(Brush), typeof(Ground), new PropertyMetadata(Settings.DefaultLineColor, SourceColor_PropertyChanged));

        private static void SourceColor_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Ground ground = (Ground)d;
            if (e.NewValue != Settings.DefaultLineColor)
                ground.AddBrushToSourceBrushes((Brush)e.NewValue);

            ground.RemoveBrushFromSourceBrushes((Brush)e.OldValue);
            ground.InvalidateVisual();
        }
        #endregion

        protected override void DrawControl(DrawingContext drawingContext)
        {
            this.DrawShape(drawingContext);
        }

       

        // ewentualnie do dopisania zmiana orientacji
        private void DrawShape(DrawingContext drawingContext)
        {
            if (drawingContext != null)
            {
                double width = this.ActualWidth;
                double height = this.ActualHeight;

                Brush brush = Settings.DefaultLineColor;
                if (SourceBrushes != null && SourceBrushes.Count > 0)
                    brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, width);

                var pen = new Pen(brush, Settings.ElementStrokeThickness);
                pen.Freeze();


                if (ControlOrientation == Orientation.Vertical)
                {
                    double yStart;

                    if (!this.FlipGraphics)
                        yStart = 0;
                    else
                        yStart = height;

                    double distance = height / 6.0;

                    drawingContext.DrawLine(pen, new Point(width / 2.0, yStart), new Point(width / 2.0, Math.Abs(yStart - height / 2.0)));
                    drawingContext.DrawLine(pen, new Point(0, Math.Abs(yStart - height / 2.0)), new Point(width, Math.Abs(yStart - height / 2.0)));
                    drawingContext.DrawLine(pen, new Point(0.15 * width, Math.Abs(yStart - height / 2.0 - distance)), new Point(0.85 * width, Math.Abs(yStart - height / 2.0 - distance)));
                    drawingContext.DrawLine(pen, new Point(0.3 * width, Math.Abs(yStart - height / 2.0 - 2.0 * distance)), new Point(0.7 * width, Math.Abs(yStart - height / 2.0 - 2.0 * distance)));
                    drawingContext.DrawLine(pen, new Point(0.45 * width, Math.Abs(yStart - height / 2.0 - 3.0 * distance)), new Point(0.55 * width, Math.Abs(yStart - height / 2.0 - 3.0 * distance)));
                }
                else
                {
                    double xStart;

                    if (!this.FlipGraphics)
                        xStart = 0;
                    else
                        xStart = width;

                    double distance = width / 6.0;

                    drawingContext.DrawLine(pen, new Point(xStart, height / 2.0), new Point(Math.Abs(xStart - width / 2.0), height / 2.0));
                    drawingContext.DrawLine(pen, new Point(Math.Abs(xStart - width / 2.0), 0), new Point(Math.Abs(xStart - width / 2.0), height));
                    drawingContext.DrawLine(pen, new Point(Math.Abs(xStart - width / 2.0 - distance), 0.15 * height), new Point(Math.Abs(xStart - width / 2.0 - distance), 0.85 * height));
                    drawingContext.DrawLine(pen, new Point(Math.Abs(xStart - width / 2.0 - 2.0 * distance), 0.3 * height), new Point(Math.Abs(xStart - width / 2.0 - 2.0 * distance), 0.7 * height));
                    drawingContext.DrawLine(pen, new Point(Math.Abs(xStart - width / 2.0 - 3.0 * distance), 0.45 * height), new Point(Math.Abs(xStart - width / 2.0 - 3.0 * distance), 0.55 * height));
                }
            }
        }

        #region konstruktory
        public Ground()
        {
            this.AddBrushToSourceBrushes(Settings.DefaultLineColor);
        }

        static Ground()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Ground), new FrameworkPropertyMetadata(typeof(Ground)));
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.SourceBrush = Brushes.Yellow;
            this.ControlOrientation = Orientation.Vertical;
        }
        #endregion

        protected override void MethodAfterAddBrushToSourceBrushes(Brush addedBrush)
        {
            if (OutputNodesCollection != null)
            {
                foreach (var output in OutputNodesCollection)
                    output.AddBrushToSourceBrushes(addedBrush);
            }
        }

        protected override void MethodAfterRemoveBrushToSourceBrushes(Brush removedBrush)
        {
            if (OutputNodesCollection != null)
            {
                foreach (var output in OutputNodesCollection)
                {
                    output.RemoveBrushFromSourceBrushes(removedBrush);
                }
            }
        }

        protected override void FlipGraphicChanged(bool newValue)
        {
            if(OutputPointsCollection.Count == 1)
            {
                this.ComputeAndAddOutputs();
            }
        }

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();

            if (ControlOrientation == Orientation.Vertical)
            {
                if (!this.FlipGraphics)
                {
                    OutputPointsCollection.Clear();
                    OutputPointsCollection.Add(new Point(this.Width / 2.0, 0));
                }
                else
                {
                    OutputPointsCollection.Clear();
                    OutputPointsCollection.Add(new Point(this.Width / 2.0, this.Height));
                }
            }
            else
            {
                if (!this.FlipGraphics)
                {
                    OutputPointsCollection.Clear();
                    OutputPointsCollection.Add(new Point(0, this.Height / 2.0));
                }
                else
                {
                    OutputPointsCollection.Clear();
                    OutputPointsCollection.Add(new Point(this.Width, this.Height / 2.0));
                }
            }
        }
    }
}
