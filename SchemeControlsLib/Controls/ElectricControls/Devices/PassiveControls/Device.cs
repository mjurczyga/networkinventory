﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.Helpers;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, true, "Urządzenie")]
    public sealed class Device : PassiveElement
    {
        public new bool FlipGraphics { get { return base.FlipGraphics; } set { base.FlipGraphics = value; } }

        //public TextAlignment TextAlignment
        //{
        //    get { return (TextAlignment)GetValue(TextAlignmentProperty); }
        //    set { SetValue(TextAlignmentProperty, value); }
        //}

        //public static readonly DependencyProperty TextAlignmentProperty =
        //    DependencyProperty.Register("TextAlignment", typeof(TextAlignment), typeof(ownerclass), new PropertyMetadata(0));

        #region Ingerited properties
        [EditorProperty("Kolor tekstu")]
        public new Brush Foreground { get { return base.Foreground; } set { base.Foreground = value; } }
        #endregion 

        #region dependecy properties
        [EditorProperty("Tekst")]
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(Device), new PropertyMetadata("", Text_PropertyChanged));

        private static void Text_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Device device = (Device)d;
            device.InvalidateVisual();
        }
        #endregion

        protected override double WidthToHeightControlRatio => 1.0001;

        protected override double DefaultWidth => 20;

        protected override void DrawControl(DrawingContext drawingContext)
        {
            double width = this.Width; double height = this.Height;           
            // kolor kontrolki
            var brush = Settings.DefaultLineColor;
            if (SourceBrushes != null && SourceBrushes.Count > 0)
            {
                brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, width, true);
            }
            var pen = new Pen(brush, Settings.ElementStrokeThickness);

            if (drawingContext != null)
            {
                drawingContext.DrawEllipse(null, pen, new System.Windows.Point(width / 2.0, height / 2.0), width / 2.0, height / 2.0);

                if (!this.Text.CheckIfIsNullOrEpmpty())
                {
                    double fontSize = this.Text.ComputeFontSize(new Size(this.Width - 6, this.Height - 6));
                    // tekst kontrolki
                    Typeface typeFace = new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Normal, FontStretches.Normal);                  
                    FormattedText formattedText = new FormattedText(this.Text, CultureInfo.CurrentUICulture, FlowDirection.LeftToRight, typeFace, fontSize, this.Foreground);
                    //formattedText.TextAlignment = 
                    Point textLocation = new Point(width / 2.0 - formattedText.WidthIncludingTrailingWhitespace / 2.0, (height - formattedText.Height) / 2.0);
                    drawingContext.DrawText(formattedText, textLocation);
                }
            }
        }

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();
            if (this.ControlOrientation == System.Windows.Controls.Orientation.Horizontal)
            {
                this.OutputPointsCollection.Add(new Point(this.Width / 2.0, 0));
                this.OutputPointsCollection.Add(new Point(this.Width / 2.0, this.Height));
            }
            else
            {
                this.OutputPointsCollection.Add(new Point(0, this.Height / 2.0));
                this.OutputPointsCollection.Add(new Point(this.Width, this.Height / 2.0));
            }
        }
    }
}
