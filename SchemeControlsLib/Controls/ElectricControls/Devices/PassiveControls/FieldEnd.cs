﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.Helpers;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, true, "Wyjście z pola")]
    public sealed class FieldEnd : PassiveElement
    {
        #region dependecy properties
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(FieldEnd), new PropertyMetadata("", Text_PropertyChanged));

        private static void Text_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FieldEnd device = (FieldEnd)d;
            device.InvalidateVisual();
        }
        #endregion

        protected override double WidthToHeightControlRatio => 2.0;

        protected override double DefaultWidth => 39;

        protected override void DrawControl(DrawingContext drawingContext)
        {
            if (drawingContext != null)
            {
                double width = this.Width; double height = this.Height;
                // kolor kontrolki
                var brush = Settings.DefaultLineColor;
                if (SourceBrushes != null && SourceBrushes.Count > 0)
                {
                    brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, width, true);
                }
                var pen = new Pen(brush, Settings.ElementStrokeThickness);

                double radius = this.FlipGraphics ? height / 5 : width / 5;

                drawingContext.DrawRoundedRectangle(null, pen, new Rect(new Size(width, height)), radius, radius);

                if (!this.Text.CheckIfIsNullOrEpmpty())
                {                   
                    // tekst kontrolki
                    Typeface typeFace = new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Normal, FontStretches.Normal);
                    FormattedText formattedText = new FormattedText(this.Text, CultureInfo.CurrentUICulture, FlowDirection.LeftToRight, typeFace, Math.Abs((ValueChecker.CheckDoubleNanInfOrZero(this.FontSize) ? 0 : this.FontSize) - 1.0), this.Foreground);
                    // tekst kontrolki
                    Point textLocation = new Point(width / 2.0 - formattedText.WidthIncludingTrailingWhitespace / 2.0, formattedText.LineHeight + height);
                    drawingContext.DrawText(formattedText, textLocation);


                    //double fontSize = this.Text.ComputeFontSize(new Size(this.Width - 6, this.Height - 6));
                    //Typeface typeFace = new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Normal, FontStretches.Normal);
                    //FormattedText formattedText = new FormattedText(this.Text, CultureInfo.CurrentUICulture, FlowDirection.LeftToRight, typeFace, fontSize, this.Foreground);
                    //Point textLocation = new Point(width / 2.0 - formattedText.WidthIncludingTrailingWhitespace / 2.0, (height - formattedText.Height) / 2.0);
                    //drawingContext.DrawText(formattedText, textLocation);
                }
            }
        }

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();
            if (this.ControlOrientation == System.Windows.Controls.Orientation.Horizontal)
            {
                if (this.FlipGraphics)
                    this.OutputPointsCollection.Add(new Point(this.Width / 2.0, this.Height));
                else
                    this.OutputPointsCollection.Add(new Point(this.Width / 2.0, 0));

            }
            else
            {
                if (this.FlipGraphics)
                    this.OutputPointsCollection.Add(new Point(this.Width, this.Height / 2.0));
                else
                    this.OutputPointsCollection.Add(new Point(0, this.Height / 2.0));
            }
        }
    }
}
