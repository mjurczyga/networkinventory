﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, true, "Falownik")]
    public class PowerInverter : PassiveElement
    {
        public new bool FlipGraphics { get { return base.FlipGraphics; } set { base.FlipGraphics = value; } }

        protected override double WidthToHeightControlRatio { get => 1.00001; }

        protected override double DefaultWidth { get => 38.0; }

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();
            if (ControlOrientation == Orientation.Vertical)
            {
                OutputPointsCollection.Add(new Point(0, this.Height / 2.0));
                OutputPointsCollection.Add(new Point(this.Width, this.Height / 2.0));
            }
            else
            {
                OutputPointsCollection.Add(new Point(this.Width / 2.0, 0));
                OutputPointsCollection.Add(new Point(this.Width / 2.0, this.Height));
            }
        }

        protected override void DrawControl(DrawingContext drawingContext)
        {
            this.DrawShape(drawingContext);
        }

        public void DrawShape(DrawingContext drawingContext)
        {
            if (drawingContext != null)
            {
                var width = this.ActualWidth;
                var height = this.ActualHeight;

                var brush = Settings.DefaultLineColor;
                var secondBrush = Settings.DefaultLineColor;

                Point p2, p3;
                Rect rect; CornerRadius cornerRadius;

                //if (ControlOrientation == Orientation.Vertical)
                //{

                //}
                //else
                //{
                    // Rectangle point and size
                    rect = new Rect(new Point(0, 0), new Size(width, height));
                    // First line points
                    p2 = new Point(width, 0); p3 = new Point(0, height);
                //}

                if (SourceBrushes != null && SourceBrushes.Count > 0)
                {
                    brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, width);
                    secondBrush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, p2, p3);
                }

                var pen = new Pen(brush, Settings.ElementStrokeThickness);
                var secondPen = new Pen(secondBrush, Settings.LinesStrokeThickness);
                pen.Freeze(); secondPen.Freeze();

                DrawingContextShapes.DrawRoundedRectangle(drawingContext, null, pen, rect, new CornerRadius());
                drawingContext.DrawLine(pen, p2, p3);

                drawingContext.DrawText(new FormattedText("~", CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight,
                                        new Typeface("Verdana"), 1.5 / 3.0 * width, System.Windows.Media.Brushes.White), 
                                        new System.Windows.Point((0.1 * width)/ 4.0, (0.1 * height) / 4.0));

                drawingContext.DrawText(new FormattedText("~", CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight,
                                        new Typeface("Verdana"), 1.5 / 3.0 * width, System.Windows.Media.Brushes.White),
                                        new System.Windows.Point((2.1 * width) / 4.0, (2.1 * width) / 4.0));
            }
        }

        static PowerInverter()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PowerInverter), new FrameworkPropertyMetadata(typeof(PowerInverter)));
        }
    }
}
