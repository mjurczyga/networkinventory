﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{

    [EditorType(SchemeTypes.SynopticScheme, true, "Przekładnik prądowy")]
    public class CurrentTransformer : PassiveElement
    {
        public new bool FlipGraphics { get { return base.FlipGraphics; } set { base.FlipGraphics = value; } }

        protected override double WidthToHeightControlRatio => 1.0 / 1.000000001;

        protected override double DefaultWidth => 19.0;

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();
            if (ControlOrientation == Orientation.Vertical)
            {
                OutputPointsCollection.Add(new Point(0, this.Height / 2.0));
                OutputPointsCollection.Add(new Point(this.Width, this.Height / 2.0));
            }
            else
            {
                OutputPointsCollection.Add(new Point(this.Width / 2.0, 0));
                OutputPointsCollection.Add(new Point(this.Width / 2.0, this.Height));
            }
        }

        protected override void DrawControl(DrawingContext drawingContext)
        {
            this.DrawShape(drawingContext);
        }

        private void DrawShape(DrawingContext drawingContext)
        {
            if (drawingContext != null)
            {
                var width = this.ActualWidth;
                var height = this.ActualHeight;
                Brush brush = Settings.DefaultLineColor;

                if (SourceBrushes != null && SourceBrushes.Count > 0)
                {
                    brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, width, true);
                }

                var pen = new Pen(brush, Settings.ElementStrokeThickness);
                pen.Freeze();

                double ellipseCenterX = width / 2.0;
                double ellipseCenterY = height / 2.0;

                if (ControlOrientation == Orientation.Vertical)
                {
                    drawingContext.DrawEllipse(null, pen, new Point(ellipseCenterX, ellipseCenterY), ellipseCenterX, ellipseCenterX);

                    drawingContext.DrawLine(pen, 
                        new Point(0, ellipseCenterX), 
                        new Point(height, ellipseCenterX));
                }
                else
                {
                    drawingContext.DrawEllipse(null, pen, new Point(ellipseCenterY, ellipseCenterX), ellipseCenterY, ellipseCenterY);

                    drawingContext.DrawLine(pen, 
                        new Point(ellipseCenterX, 0), 
                        new Point(ellipseCenterX, height));
                }
            }
        }

        static CurrentTransformer()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CurrentTransformer), new FrameworkPropertyMetadata(typeof(CurrentTransformer)));
        }
    }
}
