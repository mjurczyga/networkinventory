﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, true, "Wolne miejsce")]
    public class FreeSpot : PassiveElement
    {
        public new bool FlipGraphics { get { return base.FlipGraphics; } set { base.FlipGraphics = value; } }

        [EditorProperty("Kolor tekstu")]
        public new Brush Foreground { get { return base.Foreground; } set { base.Foreground = value; } }

        [EditorProperty("Rozmiar tekstu")]
        public new double FontSize { get { return base.FontSize; } set { base.FontSize = value; } }

        #region dependency properties
        [EditorProperty("Opis wyjścia")]
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(FreeSpot), new PropertyMetadata("", Text_PropertyChanged));

        private static void Text_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FreeSpot freeSpot = (FreeSpot)d;
            freeSpot.InvalidateVisual();
        }
        #endregion

        protected override double WidthToHeightControlRatio => 25.0 / 14.0;

        protected override double DefaultWidth => 25.0;

        protected override void DrawControl(DrawingContext drawingContext)
        {
            if (drawingContext != null)
            {
                // tekst kontrolki
                Typeface typeFace = new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Normal, FontStretches.Normal);
                FormattedText formattedText = new FormattedText(this.Text, CultureInfo.CurrentUICulture, FlowDirection.LeftToRight, typeFace, Math.Abs((ValueChecker.CheckDoubleNanInfOrZero(this.FontSize) ? 0 : this.FontSize) - 1.0), this.Foreground);

                var drawingWidth = this.ActualWidth;
                var drawingHeight = this.ActualHeight;

                // kor kontrolki
                var brush = Settings.DefaultLineColor;
                if (SourceBrushes != null && SourceBrushes.Count > 0)
                {
                    brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, drawingWidth, true);
                }
                var pen = new Pen(brush, Settings.ElementStrokeThickness);

                if (this.ControlOrientation == Orientation.Horizontal)
                {
                    double xCenter = drawingWidth / 2.0;

                    if (!string.IsNullOrEmpty(this.Text))
                    {
                        Point textLocation = new Point(drawingWidth / 2.0 - formattedText.WidthIncludingTrailingWhitespace / 2.0, formattedText.LineHeight + drawingHeight);
                        drawingContext.DrawText(formattedText, textLocation);
                    }

                    drawingContext.DrawLine(pen, new Point(0, 0), new Point(xCenter, drawingHeight));
                    drawingContext.DrawLine(pen, new Point(drawingWidth, 0), new Point(xCenter, drawingHeight));
                    drawingContext.DrawLine(pen, new Point(xCenter, 0), new Point(xCenter, drawingHeight));
                }
                else
                {
                    double yCenter = drawingHeight / 2.0;

                    if (!string.IsNullOrEmpty(this.Text))
                    {
                        Point textLocation = new Point(drawingWidth / 2.0 - formattedText.WidthIncludingTrailingWhitespace / 2.0, formattedText.LineHeight + drawingHeight);
                        drawingContext.DrawText(formattedText, textLocation);
                    }

                    drawingContext.DrawLine(pen, new Point(0, 0), new Point(drawingWidth, yCenter));
                    drawingContext.DrawLine(pen, new Point(0, drawingHeight), new Point(drawingWidth, yCenter));
                    drawingContext.DrawLine(pen, new Point(0, yCenter), new Point(drawingWidth, yCenter));
                }


            }
        }

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();
            if (this.ControlOrientation == Orientation.Horizontal)
            {
                this.OutputPointsCollection.Add(new Point(this.Width / 2.0, 0));
            }
            else
            {
                this.OutputPointsCollection.Add(new Point(0, this.Height / 2.0));
            }
        }

        static FreeSpot()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(FreeSpot), new FrameworkPropertyMetadata(typeof(FreeSpot)));
        }

        //protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        //{
        //    //Uaktualnienie współrzędnych wyjść po zmianie rozmiaru
        //    Size measuredSize = ComputeControlSize(sizeInfo.NewSize);
        //    UpdateWidthAndHeight(measuredSize);

        //    ComputeAndAddOutputs();
        //}

        //protected override Size MeasureOverride(Size constraint)
        //{
        //    /*if (ValueChecker.CheckDoubleNanInfOrZero(constraint.Width))
        //        constraint.Width = 0;
        //    if (ValueChecker.CheckDoubleNanInfOrZero(constraint.Height))
        //        constraint.Height = 0;*/
        //    Size measuredSize = ComputeControlSize(constraint);

        //    return measuredSize;
        //}

        //protected override Size ArrangeOverride(Size arrangeBounds)
        //{                  
        //    return _SavedSize;
        //}        

        //private Size _SavedSize = new Size();     
        //private void UpdateWidthAndHeight(Size measuredSize)
        //{
        //    if (this.Width != measuredSize.Width)
        //        this.Width = measuredSize.Width;

        //    if (this.Height != measuredSize.Height)
        //        this.Height = measuredSize.Height;
        //}

        //private Size ComputeControlSize(Size currentSize)
        //{
        //    double width = currentSize.Width; double height = currentSize.Height;

        //    if (ControlOrientation == Orientation.Horizontal)
        //    {
        //        //sprawdzenie czy została ustawiona właściwość Height
        //        if (_SavedSize.Height != height && !ValueChecker.CheckDoubleNanInfOrZero(this.Height))               
        //            width = height * WidthToHeightControlRatio;


        //        else if (_SavedSize.Width != width && !ValueChecker.CheckDoubleNanInfOrZero(this.Width))              
        //            height = width / WidthToHeightControlRatio;                
        //    }
        //    else
        //    {
        //        if (_SavedSize.Height != height && !ValueChecker.CheckDoubleNanInfOrZero(this.Height))
        //            width = currentSize.Height / WidthToHeightControlRatio;
        //        else if (_SavedSize.Height != width && !ValueChecker.CheckDoubleNanInfOrZero(this.Width))
        //            height = currentSize.Width * WidthToHeightControlRatio;
        //    }

        //    var result = new Size(Math.Round(width, 2), Math.Round(height, 2));

        //    _SavedSize = result;
        //    return result;
        //}

        // KOD ROZWOJOWY!
        //protected override void DrawControl(DrawingContext drawingContext)
        //{
        //    if(drawingContext != null)
        //    {
        //        double fontSize = 0;

        //        Typeface typeFace = new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Normal, FontStretches.Normal);
        //        FormattedText formattedText = new FormattedText(this.Text, CultureInfo.CurrentUICulture, FlowDirection.LeftToRight, typeFace, Math.Abs((ValueChecker.CheckDoubleNanInfOrZero(this.FontSize) ? 0 : this.FontSize) - 1.0), this.Foreground);
        //        if (!string.IsNullOrEmpty(this.Text))
        //        {
        //            fontSize = formattedText.Height;
        //        }

        //        var drawingWidth = this.ActualWidth;
        //        var drawingHeight = this.ActualHeight - fontSize;
        //        double xLineOffset = 0;
        //        double yCenter = drawingWidth / 2.0;

        //        if (!string.IsNullOrEmpty(this.Text))
        //        {                    
        //            Point textLocation = new Point(drawingWidth / 2.0 - formattedText.WidthIncludingTrailingWhitespace / 2.0, formattedText.LineHeight + drawingHeight);
        //            drawingContext.DrawText(formattedText, textLocation);


        //            var computedWidth = drawingHeight * this.WidthToHeightControlRatio;
        //            if (formattedText.Width > computedWidth)
        //            {
        //                xLineOffset = (formattedText.Width - computedWidth) / 2.0;
        //                yCenter = formattedText.Width / 2.0;
        //            }

        //        }




        //        var brush = Settings.DefaultLineColor;
        //        var secondBrush = Settings.DefaultLineColor;

        //        if (SourceBrushes != null && SourceBrushes.Count > 0)
        //        {
        //            brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, drawingWidth, true);
        //        }
        //        var pen = new Pen(brush, Settings.ElementStrokeThickness);

        //        drawingContext.DrawLine(pen, new Point(xLineOffset, 0), new Point(yCenter, drawingHeight));
        //        drawingContext.DrawLine(pen, new Point(drawingWidth - xLineOffset, 0), new Point(yCenter, drawingHeight));
        //        drawingContext.DrawLine(pen, new Point(yCenter, 0), new Point(yCenter, drawingHeight));


        //    }   
        //}

        // private Size ComputeControlSize(Size currentSize)
        // {
        //double width = currentSize.Width; double height = currentSize.Height;

        //Typeface typeFace = new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Normal, FontStretches.Normal);
        //FormattedText formattedText = new FormattedText(this.Text, CultureInfo.CurrentUICulture, FlowDirection.LeftToRight, typeFace, Math.Abs((ValueChecker.CheckDoubleNanInfOrZero(this.FontSize) ? 0 : this.FontSize) - 1.0), //this.Foreground);

        //if (this._TextDeleted)
        //{
        //    _fontHeight = -_savedFontSize;
        //}

        //if (this._TextAdded || this._TextFontSizeChanged)
        //{
        //    if (this._TextFontSizeChanged)
        //    {
        //        height = height - _savedFontSize;
        //    }

        //    _fontHeight = formattedText.Height;
        //    _savedFontSize = _fontHeight;
        //}           


        //if (ControlOrientation == Orientation.Horizontal)
        //{
        //    //sprawdzenie czy została ustawiona właściwość Height
        //    if (_SavedSize.Height != height && !ValueChecker.CheckDoubleNanInfOrZero(this.Height))
        //    {
        //        width = height * WidthToHeightControlRatio;
        //    }

        //    if (_SavedSize.Width != width && !ValueChecker.CheckDoubleNanInfOrZero(this.Width) || _fontHeight != 0)
        //    {
        //        if (_fontHeight < 0)
        //            width = (height + _fontHeight) * WidthToHeightControlRatio;

        //        height = width / WidthToHeightControlRatio + (_fontHeight <= 0 ? 0 : _fontHeight);
        //    }
        //}
        //else
        //{
        //    if (_SavedSize.Height != height && !ValueChecker.CheckDoubleNanInfOrZero(this.Height))
        //        width = currentSize.Height / WidthToHeightControlRatio;
        //    else if (_SavedSize.Height != width && !ValueChecker.CheckDoubleNanInfOrZero(this.Width))
        //        height = currentSize.Width * WidthToHeightControlRatio + (_fontHeight <= 0 ? 0 : _fontHeight);
        //}

        //if (Math.Round(formattedText.Width, 2) > width)
        //    width = Math.Round(formattedText.Width, 2);

        //var result = new Size(Math.Round(width, 2), Math.Round(height, 2));

        //_SavedSize = result;
        //_fontHeight = 0;

        //this._TextAdded = false;
        //this._TextDeleted = false;
        //this._TextFontSizeChanged = false;
        //return result;
        //}

    }
}
