﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    public enum DlawikTypes
    {
        Normal,
        Coil,
    }


    [EditorType(SchemeTypes.SynopticScheme, true, "Dlawik")]
    public sealed class Dlawik : PassiveElement
    {
        //public new bool FlipGraphics { get { return base.FlipGraphics; } set { base.FlipGraphics = value; } }

        [EditorProperty("Typ dławika")]
        public DlawikTypes DlawikType
        {
            get { return (DlawikTypes)GetValue(DlawikTypeProperty); }
            set { SetValue(DlawikTypeProperty, value); }
        }
        public static readonly DependencyProperty DlawikTypeProperty =
            DependencyProperty.Register("DlawikType", typeof(DlawikTypes), typeof(Dlawik), new PropertyMetadata(DlawikTypes.Normal, DlawikType_PropertyChanged));

        private static void DlawikType_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is Dlawik dlawik)
            {
                if ((DlawikTypes)e.NewValue == DlawikTypes.Normal)
                    dlawik.WidthToHeightControlRatio = 0.99;                
                if ((DlawikTypes)e.NewValue == DlawikTypes.Coil)
                    dlawik.WidthToHeightControlRatio = 1.0 / 3.0;

                dlawik.Height = (1.0 / dlawik.WidthToHeightControlRatio) * dlawik.DefaultWidth;
                //dlawik.InvalidateMeasure();
                //dlawik.InvalidateVisual();
            }
        }

        protected override double WidthToHeightControlRatio { get; set; } = 0.99;

        protected override double DefaultWidth => 20;

        protected override void DrawControl(DrawingContext drawingContext)
        {
            this.DrawShape(drawingContext);
        }

        private void DrawShape(DrawingContext drawingContext)
        {
            if (drawingContext != null)
            {
                var width = this.Width;
                var height = this.Height;
                var brush = Settings.DefaultLineColor;

                if (SourceBrushes != null && SourceBrushes.Count > 0)
                {
                    brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, width, true);
                }
                var pen = new Pen(brush, Settings.ElementStrokeThickness);


                var geometry = new StreamGeometry();
                using (var context = geometry.Open())
                {
                    if (this.DlawikType == DlawikTypes.Normal)
                    {
                        if (this.ControlOrientation == Orientation.Horizontal)
                        {
                            var startPoint = (this.FlipGraphics ? new Point(width / 2, Settings.ElementStrokeThickness / 2) : new Point(width / 2, height - Settings.ElementStrokeThickness / 2));

                            bool isStroked = pen != null;
                            const bool isSmoothJoin = true;

                            context.BeginFigure(startPoint, false, false);
                            context.ArcTo(new Point(Settings.ElementStrokeThickness / 2, height / 2),
                                new Size((width - Settings.ElementStrokeThickness) / 2, (height - Settings.ElementStrokeThickness) / 2),
                                0, true, (this.FlipGraphics ? SweepDirection.Clockwise : SweepDirection.Counterclockwise), isStroked, isSmoothJoin);
                            context.LineTo(new Point(width / 2, height / 2), isStroked, isSmoothJoin);
                            context.LineTo(this.FlipGraphics ? new Point(width / 2, height) : new Point(width / 2, 0), isStroked, isSmoothJoin);

                            context.Close();
                        }
                        else
                        {
                            var startPoint = (this.FlipGraphics ? new Point(Settings.ElementStrokeThickness / 2, width / 2) : new Point(height - Settings.ElementStrokeThickness / 2, width / 2));

                            bool isStroked = pen != null;
                            const bool isSmoothJoin = true;

                            context.BeginFigure(startPoint, false, false);
                            context.ArcTo(new Point(height / 2, Settings.ElementStrokeThickness / 2),
                                new Size((height - Settings.ElementStrokeThickness) / 2, (width - Settings.ElementStrokeThickness) / 2),
                                0, true, (this.FlipGraphics ? SweepDirection.Counterclockwise : SweepDirection.Clockwise), isStroked, isSmoothJoin);
                            context.LineTo(new Point(height / 2, width / 2), isStroked, isSmoothJoin);
                            context.LineTo(this.FlipGraphics ? new Point(height, width / 2) : new Point(0, width / 2), isStroked, isSmoothJoin);

                            context.Close();
                        }
                    }
                    else if (this.DlawikType == DlawikTypes.Coil)
                    {
                        if (FlipGraphics)
                        {
                            if (this.ControlOrientation == Orientation.Horizontal)
                            {
                                var startPoint = new Point(width / 2, Settings.ElementStrokeThickness / 2);

                                bool isStroked = pen != null;
                                const bool isSmoothJoin = true;

                                context.BeginFigure(startPoint, false, false);
                                context.ArcTo(new Point(width / 2, height / 3),
                                    new Size((width - Settings.ElementStrokeThickness) / 2, (height / 3 - Settings.ElementStrokeThickness) / 2),
                                    180, true, SweepDirection.Clockwise, isStroked, isSmoothJoin);

                                context.ArcTo(new Point(width / 2, 2 * height / 3),
                                    new Size((width - Settings.ElementStrokeThickness) / 2, (height / 3 - Settings.ElementStrokeThickness) / 2),
                                    180, true, SweepDirection.Clockwise, isStroked, isSmoothJoin);

                                context.ArcTo(new Point(width / 2, height),
                                    new Size((width - Settings.ElementStrokeThickness) / 2, (height / 3 - Settings.ElementStrokeThickness) / 2),
                                    180, true, SweepDirection.Clockwise, isStroked, isSmoothJoin);

                                context.Close();
                            }
                            else
                            {
                                var startPoint = new Point(Settings.ElementStrokeThickness / 2, height / 2);

                                bool isStroked = pen != null;
                                const bool isSmoothJoin = true;

                                context.BeginFigure(startPoint, false, false);
                                context.ArcTo(new Point(width / 3, height / 2),
                                    new Size((width / 3 - Settings.ElementStrokeThickness) / 2, (height - Settings.ElementStrokeThickness) / 2),
                                    180, true, SweepDirection.Counterclockwise, isStroked, isSmoothJoin);

                                context.ArcTo(new Point(2 * width / 3, height / 2),
                                    new Size((width / 3 - Settings.ElementStrokeThickness) / 2, (height - Settings.ElementStrokeThickness) / 2),
                                    180, true, SweepDirection.Counterclockwise, isStroked, isSmoothJoin);

                                context.ArcTo(new Point(width, height / 2),
                                    new Size((width / 3 - Settings.ElementStrokeThickness) / 2, (height - Settings.ElementStrokeThickness) / 2),
                                    180, true, SweepDirection.Counterclockwise, isStroked, isSmoothJoin);

                                context.Close();
                            }
                        }
                        else
                        {
                            if (this.ControlOrientation == Orientation.Horizontal)
                            {
                                var startPoint = new Point(width / 2, Settings.ElementStrokeThickness / 2);

                                bool isStroked = pen != null;
                                const bool isSmoothJoin = true;

                                context.BeginFigure(startPoint, false, false);
                                context.ArcTo(new Point(width / 2, height / 3),
                                    new Size((width - Settings.ElementStrokeThickness) / 2, (height / 3 - Settings.ElementStrokeThickness) / 2),
                                    180, true, SweepDirection.Counterclockwise, isStroked, isSmoothJoin);

                                context.ArcTo(new Point(width / 2, 2 * height / 3),
                                    new Size((width - Settings.ElementStrokeThickness) / 2, (height / 3 - Settings.ElementStrokeThickness) / 2),
                                    180, true, SweepDirection.Counterclockwise, isStroked, isSmoothJoin);

                                context.ArcTo(new Point(width / 2, height),
                                    new Size((width - Settings.ElementStrokeThickness) / 2, (height / 3 - Settings.ElementStrokeThickness) / 2),
                                    180, true, SweepDirection.Counterclockwise, isStroked, isSmoothJoin);

                                context.Close();
                            }
                            else
                            {
                                var startPoint = new Point(Settings.ElementStrokeThickness / 2, height / 2);

                                bool isStroked = pen != null;
                                const bool isSmoothJoin = true;

                                context.BeginFigure(startPoint, false, false);
                                context.ArcTo(new Point(width / 3, height / 2),
                                    new Size((width / 3 - Settings.ElementStrokeThickness) / 2, (height - Settings.ElementStrokeThickness) / 2),
                                    180, true, SweepDirection.Clockwise, isStroked, isSmoothJoin);

                                context.ArcTo(new Point(2 * width / 3, height / 2),
                                    new Size((width / 3 - Settings.ElementStrokeThickness) / 2, (height - Settings.ElementStrokeThickness) / 2),
                                    180, true, SweepDirection.Clockwise, isStroked, isSmoothJoin);

                                context.ArcTo(new Point(width, height / 2),
                                    new Size((width / 3 - Settings.ElementStrokeThickness) / 2, (height - Settings.ElementStrokeThickness) / 2),
                                    180, true, SweepDirection.Clockwise, isStroked, isSmoothJoin);

                                context.Close();
                            }
                        }
                    }
                }

                drawingContext.DrawGeometry(null, pen, geometry);

                //Canvas shapeCanvas = new Canvas { Width = width, Height = height };

            }
        }

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();
            if (this.ControlOrientation == Orientation.Vertical)
            {
                OutputPointsCollection.Add(new Point(0, this.Height / 2.0));
                OutputPointsCollection.Add(new Point(this.Width, this.Height / 2.0));
            }
            else
            {
                OutputPointsCollection.Add(new Point(this.Width / 2.0, 0));
                OutputPointsCollection.Add(new Point(this.Width / 2.0, this.Height));
            }
        }

        static Dlawik()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Dlawik), new FrameworkPropertyMetadata(typeof(Dlawik)));
        }
    }
}
