﻿using Newtonsoft.Json;
using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, true, "Bezpiecznik")]
    public class Fuse : PassiveElement
    {
        public new bool FlipGraphics { get { return base.FlipGraphics; } set { base.FlipGraphics = value; } }

        protected override double WidthToHeightControlRatio { get => 2.0; }

        protected override double DefaultWidth { get => 38.0;  }

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();
            if (ControlOrientation == Orientation.Horizontal)
            {
                OutputPointsCollection.Add(new Point(0, this.Height / 2.0));
                OutputPointsCollection.Add(new Point(this.Width, this.Height / 2.0));
            }
            else
            {
                OutputPointsCollection.Add(new Point(this.Width / 2.0, 0));
                OutputPointsCollection.Add(new Point(this.Width / 2.0, this.Height));
            }
        }

        protected override void DrawControl(DrawingContext drawingContext)
        {
            this.DrawShape(drawingContext);
        }

        public void DrawShape(DrawingContext drawingContext)
        {
            if (drawingContext != null)
            {
                var width = this.ActualWidth;
                var height = this.ActualHeight;

                var brush = Settings.DefaultLineColor;
                var secondBrush = Settings.DefaultLineColor;

                Point p2, p3, p4, p5;
                Rect rect; CornerRadius cornerRadius;

                if (ControlOrientation == Orientation.Vertical)
                {
                    // Rectangle point and size
                    rect = new Rect(new Point(width / 4.0, 0), new Size(width / 2.0, height));
                    cornerRadius = new CornerRadius(width / 4.0);
                    // First line points
                    p2 = new Point(0, 5.0 * height / 6.0); p3 = new Point(width, height / 6.0);
                    // Second line points
                    p4 = new Point(width / 2.0, 0); p5 = new Point(width / 2.0, height);
                }
                else
                {
                    // Rectangle point and size
                    rect = new Rect(new Point(0, height / 4.0), new Size(width, height / 2.0));
                    cornerRadius = new CornerRadius(height / 4.0);
                    // First line points
                    p2 = new Point(5.0 * width / 6.0, 0); p3 = new Point(width / 6.0, height);
                    // Second line points
                    p4 = new Point(0, height / 2.0); p5 = new Point(width, height / 2.0);
                }

                if (SourceBrushes != null && SourceBrushes.Count > 0)
                {
                    brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, width);
                    secondBrush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, p4, p5);
                }

                var pen = new Pen(brush, Settings.ElementStrokeThickness);
                var secondPen = new Pen(secondBrush, Settings.LinesStrokeThickness);
                pen.Freeze(); secondPen.Freeze();

                DrawingContextShapes.DrawRoundedRectangle(drawingContext, null, pen, rect, cornerRadius);
                drawingContext.DrawLine(pen, p2, p3);
                drawingContext.DrawLine(secondPen, p4, p5);
            }
        }

        static Fuse()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Fuse), new FrameworkPropertyMetadata(typeof(Fuse)));
        }
    }
}
