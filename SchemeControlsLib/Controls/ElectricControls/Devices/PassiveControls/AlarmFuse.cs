﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, true, "Bezpiecznik alarmowy")]
    public class AlarmFuse : PassiveElement
    {
        public new bool FlipGraphics { get { return base.FlipGraphics; } set { base.FlipGraphics = value; } }

        #region FuseFault
        #region Phase1FuseFault
        [EditorProperty("Zmienna błedu fazy 1")]
        public object Phase1FuseFault
        {
            get { return (object)GetValue(Phase1FuseFaultProperty); }
            set { SetValue(Phase1FuseFaultProperty, value); }
        }

        public static readonly DependencyProperty Phase1FuseFaultProperty =
            DependencyProperty.Register("Phase1FuseFault", typeof(object), typeof(AlarmFuse), new PropertyMetadata(null, Phase1FuseFault_PropertyChanged));

        private static void Phase1FuseFault_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is AlarmFuse alarmFuse)
            {
                alarmFuse.Phase1FuseFaultVisibility = alarmFuse.GetFaultIconVisibility(e.NewValue);
            }
        }

        private Visibility _Phase1FuseFaultVisibility = Visibility.Collapsed;
        public Visibility Phase1FuseFaultVisibility
        {
            get { return _Phase1FuseFaultVisibility; }
            set { _Phase1FuseFaultVisibility = value; NotifyPropertyChanged("Phase1FuseFaultVisibility"); }
        }
        #endregion Phase1FuseFault

        #region Phase2FuseFault
        [EditorProperty("Zmienna błedu fazy 2")]
        public object Phase2FuseFault
        {
            get { return (object)GetValue(Phase2FuseFaultProperty); }
            set { SetValue(Phase2FuseFaultProperty, value); }
        }

        public static readonly DependencyProperty Phase2FuseFaultProperty =
            DependencyProperty.Register("Phase2FuseFault", typeof(object), typeof(AlarmFuse), new PropertyMetadata(null, Phase2FuseFault_PropertyChanged));

        private static void Phase2FuseFault_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is AlarmFuse alarmFuse)
            {
                alarmFuse.Phase2FuseFaultVisibility = alarmFuse.GetFaultIconVisibility(e.NewValue);
            }
        }

        private Visibility _Phase2FuseFaultVisibility = Visibility.Collapsed;
        public Visibility Phase2FuseFaultVisibility
        {
            get { return _Phase2FuseFaultVisibility; }
            set { _Phase2FuseFaultVisibility = value; NotifyPropertyChanged("Phase2FuseFaultVisibility"); }
        }
        #endregion Phase2FuseFault

        #region Phase3FuseFault
        [EditorProperty("Zmienna błedu fazy 3")]
        public object Phase3FuseFault
        {
            get { return (object)GetValue(Phase3FuseFaultProperty); }
            set { SetValue(Phase3FuseFaultProperty, value); }
        }

        public static readonly DependencyProperty Phase3FuseFaultProperty =
            DependencyProperty.Register("Phase3FuseFault", typeof(object), typeof(AlarmFuse), new PropertyMetadata(null, Phase3FuseFault_PropertyChanged));

        private static void Phase3FuseFault_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is AlarmFuse alarmFuse)
            {
                alarmFuse.Phase3FuseFaultVisibility = alarmFuse.GetFaultIconVisibility(e.NewValue);
            }
        }

        private Visibility _Phase3FuseFaultVisibility = Visibility.Collapsed;
        public Visibility Phase3FuseFaultVisibility
        {
            get { return _Phase3FuseFaultVisibility; }
            set { _Phase3FuseFaultVisibility = value; NotifyPropertyChanged("Phase3FuseFaultVisibility"); }
        }
        #endregion Phase3FuseFault

        public Visibility GetFaultIconVisibility(object value)
        {
            if(bool.TryParse(value?.ToString(), out bool bValue))
            {
                if (bValue)
                    return Visibility.Visible;
                else
                    return Visibility.Collapsed;
            }

            return Visibility.Collapsed;
        }
        #endregion FuseFault

        protected override double WidthToHeightControlRatio { get => 2.5; }

        protected override double DefaultWidth { get => 31.0; }

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();
            if (ControlOrientation == Orientation.Horizontal)
            {
                OutputPointsCollection.Add(new Point(0, this.Height / 2.0));
                OutputPointsCollection.Add(new Point(this.Width, this.Height / 2.0));
            }
            else
            {
                OutputPointsCollection.Add(new Point(this.Width / 2.0, 0));
                OutputPointsCollection.Add(new Point(this.Width / 2.0, this.Height));
            }
        }

        protected override void DrawControl(DrawingContext drawingContext)
        {
            this.DrawShape(drawingContext);
        }

        public void DrawShape(DrawingContext drawingContext)
        {
            if (drawingContext != null)
            {
                var width = this.ActualWidth;
                var height = this.ActualHeight;

                var brush = Settings.DefaultLineColor;
                var secondBrush = Settings.DefaultLineColor;

                Point p2, p3, p4, p5, p6, p7;
                Rect rect; CornerRadius cornerRadius;

                if (ControlOrientation == Orientation.Vertical)
                {
                    // Rectangle point and size
                    rect = new Rect(new Point(0, 2 * Settings.ElementStrokeThickness), new Size(width, height - 4 * Settings.ElementStrokeThickness));
                    // First line points
                    p2 = new Point(width / 2.0, 2 * Settings.ElementStrokeThickness); p3 = new Point(width / 2.0, height - 2 * Settings.ElementStrokeThickness);

                    p4 = new Point(0, 0); p5 = new Point(width, 0);

                    p6 = new Point(0, height); p7 = new Point(width, height);
                }
                else
                {
                    // Rectangle point and size
                    rect = new Rect(new Point(2 * Settings.ElementStrokeThickness, 0), new Size(width - 4 * Settings.ElementStrokeThickness, height));
                    // First line points
                    p2 = new Point(2 * Settings.ElementStrokeThickness, height / 2.0); p3 = new Point(width - 2 * Settings.ElementStrokeThickness, height / 2.0);

                    p4 = new Point(0, 0); p5 = new Point(0, height);

                    p6 = new Point(width, 0); p7 = new Point(width, height);
                }

                if (SourceBrushes != null && SourceBrushes.Count > 0)
                {
                    brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, width);
                    secondBrush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, p2, p3);
                }

                var pen = new Pen(brush, Settings.ElementStrokeThickness);
                var secondPen = new Pen(secondBrush, Settings.LinesStrokeThickness);
                pen.Freeze(); secondPen.Freeze();

                //DrawingContextShapes.DrawRoundedRectangle(drawingContext, null, pen, rect, cornerRadius);
                drawingContext.DrawRectangle(null, pen, rect);
                drawingContext.DrawLine(secondPen, p2, p3);
                drawingContext.DrawLine(secondPen, p4, p5);
                drawingContext.DrawLine(secondPen, p6, p7);
            }
        }

        static AlarmFuse()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(AlarmFuse), new FrameworkPropertyMetadata(typeof(AlarmFuse)));
        }

    }
}
