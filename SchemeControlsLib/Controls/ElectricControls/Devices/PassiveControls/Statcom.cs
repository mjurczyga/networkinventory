﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, true, "Statcom")]
    public class Statcom : PassiveElement
    {
        public new bool FlipGraphics { get { return base.FlipGraphics; } set { base.FlipGraphics = value; } }

        protected override double WidthToHeightControlRatio => 1.0001;

        protected override double DefaultWidth => 29;

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();
            if (ControlOrientation == Orientation.Horizontal)
            {
                OutputPointsCollection.Add(new Point(0, this.Height / 2.0));
                OutputPointsCollection.Add(new Point(this.Width, this.Height / 2.0));
            }
            else
            {
                OutputPointsCollection.Add(new Point(this.Width / 2.0, 0));
                OutputPointsCollection.Add(new Point(this.Width / 2.0, this.Height));
            }
        }

        protected override void DrawControl(DrawingContext drawingContext)
        {
            this.DrawShape(drawingContext);
        }

        private void DrawShape(DrawingContext drawingContext)
        {
            if (drawingContext != null)
            {
                var width = this.ActualWidth;
                var height = this.ActualHeight;
                Brush brush = Settings.DefaultLineColor;

                if (SourceBrushes != null && SourceBrushes.Count > 0)
                {
                    brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, width, true);
                }

                var pen = new Pen(brush, Settings.LinesStrokeThickness);
                pen.Freeze();

                double halfWidth = Math.Round(this.Width / 2.0);
                double halfHeignt = Math.Round(this.Height / 2.0);
                double oneUnit = width / 10;

                if (ControlOrientation == Orientation.Horizontal)
                {
                    drawingContext.DrawLine(pen, new Point(0, 0), new Point(0, height));
                    drawingContext.DrawLine(pen, new Point(0, height), new Point(width + Settings.ElementStrokeThickness / 2.0, height));
                    drawingContext.DrawLine(pen, new Point(width, height), new Point(width, 0.0));
                    drawingContext.DrawLine(pen, new Point(width, 0.0), new Point(-Settings.ElementStrokeThickness / 2.0, 0.0));


                    #region Układ Tyrystorów
                    var a = 8 * oneUnit / Math.Sqrt(3);
                    var halfA = a / 2.0;

                    var geometry = new StreamGeometry();
                    using (var context = geometry.Open())
                    {

                        context.BeginFigure(new Point(oneUnit, halfHeignt), brush != null, false);
                        context.LineTo(new Point(oneUnit * 3, halfHeignt), true, true);
                        context.LineTo(new Point(oneUnit * 3, halfHeignt - halfA), true, true);
                        context.LineTo(new Point(oneUnit * 7, halfHeignt), true, true);
                        context.LineTo(new Point(oneUnit * 3, halfHeignt + halfA), true, true);
                        context.LineTo(new Point(oneUnit * 3, halfHeignt), true, true);

                        context.LineTo(new Point(oneUnit * 7, halfHeignt - halfA), false, true);
                        context.LineTo(new Point(oneUnit * 7, halfHeignt + halfA), true, true);
                        context.LineTo(new Point(oneUnit * 7, halfHeignt), false, true);

                        context.LineTo(new Point(oneUnit * 8, halfHeignt + halfA), true, true);
                        context.LineTo(new Point(oneUnit * 7, halfHeignt), false, true);
                        context.LineTo(new Point(oneUnit * 9, halfHeignt), true, true);
                        context.Close();
                        drawingContext.DrawGeometry(null, pen, geometry);
                    }
                    #endregion Układ Tyrystorów
                }
                else
                {
                    drawingContext.DrawLine(pen, new Point(0, 0), new Point(0, height));
                    drawingContext.DrawLine(pen, new Point(-Settings.ElementStrokeThickness / 2.0, height), new Point(width + Settings.ElementStrokeThickness / 2.0, height));
                    drawingContext.DrawLine(pen, new Point(width, height), new Point(width, 0.0));
                    drawingContext.DrawLine(pen, new Point(width + Settings.ElementStrokeThickness / 2.0, 0.0), new Point(-Settings.ElementStrokeThickness / 2.0, 0.0));


                    #region Układ Tyrystorów
                    var a = 8 * oneUnit / Math.Sqrt(3);
                    var halfA = a / 2.0;

                    var geometry = new StreamGeometry();
                    using (var context = geometry.Open())
                    {

                        context.BeginFigure(new Point(halfWidth, oneUnit), brush != null, false);
                        context.LineTo(new Point(halfWidth, oneUnit * 3), true, true);
                        context.LineTo(new Point(halfWidth - halfA, oneUnit * 3), true, true);
                        context.LineTo(new Point(halfWidth, oneUnit * 7), true, true);
                        context.LineTo(new Point(halfWidth + halfA, oneUnit * 3), true, true);
                        context.LineTo(new Point(halfWidth, oneUnit * 3), true, true);

                        context.LineTo(new Point(halfWidth - halfA, oneUnit * 7), false, true);
                        context.LineTo(new Point(halfWidth + halfA, oneUnit * 7), true, true);
                        context.LineTo(new Point(halfWidth, oneUnit * 7), false, true);

                        context.LineTo(new Point(halfWidth + halfA, oneUnit * 8), true, true);
                        context.LineTo(new Point(halfWidth, oneUnit * 7), false, true);
                        context.LineTo(new Point(halfWidth, oneUnit * 9), true, true);
                        context.Close();
                        drawingContext.DrawGeometry(null, pen, geometry);
                    }
                    #endregion Układ Tyrystorów

                }
            }
        }

        static Statcom()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Statcom), new FrameworkPropertyMetadata(typeof(Statcom)));
        }
    }
}
