﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, true, "Sieć")]
    public sealed class LineElement : PassiveElement
    {
        public new bool FlipGraphics { get { return base.FlipGraphics; } set { base.FlipGraphics = value; } }

        #region dependency properties
        [EditorProperty("Grubość lini")]
        public double StrokeThickness
        {
            get { return (double)GetValue(StrokeThicknessProperty); }
            set { SetValue(StrokeThicknessProperty, value); }
        }
        public static readonly DependencyProperty StrokeThicknessProperty =
            DependencyProperty.Register("StrokeThickness", typeof(double), typeof(LineElement), new PropertyMetadata(Settings.LinesStrokeThickness, StrokeThickness_PropertyChanged));

        private static void StrokeThickness_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            LineElement lineElement = (LineElement)d;
            lineElement.InvalidateVisual();
        }

        [EditorProperty("Podwójna linia")]
        public bool DoubleLine
        {
            get { return (bool)GetValue(DoubleLineProperty); }
            set { SetValue(DoubleLineProperty, value); }
        }

        public static readonly DependencyProperty DoubleLineProperty =
            DependencyProperty.Register("DoubleLine", typeof(bool), typeof(LineElement), new PropertyMetadata(false, DoubleLine_PropertyChanged));

        private static void DoubleLine_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            LineElement lineElement = (LineElement)d;
            lineElement.InvalidateVisual();
        }
        #endregion

        protected override double WidthToHeightControlRatio => 1;

        protected override double DefaultWidth => 25;

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();
            if (this.ControlOrientation == System.Windows.Controls.Orientation.Horizontal)
            {
                this.OutputPointsCollection.Add(new Point(this.Width / 2.0, 0));
            }
            else
            {
                this.OutputPointsCollection.Add(new Point(0, this.Height / 2.0));
            }
        }

        protected override void DrawControl(DrawingContext drawingContext)
        {
            double width = this.Width; double height = this.Height;
            // kolor kontrolki
            var brush = Settings.DefaultLineColor;
            if (SourceBrushes != null && SourceBrushes.Count > 0)
            {
                brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, width, true);
            }
            var pen = new Pen(brush, this.StrokeThickness);

            if (drawingContext != null)
            {
                if (!this.DoubleLine)
                {
                    if (ControlOrientation == Orientation.Horizontal)
                    {
                        GuidelineSet guidelines = new GuidelineSet();
                        guidelines.GuidelinesX.Add(0);
                        guidelines.GuidelinesY.Add(0);
                        guidelines.GuidelinesX.Add(width);
                        guidelines.GuidelinesY.Add(0);
                        drawingContext.PushGuidelineSet(guidelines);
                        drawingContext.DrawLine(pen, new Point(0, 0), new Point(width, 0));
                    }
                    else
                    {
                        GuidelineSet guidelines = new GuidelineSet();
                        guidelines.GuidelinesX.Add(0);
                        guidelines.GuidelinesY.Add(0);
                        guidelines.GuidelinesX.Add(0);
                        guidelines.GuidelinesY.Add(height);
                        drawingContext.PushGuidelineSet(guidelines);
                        drawingContext.DrawLine(pen, new Point(0, 0), new Point(0, height));
                    }
                }
                else
                {
                    if (ControlOrientation == Orientation.Horizontal)
                    {
                        GuidelineSet guidelines = new GuidelineSet();
                        guidelines.GuidelinesX.Add(0);
                        guidelines.GuidelinesY.Add(0);
                        guidelines.GuidelinesX.Add(width);
                        guidelines.GuidelinesY.Add(0);
                        drawingContext.PushGuidelineSet(guidelines);
                        drawingContext.DrawLine(pen, new Point(0, 0), new Point(width, 0));


                        GuidelineSet guidelines1 = new GuidelineSet();
                        guidelines1.GuidelinesX.Add(0);
                        guidelines1.GuidelinesY.Add(Settings.LinesStrokeThickness * 2);
                        guidelines1.GuidelinesX.Add(width);
                        guidelines1.GuidelinesY.Add(Settings.LinesStrokeThickness * 2);
                        drawingContext.PushGuidelineSet(guidelines1);
                        drawingContext.DrawLine(pen, new Point(0, Settings.LinesStrokeThickness * 2), new Point(width, Settings.LinesStrokeThickness * 2));
                    }
                    else
                    {
                        GuidelineSet guidelines = new GuidelineSet();
                        guidelines.GuidelinesX.Add(0);
                        guidelines.GuidelinesY.Add(0);
                        guidelines.GuidelinesX.Add(0);
                        guidelines.GuidelinesY.Add(height);
                        drawingContext.PushGuidelineSet(guidelines);
                        drawingContext.DrawLine(pen, new Point(0, 0), new Point(0, height));

                        GuidelineSet guidelines1 = new GuidelineSet();
                        guidelines1.GuidelinesX.Add(Settings.LinesStrokeThickness * 2);
                        guidelines1.GuidelinesY.Add(0);
                        guidelines1.GuidelinesX.Add(Settings.LinesStrokeThickness * 2);
                        guidelines1.GuidelinesY.Add(height);
                        drawingContext.PushGuidelineSet(guidelines1);
                        drawingContext.DrawLine(pen, new Point(Settings.LinesStrokeThickness * 2, 0), new Point(Settings.LinesStrokeThickness * 2, height));
                    }
                }
            }
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            if (ControlOrientation == Orientation.Vertical && ((sizeInfo.HeightChanged && sizeInfo.NewSize.Height != this.StrokeThickness) || (sizeInfo.WidthChanged && sizeInfo.NewSize.Width != this.StrokeThickness)))
            {
                this.Height = sizeInfo.NewSize.Height;
                this.Width = this.StrokeThickness;
            }

            else if (ControlOrientation == Orientation.Horizontal && ((sizeInfo.WidthChanged && sizeInfo.NewSize.Width != this.StrokeThickness) || (sizeInfo.HeightChanged && sizeInfo.NewSize.Height != this.StrokeThickness)))
            {
                this.Width = sizeInfo.NewSize.Width;
                this.Height = this.StrokeThickness;
            }

            this.ComputeAndAddOutputs();
        }

        public LineElement() : base()
        {
            this.Height = this.StrokeThickness;
        }

    }
}
