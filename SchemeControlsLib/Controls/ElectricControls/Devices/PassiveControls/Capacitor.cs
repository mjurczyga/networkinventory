﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, true, "Kondensator")]
    public sealed class Capacitor : PassiveElement
    {
        public new bool FlipGraphics { get { return base.FlipGraphics; } set { base.FlipGraphics = value; } }

        protected override double WidthToHeightControlRatio => 1.1;

        protected override double DefaultWidth => 29;

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();
            if (ControlOrientation == Orientation.Horizontal)
            {
                OutputPointsCollection.Add(new Point(0, this.Height / 2.0));
                OutputPointsCollection.Add(new Point(this.Width, this.Height / 2.0));
            }
            else
            {
                OutputPointsCollection.Add(new Point(this.Width / 2.0, 0));
                OutputPointsCollection.Add(new Point(this.Width / 2.0, this.Height));
            }
        }

        protected override void DrawControl(DrawingContext drawingContext)
        {
            this.DrawShape(drawingContext);
        }

        private void DrawShape(DrawingContext drawingContext)
        {
            if (drawingContext != null)
            {
                var width = this.ActualWidth;
                var height = this.ActualHeight;
                Brush brush = Settings.DefaultLineColor;

                if (SourceBrushes != null && SourceBrushes.Count > 0)
                {
                    brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, width, true);
                }

                var pen = new Pen(brush, Settings.LinesStrokeThickness);
                pen.Freeze();

                double halfWidth = this.Width / 2.0;
                double halfHeignt = this.Height / 2.0;
               
                if (ControlOrientation == Orientation.Horizontal)
                {
                    drawingContext.DrawLine(pen,
                        new Point(0, halfHeignt),
                        new Point(halfWidth - Settings.LinesStrokeThickness * 2, halfHeignt));

                    drawingContext.DrawLine(pen,
                        new Point(halfWidth - Settings.LinesStrokeThickness * 2, 0.0),
                        new Point(halfWidth - Settings.LinesStrokeThickness * 2, height));


                    drawingContext.DrawLine(pen,
                        new Point(halfWidth + Settings.LinesStrokeThickness * 2, 0.0),
                        new Point(halfWidth + Settings.LinesStrokeThickness * 2, height));

                    drawingContext.DrawLine(pen,
                        new Point(halfWidth + Settings.LinesStrokeThickness * 2, halfHeignt),
                        new Point(width, halfHeignt));
                }
                else
                {
                    drawingContext.DrawLine(pen,
                        new Point(halfWidth, 0.0),
                        new Point(halfWidth, halfHeignt - Settings.LinesStrokeThickness * 2));

                    drawingContext.DrawLine(pen,
                        new Point(0.0, halfHeignt - Settings.LinesStrokeThickness * 2),
                        new Point(width, halfHeignt - Settings.LinesStrokeThickness * 2));


                    drawingContext.DrawLine(pen,
                        new Point(halfWidth, halfHeignt + Settings.LinesStrokeThickness * 2),
                        new Point(halfWidth, height));

                    drawingContext.DrawLine(pen,
                        new Point(0.0, halfHeignt + Settings.LinesStrokeThickness * 2),
                        new Point(width, halfHeignt + Settings.LinesStrokeThickness * 2));
                }
            }
        }

        static Capacitor()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Capacitor), new FrameworkPropertyMetadata(typeof(Capacitor)));
        }
    }
}
