﻿using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Interfaces.ElementsInterfaces
{
    public interface IDrawLacznik
    {
        void DrawSwitchBackground(DrawingContext drawingContext, double width, double height, Orientation controlOrientation, Pen pen, Brush stateBackgroundBrush, CornerRadius cornerRadius, SwitchPlacement switchPlacement, Thickness margin, Thickness backgroundMargin, double lineDistanceRatio = 6.0);
        void DrawCloseSwitch(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, Thickness margin);
        void DrawOpenSwitch(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, Thickness margin);
        void DrawQuestionMark(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, Thickness margin);
        void DrawSwitchType(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, SwitchTypes switchType, Thickness margin);

    }
}
