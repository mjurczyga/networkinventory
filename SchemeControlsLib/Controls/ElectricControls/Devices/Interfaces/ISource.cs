﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Interfaces
{
    public interface ISource
    {
        string SourceDescription { get; }

        int SourceNumber { get; }

        Brush SourceBrush { get; }

        string Name { get; }
    }
}
