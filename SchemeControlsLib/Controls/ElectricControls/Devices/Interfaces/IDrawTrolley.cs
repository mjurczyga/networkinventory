﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Interfaces.ElementsInterfaces
{
    public interface IDrawTrolley
    {
        void DrawTrolleyBackground(DrawingContext drawingContext, double width, double height, Brush stateBackgroundBrush, CornerRadius cornerRadius);
        void DrawTrolleyTop(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, double lineEndDistance = 8.0);
        void DrawTrolleyBottom(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, double lineEndDistance = 8.0);
        void DrawTrolley(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, Thickness margin);
    }
}
