﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.Interfaces.ElementsInterfaces;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, true, "Sekcja na wózku")]
    class SectionOnTrolley : ActiveElement, ILacznik, ITrolley, IPLCControl
    {
        public new Orientation ControlOrientation { get { return base.ControlOrientation; } set { base.ControlOrientation = value; } }

        public string PLCStructName => "ST_CONNECTOR";

        #region private fields
        private LacznikBase _lB = new LacznikBase();
        private TrolleyBase _tB = new TrolleyBase();
        #endregion

        #region Dependency properties
        #region trolley
        private int _InternalTrolleyState;

        public object TrolleyState
        {
            get { return (object)GetValue(TrolleyStateProperty); }
            set { SetValue(TrolleyStateProperty, value); }
        }

        public static readonly DependencyProperty TrolleyStateProperty =
            DependencyProperty.Register("TrolleyState", typeof(object), typeof(SectionOnTrolley), new PropertyMetadata(0, TrolleyState_PropertyChanged));

        private static void TrolleyState_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SectionOnTrolley trolleySwitch = (SectionOnTrolley)d;
            if (int.TryParse(e.NewValue?.ToString(), out int newIntVal))
            {
                trolleySwitch._InternalTrolleyState = newIntVal;
                trolleySwitch.UpdateControlState();
            }
            else if (bool.TryParse(e.NewValue?.ToString(), out bool newBoolVal))
            {
                int newState = 0;
                if (newBoolVal)
                    newState = 2;
                else
                    newState = 1;

                trolleySwitch._InternalTrolleyState = newState;
                trolleySwitch.State = newState;
                trolleySwitch.UpdateControlState();
            }
            else
            {
                trolleySwitch._InternalTrolleyState = 0;
                trolleySwitch.UpdateControlState();
            }
        }

        public string TrolleyStateBindingVarName
        {
            get { return (string)GetValue(TrolleyStateBindingVarNameProperty); }
            set { SetValue(TrolleyStateBindingVarNameProperty, value); }
        }

        public static readonly DependencyProperty TrolleyStateBindingVarNameProperty =
            DependencyProperty.Register("TrolleyStateBindingVarName", typeof(string), typeof(SectionOnTrolley));
        #endregion

        #region switch
        private int _InternalSwitchState;

        public object SwitchState
        {
            get { return (object)GetValue(SwitchStateProperty); }
            set { SetValue(SwitchStateProperty, value); }
        }

        public static readonly DependencyProperty SwitchStateProperty =
            DependencyProperty.Register("SwitchState", typeof(object), typeof(SectionOnTrolley), new PropertyMetadata(0, SwitchState_PropertyChanged));

        private static void SwitchState_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SectionOnTrolley trolleySwitch = (SectionOnTrolley)d;
            if(int.TryParse(e.NewValue?.ToString(), out int newIntVal))
            {
                trolleySwitch._InternalSwitchState = newIntVal;
                trolleySwitch.UpdateControlState();
            }
            else if (bool.TryParse(e.NewValue?.ToString(), out bool newBoolVal))
            {
                int newState = 0;
                if (newBoolVal)
                    newState = 2;
                else
                    newState = 1;

                trolleySwitch._InternalSwitchState = newState;
                trolleySwitch.State = newState;
                trolleySwitch.UpdateControlState();
            }
            else
            {
                trolleySwitch._InternalSwitchState = 0;
                trolleySwitch.UpdateControlState();
            }
        }

        public string SwitchStateBindingVarName
        {
            get { return (string)GetValue(SwitchStateBindingVarNameProperty); }
            set { SetValue(SwitchStateBindingVarNameProperty, value); }
        }

        public static readonly DependencyProperty SwitchStateBindingVarNameProperty =
            DependencyProperty.Register("SwitchStateBindingVarName", typeof(string), typeof(SectionOnTrolley));


        public SwitchTypes SwitchType
        {
            get { return (SwitchTypes)GetValue(SwitchTypeProperty); }
            set { SetValue(SwitchTypeProperty, value); }
        }

        public static readonly DependencyProperty SwitchTypeProperty =
            DependencyProperty.Register("SwitchType", typeof(SwitchTypes), typeof(SectionOnTrolley), new PropertyMetadata(SwitchTypes.Odlacznik, SwitchType_PropertyChanged));

        private static void SwitchType_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var element = (SectionOnTrolley)d;
            element.InvalidateVisual();
        }
        #endregion

        [EditorProperty("Zaokrąglenia", true)]
        public double CornerRadius
        {
            get { return (double)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(double), typeof(SectionOnTrolley), new PropertyMetadata(5.0, CornerRadius_PropertyChanged));

        private static void CornerRadius_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SectionOnTrolley sectionOnTrolley)
            {
                sectionOnTrolley.InvalidateVisual();
            }
        }

        #endregion

        protected override double WidthToHeightControlRatio => 1.0 / 3.0;

        protected override double DefaultWidth => 29.0;

        private double SwitchWidthToHeightRatio = 3.0 / 4.0;

        private double ElementWidthToHeightRatio = 1.0;

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();
            var offset = 1.0;
            if (ControlOrientation == Orientation.Vertical)
            {
                OutputPointsCollection.Add(new Point(-offset, this.Height / 2.0));
                OutputPointsCollection.Add(new Point(this.Width + offset, this.Height / 2.0));
            }
            else
            {
                OutputPointsCollection.Add(new Point(this.Width / 2.0, -offset));
                OutputPointsCollection.Add(new Point(this.Width / 2.0, this.Height + offset));
            }
        }

        #region private methods
        private void UpdateControlState()
        {
            int newState;
            if ((StateTypes)this._InternalTrolleyState == StateTypes.Closed && (StateTypes)this._InternalSwitchState == StateTypes.Closed)
                newState = (int)StateTypes.Closed;
            else
                newState = (int)StateTypes.Open;

            if (newState != this.State)
                this.State = newState;
            else
                this.InvalidateVisual();
        }
        #endregion

        #region Interfaces implementation
        #region przełacznik
        public void DrawSwitchBackground(DrawingContext drawingContext, double width, double height, Orientation controlOrientation, Pen pen, Brush stateBackgroundBrush, CornerRadius cornerRadius, SwitchPlacement switchPlacement, Thickness margin, Thickness backgroundMargin, double lineDistanceRatio = 10.0)
        {
            _lB.DrawSwitchBackground(drawingContext, width, height, controlOrientation, pen, stateBackgroundBrush, cornerRadius, switchPlacement, margin, backgroundMargin, lineDistanceRatio);
        }

        public void DrawCloseSwitch(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, Thickness margin)
        {
            _lB.DrawCloseSwitch(drawingContext, width, height, flippedGraphic, controlOrientation, pen, margin);
        }

        public void DrawOpenSwitch(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, Thickness margin)
        {
            _lB.DrawOpenSwitch(drawingContext, width, height, flippedGraphic, controlOrientation, pen, margin);
        }

        public void DrawQuestionMark(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, Thickness margin)
        {
            _lB.DrawQuestionMark(drawingContext, width, height, flippedGraphic, controlOrientation, pen, margin);
        }

        public void DrawSwitchType(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, SwitchTypes switchType, Thickness margin)
        {
            _lB.DrawSwitchType(drawingContext, width, height, flippedGraphic, controlOrientation, pen, switchType, margin);
        }
        #endregion

        #region wózek
        public void DrawTrolleyTop(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, double distance = 12.0)
        {
            _tB.DrawTrolleyTop(drawingContext, width, height, flippedGraphic, controlOrientation, pen, distance);
        }

        public void DrawTrolleyBottom(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, double distance = 12.0)
        {
            _tB.DrawTrolleyBottom(drawingContext, width, height, flippedGraphic, controlOrientation, pen, distance);
        }

        public void DrawTrolleyBackground(DrawingContext drawingContext, double width, double height, Brush stateBackgroundBrush, CornerRadius cornerRadius)
        {
            _tB.DrawTrolleyBackground(drawingContext, width, height, stateBackgroundBrush, cornerRadius);
        }

        public void DrawTrolley(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, Thickness margin)
        {
            _tB.DrawTrolley(drawingContext, width, height, flippedGraphic, controlOrientation, pen, margin);
        }
        #endregion
        #endregion

        protected override void DrawControl(DrawingContext drawingContext)
        {
            if (drawingContext != null)
            {
                var width = this.Width; var height = this.ActualHeight; Brush stateBackgroundBrush = Brushes.Black;

                Brush trolleyStateBackgroundBrush = StateToBrushConverter(this._InternalTrolleyState);
                double radius = ControlOrientation == Orientation.Horizontal ? width / 4.5 : height / 4.5;
                DrawTrolleyBackground(drawingContext, width, height, trolleyStateBackgroundBrush, new CornerRadius(CornerRadius));

                var pen = new Pen(Brushes.White, Settings.LinesStrokeThickness);
                var bigPen = new Pen(Brushes.White, Settings.ElementStrokeThickness);
                stateBackgroundBrush = StateToBrushConverter(this._InternalSwitchState);

                // Background
                double backgroundWidth; double backgroundHeight; Thickness backgroundMargin;

                // element
                double elementHeight = DefaultWidth * ElementWidthToHeightRatio; double elementWidth = DefaultWidth * ElementWidthToHeightRatio; Thickness elementMargin; Thickness elementBorderMargin;

                // wyliczenie położenia łącznika
                double switchHeight; double switchWidth; Thickness switchMargin; Thickness switchBorderMargin; double openTrolleyDistance = 0;
                if (ControlOrientation == Orientation.Horizontal)
                {
                    if ((StateTypes)this._InternalTrolleyState == StateTypes.Open)
                        openTrolleyDistance = width * 0.3;

                    // switch
                    switchWidth = width;
                    switchHeight = switchWidth / SwitchWidthToHeightRatio;
                    switchMargin = new Thickness(openTrolleyDistance, ((height - switchHeight - elementHeight) / 2.0) + elementHeight, 0, 0);
                    switchBorderMargin = new Thickness(0, (height - switchHeight - elementHeight) / 2.0, 0, 0);

                    //background
                    backgroundWidth = switchWidth;
                    backgroundHeight = switchHeight + elementHeight;
                    backgroundMargin = new Thickness(openTrolleyDistance, ((height - switchHeight - elementHeight) / 2.0), 0, 0);

                    //element 
                    elementMargin = new Thickness(openTrolleyDistance, height / 12.0 + backgroundMargin.Top, 0, 0);
                }
                else
                {
                    if ((StateTypes)this._InternalTrolleyState == StateTypes.Open)
                        openTrolleyDistance = height * 0.3;

                    // switch
                    switchHeight = height;
                    switchWidth = switchHeight / SwitchWidthToHeightRatio;
                    switchMargin = new Thickness(((width - switchWidth - elementWidth) / 2.0) + elementWidth, openTrolleyDistance, 0, 0);
                    switchBorderMargin = new Thickness((width - switchWidth - elementWidth) / 2.0, 0, 0, 0);

                    //background
                    backgroundWidth = switchWidth + elementWidth;
                    backgroundHeight = switchHeight;
                    backgroundMargin = new Thickness(((width - switchWidth - elementWidth) / 2.0) + 0, openTrolleyDistance, 0, 0);

                    //element 
                    elementMargin = new Thickness(width / 12.0 + backgroundMargin.Left, openTrolleyDistance, 0, 0);
                }

                // Lacznik
                DrawSwitchBackground(drawingContext, switchWidth, switchHeight + elementHeight, ControlOrientation, pen, stateBackgroundBrush, new CornerRadius(0), SwitchPlacement.Trolley, backgroundMargin, switchBorderMargin);

                switch (this._InternalSwitchState)
                {
                    case 0: DrawQuestionMark(drawingContext, switchWidth, switchHeight, FlipGraphics, ControlOrientation, bigPen, switchMargin); break;
                    case 1: DrawOpenSwitch(drawingContext, switchWidth, switchHeight, FlipGraphics, ControlOrientation, bigPen, switchMargin); break;
                    case 2: DrawCloseSwitch(drawingContext, switchWidth, switchHeight, FlipGraphics, ControlOrientation, bigPen, switchMargin); break;
                    default: break;
                }
                DrawSwitchType(drawingContext, switchWidth, switchHeight, FlipGraphics, ControlOrientation, pen, SwitchType, switchMargin);

                // element
                this.DrawFuse(drawingContext, elementWidth, elementHeight, FlipGraphics, ControlOrientation, bigPen, pen, elementMargin);

                // wózek
                DrawTrolleyTop(drawingContext, width, height, FlipGraphics, ControlOrientation, pen);
                DrawTrolleyBottom(drawingContext, width, height, FlipGraphics, ControlOrientation, pen);           
            }
        }

        private void DrawFuse(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen bigPen, Pen smallPen, Thickness margin)
        {
            Point p2, p3, p4, p5;
            Rect rect; CornerRadius cornerRadius;

            double horizontalPosition = margin.Top - margin.Bottom;
            double verticalPosiotion = margin.Left - margin.Right;

            if (drawingContext != null)
            {
                if (controlOrientation == Orientation.Vertical)
                {
                    // Rectangle point and size
                    rect = new Rect(new Point(verticalPosiotion, horizontalPosition + height / 3.0), new Size(2.5 * width / 3.0, height / 3.0));
                    cornerRadius = new CornerRadius(width / 6.0);
                    // First line points
                    p2 = new Point(verticalPosiotion, 5.0 * height / 6.0 + horizontalPosition); p3 = new Point(rect.Width + verticalPosiotion, height / 6.0 + horizontalPosition);
                    // Second line points
                    p4 = new Point(verticalPosiotion, height / 2.0 + horizontalPosition); p5 = new Point(width + verticalPosiotion, height / 2.0 + horizontalPosition);
                }
                else
                {
                    // Rectangle point and size
                    rect = new Rect(new Point(verticalPosiotion + width / 3.0, horizontalPosition), new Size(width / 3.0, 2.5 * height / 3.0));
                    cornerRadius = new CornerRadius(height / 6.0);
                    // First line points
                    p2 = new Point(5.0 * width / 6.0 + verticalPosiotion, horizontalPosition); p3 = new Point(width / 6.0 + verticalPosiotion, rect.Height + horizontalPosition );
                    // Second line points
                    p4 = new Point(width / 2.0 + verticalPosiotion, horizontalPosition); p5 = new Point(width / 2.0 + verticalPosiotion, height + horizontalPosition);
                }

                DrawingContextShapes.DrawRoundedRectangle(drawingContext, null, bigPen, rect, cornerRadius);
                drawingContext.DrawLine(bigPen, p2, p3);
                drawingContext.DrawLine(smallPen, p4, p5);
            }

        }


    }
}
