﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.Interfaces.ElementsInterfaces;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, true, "Łącznik")]
    public sealed class Lacznik : ActiveElement, ILacznik, IPLCControl
    {
        #region Inherited property
        //sztukowane żeby ukryć na schemacie
        [EditorProperty("Widoczność")]
        public new Visibility Visibility { get { return base.Visibility; } set { base.Visibility = value; } }
        #endregion Inherited property

        public string PLCStructName => "ST_CONNECTOR";

        private LacznikBase _lB = new LacznikBase();

        protected override double WidthToHeightControlRatio { get { return 3.0 / 4.0; } }

        protected override double DefaultWidth { get { return 29.0; } }

        #region dependency properties

        [EditorProperty("Zaokrąglenia", true)]
        public double CornerRadius
        {
            get { return (double)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(double), typeof(Lacznik), new PropertyMetadata(5.0, CornerRadius_PropertyChanged));

        private static void CornerRadius_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is Lacznik lacznik)
            {
                lacznik.InvalidateVisual();
            }
        }



        #region States
        private int _InternalSwitchState;

        [EditorProperty("Stan przełącznika")]
        public object SwitchState
        {
            get { return (object)GetValue(SwitchStateProperty); }
            set { SetValue(SwitchStateProperty, value); }
        }

        public static readonly DependencyProperty SwitchStateProperty =
            DependencyProperty.Register("SwitchState", typeof(object), typeof(Lacznik), new PropertyMetadata(0, SwitchState_PropertyChanged));

        private static void SwitchState_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Lacznik lacznik = (Lacznik)d;
            if (int.TryParse(e.NewValue?.ToString(), out int newIntVal))
            {
                lacznik._InternalSwitchState = newIntVal;
                lacznik.State = newIntVal;
            }
            else if (bool.TryParse(e.NewValue?.ToString(), out bool newBoolVal))
            {
                int newState = 0;
                if (newBoolVal)
                    newState = 2;
                else
                    newState = 1;

                lacznik._InternalSwitchState = newState;
                lacznik.State = newState;
            }
            else
            {
                lacznik._InternalSwitchState = 0;
                lacznik.State = 0;
            }
        }

        [EditorProperty("Nazwa zmiennej stanu przełącznika w wizualizacji")]
        public string SwitchStateBindingVarName
        {
            get { return (string)GetValue(SwitchStateBindingVarNameProperty); }
            set { SetValue(SwitchStateBindingVarNameProperty, value); }
        }

        public static readonly DependencyProperty SwitchStateBindingVarNameProperty =
            DependencyProperty.Register("SwitchStateBindingVarName", typeof(string), typeof(Lacznik));
        #endregion

        [EditorProperty("Typ przełacznika")]
        public SwitchTypes SwitchType
        {
            get { return (SwitchTypes)GetValue(SwitchTypeProperty); }
            set { SetValue(SwitchTypeProperty, value); }
        }

        public static readonly DependencyProperty SwitchTypeProperty =
            DependencyProperty.Register("SwitchType", typeof(SwitchTypes), typeof(Lacznik), new PropertyMetadata(SwitchTypes.Odlacznik, SwitchType_PropertyChanged));

        private static void SwitchType_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var element = (Lacznik)d;
            element.InvalidateVisual();
        }
        #endregion

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();
            var offset = 1.0;
            if (ControlOrientation == Orientation.Vertical)
            {
                OutputPointsCollection.Add(new Point(-offset, this.Height / 2.0));
                OutputPointsCollection.Add(new Point(this.Width + offset, this.Height / 2.0));
            }
            else
            {
                OutputPointsCollection.Add(new Point(this.Width / 2.0, -offset));
                OutputPointsCollection.Add(new Point(this.Width / 2.0, this.Height + offset));
            }
        }

        public void DrawShape(DrawingContext drawingContext)
        {
            if (drawingContext != null && Visibility == Visibility.Visible)
            {
                var width = this.ActualWidth; var height = this.ActualHeight; Brush stateBackgroundBrush = Brushes.Black;
                var pen = new Pen(Brushes.White, Settings.LinesStrokeThickness);
                var bigPen = new Pen(Brushes.White, Settings.ElementStrokeThickness);

                stateBackgroundBrush = StateToBrushConverter(this.State);

                Thickness switchMargin = new Thickness(0); //new CornerRadius(height / 6.0)
                DrawSwitchBackground(drawingContext, width, height, ControlOrientation, pen, stateBackgroundBrush, new CornerRadius(CornerRadius), SwitchPlacement.Switch, switchMargin, switchMargin);

                switch (this.State)
                {
                    case 0: DrawQuestionMark(drawingContext, width, height, FlipGraphics, ControlOrientation, bigPen, switchMargin); break;
                    case 1: DrawOpenSwitch(drawingContext, width, height, FlipGraphics, ControlOrientation, bigPen, switchMargin); break;
                    case 2: DrawCloseSwitch(drawingContext, width, height, FlipGraphics, ControlOrientation, bigPen, switchMargin); break;
                    default: break;
                }
                DrawSwitchType(drawingContext, width, height, FlipGraphics, ControlOrientation, pen, SwitchType, switchMargin);
            }
        }

        #region Interface implementation
        public void DrawSwitchBackground(DrawingContext drawingContext, double width, double height, Orientation controlOrientation, Pen pen, Brush stateBackgroundBrush, CornerRadius cornerRadius, SwitchPlacement switchPlacement, Thickness margin, Thickness backgroundMargin, double lineDistanceRatio = 6.0)
        {
            _lB.DrawSwitchBackground(drawingContext, width, height, controlOrientation, pen, stateBackgroundBrush, cornerRadius, switchPlacement, margin, backgroundMargin);
        }

        public void DrawCloseSwitch(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, Thickness margin)
        {
            _lB.DrawCloseSwitch(drawingContext, width, height, flippedGraphic, controlOrientation, pen, margin);
        }

        public void DrawOpenSwitch(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, Thickness margin)
        {
            _lB.DrawOpenSwitch(drawingContext, width, height, flippedGraphic, controlOrientation, pen, margin);
        }

        public void DrawQuestionMark(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, Thickness margin)
        {
            _lB.DrawQuestionMark(drawingContext, width, height, flippedGraphic, controlOrientation, pen, margin);
        }

        public void DrawSwitchType(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, SwitchTypes switchType, Thickness margin)
        {
            _lB.DrawSwitchType(drawingContext, width, height, flippedGraphic, controlOrientation, pen, switchType, margin);
        }
        #endregion

        protected override void DrawControl(DrawingContext drawingContext)
        {
            DrawShape(drawingContext);
        }        

        #region konstruktory
        static Lacznik()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Lacznik), new FrameworkPropertyMetadata(typeof(Lacznik)));
        }
        #endregion
    }
}
