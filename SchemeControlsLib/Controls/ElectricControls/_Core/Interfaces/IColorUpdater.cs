﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Interfaces
{
    public interface IMultiBrushLine
    {
        /// <summary>
        /// Source color brushes
        /// </summary>
        ObservableCollection<Brush> SourceBrushes { get; }

        /// <summary>
        /// metoda dodająca element do kolekcji z informowaniem o zmianie kolekcji
        /// </summary>
        /// <param name="brushToAdd">kolor do dodania</param>
        void AddBrushToSourceBrushes(Brush brushToAdd);

        /// <summary>
        /// metoda usuwajaca element do kolekcji z informowaniem o zmianie kolekcji
        /// </summary>
        /// <param name="brushToRemove">kolor do usunięcia</param>
        void RemoveBrushFromSourceBrushes(Brush brushToRemove);

        /// <summary>
        /// metoda czyszcząca kolekcje SourceBrushes
        /// </summary>
        void ClearSourceBrushes();
    }
}
