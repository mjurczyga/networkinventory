﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.Interfaces
{
    public interface IEditorElement
    {
        double Y_Position { get; set; }
        double X_Position { get; set; }
        int LayerNumber { get; set; }
        bool SetPosition(double x, double y);
    }
}
