﻿using SchemeControlsLib.Models.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.ElectricControls._Core.Interfaces
{
    public interface IOpenWindowEvent
    {
        string SteeringVarName { get; set; }
        string SteeringValue { get; set; }

        event SteeringControlEventDelegate SteeringValueEvent;
    }
}
