﻿using SchemeControlsLib.Controls.ElectricControls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SchemeControlsLib.Controls.Interfaces
{
    interface IElectricElementBase
    {
        Orientation ControlOrientation { get; set; }

        string Name { get; set; }
    }
}
