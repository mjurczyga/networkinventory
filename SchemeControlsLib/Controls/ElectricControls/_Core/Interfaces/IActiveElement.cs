﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.Interfaces
{
    public interface IActiveElement
    {
        int State { get; set; }


    }
}
