﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SchemeControlsLib.Controls.Interfaces
{
    interface IPasiveElement : IElectricElementBase
    {
        bool FlipGraphics { get; set; }
        bool IsCompensator { get; set; }


    }
}
