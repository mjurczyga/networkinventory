﻿using Newtonsoft.Json;
using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.ElectricControls;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.Inputs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Core
{
    public abstract class ElectricElementBase : GraphicsControl, ISelectableElement, IGroupSelectableElement, IMovableElement, IMovableSelectionElement
    {
        #region IsSelectionMoving
        private bool _IsSelectionMoving;
        public bool IsSelectionMoving
        {
            get { return _IsSelectionMoving; }
            set { _IsSelectionMoving = value; NotifyPropertyChanged("IsSelectionMoving"); }
        }
        #endregion IsSelectionMoving

        #region Events
        public event OutputNodesChangedDelegate OutputNodesChangedEventHandler;
        #endregion

        #region Dependency properties
        #region orientation
        [EditorProperty("Orientacja")]
        public Orientation ControlOrientation
        {
            get { return (Orientation)GetValue(ControlOrientationProperty); }
            set { SetValue(ControlOrientationProperty, value); }
        }

        public static readonly DependencyProperty ControlOrientationProperty =
            DependencyProperty.Register("ControlOrientation", typeof(Orientation), typeof(ElectricElementBase), new FrameworkPropertyMetadata(Orientation.Horizontal, ControlOrientation_PropertyChanged));

        private static void ControlOrientation_PropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs arg)
        {
            if (obj != null)
            {
                ElectricElementBase elementBase = (ElectricElementBase)obj;
                elementBase.ChangeWidthWithHeight();

            }
        }

        //[EditorProperty("Tryb edycji")]
        //public bool IsEditMode
        //{
        //    get { return (bool)GetValue(IsEditModeProperty); }
        //    set { SetValue(IsEditModeProperty, value); }
        //}

        //public static readonly DependencyProperty IsEditModeProperty =
        //    DependencyProperty.Register("IsEditMode", typeof(bool), typeof(ElectricElementBase), new PropertyMetadata(false, IsEditMode_PropertyChanged));

        //private static void IsEditMode_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        //{
        //    ElectricElementBase electricElementBase = (ElectricElementBase)d;
        //    electricElementBase.ChangeEditMode((bool)e.NewValue);
        //}
        #endregion
        #endregion

        #region Abstracts/Vitrual 
        #region protected fields
        /// /// <value>
        ///  Współczynnik szerokości do wysokości kontrolki (żeby zachowała prawidłowy kształt)
        /// </value>
        protected virtual double WidthToHeightControlRatio { get; set; } = 0.99;
        #endregion

        #region protected methods
        protected virtual void ComputeOutputs()
        {
            OutputPointsCollection.Clear();
        }

        protected override void FlipGraphicChanged(bool newValue)
        {
            base.FlipGraphicChanged(newValue);
            this.ComputeAndAddOutputs();
        }

        protected virtual void AdditionalMethodOnPositionChanged()
        {

        }
        #endregion
        #endregion

        #region private
        #region private fields       
        // ustawiony rozmiar kontrolki
        private Size _defaultSize;

        private ObservableCollection<Point> _OutputPositionInCanvasCollection = new ObservableCollection<Point>();
        public ObservableCollection<Point> OutputPositionInCanvasCollection { get { return _OutputPositionInCanvasCollection; } }
        #endregion

        #region private methods

        protected void ComputeAndAddOutputs()
        {
            this.ComputeOutputs();
            this.ComputeOutputsPositionInCanvas();
            this.AddOrUpdateOutputsToUI();
        }

        /// <summary>
        /// Ustawia domyślny rozimar kontrolki
        /// </summary>
        /// <param name="defaultSize"></param>
        private void SetDefaultWidthAndHeight(Size defaultSize)
        {
            _defaultSize = defaultSize;
        }

        /// <summary>
        /// Zamienia rozmiar w zależności od orientacji
        /// </summary>
        private void ChangeWidthWithHeight()
        {
            SetDefaultWidthAndHeight(new Size(this.Width, this.Height));

            if (!ValueChecker.CheckDoubleNanInfOrZero(_defaultSize.Height))
                this.Width = _defaultSize.Height;
            if (!ValueChecker.CheckDoubleNanInfOrZero(_defaultSize.Width))
                this.Height = _defaultSize.Width;

        }

        /// <summary>
        /// Dodaje wyjścia kontrolki do UI
        /// </summary>
        private void AddOrUpdateOutputsToUI()
        {
            if (OutputPositionInCanvasCollection != null && OutputNodesCollection.Count == 0 && this.Parent is SchemeCanvas)
            {
                var controlSchameCanvas = (SchemeCanvas)this.Parent;
                var oldOutputNodes = new List<OutputNode>(OutputNodesCollection);

                int i = 0;
                foreach (var outputPoint in OutputPositionInCanvasCollection)
                {
                    var outputNode = new OutputNode(this);
                    // Ustawienie pozycji wyjścia
                    outputNode.SetPosition(outputPoint.X - outputNode.OutputNodeSize.Width / 2.0, outputPoint.Y - outputNode.OutputNodeSize.Height / 2.0);

                    outputNode.SetValue(FrameworkElement.NameProperty, this.Name + "_OutputX" + i);
                    OutputNodesCollection.Add(outputNode);
                    controlSchameCanvas.Children.Add(outputNode);
                    //Uaktualnienie koloru wyjść jeśli właściciel wyjść ma inny niż biały
                    if (this.SourceBrushes.Count != 0 && !(this.SourceBrushes.Count == 1 && this.SourceBrushes.Contains(Settings.DefaultLineColor)))
                        outputNode.AddRangeToSourceBrushes(this.SourceBrushes);

                    i++;
                }

                if(OutputNodesChangedEventHandler != null)
                    OutputNodesChangedEventHandler(this, new OutputNodesChangedEventArgs(oldOutputNodes, new List<OutputNode>(OutputNodesCollection)));

            }
            else if (OutputNodesCollection.Count != 0 && OutputNodesCollection.Count >= OutputPositionInCanvasCollection.Count)
            {
                for (int i = 0; i < OutputNodesCollection.Count; i++)
                {

                    OutputNodesCollection[i].SetPosition(OutputPositionInCanvasCollection[i].X - OutputNodesCollection[i].OutputNodeSize.Width / 2, OutputPositionInCanvasCollection[i].Y - OutputNodesCollection[i].OutputNodeSize.Height / 2);
                }
            }
        }

        // TODO: do usunięcia?
        private Size ComputeDesiredSize(Size currentSize)
        {
            Size resultSize = new Size(currentSize.Width, currentSize.Height);
            if (ControlOrientation == Orientation.Horizontal)
            {
                //sprawdzenie czy została ustawiona właściwość Height
                if (!ValueChecker.CheckDoubleNanInfOrZero(this.Height))
                    resultSize.Width = currentSize.Height * WidthToHeightControlRatio;

                if (!ValueChecker.CheckDoubleNanInfOrZero(this.Width))
                    resultSize.Height = currentSize.Width / WidthToHeightControlRatio;
            }
            else
            {
                if (!ValueChecker.CheckDoubleNanInfOrZero(this.Height))
                    resultSize.Width = currentSize.Height / WidthToHeightControlRatio;
                if (!ValueChecker.CheckDoubleNanInfOrZero(this.Width))
                    resultSize.Height = currentSize.Width * WidthToHeightControlRatio;
            }
            return resultSize;
        }

        /// <summary>
        /// Metoda wywoływana gdy zmieni się pozycja kontrolki w SchemeCanvas
        /// </summary>
        protected override void PositionInCanvasChanged(object sender, EventArgs e)
        {
            var control = sender as ElectricElementBase;
            if(control != null)
            {
                control.ComputeOutputsPositionInCanvas();
                control.AddOrUpdateOutputsToUI();
                control.AdditionalMethodOnPositionChanged();
            }
        }

        protected override void ControlPosition_Changed(double oldX_Position, double oldY_Position)
        {
            base.ControlPosition_Changed(oldX_Position, oldY_Position);
            this.ComputeOutputsPositionInCanvas();
            this.AddOrUpdateOutputsToUI();
            this.AdditionalMethodOnPositionChanged();
        }

        private void ComputeOutputsPositionInCanvas()
        {
            if (this != null)
            {
                this.OutputPositionInCanvasCollection.Clear();
                this.ControlPosition.X = this.X_Position;
                this.ControlPosition.Y = this.Y_Position;
                if (this.OutputPointsCollection != null)
                {
                    this.OutputPositionInCanvasCollection.Clear();
                    foreach (var outputControlPosition in this.OutputPointsCollection)
                        this.OutputPositionInCanvasCollection.Add(new Point(this.ControlPosition.X + outputControlPosition.X, this.ControlPosition.Y + outputControlPosition.Y));
                }
            }
        }

        /// <summary>
        /// actions for changed Edit mode
        /// </summary>
        /// <param name="isEditMode"></param>
        protected override void ChangeEditMode(bool isEditMode)
        {
            foreach (var output in this.OutputNodesCollection)
            {
                output.HideElement = !isEditMode;
            }

        }
        #endregion
        #endregion

        #region protected
        // pozycja kontrolki w CanvasScheme
        protected Point ControlPosition = new Point(0, 0);
        #endregion

        #region public
        #region public fields
        /// <summary>
        ///  Kolekcja "wyjść" względem zera kontrolki. Punkt: X - CanvasLeft, Y - CanvasTop
        /// </summary>
        private ObservableCollection<Point> _OutputPointsCollection = new ObservableCollection<Point>();    
        public ObservableCollection<Point> OutputPointsCollection { get { return _OutputPointsCollection; } }

        private ObservableCollection<OutputNode> _OutputNodesCollection = new ObservableCollection<OutputNode>();
        public ObservableCollection<OutputNode> OutputNodesCollection { get { return _OutputNodesCollection; } }     
        #endregion

        public void RemoveOutputs()
        {
            foreach(var output in OutputNodesCollection)
            {
                output.Remove();              
            }
            OutputNodesCollection.Clear();
        }
        #endregion

        #region overrides
        // auto scaling width and height
        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            if (ControlOrientation == Orientation.Horizontal)
            {
                //sprawdzenie czy została ustawiona właściwość Height
                if (sizeInfo.HeightChanged && !ValueChecker.CheckDoubleNanInfOrZero(this.Height))
                    this.Width = sizeInfo.NewSize.Height * WidthToHeightControlRatio;
                else if (sizeInfo.WidthChanged && !ValueChecker.CheckDoubleNanInfOrZero(this.Width))
                    this.Height = sizeInfo.NewSize.Width / WidthToHeightControlRatio;
            }
            else
            {
                if (sizeInfo.HeightChanged && !ValueChecker.CheckDoubleNanInfOrZero(this.Height))
                    this.Width = sizeInfo.NewSize.Height / WidthToHeightControlRatio;
                else if (sizeInfo.WidthChanged && !ValueChecker.CheckDoubleNanInfOrZero(this.Width))
                    this.Height = sizeInfo.NewSize.Width * WidthToHeightControlRatio;
            }

            //Uaktualnienie współrzędnych wyjść po zmianie rozmiaru
            ComputeAndAddOutputs();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
        }
      
        public override void Remove()
        {
            if(this.Parent is SchemeCanvas)
            {
                var schemeCanvas = (SchemeCanvas)this.Parent;
                this.RemoveOutputs();
                schemeCanvas.RemoveElementFromScheme(this);
            }
        }
        #endregion
    }
}
