﻿using Newtonsoft.Json;
using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Events;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.Helpers;
using SchemeControlsLib.Inputs;
using SchemeControlsLib.Models;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Core
{
    public class MultiSourceBrushControl : EditorControlBase, IMultiBrushLine, IChangeableControl
    {
        /// <summary>
        /// Ustawia nazwę kontrolki z walidacją
        /// </summary>
        /// <param name="name">nazwa</param>
        /// <returns>tru jeśli ustawiło</returns>
        public bool SetName(string name)
        {
            if(this.Parent is SchemeCanvas && name.CheckIfIsControlName() && !((SchemeCanvas)this.Parent).CheckIfNameExist(name))
            {
                this.Name = name;
                return true;
            }
            return false;
        }

        /// <summary>
        /// wpisuje wartość do podanej właściwości
        /// </summary>
        /// <param name="propertyName">nazwa właściwosci</param>
        /// <param name="value">wartość</param>
        /// <returns></returns>
        public bool SetSpecificPropdpValue(string propertyName, object value)
        {
            var specificPropdp = DependencyPropertyFinder.GetControlSpecificDependencyPropertiesDict(this, ElementPropertiesModels.GetPropertiesByElementType(this)).Where(x => x.Key is DependencyProperty).Select(x => x.Key as DependencyProperty).ToList().Where(x => x.Name == propertyName).FirstOrDefault();
            if(value != null && specificPropdp.PropertyType == value.GetType())
            {
                this.SetValue(specificPropdp, value);
                return true;
            }
            return false;
        }


        public event SourceBrushesChangedDelegate SourceBrushesChangedEvent;

        /// <summary>
        /// Source color brushes
        /// </summary>
        private ObservableCollection<Brush> _SourceBrushes = new ObservableCollection<Brush>();
        public ObservableCollection<Brush> SourceBrushes
        {
            get { return _SourceBrushes; }
        }

        /// <summary>
        /// metoda dodająca element do kolekcji z informowaniem o zmianie kolekcji
        /// </summary>
        /// <param name="brushToAdd">kolor do dodania</param>
        public void AddBrushToSourceBrushes(Brush brushToAdd)
        {
            if (SourceBrushes != null && !SourceBrushes.Any(x => x == brushToAdd) && brushToAdd != Settings.DefaultLineColor)
            {
                var oldBrushes = new ObservableCollection<Brush>(SourceBrushes);
                SourceBrushes.Add(brushToAdd);

                // podniesienie zdarzenia
                RaiseSourceBrushesChangedEvent(oldBrushes);

                NotifyPropertyChanged(() => SourceBrushes);
                this.MethodAfterAddBrushToSourceBrushes(brushToAdd);
                this.InvalidateVisual();
            }
        }

        /// <summary>
        /// metoda usuwajaca element do kolekcji z informowaniem o zmianie kolekcji
        /// </summary>
        /// <param name="brushToRemove">kolor do usunięcia</param>
        public void RemoveBrushFromSourceBrushes(Brush brushToRemove)
        {
            if (SourceBrushes != null && SourceBrushes.Any(x => x == brushToRemove))
            {
                var oldBrushes = new ObservableCollection<Brush>(SourceBrushes);
                SourceBrushes.Remove(brushToRemove);

                // podniesienie zdarzenia
                RaiseSourceBrushesChangedEvent(oldBrushes);

                NotifyPropertyChanged(() => SourceBrushes);
                this.MethodAfterRemoveBrushToSourceBrushes(brushToRemove);
                this.InvalidateVisual();
            }
        }

        /// <summary>
        /// metoda czyszcząca kolekcje SourceBrushes
        /// </summary>
        public void ClearSourceBrushes()
        {
            if (SourceBrushes.Any())
            {
                var oldBrushes = new ObservableCollection<Brush>(SourceBrushes);
                SourceBrushes.Clear();

                // podniesienie zdarzenia
                RaiseSourceBrushesChangedEvent(oldBrushes);

                NotifyPropertyChanged(() => SourceBrushes);
                this.MethodAfterClearSourceBrushes();
                this.InvalidateVisual();
            }
        }

        /// <summary>
        /// metoda dodająca kolekcje z brush do kolekcji SourceColor
        /// </summary>
        /// <param name="brushesToAdd"></param>
        public void AddRangeToSourceBrushes(IList<Brush> brushesToAdd)
        {
            if (SourceBrushes != null && brushesToAdd.Any() && brushesToAdd.Except(SourceBrushes).Any())
            {
                var oldBrushes = new ObservableCollection<Brush>(SourceBrushes);
                bool addedFlag = false;
                foreach (var brush in brushesToAdd)
                {
                    if (!SourceBrushes.Any(x => x == brush))
                    {
                        SourceBrushes.Add(brush);
                        addedFlag = true;
                    }
                }

                if (addedFlag)
                {
                    // podniesienie zdarzenia
                    RaiseSourceBrushesChangedEvent(oldBrushes);

                    NotifyPropertyChanged(() => SourceBrushes);
                    this.MethodAfterAddRangeToSourceBrushes(brushesToAdd);
                    this.InvalidateVisual();
                }
            }
        }

        /// <summary>
        /// metoda usuwająca kolekcje z brush do kolekcji SourceColor
        /// </summary>
        /// <param name="brushesToAdd"></param>
        public void RemoveRangeToSourceBrushes(IList<Brush> brushesToRemove)
        {
            if (SourceBrushes != null && brushesToRemove.Any() && brushesToRemove.Intersect(SourceBrushes).Any())
            {
                bool removedFlag = false;
                var oldBrushes = new ObservableCollection<Brush>(SourceBrushes);
                foreach (var brush in brushesToRemove)
                {
                    if (SourceBrushes.Any(x => x == brush))
                    {
                        SourceBrushes.Remove(brush);
                        removedFlag = true;
                    }
                }
                // podniesienie zdarzenia jeśli faktycznie zmieniono kolekcje
                if (removedFlag)
                {
                    RaiseSourceBrushesChangedEvent(oldBrushes);

                    NotifyPropertyChanged(() => SourceBrushes);
                    this.MethodAfterRemoveRangeToSourceBrushes(brushesToRemove);
                    this.InvalidateVisual();
                }
            }
        }

        protected virtual void MethodAfterAddBrushToSourceBrushes(Brush addedBrush) { }

        protected virtual void MethodAfterRemoveBrushToSourceBrushes(Brush removedBrush) { }

        protected virtual void MethodAfterClearSourceBrushes() { }

        protected virtual void MethodAfterAddRangeToSourceBrushes(IList<Brush> listToAdd) {  }

        protected virtual void MethodAfterRemoveRangeToSourceBrushes(IList<Brush> brushesToRemove) {  }

        private void RaiseSourceBrushesChangedEvent(IList<Brush> oldBrushes)
        {
            if (SourceBrushesChangedEvent != null && !MixColors.CompareTwoBrushes(oldBrushes, SourceBrushes))
                SourceBrushesChangedEvent(this, new SourceBrushesChangedEventArgs(oldBrushes, new ObservableCollection<Brush>(SourceBrushes)));
        }
    }
}
