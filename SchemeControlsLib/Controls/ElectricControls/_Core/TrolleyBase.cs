﻿using SchemeControlsLib.Controls.Interfaces.ElementsInterfaces;
using SchemeControlsLib.Controls.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Core
{
    public class TrolleyBase : IDrawTrolley
    {
        /// <summary>
        /// Rysuje tło wózka
        /// </summary>
        /// <param name="drawingContext"></param>
        /// <param name="width">szerokość tła</param>
        /// <param name="height">wysokość tła</param>
        /// <param name="stateBackgroundBrush">kolor tła</param>
        /// <param name="cornerRadius">zaokrąglenie rogów</param>
        public void DrawTrolleyBackground(DrawingContext drawingContext, double width, double height, Brush stateBackgroundBrush, CornerRadius cornerRadius)
        {
            drawingContext.DrawRoundedRectangle(stateBackgroundBrush, null, new Rect(new Size(width, height)), cornerRadius);
        }

        /// <summary>
        /// rysuje wózek górny. Wylicza jego wysokość w zależności od dostępnego miejsca określonego w width i height
        /// </summary>
        /// <param name="drawingContext"></param>
        /// <param name="width">Całkowita szerokość kontrolki</param>
        /// <param name="height">Całkowita wysokość kontrolki</param>
        /// <param name="flippedGraphic">Czy obrócona grafika</param>
        /// <param name="controlOrientation">Czy zmieniona orientacja</param>
        /// <param name="pen">dłogopis</param>
        public void DrawTrolleyTop(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, double lineEndDistance = 8.0)
        {
            if (controlOrientation == Orientation.Vertical)
            {
                double rectangleDistance = height / 20;
                drawingContext.DrawLine(pen, new Point(0, height / 2), new Point(width / lineEndDistance, height / 2));
                drawingContext.DrawLine(pen, new Point(width / lineEndDistance - rectangleDistance, height / 2 - rectangleDistance * 2), new Point(width / lineEndDistance, height / 2));
                drawingContext.DrawLine(pen, new Point(width / lineEndDistance - rectangleDistance, height / 2 + rectangleDistance * 2), new Point(width / lineEndDistance, height / 2));               
            }
            else
            {
                double rectangleDistance = width / 20;
                drawingContext.DrawLine(pen, new Point(width / 2, 0), new Point(width / 2, height / lineEndDistance));
                drawingContext.DrawLine(pen, new Point(width / 2 - rectangleDistance * 2, height / lineEndDistance - rectangleDistance), new Point(width / 2, height / lineEndDistance));
                drawingContext.DrawLine(pen, new Point(width / 2 + rectangleDistance * 2, height / lineEndDistance - rectangleDistance), new Point(width / 2, height / lineEndDistance));
            }
        }

        /// <summary>
        /// rysuje wózek dolny. Wylicza jego wysokość w zależności od dostępnego miejsca określonego w width i height
        /// </summary>
        /// <param name="drawingContext"></param>
        /// <param name="width">Całkowita szerokość kontrolki</param>
        /// <param name="height">Całkowita wysokość kontrolki</param>
        /// <param name="flippedGraphic">Czy obrócona grafika</param>
        /// <param name="controlOrientation">Czy zmieniona orientacja</param>
        /// <param name="pen">dłogopis</param>
        public void DrawTrolleyBottom(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, double lineEndDistance = 8.0)
        {
            if (controlOrientation == Orientation.Vertical)
            {
                double rectangleDistance = height / 20;
                drawingContext.DrawLine(pen, new Point(width, height / 2), new Point((lineEndDistance - 1) * width / lineEndDistance, height / 2));
                drawingContext.DrawLine(pen, new Point((lineEndDistance - 1) * width / lineEndDistance + rectangleDistance, height / 2 - rectangleDistance * 2), new Point((lineEndDistance - 1) * width / lineEndDistance, height / 2));
                drawingContext.DrawLine(pen, new Point((lineEndDistance - 1) * width / lineEndDistance + rectangleDistance, height / 2 + rectangleDistance * 2), new Point((lineEndDistance - 1) * width / lineEndDistance, height / 2));
            }
            else
            {              
                double rectangleDistance = width / 20;
                drawingContext.DrawLine(pen, new Point(width / 2, height), new Point(width / 2, (lineEndDistance - 1) * height / lineEndDistance));
                drawingContext.DrawLine(pen, new Point(width / 2 - rectangleDistance * 2, (lineEndDistance - 1) * height / lineEndDistance + rectangleDistance), new Point(width / 2, (lineEndDistance - 1) * height / lineEndDistance));
                drawingContext.DrawLine(pen, new Point(width / 2 + rectangleDistance * 2, (lineEndDistance - 1) * height / lineEndDistance + rectangleDistance), new Point(width / 2, (lineEndDistance - 1) * height / lineEndDistance));
            }
        }

        /// <summary>
        /// Rysuje wózek góra lub dół na określony wymiar (zajmuje całe podane miejsce w width i height)
        /// </summary>
        /// <param name="drawingContext"></param>
        /// <param name="width">Szerokość wózka</param>
        /// <param name="height">Wysokość wózka</param>
        /// <param name="flippedGraphic">Czy obrócona grafika</param>
        /// <param name="controlOrientation">Czy zmieniona orientacja</param>
        /// <param name="pen">dłogopis</param>
        /// <param name="margin">margines przesunięcia</param>
        public void DrawTrolley(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, Thickness margin)
        {
            double horizontalPosition = margin.Top - margin.Bottom;
            double verticalPosiotion = margin.Left - margin.Right;

            // strzałka w doł
            if (!flippedGraphic)
            {
                if (controlOrientation == Orientation.Vertical)
                {
                    double rectangleDistance = height / 20;
                    drawingContext.DrawLine(pen, 
                        new Point(0 + verticalPosiotion, height / 2.0 + horizontalPosition), 
                        new Point(width + verticalPosiotion, height / 2.0 + horizontalPosition));
                    drawingContext.DrawLine(pen, 
                        new Point(width - rectangleDistance + verticalPosiotion, height / 2.0 - rectangleDistance * 2 + horizontalPosition), 
                        new Point(width + verticalPosiotion, height / 2.0 + horizontalPosition));
                    drawingContext.DrawLine(pen, 
                        new Point(width - rectangleDistance + verticalPosiotion, height / 2 + rectangleDistance * 2 + horizontalPosition), 
                        new Point(width + verticalPosiotion, height / 2.0 + horizontalPosition));
                }
                else
                {
                    double rectangleDistance = width / 20;
                    drawingContext.DrawLine(pen, 
                        new Point(width / 2 + verticalPosiotion, 0 + horizontalPosition), 
                        new Point(width / 2 + verticalPosiotion, height / 2 + horizontalPosition));
                    drawingContext.DrawLine(pen, 
                        new Point(width / 2 - rectangleDistance * 2 + verticalPosiotion, height / 2 - rectangleDistance + horizontalPosition), 
                        new Point(width / 2 + verticalPosiotion, height / 2 + horizontalPosition));
                    drawingContext.DrawLine(pen, 
                        new Point(width / 2 + rectangleDistance * 2 + verticalPosiotion, height / 2 - rectangleDistance + horizontalPosition), 
                        new Point(width / 2 + verticalPosiotion, height / 2 + horizontalPosition));
                }
            }
            // strzałka w góre
            else
            {
                if (controlOrientation == Orientation.Vertical)
                {
                    double rectangleDistance = height / 20;
                    drawingContext.DrawLine(pen, 
                        new Point(width + verticalPosiotion, height / 2 + horizontalPosition), 
                        new Point(0 + verticalPosiotion, height / 2 + horizontalPosition));
                    drawingContext.DrawLine(pen, 
                        new Point(0 + rectangleDistance + verticalPosiotion, height / 2 - rectangleDistance * 2 + horizontalPosition), 
                        new Point(0 + verticalPosiotion, height / 2 + horizontalPosition));
                    drawingContext.DrawLine(pen, 
                        new Point(0 + rectangleDistance + verticalPosiotion, height / 2 + rectangleDistance * 2 + horizontalPosition), 
                        new Point(0 + verticalPosiotion, height / 2 + horizontalPosition));
                }
                else//
                {
                    double rectangleDistance = width / 20;
                    drawingContext.DrawLine(pen, 
                        new Point(width / 2 + verticalPosiotion, height + horizontalPosition), 
                        new Point(width / 2 + verticalPosiotion, 0 + horizontalPosition));
                    drawingContext.DrawLine(pen, 
                        new Point(width / 2 - rectangleDistance * 2 + verticalPosiotion, 0 + rectangleDistance + horizontalPosition), 
                        new Point(width / 2 + verticalPosiotion, 0 + horizontalPosition));
                    drawingContext.DrawLine(pen,
                        new Point(width / 2 + rectangleDistance * 2 + verticalPosiotion, 0 + rectangleDistance + horizontalPosition), 
                        new Point(width / 2 + verticalPosiotion, 0 + horizontalPosition));
                }
            }

        }
    }
}
