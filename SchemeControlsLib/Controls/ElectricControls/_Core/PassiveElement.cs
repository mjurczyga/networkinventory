﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.ElectricControls;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.SchemeControlTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Core
{
    public abstract class PassiveElement : ElectricElementBase, IPasiveElement, IBindingControl
    {
        [EditorProperty("Tag")]
        public new object Tag { get { return base.Tag; } set { base.Tag = value; } }

        #region Dependency properties - button additionals
        [EditorProperty("Czy kompensator?")]
        public bool IsCompensator
        {
            get { return (bool)GetValue(IsCompensatorProperty); }
            set { SetValue(IsCompensatorProperty, value); }
        }

        public static readonly DependencyProperty IsCompensatorProperty =
            DependencyProperty.Register("IsCompensator", typeof(bool), typeof(PassiveElement), new PropertyMetadata(false));

        //[EditorProperty("Nazwa zmiennej urządzenia")]
        //public string VarName
        //{
        //    get { return (string)GetValue(VarNameProperty); }
        //    set { SetValue(VarNameProperty, value); }
        //}

        //public static readonly DependencyProperty VarNameProperty =
        //    DependencyProperty.Register("VarName", typeof(string), typeof(PassiveElement), new PropertyMetadata(null));

        //[EditorProperty("Tytuł okna")]
        //public string WindowTitle
        //{
        //    get { return (string)GetValue(WindowTitleProperty); }
        //    set { SetValue(WindowTitleProperty, value); }
        //}

        //public static readonly DependencyProperty WindowTitleProperty =
        //    DependencyProperty.Register("WindowTitle", typeof(string), typeof(PassiveElement), new PropertyMetadata(null));

        //[EditorProperty("Nazwa pliku schematu")]
        //public string SchemeFileName
        //{
        //    get { return (string)GetValue(SchemeFileNameProperty); }
        //    set { SetValue(SchemeFileNameProperty, value); }
        //}

        //public static readonly DependencyProperty SchemeFileNameProperty =
        //    DependencyProperty.Register("SchemeFileName", typeof(string), typeof(PassiveElement), new PropertyMetadata(null));

        ////Np LumelN14
        //[EditorProperty("Typ urządzenia")]
        //public string DeviceTypeName
        //{
        //    get { return (string)GetValue(DeviceTypeNameProperty); }
        //    set { SetValue(DeviceTypeNameProperty, value); }
        //}

        //public static readonly DependencyProperty DeviceTypeNameProperty =
        //    DependencyProperty.Register("DeviceTypeName", typeof(string), typeof(PassiveElement), new PropertyMetadata(null));

        //[EditorProperty("Prefix zmiennych pola")]
        //public string FieldVarsNamePrefix
        //{
        //    get { return (string)GetValue(FieldVarsNamePrefixProperty); }
        //    set { SetValue(FieldVarsNamePrefixProperty, value); }
        //}

        //public static readonly DependencyProperty FieldVarsNamePrefixProperty =
        //    DependencyProperty.Register("FieldVarsNamePrefix", typeof(string), typeof(PassiveElement), new PropertyMetadata(null));

        [EditorProperty("ICommand na click")]
        public object ClickCommand
        {
            get { return (object)GetValue(ClickCommandProperty); }
            set { SetValue(ClickCommandProperty, value); }
        }

        public static readonly DependencyProperty ClickCommandProperty =
            DependencyProperty.Register("ClickCommand", typeof(object), typeof(PassiveElement), new PropertyMetadata(null, ClickCommand_PropertyChanged));

        private static void ClickCommand_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PassiveElement activeElement = (PassiveElement)d;
            if (e.NewValue != null)
                activeElement.Cursor = Cursors.Hand;
            else
                activeElement.Cursor = Cursors.Arrow;
        }
        #endregion

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            if (this.ClickCommand is ICommand)
                ((ICommand)this.ClickCommand).Execute(this);
        }

        protected override void MethodAfterAddBrushToSourceBrushes(Brush addedBrush)
        {
            if (OutputNodesCollection != null)
            {
                foreach(var output in OutputNodesCollection)
                {
                    if (!MixColors.CompareTwoBrushes(this.SourceBrushes, output.SourceBrushes))
                    {
                        output.AddBrushToSourceBrushes(addedBrush);
                    }
                }
            }
        }

        protected override void MethodAfterRemoveBrushToSourceBrushes(Brush removedBrush)
        {
            if (OutputNodesCollection != null)
            {
                // Sprawdzenie podłaczeńwyjść ze źródłami
                var connectedSources = ChcekSourcesForOutputs();

                foreach (var output in OutputNodesCollection)
                {                   
                    if (!MixColors.CompareTwoBrushes(this.SourceBrushes, output.SourceBrushes))
                    {
                        if(connectedSources.ContainsKey(output.Name) && !connectedSources[output.Name].Any(x => x.SourceBrush == removedBrush) || !connectedSources.ContainsKey(output.Name))
                            output.RemoveBrushFromSourceBrushes(removedBrush);
                    }
                }
            }
        }

        protected override void MethodAfterClearSourceBrushes()
        {
            if (OutputNodesCollection != null)
            {
                // Sprawdzenie podłaczeńwyjść ze źródłami
                var connectedSources = ChcekSourcesForOutputs();

                foreach (var output in OutputNodesCollection)
                {
                    if (connectedSources.All(x => x.Value.Count == 0))
                        output.ClearSourceBrushes();
                }
            }
        }

        protected override void MethodAfterAddRangeToSourceBrushes(IList<Brush> listToAdd)
        {
            if (OutputNodesCollection != null)
            {
                foreach (var output in OutputNodesCollection)
                {
                    if (!MixColors.CompareTwoBrushes(this.SourceBrushes, output.SourceBrushes))
                        output.AddRangeToSourceBrushes(listToAdd);
                }
            }
        }

        protected override void MethodAfterRemoveRangeToSourceBrushes(IList<Brush> brushesToRemove)
        {
            if (OutputNodesCollection != null)
            {
                // Sprawdzenie podłaczeńwyjść ze źródłami
                var connectedSources = ChcekSourcesForOutputs();

                foreach(var connectedSource in connectedSources)
                {
                    // sprawdzenie czy przypadkiem kolor nie jest podłączony do źródła
                    foreach(var sourceBrush in connectedSource.Value)
                    {
                        if(brushesToRemove.Contains(sourceBrush.SourceBrush))
                            brushesToRemove.Remove(sourceBrush.SourceBrush);
                    }
                }

                // usuwa kolory z wyjść
                foreach (var output in OutputNodesCollection)
                {
                    if (MixColors.CheckIfBrushesContainsAnotherBrushes(output.SourceBrushes, brushesToRemove))                                         
                        output.RemoveRangeToSourceBrushes(brushesToRemove);                  
                }
            }
        }



        private List<string> checkedControl = new List<string>();
        /// <summary>
        /// sprawdza po kolei czy wyjście kontrolki jest połączone ze źródłem
        /// </summary>
        /// <returns>słownik nazwa wyjścia, lista źródeł</returns>
        private Dictionary<string, List<ISource>> ChcekSourcesForOutputs()
        {
            var resultDictionary = new Dictionary<string, List<ISource>>();

            foreach (var output in this.OutputNodesCollection)
            {
                checkedControl.Clear();
                resultDictionary.Add(output.Name, output.CheckSourceConnection(output, checkedControl));
            }

            return resultDictionary;
        }
    }
}
