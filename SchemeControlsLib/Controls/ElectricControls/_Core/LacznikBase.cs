﻿using SchemeControlsLib.Controls.Converters;
using SchemeControlsLib.Controls.Interfaces.ElementsInterfaces;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.Core
{
    public class LacznikBase : IDrawLacznik
    {
        public void DrawSwitchBackground(DrawingContext drawingContext, double width, double height, Orientation controlOrientation, Pen pen, Brush stateBackgroundBrush, CornerRadius cornerRadius, SwitchPlacement switchPlacement, Thickness margin, Thickness backgroundMargin, double lineDistanceRatio = 6.0)
        {
            drawingContext.DrawRoundedRectangle(stateBackgroundBrush, null, new Rect(new Point(backgroundMargin.Left, backgroundMargin.Top), new Size(width, height)), cornerRadius);

            double horizontalPosition = margin.Top - margin.Bottom;
            double verticalPosiotion = margin.Left - margin.Right;

            if (switchPlacement == SwitchPlacement.Switch)
            {
                if (controlOrientation == Orientation.Vertical)
                {
                    GuidelineSet guidelines = new GuidelineSet();
                    guidelines.GuidelinesX.Add(-Settings.ElementStrokeThickness + verticalPosiotion);
                    guidelines.GuidelinesX.Add(width / 6.0 + verticalPosiotion);
                    guidelines.GuidelinesY.Add(height / 2.0 + horizontalPosition);

                    guidelines.GuidelinesX.Add(width + Settings.ElementStrokeThickness + verticalPosiotion);
                    guidelines.GuidelinesX.Add(5.0 * width / 6.0 + verticalPosiotion);
                    drawingContext.PushGuidelineSet(guidelines);

                    drawingContext.DrawLine(pen, new Point(-Settings.ElementStrokeThickness + verticalPosiotion, height / 2.0 + horizontalPosition), new Point(width / 6.0 + verticalPosiotion, height / 2.0 + horizontalPosition));
                    drawingContext.DrawLine(pen, new Point(width + Settings.ElementStrokeThickness + verticalPosiotion, height / 2.0 + horizontalPosition), new Point(5.0 * width / 6.0 + verticalPosiotion, height / 2.0 + horizontalPosition));
                }
                else
                {
                    GuidelineSet guidelines = new GuidelineSet();
                    guidelines.GuidelinesX.Add(width / 2.0 + verticalPosiotion);
                    guidelines.GuidelinesY.Add(-Settings.ElementStrokeThickness + horizontalPosition);
                    guidelines.GuidelinesY.Add(height / 6.0 + horizontalPosition);

                    guidelines.GuidelinesY.Add(height + Settings.ElementStrokeThickness + horizontalPosition);
                    guidelines.GuidelinesY.Add(5.0 * height / 6.0 + horizontalPosition);
                    drawingContext.PushGuidelineSet(guidelines);


                    drawingContext.DrawLine(pen, new Point(width / 2.0 + verticalPosiotion, -Settings.ElementStrokeThickness + horizontalPosition), new Point(width / 2.0 + verticalPosiotion, height / 6.0 + horizontalPosition));
                    drawingContext.DrawLine(pen, new Point(width / 2.0 + verticalPosiotion, height + Settings.ElementStrokeThickness + horizontalPosition), new Point(width / 2.0 + verticalPosiotion, 5.0 * height / 6.0 + horizontalPosition));
                }
            }
            else if (switchPlacement == SwitchPlacement.Trolley)
            {
                if (controlOrientation == Orientation.Vertical)
                {
                    double rectangleDistance = height / 20;
                    //prawa
                    drawingContext.DrawLine(pen, new Point(width + verticalPosiotion, height / 2 + horizontalPosition), new Point((lineDistanceRatio - 1) * width / lineDistanceRatio + verticalPosiotion, height / 2 + horizontalPosition));
                    drawingContext.DrawLine(pen, new Point(width + rectangleDistance + verticalPosiotion, height / 2 - rectangleDistance * 2 + horizontalPosition), new Point(width + verticalPosiotion, height / 2 + horizontalPosition));
                    drawingContext.DrawLine(pen, new Point(width + rectangleDistance + verticalPosiotion, height / 2 + rectangleDistance * 2 + horizontalPosition), new Point(width + verticalPosiotion, height / 2 + horizontalPosition));

                    //lewa
                    drawingContext.DrawLine(pen, new Point(0 + verticalPosiotion, height / 2 + horizontalPosition), new Point(width / lineDistanceRatio + verticalPosiotion, height / 2 + horizontalPosition));
                    drawingContext.DrawLine(pen, new Point(-rectangleDistance + verticalPosiotion, height / 2 - rectangleDistance * 2 + horizontalPosition), new Point(0 + verticalPosiotion, height / 2 + horizontalPosition));
                    drawingContext.DrawLine(pen, new Point(-rectangleDistance + verticalPosiotion, height / 2 + rectangleDistance * 2 + horizontalPosition), new Point(0 + verticalPosiotion, height / 2 + horizontalPosition));

                }
                else
                {
                    double rectangleDistance = width / 20;
                    //góra
                    drawingContext.DrawLine(pen, new Point(width / 2 + verticalPosiotion, height + horizontalPosition), new Point(width / 2 + verticalPosiotion, (lineDistanceRatio - 1) * height / lineDistanceRatio + horizontalPosition));
                    drawingContext.DrawLine(pen, new Point(width / 2 - rectangleDistance * 2 + verticalPosiotion, height + rectangleDistance + horizontalPosition), new Point(width / 2 + verticalPosiotion, height + horizontalPosition));
                    drawingContext.DrawLine(pen, new Point(width / 2 + rectangleDistance * 2 + verticalPosiotion, height + rectangleDistance + horizontalPosition), new Point(width / 2 + verticalPosiotion, height + horizontalPosition));

                    //dół
                    drawingContext.DrawLine(pen, new Point(width / 2 + verticalPosiotion, 0 + horizontalPosition), new Point(width / 2 + verticalPosiotion, height / lineDistanceRatio + horizontalPosition));
                    drawingContext.DrawLine(pen, new Point(width / 2 - rectangleDistance * 2 + verticalPosiotion, -rectangleDistance + horizontalPosition), new Point(width / 2 + verticalPosiotion, 0 + horizontalPosition));
                    drawingContext.DrawLine(pen, new Point(width / 2 + rectangleDistance * 2 + verticalPosiotion, -rectangleDistance + horizontalPosition), new Point(width / 2 + verticalPosiotion, 0 + horizontalPosition));
                }
            }
        }

        /// <summary>
        /// If Closed switch (state 1)
        /// </summary>
        /// <param name="drawingContext"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="flippedGraphic"></param>
        /// <param name="controlOrientation"></param>
        /// <param name="pen"></param>
        public void DrawCloseSwitch(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, Thickness margin)
        {



            double horizontalPosition = margin.Top - margin.Bottom;
            double verticalPosiotion = margin.Left - margin.Right;
            if (controlOrientation == Orientation.Horizontal)
            {

                GuidelineSet guidelines = new GuidelineSet();
                guidelines.GuidelinesX.Add(width / 2 + verticalPosiotion);
                guidelines.GuidelinesY.Add(height / 6 + horizontalPosition);
                guidelines.GuidelinesY.Add(5 * height / 6 + horizontalPosition);
                drawingContext.PushGuidelineSet(guidelines);

                drawingContext.DrawLine(pen, new Point(width / 2 + verticalPosiotion, height / 6 + horizontalPosition), new Point(width / 2 + verticalPosiotion, 5 * height / 6 + horizontalPosition));

            }
            else
            {

                GuidelineSet guidelines = new GuidelineSet();
                guidelines.GuidelinesX.Add(width / 6 + verticalPosiotion);
                guidelines.GuidelinesY.Add(5 * width / 6 + verticalPosiotion);
                guidelines.GuidelinesY.Add(height / 2 + horizontalPosition);
                drawingContext.PushGuidelineSet(guidelines);

                drawingContext.DrawLine(pen, new Point(width / 6 + verticalPosiotion, height / 2 + horizontalPosition), new Point(5 * width / 6 + verticalPosiotion, height / 2 + horizontalPosition));
            }
        }

        /// <summary>
        /// If switchstatus.open
        /// </summary>
        /// <param name="drawingContext"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="flippedGraphic"></param>
        /// <param name="controlOrientation"></param>
        /// <param name="pen"></param>
        public void DrawOpenSwitch(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, Thickness margin)
        {
            double horizontalPosition = margin.Top - margin.Bottom;
            double verticalPosiotion = margin.Left - margin.Right;
            if (!flippedGraphic)
            {
                if (controlOrientation == Orientation.Horizontal)
                    drawingContext.DrawLine(pen, new Point(width / 2.0 + verticalPosiotion, height / 6.0 + horizontalPosition), new Point(width / 4.0 + verticalPosiotion, 4.0 * height / 5.0 + horizontalPosition));
                else
                    drawingContext.DrawLine(pen, new Point(width / 6.0 + verticalPosiotion, height / 2.0 + horizontalPosition), new Point(4.0 * width / 5.0 + verticalPosiotion, height / 4.0 + horizontalPosition));
            }
            else
            {
                if (controlOrientation == Orientation.Horizontal)
                    drawingContext.DrawLine(pen, new Point(width / 4.0 + verticalPosiotion, height / 5.0 + horizontalPosition), new Point(width / 2.0 + verticalPosiotion, 5.0 * height / 6.0 + horizontalPosition));
                else
                    drawingContext.DrawLine(pen, new Point(width / 5.0 + verticalPosiotion, height / 4.0 + horizontalPosition), new Point(5.0 * width / 6.0 + verticalPosiotion, height / 2.0 + horizontalPosition));
            }
        }

        public void DrawQuestionMark(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, Thickness margin)
        {
            double horizontalPosition = margin.Top - margin.Bottom;
            double verticalPosiotion = margin.Left - margin.Right;

            double qMarkWidth, qMarkHeight; Size size;

            if (controlOrientation == Orientation.Horizontal) { qMarkWidth = width; qMarkHeight = height; } else { qMarkWidth = height; qMarkHeight = width; };
            double offset = 0.05 * qMarkWidth;
            double xDistance = qMarkWidth / 2.0 - qMarkWidth / 2.5;
            double yDistance = qMarkHeight / 2.0 - (2.9 * qMarkHeight / 8.0 + offset);

            Point startPoint = new Point(width / 2.0 - xDistance + verticalPosiotion, height / 2.0 - yDistance + horizontalPosition);
            size = new Size(xDistance, Math.Abs(yDistance));

            Point lineEndPoint = new Point(size.Width + startPoint.X, size.Height + startPoint.Y + size.Height);

            var geometry = new StreamGeometry();
            using (var context = geometry.Open())
            {
                bool isStroked = pen != null;
                const bool isSmoothJoin = true;

                context.BeginFigure(startPoint, false, false);
                context.ArcTo(new Point(size.Width + startPoint.X, size.Height + startPoint.Y),
                    size,
                    0, true, SweepDirection.Clockwise, isStroked, isSmoothJoin);
                context.LineTo(lineEndPoint, isStroked, isSmoothJoin);

                context.Close();
            }

            drawingContext.DrawGeometry(null, pen, geometry);

            // kropka
            drawingContext.DrawLine(pen, new Point(width / 2.0 - 0.5 + verticalPosiotion, lineEndPoint.Y + 2.0), new Point(width / 2.0 + 0.5 + verticalPosiotion, lineEndPoint.Y + 2.0));

        }

        public void DrawSwitchType(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, SwitchTypes switchType, Thickness margin)
        {
            double horizontalPosition = margin.Top - margin.Bottom;
            double verticalPosiotion = margin.Left - margin.Right;

            double halfSquare = controlOrientation == Orientation.Horizontal ? 0.1 * width : 0.1 * height;
            double yPosition = controlOrientation == Orientation.Horizontal ? (flippedGraphic ? height / 6.0 : 5.0 * height / 6.0) : (flippedGraphic ? width / 6.0 : 5.0 * width / 6.0);

            if (switchType == SwitchTypes.Odlacznik)
            {
                if (controlOrientation == Orientation.Horizontal)
                    drawingContext.DrawLine(pen, new Point(width / 2.5 + verticalPosiotion, yPosition + horizontalPosition), new Point(1.5 * width / 2.5 + verticalPosiotion, yPosition + horizontalPosition));
                else
                    drawingContext.DrawLine(pen, new Point(yPosition + verticalPosiotion, height / 2.5 + horizontalPosition), new Point(yPosition + verticalPosiotion, 1.5 * height / 2.5 + horizontalPosition));
            }
            else if (switchType == SwitchTypes.Stycznik)
            {

                Point arcEndPoint, pathStartPoint;
                if (controlOrientation == Orientation.Horizontal)
                {
                    arcEndPoint = new Point(width / 2.0, (flippedGraphic ? -halfSquare : halfSquare) + yPosition);
                    pathStartPoint = new Point(width / 2.0, yPosition + (flippedGraphic ? halfSquare / 2.0 : -halfSquare / 2.0));
                }
                else
                {
                    arcEndPoint = new Point((flippedGraphic ? -halfSquare : halfSquare) + yPosition, height / 2.0);
                    pathStartPoint = new Point(yPosition + (flippedGraphic ? halfSquare / 2.0 : -halfSquare / 2.0), height / 2.0);
                }

                arcEndPoint.X += verticalPosiotion; arcEndPoint.Y += horizontalPosition;
                pathStartPoint.X += verticalPosiotion; pathStartPoint.Y += horizontalPosition;

                var geometry = new StreamGeometry();
                using (var context = geometry.Open())
                {
                    bool isStroked = pen != null;
                    const bool isSmoothJoin = true;

                    context.BeginFigure(pathStartPoint, false, true);
                    context.ArcTo(arcEndPoint,
                        new Size(halfSquare / 2.0, halfSquare / 2.0),
                        180, false,
                       (controlOrientation == Orientation.Horizontal ? (flippedGraphic ? SweepDirection.Clockwise : SweepDirection.Counterclockwise) : (flippedGraphic ? SweepDirection.Counterclockwise : SweepDirection.Clockwise)),
                        isStroked, isSmoothJoin);

                    context.Close();
                }

                drawingContext.DrawGeometry(null, pen, geometry);
            }
            else if (switchType == SwitchTypes.Przelacznik)
            {

            }

            else if (switchType == SwitchTypes.Rozlacznik)
            {
                Thickness circleMargin; double circleDiameter = halfSquare;
                if (controlOrientation == Orientation.Horizontal)
                {                    
                    drawingContext.DrawLine(pen, new Point(width / 2.5 + verticalPosiotion, yPosition + horizontalPosition), new Point(1.5 * width / 2.5 + verticalPosiotion, yPosition + horizontalPosition));

                    //circleMargin = new Thickness(width / 2.0 - circleDiameter / 2.0 - pen.Thickness / 2.0 + verticalPosiotion, yPosition - (flippedGraphic ? 0.0 : circleDiameter + 1.0) + horizontalPosition, 0, 0);

                    circleMargin = new Thickness(width / 2.0, yPosition - (flippedGraphic ? 0.0 : circleDiameter + 1.0) + horizontalPosition, 0, 0);
                }
                else
                {
                    drawingContext.DrawLine(pen, new Point(yPosition + verticalPosiotion, height / 2.5 + horizontalPosition), new Point(yPosition + verticalPosiotion, 1.5 * height / 2.5 + horizontalPosition));
                    circleMargin = new Thickness(yPosition - (flippedGraphic ? 0.0 : circleDiameter + 1.0) + verticalPosiotion, height / 2.0 - circleDiameter / 2.0 - pen.Thickness / 2 + horizontalPosition, 0, 0);

                }

                drawingContext.DrawEllipse(null, pen, //+(circleDiameter + 1.0) / 2.0
                    new Point(circleMargin.Left, circleMargin.Top + (circleDiameter + 1.0) / 2.0), (circleDiameter + 1.0) / 2.0, (circleDiameter + 1.0) / 2.0);
            }

            else if (switchType == SwitchTypes.Wylacznik)
            {
                double wDistance = halfSquare;
                if (controlOrientation == Orientation.Horizontal)
                {
                    drawingContext.DrawLine(pen, new Point(width / 2.0 - wDistance + verticalPosiotion, yPosition + wDistance + horizontalPosition), new Point(width / 2.0 + wDistance + verticalPosiotion, yPosition - wDistance + horizontalPosition));
                    drawingContext.DrawLine(pen, new Point(width / 2.0 - wDistance + verticalPosiotion, yPosition - wDistance + horizontalPosition), new Point(width / 2.0 + wDistance + verticalPosiotion, yPosition + wDistance + horizontalPosition));
                }
                else
                {
                    drawingContext.DrawLine(pen, new Point(yPosition + wDistance + verticalPosiotion, height / 2.0 - wDistance + horizontalPosition), new Point(yPosition - wDistance + verticalPosiotion, height / 2.0 + wDistance + horizontalPosition));
                    drawingContext.DrawLine(pen, new Point(yPosition - wDistance + verticalPosiotion, height / 2.0 - wDistance + horizontalPosition), new Point(yPosition + wDistance + verticalPosiotion, height / 2.0 + wDistance + horizontalPosition));
                }
            }
        }
    }
}
