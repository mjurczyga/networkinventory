﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Converters;
using SchemeControlsLib.Controls.ElectricControls;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.Inputs;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using SchemeControlsLib.Controls.ElectricControls._Core.Interfaces;
using SchemeControlsLib.Models.Parameters;

namespace SchemeControlsLib.Controls.Core
{
    public abstract class ActiveElement : ElectricElementBase, IActiveElement, IBindingControl, IOpenWindowEvent
    {
        #region Dependency properties
        #region ISteeringControl
        public string SteeringVarName
        {
            get { return (string)GetValue(SteeringVarNameProperty); }
            set { SetValue(SteeringVarNameProperty, value); }
        }
        public static readonly DependencyProperty SteeringVarNameProperty =
            DependencyProperty.Register("SteeringVarName", typeof(string), typeof(ActiveElement), new PropertyMetadata(null));

        public string SteeringValue
        {
            get { return (string)GetValue(SteeringValueProperty); }
            set { SetValue(SteeringValueProperty, value); }
        }

        public static readonly DependencyProperty SteeringValueProperty =
            DependencyProperty.Register("SteeringValue", typeof(string), typeof(ActiveElement), new PropertyMetadata(null));

        public event SteeringControlEventDelegate SteeringValueEvent;
        #endregion ISteeringControl

        public int State
        {
            get { return (int)GetValue(StateProperty); }
            set { SetValue(StateProperty, value); }
        }

        public static readonly DependencyProperty StateProperty =
            DependencyProperty.Register("State", typeof(int), typeof(ActiveElement), new PropertyMetadata(0, State_PropertyChanged));

        private static void State_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var element = (ActiveElement)d;
            element.TransferOutputsColors((int)e.NewValue);
            element.InvalidateVisual();
        }

        [EditorProperty("Zdarzenie na kliknięcie")]
        public object ClickCommand
        {
            get { return (object)GetValue(ClickCommandProperty); }
            set { SetValue(ClickCommandProperty, value); }
        }

        public static readonly DependencyProperty ClickCommandProperty =
            DependencyProperty.Register("ClickCommand", typeof(object), typeof(ActiveElement), new PropertyMetadata(null, ClickCommand_PropertyChanged));

        private static void ClickCommand_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ActiveElement activeElement = (ActiveElement)d;
            if (e.NewValue != null)
                activeElement.Cursor = Cursors.Hand;
            else
                activeElement.Cursor = Cursors.Arrow;
        }
        #endregion

        #region public fields
        public bool IsClickCommandBinded
        {
            get { return this.ClickCommand is ICommand ? true : false; }
        }
        #endregion

        #region private fields
        /// <summary>
        /// Słownik zapisanych wejśćkontrolki (pamięć na poprzedni stan wyjść)
        /// </summary>
        private Dictionary<string, ObservableCollection<Brush>> _OldOutputsSourcesColors = new Dictionary<string, ObservableCollection<Brush>>();
        #endregion

        #region protected methods
        protected Brush StateToBrushConverter(int state)
        {
            switch (state)
            {
                case 0: return LibSettings.STATUS_0_BRUSH; //Brushes.SandyBrown;
                case 1: return LibSettings.STATUS_1_BRUSH; //Brushes.Green;
                case 2: return LibSettings.STATUS_2_BRUSH; //Brushes.Red;
                default: return LibSettings.STATUS_DEFAULT_BRUSH; //Brushes.Black;
            }
        }
        #endregion

        #region private methods
        private Border _MouseEnterBorder = null;
        private double _MouseBorderDistance = 8;
        /// <summary>
        /// Drawing background rectangle when mouse enter
        /// </summary>
        private void AddMouseEnterBorder()
        {
            if (this.Parent != null && this.Parent is SchemeCanvas && this._MouseEnterBorder == null)
            {
                double width = this.Width;
                double height = this.Height;

                var border = new Border() { Background = Brushes.White, Opacity = 0.3, Width = width + _MouseBorderDistance, Height = height + _MouseBorderDistance, CornerRadius = new CornerRadius(_MouseBorderDistance / 2) };
                Canvas.SetZIndex(border, -1);
                Canvas.SetLeft(border, this.X_Position - _MouseBorderDistance / 2);
                Canvas.SetTop(border, this.Y_Position - _MouseBorderDistance / 2);

                this._MouseEnterBorder = border;

                ((SchemeCanvas)this.Parent).Children.Add(this._MouseEnterBorder);
            }
        }

        private void RemoveMouseEnterBorder()
        {
            if (this.Parent != null && this.Parent is SchemeCanvas && this._MouseEnterBorder != null && ((SchemeCanvas)this.Parent).Children.Contains(this._MouseEnterBorder))
            {
                ((SchemeCanvas)this.Parent).Children.Remove(this._MouseEnterBorder);
                this._MouseEnterBorder = null;
            }
        }

        private void UpdateMouseEnterBroder()
        {
            if (this._MouseEnterBorder != null)
            {

                Canvas.SetLeft(this._MouseEnterBorder, Canvas.GetLeft(this) - _MouseBorderDistance / 2);
                Canvas.SetTop(this._MouseEnterBorder, Canvas.GetTop(this) - _MouseBorderDistance / 2);
            }
        }

        /// <summary>
        /// zdarzenie na dodanie wyjść do kontrolki. Po dodaniu wejść można pyrzpisać do nich zdarzenia. W tej metodzie przypisanie zdarzeń na zmianę koloru wyjścia.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="args"></param>
        private void OnOutputNodesChanged(object obj, OutputNodesChangedEventArgs args)
        {
            var addedOutputNodes = args.NewOutputNodes.Except(args.OldOutputNodes);

            var deletedOutputNodes = args.OldOutputNodes.Except(args.NewOutputNodes);

            foreach (var output in addedOutputNodes)
            {
                output.SourceBrushesChangedEvent += OnOutputSourceBrushesChanged;
                if (!_OldOutputsSourcesColors.ContainsKey(output.Name))
                    _OldOutputsSourcesColors.Add(output.Name, new ObservableCollection<Brush>(output.SourceBrushes));
            }

            foreach (var output in deletedOutputNodes)
            {
                output.SourceBrushesChangedEvent -= OnOutputSourceBrushesChanged;
                if (_OldOutputsSourcesColors.ContainsKey(output.Name))
                    _OldOutputsSourcesColors.Remove(output.Name);
            }
        }

        /// <summary>
        /// zdarzenie na zmiane kolorów wejść/wyjść kontrolki
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="args"></param>
        private void OnOutputSourceBrushesChanged(object obj, SourceBrushesChangedEventArgs args)
        {
            var outputNode = (OutputNode)obj;

            // Dodanie nowego koloru do listy
            if (args.OldBrushes.Count < args.NewBrushes.Count)
            {
                // wybranie dodanego koloru
                var addedBrushes = args.NewBrushes.Except(args.OldBrushes).ToList();
                // wybranie wszystkich dodanych już kolorów
                var otherOutputsBrushes = this.GetAllOutputsSourceBrushesWithoutParameterOutput(outputNode);

                // dodaj kolor jeśli nie ma go jeszcze i nie należy do innych wyjść
                if (_OldOutputsSourcesColors.ContainsKey(outputNode.Name) && addedBrushes.Except(otherOutputsBrushes).Any())
                    ListExt.AddListToList(_OldOutputsSourcesColors[outputNode.Name], addedBrushes);
            }
            // usuniecie koloru z listy
            else if (args.OldBrushes.Count > args.NewBrushes.Count)
            {
                var deletedBrushes = args.OldBrushes.Except(args.NewBrushes).ToList();
                if (_OldOutputsSourcesColors.ContainsKey(outputNode.Name))
                    ListExt.RemoveListFromList(_OldOutputsSourcesColors[outputNode.Name], deletedBrushes);
            }
        }

        /// <summary>
        /// pobiera wszystkie zapisane kolory wyść
        /// </summary>
        /// <returns></returns>
        private List<Brush> CollectAllOldOutputsSourceBrushes()
        {
            var result = new List<Brush>();
            foreach (var oldOutputState in _OldOutputsSourcesColors)
            {
                ListExt.AddListToList(result, oldOutputState.Value);
            }

            return result;
        }

        /// <summary>
        /// Pobiera wszystkie zapisane kolory wyjścia zwyjątkiem wyjścia w parametrze
        /// </summary>
        /// <param name="outputNode"></param>
        /// <returns></returns>
        private List<Brush> GetAllOutputsSourceBrushesWithoutParameterOutput(OutputNode outputNode)
        {
            var result = new List<Brush>();
            foreach (var loopOutputNode in this.OutputNodesCollection)
            {
                if (loopOutputNode.Name != outputNode.Name)
                {
                    var tempSourceBrushes = loopOutputNode.SourceBrushes.Except(result);
                    foreach (var brush in tempSourceBrushes)
                        if (!result.Contains(brush))
                            result.Add(brush);
                }
            }

            return result;
        }

        /// <summary>
        /// przenosi kolory w zalerznosci od stanu kontrolki
        /// </summary>
        /// <param name="state"></param>
        private void TransferOutputsColors(int state)
        {
            if (OutputNodesCollection.Any())
            {
                // update kolorów kontrolki jeśli zostały wyczyszczone a do wejść jakiś kolor dochodzi
                if (!this.SourceBrushes.Any())
                {
                    this.SilentUpdateSourceBrushesForActiveControl();
                }

                // jeśli kontrolka zamknięta
                if ((StateTypes)this.State == StateTypes.Closed)
                {
                    // przenieś kolory źródłowe (kontrolki) na wyjścia
                    foreach (var output in OutputNodesCollection)
                    {
                        output.AddRangeToSourceBrushes(this.SourceBrushes);
                    }
                }
                // jeśli kontrolka nie zamknięta
                if ((StateTypes)this.State != StateTypes.Closed)
                {
                    var sourceBrushesDict = this.ChcekSourcesForOutputs();

                    // usuń kolory z wyjść (wszystkie które nie należały do tego wyjścia)
                    foreach (var output in OutputNodesCollection)
                    {
                        var brushesNotFromThisOutput = new ObservableCollection<Brush>();
                        //wybierz kolory które nie naleą do tego wyjscia
                        foreach (var outputSavedBrushes in _OldOutputsSourcesColors)
                        {
                            // nie dodawaj do listy kolorów do usunięcia koloru który należał do tego wyjscia
                            if (outputSavedBrushes.Key != output.Name)
                            {
                                foreach (var brush in outputSavedBrushes.Value)
                                {
                                    if (!sourceBrushesDict[output.Name].Any(x => x.SourceBrush == brush))
                                        brushesNotFromThisOutput.Add(brush);
                                }
                            }
                        }

                        output.RemoveRangeToSourceBrushes(brushesNotFromThisOutput);
                    }
                }
            }
        }

        /// <summary>
        /// Przenosi kolory z wyjść do kontrolki po cichu (bez wywoływania zdarzeń)
        /// </summary>
        private void SilentUpdateSourceBrushesForActiveControl()
        {
            foreach (var output in OutputNodesCollection)
            {
                foreach (var brush in output.SourceBrushes)
                {
                    if (!this.SourceBrushes.Contains(brush) && brush != Settings.DefaultLineColor)
                        this.SourceBrushes.Add(brush);
                }
            }
        }
        #endregion

        #region overrides
        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            if (this.IsClickCommandBinded)
            {
                this.AddMouseEnterBorder();
            }
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            this.RemoveMouseEnterBorder();
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            if (this.ClickCommand is ICommand)
                ((ICommand)this.ClickCommand).Execute(this);

            //testmode
            //if (State >= 2)
            //    State = 0;
            //else
            //    State++;
        }

        protected override void AdditionalMethodOnPositionChanged()
        {
            base.AdditionalMethodOnPositionChanged();
            this.UpdateMouseEnterBroder();
        }

        protected override void MethodAfterAddBrushToSourceBrushes(Brush addedBrush)
        {
            if (OutputNodesCollection != null)
            {
                foreach (var output in OutputNodesCollection)
                {
                    // jeśli jest zamknięta zachowuje się jak pasywna
                    if (!MixColors.CompareTwoBrushes(this.SourceBrushes, output.SourceBrushes) && (StateTypes)State == StateTypes.Closed)
                    {
                        output.AddBrushToSourceBrushes(addedBrush);
                    }
                    // jeśli jest nie zamknięta, akcje dla kontrolki aktywnaj
                    else if ((StateTypes)State != StateTypes.Closed)
                    {
                        this.SourceBrushes.Clear();
                    }
                }

            }
        }

        protected override void MethodAfterRemoveBrushToSourceBrushes(Brush removedBrush)
        {
            if (OutputNodesCollection != null)
            {
                foreach (var output in OutputNodesCollection)
                {
                    // jeśli jest zamknięta zachowuje się jak pasywna
                    if (!MixColors.CompareTwoBrushes(this.SourceBrushes, output.SourceBrushes) && (StateTypes)State == StateTypes.Closed)
                    {
                        output.RemoveBrushFromSourceBrushes(removedBrush);
                    }
                    // jeśli jest nie zamknięta, akcje dla kontrolki aktywnaj
                    else if ((StateTypes)State != StateTypes.Closed)
                    {
                        this.SourceBrushes.Clear();
                    }
                }
            }
        }

        protected override void MethodAfterClearSourceBrushes()
        {
            if (OutputNodesCollection != null)
            {
                foreach (var output in OutputNodesCollection)
                {
                    // jeśli jest zamknięta zachowuje się jak pasywna
                    if (output.SourceBrushes.Any() && (StateTypes)State == StateTypes.Closed)
                    {
                        output.ClearSourceBrushes();
                    }
                    // jeśli jest nie zamknięta, akcje dla kontrolki aktywnaj
                    else if ((StateTypes)State != StateTypes.Closed)
                    {
                        this.SourceBrushes.Clear();
                    }
                }
            }
        }

        protected override void MethodAfterAddRangeToSourceBrushes(IList<Brush> listToAdd)
        {
            if (OutputNodesCollection != null)
            {
                foreach (var output in OutputNodesCollection)
                {
                    // jeśli jest zamknięta zachowuje się jak pasywna
                    if (!MixColors.CompareTwoBrushes(this.SourceBrushes, output.SourceBrushes) && (StateTypes)State == StateTypes.Closed)
                    {
                        output.AddRangeToSourceBrushes(listToAdd);
                    }
                    // jeśli jest nie zamknięta, akcje dla kontrolki aktywnaj
                    else if ((StateTypes)State != StateTypes.Closed)
                    {
                        this.SourceBrushes.Clear();
                    }
                }
            }
        }

        protected override void MethodAfterRemoveRangeToSourceBrushes(IList<Brush> brushesToRemove)
        {
            if (OutputNodesCollection != null)
            {
                foreach (var output in OutputNodesCollection)
                {
                    // jeśli jest zamknięta zachowuje się jak pasywna
                    if (MixColors.CheckIfBrushesContainsAnotherBrushes(output.SourceBrushes, brushesToRemove) && (StateTypes)State == StateTypes.Closed)
                    {
                        // kopia bardzo potrzebna - bez niej opracje usunięcia ciągle byłyby wykonywane na tej samej tablicy
                        var copyOfBrushesToRemove = new List<Brush>(brushesToRemove);

                        // usunięcia kolorów z SourceBrushes wyjścia
                        output.RemoveRangeToSourceBrushes(copyOfBrushesToRemove);
                    }
                    // jeśli jest nie zamknięta, akcje dla kontrolki aktywnaj
                    else if ((StateTypes)State != StateTypes.Closed)
                    {
                        this.SourceBrushes.Clear();
                    }
                }
            }
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            this.InvalidateVisual();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
        }

        public ActiveElement()
        {
            this.OutputNodesChangedEventHandler += OnOutputNodesChanged;
        }
        #endregion

        #region połaczenie do źródła
        private List<string> checkedControl = new List<string>();
        /// <summary>
        /// sprawdza po kolei czy wyjście kontrolki jest połączone ze źródłem
        /// </summary>
        /// <returns>słownik nazwa wyjścia, lista źródeł</returns>
        private Dictionary<string, List<ISource>> ChcekSourcesForOutputs()
        {            
            var resultDictionary = new Dictionary<string, List<ISource>>();

            foreach (var output in this.OutputNodesCollection)
            {
                checkedControl.Clear();
                resultDictionary.Add(output.Name, output.CheckSourceConnection(output, checkedControl));
            }

            return resultDictionary;
        }
        #endregion
    }
}
