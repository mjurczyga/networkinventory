﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.Core
{
    public abstract class GraphicsControl : MultiSourceBrushControl, IEditableControl
    {


        protected abstract void DrawControl(DrawingContext drawingContext);

        protected virtual void FlipGraphicChanged(bool newValue) { }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            this.InvalidateVisual();
        }

        protected sealed override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);
            if(!ValueChecker.CheckDoubleNanInfOrZero(this.Width) && !ValueChecker.CheckDoubleNanInfOrZero(this.Height))
                DrawControl(drawingContext);
        }

        /// <summary>
        /// Domyślna szerokość kontrolki
        /// </summary>
        protected abstract double DefaultWidth { get; }

        /// <summary>
        /// Metoda wywoływana gdy zmieni się pozycja kontrolki w SchemeCanvas
        /// </summary>
        protected abstract void PositionInCanvasChanged(object sender, EventArgs e);


        #region Editable control
        //[EditorProperty("Tryb edycji")]
        public bool IsEditMode
        {
            get { return (bool)GetValue(IsEditModeProperty); }
            set { SetValue(IsEditModeProperty, value); }
        }

        public static readonly DependencyProperty IsEditModeProperty =
            DependencyProperty.Register("IsEditMode", typeof(bool), typeof(GraphicsControl), new PropertyMetadata(false, IsEditMode_PropertyChanged));

        private static void IsEditMode_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            GraphicsControl graphicsControl = (GraphicsControl)d;
            graphicsControl.ChangeEditMode((bool)e.NewValue);
        }

        /// <summary>
        /// actions for changed Edit mode
        /// </summary>
        /// <param name="isEditMode"></param>
        protected virtual void ChangeEditMode(bool isEditMode)
        {

        }

        /// <summary>
        /// metoda usuwająca kontroklę z UI(SchemeCanvas)
        /// </summary>
        public abstract void Remove();

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public object Copy()
        {
            var copiedElement = Activator.CreateInstance(this.GetType()) as GraphicsControl;

            var thisDependencyProperties = DependencyPropertyFinder.GetAllDependencyProperties(this);
            foreach (var property in thisDependencyProperties)
            {
                if (!property.ReadOnly && property.Name.ToUpper() != "NAME")
                    copiedElement.SetValue(property, this.GetValue(property));
                else if (!property.ReadOnly && property.Name.ToUpper() != "NAME" && this.Parent is Canvas)
                    copiedElement.SetValue(property, CanvasUtilities.GetNextNewElementName((Canvas)this.Parent));
            }

            var eventsFields = DependencyPropertyFinder.GetAllRoutedEvents(this);

            foreach (var eventField in eventsFields)
            {
                copiedElement.AddHandler((RoutedEvent)eventField.Key.GetValue(this), eventField.Value.Handler);
            }

            copiedElement.SetPosition(this.X_Position, this.Y_Position); 
            return copiedElement;
        }
        #endregion Editable control

        #region Dependency properties
        [EditorProperty("Odwrócona grafika")]
        public bool FlipGraphics
        {
            get { return (bool)GetValue(FlipGraphicsProperty); }
            set { SetValue(FlipGraphicsProperty, value); }
        }

        public static readonly DependencyProperty FlipGraphicsProperty =
            DependencyProperty.Register("FlipGraphics", typeof(bool), typeof(GraphicsControl), new PropertyMetadata(false, FlipGraphics_PropertyChanged));

        private static void FlipGraphics_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var element = (GraphicsControl)d;
            element.FlipGraphicChanged((bool)e.NewValue);
            element.InvalidateVisual();

        }
        #endregion

        #region overrides
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();            
        }
        #endregion

        #region konstruktory
        public GraphicsControl()
        {
            this.SetValue(GraphicsControl.WidthProperty, DefaultWidth);
            this.SnapsToDevicePixels = true;
        }
        #endregion
    }
}