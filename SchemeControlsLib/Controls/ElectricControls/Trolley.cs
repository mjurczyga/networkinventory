﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces.ElementsInterfaces;
using SchemeControlsLib.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorElement(true, "Wózek")]
    public class Trolley : ActiveElement, ITrolley
    {
        #region
        private TrolleyBase _tB = new TrolleyBase();
        #endregion
        protected override double WidthToHeightControlRatio => 1.0 / 2.0;

        protected override double DefaultWidth => 29.0;

        private double SwitchWidthToHeightRatio = 3.0 / 4.0;

        protected override void DrawControl(DrawingContext drawingContext)
        {
            DrawShape(drawingContext);
        }

        public void DrawShape(DrawingContext drawingContext)
        {
            if (drawingContext != null)
            {
                var width = this.Width; var height = this.ActualHeight; Brush stateBackgroundBrush = Brushes.Black;

                Brush trolleyStateBackgroundBrush = StateToBrushConverter(this.State);
                double radius = ControlOrientation == Orientation.Horizontal ? width / 4.5 : height / 4.5;
                DrawTrolleyBackground(drawingContext, width, height, trolleyStateBackgroundBrush, new CornerRadius(radius));

                var pen = new Pen(Brushes.White, Settings.LinesStrokeThickness);
                var bigPen = new Pen(Brushes.White, Settings.ElementStrokeThickness);

                // wyliczenie położenia łącznika
                double switchHeight; double switchWidth; Thickness switchMargin; double openTrolleyDistance = 0;
                if (ControlOrientation == Orientation.Horizontal)
                {
                    if ((StateTypes)this.State == StateTypes.Open)
                        openTrolleyDistance = width * 0.3;

                    switchWidth = width;
                    switchHeight = switchWidth / SwitchWidthToHeightRatio;
                    switchMargin = new Thickness(openTrolleyDistance, (height - switchHeight) / 2.0, 0, 0);
                }
                else
                {
                    if ((StateTypes)this.State == StateTypes.Open)
                        openTrolleyDistance = height * 0.3;

                    switchHeight = height;
                    switchWidth = switchHeight / SwitchWidthToHeightRatio;
                    switchMargin = new Thickness((width - switchWidth) / 2.0, openTrolleyDistance, 0, 0);
                }

                // Lacznik
                

                switch (this.State)
                {
                    case 0: DrawQuestionMark(drawingContext, switchWidth, switchHeight, FlipGraphics, ControlOrientation, bigPen, switchMargin); break;
                    case 1: this.DrawTrolleyInside(drawingContext, switchWidth, switchHeight, FlipGraphics, ControlOrientation, bigPen, switchMargin); break;
                    case 2: this.DrawTrolleyInside(drawingContext, switchWidth, switchHeight, FlipGraphics, ControlOrientation, bigPen, switchMargin); break;
                    default: break;
                }

                // wózek
                DrawTrolleyTop(drawingContext, width, height, FlipGraphics, ControlOrientation, pen);
                DrawTrolleyBottom(drawingContext, width, height, FlipGraphics, ControlOrientation, pen);
            }
        }

        private void DrawTrolleyInside(DrawingContext drawingContext, double width, double height, bool flipGraphics, Orientation controlOrientation, Pen pen, Thickness margin)
        {
            double horizontalPosition = margin.Top - margin.Bottom;
            double verticalPosiotion = margin.Left - margin.Right;

            if (controlOrientation == Orientation.Vertical)
            {
                double rectangleDistance = height / 20;
                //prawa
                drawingContext.DrawLine(pen, new Point(width + verticalPosiotion, height / 2 + horizontalPosition), new Point(5 * width / 6 + verticalPosiotion, height / 2 + horizontalPosition));
                drawingContext.DrawLine(pen, new Point(width + rectangleDistance + verticalPosiotion, height / 2 - rectangleDistance * 2 + horizontalPosition), new Point(width + verticalPosiotion, height / 2 + horizontalPosition));
                drawingContext.DrawLine(pen, new Point(width + rectangleDistance + verticalPosiotion, height / 2 + rectangleDistance * 2 + horizontalPosition), new Point(width + verticalPosiotion, height / 2 + horizontalPosition));

                //lewa
                drawingContext.DrawLine(pen, new Point(0 + verticalPosiotion, height / 2 + horizontalPosition), new Point(width / 6 + verticalPosiotion, height / 2 + horizontalPosition));
                drawingContext.DrawLine(pen, new Point(-rectangleDistance + verticalPosiotion, height / 2 - rectangleDistance * 2 + horizontalPosition), new Point(0 + verticalPosiotion, height / 2 + horizontalPosition));
                drawingContext.DrawLine(pen, new Point(-rectangleDistance + verticalPosiotion, height / 2 + rectangleDistance * 2 + horizontalPosition), new Point(0 + verticalPosiotion, height / 2 + horizontalPosition));

                drawingContext.DrawLine(pen, new Point(width / 6 + verticalPosiotion, height / 2 + horizontalPosition), new Point(5 * width / 6 + verticalPosiotion, height / 2 + horizontalPosition));
            }
            else
            {
                double rectangleDistance = width / 20;
                //góra
                drawingContext.DrawLine(pen, new Point(width / 2 + verticalPosiotion, height + horizontalPosition), new Point(width / 2 + verticalPosiotion, 5 * height / 6 + horizontalPosition));
                drawingContext.DrawLine(pen, new Point(width / 2 - rectangleDistance * 2 + verticalPosiotion, height + rectangleDistance + horizontalPosition), new Point(width / 2 + verticalPosiotion, height + horizontalPosition));
                drawingContext.DrawLine(pen, new Point(width / 2 + rectangleDistance * 2 + verticalPosiotion, height + rectangleDistance + horizontalPosition), new Point(width / 2 + verticalPosiotion, height + horizontalPosition));

                //dół
                drawingContext.DrawLine(pen, new Point(width / 2 + verticalPosiotion, 0 + horizontalPosition), new Point(width / 2 + verticalPosiotion, height / 6 + horizontalPosition));
                drawingContext.DrawLine(pen, new Point(width / 2 - rectangleDistance * 2 + verticalPosiotion, -rectangleDistance + horizontalPosition), new Point(width / 2 + verticalPosiotion, 0 + horizontalPosition));
                drawingContext.DrawLine(pen, new Point(width / 2 + rectangleDistance * 2 + verticalPosiotion, -rectangleDistance + horizontalPosition), new Point(width / 2 + verticalPosiotion, 0 + horizontalPosition));

                drawingContext.DrawLine(pen, new Point(width / 2 + verticalPosiotion, height / 6 + horizontalPosition), new Point(width / 2 + verticalPosiotion, 5 * height / 6 + horizontalPosition));
            }
        
        }


        private void DrawQuestionMark(DrawingContext drawingContext, double width, double height, bool flipGraphics, Orientation controlOrientation, Pen pen, Thickness switchMargin)
        {
            double horizontalPosition = switchMargin.Top - switchMargin.Bottom;
            double verticalPosiotion = switchMargin.Left - switchMargin.Right;

            double qMarkWidth, qMarkHeight; Size size;

            if (controlOrientation == Orientation.Horizontal) { qMarkWidth = width; qMarkHeight = height; } else { qMarkWidth = height; qMarkHeight = width; };
            double offset = 0.05 * qMarkWidth;
            double xDistance = qMarkWidth / 2.0 - qMarkWidth / 2.5;
            double yDistance = qMarkHeight / 2.0 - (2.9 * qMarkHeight / 8.0 + offset);

            Point startPoint = new Point(width / 2.0 - xDistance + verticalPosiotion, height / 2.0 - yDistance + horizontalPosition);
            size = new Size(xDistance, Math.Abs(yDistance));

            Point lineEndPoint = new Point(size.Width + startPoint.X, size.Height + startPoint.Y + size.Height);

            var geometry = new StreamGeometry();
            using (var context = geometry.Open())
            {
                bool isStroked = pen != null;
                const bool isSmoothJoin = true;

                context.BeginFigure(startPoint, false, false);
                context.ArcTo(new Point(size.Width + startPoint.X, size.Height + startPoint.Y),
                    size,
                    0, true, SweepDirection.Clockwise, isStroked, isSmoothJoin);
                context.LineTo(lineEndPoint, isStroked, isSmoothJoin);

                context.Close();
            }

            drawingContext.DrawGeometry(null, pen, geometry);

            // kropka
            drawingContext.DrawLine(pen, new Point(width / 2.0 - 0.5 + verticalPosiotion, lineEndPoint.Y + 2.0), new Point(width / 2.0 + 0.5 + verticalPosiotion, lineEndPoint.Y + 2.0));


        }

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();
            var offset = 1.0;
            if (ControlOrientation == Orientation.Vertical)
            {
                OutputPointsCollection.Add(new Point(-offset, this.Height / 2.0));
                OutputPointsCollection.Add(new Point(this.Width + offset, this.Height / 2.0));
            }
            else
            {
                OutputPointsCollection.Add(new Point(this.Width / 2.0, -offset));
                OutputPointsCollection.Add(new Point(this.Width / 2.0, this.Height + offset));
            }
        }
        #region wózek
        public void DrawTrolleyTop(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, double distance = 8.0)
        {
            _tB.DrawTrolleyTop(drawingContext, width, height, flippedGraphic, controlOrientation, pen);
        }

        public void DrawTrolleyBottom(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, double distance = 8.0)
        {
            _tB.DrawTrolleyBottom(drawingContext, width, height, flippedGraphic, controlOrientation, pen);
        }

        public void DrawTrolleyBackground(DrawingContext drawingContext, double width, double height, Brush stateBackgroundBrush, CornerRadius cornerRadius)
        {
            _tB.DrawTrolleyBackground(drawingContext, width, height, stateBackgroundBrush, cornerRadius);
        }

        public void DrawTrolley(DrawingContext drawingContext, double width, double height, bool flippedGraphic, Orientation controlOrientation, Pen pen, Thickness margin)
        {
            _tB.DrawTrolley(drawingContext, width, height, flippedGraphic, controlOrientation, pen, margin);
        }
        #endregion
    }
}
