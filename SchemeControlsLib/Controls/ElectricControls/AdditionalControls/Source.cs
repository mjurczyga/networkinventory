﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Events;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, true, "Źródło")]
    public sealed class Source : GraphicsControl, ISelectableElement, IMovableElement, IMovableSelectionElement, ISource, IGroupSelectableElement
    {
        public new bool FlipGraphics { get { return base.FlipGraphics; } set { base.FlipGraphics = value; } }

        #region IsSelectionMoving
        private bool _IsSelectionMoving;
        public bool IsSelectionMoving
        {
            get { return _IsSelectionMoving; }
            set { _IsSelectionMoving = value; NotifyPropertyChanged("IsSelectionMoving"); }
        }
        #endregion IsSelectionMoving

        protected override double DefaultWidth => 19.0;

        private Point _outputPoint { get { return new Point(this.Width / 2.0, this.Height / 2.0); } }

        public MultiOutputNode MultiOutputNode { get; private set; }

        #region Dependency properties
        [EditorProperty("Numer źródła")]
        public int SourceNumber
        {
            get { return (int)GetValue(SourceNumberProperty); }
            set { SetValue(SourceNumberProperty, value); }
        }

        public static readonly DependencyProperty SourceNumberProperty =
            DependencyProperty.Register("SourceNumber", typeof(int), typeof(Source));

        [EditorProperty("Opis źródła")]
        public string SourceDescription
        {
            get { return (string)GetValue(SourceDescriptionProperty); }
            set { SetValue(SourceDescriptionProperty, value); }
        }

        public static readonly DependencyProperty SourceDescriptionProperty =
            DependencyProperty.Register("SourceDescription", typeof(string), typeof(Source));


        [EditorProperty("Widoczność grafiki")]
        public bool IsGraphicsVisible
        {
            get { return (bool)GetValue(IsGraphicsVisibleProperty); }
            set { SetValue(IsGraphicsVisibleProperty, value); }
        }

        public static readonly DependencyProperty IsGraphicsVisibleProperty =
            DependencyProperty.Register("IsGraphicsVisible", typeof(bool), typeof(Source), new PropertyMetadata(true, IsGraphicsVisible_PropertyChanged));

        private static void IsGraphicsVisible_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Source source = (Source)d;
            source.InvalidateVisual();
            if (source.MultiOutputNode != null)
                source.MultiOutputNode.HideElement = !(bool)e.NewValue;
        }

        [EditorProperty("Kolor źródła")]
        public Brush SourceBrush
        {
            get { return (Brush)GetValue(SourceBrushProperty); }
            set { SetValue(SourceBrushProperty, value); }
        }

        public static readonly DependencyProperty SourceBrushProperty =
            DependencyProperty.Register("SourceBrush", typeof(Brush), typeof(Source), new PropertyMetadata(Settings.DefaultLineColor, SourceColor_PropertyChanged));

        private static void SourceColor_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Source source = (Source)d;
            if (e.NewValue != Settings.DefaultLineColor)
                source.AddBrushToSourceBrushes((Brush)e.NewValue);

            source.RemoveBrushFromSourceBrushes((Brush)e.OldValue);
            source.InvalidateVisual();
        } 
        #endregion

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();         
        }

        protected override void DrawControl(DrawingContext drawingContext)
        {            
            if (drawingContext != null && this.IsGraphicsVisible)
            {
                var diameterX = Math.Round(this.ActualWidth / 2.0);
                var diameterY = Math.Round(this.ActualHeight / 2.0);

                var pen = new Pen(Settings.DefaultLineColor, Settings.ElementStrokeThickness);

                Brush brush = Settings.DefaultLineColor;
                if (this.SourceBrushes != null && this.SourceBrushes.Count != 0)
                {
                    brush = MixColors.GenerateTwoColorBrushWithLength(this.SourceBrushes, diameterX, true);
                }
                else
                    brush = SourceBrush;

                Typeface typeFace = new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Normal, FontStretches.Normal);
                FormattedText formattedText = new FormattedText("S", CultureInfo.CurrentUICulture, FlowDirection.LeftToRight, typeFace, Math.Abs(diameterX * 2.0 - 1.0), brush);
                Point textLocation = new Point(diameterX - formattedText.WidthIncludingTrailingWhitespace / 2.0, formattedText.LineHeight);


                drawingContext.DrawEllipse(null, pen, new Point(diameterX, diameterY), diameterX, diameterY);
                drawingContext.DrawText(formattedText, textLocation);
            }
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            //sprawdzenie czy została ustawiona właściwość Height
            if (sizeInfo.HeightChanged && !ValueChecker.CheckDoubleNanInfOrZero(this.Height))
                this.Width = sizeInfo.NewSize.Height;
            else if (sizeInfo.WidthChanged && !ValueChecker.CheckDoubleNanInfOrZero(this.Width))
                this.Height = sizeInfo.NewSize.Width;

            this.AddOrUpdateMultiOutputNode();
        }

        static Source()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Source), new FrameworkPropertyMetadata(typeof(Source)));
        }

        public Source()
        {
            //this.SourceBrushes = ObservableCollection<Brush>();
            this.AddBrushToSourceBrushes(Settings.DefaultLineColor);
        }



        /// <summary>
        /// Metoda wywoływana gdy zmieni się pozycja kontrolki w SchemeCanvas
        /// </summary>
        protected override void PositionInCanvasChanged(object sender, EventArgs e)
        {
            var control = sender as Source;
            if (control != null)
            {
                control.AddOrUpdateMultiOutputNode();
            }
        }

        protected override void ControlPosition_Changed(double oldX_Position, double oldY_Position)
        {
            base.ControlPosition_Changed(oldX_Position, oldY_Position);
            this.AddOrUpdateMultiOutputNode();
        }

        private void AddOrUpdateMultiOutputNode()
        {
            if (MultiOutputNode == null && this.Parent is SchemeCanvas)
            {
                var controlSchameCanvas = (SchemeCanvas)this.Parent;

                var multiOutputNode = new MultiOutputNode(this);
                multiOutputNode.OutputNodesChangedEvent += OnMultiNodeSourceBrushesChanged;
                // Ustawienie pozycji wyjścia

                //POZYCJA
                multiOutputNode.SetPosition(CenterPoint.X - multiOutputNode.MultiOutputNodeSize.Width / 2.0, CenterPoint.Y - multiOutputNode.MultiOutputNodeSize.Height / 2.0);
                multiOutputNode.SetValue(FrameworkElement.NameProperty, this.Name + "_MultiOutput");
                controlSchameCanvas.Children.Add(multiOutputNode);

                MultiOutputNode = multiOutputNode;

                //update color
                if (this.SourceBrush != Settings.DefaultLineColor && multiOutputNode.SourceBrushes.Count == 0)
                    MultiOutputNode.AddBrushToSourceBrushes(SourceBrush);
            }
            else if (MultiOutputNode != null)
            {

                //POZYCJA
                MultiOutputNode.SetPosition(CenterPoint.X - MultiOutputNode.MultiOutputNodeSize.Width / 2.0, CenterPoint.Y - MultiOutputNode.MultiOutputNodeSize.Height / 2.0);
            }
        }

        private void OnMultiNodeSourceBrushesChanged(object sender, OutputNodesChangedEventArgs e)
        {
            if(sender != null && sender is MultiOutputNode)
            {
                var msn = (MultiOutputNode)sender;
                foreach(var output in msn.OutputNodes)
                {
                    if(output.SourceBrushes != null && output.SourceBrushes.Count == 0)
                    {
                        output.AddRangeToSourceBrushes(this.SourceBrushes);
                    }
                }
                //if(this.MultiOutputNode.SourceBrushes != null && this.MultiOutputNode.SourceBrushes.Count == 0)
                //{
                //    this.MultiOutputNode.AddRangeToSourceBrushes(this.SourceBrushes);
                //}
            }
        }

        public override void Remove()
        {
            if (this.Parent is SchemeCanvas)
            {
                var schemeCanvas = (SchemeCanvas)this.Parent;
                if(this.MultiOutputNode != null)
                    this.MultiOutputNode.Remove();
                schemeCanvas.Children.Remove(this);
            }
        }

        public void Connect(OutputNode outputNode)
        {
            if(this.MultiOutputNode != null)
            {
                this.MultiOutputNode.MakeConnection(outputNode);
            }
        }

        public void AddMultiOutputNodeSourceColors(Brush brush)
        {
            if(MultiOutputNode != null)
            {
                this.MultiOutputNode.AddBrushToSourceBrushes(brush);
            }
        }

        public void RemoveMultiOutputNodeSourceColors(Brush brush)
        {
            if (MultiOutputNode != null)
            {
                this.MultiOutputNode.RemoveBrushFromSourceBrushes(brush);
            }
        }

        private void AddSourceColorIfEmpty()
        {
            if(MultiOutputNode?.SourceBrushes != null && (MultiOutputNode?.SourceBrushes.Count == 0 || MultiOutputNode.SourceBrushes.Count == 1 && MultiOutputNode?.SourceBrushes[0] == Settings.DefaultLineColor))
            {
                 this.AddBrushToSourceBrushes(this.SourceBrush);
            }
        }

        protected override void MethodAfterRemoveRangeToSourceBrushes(IList<Brush> brushesToRemove)
        {
            base.MethodAfterRemoveRangeToSourceBrushes(brushesToRemove);
            this.AddSourceColorIfEmpty();
        }

        protected override void MethodAfterAddBrushToSourceBrushes(Brush addedBrush)
        {
            base.MethodAfterAddBrushToSourceBrushes(addedBrush);
            this.AddMultiOutputNodeSourceColors(addedBrush);
        }

        protected override void MethodAfterRemoveBrushToSourceBrushes(Brush removedBrush)
        {
            base.MethodAfterRemoveBrushToSourceBrushes(removedBrush);
            this.RemoveMultiOutputNodeSourceColors(removedBrush);
        }

        protected override void MethodAfterClearSourceBrushes()
        {
            base.MethodAfterClearSourceBrushes();

        }

    }
}
