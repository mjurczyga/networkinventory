﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.LibElementsControls;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, false, "Załamanie lini")]
    public sealed class LineConnector : PassiveElement, IHideableElement
    {
        #region dependency properties
        public bool HideElement
        {
            get { return (bool)GetValue(IsElementHiddenProperty); }
            set { SetValue(IsElementHiddenProperty, value); }
        }

        public static readonly DependencyProperty IsElementHiddenProperty =
            DependencyProperty.Register("HideElement", typeof(bool), typeof(LineConnector), new PropertyMetadata(true, HideElement_PropertyChanged));

        private static void HideElement_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            LineConnector lineConnector = (LineConnector)d;
            lineConnector.ShowOrHideOutput((bool)e.NewValue);
            lineConnector.InvalidateVisual();
        }
        #endregion

        protected override double WidthToHeightControlRatio => 1.0;

        protected override double DefaultWidth => 5;

        protected override void DrawControl(DrawingContext drawingContext)
        {
            if(drawingContext != null)
            {
                if(!this.HideElement)
                {
                    drawingContext.DrawRectangle(Brushes.DarkBlue, null, new Rect(new Size(this.Width, this.Height)));
                }
            }
        }

        protected override void ComputeOutputs()
        {
            base.ComputeOutputs();
            this.OutputPointsCollection.Add(new Point(this.Width / 2.0, this.Height / 2.0));
            this.OutputPointsCollection.Add(new Point(this.Width / 2.0, this.Height / 2.0));
        }

        #region public methods
        public void BreakTheLine(ILine schemeLine, SchemeCanvas schemeCanvas)
        {
            if (schemeCanvas != null)
            {
                this.SetIsHitTestVisibleToOutputs(false);

                var on1 = schemeLine.OutputNode1;
                var on2 = schemeLine.OutputNode2;
                var schemeLineStrokeThickness = schemeLine.StrokeThickness;
                schemeLine.Disconnect();

                this.HideElement = !schemeCanvas.IsEditMode;

                //pierwsza linia                            
                var newSchemeLine1 = new SchemeLine() { StrokeThickness = schemeLineStrokeThickness };
                newSchemeLine1.Connect(on1, this.OutputNodesCollection[0]);
                newSchemeLine1.ContextMenu = new SchemeElementContextMenu();
                schemeCanvas.Children.Add(newSchemeLine1);

                //druga linia
                var newSchemeLine2 = new SchemeLine() { StrokeThickness = schemeLineStrokeThickness };
                newSchemeLine2.Connect(on2, this.OutputNodesCollection[1]);
                newSchemeLine2.ContextMenu = new SchemeElementContextMenu();
                schemeCanvas.Children.Add(newSchemeLine2);                
            }
        }

        public void RemoveTheLinieBreak()
        {
            var on1 = FindOutputNode(this.OutputNodesCollection[0]);
            var on2 = FindOutputNode(this.OutputNodesCollection[1]);
            if(on1 is OutputNode outputNode1)
                outputNode1?.OutputLine?.Disconnect();
            if (on2 is OutputNode outputNode2)
                outputNode2?.OutputLine?.Disconnect();

            var schemeLine = new SchemeLine();

            if (on1 is OutputNode outputNode_1 && on2 is OutputNode outputNode_2)
            {
                schemeLine.Connect(on1, on2);

                var schemeCanvas = (SchemeCanvas)outputNode_1.Parent;
                schemeCanvas.AddSchemeElement(schemeLine, true);
            }
            this.Remove();

        }

        public OutputNode GetOpposedOutputNode(OutputNode outputNode)
        {
            if (OutputNodesCollection.Contains(outputNode))
                return this.OutputNodesCollection.Where(x => x != outputNode).FirstOrDefault();

            return null;
        }
        #endregion

        public LineConnector()
        {
            this.Opacity = 0.7;
        }

        #region Private methods
        private void ShowOrHideOutput(bool hideOutput)
        {
            foreach(var output in OutputNodesCollection)
            {
                output.HideElement = hideOutput;
            }
        }

        private void SetIsHitTestVisibleToOutputs(bool isHitTestVisible)
        {
            foreach (var output in OutputNodesCollection)
            {
                output.IsHitTestVisible = isHitTestVisible;
            }
        }

        /// <summary>
        /// Szuka wyjscia z linii które nie należy do tej kontrolki
        /// </summary>
        private IOutputNode FindOutputNode(OutputNode outputNode)
        {
            if (outputNode.OutputLine != null)
            {
                var schemeLine = outputNode.OutputLine;
                if (schemeLine != null)
                {
                    if (!schemeLine.OutputNode1.CenterPoint.ComparePoints(outputNode.CenterPoint))
                        return schemeLine.OutputNode1;
                    else
                        return schemeLine.OutputNode2;
                }
            }

            return null;
        }
        #endregion
    }
}
