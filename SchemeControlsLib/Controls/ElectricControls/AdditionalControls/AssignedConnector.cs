﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, true, "Ręczne przyłącze")]
    public class AssignedConnector : CommonControlBase, IMovableElement, IMovableSelectionElement, IBindingControl, IHideableElement
    {
        #region IsSelectionMoving
        private bool _IsSelectionMoving;
        public bool IsSelectionMoving
        {
            get { return _IsSelectionMoving; }
            set { _IsSelectionMoving = value; NotifyPropertyChanged("IsSelectionMoving"); }
        }
        #endregion IsSelectionMoving

        #region właściwości UI
        [EditorProperty("Zaokrąglenia")]
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(CommonControlBase));
        #endregion właściwości UI

        #region numer przyłącza
        [EditorProperty("Numer przypisanego przyłącza")]
        public object AssignedConnectorNumber
        {
            get { return (object)GetValue(AssignedConnectorNumberProperty); }
            set { SetValue(AssignedConnectorNumberProperty, value); }
        }

        public static readonly DependencyProperty AssignedConnectorNumberProperty =
            DependencyProperty.Register("AssignedConnectorNumber", typeof(object), typeof(AssignedConnector), new PropertyMetadata(AssignedConnectorNumber_PropertyChanged));

        private static void AssignedConnectorNumber_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is AssignedConnector assignedConnector)
            {
                if (assignedConnector.Parent is SchemeCanvas schemeCanvas && int.TryParse(e.NewValue?.ToString(), out int intValue))
                {
                    var sources = schemeCanvas.Children.OfType<ISource>().Where(x => x.SourceNumber == intValue).FirstOrDefault();
                    if (sources != null)
                    {
                        assignedConnector.InternalBackground = sources.SourceBrush;
                        assignedConnector.ToolTip = "Przypisanie ręczne: "  + sources.SourceDescription;
                    }
                    else
                    {
                        assignedConnector.InternalBackground = Brushes.White;
                        assignedConnector.ToolTip = "Barak danych";
                    }
                    
                }
                else
                {
                    assignedConnector.InternalBackground = Brushes.White;
                    assignedConnector.ToolTip = "Barak danych";
                }
            }
        }
        #endregion numer przyłącza

        #region Tryb przypisania
        [EditorProperty("Tryb przypisania")]
        public object AssignedConnectorMode
        {
            get { return (object)GetValue(AssignedConnectorModeProperty); }
            set { SetValue(AssignedConnectorModeProperty, value); }
        }

        public static readonly DependencyProperty AssignedConnectorModeProperty =
            DependencyProperty.Register("AssignedConnectorMode", typeof(object), typeof(AssignedConnector), new PropertyMetadata(AssignedConnectorMode_PropertyChanged));

        private static void AssignedConnectorMode_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is AssignedConnector assignedConnector && assignedConnector.IsEditMode == false)
            {
                if(assignedConnector.HandModeValue?.ToString() == e.NewValue?.ToString())
                    assignedConnector.Visibility = Visibility.Visible;
                else
                    assignedConnector.Visibility = Visibility.Collapsed;
            }
        }
        #endregion Tryb przypisania

        #region Wartosć dla trybu ręcznego

        [EditorProperty("Wartość dla trybu ręcznego")]
        public object HandModeValue
        {
            get { return (object)GetValue(HandModeValueProperty); }
            set { SetValue(HandModeValueProperty, value); }
        }

        public static readonly DependencyProperty HandModeValueProperty =
            DependencyProperty.Register("HandModeValue", typeof(object), typeof(AssignedConnector));  
        #endregion Wartosć dla trybu ręcznego

        private Brush _InternalBackground = Brushes.White;
        public Brush InternalBackground
        {
            get { return _InternalBackground; }
            set { _InternalBackground = value; NotifyPropertyChanged("InternalBackground"); }
        }


        #region IHideableElement
        public bool HideElement
        {
            get { return (bool)GetValue(HideElementProperty); }
            set { SetValue(HideElementProperty, value); }
        }

        public static readonly DependencyProperty HideElementProperty =
            DependencyProperty.Register("HideElement", typeof(bool), typeof(AssignedConnector), new PropertyMetadata(false, HideElement_PropertyChanged));

        private static void HideElement_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is AssignedConnector assignedConnector)
            {
                assignedConnector.IsEditMode = !(bool)e.NewValue;
            }
        }
        #endregion IHideableElement


        protected override void DrawControl(DrawingContext drawingContext)
        {

        }


        protected override void ChangeEditMode(bool isEditMode)
        {
            base.ChangeEditMode(isEditMode);

            this.SetVisibility(isEditMode);
        }

        private void SetVisibility(bool isVisible)
        {
            if (this.IsEditMode || !this.HideElement)
                this.Visibility = Visibility.Visible;
            else
            {
                if (HandModeValue != null && AssignedConnectorMode != null && this.HandModeValue?.ToString() == this.AssignedConnectorMode?.ToString())
                    this.Visibility = Visibility.Visible;
                else
                    this.Visibility = Visibility.Collapsed;
            }
        }

        static AssignedConnector()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(AssignedConnector), new FrameworkPropertyMetadata(typeof(AssignedConnector)));
        }
    }
}
