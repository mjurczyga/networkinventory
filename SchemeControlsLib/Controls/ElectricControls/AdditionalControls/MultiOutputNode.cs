﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Events;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, false, "Wielokrotne wyjście kontrolki")]
    public sealed class MultiOutputNode : GraphicsControl, IHideableElement
    {
        protected override void DrawControl(DrawingContext drawingContext)
        {           
        }

        public event EventHandler<OutputNodesChangedEventArgs> OutputNodesChangedEvent;

        public bool HideElement
        {
            get { return (bool)GetValue(IsElementHiddenProperty); }
            set { SetValue(IsElementHiddenProperty, value); }
        }

        public static readonly DependencyProperty IsElementHiddenProperty =
            DependencyProperty.Register("HideElement", typeof(bool), typeof(MultiOutputNode), new PropertyMetadata(true, HideElement_PropertyChanged));

        private static void HideElement_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MultiOutputNode outputNode = (MultiOutputNode)d;
            outputNode.ShowOrHideOutput((bool)e.NewValue);
        }

        public ObservableCollection<OutputNode> OutputNodes { get; private set; }

        public FrameworkElement ControlParent { get; private set; }

        protected override double DefaultWidth => 6.0;

        public Size MultiOutputNodeSize = new Size(6.0, 6.0);

        public MultiOutputNode(FrameworkElement controlParent)
        {
            ControlParent = controlParent;
            this.IsHitTestVisible = false;
            OutputNodes = new ObservableCollection<OutputNode>();
        }

        public override void Remove()
        {
            if (this.Parent is SchemeCanvas)
            {
                SchemeCanvas schemeCanvas = (SchemeCanvas)this.Parent;
                var tempOutputs = new ObservableCollection<OutputNode>(OutputNodes);
                foreach (var output in tempOutputs)
                {
                    output.Remove();
                }
                OutputNodes.Clear();
                tempOutputs.Clear();
                schemeCanvas.Children.Remove(this);
            }
        }

        public void MakeConnection(OutputNode connectionWith)
        {
            var schemeCanvas = (SchemeCanvas)this.Parent;
            OutputNode outputNode = new OutputNode(this) { HideElement = true, IsHitTestVisible = false };
            this.AddOutput(outputNode);
            var schemeLine = new SchemeLine() { Stroke = Settings.DefaultLineColor, StrokeThickness = Settings.LinesStrokeThickness };
            schemeLine.Connect(outputNode, connectionWith);
            // dodanie context menu do linii
            if(connectionWith.ContextMenu != null)
                schemeLine.ContextMenu = Activator.CreateInstance(connectionWith.ContextMenu.GetType()) as ContextMenu;

            schemeCanvas.Children.Add(schemeLine);
            if(SourceBrushes.Count > 0)
                outputNode.AddRangeToSourceBrushes(this.SourceBrushes);
        }

        public void DeleteConnection(OutputNode outputNode)
        {
            this.RemoveOutput(outputNode);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.Height = DefaultWidth;
            AddParentClickHandler();
            ShowOrHideOutput(true);
        }

        protected override void PositionInCanvasChanged(object sender, EventArgs e)
        {
            this.UpdateOutputPosition();
        }

        protected override void ControlPosition_Changed(double oldX_Position, double oldY_Position)
        {
            base.ControlPosition_Changed(oldX_Position, oldY_Position);
            this.UpdateOutputPosition();
        }

        private void AddParentClickHandler()
        {
            if (this.ControlParent != null)
            {
                var eventHandlers = DependencyPropertyFinder.GetSpecyficRoutedEvents(this.ControlParent, this.ControlParent.GetType().GetField("MouseDownEvent", BindingFlags.Static | BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy));
                if (eventHandlers != null)
                {
                    foreach (var eventHandler in eventHandlers)
                    {
                        this.AddHandler(OutputNode.MouseDownEvent, eventHandler.Handler);
                    }
                }
                if (this.ControlParent.ContextMenu != null)
                    this.ContextMenu = Activator.CreateInstance(this.ControlParent.ContextMenu.GetType()) as ContextMenu;
            }
        }

        private void ShowOrHideOutput(bool hideOutput)
        {
            if (hideOutput)
            {
                this.Opacity = 1;
                this.Background = Brushes.Transparent;
            }
            else
            {
                this.Opacity = 0.7;
                this.Background = Brushes.Red;
            }
        }

        /// <summary>
        /// Dodaje wyjścia kontrolki do UI
        /// </summary>
        private void AddOutput(OutputNode outputNode)
        {
            if (this.Parent is SchemeCanvas)
            {
                SchemeCanvas schemeCanvas = (SchemeCanvas)this.Parent;
                // Ustawienie pozycji wyjścia 

                //POZYCJA
                outputNode.SetPosition(CenterPoint.X - outputNode.OutputNodeSize.Width / 2.0, CenterPoint.Y - outputNode.OutputNodeSize.Height / 2.0);

                outputNode.SetValue(FrameworkElement.NameProperty, this.Name + "_OutputX" + OutputNodes.Count());

                OutputNodes.Add(outputNode);
                NotifyPropertyChanged(() => OutputNodes);
                schemeCanvas.Children.Add(outputNode);
            }
        }

        /// <summary>
        ///  usunięcie wyjscia z kontrolki
        /// </summary>
        /// <param name="outputNode"></param>
        private void RemoveOutput(OutputNode outputNode)
        {
            if (this.Parent is SchemeCanvas)
            {
                SchemeCanvas schemeCanvas = (SchemeCanvas)this.Parent;
                if (OutputNodes.Any(x => x == outputNode))
                {
                    OutputNodes.Remove(outputNode);
                    outputNode.Remove();
                    this.ClearSourceBrushes();
                }
            }
        }

        public void RemoveOutputFromList(OutputNode outputNode)
        {
            if (this.Parent is SchemeCanvas)
            {
                SchemeCanvas schemeCanvas = (SchemeCanvas)this.Parent;
                if (OutputNodes.Any(x => x == outputNode))
                {
                    OutputNodes.Remove(outputNode);
                    schemeCanvas.Children.Remove(outputNode);
                    NotifyPropertyChanged(() => OutputNodes);
                }
            }
        }

        private void UpdateOutputPosition()
        {
            if (OutputNodes != null)
            {
                foreach (var outputNode in OutputNodes)
                {
                    //POZYCJA
                    outputNode.SetPosition(CenterPoint.X - outputNode.OutputNodeSize.Width / 2.0, CenterPoint.Y - outputNode.OutputNodeSize.Height / 2.0);
                }
            }
        }

        static MultiOutputNode()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MultiOutputNode), new FrameworkPropertyMetadata(typeof(MultiOutputNode)));
        }

        public void AddOutputNodeSourceBrushes(Brush brush)
        {
            if(this.OutputNodes != null && this.OutputNodes.Count > 0)
            {
                var oldValue = new ObservableCollection<OutputNode>(this.OutputNodes);
                foreach(var output in this.OutputNodes)
                {
                    output.AddBrushToSourceBrushes(brush);
                }

                if (OutputNodesChangedEvent != null)
                {
                    var eventArgs = new OutputNodesChangedEventArgs(oldValue, new ObservableCollection<OutputNode>(this.OutputNodes));
                    OutputNodesChangedEvent(this, eventArgs);
                }
            }
        }

        public void RemoveOutputNodeSourceBrushes(Brush brush)
        {
            if (this.OutputNodes != null && this.OutputNodes.Count > 0)
            {
                var oldValue = new ObservableCollection<OutputNode>(this.OutputNodes);
                foreach (var output in this.OutputNodes)
                {
                    output.RemoveBrushFromSourceBrushes(brush);
                }
                if (OutputNodesChangedEvent != null)
                {
                    var eventArgs = new OutputNodesChangedEventArgs(oldValue, new ObservableCollection<OutputNode>(this.OutputNodes));
                    OutputNodesChangedEvent(this, eventArgs);
                }
            }
        }

        private void ClearOutputNodeSourceBrushes()
        {
            if (this.OutputNodes != null && this.OutputNodes.Count > 0)
            {
                var oldValue = new ObservableCollection<OutputNode>(this.OutputNodes);
                foreach (var output in this.OutputNodes)
                {
                    output.ClearSourceBrushes();
                }
                if (OutputNodesChangedEvent != null)
                {
                    var eventArgs = new OutputNodesChangedEventArgs(oldValue, new ObservableCollection<OutputNode>(this.OutputNodes));
                    OutputNodesChangedEvent(this, eventArgs);
                }
            }
            /*if (this.SourceBrushes.Count == 0 && this.OutputNodes.Select(x => x.SourceBrushes.Count > 0).Any())
                this.AddBrushToSourceBrushes(((Source)this.ControlParent).SourceBrush);*/
        }

        private void AddRangeFromConectedElementSourceBrushes(IList<Brush> addedBrushes)
        {
            var oldValue = new ObservableCollection<OutputNode>(this.OutputNodes);
            if (this.OutputNodes != null && this.OutputNodes.Count > 0)
            {
                foreach (var output in this.OutputNodes)
                {
                    output.AddRangeToSourceBrushes(addedBrushes);
                }
                if (OutputNodesChangedEvent != null)
                {
                    var eventArgs = new OutputNodesChangedEventArgs(oldValue, new ObservableCollection<OutputNode>(this.OutputNodes));
                    OutputNodesChangedEvent(this, eventArgs);
                }
            }
        }

        private void RemoveRangeFromConectedElementSourceBrushes(IList<Brush> brushesToRemove)
        {
            var oldValue = new ObservableCollection<OutputNode>(this.OutputNodes);
            if (this.OutputNodes != null && this.OutputNodes.Count > 0)
            {
                foreach (var output in this.OutputNodes)
                {
                    if(MixColors.CheckIfBrushesContainsAnotherBrushes(output.SourceBrushes, brushesToRemove))
                        output.RemoveRangeToSourceBrushes(brushesToRemove);
                }
                if (OutputNodesChangedEvent != null)
                {
                    var eventArgs = new OutputNodesChangedEventArgs(oldValue, new ObservableCollection<OutputNode>(this.OutputNodes));
                    OutputNodesChangedEvent(this, eventArgs);
                }
            }
        }

        protected override void MethodAfterAddBrushToSourceBrushes(Brush addedBrush)
        {
            base.MethodAfterAddBrushToSourceBrushes(addedBrush);
            this.AddOutputNodeSourceBrushes(addedBrush);


        }

        protected override void MethodAfterRemoveBrushToSourceBrushes(Brush removedBrush)
        {
            base.MethodAfterRemoveBrushToSourceBrushes(removedBrush);
            this.RemoveOutputNodeSourceBrushes(removedBrush);
        }

        protected override void MethodAfterClearSourceBrushes()
        {
            base.MethodAfterClearSourceBrushes();
            this.ClearOutputNodeSourceBrushes();
        }

        protected override void MethodAfterAddRangeToSourceBrushes(IList<Brush> listToAdd)
        {
            base.MethodAfterAddRangeToSourceBrushes(listToAdd);
            this.AddRangeFromConectedElementSourceBrushes(listToAdd);
        }

        protected override void MethodAfterRemoveRangeToSourceBrushes(IList<Brush> brushesToRemove)
        {
            base.MethodAfterRemoveRangeToSourceBrushes(brushesToRemove);
            this.RemoveRangeFromConectedElementSourceBrushes(brushesToRemove);
        }

        private void RiseSourceBrushesChangedEvent(ObservableCollection<OutputNode> oldValue, ObservableCollection<OutputNode> newValue)
        {
            if (OutputNodesChangedEvent != null)
            {
                var eventArgs = new OutputNodesChangedEventArgs(oldValue, newValue);
                OutputNodesChangedEvent(this, eventArgs);
            }
        }
    }
}
