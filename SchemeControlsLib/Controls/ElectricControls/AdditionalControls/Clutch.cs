﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, false, "Sprzęłgo")]
    public class Clutch : CommonControlBase, IHideableElement, IMovableElement, IMovableSelectionElement, IBindingControl
    {
        private double _defaultSize = 15.0;

        #region IsSelectionMoving
        private bool _IsSelectionMoving;
        public bool IsSelectionMoving
        {
            get { return _IsSelectionMoving; }
            set { _IsSelectionMoving = value; NotifyPropertyChanged("IsSelectionMoving"); }
        }
        #endregion IsSelectionMoving

        public bool HideElement
        {
            get { return (bool)GetValue(HideElementProperty); }
            set { SetValue(HideElementProperty, value); }
        }

        public static readonly DependencyProperty HideElementProperty =
            DependencyProperty.Register("HideElement", typeof(bool), typeof(Clutch), new PropertyMetadata(true, HideElement_PropertyChanged));

        private static void HideElement_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Clutch clutch = (Clutch)d;
            clutch.SetControlVisibility((bool)e.NewValue);
        }
 
        public Brush ClutchBrush
        {
            get { return (Brush)GetValue(ClutchBrushProperty); }
            set { SetValue(ClutchBrushProperty, value); }
        }

        public static readonly DependencyProperty ClutchBrushProperty =
            DependencyProperty.Register("ClutchBrush", typeof(Brush), typeof(Clutch), new PropertyMetadata(Brushes.Yellow, ClutchColor_PropertyChanged));

        private static void ClutchColor_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Clutch clutch = (Clutch)d;
            clutch.SetBackgroundColor();
        }

        #region Source 1
        public string Source1Name
        {
            get { return (string)GetValue(Source1NameProperty); }
            set { SetValue(Source1NameProperty, value); }
        }

        public static readonly DependencyProperty Source1NameProperty =
            DependencyProperty.Register("Source1Name", typeof(string), typeof(Clutch), new PropertyMetadata("", Source1Name_PropertyChanged));

        private static void Source1Name_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Clutch clutch = (Clutch)d;
            if(!string.IsNullOrEmpty((string)e.NewValue))
            {
                clutch.FindSource1((string)e.NewValue);
            }

        }

        private void FindSource1(string name)
        {
            this._Source1 = this.FindSourceByName(name);
            if (this._Source1 == null)
                this._IsFindingFaultSource1 = true;
            else
            {
                this._SavedSource1Brush = this._Source1.SourceBrush;
                this._IsFindingFaultSource1 = false;
            }
            NotifyPropertyChanged("IsFindingFault");
        }

        private Source _Source1 = null;
        private Brush _SavedSource1Brush = null;
        private bool _IsFindingFaultSource1 = false;
        #endregion

        #region Source 2
        public string Source2Name
        {
            get { return (string)GetValue(Source2NameProperty); }
            set { SetValue(Source2NameProperty, value); }
        }

        public static readonly DependencyProperty Source2NameProperty =
            DependencyProperty.Register("Source2Name", typeof(string), typeof(Clutch), new PropertyMetadata("", Source2Name_PropertyChanged));

        private static void Source2Name_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Clutch clutch = (Clutch)d;
            if (!string.IsNullOrEmpty((string)e.NewValue))
            {
                clutch.FindSource2((string)e.NewValue);
            }
        }

        private void FindSource2(string name)
        {
            this._Source2 = this.FindSourceByName(name);
            if (this._Source2 == null)
                this._IsFindingFaultSource2 = true;
            else
            {
                this._SavedSource2Brush = this._Source2.SourceBrush;
                this._IsFindingFaultSource2 = false; 
            }
            NotifyPropertyChanged("IsFindingFault");
        }

        private Source _Source2 = null;
        private Brush _SavedSource2Brush = null;
        private bool _IsFindingFaultSource2 = false;
        #endregion

        public object ClutchPosition
        {
            get { return (object)GetValue(ClutchPositionProperty); }
            set { SetValue(ClutchPositionProperty, value); }
        } 

        public static readonly DependencyProperty ClutchPositionProperty =
            DependencyProperty.Register("ClutchPosition", typeof(object), typeof(Clutch), new PropertyMetadata(0, ClutchPosition_PropertyChanged));

        private static void ClutchPosition_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Clutch clutch = (Clutch)d;
            if (int.TryParse(e.NewValue?.ToString(), out int value))
                clutch.StateProcess((int)value);
        }

        public bool IsFindingFault
        {
            get { return this._IsFindingFaultSource1 || this._IsFindingFaultSource2; }
        }

        private void StateProcess(int state)
        {
            if (this._Source1 == null)
                this.FindSource1(this.Source1Name);
            if (this._Source2 == null)
                this.FindSource2(this.Source2Name);

            if (this._Source1 != null && this._Source2 != null)
            {
                switch (state)
                {
                    case 0:
                    case 1:
                        this._Source1.SourceBrush = this._SavedSource1Brush;
                        this._Source2.SourceBrush = this._SavedSource2Brush;
                        break;
                    case 2:
                        this._Source1.SourceBrush = this.ClutchBrush;
                        this._Source2.SourceBrush = this.ClutchBrush;
                        break;
                    default:
                        this._Source1.SourceBrush = this._SavedSource1Brush;
                        this._Source2.SourceBrush = this._SavedSource2Brush;
                        break;

                }
            }
        }

        private Source FindSourceByName(string sourceName)
        {
            if(this.Parent is Canvas)
            {
                var schemeCanvas = (Canvas)this.Parent;
                if (schemeCanvas != null)
                {
                    var source = schemeCanvas.Children.OfType<Source>().Where(x => x.Name == sourceName).FirstOrDefault();
                    if (source is Source)
                    {
                        return (Source)source;
                    }
                }
            }

            return null;
        }

        private void SetControlVisibility(bool hide)
        {
            if (!hide)
            {
                //this.Visibility = Visibility.Visible;
            }
            else
            {
                //this.Visibility = Visibility.Collapsed;
            }
        }

        private void SetBackgroundColor()
        {           
            if (IsFindingFault)
            {
                this.Background = Brushes.Black;
            }
            else
                this.Background = this.ClutchBrush;
        }

        protected override void DrawControl(DrawingContext drawingContext)
        {
           
        }

        public Clutch()
        {
            this.Visibility = Visibility.Hidden;
            this.Width = _defaultSize;
            this.Height = _defaultSize;
            this.SetBackgroundColor();
        }

        static Clutch()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Clutch), new FrameworkPropertyMetadata(typeof(Clutch)));
        }


    }
}
