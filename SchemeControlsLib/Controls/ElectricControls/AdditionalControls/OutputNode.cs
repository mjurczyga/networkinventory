﻿using Newtonsoft.Json;
using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.SchemeControlTypes;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.ElectricControls
{ 
    [EditorType(SchemeTypes.SynopticScheme, false, "Wyjście kontrolki")]
    public sealed class OutputNode : GraphicsControl, IHideableElement, IOutputNode
    {
        public int ParentState { get; set; }
        public NodeType NodeType { get; set; }

        public new double X_Position { get { return base.X_Position; } set { base.X_Position = value; } }

        public new double Y_Position { get { return base.Y_Position; } set { base.Y_Position = value; } }

        public new bool IsEditMode { get { return base.IsEditMode; } set { base.IsEditMode = value; } }
        public new bool FlipGraphics { get { return base.FlipGraphics; } set { base.FlipGraphics = value; } }

        [EditorProperty("Szerokość", serialize: false)]
        public new double Width { get { return base.Width; } set { base.Width = value; } }

        [EditorProperty("Wysokość", serialize: false)]
        public new double Height { get { return base.Height; } set { base.Height = value; } }

        protected override double DefaultWidth => 4;

        public Size OutputNodeSize = new Size(4, 4);

        #region IHideableElement
        [EditorProperty("Ukryj element", serialize:false)]
        public bool HideElement
        {
            get { return (bool)GetValue(HideElementProperty); }
            set { SetValue(HideElementProperty, value); }
        }

        public static readonly DependencyProperty HideElementProperty =
            DependencyProperty.Register("HideElement", typeof(bool), typeof(OutputNode), new PropertyMetadata(HideElement_PropertyChanged));

        private static void HideElement_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            OutputNode outputNode = (OutputNode)d;
            outputNode.ShowOrHideOutput((bool)e.NewValue);
        }
        #endregion IHideableElement

        /// <summary>
        /// Koordynaty środka wyjścia w canvasie
        /// </summary>
        //public Point _CenterPoint;
        //public Point CenterPoint
        //{
        //    get
        //    {
        //        _CenterPoint.X = this.X_Position + this.Width / 2.0;
        //        _CenterPoint.Y = this.Y_Position + this.Height / 2.0; 
        //        return _CenterPoint;
        //    }
        //}

        #region Właściwosci referencyjne
        /// <summary>
        /// Linia przypisana do wyjścia
        /// </summary>
        public SchemeLine OutputLine { get; set; }

        /// <summary>
        /// Właściciel wyjścia
        /// </summary>
        public FrameworkElement ControlParent { get; private set; }
        #endregion

        public OutputNode(FrameworkElement controlParent)
        {
            ControlParent = controlParent;
            this.Height = DefaultWidth;
            ShowOrHideOutput(false);
        }

        public override void Remove()
        {
            if (this.Parent is SchemeCanvas)
            {
                var schemeCanvas = (SchemeCanvas)this.Parent;
                this.ClearSourceBrushes();
                OutputLine?.Disconnect();
                schemeCanvas.Children.Remove(this);
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            AddParentClickHandler();
        }

        protected override void PositionInCanvasChanged(object sender, EventArgs e)
        {
            OutputLine?.UpdateConnection();
        }

        protected override void ControlPosition_Changed(double oldX_Position, double oldY_Position)
        {
            base.ControlPosition_Changed(oldX_Position, oldY_Position);
            OutputLine?.UpdateConnection();
        }

        private void AddParentClickHandler()
        {
            if (this.ControlParent is ElectricElementBase)
            {
                var eventHandlers = DependencyPropertyFinder.GetSpecyficRoutedEvents(this.ControlParent, this.ControlParent.GetType().GetField("MouseDownEvent", BindingFlags.Static | BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy));
                if (eventHandlers != null)
                {
                    foreach (var eventHandler in eventHandlers)
                    {
                        this.AddHandler(OutputNode.MouseDownEvent, eventHandler.Handler);
                    }
                }
                if(this.ControlParent.ContextMenu != null)
                    this.ContextMenu = Activator.CreateInstance(this.ControlParent.ContextMenu.GetType()) as ContextMenu;
            }
        }

        private void ShowOrHideOutput(bool hideOutput)
        {
            if(hideOutput)
            {
                this.Opacity = 1;
                this.Background = Brushes.Transparent;            
            }
            else
            {
                this.Opacity = 0.7;
                this.Background = Brushes.Red;
            }
        }

        static OutputNode()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(OutputNode), new FrameworkPropertyMetadata(typeof(OutputNode)));
        }

        public OutputNode()
        {
            HideElement = true;
        }


        #region ActiveControlMethods
        private void AddFromConnectedElementSourceColors(Brush brush)
        {
            if(this.OutputLine != null)
                this.OutputLine.AddBrushToSourceBrushes(brush);
            if(this.ControlParent is PassiveElement || this.ControlParent is ActiveElement)
            {
                var controlParent = (MultiSourceBrushControl)this.ControlParent;
                controlParent.AddBrushToSourceBrushes(brush);
            }
        }

        private void RemoveFromConnectedElementSourceColors(Brush brush)
        {
            if (this.OutputLine != null)
                this.OutputLine.RemoveBrushFromSourceBrushes(brush);
            if (this.ControlParent is PassiveElement || this.ControlParent is ActiveElement)
            {
                var controlParent = (MultiSourceBrushControl)this.ControlParent;
                controlParent.RemoveBrushFromSourceBrushes(brush);
            }
        }

        private void ClearFromConnectedElementSourceBrushes()
        {
            //if (!this.IsConnectedToSource(out Brush brush))
            //{
                if (this.OutputLine != null)
                    this.OutputLine.ClearSourceBrushes();
                if (this.ControlParent is PassiveElement || this.ControlParent is ActiveElement || this.ControlParent is MultiOutputNode)
                {
                    var controlParent = (MultiSourceBrushControl)this.ControlParent;
                    controlParent.ClearSourceBrushes();
                }
            //}
        }

        private void AddRangeFromConectedElementSourceBrushes(IList<Brush> addedBrushes)
        {
            if (this.OutputLine != null)
                this.OutputLine.AddRangeToSourceBrushes(addedBrushes);
            if (this.ControlParent is PassiveElement || this.ControlParent is ActiveElement)
            {
                var controlParent = (MultiSourceBrushControl)this.ControlParent;
                controlParent.AddRangeToSourceBrushes(addedBrushes);
            }
        }

        private void RemoveRangeFromConectedElementSourceBrushes(IList<Brush> brushesToRemove)
        {
            if (this.OutputLine != null && MixColors.CheckIfBrushesContainsAnotherBrushes(this.OutputLine.SourceBrushes, brushesToRemove))
                this.OutputLine.RemoveRangeToSourceBrushes(brushesToRemove);

            // dla aktywnej kontrolki
            if (this.ControlParent is PassiveElement)
            {
                var controlParent = (MultiSourceBrushControl)this.ControlParent;
                if (MixColors.CheckIfBrushesContainsAnotherBrushes(controlParent.SourceBrushes, brushesToRemove))                   
                    controlParent.RemoveRangeToSourceBrushes(brushesToRemove);
            }
            else if (this.ControlParent is ActiveElement)
            {
                var controlParent = (MultiSourceBrushControl)this.ControlParent;
                if (MixColors.CheckIfBrushesContainsAnotherBrushes(controlParent.SourceBrushes, brushesToRemove))
                    controlParent.RemoveRangeToSourceBrushes(brushesToRemove);
            }
        }

        protected override void MethodAfterAddBrushToSourceBrushes(Brush addedBrush)
        {
            base.MethodAfterAddBrushToSourceBrushes(addedBrush);
            this.AddFromConnectedElementSourceColors(addedBrush);
        }

        protected override void MethodAfterRemoveBrushToSourceBrushes(Brush removedBrush)
        {
            base.MethodAfterRemoveBrushToSourceBrushes(removedBrush);
            this.RemoveFromConnectedElementSourceColors(removedBrush);
        }

        protected override void MethodAfterClearSourceBrushes()
        {
            base.MethodAfterClearSourceBrushes();
            this.ClearFromConnectedElementSourceBrushes();
        }

        protected override void MethodAfterAddRangeToSourceBrushes(IList<Brush> listToAdd)
        {
            base.MethodAfterAddRangeToSourceBrushes(listToAdd);
            this.AddRangeFromConectedElementSourceBrushes(listToAdd);
        }

        protected override void MethodAfterRemoveRangeToSourceBrushes(IList<Brush> brushesToRemove)
        {
            base.MethodAfterRemoveRangeToSourceBrushes(brushesToRemove);
            this.RemoveRangeFromConectedElementSourceBrushes(brushesToRemove);
        }
        #endregion ActiveControlMethods

        protected override void DrawControl(DrawingContext drawingContext)
        {          
        }

        /// <summary>
        /// sprawdza pojedyńcze wyjscie czy jest połączone ze źródłami
        /// </summary>
        /// <param name="outputNode">wyjście</param>
        /// <returns>lista źródeł połaczonych</returns>
        public List<ISource> CheckSourceConnection(OutputNode outputNode, List<string> checkedControls)
        {
            var resultLits = new List<ISource>();
            if (outputNode != null && outputNode.OutputLine != null)
            {
                if (outputNode.OutputLine.OutputNode1 is OutputNode lineOutputNode1 && lineOutputNode1 != outputNode)
                {
                    if (lineOutputNode1?.ControlParent is GraphicsControl)
                        CheckSourcesForOutputNode(resultLits, lineOutputNode1, lineOutputNode1.ControlParent as GraphicsControl, checkedControls);
                }
                else if (outputNode.OutputLine.OutputNode2 is OutputNode lineOutputNode2 && lineOutputNode2 != outputNode)
                {
                    if (lineOutputNode2?.ControlParent is GraphicsControl)
                        CheckSourcesForOutputNode(resultLits, lineOutputNode2, lineOutputNode2.ControlParent as GraphicsControl, checkedControls);
                }
            }
            return resultLits;
        }

        /// <summary>
        /// Metoda pomocnicza (żeby rozbić metodę CheckSourceConnection. Tam trzeba sprawdzić dwa końce linii)
        /// </summary>
        /// <param name="sources"></param>
        /// <param name="lineOutputNode"></param>
        /// <param name="parentControl"></param>
        private void CheckSourcesForOutputNode(List<ISource> sources, OutputNode lineOutputNode, GraphicsControl parentControl, List<string> checkedControls)
        {
            if (parentControl != null && !checkedControls.Contains(parentControl.Name))
            {
                checkedControls.Add(parentControl.Name);

                if (parentControl is IActiveElement)
                {
                    ActiveElement activeElement = parentControl as ActiveElement;
                    if ((StateTypes)activeElement.State == StateTypes.Closed && activeElement.Name != this.Name)
                    {
                        foreach (var activeOutput in activeElement.OutputNodesCollection)
                        {
                            if (activeOutput != lineOutputNode)
                            {
                                var activeResults = CheckSourceConnection(activeOutput, checkedControls);
                                foreach (var activeResult in activeResults)
                                {
                                    if (!sources.Contains(activeResult))
                                        sources.Add(activeResult);
                                }
                            }
                        }
                    }
                }
                else if (parentControl is IPasiveElement)
                {
                    PassiveElement passiveElement = parentControl as PassiveElement;
                    foreach (var passiveOutpu in passiveElement.OutputNodesCollection)
                    {
                        if (passiveOutpu != lineOutputNode)
                        {
                            var opName = passiveOutpu.Name;
                            var passiveResults = CheckSourceConnection(passiveOutpu, checkedControls);
                            foreach (var passiveResult in passiveResults)
                            {
                                if (!sources.Contains(passiveResult))
                                    sources.Add(passiveResult);
                            }
                        }
                    }
                }
                else if (parentControl?.GetType() == typeof(MultiOutputNode))
                {
                    MultiOutputNode multiOutputNode = (MultiOutputNode)parentControl;
                    if (multiOutputNode.ControlParent is ISource)
                        sources.Add((ISource)multiOutputNode.ControlParent);
                }
                else if (parentControl != null && parentControl.GetType().GetInterfaces().Contains(typeof(ISource)))
                {
                    ISource source = (ISource)parentControl;
                    sources.Add(source);
                }
            }
        }
    }
}
