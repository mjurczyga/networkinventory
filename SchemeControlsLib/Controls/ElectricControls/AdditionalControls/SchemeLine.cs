﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.ElectricControls
{
    [EditorType(SchemeTypes.SynopticScheme, false, "Linia synoptyczna")]
    public sealed class SchemeLine : MultiSourceBrushControl, ILine
    {
        public bool IsArrowed { get; set; }
        public bool IsDashed { get; set; }
        public void ChangeState(IOutputNode outputNode, int state) { }

        #region Ingerited properties
        public new double X_Position { get { return base.X_Position; } set { base.X_Position = value; } }

        public new double Y_Position { get { return base.Y_Position; } set { base.Y_Position = value; } }

        public new double Width { get { return base.Width; } set { base.Width = value; } }

        public new double Height { get { return base.Height; } set { base.Height = value; } }

        public new string Name { get { return base.Name; } set { base.Name = value; } }
        #endregion

        #region Dependency properties
        public double X1
        {
            get { return (double)GetValue(X1Property); }
            set { SetValue(X1Property, value); }
        }

        public static readonly DependencyProperty X1Property =
            DependencyProperty.Register("X1", typeof(double), typeof(SchemeLine), new PropertyMetadata(0.0, InvalidateVisualAndMeasure));

        public double Y1
        {
            get { return (double)GetValue(Y1Property); }
            set { SetValue(Y1Property, value); }
        }

        public static readonly DependencyProperty Y1Property =
            DependencyProperty.Register("Y1", typeof(double), typeof(SchemeLine), new PropertyMetadata(0.0, InvalidateVisualAndMeasure));

        public double X2
        {
            get { return (double)GetValue(X2Property); }
            set { SetValue(X2Property, value); }
        }

        public static readonly DependencyProperty X2Property =
            DependencyProperty.Register("X2", typeof(double), typeof(SchemeLine), new PropertyMetadata(0.0, InvalidateVisualAndMeasure));

        public double Y2
        {
            get { return (double)GetValue(Y2Property); }
            set { SetValue(Y2Property, value); }
        }

        public static readonly DependencyProperty Y2Property =
            DependencyProperty.Register("Y2", typeof(double), typeof(SchemeLine), new PropertyMetadata(0.0, InvalidateVisualAndMeasure));


        [EditorProperty("Odwrócone rysowanie")]
        public bool ReverseDrawing
        {
            get { return (bool)GetValue(ReverseDrawingProperty); }
            set { SetValue(ReverseDrawingProperty, value); }
        }

        public static readonly DependencyProperty ReverseDrawingProperty =
            DependencyProperty.Register("ReverseDrawing", typeof(bool), typeof(SchemeLine), new PropertyMetadata(false, InvalidateVisual));

        public Brush Stroke
        {
            get { return (Brush)GetValue(StrokeProperty); }
            set { SetValue(StrokeProperty, value); }
        }

        public static readonly DependencyProperty StrokeProperty =
            DependencyProperty.Register("Stroke", typeof(Brush), typeof(SchemeLine), new PropertyMetadata(Settings.DefaultLineColor, InvalidateVisual));

        [EditorProperty("Grubość linii")]
        public double StrokeThickness
        {
            get { return (double)GetValue(StrokeThicknessProperty); }
            set { SetValue(StrokeThicknessProperty, value); }
        }
        public static readonly DependencyProperty StrokeThicknessProperty =
            DependencyProperty.Register("StrokeThickness", typeof(double), typeof(SchemeLine), new PropertyMetadata(1.0, InvalidateVisual));
        #endregion

        public IOutputNode OutputNode1 { get; private set; }

        public IOutputNode OutputNode2 { get; private set; }

        public void BreakLine(Point menuOpenedPoint, SchemeCanvas schemeCanvas)
        {
            var lineConnector = new LineConnector();
            schemeCanvas.AddSchemeElement(lineConnector, true);

            lineConnector.SetPosition(menuOpenedPoint.X - lineConnector.Width / 2.0, menuOpenedPoint.Y - lineConnector.Height / 2.0);
            lineConnector.BreakTheLine(this, schemeCanvas);
        }

        public SchemeLine()
        {
        }

        private static void InvalidateVisual(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SchemeLine schemeLine = (SchemeLine)d;
            schemeLine.InvalidateVisual();
        }

        private static void InvalidateVisualAndMeasure(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SchemeLine schemeLine = (SchemeLine)d;
            schemeLine.InvalidateVisual();
            schemeLine.InvalidateMeasure();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);
            InternalDrawLineGeometry(drawingContext);
        }

        protected override Size MeasureOverride(Size constraint)
        {
            return this.ComputeControlSize();
        }

        private void InternalDrawLineGeometry(DrawingContext context)
        {
            Point p1 = new Point(this.X1, this.Y1);
            Point p2 = new Point(this.X2, this.Y2);

            Brush brush = Stroke;

            if (p1.X == p2.X || p1.Y == p2.Y)
            {
                if (SourceBrushes != null && SourceBrushes.Count > 0)
                    brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, this.ComputeLineLength(p1, p2));
                var pen = new Pen(brush, StrokeThickness);
                pen.Freeze();

                GuidelineSet guidelines = new GuidelineSet();
                guidelines.GuidelinesX.Add(p1.X);
                guidelines.GuidelinesY.Add(p1.Y);
                guidelines.GuidelinesX.Add(p2.X);
                guidelines.GuidelinesY.Add(p2.Y);
                context.PushGuidelineSet(guidelines);

                context.DrawLine(pen, p1, p2);
            }
            else
            {
                Point p3;
                if (this.ReverseDrawing)
                    p3 = new Point(this.X2, this.Y1);
                else
                    p3 = new Point(this.X1, this.Y2);

                if (SourceBrushes != null && SourceBrushes.Count > 0)
                    brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, this.ComputeLineLength(p1, p3));
                var pen = new Pen(brush, StrokeThickness);
                pen.Freeze();

                GuidelineSet guidelines = new GuidelineSet();
                guidelines.GuidelinesX.Add(p1.X);
                guidelines.GuidelinesY.Add(p1.Y);
                guidelines.GuidelinesX.Add(p3.X);
                guidelines.GuidelinesY.Add(p3.Y);
                context.PushGuidelineSet(guidelines);

                context.DrawLine(pen, p1, p3);

                if (SourceBrushes != null && SourceBrushes.Count > 0)
                    brush = MixColors.GenerateTwoColorBrushWithLength(SourceBrushes, this.ComputeLineLength(p3, p2));
                var pen1 = new Pen(brush, StrokeThickness);
                pen1.Freeze();

                GuidelineSet guidelines1 = new GuidelineSet();
                guidelines1.GuidelinesX.Add(p3.X);
                guidelines1.GuidelinesY.Add(p3.Y);
                guidelines1.GuidelinesX.Add(p2.X);
                guidelines1.GuidelinesY.Add(p2.Y);
                context.PushGuidelineSet(guidelines1);
                context.DrawLine(pen1, p3, p2);

            }
        }

        private void InternalDrawLineGeometry(StreamGeometryContext context)
        {
            Point p1 = new Point(this.X1, this.Y1);
            Point p2 = new Point(this.X2, this.Y2);


            context.BeginFigure(new Point(p1.X, p1.Y), false, false);
            if (p1.X == p2.X || p1.Y == p2.Y)
            {
                context.LineTo(p2, true, true);
            }
            else
            {
                if (this.ReverseDrawing)
                {
                    context.LineTo(new Point(this.X2, this.Y1), true, true);
                    context.LineTo(new Point(this.X2, this.Y2), true, true);
                }
                else
                {
                    context.LineTo(new Point(this.X1, this.Y2), true, true);
                    context.LineTo(new Point(this.X2, this.Y2), true, true);
                }
            }
        }

        /// <summary>
        /// Uaktualnia połączenie pomiędzy wyjsciami
        /// </summary>
        public void UpdateConnection()
        {
            if(OutputNode1 != null && OutputNode2 != null)
                this.UpdateConnection(OutputNode1, OutputNode2);
        }     

        /// <summary>
        /// Wykonuje połączenie pomiędzy dwoma OutputNode.
        /// </summary>
        /// <param name="output1"></param>
        /// <param name="output2"></param>
        public void Connect(IOutputNode iOutput1, IOutputNode iOutput2)
        {
            if (iOutput1 is OutputNode output1 && iOutput2 is OutputNode output2  && output1.OutputLine == null && output2.OutputLine == null)
            {
                bool result = DependencyPropertyFinder.CopyParentClickHandler(output1, this);
                if (!result)
                    DependencyPropertyFinder.CopyParentClickHandler(output2, this);

                output1.OutputLine = this;
                output2.OutputLine = this;

                OutputNode1 = output1;
                OutputNode2 = output2;
                this.UpdateConnection(output1, output2);

                this.SetValue(FrameworkElement.NameProperty, output1.Name + "TO" + output2.Name);

                this.RefreshBrushes();
            }
        }

        private void RefreshBrushes()
        {
            if (OutputNode1 is OutputNode outputNode1 && OutputNode2 is OutputNode outputNode2)
            {
                if (outputNode1.SourceBrushes.Count != 0 && !(outputNode1.SourceBrushes.Count == 1 && outputNode1.SourceBrushes.Contains(Settings.DefaultLineColor)))
                    this.AddRangeToSourceBrushes(outputNode1.SourceBrushes);
                if (outputNode2.SourceBrushes.Count != 0 && !(outputNode2.SourceBrushes.Count == 1 && outputNode2.SourceBrushes.Contains(Settings.DefaultLineColor)))
                    this.AddRangeToSourceBrushes(outputNode2.SourceBrushes);
            }
        }

        /// <summary>
        /// Rozłącza 2 OutputNode
        /// </summary>
        public void Disconnect(bool remove = false)
        {
            if (OutputNode1 is OutputNode outputNode1 && OutputNode2 is OutputNode outputNode2)
            {
                if (remove)
                {
                    if (outputNode1.ControlParent is LineConnector)
                        ((LineConnector)outputNode1.ControlParent)?.RemoveTheLinieBreak();
                    else if (outputNode2.ControlParent is LineConnector)
                        ((LineConnector)outputNode2.ControlParent)?.RemoveTheLinieBreak();
                }

                if (outputNode1.ControlParent is MultiOutputNode multiOutputNode_1 && outputNode2.ControlParent is MultiOutputNode multiOutputNode_2)
                {
                    multiOutputNode_1.RemoveOutputFromList(outputNode1);
                    multiOutputNode_1.RemoveOutputFromList(outputNode2);
                }
                else if (outputNode1.ControlParent is MultiOutputNode multiOutputNode1)
                {
                    multiOutputNode1.RemoveOutputFromList(outputNode1);
                    if (outputNode2.ControlParent is IMultiBrushLine)
                        ((IMultiBrushLine)outputNode2.ControlParent).ClearSourceBrushes();

                }
                else if (outputNode2.ControlParent is MultiOutputNode multiOutputNode2)
                {
                    multiOutputNode2.RemoveOutputFromList(outputNode2);
                    if (outputNode1.ControlParent is IMultiBrushLine)
                        ((IMultiBrushLine)outputNode1.ControlParent).ClearSourceBrushes();
                }

                outputNode1.OutputLine = outputNode2.OutputLine = null;
                outputNode1 = outputNode2 = null;

                if (this.Parent is SchemeCanvas && ((SchemeCanvas)this.Parent).Children.Contains(this))
                {
                    var schemeCanvas = (SchemeCanvas)this.Parent;
                    schemeCanvas.Children.Remove(this);
                }
            }
        }    

        private void UpdateConnection(IOutputNode output1, IOutputNode output2)
        {
            if (output1.CenterPoint.X >= output2.CenterPoint.X)
            {
                this.X_Position = output2.CenterPoint.X;
                //POZYCJA
                this.X1 = output1.CenterPoint.X - output2.CenterPoint.X;
                this.X2 = 0;
            }
            else
            {
                this.X_Position = output1.CenterPoint.X;
                //POZYCJA
                this.X1 = 0;
                this.X2 = output2.CenterPoint.X - output1.CenterPoint.X;
            }

            if (output1.CenterPoint.Y >= output2.CenterPoint.Y)
            {
                this.Y_Position = output2.CenterPoint.Y;
                //POZYCJA
                this.Y1 = output1.CenterPoint.Y - output2.CenterPoint.Y;
                this.Y2 = 0;
            }
            else
            {
                this.Y_Position = output1.CenterPoint.Y;
                //POZYCJA
                this.Y1 = 0;
                this.Y2 = output2.CenterPoint.Y - output1.CenterPoint.Y;
            }
        }

        private double ComputeLineLength(Point p1, Point p2)
        {
            return Math.Sqrt((p2.X - p1.X) * (p2.X - p1.X) + (p2.Y - p1.Y) * (p2.Y - p1.Y));
        }

        private Size ComputeControlSize()
        {
            Point p1 = new Point(this.X1, this.Y1);
            Point p2 = new Point(this.X2, this.Y2);
            Point p3 = new Point(this.X1, this.Y2);

            return new Size(this.ComputeLineLength(p2,p3), this.ComputeLineLength(p3,p1));
        }      

        protected override void MethodAfterAddBrushToSourceBrushes(Brush addedBrush)
        {
            if (OutputNode1 is OutputNode outputNode1 && OutputNode2 is OutputNode outputNode2)
            {
                if(!MixColors.CompareTwoBrushes(this.SourceBrushes, outputNode1.SourceBrushes))
                    outputNode1.AddBrushToSourceBrushes(addedBrush);
                if(!MixColors.CompareTwoBrushes(this.SourceBrushes, outputNode2.SourceBrushes))
                    outputNode2.AddBrushToSourceBrushes(addedBrush);
            }
        }

        protected override void MethodAfterRemoveBrushToSourceBrushes(Brush removedBrush)
        {
            if (OutputNode1 is OutputNode outputNode1 && OutputNode2 is OutputNode outputNode2)
            {
                if (outputNode1.SourceBrushes.Contains(removedBrush))
                    outputNode1.RemoveBrushFromSourceBrushes(removedBrush);
                if (outputNode2.SourceBrushes.Contains(removedBrush))
                    outputNode2.RemoveBrushFromSourceBrushes(removedBrush);
            }
        }

        protected override void MethodAfterClearSourceBrushes()
        {
            if (OutputNode1 is OutputNode outputNode1 && OutputNode2 is OutputNode outputNode2)
            {
                if (outputNode1.SourceBrushes.Count != 0)
                    outputNode1.ClearSourceBrushes();
                if (outputNode2.SourceBrushes.Count != 0)
                    outputNode2.ClearSourceBrushes();
            }
        }

        protected override void MethodAfterAddRangeToSourceBrushes(IList<Brush> listToAdd)
        {
            if (OutputNode1 is OutputNode outputNode1 && OutputNode2 is OutputNode outputNode2)
            {
                if (!MixColors.CompareTwoBrushes(this.SourceBrushes, outputNode1.SourceBrushes))
                    outputNode1.AddRangeToSourceBrushes(listToAdd);
                if (!MixColors.CompareTwoBrushes(this.SourceBrushes, outputNode2.SourceBrushes))
                    outputNode2.AddRangeToSourceBrushes(listToAdd);
            }
        }

        protected override void MethodAfterRemoveRangeToSourceBrushes(IList<Brush> brushesToRemove)
        {
            if (OutputNode1 is OutputNode outputNode1 && OutputNode2 is OutputNode outputNode2)
            {
                if (MixColors.CheckIfBrushesContainsAnotherBrushes(outputNode1.SourceBrushes, brushesToRemove))
                    outputNode1.RemoveRangeToSourceBrushes(brushesToRemove);
                if (MixColors.CheckIfBrushesContainsAnotherBrushes(outputNode2.SourceBrushes, brushesToRemove))
                    outputNode2.RemoveRangeToSourceBrushes(brushesToRemove);
            }
        }
    }

}

