﻿using SchemeControlsLib.Controls.NetworkControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace SchemeControlsLib.Controls
{
    public static class Settings
    {
        #region ElectricSchemesSettings
        public static readonly int FontSize = 6;
        public static readonly Brush ForegroundColor = Brushes.White;
        public static readonly Brush BrushInsideElement = Brushes.White;

        public static readonly double ElementStrokeThickness = 1.5;
        public static readonly double LinesStrokeThickness = 2;

        public static readonly double MixColorLength = 3;
        public static readonly Brush BaseGroundColor = Brushes.Yellow;
        public static readonly Brush DefaultLineColor = Brushes.White;

        public static readonly double ColorLineLength = 10;

        // brushes setings
        public static readonly Brush OffStateColor = Brushes.White;
        public static readonly Brush UniknowStateColor = Brushes.White;
        #endregion ElectricSchemesSettings

        #region New controls settings (network, coal)
        #region Coal
        public static double Coal_ElementStrokeThickness = 2.0;
        public static Brush Coal_ElementStroke = Brushes.White;
        #endregion Coal

        #region network
        public static double Network_ElementStrokeThickness { get; set; } = 5.0;
        public static double Network_PathStrokeThickness { get; set; } = 5.0;
        public static Brush Network_ElementStroke { get; set; } = Brushes.Black;

        public static NetworkLinesSettings Network_Lines { get; set; } = new NetworkLinesSettings();
        #endregion network

        #region Colors

        public static Brush StatusOnBrush { get; set; } = Brushes.Green;
        public static Brush StatusOffBrush { get; set; } = Brushes.Red;
        public static Brush StatusInvalidBrush { get; set; } = Brushes.Gray;
        public static Brush StatusFaultBrush { get; set; } = (SolidColorBrush)new BrushConverter().ConvertFrom("#edd500");

        public static Brush StatusPortOkBrush { get; set; } = (SolidColorBrush)new BrushConverter().ConvertFrom("#11ff00");
        public static Brush StatusPortOffBrush { get; set; } = Brushes.LightGray;
        public static Brush StatusPortInvalidBrush { get; set; } = (SolidColorBrush)new BrushConverter().ConvertFrom("#e89300");
        public static Brush StatusPortTestingBrush { get; set; } = Brushes.Blue;
        #endregion Colors

        public static double LabelAngle = 60;
        public static Brush TextForeground = Brushes.White;
        #endregion New controls settings (network, coal)


        private static bool _IsITSApp = false;
        public static bool IsITSApp { get { return _IsITSApp; } set { _IsITSApp = value; } }
    }
}
