﻿using SchemeControlsLib.Controls.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SchemeControlsLib.Controls.LibElementsControls
{
    public class PopupButton : ObservableControl
    {
        private bool _IsPopupOpen = false;
        public bool IsPopupOpen { get { return _IsPopupOpen; } private set { _IsPopupOpen = value; NotifyPropertyChanged("IsPopupOpen"); } }

        #region Dependency properties
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(PopupButton), new PropertyMetadata(""));

        public object PopupContent
        {
            get { return (object)GetValue(PopupContentProperty); }
            set { SetValue(PopupContentProperty, value); }
        }

        public static readonly DependencyProperty PopupContentProperty =
            DependencyProperty.Register("PopupContent", typeof(object), typeof(PopupButton), new PropertyMetadata(null));
        #endregion



        public ICommand MouseDownCommand
        {
            get { return (ICommand)GetValue(MouseDownCommandProperty); }
            set { SetValue(MouseDownCommandProperty, value); }
        }

        public static readonly DependencyProperty MouseDownCommandProperty =
            DependencyProperty.Register("MouseDownCommand", typeof(ICommand), typeof(PopupButton), new PropertyMetadata(null));



        public PopupButton()
        {
        }

        //protected override void OnMouseEnter(MouseEventArgs e)
        //{
        //    this.IsPopupOpen = true;
        //}

        //protected override void OnMouseLeave(MouseEventArgs e)
        //{
        //    this.IsPopupOpen = false;
        //}

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            this.IsPopupOpen = !this.IsPopupOpen;
            MouseDownCommand?.Execute(this);
        }

        static PopupButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PopupButton), new FrameworkPropertyMetadata(typeof(PopupButton)));
        }
    }
}
