﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.LibElementsControls
{
    public class PropertyControl : Control
    {
        public object PropertyValueContent
        {
            get { return (object)GetValue(PropertyValueContentProperty); }
            set { SetValue(PropertyValueContentProperty, value); }
        }

        public static readonly DependencyProperty PropertyValueContentProperty =
            DependencyProperty.Register("PropertyValueContent", typeof(object), typeof(PropertyControl), new PropertyMetadata(null));

        public string PropertyName
        {
            get { return (string)GetValue(PropertyNameProperty); }
            set { SetValue(PropertyNameProperty, value); }
        }

        public static readonly DependencyProperty PropertyNameProperty =
            DependencyProperty.Register("PropertyName", typeof(string), typeof(PropertyControl), new PropertyMetadata(""));

        public double PropertyNameFontSize
        {
            get { return (double)GetValue(PropertyNameFontSizeProperty); }
            set { SetValue(PropertyNameFontSizeProperty, value); }
        }
        public static readonly DependencyProperty PropertyNameFontSizeProperty =
            DependencyProperty.Register("PropertyNameFontSize", typeof(double), typeof(PropertyControl), new PropertyMetadata(12.0));

        public Brush PropertyNameForeground
        {
            get { return (Brush)GetValue(PropertyNameForegroundProperty); }
            set { SetValue(PropertyNameForegroundProperty, value); }
        }

        public static readonly DependencyProperty PropertyNameForegroundProperty =
            DependencyProperty.Register("PropertyNameForeground", typeof(Brush), typeof(PropertyControl), new PropertyMetadata(Brushes.Black));

        public Thickness TextMargin
        {
            get { return (Thickness)GetValue(TextMarginProperty); }
            set { SetValue(TextMarginProperty, value); }
        }

        public static readonly DependencyProperty TextMarginProperty =
            DependencyProperty.Register("TextMargin", typeof(Thickness), typeof(PropertyControl), new PropertyMetadata(new Thickness(0)));



        static PropertyControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PropertyControl), new FrameworkPropertyMetadata(typeof(PropertyControl)));
        }
    }
}
