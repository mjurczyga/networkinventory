﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.CoalProcessing;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.Controls.ElectricControls;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.NetworkControls;
using SchemeControlsLib.Controls.NetworkControls.Interfaces;
using SchemeControlsLib.Models;
using SchemeControlsLib.Services;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
namespace SchemeControlsLib.Controls.LibElementsControls
{
    public sealed class SchemeElementContextMenu : ContextMenu
    {
        private SchemeCanvas _schemeCanvas;
        private System.Windows.Point _menuOpendPoint;

        public SchemeElementContextMenu()
        {
            this.Placement = System.Windows.Controls.Primitives.PlacementMode.Right;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if (this?.PlacementTarget is FrameworkElement frameworkElement && frameworkElement?.Parent is SchemeCanvas schemeCanvas)
                _schemeCanvas = schemeCanvas;

            AddMenuItems();
        }

        protected override void OnOpened(RoutedEventArgs e)
        {
            base.OnOpened(e);
            if (_schemeCanvas != null)
                _menuOpendPoint = Mouse.GetPosition(_schemeCanvas);
        }

        #region Menu generating
        private void AddMenuItems()
        {           
            var controlType = this.PlacementTarget.GetType().GetCustomAttribute<EditorType>();
            if (controlType != null)
            {
                IList<FrameworkElement> itemsCollection = new List<FrameworkElement>();

                if (controlType.SchemeType == EnumTypes.SchemeTypes.SynopticScheme)
                {

                    if (this.PlacementTarget is LineConnector)
                        itemsCollection = this.AddLineConnectorContextMenuItems();
                    else if (this.PlacementTarget is ElectricElementBase)
                        itemsCollection = this.AddElectricElementBaseContextMenuItems();
                    else if (this.PlacementTarget is Source)
                        itemsCollection = this.AddSourceContextMenuItems();
                }
                else if (controlType.SchemeType == EnumTypes.SchemeTypes.NetworkScheme)
                {
                    if (this.PlacementTarget is IControlWithOutputs)
                        itemsCollection = this.AddNetworkControlMenuItems();
                }
                else if (controlType.SchemeType == EnumTypes.SchemeTypes.CoalProcessingScheme)
                {
                    if (this.PlacementTarget is IControlWithOutputs)
                        itemsCollection = this.AddNetworkControlMenuItems();
                }

                if (this.PlacementTarget is IOutputNode)
                    itemsCollection = this.AddOutputMenuItems(controlType.SchemeType);

                if (this.PlacementTarget is ILine)
                    itemsCollection = this.AddSchemeLineContextmenuItems();

                if (this.PlacementTarget is IEditableControl)
                    this.AddEditableElementContextMenuItems(itemsCollection);

                if (ElementPropertiesModels.HasCommonProperties(this.PlacementTarget))
                {
                    MenuItem commonProperties = new MenuItem() { Header = "Zmień właściwosci wspólne", Foreground = this.Foreground, Icon = SchemeControlsLibResources.GetIconByKey("PropertyModify") };
                    commonProperties.Click += CommonProperties_Click;
                    itemsCollection.Insert(0, commonProperties);
                }

                this.ItemsSource = itemsCollection.Count == 0 ? null : itemsCollection;
            }
        }

        private void CommonProperties_Click(object sender, RoutedEventArgs e)
        {
            FrameworkElement schemeElement = this.PlacementTarget as FrameworkElement;
            SchemeCanvas schemeCanvas = null;
            if (schemeElement != null)
                schemeCanvas = schemeElement.Parent as SchemeCanvas;

            if(schemeCanvas != null)
            {
                var foreground = SchemeControlsLibResources.GetResourceByKey("_SchemePrimaryForeground") as Brush;
                if (foreground == null)
                    foreground = this.Foreground;
                var propertyControl = new ElementCommonPropertiesControl(schemeCanvas, schemeElement) { Foreground = foreground, Background = this.Background, ButtonBrush = Brushes.Black, MinWidth = 300, MinHeight = 100 };
                WindowManager.ShowContentInNewDialogWindow(propertyControl, "Właściwości wspólne", foreground, windowState: WindowState.Normal);
            }
        }
        #endregion Menu generating

        #region Coal processing
        #region Click actions
        private void OnCoalConnect_Click(object sender, RoutedEventArgs e)
        {
            if (this.PlacementTarget is IOutputNode networkOutput && _schemeCanvas != null && _schemeCanvas.SelectedElement is IOutputNode selectedOutput)
            {
                // tworzenie nowej linii
                var schemeLine = new CoalProcessingLine() { Stroke = Settings.DefaultLineColor, StrokeThickness = Settings.LinesStrokeThickness };
                schemeLine.Connect(selectedOutput, networkOutput); // wykonanie połączenia
                schemeLine.ContextMenu = new SchemeElementContextMenu();
                _schemeCanvas.Children.Add(schemeLine);

            }
        }
        #endregion Click actions
        #endregion Coal processing

        #region Network elements
        #region Generic menu items methods
        private List<FrameworkElement> AddNetworkControlMenuItems()
        {
            List<FrameworkElement> itemCollection = new List<FrameworkElement>();

            MenuItem addOutput = new MenuItem() { Header = "Dodaj wyjscie", Foreground = this.Foreground, Icon = SchemeControlsLibResources.GetIconByKey("AddItem") };
            addOutput.Click += AddOutput_Click;
            itemCollection.Add(addOutput);

            return itemCollection;
        }

        private void AddOutput_Click(object sender, RoutedEventArgs e)
        {
            if (this.PlacementTarget is IControlWithOutputs networkControl)
            {
                networkControl.AddOutput(_menuOpendPoint.X, _menuOpendPoint.Y);
            }
        }
        #endregion Generic menu items methods

        #region Click actions
        private void OnNetworkConnect_Click(object sender, RoutedEventArgs e)
        {
            if (this.PlacementTarget is IOutputNode networkOutput && _schemeCanvas != null && _schemeCanvas.SelectedElement is IOutputNode selectedNetworkOutput)
            {
                // tworzenie nowej linii
                var schemeLine = new NetworkSchemeLine() { Stroke = Settings.DefaultLineColor, StrokeThickness = Settings.LinesStrokeThickness };
                schemeLine.Connect(networkOutput, selectedNetworkOutput); // wykonanie połączenia
                schemeLine.ContextMenu = new SchemeElementContextMenu();
                _schemeCanvas.Children.Add(schemeLine);

            }
        }
        #endregion Click actions
        #endregion Network elements

        #region Synoptic elements
        #region Generic menu items methods
        private List<FrameworkElement> AddSourceContextMenuItems()
        {
            List<FrameworkElement> itemCollection = new List<FrameworkElement>();

            MenuItem connect = new MenuItem() { Header = "Połącz", Foreground = this.Foreground, Icon = SchemeControlsLibResources.GetIconByKey("Connect") };
            connect.Click += OnConnectToSource;

            itemCollection.Add(connect);

            return itemCollection;
        }

        private List<FrameworkElement> AddSchemeLineContextmenuItems()
        {
            List<FrameworkElement> itemCollection = new List<FrameworkElement>();

            MenuItem breakTheLine = new MenuItem() { Header = "Złam połączenie", Foreground = this.Foreground, Icon = SchemeControlsLibResources.GetIconByKey("AddTheLineBreak") };
            breakTheLine.Click += OnBreakTheLine;

            MenuItem changeConnection = new MenuItem() { Header = "Odwróć połączenie", Foreground = this.Foreground, Icon = SchemeControlsLibResources.GetIconByKey("ChangeConnection") };
            changeConnection.Click += OnChangeConnection;

            MenuItem deleteItem = new MenuItem() { Header = "Usuń połączenie", Foreground = this.Foreground, Icon = SchemeControlsLibResources.GetIconByKey("Disconnect") };
            deleteItem.Click += OnDeleteConnectionCommand;

            itemCollection.Add(breakTheLine);
            itemCollection.Add(changeConnection);
            itemCollection.Add(new Separator());
            itemCollection.Add(deleteItem);

            return itemCollection;
        }

        private List<FrameworkElement> AddElectricElementBaseContextMenuItems()
        {
            List<FrameworkElement> itemCollection = new List<FrameworkElement>();

            MenuItem changeOrientation = new MenuItem() { Header = "Zmień orientacje", Foreground = this.Foreground, Icon = SchemeControlsLibResources.GetIconByKey("LayoutTransform") };
            changeOrientation.Click += OnChangeOrientation;

            itemCollection.Add(changeOrientation);
            return itemCollection;
        }

        private List<FrameworkElement> AddLineConnectorContextMenuItems()
        {
            List<FrameworkElement> itemCollection = new List<FrameworkElement>();

            MenuItem deleteLineConnector = new MenuItem() { Header = "Usuń załamanie", Foreground = this.Foreground, Icon = SchemeControlsLibResources.GetIconByKey("RemoveTheLineBreak") };
            deleteLineConnector.Click += OnDeleteLineConnector;

            itemCollection.Add(deleteLineConnector);

            return itemCollection;
        }
        #endregion Generic menu items methods

        #region Click actions
        private void OnElectricConnect_Click(object sender, RoutedEventArgs e)
        {
            var typ = e.OriginalSource.GetType();

            if (e?.OriginalSource is MenuItem)
            {
                var menuItem = (MenuItem)e.OriginalSource;

                if (this.PlacementTarget is OutputNode)
                {
                    // zapis źródła 2
                    var output2 = (OutputNode)this.PlacementTarget;
                    if (output2.Parent is SchemeCanvas && ((SchemeCanvas)output2.Parent).SelectedElement is OutputNode)
                    {
                        // zapis scheme canvas i wyjścia 2
                        var schemeCanvas = (SchemeCanvas)output2.Parent;
                        var output1 = (OutputNode)schemeCanvas.SelectedElement;

                        // tworzenie nowej linii
                        var schemeLine = new SchemeLine() { Stroke = Settings.DefaultLineColor, StrokeThickness = Settings.LinesStrokeThickness };
                        schemeLine.Connect(output1, output2); // wykonanie połączenia
                        schemeLine.ContextMenu = new SchemeElementContextMenu();
                        schemeCanvas.Children.Add(schemeLine);
                    }
                }
            }
        }
        #endregion Click actions
        #endregion Synoptic elements

        #region IEditable
        private void AddEditableElementContextMenuItems(IList<FrameworkElement> itemCollection = null)
        {
            if (itemCollection == null)
            {
                itemCollection = new List<FrameworkElement>();
                this.ItemsSource = itemCollection;
            }
            else if (itemCollection.Count == 0)
            {
                this.ItemsSource = itemCollection;
            }
            else
                itemCollection.Add(new Separator());

            MenuItem copyItem = new MenuItem() { Header = "Kopiuj", Foreground = this.Foreground, Icon = SchemeControlsLibResources.GetIconByKey("Copy") };
            copyItem.Click += OnCopyCommand;

            MenuItem deleteItem = new MenuItem() { Header = "Usuń", Foreground = this.Foreground, Icon = SchemeControlsLibResources.GetIconByKey("Delete") };
            deleteItem.Click += OnDeleteCommand;

            itemCollection.Add(copyItem);
            itemCollection.Add(deleteItem);
        }
        #endregion IEditable

        #region IOutputNode
        private IList<FrameworkElement> AddOutputMenuItems(EnumTypes.SchemeTypes schemeType)
        {
            List<FrameworkElement> itemCollection = new List<FrameworkElement>();

            MenuItem addedOutput = new MenuItem() { Header = "Połącz", Foreground = this.Foreground, Icon = SchemeControlsLibResources.GetIconByKey("Connect") };
            if (schemeType == EnumTypes.SchemeTypes.SynopticScheme)
                addedOutput.Click += OnElectricConnect_Click;
            else if (schemeType == EnumTypes.SchemeTypes.NetworkScheme)
                addedOutput.Click += OnNetworkConnect_Click;
            else if (schemeType == EnumTypes.SchemeTypes.CoalProcessingScheme)
                addedOutput.Click += OnCoalConnect_Click;

            itemCollection.Add(addedOutput);

            return itemCollection;
        }
        #endregion IOutputNode

        #region Common actions
        private void OnCopyCommand(object sender, RoutedEventArgs e)
        {
            var schemeElement = this.PlacementTarget as FrameworkElement;
            SchemeCanvas schemeCanvas = null;
            if (schemeElement != null)
                schemeCanvas = schemeElement.Parent as SchemeCanvas;

            //var copiedElement = (FrameworkElement)((IEditableControl)schemeElement).Copy();

            if (schemeCanvas != null)
            {
                schemeCanvas.CopySelectedChild();
                //if (!schemeCanvas.SelectionDict.Any())
                //    schemeCanvas.CopiedElement = (ObservableControl)copiedElement;
                //else
                //    schemeCanvas.CopySelection();
            }
        }

        private void OnDeleteConnectionCommand(object sender, RoutedEventArgs e)
        {
            MenuItem mnu = sender as MenuItem;
            if(this.PlacementTarget is ILine line)
            {
                line.Disconnect(true);
            }
        }

        private void OnDeleteCommand(object sender, RoutedEventArgs e)
        {
            MenuItem mnu = sender as MenuItem;
            FrameworkElement schemeElement = this.PlacementTarget as FrameworkElement;
            SchemeCanvas schemeCanvas = null;
            if (schemeElement != null)
                schemeCanvas = schemeElement.Parent as SchemeCanvas;

            if (schemeCanvas != null)
                schemeCanvas.DeleteSelectedChild();

            //if (schemeCanvas != null && schemeCanvas.SelectedElement != null)
            //{
            //    if (schemeCanvas.SelectedElement is IEditableControl)
            //        ((IEditableControl)schemeCanvas.SelectedElement).Remove();
            //}
            //else if((schemeCanvas != null && schemeCanvas.SelectionDict != null && schemeCanvas.SelectionDict.Any()))
            //{
            //    schemeCanvas.DeleteSelectedItems();
            //}
        }

        private void OnDeleteLineConnector(object sender, RoutedEventArgs e)
        {
            FrameworkElement schemeElement = this.PlacementTarget as FrameworkElement;

            if (schemeElement is LineConnector)
            {
                var lineConnector = (LineConnector)schemeElement;
                lineConnector.RemoveTheLinieBreak();
            }
        }

        private void OnChangeOrientation(object sender, RoutedEventArgs e)
        {
            FrameworkElement schemeElement = this.PlacementTarget as FrameworkElement;

            if (schemeElement is ElectricElementBase)
            {
                var electricElement = (ElectricElementBase)schemeElement;
                var currentOrientation = electricElement.ControlOrientation;
                electricElement.SetValue(ElectricElementBase.ControlOrientationProperty, currentOrientation == Orientation.Vertical ? Orientation.Horizontal : Orientation.Vertical);
            }
        }

        private void OnBreakTheLine(object sender, RoutedEventArgs e )
        {
            if (this.PlacementTarget is ILine schemeLine)
            {
                if (_schemeCanvas != null)
                    schemeLine.BreakLine(_menuOpendPoint, _schemeCanvas);
            }
        }

        private void OnChangeConnection(object sender, RoutedEventArgs e)
        {
            FrameworkElement schemeElement = this.PlacementTarget as FrameworkElement;

            if (schemeElement is ILine)
            {
                var electricElement = (ILine)schemeElement;
                var currentDrawing = electricElement.ReverseDrawing;
                electricElement.ReverseDrawing = !currentDrawing;
            }
        }

        private void OnConnectToSource(object sender, RoutedEventArgs e)
        {
            var typ = e.OriginalSource.GetType();

            if (e?.OriginalSource is MenuItem)
            {
                var menuItem = (MenuItem)e.OriginalSource;
                if (this.PlacementTarget is Source)
                {
                    var source = (Source)this.PlacementTarget;
                    if (source.Parent is SchemeCanvas && ((SchemeCanvas)source.Parent).SelectedElement is OutputNode)
                    {
                        var output = ((SchemeCanvas)source.Parent).SelectedElement as OutputNode;
                        source.Connect(output);
                    }
                }
            }
        }
        #endregion Common actions

    }
}
