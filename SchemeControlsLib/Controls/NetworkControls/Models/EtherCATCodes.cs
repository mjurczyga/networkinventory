﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.NetworkControls.Models
{
    public static class EtherCATCodes
    {
        private static Dictionary<int, string> EtherCATStates0 = new Dictionary<int, string>()
        {
            { 0 , "Nieznany stan" },
            { 1 , "Moduł w stanie 'INIT'" },
            { 2 , "Moduł w stanie 'Pre-Operational'" },
            { 3 , "Moduł w stanie 'BOOT'" },
            { 4 , "Moduł w stanie 'Safe-Operational'" },
            { 8 , "Moduł w stanie 'Operational'" },
        };

        private static Dictionary<int, string> EtherCATStates1 = new Dictionary<int, string>()
        {
            { 10 , "Błędne syganły modułu" },
            { 20 , "Nieprawidłowy vendorId, productCode... czytanie" },
            { 40 , "Wystąpił błąd inicjalizacji" },
            { 80 , "Moduł wyłączony" },
            { 100 , "Moduł nieobecny" },
            { 200 , "Błędne sygnały połączeń modułu" },
            { 400 , "Brakujące sygnały połączeń modułu" },
            { 800 , "Nieoczekiwane sygnały połączeń modułu" },
            { 1000 , "Port komunikacyjny A" },
            { 2000 , "Port komunikacyjny B" },
            { 4000 , "Port komunikacyjny C" },
            { 8000 , "Port komunikacyjny D" },
        };


        public static string GetCodeDescription(int codeNumber)
        {
            string hexValue = codeNumber.ToString("X");
            string result = "Brak danych";

            if (!string.IsNullOrEmpty(hexValue))
            {
                var stateVal = hexValue.Last().ToString();

                if (int.TryParse(hexValue, out int hexIntVal) && int.TryParse(stateVal, out int hexStateVal))
                {
                    var descriptionCode = hexIntVal - hexStateVal;

                    if (EtherCATStates0.TryGetValue(hexStateVal, out string state))
                    {
                        result = state;
                    }

                    if (EtherCATStates1.TryGetValue(descriptionCode, out string description))
                    {
                        if (result == "Brak danych")
                        {
                            result = description;
                        }
                        else
                        {
                            result = result + ": " + description;
                        }
                    }

                    return result;
                }

            }

            return result;
        }
    }
}
