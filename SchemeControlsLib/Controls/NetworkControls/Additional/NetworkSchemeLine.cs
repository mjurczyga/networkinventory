﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.NetworkControls.Interfaces;
using SchemeControlsLib.Controls.NetworkControls.Types;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.NetworkControls
{
    [EditorType(SchemeTypes.NetworkScheme, false, "Linia sieci")]
    public sealed class NetworkSchemeLine : LineBase, INetworkLine
    {

        [EditorProperty("Typ sieci", "Ustawienia sieci")]
        public NetworkType NetworkType
        {
            get { return (NetworkType)GetValue(NetworkTypeProperty); }
            set { SetValue(NetworkTypeProperty, value); }
        }
        public static readonly DependencyProperty NetworkTypeProperty =
            DependencyProperty.Register("NetworkType", typeof(NetworkType), typeof(NetworkSchemeLine), new PropertyMetadata(NetworkType_PropertyChanged));

        private static void NetworkType_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is NetworkSchemeLine networkSchemeLine)
            {
                var networkLine = NetworkLines.GetElementByType(networkSchemeLine.NetworkType);
                if (networkLine != networkSchemeLine.NetworkLine)
                    networkSchemeLine.NetworkLine = networkLine;

                if (networkSchemeLine.OutputNode1 is INetworkElement networkOutput1)
                    networkOutput1.NetworkType = networkSchemeLine.NetworkType;

                if (networkSchemeLine.OutputNode2 is INetworkElement networkOutput2)
                    networkOutput2.NetworkType = networkSchemeLine.NetworkType;
            }
        }
        #region ILine props

        #region INetworkLine dependency property
        [EditorProperty("Typ linii", "Ustawienia sieci", Serialize = false)]
        public NetworkLine NetworkLine
        {
            get { return (NetworkLine)GetValue(NetworkLineProperty); }
            set { SetValue(NetworkLineProperty, value); }
        }

        public static readonly DependencyProperty NetworkLineProperty =
            DependencyProperty.Register("NetworkLine", typeof(NetworkLine), typeof(NetworkSchemeLine), new PropertyMetadata(NetworkLine_PropertyChanged));

        private static void NetworkLine_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is NetworkSchemeLine networkSchemeLine && e.NewValue is NetworkLine newNetworkLine)
            {
                if (e.OldValue == null ||(e.OldValue is NetworkLine oldNetworkLine && oldNetworkLine .NetworkType != newNetworkLine.NetworkType))
                {
                    networkSchemeLine.Stroke = newNetworkLine.Stroke;
                    networkSchemeLine.InternalBackgroundBrush = newNetworkLine.Stroke;
                    networkSchemeLine.StrokeThickness = newNetworkLine.StrokeThickness;
                    networkSchemeLine.ToolTip = newNetworkLine.Name;

                    networkSchemeLine.NetworkType = newNetworkLine.NetworkType;
                }
            }
        }
        #endregion Network line dependency property
       
        public override void Connect(IOutputNode outputNode, IOutputNode outputNode1)
        {
            if (outputNode is NetworkOutput networkOutput1 && outputNode1 is NetworkOutput networkOutput2)
            {
                var networkType = NetworkTypes.None;
                if (networkOutput1.NetworkType == networkOutput2.NetworkType)
                {
                    networkType = networkOutput2.NetworkType;
                }
                this.NetworkType = networkType;

                bool result = DependencyPropertyFinder.CopyParentClickHandler(networkOutput1, this);
                if (!result)
                    DependencyPropertyFinder.CopyParentClickHandler(networkOutput2, this);

                networkOutput1.AddLine(this);
                networkOutput2.AddLine(this);

                this.OutputNode1 = networkOutput1;
                this.OutputNode2 = networkOutput2;

                this.SetValue(FrameworkElement.NameProperty, networkOutput1.Name + "_TO_" + networkOutput2.Name);

                this.UpdateConnection(outputNode, outputNode1);
            }
        }


        public override void Disconnect(bool remove = false)
        {
           if(this.Parent is SchemeCanvas schemeCanvas && this.OutputNode1 is NetworkOutput networkOutput1 && this.OutputNode2 is NetworkOutput networkOutput2)
            {
                //TODO: Dopisać połączeie linni 

                //jeśli linia jest połączona z NetworkLineBreaker, to usuń linie, breaker i połącz pozostałe elementy
                //var no1_CP = networkOutput1.ControlParent;
                //var no2_CP = networkOutput2.ControlParent;

                //if (no1_CP is NetworkLineBreaker networkLineBreaker1 && (!(no2_CP is NetworkLineBreaker) || no2_CP is BaseNetworkControl baseNetworkControl2))
                //{
                //}

                networkOutput1.RemoveLine(this);
                networkOutput2.RemoveLine(this);
                schemeCanvas.RemoveElementFromScheme(this);
            }
        }

        public override void BreakLine(Point menuOpenedPoint, SchemeCanvas schemeCanvas)
        {
            var networkLineBreaker = new NetworkLineBreaker() { NetworkType = this.NetworkType };
            schemeCanvas.AddSchemeElement(networkLineBreaker, true);

            networkLineBreaker.SetPosition(menuOpenedPoint.X - networkLineBreaker.Width / 2.0, menuOpenedPoint.Y - networkLineBreaker.Height / 2.0);
            networkLineBreaker.BreakTheLine(this, schemeCanvas);
        }

        protected override void UpdateBackgroundBrush(int value)
        {
            _Dispatcher.Invoke(() =>
            {
                if (value == 0 || value == 1)
                {
                    InternalBackgroundBrush = Settings.StatusOffBrush;
                }
                else
                {
                    InternalBackgroundBrush = NetworkLine == null ? Brushes.GreenYellow : this.Stroke;
                }
            });
        }
        #endregion Control Drawing 

        public NetworkSchemeLine()
        {
        }

        static NetworkSchemeLine()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NetworkSchemeLine), new FrameworkPropertyMetadata(typeof(NetworkSchemeLine)));
        }
    }
}
