﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.NetworkControls.Interfaces;
using SchemeControlsLib.Controls.NetworkControls.Types;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.NetworkControls
{
    [EditorType(SchemeTypes.SynopticScheme, false, "Załamanie lini - sieci")]
    public sealed class NetworkLineBreaker : NetworkControlBase, IHideableElement, ILineBreaker
    {
        #region dependency properties
        public bool HideElement
        {
            get { return (bool)GetValue(IsElementHiddenProperty); }
            set { SetValue(IsElementHiddenProperty, value); }
        }

        public static readonly DependencyProperty IsElementHiddenProperty =
            DependencyProperty.Register("HideElement", typeof(bool), typeof(NetworkLineBreaker), new PropertyMetadata(false, HideElement_PropertyChanged));

        private static void HideElement_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is NetworkLineBreaker networkLineBreaker)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                {
                    if(boolVal)
                        networkLineBreaker.Visibility = Visibility.Hidden;
                    else
                        networkLineBreaker.Visibility = Visibility.Visible;
                }
            }
        }
        #endregion

        #region ILineBreaker
        private bool _IsStateUpdating = false;
        public bool IsStateUpdating
        {
            get { return _IsStateUpdating; }
            private set { _IsStateUpdating = value; NotifyPropertyChanged("IsStateUpdating"); }
        }

        public void BreakTheLine(ILine schemeLine, SchemeCanvas schemeCanvas)
        {
            if (schemeCanvas != null)
            {
                this.SetIsHitTestVisibleToOutputs(true);

                var on1 = schemeLine.OutputNode1;
                var on2 = schemeLine.OutputNode2;

                var networkType = NetworkTypes.None;
                if (on1 is NetworkOutput networkOutput1 && on2 is NetworkOutput networkOutput2)
                {
                    if (networkOutput1.NetworkType == networkOutput2.NetworkType)
                    {
                        networkType = networkOutput1.NetworkType;
                    }
                    schemeLine.Disconnect();

                    this.HideElement = !schemeCanvas.IsEditMode;

                    //pierwsza linia                            
                    var newSchemeLine1 = new NetworkSchemeLine() { NetworkType = networkType };
                    var output1 = this.AddOutput(this.CenterPoint.X, this.CenterPoint.Y) as NetworkOutput;
                    schemeCanvas.AddSchemeElement(newSchemeLine1, false, $"{output1?.Name}_TO_{networkOutput1?.Name}");
                    newSchemeLine1.Connect(output1, on1);

                    //druga linia
                    var newSchemeLine2 = new NetworkSchemeLine() { NetworkType = networkType };
                    schemeCanvas.AddSchemeElement(newSchemeLine2, false, $"{networkOutput2?.Name}_TO_{output1?.Name}");
                    newSchemeLine2.Connect(on2, output1);

                }
            }
        }

        public void UpdateBreakerState(ILine schemeLine, IOutputNode outputNode, int state)
        {
            // dopisać przechodzenie koloru przez connector
        }
        #endregion ILineBreaker

        private void SetIsHitTestVisibleToOutputs(bool isHitTestVisible)
        {
            foreach (var output in OutputNodes)
            {
                if (output is Control control)
                    control.IsHitTestVisible = isHitTestVisible;
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
        }

        static NetworkLineBreaker()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NetworkLineBreaker), new FrameworkPropertyMetadata(typeof(NetworkLineBreaker)));
        }

        public NetworkLineBreaker()
        {
            this.Width = 16;
            this.Height = 16;


            DependencyPropertyDescriptor.FromProperty(NetworkTypeProperty, typeof(NetworkLineBreaker)).AddValueChanged(this, NetworkType_PropertyChanged);
        }

        private void NetworkType_PropertyChanged(object sender, EventArgs e)
        {
            if(sender is NetworkLineBreaker networkLineBreaker)
            {
                lock (networkLineBreaker.OutputNodes)
                {
                    foreach (var outputNode in networkLineBreaker.OutputNodes)
                    {
                        if (outputNode is INetworkElement networkElement)
                            networkElement.NetworkType = networkLineBreaker.NetworkType;
                    }
                }
            }
        }
    }
}
