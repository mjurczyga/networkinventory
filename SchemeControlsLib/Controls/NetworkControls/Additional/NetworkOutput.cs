﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.NetworkControls.Interfaces;
using SchemeControlsLib.Controls.NetworkControls.Types;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SchemeControlsLib.Controls.NetworkControls
{
    [EditorType(SchemeTypes.NetworkScheme, false, "Wyjście kontrolki - sieci")]
    public sealed class NetworkOutput : OutputBase, INetworkElement
    {
        #region INetworkElement
        [EditorProperty("Typ sieci", "Ustawienia sieci")]
        public NetworkType NetworkType
        {
            get { return (NetworkType)GetValue(NetworkTypeProperty); }
            set { SetValue(NetworkTypeProperty, value); }
        }
        public static readonly DependencyProperty NetworkTypeProperty = 
            DependencyProperty.Register("NetworkType", typeof(NetworkType), typeof(NetworkOutput), new PropertyMetadata(NetworkTypes.None, NetworkType_PropertyChanged));

        private static void NetworkType_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is NetworkOutput networkOutput)
            {
                foreach (var line in networkOutput.ConnectedLines)
                {
                    if(line is NetworkLine networkLine)
                        networkLine.NetworkType = networkOutput.NetworkType;
                }
            }
        }
        #endregion INetworkElement

        static NetworkOutput()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NetworkOutput), new FrameworkPropertyMetadata(typeof(NetworkOutput)));
        }

        public NetworkOutput(FrameworkElement controlParent) : base(controlParent)
        {
            if (controlParent is NetworkControlBase baseNetworkControl)
                this.NetworkType = baseNetworkControl.NetworkType;

            this.ConstructorMethods();
        }
        public NetworkOutput()
        {
            this.ConstructorMethods();
        }

        private void ConstructorMethods()
        {
        }
    }
}
