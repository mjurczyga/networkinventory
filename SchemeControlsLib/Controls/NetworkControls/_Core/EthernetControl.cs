﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.NetworkControls._Core.Helpers;
using SchemeControlsLib.Controls.NetworkControls.Interfaces;
using SchemeControlsLib.Controls.NetworkControls.Types;
using SchemeControlsLib.Services.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;

namespace SchemeControlsLib.Controls.NetworkControls
{
    public class EthernetControl : NetworkControlBase, IEthernetControl
    {
        [EditorProperty("Czy pingować?", "Ustawienia sieci", "Pingować (true) czy użyć rest api (false)" )]
        public bool IfUsePing
        {
            get { return (bool)GetValue(IfUsePingProperty); }
            set { SetValue(IfUsePingProperty, value); }
        }

        public static readonly DependencyProperty IfUsePingProperty =
            DependencyProperty.Register("IfUsePing", typeof(bool), typeof(EthernetControl), new PropertyMetadata(false));

        [EditorProperty("Adres IP", "Ustawienia sieci")]
        public string IP
        {
            get { return (string)GetValue(IPProperty); }
            set { SetValue(IPProperty, value); }
        }

        public static readonly DependencyProperty IPProperty =
            DependencyProperty.Register("IP", typeof(string), typeof(EthernetControl), new PropertyMetadata(""));


        #region Ping service
        //[EditorProperty("Interwał pingu", "Ustawienia IP")]
        //public double PingInterval
        //{
        //    get { return (double)GetValue(PingIntervalProperty); }
        //    set { SetValue(PingIntervalProperty, value); }
        //}

        //public static readonly DependencyProperty PingIntervalProperty =
        //    DependencyProperty.Register("PingInterval", typeof(double), typeof(EthernetControl), new PropertyMetadata(0.0, PingInterval_PropertyChnaged));

        //private static void PingInterval_PropertyChnaged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        //{
        //    if(d is EthernetControl ethernetControl)
        //    {
                
        //    }
        //}

        public bool TryGetIPAdress(string ip, out IPAddress iPAddress)
        {
            return IPHelpers.TryGetIPAdress(ip, out iPAddress);
        }

        private bool _busyPing = false;
        public Task RefreshState()
        {
            bool isIP = false;
            bool usePing = false;
            string ipString = "";
            _Dispatcher.Invoke(() =>
            {
                isIP = IPHelpers.TryGetIPAdress(IP, out IPAddress ip);
                ipString = this.IP;
                usePing = this.IfUsePing;
            });

            return Task.Run(async () =>
            {

                if (!_busyPing)
                {
                    _busyPing = true;
                    if (usePing)
                    {

                        if (isIP)
                        {
                            var pingState = new Ping();
                            try
                            {
                                var result = pingState.Send(ipString);
                                if (result.Status == IPStatus.Success)
                                    State = 2;
                                else
                                    State = 1;

                            }
                            catch (Exception e)
                            {
                                State = 0;
                            }
                        }
                        else
                            State = 0;
                    }
                    else
                    {
                        if (isIP)
                        {
                            var result = await RestHttpClient.GetDeviceStatus(ipString);
                            if (result)
                                State = 2;
                            else
                                State = 1;
                        }
                        else
                            State = 0;
                    }
                    NotifyPropertyChanged("State");
                    NotifyPropertyChanged("Connected");
                    _busyPing = false;
                }
            });

        }
        #endregion Ping service

        public void SetState(int state)
        {
            State = state;
        }

        public EthernetControl()
        {
            this.NetworkType = NetworkTypes.Ethernet;
        }
    }
}
