﻿using SchemeControlsLib.Controls.NetworkControls.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.NetworkControls.Interfaces
{
    public interface INetworkElement
    {
        NetworkType NetworkType { get; set;}        
    }
}
