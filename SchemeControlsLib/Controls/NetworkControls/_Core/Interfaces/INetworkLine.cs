﻿using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.NetworkControls.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Controls.NetworkControls.Interfaces
{
    public interface INetworkLine
    {
        NetworkLine NetworkLine { get; set; }

        UIElement Lines { get; }
    }
}
