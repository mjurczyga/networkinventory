﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.NetworkControls.Interfaces
{
    public interface IEthernetControl
    {        
        string IP { get; set; }
        //double PingInterval { get; set; }

        bool IfUsePing { get; set; }

        bool TryGetIPAdress(string ip, out IPAddress iPAddress);
        Task RefreshState();

        void SetState(int state);
    }
}
