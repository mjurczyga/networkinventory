﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.NetworkControls._Core.Helpers
{
    public static class IPHelpers
    {
        public static bool TryGetIPAdress(string ip, out IPAddress iPAddress)
        {
            bool result = false;
            iPAddress = null;

            if (!string.IsNullOrEmpty(ip) && IPAddress.TryParse(ip, out IPAddress ipAddress))
            {
                iPAddress = ipAddress;
                return true;
            }

            return result;
        }
    }
}
