﻿using SchemeControlsLib.Base;
using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Services.PLC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.NetworkControls._Core
{
    public abstract class BaseTangoControl : NetworkControlBase
    {
        #region control properties
        public Brush InternalPortBackgroundBrush { get; private set; }
        #endregion

        [EditorProperty("Rozmiar tekstu pomiaórów")]
        public double MeasuresFontSize
        {
            get { return (double)GetValue(MeasuresFontSizeProperty); }
            set { SetValue(MeasuresFontSizeProperty, value); }
        }

        public static readonly DependencyProperty MeasuresFontSizeProperty =
            DependencyProperty.Register("MeasuresFontSize", typeof(double), typeof(BaseTangoControl), new PropertyMetadata(5.0));

        #region Pomiar 1
        [EditorProperty("Pomiar 1 - nazwa")]
        public object FirstMeasureName
        {
            get { return (object)GetValue(FirstMeasureNameProperty); }
            set { SetValue(FirstMeasureNameProperty, value); }
        }

        public static readonly DependencyProperty FirstMeasureNameProperty =
            DependencyProperty.Register("FirstMeasureName", typeof(object), typeof(BaseTangoControl), new PropertyMetadata());

        [EditorProperty("Pomiar 1 - jednostka")]
        public object FirstMeasureUnit
        {
            get { return (object)GetValue(FirstMeasureUnitProperty); }
            set { SetValue(FirstMeasureUnitProperty, value); }
        }

        public static readonly DependencyProperty FirstMeasureUnitProperty =
            DependencyProperty.Register("FirstMeasureUnit", typeof(object), typeof(BaseTangoControl), new PropertyMetadata());

        [EditorProperty("Pomiar 1 - wartość")]
        public object FirstMeasureValue
        {
            get { return (object)GetValue(FirstMeasureValueProperty); }
            set { SetValue(FirstMeasureValueProperty, value); }
        }

        public static readonly DependencyProperty FirstMeasureValueProperty =
            DependencyProperty.Register("FirstMeasureValue", typeof(object), typeof(BaseTangoControl), new PropertyMetadata(FirstMeasureValue_PropertyChanged));

        private static void FirstMeasureValue_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is BaseTangoControl baseTangoControl)
            {
                baseTangoControl.FirstMeasureValueString = ValuesComparer.ProcessObjValue(e.NewValue);
            }
        }

        private string _FirstMeasureValueString;
        public string FirstMeasureValueString
        {
            get { return _FirstMeasureValueString; }
            set { _FirstMeasureValueString = value; NotifyPropertyChanged("FirstMeasureValueString"); }
        }
        #endregion Pomiar 1

        #region Pomiar 2
        [EditorProperty("Pomiar 2 - nazwa")]
        public object SecondMeasureName
        {
            get { return (object)GetValue(SecondMeasureNameProperty); }
            set { SetValue(SecondMeasureNameProperty, value); }
        }

        public static readonly DependencyProperty SecondMeasureNameProperty =
            DependencyProperty.Register("SecondMeasureName", typeof(object), typeof(BaseTangoControl), new PropertyMetadata());

        [EditorProperty("Pomiar 2 - jednostka")]
        public object SecondMeasureUnit
        {
            get { return (object)GetValue(SecondMeasureUnitProperty); }
            set { SetValue(SecondMeasureUnitProperty, value); }
        }

        public static readonly DependencyProperty SecondMeasureUnitProperty =
            DependencyProperty.Register("SecondMeasureUnit", typeof(object), typeof(BaseTangoControl), new PropertyMetadata());

        [EditorProperty("Pomiar 2 - wartość")]
        public object SecondMeasureValue
        {
            get { return (object)GetValue(SecondMeasureValueProperty); }
            set { SetValue(SecondMeasureValueProperty, value); }
        }

        public static readonly DependencyProperty SecondMeasureValueProperty =
            DependencyProperty.Register("SecondMeasureValue", typeof(object), typeof(BaseTangoControl), new PropertyMetadata(SecondMeasureValue_PropertyChanged));

        private static void SecondMeasureValue_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is BaseTangoControl baseTangoControl)
            {
                baseTangoControl.SecondMeasureValueString = ValuesComparer.ProcessObjValue(e.NewValue);
            }
        }

        private string _SecondMeasureValueString;
        public string SecondMeasureValueString
        {
            get { return _SecondMeasureValueString; }
            set { _SecondMeasureValueString = value; NotifyPropertyChanged("SecondMeasureValueString"); }
        }
        #endregion Pomiar 2

        #region Pomiar 3
        [EditorProperty("Pomiar 3 - nazwa")]
        public object ThirdMeasureName
        {
            get { return (object)GetValue(ThirdMeasureNameProperty); }
            set { SetValue(ThirdMeasureNameProperty, value); }
        }

        public static readonly DependencyProperty ThirdMeasureNameProperty =
            DependencyProperty.Register("ThirdMeasureName", typeof(object), typeof(BaseTangoControl), new PropertyMetadata());

        [EditorProperty("Pomiar 3 - jednostka")]
        public object ThirdMeasureUnit
        {
            get { return (object)GetValue(ThirdMeasureUnitProperty); }
            set { SetValue(ThirdMeasureUnitProperty, value); }
        }

        public static readonly DependencyProperty ThirdMeasureUnitProperty =
            DependencyProperty.Register("ThirdMeasureUnit", typeof(object), typeof(BaseTangoControl), new PropertyMetadata());

        [EditorProperty("Pomiar 3 - wartość")]
        public object ThirdMeasureValue
        {
            get { return (object)GetValue(ThirdMeasureValueProperty); }
            set { SetValue(ThirdMeasureValueProperty, value); }
        }

        public static readonly DependencyProperty ThirdMeasureValueProperty =
            DependencyProperty.Register("ThirdMeasureValue", typeof(object), typeof(BaseTangoControl), new PropertyMetadata(ThirdMeasureValue_PropertyChanged));

        private static void ThirdMeasureValue_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is BaseTangoControl baseTangoControl)
            {
                baseTangoControl.ThirdMeasureValueString = ValuesComparer.ProcessObjValue(e.NewValue);
            }
        }

        private string _ThirdMeasureValueString;
        public string ThirdMeasureValueString
        {
            get { return _ThirdMeasureValueString; }
            set { _ThirdMeasureValueString = value; NotifyPropertyChanged("ThirdMeasureValueString"); }
        }
        #endregion Pomiar 3

        #region Pomiar 4
        [EditorProperty("Pomiar 4 - nazwa")]
        public object FourthMeasureName
        {
            get { return (object)GetValue(FourthMeasureNameProperty); }
            set { SetValue(FourthMeasureNameProperty, value); }
        }

        public static readonly DependencyProperty FourthMeasureNameProperty =
            DependencyProperty.Register("FourthMeasureName", typeof(object), typeof(BaseTangoControl), new PropertyMetadata());

        [EditorProperty("Pomiar 4 - jednostka")]
        public object FourthMeasureUnit
        {
            get { return (object)GetValue(FourthMeasureUnitProperty); }
            set { SetValue(FourthMeasureUnitProperty, value); }
        }

        public static readonly DependencyProperty FourthMeasureUnitProperty =
            DependencyProperty.Register("FourthMeasureUnit", typeof(object), typeof(BaseTangoControl), new PropertyMetadata());

        [EditorProperty("Pomiar 4 - wartość")]
        public object FourthMeasureValue
        {
            get { return (object)GetValue(FourthMeasureValueProperty); }
            set { SetValue(FourthMeasureValueProperty, value); }
        }

        public static readonly DependencyProperty FourthMeasureValueProperty =
            DependencyProperty.Register("FourthMeasureValue", typeof(object), typeof(BaseTangoControl), new PropertyMetadata(FourthMeasureValue_PropertyChanged));

        private static void FourthMeasureValue_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is BaseTangoControl baseTangoControl)
            {
                baseTangoControl.FourthMeasureValueString = ValuesComparer.ProcessObjValue(e.NewValue);
            }
        }

        private string _FourthMeasureValueString;
        public string FourthMeasureValueString
        {
            get { return _FourthMeasureValueString; }
            set { _FourthMeasureValueString = value; NotifyPropertyChanged("FourthMeasureValueString"); }
        }
        #endregion Pomiar 4

        #region Dioda Awarii
        [EditorProperty("Zmienna 'Czy awaria?'", "Urządzenie")]
        public object AW
        {
            get { return (object)GetValue(AWProperty); }
            set { SetValue(AWProperty, value); }
        }
        public static readonly DependencyProperty AWProperty =
            DependencyProperty.Register("AW", typeof(object), typeof(BaseTangoControl), new PropertyMetadata(AW_PropertyChanged));

        private static void AW_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is BaseTangoControl baseTangoControl)
            {
                if (e.NewValue != null)
                {
                    baseTangoControl.AWDiodeVisibility = Visibility.Visible;
                    if (bool.TryParse(e.NewValue?.ToString(), out bool boolValue))
                    {
                        if (boolValue)
                            baseTangoControl._aw_animationTimer.Animate(1000, baseTangoControl.OnAWTimerElapsed, baseTangoControl.Name);
                        else
                        {
                            baseTangoControl._aw_animationTimer.CleanTimer();
                            baseTangoControl.AWBrush = Brushes.Gray;
                        }

                    }
                }
                else
                {
                    baseTangoControl.AWDiodeVisibility = Visibility.Collapsed;
                }
            }
        }


        private Brush _AWBrush = Brushes.Gray;
        public Brush AWBrush
        {
            get { return _AWBrush; }
            set { _AWBrush = value; NotifyPropertyChanged("AWBrush"); }
        }

        private Visibility _AWDiodeVisibility = Visibility.Collapsed;
        public Visibility AWDiodeVisibility
        {
            get { return _AWDiodeVisibility; }
            set { _AWDiodeVisibility = value; NotifyPropertyChanged("AWDiodeVisibility"); }
        }

        private AnimationTimer _aw_animationTimer = new AnimationTimer();

        protected virtual void OnAWTimerElapsed()
        {
            if (bool.TryParse(AW?.ToString(), out bool resultBool))
            {
                if (AWBrush == Brushes.Red)
                    AWBrush = Brushes.Gray;
                else
                    AWBrush = Brushes.Red;
            }
            else
            {
                _aw_animationTimer?.CleanTimer();
                AWBrush = Brushes.Gray;
            }


        }
        #endregion Dioda Awarii

        #region Dioda Uprzedzenia
        [EditorProperty("Zmienna 'Czy uprzedzenie?'", "Urządzenie")]
        public object UP
        {
            get { return (object)GetValue(UPProperty); }
            set { SetValue(UPProperty, value); }
        }
        public static readonly DependencyProperty UPProperty =
            DependencyProperty.Register("UP", typeof(object), typeof(BaseTangoControl), new PropertyMetadata(UP_PropertyChanged));

        private static void UP_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is BaseTangoControl baseTangoControl)
            {
                if (e.NewValue != null)
                {
                    baseTangoControl.UPDiodeVisibility = Visibility.Visible;
                    if (bool.TryParse(e.NewValue?.ToString(), out bool boolValue))
                    {
                        if (boolValue)
                            baseTangoControl._up_animationTimer.Animate(1000, baseTangoControl.OnUPTimerElapsed, baseTangoControl.Name);
                        else
                        {
                            baseTangoControl._up_animationTimer.CleanTimer();
                            baseTangoControl.UPBrush = Brushes.Gray;
                        }
                    }
                }
                else
                {
                    baseTangoControl.UPDiodeVisibility = Visibility.Collapsed;
                }
            }
        }


        private Brush _UPBrush = Brushes.Gray;
        public Brush UPBrush
        {
            get { return _UPBrush; }
            set { _UPBrush = value; NotifyPropertyChanged("UPBrush"); }
        }

        private Visibility _UPDiodeVisibility = Visibility.Collapsed;
        public Visibility UPDiodeVisibility
        {
            get { return _UPDiodeVisibility; }
            set { _UPDiodeVisibility = value; NotifyPropertyChanged("UPDiodeVisibility"); }
        }

        private AnimationTimer _up_animationTimer = new AnimationTimer();

        protected virtual void OnUPTimerElapsed()
        {
            if (bool.TryParse(UP?.ToString(), out bool resultBool))
            {
                if (UPBrush == Brushes.Yellow)
                    UPBrush = Brushes.Gray;
                else
                    UPBrush = Brushes.Yellow;
            }
            else
            {
                _up_animationTimer?.CleanTimer();
                this.UPBrush = Brushes.Gray;
            }


        }
        #endregion Dioda Uprzedzenia

        [EditorProperty("Kolor tła")]
        public new Brush Background { get { return base.Background; } set { base.Background = value; }  }

        public override void Remove()
        {
            base.Remove();
            _aw_animationTimer?.CleanTimer();
            _up_animationTimer?.CleanTimer();
        }

        protected override void UpdateBackgroundBrush(int value)
        {
            if (value == 2)
            {
                InternalBackgroundBrush = Background;
                InternalPortBackgroundBrush = Settings.StatusPortOkBrush;
            }
            else
            {
                InternalBackgroundBrush = (SolidColorBrush)new BrushConverter().ConvertFrom("#FFCB1402");
                InternalPortBackgroundBrush = Settings.StatusPortOffBrush;
            }

        }

        public BaseTangoControl()
        {
            DependencyPropertyDescriptor.FromProperty(Control.BackgroundProperty, typeof(BaseTangoControl)).AddValueChanged(this, Background_PropertyChanged);
        }

        private void Background_PropertyChanged(object sender, EventArgs e)
        {
            UpdateBackgroundBrush(this.State);
        }
    }
}
