﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.Controls.Interfaces;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Controls.NetworkControls.Interfaces;
using SchemeControlsLib.Controls.NetworkControls.Models;
using SchemeControlsLib.Controls.NetworkControls.Types;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.NetworkControls
{
    public abstract class NetworkControlBase : EditableControlBase, INetworkElement
    {
        [EditorProperty("Typ sieci", "Ustawienia sieci")]
        public NetworkType NetworkType
        {
            get { return (NetworkType)GetValue(NetworkTypeProperty); }
            set { SetValue(NetworkTypeProperty, value); }
        }
        public static readonly DependencyProperty NetworkTypeProperty = DependencyProperty.Register("NetworkType", typeof(NetworkType), typeof(NetworkControlBase), new PropertyMetadata(NetworkTypes.None));
    
        public override IOutputWithLines AddOutput(double x_Position, double y_Position, bool checkBounds = true)
        {
            NetworkOutput result = null;
            if (!OutputBounds.IsInBounds(x_Position, y_Position) && checkBounds)
            {
                //INFO - Wyjście nie jest w granicy kontrolki
            }
            else
            {
                if (this.Parent is SchemeCanvas schemeCanvas)
                {
                    result = new NetworkOutput(this);
                    result.SetPosition(x_Position - result.Width / 2.0, y_Position - result.Height / 2.0);

                    OutputNodes.Add(result);
                    schemeCanvas.AddSchemeElement(result, false, this.Name + "_X" + OutputNodes.Count());

                }
            }
            return result;
        }

        public override double ElementStrokeThickness { get; protected set; } = Settings.Coal_ElementStrokeThickness;
        public override Brush ElementStroke { get; protected set; } = Settings.Coal_ElementStroke;

        public NetworkControlBase()
        { 
        }
    }
}
