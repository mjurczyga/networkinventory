﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.NetworkControls
{
    [EditorType(SchemeTypes.NetworkScheme, true, "Karta etherCAT")]
    public sealed class EtherCATCardControl : EthernetControl
    {
        #region control properties
        public Brush InternalPortBackgroundBrush { get; private set; }
        #endregion

        static EtherCATCardControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EtherCATCardControl), new FrameworkPropertyMetadata(typeof(EtherCATCardControl)));
        }

        protected override void UpdateBackgroundBrush(int value)
        {
            if (value == 0)
            {
                InternalBackgroundBrush = Brushes.Red;
                InternalPortBackgroundBrush = Brushes.IndianRed;
            }
            else if (value != 0)
            {
                InternalPortBackgroundBrush = Brushes.LawnGreen;
                InternalBackgroundBrush = Brushes.Green;
            }
        }
    }
}
