﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Controls.NetworkControls
{
    [EditorType(SchemeTypes.NetworkScheme, true, "Switch")]
    public sealed class SwitchControl : EthernetControl
    {
        [EditorProperty("Liczba portów", "Ustawienia sieci")]
        public int SwitchPortsNumber
        {
            get { return (int)GetValue(SwitchPortsNumberProperty); }
            set { SetValue(SwitchPortsNumberProperty, value); }
        }

        public static readonly DependencyProperty SwitchPortsNumberProperty =
            DependencyProperty.Register("SwitchPortsNumber", typeof(int), typeof(SwitchControl), new PropertyMetadata(0, SwitchPortsNumber_PropertyChanged));

        private static void SwitchPortsNumber_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SwitchControl switchControl)
            {
                if (int.TryParse(e.NewValue?.ToString(), out int intValue))
                    switchControl.SetPortVisibility(intValue);
                else
                    switchControl.SetPortVisibility(0);
            }
        }

        private Visibility _Port8Visibility = Visibility.Collapsed;
        public Visibility Port8Visibility
        {
            get { return _Port8Visibility; }
            set { _Port8Visibility = value; NotifyPropertyChanged("Port8Visibility"); }
        }

        private Visibility _Port16Visibility = Visibility.Collapsed;
        public Visibility Port16Visibility
        {
            get { return _Port16Visibility; }
            set { _Port16Visibility = value; NotifyPropertyChanged("Port16Visibility"); }
        }

        private Visibility _Port24Visibility = Visibility.Collapsed;
        public Visibility Port24Visibility
        {
            get { return _Port24Visibility; }
            set { _Port24Visibility = value; NotifyPropertyChanged("Port24Visibility"); }
        }

        private Visibility _Port32Visibility = Visibility.Collapsed;
        public Visibility Port32Visibility
        {
            get { return _Port32Visibility; }
            set { _Port32Visibility = value; NotifyPropertyChanged("Port32Visibility"); }
        }

        private void SetPortVisibility(int numberOfPots)
        {
            if (numberOfPots > 0 && numberOfPots <= 8)
            {
                Port8Visibility = Visibility.Visible;
                Port16Visibility = Visibility.Collapsed;
                Port24Visibility = Visibility.Collapsed;
                Port32Visibility = Visibility.Collapsed;
            }
            else if (numberOfPots > 8 && numberOfPots <= 16)
            {
                Port8Visibility = Visibility.Visible;
                Port16Visibility = Visibility.Visible;
                Port24Visibility = Visibility.Collapsed;
                Port32Visibility = Visibility.Collapsed;
            }
            else if (numberOfPots > 16 && numberOfPots <= 24)
            {
                Port8Visibility = Visibility.Visible;
                Port16Visibility = Visibility.Visible;
                Port24Visibility = Visibility.Visible;
                Port32Visibility = Visibility.Collapsed;
            }
            else if (numberOfPots > 24 && numberOfPots <= 32)
            {
                Port8Visibility = Visibility.Visible;
                Port16Visibility = Visibility.Visible;
                Port24Visibility = Visibility.Visible;
                Port32Visibility = Visibility.Visible;
            }
            else
            {
                Port8Visibility = Visibility.Collapsed;
                Port16Visibility = Visibility.Collapsed;
                Port24Visibility = Visibility.Collapsed;
                Port32Visibility = Visibility.Collapsed;
            }
        }



        static SwitchControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SwitchControl), new FrameworkPropertyMetadata(typeof(SwitchControl)));
        }
    }
}
