﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.NetworkControls.Models;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.NetworkControls
{
    [EditorType(SchemeTypes.NetworkScheme, true, "Moduł etherCAT")]
    public sealed class EtherCATModule : NetworkControlBase
    {
        #region Port
        private Visibility _RS485PortVisibility = Visibility.Collapsed;
        public Visibility RS485PortVisibility
        {
            get { return _RS485PortVisibility; }
            set { _RS485PortVisibility = value; NotifyPropertyChanged("RS485PortVisibility"); }
        }

        private Visibility _EthernetPortVisibility = Visibility.Collapsed;
        public Visibility EthernetPortVisibility
        {
            get { return _EthernetPortVisibility; }
            set { _EthernetPortVisibility = value; NotifyPropertyChanged("EthernetPortVisibility"); }
        }

        private Brush _InternalPortBackgroundBrush;
        public Brush InternalPortBackgroundBrush
        {
            get { return _InternalPortBackgroundBrush; }
            set { _InternalPortBackgroundBrush = value; ; NotifyPropertyChanged("InternalPortBackgroundBrush"); }
        }
        #endregion port  

        [EditorProperty("Kod błędu modułu", "Ustawienia sieci", "Aby zbindować zmienną należy jej nazwę umieścić w nawiasie kwadratowym - [ZmmiennaDoZbindowania]")]
        public object ErrorCode
        {
            get { return (object)GetValue(ErrorCodeProperty); }
            set { SetValue(ErrorCodeProperty, value); }
        }

        public static readonly DependencyProperty ErrorCodeProperty =
            DependencyProperty.Register("ErrorCode", typeof(object), typeof(EtherCATModule), new PropertyMetadata(ErrorCode_PropertyChanged));

        private static void ErrorCode_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is EtherCATModule etherCATModule)
            {
                if (int.TryParse(e.NewValue?.ToString(), out int intValue))
                {
                    etherCATModule.InternalErrorCode = intValue;
                    etherCATModule.ErrorDescription = EtherCATCodes.GetCodeDescription(intValue);
                }
            }
        }

        private int _InternalErrorCode = -1;
        public int InternalErrorCode
        {
            get { return _InternalErrorCode; }
            set
            {
                _InternalErrorCode = value;
                SetIconVisibility(value);
                NotifyPropertyChanged("InternalErrorCode");
            }
        }

        private void SetIconVisibility(int errorCode)
        {
            if (errorCode == 8)
            {
                OkIconVisibility = Visibility.Visible;
                ErrorIconVisibility = Visibility.Collapsed;
            }
            else
            {
                OkIconVisibility = Visibility.Collapsed;
                ErrorIconVisibility = Visibility.Visible;
            }
        }

        private Visibility _OkIconVisibility = Visibility.Collapsed;
        public Visibility OkIconVisibility
        {
            get { return _OkIconVisibility; }
            set { _OkIconVisibility = value; NotifyPropertyChanged("OkIconVisibility"); }
        }

        private Visibility _ErrorIconVisibility = Visibility.Collapsed;
        public Visibility ErrorIconVisibility
        {
            get { return _ErrorIconVisibility; }
            set { _ErrorIconVisibility = value; NotifyPropertyChanged("ErrorIconVisibility"); }
        }

        private string _ErrorDescription = "Błąd połączenia";
        public string ErrorDescription
        {
            get { return _ErrorDescription; }
            set { _ErrorDescription = value; NotifyPropertyChanged("ErrorDescription"); }
        }


        static EtherCATModule()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EtherCATModule), new FrameworkPropertyMetadata(typeof(EtherCATModule)));
        }

        public EtherCATModule()
        {
            DependencyPropertyDescriptor.FromProperty(DeviceNameProperty, typeof(EtherCATModule)).AddValueChanged(this, DeviceName_PropertyChanged);
        }

        private void DeviceName_PropertyChanged(object sender, EventArgs e)
        {
            if (sender is EtherCATModule etherCATModule)
            {
                if (etherCATModule.DeviceName.Contains("RS") && etherCATModule.DeviceName.Contains("485"))
                    this.RS485PortVisibility = Visibility.Visible;
                else
                    this.RS485PortVisibility = Visibility.Collapsed;
            }
        }

        protected override void UpdateBackgroundBrush(int value)
        {
            if (value == 0)
            {
                InternalBackgroundBrush = Brushes.Red;
                InternalPortBackgroundBrush = Brushes.IndianRed;
            }
            else if (value != 0)
            {
                InternalPortBackgroundBrush = Brushes.LawnGreen;
                InternalBackgroundBrush = Brushes.Green;
            }
        }
    }
}
