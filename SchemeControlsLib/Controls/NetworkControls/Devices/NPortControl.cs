﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.NetworkControls
{
    [EditorType(SchemeTypes.NetworkScheme, true, "N-Port")]
    public sealed class NPortControl : EthernetControl
    {
        #region control properties
        //public Brush InternalPortBackgroundBrush { get; private set; }
        #endregion

        static NPortControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NPortControl), new FrameworkPropertyMetadata(typeof(NPortControl)));
        }
    }
}
