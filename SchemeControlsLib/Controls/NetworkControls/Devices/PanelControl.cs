﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Controls.NetworkControls
{
    [EditorType(SchemeTypes.NetworkScheme, true, "Panel")]
    public sealed class PanelControl : EthernetControl
    {
        static PanelControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PanelControl), new FrameworkPropertyMetadata(typeof(PanelControl)));
        }

    }
}
