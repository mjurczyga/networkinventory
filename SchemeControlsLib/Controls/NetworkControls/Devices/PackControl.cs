﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Controls.NetworkControls
{
    [EditorType(SchemeTypes.NetworkScheme, true, "PAC-3200")]
    public sealed class PackControl : NetworkControlBase
    {
        static PackControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PackControl), new FrameworkPropertyMetadata(typeof(PackControl)));
        }
    }
}
