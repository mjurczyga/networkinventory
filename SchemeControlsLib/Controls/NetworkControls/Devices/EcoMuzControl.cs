﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.NetworkControls.Types;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.NetworkControls
{
    [EditorType(SchemeTypes.NetworkScheme, true, "EcoMUZ-2")]
    public sealed class EcoMuzControl : NetworkControlBase
    {
        #region control properties
        public Brush InternalPortBackgroundBrush { get; private set; }
        #endregion

        static EcoMuzControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EcoMuzControl), new FrameworkPropertyMetadata(typeof(EcoMuzControl)));
        }

        public EcoMuzControl()
        {
            NetworkType = NetworkTypes.RS485;
        }

        protected override void UpdateBackgroundBrush(int value)
        {
            if (value == 0)
            {
                InternalBackgroundBrush = Brushes.Red;
                InternalPortBackgroundBrush = Brushes.IndianRed;
            }
            else if (value != 0)
            {
                InternalPortBackgroundBrush = Brushes.LawnGreen;
                InternalBackgroundBrush = Brushes.Green;
            }
        }
    }
}
