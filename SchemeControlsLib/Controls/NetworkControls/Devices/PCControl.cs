﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Controls.NetworkControls
{
    [EditorType(SchemeTypes.NetworkScheme, true, "Komputer PC")]
    public sealed class PCControl : EthernetControl
    {
        static PCControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PCControl), new FrameworkPropertyMetadata(typeof(PCControl)));
        }
    }
}
