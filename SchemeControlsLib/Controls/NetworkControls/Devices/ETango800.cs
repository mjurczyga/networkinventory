﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.NetworkControls._Core;
using SchemeControlsLib.Controls.NetworkControls.Types;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.NetworkControls
{
    [EditorType(SchemeTypes.NetworkScheme, true, "e2Tango-800")]
    public class ETango800 : BaseTangoControl
    {

        static ETango800()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ETango800), new FrameworkPropertyMetadata(typeof(ETango800)));
        }

        public ETango800()
        {
            NetworkType = NetworkTypes.RS485;

        }

    }
}
