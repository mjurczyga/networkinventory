﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Controls.NetworkControls
{
    [EditorType(SchemeTypes.NetworkScheme, true, "Sterownik PLC")]
    public sealed class PLCControl : EthernetControl
    {
        static PLCControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PLCControl), new FrameworkPropertyMetadata(typeof(PLCControl)));
        }
    }
}
