﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.NetworkControls.Types;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.NetworkControls
{
    [EditorType(SchemeTypes.NetworkScheme, true, "Wyspa etherCAT")]
    public sealed class EtherCATField : NetworkControlBase
    {
        private readonly double _defaultWidth = 50;

        private StackPanel _ContentStackPanel;
        public StackPanel ContentStackPanel
        {
            get { return _ContentStackPanel; }
            set { _ContentStackPanel = value; NotifyPropertyChanged("ContentStackPanel"); }
        }

        #region DI
        [EditorProperty("Karta wejść cyfrowych?", "Ustawienia sieci")]
        public bool HasDI
        {
            get { return (bool)GetValue(HasDIProperty); }
            set { SetValue(HasDIProperty, value); }
        }

        public static readonly DependencyProperty HasDIProperty =
            DependencyProperty.Register("HasDI", typeof(bool), typeof(EtherCATField), new PropertyMetadata(HasDI_PropertyChanged));

        private static void HasDI_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is EtherCATField etherCATField)
                etherCATField.UpdateContent();
        }
        [EditorProperty("Struktura modułu DI", "Ustawienia sieci", "Aby zbindować zmienną należy jej nazwę umieścić w nawiasie kwadratowym - [ZmmiennaDoZbindowania]")]
        public object DIVarStruct
        {
            get { return (object)GetValue(DIVarStructProperty); }
            set { SetValue(DIVarStructProperty, value); }
        }

        public static readonly DependencyProperty DIVarStructProperty =
            DependencyProperty.Register("DIVarStruct", typeof(object), typeof(EtherCATField));
        #endregion

        #region AQ
        [EditorProperty("Karta wyjść analogowych?", "Ustawienia sieci")]
        public bool HasAQ
        {
            get { return (bool)GetValue(HasAQProperty); }
            set { SetValue(HasAQProperty, value); }
        }

        public static readonly DependencyProperty HasAQProperty =
            DependencyProperty.Register("HasAQ", typeof(bool), typeof(EtherCATField), new PropertyMetadata(HasAQ_PropertyChanged));

        private static void HasAQ_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is EtherCATField etherCATField)
                etherCATField.UpdateContent();
        }
        [EditorProperty("Struktura modułu AQ", "Ustawienia sieci", "Aby zbindować zmienną należy jej nazwę umieścić w nawiasie kwadratowym - [ZmmiennaDoZbindowania]")]
        public object AQVarStruct
        {
            get { return (object)GetValue(AQVarStructProperty); }
            set { SetValue(AQVarStructProperty, value); }
        }

        public static readonly DependencyProperty AQVarStructProperty =
            DependencyProperty.Register("AQVarStruct", typeof(object), typeof(EtherCATField));

        #endregion

        #region RS
        [EditorProperty("Karta RS-485?", "Ustawienia sieci")]
        public bool HasRS485
        {
            get { return (bool)GetValue(HasRS485Property); }
            set { SetValue(HasRS485Property, value); }
        }
        public static readonly DependencyProperty HasRS485Property =
            DependencyProperty.Register("HasRS485", typeof(bool), typeof(EtherCATField), new PropertyMetadata(HasRS485_PropertyChanged));

        private static void HasRS485_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is EtherCATField etherCATField)
                etherCATField.UpdateContent();
        }
        [EditorProperty("Struktura modułu RS-485", "Ustawienia sieci", "Aby zbindować zmienną należy jej nazwę umieścić w nawiasie kwadratowym - [ZmmiennaDoZbindowania]")]
        public object RS485VarStruct
        {
            get { return (object)GetValue(RS485VarStructProperty); }
            set { SetValue(RS485VarStructProperty, value); }
        }

        public static readonly DependencyProperty RS485VarStructProperty =
            DependencyProperty.Register("RS485VarStruct", typeof(object), typeof(EtherCATField));
        #endregion

        #region Ethernet
        [EditorProperty("Karta etherCAT?", "Ustawienia sieci")]
        public bool HasEthernet
        {
            get { return (bool)GetValue(HasEthernetProperty); }
            set { SetValue(HasEthernetProperty, value); }
        }

        public static readonly DependencyProperty HasEthernetProperty =
            DependencyProperty.Register("HasEthernet", typeof(bool), typeof(EtherCATField), new PropertyMetadata(HasEthernetProperty_PropertyChanged));

        private static void HasEthernetProperty_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is EtherCATField etherCATField)
                etherCATField.UpdateContent();
        }

        [EditorProperty("Struktura modułu RS-485", "Ustawienia sieci", "Aby zbindować zmienną należy jej nazwę umieścić w nawiasie kwadratowym - [ZmmiennaDoZbindowania]")]
        public object EthernetVarStruct
        {
            get { return (object)GetValue(EthernetVarStructProperty); }
            set { SetValue(EthernetVarStructProperty, value); }
        }

        public static readonly DependencyProperty EthernetVarStructProperty =
            DependencyProperty.Register("EthernetVarStruct", typeof(object), typeof(EtherCATField));

        #endregion

        #region Wyspa
        [EditorProperty("Wyspa?", "Ustawienia sieci")]
        public bool HasWyspa
        {
            get { return (bool)GetValue(HasWyspaProperty); }
            set { SetValue(HasWyspaProperty, value); }
        }

        public static readonly DependencyProperty HasWyspaProperty =
            DependencyProperty.Register("HasWyspa", typeof(bool), typeof(EtherCATField), new PropertyMetadata(true, HasWyspa_PropertyChanged));

        private static void HasWyspa_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is EtherCATField etherCATField)
                etherCATField.UpdateContent();
        }

        [EditorProperty("Struktura wyspy", "Ustawienia sieci", "Aby zbindować zmienną należy jej nazwę umieścić w nawiasie kwadratowym - [ZmmiennaDoZbindowania]")]
        public object WyspaVarsStruct
        {
            get { return (object)GetValue(WyspaVarsStructProperty); }
            set { SetValue(WyspaVarsStructProperty, value); }
        }

        public static readonly DependencyProperty WyspaVarsStructProperty =
            DependencyProperty.Register("WyspaVarsStruct", typeof(object), typeof(EtherCATField));
        #endregion

        static EtherCATField()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EtherCATField), new FrameworkPropertyMetadata(typeof(EtherCATField)));
        }

        public EtherCATField()
        {
            this.Width = _defaultWidth;
        }

        private void UpdateContent()
        {
            StackPanel stackPanel = new StackPanel() { Orientation = Orientation.Horizontal };

            var width = _defaultWidth;

            if (HasWyspa)
            {
                var wControl = new EtherCATModule() { DeviceName = "Wyspa", Width = 50, Height = 100, BorderBrush = Brushes.Black };
                wControl.SetBinding(EtherCATModule.DeviceStateProperty, new Binding("WyspaVarsStruct.BLAD.Value") { Source = this, Mode = BindingMode.OneWay });
                wControl.SetBinding(EtherCATModule.ErrorCodeProperty, new Binding("WyspaVarsStruct.STAN.Value") { Source = this, Mode = BindingMode.OneWay });
                stackPanel.Children.Add(wControl);
                width += wControl.Width;
            }
            if (HasAQ)
            {
                var aqControl = new EtherCATModule() { DeviceName = "AQ", Width = 50, Height = 100, BorderBrush = Brushes.Black };
                aqControl.SetBinding(EtherCATModule.DeviceStateProperty, new Binding("AQVarStruct.BLAD.Value") { Source = this, Mode = BindingMode.OneWay });
                aqControl.SetBinding(EtherCATModule.ErrorCodeProperty, new Binding("AQVarStruct.STAN.Value") { Source = this, Mode = BindingMode.OneWay });
                stackPanel.Children.Add(aqControl);
                width += aqControl.Width;
            }
            if (HasDI)
            {
                var diControl = new EtherCATModule() { DeviceName = "DI", Width = 50, Height = 100, BorderBrush = Brushes.Black };
                diControl.SetBinding(EtherCATModule.DeviceStateProperty, new Binding("DIVarStruct.BLAD.Value") { Source = this, Mode = BindingMode.OneWay });
                diControl.SetBinding(EtherCATModule.ErrorCodeProperty, new Binding("DIVarStruct.STAN.Value") { Source = this, Mode = BindingMode.OneWay });
                stackPanel.Children.Add(diControl);
                width += diControl.Width;
            }
            if (HasRS485)
            {
                var rsControl = new EtherCATModule() { DeviceName = "RS485", Width = 50, Height = 100, BorderBrush = Brushes.Black, NetworkType = NetworkTypes.RS485 };
                rsControl.SetBinding(EtherCATModule.DeviceStateProperty, new Binding("RS485VarStruct.BLAD.Value") { Source = this, Mode = BindingMode.OneWay });
                rsControl.SetBinding(EtherCATModule.ErrorCodeProperty, new Binding("RS485VarStruct.STAN.Value") { Source = this, Mode = BindingMode.OneWay });
                stackPanel.Children.Add(rsControl);
                width += rsControl.Width;
            }
            if (HasEthernet)
            {
                var eControl = new EtherCATModule() { DeviceName = "Ethernet", Width = 50, Height = 100, BorderBrush = Brushes.Black, NetworkType = NetworkTypes.Ethernet };
                eControl.SetBinding(EtherCATModule.DeviceStateProperty, new Binding("EthernetVarStruct.BLAD.Value") { Source = this, Mode = BindingMode.OneWay });
                eControl.SetBinding(EtherCATModule.ErrorCodeProperty, new Binding("EthernetVarStruct.STAN.Value") { Source = this, Mode = BindingMode.OneWay });
                stackPanel.Children.Add(eControl);
                width += eControl.Width;
            }

            this.Width = width;
            ContentStackPanel = stackPanel;
        }

    }
}
