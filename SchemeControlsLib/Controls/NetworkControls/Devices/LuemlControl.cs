﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Controls.NetworkControls
{
    [EditorType(SchemeTypes.NetworkScheme, true, "Lumel")]
    public sealed class LumelControl : NetworkControlBase
    {
        static LumelControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(LumelControl), new FrameworkPropertyMetadata(typeof(LumelControl)));
        }
    }
}
