﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.EnumTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Controls.NetworkControls
{
    [EditorType(SchemeTypes.NetworkScheme, true, "Routerboard")]
    public sealed class RouterboardControl : EthernetControl
    {
        static RouterboardControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(RouterboardControl), new FrameworkPropertyMetadata(typeof(RouterboardControl)));
        }
    }
}
