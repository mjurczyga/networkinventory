﻿using SchemeControlsLib.Controls.NetworkControls.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.NetworkControls
{
    public sealed class NetworkTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                if (context != null && context.Instance != null)
                {
                    if (!(context.Instance is NetworkType))
                    {
                        throw new ArgumentException("General_Expected_Type  NEtworkType", "context");
                    }
                    NetworkType networkType = (NetworkType)context.Instance;
                    return networkType.CanSerializeToString();
                }
                return true;
            }
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value == null)
            {
                throw GetConvertFromException(value);
            }
            string text = value as string;
            if (text != null)
            {
                return NetworkType.Parse(text, context);
            }
            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType != null && value is NetworkType)
            {
                NetworkType networkType = (NetworkType)value;
                if (destinationType == typeof(string))
                {
                    if (context != null && context.Instance != null && !networkType.CanSerializeToString())
                    {
                        throw new NotSupportedException("Converter_ConvertToNotSupported");
                    }
                    return networkType.ConvertToString(null, culture);
                }
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        public NetworkTypeConverter()
        {
        }

    }
}
