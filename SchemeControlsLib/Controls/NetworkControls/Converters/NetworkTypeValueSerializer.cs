﻿using SchemeControlsLib.Controls.NetworkControls.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace SchemeControlsLib.Controls.NetworkControls.ValueSerializers
{
    public class NetworkTypeValueSerializer : ValueSerializer
    {
        public override bool CanConvertFromString(string value, IValueSerializerContext context)
        {
            return NetworkTypes.GetElementFromString(value) != null ? true : false;
        }

        public override bool CanConvertToString(object value, IValueSerializerContext context)
        {
            return true;
        }

        public override object ConvertFromString(string value, IValueSerializerContext context)
        {
            return NetworkTypes.GetElementFromString(value);
        }

        public override string ConvertToString(object value, IValueSerializerContext context)
        {
            var networkType = NetworkTypes.GetList().Where(x => x == value).FirstOrDefault();

            return networkType.Name;
        }

        public NetworkTypeValueSerializer()
        {
        }
    }
}
