﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Core.Interfaces;
using SchemeControlsLib.Controls.NetworkControls.SNMP.Bases.Interfaces;
using SchemeControlsLib.Controls.NetworkControls.SNMP.Dictionaries;
using SchemeControlsLib.Controls.NetworkControls.SNMP.Models;
using SchemeControlsLib.EnumTypes;
using SnmpSharpNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.NetworkControls.SNMP.Devices
{
    [EditorType(SchemeTypes.NetworkScheme, false, "Port kontrolki SNMP")]
    public class EthernetPortControl : SNMPControl, ISNMPChildrenControl
    {
        private ISNMPControl _ParentControl;
        public ISNMPControl ParentControl
        {
            get { return _ParentControl; }
            set { _ParentControl = value; }
        }


        [EditorProperty("Odwrócona grafika", "Grafika")]
        public bool FlipGraphic
        {
            get { return (bool)GetValue(FlipGraphicProperty); }
            set { SetValue(FlipGraphicProperty, value); }
        }

        #region FlipGraphic
        public static readonly DependencyProperty FlipGraphicProperty =
            DependencyProperty.Register("FlipGraphic", typeof(bool), typeof(EthernetPortControl), new PropertyMetadata(FlipGraphic_PropertyChanged));

        private static void FlipGraphic_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is EthernetPortControl ethernetPortControl)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool bValue))
                    ethernetPortControl.UpdateVisibility(bValue);
            }
        }

        private void UpdateVisibility(bool flippedGraphic)
        {
            if(flippedGraphic)
            {
                NormalGraphicVisibility = Visibility.Collapsed;
                FlippedGraphicVisibility = Visibility.Visible;
            }
            else
            {
                NormalGraphicVisibility = Visibility.Visible;
                FlippedGraphicVisibility = Visibility.Collapsed;
            }
           
        }

        private Visibility _NormalGraphicVisibility = Visibility.Visible;
        public Visibility NormalGraphicVisibility
        {
            get { return _NormalGraphicVisibility; }
            set { _NormalGraphicVisibility = value; NotifyPropertyChanged("NormalGraphicVisibility"); }
        }

        private Visibility _FlippedGraphicVisibility = Visibility.Collapsed;
        public Visibility FlippedGraphicVisibility
        {
            get { return _FlippedGraphicVisibility; }
            set { _FlippedGraphicVisibility = value; NotifyPropertyChanged("FlippedGraphicVisibility"); }
        }

        #endregion FlipGraphic

        public int PortNumber
        {
            get { return (int)GetValue(PortNumberProperty); }
            set { SetValue(PortNumberProperty, value); }
        }

        public static readonly DependencyProperty PortNumberProperty =
            DependencyProperty.Register("PortNumber", typeof(int), typeof(EthernetPortControl), new PropertyMetadata(0));



        public int PortState
        {
            get { return (int)GetValue(PortStateProperty); }
            set { SetValue(PortStateProperty, value); }
        }

        public static readonly DependencyProperty PortStateProperty =
            DependencyProperty.Register("PortState", typeof(int), typeof(EthernetPortControl), new PropertyMetadata(0, PortState_PropertyChanged));

        private static void PortState_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is EthernetPortControl ethernetPortControl)
            {
                if(int.TryParse(e.NewValue?.ToString(), out int iNewValue))
                {
                    if (iNewValue == 1)
                        ethernetPortControl.PortBrush = Settings.StatusPortOkBrush;
                    else if (iNewValue == 2)
                        ethernetPortControl.PortBrush = Settings.StatusPortOffBrush;
                    else if (iNewValue == 3)
                        ethernetPortControl.PortBrush = Settings.StatusPortTestingBrush;
                    else
                        ethernetPortControl.PortBrush = Settings.StatusPortInvalidBrush;
                }
            }
        }

        private Brush _PortBrush;
        public Brush PortBrush
        {
            get { return _PortBrush; }
            set { _PortBrush = value; NotifyPropertyChanged("PortBrush"); }
        }

        public override void UpdateControl()
        {
        }

        public override void InitControl(IPAddress iPAddress)
        {
            
        }

        static EthernetPortControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EthernetPortControl), new FrameworkPropertyMetadata(typeof(EthernetPortControl)));
        }
    }
}
