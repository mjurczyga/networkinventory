﻿using SchemeControlsLib.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Shapes;

namespace SchemeControlsLib.Controls.NetworkControls.SNMP.Devices.DeviceBuilder
{
    public static class MikroTikControl
    {
        public static FrameworkElement DrawControl(FrameworkElement bindingSource, int numberOfPorts, string deviceDescription, string deviceName, out List<EthernetPortControl> addedPorts)
        {
            var width = 0.0;
            var height = 0.0;
            var mainCanvas = new Canvas();

            Rectangle mainBackground = new Rectangle() { StrokeThickness = 3 };
            mainBackground.SetBinding(Rectangle.FillProperty, GetBinding("InternalBackgroundBrush", bindingSource));
            mainBackground.SetBinding(Rectangle.StrokeProperty, GetBinding("BorderBrush", bindingSource));
            mainCanvas.Children.Add(mainBackground);


            if (numberOfPorts > 0)
            {
                var resultGrid = BuildPortsControl(mainCanvas, numberOfPorts, out List<EthernetPortControl> ports);
                addedPorts = ports;
                Canvas.SetLeft(resultGrid, 45);
                Canvas.SetTop(resultGrid, 25);
                height += resultGrid.Height + 25 + 10; //25 - góra, 10 dół;
                width += resultGrid.Width + 30;
            }
            else
                addedPorts = new List<EthernetPortControl>();

            //Nazwa urządzenia
            var deviceNameSize = deviceName.ComputeTextSize(15);
            TextBlock deviceNameTextBlock = new TextBlock() { Text = deviceName, FontSize = 15, FontWeight = FontWeights.Bold };
            deviceNameTextBlock.SetBinding(TextBlock.ForegroundProperty, GetBinding("Foreground", bindingSource));
            Canvas.SetLeft(deviceNameTextBlock, 15 + 20);
            Canvas.SetTop(deviceNameTextBlock, 2);
            mainCanvas.Children.Add(deviceNameTextBlock);

            var deviceDescriptionSize = deviceDescription.ComputeTextSize(12);
            TextBlock deviceDescriptionTextBlock = new TextBlock() { Text = deviceDescription, FontSize = 12, FontWeight = FontWeights.Medium };
            deviceDescriptionTextBlock.SetBinding(TextBlock.ForegroundProperty, GetBinding("Foreground", bindingSource));
            Canvas.SetRight(deviceDescriptionTextBlock, 20);
            Canvas.SetTop(deviceDescriptionTextBlock, 8);
            mainCanvas.Children.Add(deviceDescriptionTextBlock);

            Line leftLine = new Line() { StrokeThickness = 2, X1 = 0.0, Y1 = 0.0, X2 = 0.0, Y2 = height };
            leftLine.SetBinding(Line.StrokeProperty, GetBinding("BorderBrush", bindingSource));
            Canvas.SetLeft(leftLine, 15.0);
            width += 15;
            mainCanvas.Children.Add(leftLine);

            Line rightLine = new Line() { StrokeThickness = 2, X1 = 0.0, Y1 = 0.0, X2 = 0.0, Y2 = height };
            rightLine.SetBinding(Line.StrokeProperty, GetBinding("BorderBrush", bindingSource));
            Canvas.SetRight(rightLine, 15.0);
            width += 165;
            mainCanvas.Children.Add(rightLine);

            mainBackground.Width = width;
            mainBackground.Height = height;
            mainCanvas.Width = width;
            mainCanvas.Height = height;


            bindingSource.Width = width;
            bindingSource.Height = height;


            return mainCanvas;
        }

        private static Grid BuildPortsControl(Canvas canvas, int numberOfPorts, out List<EthernetPortControl> SwitchPorts)
        {
            SwitchPorts = new List<EthernetPortControl>();
            Grid portsGrid = new Grid();
            canvas.Children.Add(portsGrid);
            RowDefinition rowDefinition = new RowDefinition() { Height = GridLength.Auto };
            RowDefinition rowDefinition1 = new RowDefinition() { Height = GridLength.Auto };
            portsGrid.RowDefinitions.Add(rowDefinition);
            portsGrid.RowDefinitions.Add(rowDefinition1);

            if (numberOfPorts >= 1)
            {
                int numbersOfColumn = numberOfPorts / 8 * 4;
                int restOfPorts = numberOfPorts - (numbersOfColumn * 2);
                int tempNumberOfSpaces = (numbersOfColumn / 4) + (restOfPorts > 0 ? 0 : -1); // jeśli ma jakieś porty dodatkowe to zostaw przerwę
                int numberOfSpaces = tempNumberOfSpaces < 0 ? 0 : tempNumberOfSpaces;

                //Define a columns with "spaces" for ports
                for (int i = 0; i < numbersOfColumn + numberOfSpaces + restOfPorts; i++)
                {
                    ColumnDefinition columnDefinition = new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) };
                    portsGrid.ColumnDefinitions.Add(columnDefinition);
                }

                int column = 0, row = 1;
                int addedPorts = 0;
                bool reversePort = true;

                for (int i = 1; i <= numberOfPorts - restOfPorts; i++)
                {
                    int numberOfPort = i;

                    SwitchPorts.Add(GetEthernetPortControl(numberOfPort, reversePort, column, row));
                    addedPorts++;

                    if (addedPorts % 2 == 0)
                    {
                        row = 1;
                        reversePort = true;
                        column++;
                    }
                    else
                    {
                        row = 0;
                        reversePort = false;
                    }

                    //odstęp pomiędzy portami (paczki po 8 portów)
                    if (addedPorts % 8 == 0)
                    {
                        column++;
                    }
                }

                for (int i = addedPorts + 1; i < addedPorts + 1 + restOfPorts; i++)
                {
                    SwitchPorts.Add(GetEthernetPortControl(i, true, column, 1));
                    column++;
                }

                portsGrid.Width = (numbersOfColumn + numberOfSpaces + restOfPorts) * 19;
                portsGrid.Height = 18 * 2;

            }
            else
            {

            }


            foreach (var port in SwitchPorts)
            {
                if (port is FrameworkElement frameworkElement)
                    portsGrid.Children.Add(frameworkElement);
            }

            return portsGrid;
        }

        private static Grid DrawFourPortSwitch(Canvas canvas, int numberOfPorts, out List<EthernetPortControl> SwitchPorts)
        {
            SwitchPorts = new List<EthernetPortControl>();

            Grid portsGrid = new Grid();
            Canvas.SetLeft(portsGrid, 20);
            Canvas.SetTop(portsGrid, 17);

            canvas.Children.Add(portsGrid);
            RowDefinition rowDefinition = new RowDefinition() { Height = GridLength.Auto };
            portsGrid.RowDefinitions.Add(rowDefinition);

            ColumnDefinition columnDefinition = new ColumnDefinition() { Width = GridLength.Auto };
            ColumnDefinition columnDefinition1 = new ColumnDefinition() { Width = GridLength.Auto };
            ColumnDefinition columnDefinition2 = new ColumnDefinition() { Width = GridLength.Auto };
            ColumnDefinition columnDefinition3 = new ColumnDefinition() { Width = GridLength.Auto };
            portsGrid.ColumnDefinitions.Add(columnDefinition);
            portsGrid.ColumnDefinitions.Add(columnDefinition1);
            portsGrid.ColumnDefinitions.Add(columnDefinition2);
            portsGrid.ColumnDefinitions.Add(columnDefinition3);

            SwitchPorts.Add(GetEthernetPortControl(1, false, 0, 0));
            SwitchPorts.Add(GetEthernetPortControl(2, false, 1, 0));
            SwitchPorts.Add(GetEthernetPortControl(3, false, 2, 0));
            SwitchPorts.Add(GetEthernetPortControl(4, false, 3, 0));

            foreach (var port in SwitchPorts)
            {
                if (port is UIElement uIElement)
                    portsGrid.Children.Add(uIElement);
            }

            portsGrid.Width += 4 * 19;
            portsGrid.Height += 18;

            return portsGrid;
        }

        private static EthernetPortControl GetEthernetPortControl(int portNumber, bool flipGraphic, int column, int row)
        {
            var port = new EthernetPortControl() { PortNumber = portNumber, FlipGraphic = flipGraphic };
            Grid.SetColumn(port, column);
            Grid.SetRow(port, row);
            return port;
        }

        private static Binding GetBinding(string path, FrameworkElement bindingSource)
        {
            Binding binding = new Binding(path);
            binding.Source = bindingSource;
            binding.Mode = BindingMode.OneWay;

            return binding;
        }

    }
}
