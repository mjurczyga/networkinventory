﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;
using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.NetworkControls._Core.Helpers;
using SchemeControlsLib.Controls.NetworkControls.SNMP.Devices.DeviceBuilder;
using SchemeControlsLib.Controls.NetworkControls.SNMP.Dictionaries;
using SchemeControlsLib.Controls.NetworkControls.SNMP.Models;
using SchemeControlsLib.Controls.NetworkControls.SNMP.Bases;
using SchemeControlsLib.EnumTypes;
using SchemeControlsLib.Helpers;
using SnmpSharpNet;

namespace SchemeControlsLib.Controls.NetworkControls.SNMP.Devices
{
    [EditorType(SchemeTypes.NetworkScheme, true, "SNMP Switch")]
    public class SNMPDeviceControl : SNMPControl
    {
        private List<EthernetPortControl> _ControlChildrens = new List<EthernetPortControl>();

        public string DeviceDescription
        {
            get { return (string)GetValue(DeviceDescriptionProperty); }
            set { SetValue(DeviceDescriptionProperty, value); }
        }

        public static readonly DependencyProperty DeviceDescriptionProperty =
            DependencyProperty.Register("DeviceDescription", typeof(string), typeof(SNMPDeviceControl), new PropertyMetadata(null));


        public async void BuildControl()
        {
            if (IPHelpers.TryGetIPAdress(this.IP, out IPAddress iPAddress))
            {
                UdpTarget udpTarget = new UdpTarget(iPAddress, this.SNMPPort, 2000, 1);
                AgentParameters agentParams = new AgentParameters(SnmpVersion.Ver1, new OctetString("public"));
                Pdu pdu = new Pdu(PduType.Get);
                if (AllSwitches.TryGetOidModel("ifNumber", out OidModel oidModel))
                {
                    pdu.VbList.Add(oidModel.Oid);

                    var result = await this.InternalDoRequestAsync(udpTarget, agentParams, pdu);
                    if (result != null)
                    {
                        var ifNumber = result.Pdu.VbList.Where(x => x.Oid.ToString() == oidModel.Oid).FirstOrDefault();
                        if(int.TryParse(ifNumber?.ToString(), out int iIfNumber))
                        {

                        }
                    }
                }

                udpTarget.Close();

            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            if (_iPAddress == null)
            {
                var initControl = new Rectangle() { Width = 50, Height = 50, Fill = Brushes.Blue, Stroke = Brushes.Black, StrokeThickness = 3 };
                this.Width = 50;
                this.Height = 50;
                MainControl = initControl;
            }
        
        }

        private FrameworkElement _MainControl;
        public FrameworkElement MainControl
        {
            get { return _MainControl; }
            private set { _MainControl = value; NotifyPropertyChanged("MainControl"); }
        }

        private SnmpAsyncResponse _SnmpAsyncResponse;

        public async override void InitControl(IPAddress iPAddress)
        {
            UdpTarget udpTarget = new UdpTarget(iPAddress, SNMPPort, 2000, 1);
            AgentParameters agentParams = new AgentParameters(SnmpVersion.Ver1, new OctetString("public"));
            Pdu pdu = new Pdu(PduType.Get);

            AllSwitches.TryGetOidModel("sysDescr", out OidModel sysDescr);
            AllSwitches.TryGetOidModel("sysName", out OidModel sysName);
            AllSwitches.TryGetOidModel("ifNumber", out OidModel ifNumber);
            pdu.VbList.Add(sysDescr.Oid);
            pdu.VbList.Add(sysName.Oid);
            pdu.VbList.Add(ifNumber.Oid);
            try
            {
                SnmpV1Packet resultaa;
                AsyncRequestResult asyncRequestResult;

                resultaa = (SnmpV1Packet)await this.InternalDoRequestAsync(udpTarget, agentParams, pdu);//(SnmpV1Packet)udpTarget.Request(pdu, agentParams);
                //udpTarget.RequestAsync(pdu, agentParams, _SnmpAsyncResponse);

                if (resultaa != null)
                {
                    int numberOfPorts = 0;

                    if (int.TryParse(resultaa.Pdu.VbList.Where(x => x.Oid.ToString() == ifNumber.Oid).FirstOrDefault()?.Value.ToString(), out int iSysDescrValue))
                        numberOfPorts = iSysDescrValue;

                    string devDesc = resultaa.Pdu.VbList.Where(x => x.Oid.ToString() == sysDescr.Oid).FirstOrDefault()?.Value.ToString();
                    string devName = resultaa.Pdu.VbList.Where(x => x.Oid.ToString() == sysName.Oid).FirstOrDefault()?.Value.ToString();

                    _Dispatcher.Invoke(() =>
                    {
                        DrawTestControl(numberOfPorts, devDesc, devName);
                    });
                }
            }
            catch(Exception e)
            {
                MessageBox.Show($"Problem z inicjalizacją kontrolki o adresie IP - {iPAddress.ToString()}. Treść błędu: {e.Message}");
            }
            udpTarget.Close();

        }

        public void DrawTestControl(int numberOfPorts, string deviceDescription = "", string deviceName = "")
        {
            if (deviceName?.ToUpper() == "MIKROTIK" || true) 
                MainControl = MikroTikControl.DrawControl(this, numberOfPorts, deviceDescription, deviceName, out _ControlChildrens);
            
            this.initializeTimer();
        }      
        
        public async override void UpdateControl()
        {
            if (_iPAddress != null)
            {
                if (UsePing)
                    await PingTask();
                else
                    UpdateState(0);

                    AllSwitches.TryGetOidModel("ifOperStatus", out OidModel portStateOid);
                Dictionary<OidModel, EthernetPortControl> portsDictionary = new Dictionary<OidModel, EthernetPortControl>();

                UdpTarget udpTarget = new UdpTarget(_iPAddress, SNMPPort, 2000, 1);
                AgentParameters agentParams = new AgentParameters(SnmpVersion.Ver1, new OctetString("public"));
                Pdu pdu = new Pdu(PduType.Get);

                foreach (var port in _ControlChildrens)
                {
                    var portOid = AllSwitches.ReplaceXX(portStateOid, port.PortNumber);
                    portsDictionary.Add(portOid, port);
                    pdu.VbList.Add(portOid.Oid);
                }
                SnmpV1Packet result = (SnmpV1Packet)await InternalDoRequestAsync(udpTarget, agentParams, pdu);//(SnmpV1Packet)udpTarget.Request(pdu, agentParams);

                foreach (var resuldVb in result.Pdu.VbList)
                {
                    if (int.TryParse(resuldVb.Value?.ToString(), out int iPortState))
                    {
                        var portControl = portsDictionary.Where(x => x.Key.Oid == resuldVb.Oid.ToString()).FirstOrDefault();
                        portControl.Value.PortState = iPortState;
                    }
                }
                udpTarget.Close();

            }
        }


        static SNMPDeviceControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SNMPDeviceControl), new FrameworkPropertyMetadata(typeof(SNMPDeviceControl)));
        }
    }
}
