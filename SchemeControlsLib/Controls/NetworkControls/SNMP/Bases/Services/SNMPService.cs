﻿using SchemeControlsLib.Controls.NetworkControls.SNMP.Models;
using SnmpSharpNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.NetworkControls.SNMP.Services
{
    public static class SNMPService
    {
        public static SnmpV1Packet DoRequest(Dictionary<string, OidModel> oidsDict, PduType pduType, IPAddress iPAddress, int port = 161, int timeout = 2000, int retry = 1)
        {
            UdpTarget udpTarget = new UdpTarget(iPAddress, port, timeout, retry);
            AgentParameters agentParams = new AgentParameters(SnmpVersion.Ver1, new OctetString("public"));
            Pdu pdu = new Pdu(pduType);

            foreach (var oid in oidsDict)
            {
                if (!(string.IsNullOrEmpty(oid.Value.Oid)))
                {
                    pdu.VbList.Add(oid.Value.Oid);
                }
            }

            var result = InternalDoRequest(udpTarget, agentParams, pdu);
            if (result != null)
            {
                foreach (var vb in result.Pdu.VbList)
                {
                    var a = vb.Oid.ToString();
                    var dictElement = oidsDict.Where(x => x.Value.Oid.ToString() == vb.Oid.ToString()).FirstOrDefault().Value;
                    if (dictElement != null)
                        Console.WriteLine($"{dictElement.Name} : {dictElement.Description} - {vb.Value.ToString()}");
                }
                udpTarget.Close();
                return result;
            }
            else
            {

                Console.WriteLine("No response received from SNMP agent.");
            }

            udpTarget.Close();
            return null;
        }

        public static SnmpV1Packet DoRequest(KeyValuePair<string, OidModel> oidsDict, PduType pduType, IPAddress iPAddress, int port = 161, int timeout = 2000, int retry = 1)
        {
            UdpTarget udpTarget = new UdpTarget(iPAddress, port, timeout, retry);
            AgentParameters agentParams = new AgentParameters(SnmpVersion.Ver1, new OctetString("public"));
            Pdu pdu = new Pdu(pduType);


            pdu.VbList.Add(oidsDict.Value.Oid);

            var result = InternalDoRequest(udpTarget, agentParams, pdu);
            if (result != null)
            {
                foreach (var vb in result.Pdu.VbList)
                {
                    var dictElement = oidsDict.Value;
                    if (dictElement != null)
                        Console.WriteLine($"{dictElement.Name} : {dictElement.Description} - {vb.Value.ToString()}");
                }
                udpTarget.Close();
                return result;
            }
            else
            {
                Console.WriteLine("No response received from SNMP agent.");
            }
            udpTarget.Close();
            return null;
        }



        private static SnmpV1Packet InternalDoRequest(UdpTarget udpTarget, AgentParameters agentParameters, Pdu pdu)
        {
            SnmpV1Packet result = (SnmpV1Packet)udpTarget.Request(pdu, agentParameters);

            if (result != null)
            {
                if (result.Pdu.ErrorStatus != 0)
                {
                    // agent reported an error with the request
                    Console.WriteLine("Error in SNMP reply. Error {0} index {1}",
                        result.Pdu.ErrorStatus,
                        result.Pdu.ErrorIndex);
                }
                else
                {
                    return result;
                }

            }
            else
            {
                Console.WriteLine("No response received from SNMP agent.");
            }

            return null;
        }
    }
}
