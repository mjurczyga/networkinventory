﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.NetworkControls._Core.Helpers;
using SchemeControlsLib.Controls.NetworkControls.SNMP.Dictionaries;
using SchemeControlsLib.Controls.NetworkControls.SNMP.Models;
using SnmpSharpNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;

namespace SchemeControlsLib.Controls.NetworkControls
{
    public abstract class SNMPControl : NetworkControlBase, ISNMPControl
    {
        private KeyValuePair<string, OidModel> _ifNumber = AllSwitches.InterfaceOids.Where(x => x.Key == "ifNumber").FirstOrDefault();

        private KeyValuePair<string, OidModel> _ifOperStatus = AllSwitches.InterfacesOids.Where(x => x.Key == "ifOperStatus").FirstOrDefault();

        private Timer _UpdateControlTimer;
        protected IPAddress _iPAddress;

        private string _ErrorMessage;
        public string ErrorMessage
        {
            get { return _ErrorMessage; }
            set { _ErrorMessage = value; NotifyPropertyChanged("ErrorMessage"); }
        }

        #region dependency properties
        public int TimerInterval
        {
            get { return (int)GetValue(TimerIntervalProperty); }
            set { SetValue(TimerIntervalProperty, value); }
        }

        public static readonly DependencyProperty TimerIntervalProperty =
            DependencyProperty.Register("TimerInterval", typeof(int), typeof(SNMPControl), new PropertyMetadata(1000, TimerInteral_PropertyChanged));

        private static void TimerInteral_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SNMPControl sNMPControl)
            {
                if (int.TryParse(e.NewValue?.ToString(), out int iValue))
                    sNMPControl.initializeTimer();
            }
        }

        #region IP Address
        [EditorProperty("Adres IP", "Ustawienia sieci")]
        public string IP
        {
            get { return (string)GetValue(IPProperty); }
            set { SetValue(IPProperty, value); }
        }

        public static readonly DependencyProperty IPProperty =
            DependencyProperty.Register("IP", typeof(string), typeof(SNMPControl), new PropertyMetadata(null, IP_PropertyChange));

        private static void IP_PropertyChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SNMPControl sNMPControl)
            {
                if (IPHelpers.TryGetIPAdress(e.NewValue?.ToString(), out IPAddress iPAddress))
                {
                    sNMPControl._iPAddress = iPAddress;
                    sNMPControl.InitControl(iPAddress);
                    sNMPControl.AddCliclEvent();
                }
            }
        }


        public int SNMPPort
        {
            get { return (int)GetValue(SNMPPortProperty); }
            set { SetValue(SNMPPortProperty, value); }
        }

        public static readonly DependencyProperty SNMPPortProperty =
            DependencyProperty.Register("SNMPPort", typeof(int), typeof(SNMPControl), new PropertyMetadata(161));
        #endregion IP Address

        #region Ping
        [EditorProperty("Czy pingować", "Ustawienia sieci")]
        public bool UsePing
        {
            get { return (bool)GetValue(UsePingProperty); }
            set { SetValue(UsePingProperty, value); }
        }

        public static readonly DependencyProperty UsePingProperty =
            DependencyProperty.Register("UsePing", typeof(bool), typeof(SNMPControl), new PropertyMetadata(false));
        #endregion Ping

        #endregion dependency properties


        protected void initializeTimer()
        {
            if (TimerInterval > 0 && _iPAddress != null)
            {
                _UpdateControlTimer = new Timer(TimerInterval);
                _UpdateControlTimer.Elapsed += _UpdateControlTimer_Elapsed;
                _UpdateControlTimer.AutoReset = true;
                _UpdateControlTimer.Start();
            }
        }

        private void _UpdateControlTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _Dispatcher.Invoke(() =>
            {
                UpdateControl();
            });
        }

        private void AddCliclEvent()
        {
            MouseDoubleClick += SNMPControl_MouseDoubleClick;
        }

        private void SNMPControl_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if(_iPAddress != null)
                System.Diagnostics.Process.Start($"http://{_iPAddress.ToString()}");
        }

        public abstract void UpdateControl();

        public abstract void InitControl(IPAddress iPAddress);

        public override void Remove()
        {
            base.Remove();
            if (_UpdateControlTimer != null)
            {
                _UpdateControlTimer.Stop();
                _UpdateControlTimer = null;
            }
        }

        protected SnmpPacket InternalDoRequest(UdpTarget udpTarget, AgentParameters agentParameters, Pdu pdu)
        {
            try
            {
                SnmpPacket result = udpTarget.Request(pdu, agentParameters);
                if (result != null)
                {
                    if (result.Pdu.ErrorStatus != 0)
                    {
                        Console.WriteLine("Error in SNMP reply. Error {0} index {1}",
                            result.Pdu.ErrorStatus,
                            result.Pdu.ErrorIndex);
                    }
                    else
                    {
                        return result;
                    }

                }
                else
                {
                    Console.WriteLine("No response received from SNMP agent.");
                }
            }
            catch(Exception e)
            {
                MessageBox.Show($"Problem z pobraniem danych po SNMP dla adresu IP - {_iPAddress.ToString()}. Treść błędu: {e.Message}");
            }



            return null;
        }

        protected Task<SnmpPacket> InternalDoRequestAsync(UdpTarget udpTarget, AgentParameters agentParameters, Pdu pdu)
        {
            return Task.Run(() =>
            {
                return InternalDoRequest(udpTarget, agentParameters, pdu);
            });
        }

        private bool _busyPing = false;
        protected Task PingTask()
        {
            return Task.Run(async () =>
           {
               if (!_busyPing && _iPAddress != null)
               {
                   _busyPing = true;
                   var pingState = new Ping();
                   try
                   {
                       var result = await pingState.SendPingAsync(_iPAddress);
                       if (result.Status == IPStatus.Success)
                           UpdateState(2);
                       else
                           UpdateState(1);
                   }
                   catch (Exception e)
                   {
                       UpdateState(1);
                   }

               }
               _busyPing = false;

           });
        }

        protected void UpdateState(int state)
        {
            _Dispatcher.Invoke(() =>
            {
                State = state;
                NotifyPropertyChanged("State");
                NotifyPropertyChanged("Connected");
            });
        }
    }
}
