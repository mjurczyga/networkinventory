﻿using SchemeControlsLib.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.NetworkControls.SNMP.Models
{
    public class OidModel : ObservableObject
    {
        private string _Oid;
        public string Oid
        {
            get { return _Oid; }
            set { _Oid = value; NotifyPropertyChanged("Oid"); }
        }

        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; NotifyPropertyChanged("Name"); }
        }

        private string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; NotifyPropertyChanged("Description"); }
        }

        private object _Value;
        public object Value
        {
            get { return _Value; }
            set { _Value = value; NotifyPropertyChanged("Description"); }
        }


        public OidModel()
        {

        }

        public OidModel(OidModel oidModel)
        {
            SetProperties(oidModel.Oid, oidModel.Name, oidModel.Description);
        }

        public OidModel(string oid, string name, string description)
        {
            SetProperties(oid, name, description);
        }

        public void UpdateValue(object value)
        {
            Value = value;
        }

        private void SetProperties(string oid, string name, string description)
        {
            Oid = oid;
            Name = name;
            Description = description;
        }

    }
}
