﻿using SnmpSharpNet;

namespace SchemeControlsLib.Controls.NetworkControls
{
    public interface ISNMPControl
    {
        string IP { get; }
        int SNMPPort { get; }

        //Pdu GetControlPDU();

        void UpdateControl();
    }
}