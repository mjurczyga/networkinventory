﻿using SchemeControlsLib.Controls.NetworkControls.SNMP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.NetworkControls.SNMP.Dictionaries
{
    public class AllSwitches
    {
        public static Dictionary<string, OidModel> InterfaceOids = new Dictionary<string, OidModel>()
        {
            { "ifNumber", new OidModel("1.3.6.1.2.1.2.1.0", "ifNumber", "Liczba portów") },
        };

        public static Dictionary<string, OidModel> InterfacesOids = new Dictionary<string, OidModel>()
        {
            { "ifIndex", new OidModel("1.3.6.1.2.1.2.2.1.1.XX", "ifIndex", "Index") },
            { "ifDescr", new OidModel("1.3.6.1.2.1.2.2.1.2.XX", "ifDescr", "Opis") },
            { "ifType ", new OidModel("1.3.6.1.2.1.2.2.1.3.XX", "ifType", "Typ") },
            { "ifMtu", new OidModel("1.3.6.1.2.1.2.2.1.4.XX", "ifMtu", "Rozmiar największego datagramu") },
            { "ifSpeed", new OidModel("1.3.6.1.2.1.2.2.1.5.XX", "ifSpeed", "Prędkość") },
            { "ifPhysAddress", new OidModel("1.3.6.1.2.1.2.2.1.6.XX", "ifPhysAddress", "Adres fizyczny") },
            { "ifAdminStatus", new OidModel("1.3.6.1.2.1.2.2.1.7.XX", "ifAdminStatus", "Stan") },
            { "ifOperStatus", new OidModel("1.3.6.1.2.1.2.2.1.8.XX", "ifOperStatus", "Status portu") },
            { "ifLastChange", new OidModel("1.3.6.1.2.1.2.2.1.9.XX", "ifLastChange", "Ostatnia zmiana") },
            { "ifInOctets", new OidModel("1.3.6.1.2.1.2.2.1.10.XX", "ifInOctets", "Oktety") },
            { "ifInUcastPkts ", new OidModel("1.3.6.1.2.1.2.2.1.11.XX", "ifInUcastPkts", "") },
            { "ifInNUcastPkts", new OidModel("1.3.6.1.2.1.2.2.1.12.XX", "ifInNUcastPkts", "") },
            { "ifInDiscards", new OidModel("1.3.6.1.2.1.2.2.1.13.XX", "ifInDiscards ", "") },
            { "ifInErrors", new OidModel("1.3.6.1.2.1.2.2.1.14.XX", "ifInErrors", "") },
            { "ifInUnknownProtos", new OidModel("1.3.6.1.2.1.2.2.1.15.XX", "ifInUnknownProtos", "") },
            { "ifOutOctets", new OidModel("1.3.6.1.2.1.2.2.1.16.XX", "ifOutOctets", "") },
            { "ifOutUcastPkts", new OidModel("1.3.6.1.2.1.2.2.1.17.XX", "ifOutUcastPkts", "") },
            { "ifOutNUcastPkts", new OidModel("1.3.6.1.2.1.2.2.1.18.XX", "ifOutNUcastPkts", "") },
            { "ifOutDiscards", new OidModel("1.3.6.1.2.1.2.2.1.19.XX", "ifOutDiscards", "") },
            { "ifOutErrors", new OidModel("1.3.6.1.2.1.2.2.1.20.XX", "ifOutErrors", "") },
            { "ifOutQLen", new OidModel("1.3.6.1.2.1.2.2.1.21.XX", "ifOutQLen", "") },
            //{ "ifSpecific", new OidModel("1.3.6.1.2.1.2.2.1.22.XX", "ifSpecific", "") },
        };



        public static Dictionary<string, OidModel> SystemOids = new Dictionary<string, OidModel>()
        {
            { "sysDescr", new OidModel("1.3.6.1.2.1.1.1.0", "sysDescr", "Liczba portów") },
            { "sysObjectID", new OidModel("1.3.6.1.2.1.1.2.0", "sysObjectID", "ID objektu") },
            { "sysUpTime", new OidModel("1.3.6.1.2.1.1.3.0", "sysUpTime", "Czas działania") },
            { "sysContact", new OidModel("1.3.6.1.2.1.1.4.0", "sysContact", "Kontakt") },
            { "sysName", new OidModel("1.3.6.1.2.1.1.5.0", "sysName", "Nazwa urządzenia") },
            { "sysLocation", new OidModel("1.3.6.1.2.1.1.6.0", "sysLocation", "Lokalizacja") },
            { "sysServices", new OidModel("1.3.6.1.2.1.1.7.0", "sysServices", "Serwisy") },

        };

        public static bool TryGetOidModel(string propertyName, out OidModel oidModel)
        {
            oidModel = null;

            if (InterfaceOids.TryGetValue(propertyName, out OidModel oidModel1))
            {
                oidModel = oidModel1;
                return true;
            }
            else if (InterfacesOids.TryGetValue(propertyName, out OidModel oidModel2))
            {
                oidModel = oidModel2;
                return true;
            }
            else if (SystemOids.TryGetValue(propertyName, out OidModel oidModel3))
            {
                oidModel = oidModel3;
                return true;
            }
            else
            {
                return false;
            }

        }

        public static Dictionary<string, OidModel> ReplaceXX(Dictionary<string, OidModel> oidsDict, int number)
        {
            Dictionary<string, OidModel> result = new Dictionary<string, OidModel>();

            foreach (var element in oidsDict)
            {
                OidModel oid = new OidModel(element.Value.Oid.Replace("XX", number.ToString()), element.Value.Name, element.Value.Description);

                result.Add(element.Key, oid);
            }

            return result;
        }

        public static OidModel ReplaceXX(OidModel oidModel, int number)
        {
            OidModel result = null;
            if (oidModel != null)
            {
                result = new OidModel(oidModel.Oid.Replace("XX", number.ToString()), oidModel.Name, oidModel.Description);
            }


            return result;
        }

    }
}
