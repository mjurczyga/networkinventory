﻿using SchemeControlsLib.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using SchemeControlsLib.Models;
using SchemeControlsLib.Controls.NetworkControls.Types;

namespace SchemeControlsLib.Controls.NetworkControls
{
    public class NetworkLinesSettings
    {
        private Dictionary<int, NetworkLine> _NetworkSchemeLines;
        public Dictionary<int, NetworkLine> NetworkSchemeLines
        {
            get { return _NetworkSchemeLines; }
            set { _NetworkSchemeLines = value; }
        }

        //public static NetworkLine AddNewLine()
        //{
            //var name = GetNextLineName();
            //var newLine = new NetworkLine(Brushes.LightGreen, 2, name, NetworkTypes.None);
            //AddLineToLines(newLine);

            //return newLine;
        //}

        public ResultMessage ChangeLineName(NetworkLine networkLine, string newName)
        {


            return new ResultMessage();
        }

        public string GetNextLineName()
        {
            var resultName = "NewLine";
            var newLines = NetworkSchemeLines.Where(x => x.Value.Name.Contains(resultName));
            if(newLines != null)
            {
                foreach(var newLine in newLines)
                {
                    var splittedName = newLine.Value.Name.Split('_');
                    if(splittedName.Count() > 2 && int.TryParse(splittedName[1], out int intVal))
                    {
                        resultName += "_" + (intVal++).ToString();
                    }
                    else
                        resultName += "_1";
                }
            }

            return resultName;
        }

        private void AddLineToLines(NetworkLine networkLine)
        {
            if (networkLine != null && NetworkSchemeLines != null)
                NetworkSchemeLines.Add(GetNextNetworkSchemeLinesIndex(), networkLine);
        }

        private void RemoveLineFromLines(NetworkLine networkLine)
        {
            if (networkLine != null && NetworkSchemeLines != null)
                NetworkSchemeLines.Add(GetNextNetworkSchemeLinesIndex(), networkLine);
        }

        private int GetNextNetworkSchemeLinesIndex()
        {
            int currentIndex = 0;
            int nextIndex = 0;
            foreach(var element in NetworkSchemeLines)
            {
                if (element.Key > currentIndex)
                {
                    currentIndex = element.Key;
                    nextIndex = currentIndex + 1;
                }
            }

            return nextIndex;
        }
    }
}
