﻿using SchemeControlsLib.Base;
using SchemeControlsLib.Controls.NetworkControls.ValueSerializers;
using SchemeControlsLib.Controls.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.NetworkControls.Types
{
    [ValueSerializer(typeof(NetworkTypeValueSerializer))]
    [TypeConverter(typeof(NetworkTypeConverter))]
    public class NetworkType : ClassType
    {
        public override List<ClassType> GetClassTypes()
        {
            return NetworkTypes.GetList().ToList<ClassType>();
        }

        public NetworkType(int typeNumber, string typeName, string name, string toolTip) : base(typeNumber, typeName, name, toolTip)
        {
        }

        public override string ToString()
        {
            return this.TypeName;
        }

        public virtual bool CanSerializeToString()
        {
            return NetworkTypes.GetList().Any(x => x.TypeName == this.TypeName);
        }

        internal static NetworkType Parse(string value, ITypeDescriptorContext context)
        {
            return NetworkTypes.GetList().Where(x => x.TypeName == value).FirstOrDefault();
        }

        internal virtual string ConvertToString(string format, IFormatProvider provider)
        {
            return this.ToString();
        }
    }
}
