﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.NetworkControls.Types
{
    public static class NetworkLines
    {
        private static List<NetworkLine> _Sources;

        public static NetworkLine Disconnected = new NetworkLine(0, "Disconnected", "Brak połączenia", "", Brushes.Red, 4, NetworkTypes.Disconnected);
        public static NetworkLine None = new NetworkLine(1, "None", "Brak nazwy", "", Brushes.White, 2, NetworkTypes.None);
        public static NetworkLine Ethernet = new NetworkLine(2, "Ethernet", "Ethernet", "", Brushes.LightGreen, 4, NetworkTypes.Ethernet);
        public static NetworkLine RS = new NetworkLine(3, "RS", "RS - 485", "", Brushes.Orange, 3, NetworkTypes.RS485);
        public static NetworkLine EtherCat = new NetworkLine(4, "EtherCat", "EtherCat", "", Brushes.Blue, 4, NetworkTypes.EtherCat);
        public static NetworkLine Fibre = new NetworkLine(5, "Fibre", "Fibre", "", Brushes.Yellow, 4, NetworkTypes.Fibre);

        static NetworkLines()
        {
                _Sources = typeof(NetworkLines).GetFields(BindingFlags.Public | BindingFlags.Static).
                Where(f => f.FieldType == typeof(NetworkLine)).
                Select(f => f.GetValue(null)).
                Cast<NetworkLine>().ToList();
       }    

        public static List<NetworkLine> GetList()
        {
            return _Sources;
        }

        public static NetworkLine GetElementByType(NetworkType networkType)
        {
            return _Sources?.Where(x => x.NetworkType == networkType).FirstOrDefault();
        }
    }
}
