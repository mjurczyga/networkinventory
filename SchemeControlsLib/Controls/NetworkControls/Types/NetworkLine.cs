﻿using SchemeControlsLib.Base;
using SchemeControlsLib.Controls.NetworkControls.ValueSerializers;
using SchemeControlsLib.Controls.Types;
using SchemeControlsLib.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace SchemeControlsLib.Controls.NetworkControls.Types
{
    [TypeConverter(typeof(NetworkLineConverter))]
    public class NetworkLine : ClassType
    {
        private Brush _Stroke;
        public Brush Stroke
        {
            get { return _Stroke; }
            set { _Stroke = value; NotifyPropertyChanged("Stroke"); }
        }

        private double _StrokeThickness;
        public double StrokeThickness
        {
            get { return _StrokeThickness; }
            set { _StrokeThickness = value; NotifyPropertyChanged("StrokeThickness"); }
        }

        private NetworkType _NetworkType;
        public NetworkType NetworkType
        {
            get { return _NetworkType; }
            set { _NetworkType = value; NotifyPropertyChanged("NetworkType"); }
        }

        public override List<ClassType> GetClassTypes()
        {
            return NetworkLines.GetList().ToList<ClassType>();
        }

        public NetworkLine(int typeNumber, string typeName, string name, string toolTip, Brush stroke, double strokeThickness, NetworkType networkType) : base(typeNumber, typeName, name, toolTip)
        {
            Stroke = stroke;
            StrokeThickness = strokeThickness;
            NetworkType = networkType;            
        }

        public ResultMessage ChangeName(string newName, List<string> usedNames)
        {
            if (newName == this.Name)
                return new ResultMessage(false, "Nie zmieniono nazwy linii");           
            else if (usedNames.Contains(newName))
                return new ResultMessage(false, "Nazwa jest już w użyciu");
            else
                return new ResultMessage(true, "Nazwa została zaktualizowana");
        }

        public override string ToString()
        {
            return this.NetworkType.ToString();
        }

        public virtual bool CanSerializeToString()
        {
            return NetworkLines.GetList().Any(x => x.NetworkType == this.NetworkType);
        }

        internal static NetworkLine Parse(string value, ITypeDescriptorContext context)
        {
            return NetworkLines.GetList().Where(x => x.NetworkType.ToString() == value).FirstOrDefault();
        }

        internal virtual string ConvertToString(string format, IFormatProvider provider)
        {
            return this.ToString();
        }
    }
}
