﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Controls.NetworkControls.Types
{
    public static class NetworkTypes
    {
        private static List<NetworkType> _Sources; 

        public static NetworkType Disconnected = new NetworkType(0, "Disconnected", "Brak połączenia", "");
        public static NetworkType None = new NetworkType(1, "None", "Brak", "");
        public static NetworkType Ethernet = new NetworkType(2, "Ethernet", "Ethernet", "");
        public static NetworkType EtherCat = new NetworkType(3, "EtherCat", "EtherCat", "");
        public static NetworkType RS485 = new NetworkType(4, "RS485", "RS-485", "");
        public static NetworkType Fibre = new NetworkType(5, "Fibre", "Światłowód", "");

        static NetworkTypes()
        {
                _Sources = typeof(NetworkTypes)
              .GetFields(BindingFlags.Public | BindingFlags.Static).Where(f => f.FieldType == typeof(NetworkType))
              .Select(f => f.GetValue(null)).Cast<NetworkType>().ToList();
        }

        public static List<NetworkType> GetList()
        {
            return _Sources;
        }

        public static NetworkType GetElementFromString(string type)
        {
            return _Sources.Where(x => x.Name == "type").FirstOrDefault();
        }
    }
}
