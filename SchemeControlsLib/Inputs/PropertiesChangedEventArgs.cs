﻿using SchemeControlsLib.Controls.ElectricControls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SchemeControlsLib.Inputs
{
    public class PropertiesChangedEventArgs : EventArgs
    {
        private FrameworkElement _schemeElement;
        public PropertiesChangedEventArgs(FrameworkElement schemeElement)
        {
            _schemeElement = schemeElement;
        }

        public FrameworkElement Value { get { return _schemeElement; } }
    }

    public delegate void PropertiesChangedDelegate(object obj, PropertiesChangedEventArgs args);


    public class SelectedElementChangedEventArgs : EventArgs
    {
        private FrameworkElement _schemeElement;
        public SelectedElementChangedEventArgs(FrameworkElement schemeElement)
        {
            _schemeElement = schemeElement;
        }

        public FrameworkElement Value { get { return _schemeElement; } }
    }

    public delegate void SelectedElementChangedDelegate(object obj, SelectedElementChangedEventArgs args);

    public class SourceBrushesChangedEventArgs : EventArgs
    {
        private IList<Brush> _OldBrushes;
        private IList<Brush> _NewBrushes;
        public SourceBrushesChangedEventArgs(IList<Brush> oldBrushes, IList<Brush> newBrushes)
        {
            _OldBrushes = oldBrushes;
            _NewBrushes = newBrushes;
        }

        public IList<Brush> OldBrushes { get { return _OldBrushes; } }
        public IList<Brush> NewBrushes { get { return _NewBrushes; } }
    }

    public delegate void SourceBrushesChangedDelegate(object obj, SourceBrushesChangedEventArgs args);

    public class OutputNodesChangedEventArgs : EventArgs
    {
        private IList<OutputNode> _OldOutputNodes;
        private IList<OutputNode> _NewOutputNodes;
        public OutputNodesChangedEventArgs(IList<OutputNode> oldOutputNodes, IList<OutputNode> newOutputNodes)
        {
            _OldOutputNodes = oldOutputNodes;
            _NewOutputNodes = newOutputNodes;
        }

        public IList<OutputNode> OldOutputNodes { get { return _OldOutputNodes; } }
        public IList<OutputNode> NewOutputNodes { get { return _NewOutputNodes; } }
    }

    public delegate void OutputNodesChangedDelegate(object obj, OutputNodesChangedEventArgs args);

}

