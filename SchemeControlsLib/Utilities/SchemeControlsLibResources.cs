﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SchemeControlsLib.Utilities
{
    public static class SchemeControlsLibResources
    {
        public static ResourceDictionary IconsDrawingBrushDictionary;

        static SchemeControlsLibResources()
        {
            IconsDrawingBrushDictionary = new ResourceDictionary();
            IconsDrawingBrushDictionary.Source =
                new Uri("/SchemeControlsLib;component/Themes/Generic.xaml",
                    UriKind.RelativeOrAbsolute);
        }

        public static object GetIconByKey(string resourceKey)
        {
            if(IconsDrawingBrushDictionary.Contains(resourceKey) && IconsDrawingBrushDictionary[resourceKey] is DrawingBrush)
            {
                return new Border() { Background = (DrawingBrush)IconsDrawingBrushDictionary[resourceKey] };
            }
            return null;
        }

        public static System.Windows.Media.Brush GetIconBrushByKey(string resourceKey)
        {
            if (IconsDrawingBrushDictionary.Contains(resourceKey) && IconsDrawingBrushDictionary[resourceKey] is DrawingBrush)
            {
                return (DrawingBrush)IconsDrawingBrushDictionary[resourceKey];
            }
            return null;
        }

        public static object GetResourceByKey(string resourceKey)
        {
            if (IconsDrawingBrushDictionary.Contains(resourceKey))
            {
                return IconsDrawingBrushDictionary[resourceKey];
            }
            return null;
        }
    }
}
