﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Utilities
{
    public static class DictionaryExt
    {
        public static void AddDictToDict<T, T1>(IDictionary<T,T1> firstDictionary, IDictionary<T,T1> secondDictionary) 
        {
            if (firstDictionary != null && secondDictionary != null)
            {
                foreach (var element in secondDictionary)
                {
                    if (!firstDictionary.Contains(element))
                        firstDictionary.Add(element);
                }
            }
        }
    }
}
