﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeControlsLib.Utilities
{
    public static class ThicknessConv
    {
        public static Thickness FromString(this string thickness)
        {
            var splitted = thickness.Split(',');
            if (splitted.Count() == 4)
                return new Thickness(double.Parse(splitted[0]), double.Parse(splitted[1]), double.Parse(splitted[2]), double.Parse(splitted[3]));
            return new Thickness(0);
        }     
    }
    public static class CornerRadiusConv
    {
        public static CornerRadius FromString(this string thickness)
        {
            var splitted = thickness.Split(',');
            if (splitted.Count() == 4)
                return new CornerRadius(double.Parse(splitted[0]), double.Parse(splitted[1]), double.Parse(splitted[2]), double.Parse(splitted[3]));
            return new CornerRadius(0);
        }
    }
}
