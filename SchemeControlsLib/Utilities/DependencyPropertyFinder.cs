﻿using SchemeControlsLib.Controls.Attributes;
using SchemeControlsLib.Controls.Utilities;
using SchemeControlsLib.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup.Primitives;

namespace SchemeControlsLib.Utilities
{
    public static class DependencyPropertyFinder
    {

        public static Dictionary<object,string> GetControlSpecificDependencyPropertiesDict(object control, IDictionary<string, EditorProperty> controlProperties, bool findConnections = false)
        {
            Dictionary<object, string> properties = null;
            if (control != null && controlProperties != null)
            {
                properties = new Dictionary<object, string>();
                var allControlProperties = GetDependencyProperties(control);
                foreach (var fieldInfo in allControlProperties)
                {
                    if (controlProperties.Any(s => fieldInfo.Name == s.Key + "Property"))
                    {
                        var fieldInfoValue = fieldInfo.GetValue(fieldInfo) as DependencyProperty;
                        properties.Add(fieldInfoValue, controlProperties[fieldInfo.Name.Replace("Property", "")].Description);
                    }
                }
                var allAtached = GetAttachedProperties(control);
                foreach (var dependencyProperty in allAtached)
                {
                    if (controlProperties.Any(s => dependencyProperty.Name == s.Key ))
                    {
                        var fieldInfoValue = dependencyProperty;
                        properties.Add(fieldInfoValue, controlProperties[dependencyProperty.Name].Description);
                    }
                }
                if (findConnections)
                {
                    var props = GetProperties(control);
                    var allProperties = GetProperties(control).Where(x => ElementPropertiesModels.OutputNodesProperties.ContainsKey(x.Name) || ElementPropertiesModels.OutputNodesProperties1.ContainsKey(x.Name)).Select(x => new KeyValuePair<object, string>(x, x.Name)).ToDictionary(x => x.Key, x => x.Value);
                    DictionaryExt.AddDictToDict(properties, allProperties);
                }
            }
            return properties;
        }

        /// <summary>
        /// Metoda dodana dla budowania "edytora" właściwośći
        /// </summary>
        /// <param name="control"></param>
        /// <param name="controlProperties"></param>
        /// <returns></returns>
        public static Dictionary<object, EditorProperty> GetControlSpecificDependencyPropertiesWithAttributesDict(object control, IDictionary<string, EditorProperty> controlProperties)
        {
            Dictionary<object, EditorProperty> properties = null;
            if (control != null && controlProperties != null)
            {
                properties = new Dictionary<object, EditorProperty>();
                var allControlProperties = GetDependencyProperties(control);
                foreach (var fieldInfo in allControlProperties)
                {
                    if (controlProperties.Any(s => fieldInfo.Name == s.Key + "Property"))
                    {
                        var fieldInfoValue = fieldInfo.GetValue(fieldInfo) as DependencyProperty;
                        properties.Add(fieldInfoValue, controlProperties[fieldInfo.Name.Replace("Property", "")]);
                    }
                }
                var allAtached = GetAttachedProperties(control);
                foreach (var dependencyProperty in allAtached)
                {
                    if (controlProperties.Any(s => dependencyProperty.Name == s.Key))
                    {
                        var fieldInfoValue = dependencyProperty;
                        properties.Add(fieldInfoValue, controlProperties[dependencyProperty.Name]);
                    }
                }
            }
            return properties;
        }

        public static Dictionary<object, string> GetControlSpecificDependencyProperty(object control, string controlPropertyName, EditorProperty editorProperty)
        {
            Dictionary<object, string> properties = null;
            if (control != null && controlPropertyName != null)
            {
                properties = new Dictionary<object, string>();
                var allControlProperties = GetDependencyProperties(control);
                foreach (var fieldInfo in allControlProperties)
                {
                    if (fieldInfo.Name == controlPropertyName + "Property")
                    {
                        var fieldInfoValue = fieldInfo.GetValue(fieldInfo) as DependencyProperty;
                        properties.Add(fieldInfoValue, editorProperty.Description);
                        return properties;
                    }
                }
                var allAtached = GetAttachedProperties(control);
                foreach (var dependencyProperty in allAtached)
                {
                    if (dependencyProperty.Name == controlPropertyName)
                    {
                        var fieldInfoValue = dependencyProperty;
                        properties.Add(fieldInfoValue, editorProperty.Description);
                        return properties;
                    }
                }
            }
            return properties;
        }

        public static List<DependencyProperty> GetAllDependencyProperties(object control)
        {
            List<DependencyProperty> properties = null;
            if (control != null)
            {
                properties = new List<DependencyProperty>();
                var allControlProperties = GetDependencyProperties(control);
                foreach (var fieldInfo in allControlProperties)
                {
                    var fieldInfoValue = fieldInfo.GetValue(fieldInfo) as DependencyProperty;
                    properties.Add(fieldInfoValue);
                }
            }
            return properties;
        }

        /// <summary>
        /// Pobiera wszystkie uchwyty do zdarzeń
        /// </summary>
        /// <param name="control">Kontrolka z której ma pobrać</param>
        /// <returns></returns>
        public static Dictionary<FieldInfo,RoutedEventHandlerInfo> GetAllRoutedEvents(object control)
        {
            Dictionary<FieldInfo, RoutedEventHandlerInfo> result = null;
            if (control != null)
            {
                result = new Dictionary<FieldInfo, RoutedEventHandlerInfo>();
                FieldInfo[] fields = control.GetType().GetFields(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy);

                foreach (FieldInfo field in fields.Where(x => x.FieldType == typeof(RoutedEvent)))
                {
                    RoutedEventHandlerInfo[] routedEventHandlerInfos = GetRoutedEventHandlers((UIElement)control, (RoutedEvent)field.GetValue(control));
                    if (routedEventHandlerInfos != null)

                        foreach (RoutedEventHandlerInfo routedEventHandlerInfo in routedEventHandlerInfos)
                            result.Add(field, routedEventHandlerInfo);
                }
            }
            return result;
        }

        /// <summary>
        /// Pobiera podany uchwyt do zdarzenia
        /// </summary>
        /// <param name="control">kontrolka z której ma pobrać</param>
        /// <param name="handlerField">pole z którego ma pobrać</param>
        /// <returns></returns>
        public static List<RoutedEventHandlerInfo> GetSpecyficRoutedEvents(object control, FieldInfo handlerField)
        {
            List<RoutedEventHandlerInfo> result = null;
            if (control != null && handlerField != null)
            {

                if (handlerField.FieldType == typeof(RoutedEvent))
                {
                    RoutedEventHandlerInfo[] routedEventHandlerInfos = GetRoutedEventHandlers((UIElement)control, (RoutedEvent)handlerField.GetValue(control));
                    result = new List<RoutedEventHandlerInfo>();
                    if (routedEventHandlerInfos != null)

                        foreach (RoutedEventHandlerInfo routedEventHandlerInfo in routedEventHandlerInfos)
                            result.Add(routedEventHandlerInfo);
                }
            }
            return result;
        }

        /// <summary>
        /// Copy click event handlers from parent to child
        /// </summary>
        public static bool CopyParentClickHandler(UIElement parent, UIElement child)
        {
            bool result = false;
            if (parent != null)
            {
                var eventHandlers = DependencyPropertyFinder.GetSpecyficRoutedEvents(parent, parent.GetType().GetField("MouseDownEvent", BindingFlags.Static | BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy));
                if (eventHandlers != null)
                {
                    result = eventHandlers.Count > 0 ? true : false;
                    foreach (var eventHandler in eventHandlers)
                    {
                        child.AddHandler(UIElement.MouseDownEvent, eventHandler.Handler);
                    }
                }
            }
            return result;
        }

        private static RoutedEventHandlerInfo[] GetRoutedEventHandlers(UIElement element, RoutedEvent routedEvent)
        {
            PropertyInfo eventHandlersStoreProperty = typeof(UIElement).GetProperty("EventHandlersStore", BindingFlags.Instance | BindingFlags.NonPublic);
            object eventHandlersStore = eventHandlersStoreProperty.GetValue(element, null);

            if (eventHandlersStore == null)
                return null;

            MethodInfo getRoutedEventHandlers = eventHandlersStore.GetType().GetMethod("GetRoutedEventHandlers", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            return (RoutedEventHandlerInfo[])getRoutedEventHandlers.Invoke(eventHandlersStore, new object[] { routedEvent });
        }

        private static List<FieldInfo> GetDependencyProperties(object element)
        {
            var dependencyProperties = element.GetType().GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy | BindingFlags.NonPublic | BindingFlags.Instance)
                                       .Where(p => p.FieldType.Equals(typeof(DependencyProperty)));

            return dependencyProperties.ToList();
        }

        private static IEnumerable<DependencyProperty> GetAttachedProperties(object element)
        {
            if (element != null)
            {
                MarkupObject markupObject = MarkupWriter.GetMarkupObjectFor(element);
                if (markupObject != null)
                {
                    foreach (MarkupProperty mp in markupObject.Properties)
                    {
                        if (mp.IsAttached)
                            yield return mp.DependencyProperty;
                    }
                }
            }
        }

        private static IEnumerable<PropertyInfo> GetProperties(object element)
        {
            var properties = element.GetType().GetProperties();

            return properties.ToList();
        }

    }
}
