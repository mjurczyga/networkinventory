﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Utilities
{
    public static class MathExt
    {
        public static bool IsNegative(this double checkedVal)
        {
            if (checkedVal <= 0)
                return true;
            else
                return false;              
        }

        public static double GetValueDblSign(this double value)
        {
            if (value < 0.0)
                return -1.0;
            else 
                return 1.0;

        }

        public static int GetValueIntSign(this double value)
        {
            if (value < 0.0)
                return -1;
            else
                return 1;
        }

        public static double GetSinusSign(double angle)
        {
            var angleRadians = Math.PI * angle / 180.0;
            return  Math.Sin(angleRadians).GetValueDblSign();
        }

        public static double GetCosinusSign(double angle)
        {
            var angleRadians = Math.PI * angle / 180.0;
            return Math.Cos(angleRadians).GetValueDblSign();
        }

        public static bool IsSinusSignNegative(double angle)
        {
            var angleRadians = Math.PI * angle / 180.0;
            return Math.Sin(angleRadians).GetValueDblSign().IsNegative();
        }

        public static bool IsCosinusSignNegative(double angle)
        {
            var angleRadians = Math.PI * angle / 180.0;
            return Math.Cos(angleRadians).GetValueDblSign().IsNegative();
        }
    }
}
