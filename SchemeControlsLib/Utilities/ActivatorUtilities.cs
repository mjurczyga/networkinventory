﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Utilities
{
    public static class ActivatorUtilities
    {
        //TODO: zrobić tak żeby robiło kontrolkę jeśli znajduję się parametr na liscie
        public static object CreateInstance(Type type, IEnumerable<object> parameters)
        {
            var ctors = type.GetConstructors();
            foreach (var ctor in ctors)
            {
                var ctorParameters = ctor.GetParameters();
                if (ctorParameters.Length != parameters.Count())
                {
                    continue;
                }

                object[] args = new object[ctorParameters.Length];
                bool success = true;
                for (int i = 0; i < ctorParameters.Length; i++)
                {
                    var parameter = ctorParameters[i];
                    // wybranie właściwości typu jak parametr konstruktora
                    var property = parameters.Where(x => x.GetType() == parameter.ParameterType).FirstOrDefault();
                    if (property == null)
                    {
                        success = false;
                        break;
                    }
                    args[i] = property;
                }
                if (!success)
                {
                    continue;
                }
                return ctor.Invoke(args);
            }
            throw new ArgumentException("No suitable constructor found");
        }
    }
}
