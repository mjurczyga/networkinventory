﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using SchemeControlsLib.Controls.Core;

namespace SchemeControlsLib.Utilities
{
    public static class CanvasUtilities
    {
        public static string GetNextNewElementName(Canvas canvas)
        {
            var electricElements = canvas?.Children?.OfType<FrameworkElement>();
            var elementsWitchNewControlName = electricElements?.Where(x => x.Name.StartsWith("NewControl"));
            List<int> indexes = new List<int>();
            foreach (var elementWitchControlName in elementsWitchNewControlName)
            {
                var number = elementWitchControlName.Name.Replace("NewControl", "");
                if (int.TryParse(number, out int result))
                    indexes.Add(result);
            }
            int newElementsIndex = 0;
            while (indexes.Exists(x => x == newElementsIndex)) { newElementsIndex++; };
            return "NewControl" + newElementsIndex;
        }

        public static Canvas SetCoordinateSystem(this Canvas canvas, Double xMin, Double xMax, Double yMin, Double yMax)
        {
            var width = xMax - xMin;
            var height = yMax - yMin;

            var translateX = -xMin;
            var translateY = height + yMin;

            var group = new TransformGroup();

            group.Children.Add(new TranslateTransform(translateX, -translateY));
            group.Children.Add(new ScaleTransform(canvas.ActualWidth / width, canvas.ActualHeight / -height));

            canvas.RenderTransform = group;

            return canvas;
        }

        public static void DrawGridLines(int yoffSet, int xoffSet, Canvas mainCanvas)
        {
            RemoveGridLines(mainCanvas);

            var width = mainCanvas.ActualWidth; // SystemParameters.PrimaryScreenWidth;
            var height = mainCanvas.ActualHeight; //SystemParameters.PrimaryScreenHeight;

            Image lines = new Image();

            lines.SetValue(Panel.ZIndexProperty, -100);
            //Draw the grid
            DrawingVisual gridLinesVisual = new DrawingVisual();
            DrawingContext dct = gridLinesVisual.RenderOpen();
            Pen lightPen = new Pen(Brushes.Gray, 0.3), darkPen = new Pen(Brushes.Gray, 0.5);
            lightPen.Freeze();
            darkPen.Freeze();

            int yOffset = yoffSet,
                xOffset = xoffSet,
                rows = (int)(height),
                columns = (int)(width),
                alternate = yOffset == 5 ? yOffset : 1,
                j = 0;

            //Draw the horizontal lines
            Point x = new Point(0, 0.5);
            Point y = new Point(width, 0.5);


            for (int i = 0; i <= rows; i++, j++)
            {
                dct.DrawLine(j % alternate == 0 ? lightPen : darkPen, x, y);
                x.Offset(0, yOffset);
                y.Offset(0, yOffset);
                //GuidelineSet guidelines = new GuidelineSet();
                //guidelines.GuidelinesX.Add(y.X);
                //guidelines.GuidelinesY.Add(y.Y);
                //dct.PushGuidelineSet(guidelines);
            }
            j = 0;
            //Draw the vertical lines
            x = new Point(0.5, 0);
            y = new Point(0.5, height);

            for (int i = 0; i <= columns; i++, j++)
            {
                dct.DrawLine(j % alternate == 0 ? lightPen : darkPen, x, y);
                x.Offset(xOffset, 0);
                y.Offset(xOffset, 0);
                //GuidelineSet guidelines = new GuidelineSet();
                //guidelines.GuidelinesX.Add(x.X);
                //guidelines.GuidelinesY.Add(x.Y);
                //guidelines.GuidelinesX.Add(y.X);
                //guidelines.GuidelinesY.Add(y.Y);
                //dct.PushGuidelineSet(guidelines);
            }

            dct.Close();

            RenderTargetBitmap bmp = new RenderTargetBitmap((int)width,
                (int)height, 96, 96, PixelFormats.Pbgra32);

            //lines.Measure(mainCanvas.RenderSize); //Important
            //lines.Arrange(new Rect(mainCanvas.RenderSize)); //Important

            bmp.Render(gridLinesVisual);
            bmp.Freeze();


            RenderOptions.SetBitmapScalingMode(lines, BitmapScalingMode.NearestNeighbor);
            RenderOptions.SetEdgeMode(lines, EdgeMode.Aliased);
            lines.Source = bmp;

            mainCanvas.Children.Add(lines);
        }

        public static void RemoveGridLines(Canvas mainCanvas)
        {
            foreach (UIElement obj in mainCanvas.Children)
            {
                if (obj is Image)
                {
                    mainCanvas.Children.Remove(obj);
                    break;
                }
            }
        }
    }
}
