﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Utilities
{
    public static class ListExt
    {
        /// <summary>
        /// dodaje liste 2 do listy 1 bez powtorzeń 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="firstList"></param>
        /// <param name="secondList"></param>
        public static void AddListToList<T>(IList<T> firstList, IList<T> secondList)
        {
            if(firstList != null && secondList != null)
            {
                foreach(var valToAdd in secondList)
                {
                    if (!firstList.Contains(valToAdd))
                        firstList.Add(valToAdd);
                }
            }
        }


        /// <summary>
        /// odejmule od listy 1 liste 2 
        /// </summary>
        /// <param name="brushesToRemove"></param>
        public static void RemoveListFromList<T>(IList<T> firstList, IList<T> secondList)
        {
            if (firstList != null && secondList.Any())
            {
                foreach (T listElement in secondList)
                {
                    if (firstList.Contains(listElement))
                    {
                        firstList.Remove(listElement);
                    }
                }
            }
        }
    }
}
