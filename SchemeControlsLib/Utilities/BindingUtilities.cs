﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace SchemeControlsLib.Utilities
{
    public static class BindingUtilities
    {
        public static IEnumerable<DependencyObject> EnumerateVisualChildren(this DependencyObject dependencyObject)
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(dependencyObject); i++)
            {
                yield return VisualTreeHelper.GetChild(dependencyObject, i);
            }
        }

        public static IEnumerable<DependencyObject> EnumerateVisualDescendents(this DependencyObject dependencyObject)
        {
            yield return dependencyObject;

            foreach (DependencyObject child in dependencyObject.EnumerateVisualChildren())
            {
                foreach (DependencyObject descendent in child.EnumerateVisualDescendents())
                {
                    yield return descendent;
                }
            }
        }

        public static void ClearBindings(this DependencyObject dependencyObject)
        {
            foreach (DependencyObject element in dependencyObject.EnumerateVisualDescendents())
            {
                BindingOperations.ClearAllBindings(element);
            }
        }

        public static Binding GetBinding(string path, object source, object fallBackValue, BindingMode bindingMode = BindingMode.Default, IValueConverter converter = null, object converterParameter = null)
        {
            Binding result = new Binding(path);
            result.Source = source;
            if (fallBackValue != null)
                result.FallbackValue = fallBackValue;
            if (converter != null)
            {
                result.Converter = converter;
                result.ConverterParameter = converterParameter;
            }
            result.Mode = bindingMode;

            return result;
        }
    }
}
