﻿using SchemeControlsLib.Controls.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Utilities
{
    public static class SchemeElementsFinder
    {
        public static Dictionary<EditorType, Type> GetSchemeControls()
        {
            var controlTypes = GetTypesWith<EditorType>(false);
            return controlTypes.ToDictionary(x => x.GetCustomAttribute<EditorType>(), x => x);
        }

        public static IEnumerable<Type> GetTypesWith<TAttribute>(bool inherit) where TAttribute : Attribute
        {
            var output = new List<Type>();

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            foreach (var assembly in assemblies)
            {
                var assembly_types = assembly.GetTypes();

                foreach (var type in assembly_types)
                {
                    if (type.IsDefined(typeof(TAttribute), inherit))
                        output.Add(type);
                }
            }

            return output;
        }
    }
}
