﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Shapes
{
    public class Triangle : Shape
    {
        public Triangle() : base()
        {
            this.Stretch = System.Windows.Media.Stretch.Uniform;
            this.ClipToBounds = true;
        }


        public enum Orientation
        {
            N,
            NE,
            E,
            SE,
            S,
            SW,
            W,
            NW
        }

        public Orientation TriangleOrientation
        {
            get { return (Orientation)GetValue(TriangleOrientationProperty); }
            set { SetValue(TriangleOrientationProperty, value); }
        }

        public static readonly DependencyProperty TriangleOrientationProperty =
            DependencyProperty.Register("TriangleOrientation", typeof(Orientation), typeof(Triangle), new UIPropertyMetadata(Orientation.N, OnOrientationChanged));

        private static void OnOrientationChanged(DependencyObject d, DependencyPropertyChangedEventArgs ek)
        {
            Triangle t = (Triangle)d;
            t.InvalidateVisual();
        }

        protected override Geometry DefiningGeometry
        {
            get { return GetGeometry(); }
        }

        private Geometry GetGeometry()
        {
            double maxWidth = Math.Max(0.0, this.Width - StrokeThickness);
            double maxHeight = Math.Max(0.0, this.Height - StrokeThickness);

            double halfStroke = 0;

            PathGeometry pathGeometry = new PathGeometry();
            if (TriangleOrientation == Orientation.N)
                pathGeometry = GetTriangleGeometry(new Point(maxWidth / 2.0, halfStroke), new Point(maxWidth, maxHeight), new Point(halfStroke, maxHeight));
            else if (TriangleOrientation == Orientation.NE)
                pathGeometry = GetTriangleGeometry(new Point(halfStroke, halfStroke), new Point(maxWidth, halfStroke), new Point(maxWidth, maxHeight));
            else if (TriangleOrientation == Orientation.E)
                pathGeometry = GetTriangleGeometry(new Point(halfStroke, halfStroke), new Point(maxWidth, maxHeight / 2.0), new Point(halfStroke, maxHeight));
            else if (TriangleOrientation == Orientation.SE)
                pathGeometry = GetTriangleGeometry(new Point(maxWidth, halfStroke), new Point(maxWidth, maxHeight), new Point(halfStroke, maxHeight));
            else if (TriangleOrientation == Orientation.S)
                pathGeometry = GetTriangleGeometry(new Point(halfStroke, halfStroke), new Point(maxWidth, halfStroke), new Point(maxWidth / 2.0, maxHeight));
            else if (TriangleOrientation == Orientation.SW)
                pathGeometry = GetTriangleGeometry(new Point(halfStroke, halfStroke), new Point(maxWidth, maxHeight), new Point(halfStroke, maxHeight));
            else if (TriangleOrientation == Orientation.W)
                pathGeometry = GetTriangleGeometry(new Point(maxWidth, halfStroke), new Point(maxWidth, maxHeight), new Point(halfStroke, maxHeight / 2.0));
            else
                pathGeometry = GetTriangleGeometry(new Point(maxWidth, halfStroke), new Point(maxWidth, maxHeight), new Point(halfStroke, maxHeight / 2.0));


            return pathGeometry;
        }

        private PathGeometry GetTriangleGeometry(Point point1, Point point2, Point point3)
        {
            List<PathSegment> pathSegments = new List<PathSegment>();
            //LineSegment line1 = new LineSegment(point1, true);
            LineSegment line2 = new LineSegment(point2, true);
            LineSegment line3 = new LineSegment(point3, true);
            pathSegments.Add(line2);
            pathSegments.Add(line3);
            PathFigure pathFigure = new PathFigure(point1, pathSegments, true);
            PathGeometry pathGeometry = new PathGeometry();
            pathGeometry.Figures.Add(pathFigure);

            return pathGeometry;

        }
    }
}


            //string data;
            //if (TriangleOrientation == Orientation.N)
            //    data = "M 0,1 l 1,1 h -2 Z";
            //else if (TriangleOrientation == Orientation.NE)
            //    data = "M 0,0 h 1 v 1 Z";
            //else if (TriangleOrientation == Orientation.E)
            //    data = "M 0,0 l 1,1 l -1,1 Z";
            //else if (TriangleOrientation == Orientation.SE)
            //    data = "M 1,0 v 1 h -1 Z";
            //else if (TriangleOrientation == Orientation.S)
            //    data = "M 0,0 h 2 l -1,1 Z";
            //else if (TriangleOrientation == Orientation.SW)
            //    data = "M 0,0 v 1 h 1 Z";
            //else if (TriangleOrientation == Orientation.W)
            //    data = "M 0,1 l 1,1 v -2 Z";
            //else
            //    data = "M 0,0 h 1 l -1,1 Z";

            //return Geometry.Parse(data);