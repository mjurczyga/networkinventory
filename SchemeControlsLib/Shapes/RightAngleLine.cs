﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchemeControlsLib.Shapes
{
    public class RightAngleLine : Shape
    {
        #region Propdps
        #region ChangeArrows
        public bool ChangeArrows
        {
            get { return (bool)GetValue(ChangeArrowsProperty); }
            set { SetValue(ChangeArrowsProperty, value); }
        }

        public static readonly DependencyProperty ChangeArrowsProperty =
            DependencyProperty.Register("ChangeArrows", typeof(bool), typeof(RightAngleLine), new PropertyMetadata(false, ChangeArrows_PropertyChanged));

        private static void ChangeArrows_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is RightAngleLine rightAngleLine)
            {

            }
        }
        #endregion ChangeArrows

        #region X1
        [TypeConverter(typeof(LengthConverter))]
        public double X1
        {
            get { return (double)GetValue(X1Property); }
            set { SetValue(X1Property, value); }
        }

        public static readonly DependencyProperty X1Property =
            DependencyProperty.Register("X1", typeof(double), typeof(RightAngleLine), new PropertyMetadata(0.0, X1_PropertyChanged));

        private static void X1_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is RightAngleLine rightAngleLine)
            {
                rightAngleLine.InvalidateMeasure();
                rightAngleLine.InvalidateVisual();
            }
        }
        #endregion X1

        #region Y1
        [TypeConverter(typeof(LengthConverter))]
        public double Y1
        {
            get { return (double)GetValue(Y1Property); }
            set { SetValue(Y1Property, value); }
        }

        public static readonly DependencyProperty Y1Property =
            DependencyProperty.Register("Y1", typeof(double), typeof(RightAngleLine), new PropertyMetadata(0.0, Y1_PropertyChanged));

        private static void Y1_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is RightAngleLine rightAngleLine)
            {
                rightAngleLine.InvalidateMeasure();
                rightAngleLine.InvalidateVisual();
            }
        }
        #endregion Y1

        #region X2
        [TypeConverter(typeof(LengthConverter))]
        public double X2
        {
            get { return (double)GetValue(X2Property); }
            set { SetValue(X2Property, value); }
        }

        public static readonly DependencyProperty X2Property =
            DependencyProperty.Register("X2", typeof(double), typeof(RightAngleLine), new PropertyMetadata(0.0, X2_PropertyChanged));

        private static void X2_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is RightAngleLine rightAngleLine)
            {
                rightAngleLine.InvalidateMeasure();
                rightAngleLine.InvalidateVisual();
            }
        }
        #endregion X2

        #region Y2
        [TypeConverter(typeof(LengthConverter))]
        public double Y2
        {
            get { return (double)GetValue(Y2Property); }
            set { SetValue(Y2Property, value); }
        }

        public static readonly DependencyProperty Y2Property =
            DependencyProperty.Register("Y2", typeof(double), typeof(RightAngleLine), new PropertyMetadata(0.0, Y2_PropertyChanged));

        private static void Y2_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is RightAngleLine rightAngleLine)
            {
                rightAngleLine.InvalidateMeasure();
                rightAngleLine.InvalidateVisual();
            }
        }
        #endregion Y2

        #region ReverseDrawing
        public bool ReverseDrawing
        {
            get { return (bool)GetValue(ReverseDrawingProperty); }
            set { SetValue(ReverseDrawingProperty, value); }
        }

        public static readonly DependencyProperty ReverseDrawingProperty =
            DependencyProperty.Register("ReverseDrawing", typeof(bool), typeof(RightAngleLine), new PropertyMetadata(false, ReverseDrawing_PropertyChanged));

        private static void ReverseDrawing_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is RightAngleLine rightAngleLine)
            {
                rightAngleLine.InvalidateVisual();
                rightAngleLine.InvalidateMeasure();
            }
        }
        #endregion ReverseDrawing

        #region IsArrowOnFirstLineEnd
        public bool IsArrowOnFirstLineEnd
        {
            get { return (bool)GetValue(IsArrowOnFirstLineEndProperty); }
            set { SetValue(IsArrowOnFirstLineEndProperty, value); }
        }

        public static readonly DependencyProperty IsArrowOnFirstLineEndProperty =
            DependencyProperty.Register("IsArrowOnFirstLineEnd", typeof(bool), typeof(RightAngleLine), new PropertyMetadata(false, IsArrowOnFirstLineEnd_PropertyChanged));

        private static void IsArrowOnFirstLineEnd_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is RightAngleLine rightAngleLine)
            {
                rightAngleLine.InvalidateVisual();
            }
        }
        #endregion IsArrowOnFirstLineEnd

        #region IsArrowOnSecondLineEnd
        public bool IsArrowOnSecondLineEnd
        {
            get { return (bool)GetValue(IsArrowOnSecondLineEndProperty); }
            set { SetValue(IsArrowOnSecondLineEndProperty, value); }
        }

        public static readonly DependencyProperty IsArrowOnSecondLineEndProperty =
            DependencyProperty.Register("IsArrowOnSecondLineEnd", typeof(bool), typeof(RightAngleLine), new PropertyMetadata(false, IsArrowOnSecondLineEndProperty_PropertyChanged));

        private static void IsArrowOnSecondLineEndProperty_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is RightAngleLine rightAngleLine)
            {
                rightAngleLine.InvalidateVisual();
            }
        }
        #endregion ArrowOnSecondLineEnd
        #endregion Propdps

        protected override Geometry DefiningGeometry
        {
            get { return GetGeometry(); }
        }

        private double arrowOffset = 0.3;

        private Geometry GetGeometry()
        {
            List<PathSegment> pathSegments = new List<PathSegment>();
            if (!ReverseDrawing)
            {
                // firs line
                var y_Difference = Y2 - Y1;
                if (!IsArrowOnFirstLineEnd || y_Difference == 0)
                    pathSegments.Add(new LineSegment(new Point(X1, Y2), true));
                else
                {
                    if (Math.Abs(y_Difference) > arrowOffset )
                    {
                        double lineEnd = 0, arrowEnd = 0;

                        if (y_Difference > 0) // strzałka w górę
                        {
                            lineEnd = Y2 - 2 * this.StrokeThickness;
                            arrowEnd = Y2 - this.StrokeThickness;
                        }
                        else if (y_Difference < 0) // strzałka w dół
                        {
                            lineEnd = Y2 + 2 * this.StrokeThickness;
                            arrowEnd = Y2 + this.StrokeThickness;
                        }

                        pathSegments.Add(new LineSegment(new Point(X1, lineEnd), true));
                        pathSegments.Add(new LineSegment(new Point(X1 + 0.5 * this.StrokeThickness, lineEnd), true));
                        pathSegments.Add(new LineSegment(new Point(X1, arrowEnd), true));
                        pathSegments.Add(new LineSegment(new Point(X1 - 0.5 * this.StrokeThickness, lineEnd), true));
                        pathSegments.Add(new LineSegment(new Point(X1, lineEnd), true));
                        pathSegments.Add(new LineSegment(new Point(X1, Y2), false));
                    }
                }

                // second line
                var x_Difference = X2 - X1;
                if (!IsArrowOnSecondLineEnd || x_Difference == 0)
                    pathSegments.Add(new LineSegment(new Point(X2, Y2), true));
                else
                {
                    if (Math.Abs(x_Difference) > arrowOffset)
                    {
                        double lineEnd = 0, arrowEnd = 0;
                        if (x_Difference > 0) // strzałka w prawo
                        {
                            lineEnd = X2 - 2 * this.StrokeThickness;
                            arrowEnd = X2 - this.StrokeThickness;
                        }
                        else if (x_Difference < 0) // strzałka w lewo
                        {
                            lineEnd = X2 + 2 * this.StrokeThickness;
                            arrowEnd = X2 + this.StrokeThickness;
                        }

                        pathSegments.Add(new LineSegment(new Point(lineEnd, Y2), true));
                        pathSegments.Add(new LineSegment(new Point(lineEnd, Y2 + 0.5 * this.StrokeThickness), true));
                        pathSegments.Add(new LineSegment(new Point(arrowEnd, Y2), true));
                        pathSegments.Add(new LineSegment(new Point(lineEnd, Y2 - 0.5 * this.StrokeThickness), true));
                        pathSegments.Add(new LineSegment(new Point(lineEnd, Y2), true));
                    }

                }
            }
            else
            {
                //first line
                var x_Difference = X2 - X1;
                if (!IsArrowOnFirstLineEnd || x_Difference == 0)
                    pathSegments.Add(new LineSegment(new Point(X2, Y1), true));
                else
                {
                    if (Math.Abs(x_Difference) > arrowOffset)
                    {
                        double lineEnd = 0, arrowEnd = 0;
                        if (x_Difference > 0) // strzałka w prawo
                        {
                            lineEnd = X2 - 2 * this.StrokeThickness;
                            arrowEnd = X2 - this.StrokeThickness;
                        }
                        else if (x_Difference < 0) // strzałka w lewo
                        {
                            lineEnd = X2 + 2 * this.StrokeThickness;
                            arrowEnd = X2 + this.StrokeThickness;
                        }

                        pathSegments.Add(new LineSegment(new Point(lineEnd, Y1), true));
                        pathSegments.Add(new LineSegment(new Point(lineEnd, Y1 + 0.5 * this.StrokeThickness), true));
                        pathSegments.Add(new LineSegment(new Point(arrowEnd, Y1), true));
                        pathSegments.Add(new LineSegment(new Point(lineEnd, Y1 - 0.5 * this.StrokeThickness), true));
                        pathSegments.Add(new LineSegment(new Point(lineEnd, Y1), true));
                        pathSegments.Add(new LineSegment(new Point(X2, Y1), false));
                    }

                }

                //second line
                var y_Difference = Y2 - Y1;
                if (!IsArrowOnSecondLineEnd || y_Difference == 0)
                    pathSegments.Add(new LineSegment(new Point(X2, Y2), true));
                else
                {
                    if (Math.Abs(y_Difference) > arrowOffset)
                    {
                        double lineEnd = 0, arrowEnd = 0;

                        if (y_Difference > 0) // strzałka w górę
                        {
                            lineEnd = Y2 - 2 * this.StrokeThickness;
                            arrowEnd = Y2 - this.StrokeThickness;
                        }
                        else if (y_Difference < 0) // strzałka w dół
                        {
                            lineEnd = Y2 + 2 * this.StrokeThickness;
                            arrowEnd = Y2 + this.StrokeThickness;
                        }

                        pathSegments.Add(new LineSegment(new Point(X2, lineEnd), true));
                        pathSegments.Add(new LineSegment(new Point(X2 + 0.5 * this.StrokeThickness, lineEnd), true));
                        pathSegments.Add(new LineSegment(new Point(X2, arrowEnd), true));
                        pathSegments.Add(new LineSegment(new Point(X2 - 0.5 * this.StrokeThickness, lineEnd), true));
                        pathSegments.Add(new LineSegment(new Point(X2, lineEnd), true));
                    }
                }

            }

            PathFigure pathFigure = new PathFigure(new Point(X1, Y1), pathSegments, false);
            PathGeometry pathGeometry = new PathGeometry();
            pathGeometry.Figures.Add(pathFigure);

            return pathGeometry;

        }
    }


}
