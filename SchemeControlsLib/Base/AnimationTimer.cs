﻿using SchemeControlsLib.Controls.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Threading;

namespace SchemeControlsLib.Base
{
    public class AnimationTimer
    {
        private bool _Debug = true;

        private Dispatcher _dispatcher = Dispatcher.CurrentDispatcher;

        private Timer _animationTimer;
        private string _parentName;
        private Action _onTimerElapsed;

        public void Animate(int animationInterval, Action timerElapsedAction, string parentName)
        {
            if (_animationTimer == null && timerElapsedAction != null)
            {
                _onTimerElapsed = timerElapsedAction;
                _parentName = parentName;
                _animationTimer = new Timer(animationInterval);
                _animationTimer.Elapsed += _animationTimer_Elapsed;
                _animationTimer.AutoReset = true;
                _animationTimer.Start();
                CWDebugMessage("Start animacji : [" + this._parentName + "]");
            }
            else if (_animationTimer != null)
            {
                this.CleanTimer();
            }
        }

        private void _animationTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                _dispatcher?.Invoke(() =>
                {
                    if (_onTimerElapsed != null)
                    {
                        this._onTimerElapsed();
                    }

                });
            }
            catch { }
        }


        public void CleanTimer()
        {
            if (_animationTimer != null)
            {
                _animationTimer.Stop();
                _animationTimer.Elapsed -= _animationTimer_Elapsed;
                _animationTimer.Dispose();
                _animationTimer = null;

                CWDebugMessage("STOP animacji : [" + this._parentName + "]");
            }
        }

        private void CWDebugMessage(string message)
        {
            if (_Debug)
                Console.WriteLine(message);
        }
    }
}
