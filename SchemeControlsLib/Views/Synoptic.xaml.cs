﻿using SchemeControlsLib.Controls.Events;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SchemeControlsLib.Views
{
    /// <summary>
    /// Interaction logic for Synoptic.xaml
    /// </summary>
    public partial class Synoptic : UserControl
    {

        public event EventHandler<VarsBindingsDoneEventArgs> VarsBindingsDone;

        public SynopticViewModel SynopticViewModel;

        protected virtual void OnVarsBindingsDone(VarsBindingsDoneEventArgs e)
        {
            EventHandler<VarsBindingsDoneEventArgs> handler = this.VarsBindingsDone;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public Synoptic()
        {

            SynopticViewModel = new SynopticViewModel();
            DataContext = SynopticViewModel;

            InitializeComponent();
        }

        private void _Canvas_VarsBindingsDone(object sender, Controls.Events.VarsBindingsDoneEventArgs e)
        {
            this.OnVarsBindingsDone(e);

        }

        public void SetVars(Dictionary<string, object> vars)
        {
            if (SynopticViewModel != null)
                SynopticViewModel.Vars = vars;
        }
    }
}
