﻿using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SchemeControlsLib.Views
{
    public class SynopticViewModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        private Dictionary<string, object> _Vars = new Dictionary<string, object>();
        public Dictionary<string, object> Vars
        {
            get { return _Vars; }
            set
            {
                _Vars = value; NotifyPropertyChanged("Vars");
            }
        }

        private string _SchemePath = @"C:\Users\admin\Desktop\Logi\Schemes\Borynia_Main_Scheme";
        public string SchemePath
        {
            get { return _SchemePath; }
            set { _SchemePath = value; NotifyPropertyChanged("SchemePath"); }
        }

    }
}
