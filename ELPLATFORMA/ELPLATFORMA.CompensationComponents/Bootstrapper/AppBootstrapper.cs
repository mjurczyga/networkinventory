﻿using Autofac;
using CommonServiceLocator;
using ELPLATFORMA.CompensationComponents.Attributes;
using ELPLATFORMA.CompensationComponents.Services;
using ELPLATFORMA.CompensationComponents.Services.RegionRegistrator;
using ELPLATFORMA.CompensationComponents.ViewModels;
using ELPLATFORMA.CompensationComponents.ViewModels.EachDevice.Devices;
using ELPLATFORMA.CompensationComponents.Views;
using SchemeControlsLib.Controls;
using SchemeControlsLib.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ELPLATFORMA.CompensationComponents.Bootstrapper
{
    public class AppBootstrapper
    {
        public static bool IsBootstrapperLoaded {get; set; } = false;

        private IContainer _container;

        public AppBootstrapper()
        {
            Settings.IsITSApp = true;

            var containerBuilder = new ContainerBuilder();

            //RegisterTypesByAttribute(containerBuilder);

            ConfigureContainer(containerBuilder);

            _container = containerBuilder.Build();

           //RegionControlRegistrator.SetContainer(_container);

            ServiceLocator.SetLocatorProvider(() => new Autofac.Extras.CommonServiceLocator.AutofacServiceLocator(_container));
        }

        private void RegisterTypesByAttribute(ContainerBuilder containerBuilder)
        {
            var typesToRegister = LoadTypesToRegister<ContainerRegistration>()?.ToArray();
            if (typesToRegister != null)
            {
                foreach (var item in typesToRegister)
                {
                    if (item.Value.RegisterAs != null)
                    {
                        if (item.Value.SingleInstance)
                        {
                            containerBuilder.RegisterType(item.Key).As(item.Value.RegisterAs).SingleInstance();
                        }
                        else
                        {
                            containerBuilder.RegisterType(item.Key).As(item.Value.RegisterAs);
                        }
                    }
                    else
                    {
                        if (item.Value.SingleInstance)
                        {
                            containerBuilder.RegisterType(item.Key).AsSelf().SingleInstance();
                        }
                        else
                        {
                            containerBuilder.RegisterType(item.Key).AsSelf();
                        }
                    }
                }
            }
        }

        public void Start()
        {
            var dtf = _container.Resolve<DataTemplateFactory>();
            dtf.InitMapping();
            IsBootstrapperLoaded = true;
        }

        private void ConfigureContainer(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterType<SchemeService>().As<ISchemeService>().SingleInstance();
            containerBuilder.RegisterType<EvoVarsService>().As<IEvoVarsService>().SingleInstance();
            //containerBuilder.RegisterType<RegionManager>().As<IRegionManager>().SingleInstance();
            containerBuilder.RegisterType<DataTemplateFactory>().SingleInstance();

            containerBuilder.RegisterType<SynopticViewModel>();


            //containerBuilder.RegisterType<EcoMuz2ViewModel>();
            //containerBuilder.RegisterType<PECAViewModel>();
            //containerBuilder.RegisterType<DeviceFieldViewModel>();
            //containerBuilder.RegisterType<MultiDeviceFieldViewModel>();

        }

        private static List<Assembly> GetListOfEntryAssemblyWithReferences()
        {
            List<Assembly> listOfAssemblies = new List<Assembly>();
            var mainAsm = Assembly.GetEntryAssembly();
            if (mainAsm != null)
            {
                listOfAssemblies.Add(mainAsm);

                foreach (var refAsmName in mainAsm.GetReferencedAssemblies())
                {
                    listOfAssemblies.Add(Assembly.Load(refAsmName));
                }
                return listOfAssemblies;
            }
            return null;
        }

        private static Dictionary<Type, T> LoadTypesToRegister<T>() where T : Attribute
        {
            var types = new List<Type>();
            var result = new Dictionary<Type, T>();

            var assemblyes = GetListOfEntryAssemblyWithReferences();
            if (assemblyes != null)
            {
                foreach (var item in assemblyes)
                {
                    types.AddRange(item.GetTypes());
                }

                foreach (Type type in types)
                {
                    foreach (Attribute attribute in type.GetCustomAttributes(typeof(T), true))
                        result.Add(type, (T)attribute);
                }


                return result;
            }
            return null;
        }
    }
}
