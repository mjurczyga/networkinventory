﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ELPLATFORMA.CompensationComponents.Controls
{
    public static class ControlsSettings
    {
        public static Brush OpenBrush { get { return Brushes.DarkGreen; } }
        public static Brush CloseBrush { get { return Brushes.DarkRed; } }
        public static Brush InvalidBrush { get { return (SolidColorBrush)new BrushConverter().ConvertFromString("#FFFFCC00"); } }
        public static Brush DisabledBrush { get { return Brushes.LightGray; } }
        public static Brush AlarmBrush { get { return new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFE41400")); } }
        public static Brush WarningBrush { get { return new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFCC00")); } }
    }
}
