﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ELPLATFORMA.CompensationComponents.Controls
{
    public class LoadingCircle : NotifyControl
    {
        static LoadingCircle()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(LoadingCircle), new FrameworkPropertyMetadata(typeof(LoadingCircle)));
        }
    }
}
