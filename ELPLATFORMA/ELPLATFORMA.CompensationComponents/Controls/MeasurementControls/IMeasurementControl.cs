﻿using ELPLATFORMA.CompensationComponents.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELPLATFORMA.CompensationComponents.Controls
{
    public interface IMeasurementControl
    {
        object BaseVar { get; set; }

        EvoVarModel BaseVarInternal { get; set; }
    }
}
