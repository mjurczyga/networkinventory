﻿using ELPLATFORMA.CompensationComponents.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Threading;

namespace ELPLATFORMA.CompensationComponents.Controls
{
    public abstract class BaseMeasureControl : NotifyControl, IMeasurementControl
    {
        private readonly Dispatcher _dispatcher = Dispatcher.CurrentDispatcher;

        #region BaseVar
        public object BaseVar
        {
            get { return (object)GetValue(BaseVarProperty); }
            set { SetValue(BaseVarProperty, value); }
        }

        public static readonly DependencyProperty BaseVarProperty =
            DependencyProperty.Register("BaseVar", typeof(object), typeof(BaseMeasureControl), new PropertyMetadata(null, BaseVar_PropertyChanged));

        private static void BaseVar_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is EvoVarModel baseVar)
            {
                BaseMeasureControl synoptykaPopupMeasurement = (BaseMeasureControl)d;
                synoptykaPopupMeasurement.BaseVarInternal = baseVar;
                synoptykaPopupMeasurement.SetBaseVarBindings(baseVar);
            }
        }

        private void SetBaseVarBindings(EvoVarModel baseVar)
        {
            if (baseVar != null)
            {
                this.SetBinding(BaseMeasureControl.DescriptionProperty, GetBinding(this, "BaseVar.Description"));
                this.SetBinding(BaseMeasureControl.UnitProperty, GetBinding(this, "BaseVar.Unit"));
                this.SetBinding(BaseMeasureControl.ValueProperty, GetBinding(this, "BaseVar.Value"));
            }
        }

        private Binding GetBinding(object bindingSource, string path)
        {
            Binding binding = new Binding(path);
            binding.Source = bindingSource;
            return binding;
        }
        #endregion

        #region var properties


        public Dictionary<object, string> ValueDictionary
        {
            get { return (Dictionary<object, string>)GetValue(ValueDictionaryProperty); }
            set { SetValue(ValueDictionaryProperty, value); }
        }

        public static readonly DependencyProperty ValueDictionaryProperty =
            DependencyProperty.Register("ValueDictionary", typeof(Dictionary<object, string>), typeof(BaseMeasureControl), new PropertyMetadata(null));



        public object Value
        {
            get { return (object)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(object), typeof(BaseMeasureControl), new PropertyMetadata("999", Value_PropertyChanged));

        private static void Value_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is BaseMeasureControl baseMeasureControl)
            {
                if (baseMeasureControl.ValueDictionary == null)
                {
                    if (double.TryParse(e.NewValue?.ToString(), out double dblValue) && (dblValue.ToString().ToUpper() == "NAN" || dblValue > 9999999999999999999.0 || dblValue == double.NaN))
                        baseMeasureControl.InternalValue = "---";
                    else if (e.NewValue?.ToString().ToUpper() == "NAN")
                    {
                        baseMeasureControl.InternalValue = "---";
                    }
                    else
                        baseMeasureControl.InternalValue = e.NewValue;
                }
                else
                {
                    if (int.TryParse(e.NewValue?.ToString(), out int intVal) && baseMeasureControl.ValueDictionary.TryGetValue(intVal, out string valueDescription))
                        baseMeasureControl.InternalValue = valueDescription;
                    else if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && baseMeasureControl.ValueDictionary.TryGetValue(boolVal, out string boolValueDescription))
                        baseMeasureControl.InternalValue = boolValueDescription;
                    else
                        baseMeasureControl.InternalValue = e.NewValue;
                }
            }
        }

        private object _InternalValue;
        public object InternalValue
        {
            get { return _InternalValue; }
            set { _InternalValue = value; NotifyPropertyChanged("InternalValue"); }
        }

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(BaseMeasureControl), new PropertyMetadata("Brak danych"));

        public string Unit
        {
            get { return (string)GetValue(UnitProperty); }
            set { SetValue(UnitProperty, value); }
        }

        public static readonly DependencyProperty UnitProperty =
            DependencyProperty.Register("Unit", typeof(string), typeof(BaseMeasureControl), new PropertyMetadata(null, Unit_PropertyChanged));

        private static void Unit_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            BaseMeasureControl synoptykaPopupMeasurement = (BaseMeasureControl)d;
            synoptykaPopupMeasurement.UnitFormat(e.NewValue?.ToString());
        }

        private string _InternalUnit = "";
        public string InternalUnit
        {
            get { return _InternalUnit; }
            set { _InternalUnit = value; NotifyPropertyChanged("InternalUnit"); }
        }

        private EvoVarModel _BaseVarInternal;
        public EvoVarModel BaseVarInternal
        {
            get { return _BaseVarInternal; }
            set { _BaseVarInternal = value; NotifyPropertyChanged("BaseVarInternal"); }
        }
        #endregion

        protected virtual void UnitFormat(string unit)
        {
            if (!string.IsNullOrEmpty(unit))
            {
                if (!unit.Contains("["))
                    unit = "[" + unit;
                if (!unit.Contains("]"))
                    unit = unit + "]";
                this.InternalUnit = unit;
            }

            else
                this.InternalUnit = "";
        }
    }
}
