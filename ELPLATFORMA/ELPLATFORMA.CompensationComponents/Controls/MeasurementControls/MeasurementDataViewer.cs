﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace ELPLATFORMA.CompensationComponents.Controls
{
    public class MeasurementDataViewer : BaseMeasureControl
    {
        #region Dependency commands
        public ICommand ClickCommand
        {
            get { return (ICommand)GetValue(ClickCommandProperty); }
            set { SetValue(ClickCommandProperty, value); }
        }

        public static readonly DependencyProperty ClickCommandProperty =
            DependencyProperty.Register("ClickCommand", typeof(ICommand), typeof(MeasurementDataViewer), new PropertyMetadata(ClickCommand_PropertyChanged));

        private static void ClickCommand_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MeasurementDataViewer measurementDataViewer = (MeasurementDataViewer)d;
            if (e.NewValue != null)
                measurementDataViewer.Cursor = Cursors.Hand;
            else
                measurementDataViewer.Cursor = Cursors.Arrow;
        }

        public object ClickCommandParameter
        {
            get { return (object)GetValue(ClickCommandParameterProperty); }
            set { SetValue(ClickCommandParameterProperty, value); }
        }

        public static readonly DependencyProperty ClickCommandParameterProperty =
            DependencyProperty.Register("ClickCommandParameter", typeof(object), typeof(MeasurementDataViewer));

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            if (this.ClickCommand != null)
                this.ClickCommand.Execute(this.ClickCommandParameter);
        }
        #endregion

        #region IsLimitExceeded (BOOL)
        public object IsLimitExceeded
        {
            get { return (object)GetValue(IsLimitExceededProperty); }
            set { SetValue(IsLimitExceededProperty, value); }
        }

        public static readonly DependencyProperty IsLimitExceededProperty =
            DependencyProperty.Register("IsLimitExceeded", typeof(object), typeof(MeasurementDataViewer), new PropertyMetadata(false, IsLimitExceeded_PropertyChanged));

        private static void IsLimitExceeded_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MeasurementDataViewer measurementDataViewer = (MeasurementDataViewer)d;
            if (e.NewValue == null)
                measurementDataViewer.InternalIsLimitExceeded = false;
            else if (bool.TryParse(e.NewValue?.ToString(), out bool result))
                measurementDataViewer.InternalIsLimitExceeded = result;
        }

        private bool _InternalIsLimitExceeded;
        public bool InternalIsLimitExceeded
        {
            get { return _InternalIsLimitExceeded; }
            private set { _InternalIsLimitExceeded = value; UpdateInternalBackground(_InternalIsLimitExceeded); NotifyPropertyChanged("InternalIsLimitExceeded"); }
        }

        private Brush _InternalBackground;
        public Brush InternalBackground
        {
            get { return _InternalBackground; }
            set { _InternalBackground = value; NotifyPropertyChanged("InternalBackground"); }
        }

        private void UpdateInternalBackground(bool isLimitExceeded)
        {
            if (isLimitExceeded)
                InternalBackground = ControlsSettings.AlarmBrush;
            else
                InternalBackground = this.Background;
        }
        #endregion

        #region style properties
        public Brush ControlBackground
        {
            get { return (Brush)GetValue(ControlBackgroundProperty); }
            set { SetValue(ControlBackgroundProperty, value); }
        }

        public static readonly DependencyProperty ControlBackgroundProperty =
            DependencyProperty.Register("ControlBackground", typeof(Brush), typeof(MeasurementDataViewer), new PropertyMetadata(Brushes.Transparent, ControlBackground_PropertyChanged));

        private static void ControlBackground_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MeasurementDataViewer measurementDataViewer = (MeasurementDataViewer)d;
            if (!measurementDataViewer.InternalIsLimitExceeded)
            {
                measurementDataViewer.InternalBackground = (Brush)e.NewValue;
            }
        }

        [TypeConverter(typeof(FontSizeConverter))]
        public double ValueFontSize
        {
            get { return (double)GetValue(ValueFontSizeProperty); }
            set { SetValue(ValueFontSizeProperty, value); }
        }

        public static readonly DependencyProperty ValueFontSizeProperty =
            DependencyProperty.Register("ValueFontSize", typeof(double), typeof(MeasurementDataViewer), new PropertyMetadata(27.0));


        public Visibility IconVisibility
        {
            get { return (Visibility)GetValue(IconVisibilityProperty); }
            set { SetValue(IconVisibilityProperty, value); }
        }

        public static readonly DependencyProperty IconVisibilityProperty =
            DependencyProperty.Register("IconVisibility", typeof(Visibility), typeof(MeasurementDataViewer), new PropertyMetadata(Visibility.Collapsed));



        public DrawingBrush IconDrawingBrush
        {
            get { return (DrawingBrush)GetValue(IconDrawingBrushProperty); }
            set { SetValue(IconDrawingBrushProperty, value); }
        }

        public static readonly DependencyProperty IconDrawingBrushProperty =
            DependencyProperty.Register("IconDrawingBrush", typeof(DrawingBrush), typeof(MeasurementDataViewer), new PropertyMetadata(null));


        #endregion

        #region Value
        private void UpdateInternalBackground(double newValue)
        {
            if (AlarmValue != 0 && newValue > AlarmValue)
                InternalBackground = ControlsSettings.AlarmBrush;
            else if (WarrningValue != 0 && newValue > WarrningValue)
                InternalBackground = ControlsSettings.WarningBrush;
            else
                InternalBackground = this.Background;
        }

        private void Value_PropertyChanged(object sender, EventArgs e)
        {
            if (double.TryParse(Value?.ToString(), out double dblValue))
            {
                this.UpdateInternalBackground(dblValue);
            }
        }

        #region alarms value properties
        public double AlarmValue
        {
            get { return (double)GetValue(AlarmValueProperty); }
            set { SetValue(AlarmValueProperty, value); }
        }

        public static readonly DependencyProperty AlarmValueProperty =
            DependencyProperty.Register("AlarmValue", typeof(double), typeof(MeasurementDataViewer), new PropertyMetadata(0.0));

        public double WarrningValue
        {
            get { return (double)GetValue(WarrningValueProperty); }
            set { SetValue(WarrningValueProperty, value); }
        }

        public static readonly DependencyProperty WarrningValueProperty =
            DependencyProperty.Register("WarrningValue", typeof(double), typeof(MeasurementDataViewer), new PropertyMetadata(0.0));


        public Thickness MeasurementValueTextPadding
        {
            get { return (Thickness)GetValue(MeasurementValueTextPaddingProperty); }
            set { SetValue(MeasurementValueTextPaddingProperty, value); }
        }

        public static readonly DependencyProperty MeasurementValueTextPaddingProperty =
            DependencyProperty.Register("MeasurementValueTextPadding", typeof(Thickness), typeof(MeasurementDataViewer), new PropertyMetadata(new Thickness(5, 0, 5, 5)));

        #endregion
        #endregion

        #region Description
        public Thickness DescriptionMargin
        {
            get { return (Thickness)GetValue(DescriptionMarginProperty); }
            set { SetValue(DescriptionMarginProperty, value); }
        }
        public static readonly DependencyProperty DescriptionMarginProperty =
            DependencyProperty.Register("DescriptionMargin", typeof(Thickness), typeof(MeasurementDataViewer), new PropertyMetadata(new Thickness(0, 0, 0, 5)));
        #endregion

        static MeasurementDataViewer()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MeasurementDataViewer), new FrameworkPropertyMetadata(typeof(MeasurementDataViewer)));
        }

        public MeasurementDataViewer()
        {
            DependencyPropertyDescriptor.FromProperty(ValueProperty, typeof(MeasurementDataViewer)).AddValueChanged(this, Value_PropertyChanged);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.UpdateInternalBackground(false);
        }
    }
}
