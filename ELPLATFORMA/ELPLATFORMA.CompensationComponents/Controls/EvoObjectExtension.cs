﻿using Askom.AddonManager;
using ELPLATFORMA.CompensationComponents.Models;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ELPLATFORMA.CompensationComponents.Controls
{
    public class EvoObjectPropertyChangedEventArgs
    {
        private string _PropertyName;
        public string PropertyName
        {
            get { return _PropertyName; }
            set { _PropertyName = value; }
        }

        private object _PropertyValue;
        public object PropertyValue
        {
            get { return _PropertyValue; }
            set { _PropertyValue = value; }
        }

        private Type _PropertyType;
        public Type PropertyType
        {
            get { return _PropertyType; }
            set { _PropertyType = value; }
        }


        public EvoObjectPropertyChangedEventArgs() { }

        public EvoObjectPropertyChangedEventArgs(string propertyName, object propertyValue, Type propertyType)
        {
            PropertyName = propertyName;
            PropertyValue = propertyValue;
            PropertyType = propertyType;
        }
    }

    public delegate void EvoObjectPropertyChangedEventHandler(object sender, EvoObjectPropertyChangedEventArgs evoObjectPropertyChangedEventArgs);

    public class EvoObjectExtension : EvoObjectBase
    {
        //public override bool EvoObjectUsesTransparency => true;

        public event  EvoObjectPropertyChangedEventHandler EvoObjectPropertyChangedEvent;

        protected virtual void OnEvoObjectPropertyChangedEvent(EvoObjectPropertyChangedEventArgs e)
        {
                EvoObjectPropertyChangedEvent?.Invoke(this, e);
        }

        public event VariableStateChangedEvent VarStateChangedEvent;

        [Askom.AddonManager.EvoObjectEvent(Description = "Wybranie elementu")]
        public event Askom.AddonManager.EvoObjectEventHandler ElementClickEvent;

        public EvoObjectExtension()
        {
            VariableStateChangedEvent += EvoObjectExtension_VariableStateChangedEvent;
        }

        private void EvoObjectExtension_VariableStateChangedEvent(string aVariableName, VariableState aVariableState)
        {
            VarStateChangedEvent?.Invoke(aVariableName, aVariableState);
        }

        #region Variable methods
        public bool RegisterVar(string varName)
        {
            return RegisterVariable(varName);


        }

        public void SchemeClick()
        {
            Askom.AddonManager.AsixEvo.Application.ExecuteAction("^OpenDiagram(\"Przycisk\", \"\", \"\", $Normal, $CursorLocation, $CursorLocation, \"\", $None)", out string aaa);
        }

        public void ReportMessageExt(AddonMessageType aType, string aMessage)
        {
            this.ReportMessage(aType, aMessage);
        }

        public void UnregisterAllRegisteredVars()
        {
            UnregisterAllRegisteredVariables();           
        }

        public void UnregisterVar(string aVarName)
        {
            UnregisterVariable(aVarName);
        }

        public object GetVarAttribute(string varName, string attributeName)
        {
            return GetVariableAttribute(varName, attributeName);
        }
        #endregion Variable methods

        public VariableState GetVarState(string aVarName)
        {
            return GetVariableState(aVarName);
        }

        public void OnVarStateChanged(string aVarName, VariableState aState)
        {
            OnVariableStateChanged(aVarName, aState);
        }

        #region Reset disconnect
        public delegate void ResetEventHandler(object sender, EventArgs e);
        public event ResetEventHandler ResetEvent;

        public delegate void DisconnectEventHandler(object sender, EventArgs e);
        public event DisconnectEventHandler DisconnectEvent;

        public override void Reset()
        {
            base.Reset();
            ResetEvent?.Invoke(this, new EventArgs());
            this.ReportMessageExt(AddonMessageType.Info, "[EvoObjectBase] Wykonano 'Reset' obiektu.");
        }

        public override void Disconnect()
        {
            base.Disconnect();
            DisconnectEvent?.Invoke(this, new EventArgs());
            this.ReportMessageExt(AddonMessageType.Info, "[EvoObjectBase] Wykonano 'Disconnect' obiektu.");
            // TODO: Zastanowić się
            //Messenger.Default.Send(new ApplicationClosingMessage());
        }
        #endregion Reset disconnect


    }
}
