﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ELPLATFORMA.CompensationComponents.Controls
{
    public class MultiMuzMeasureControl : BaseMeasureControl
    {
        public string Separator
        {
            get { return (string)GetValue(SeparatorProperty); }
            set { SetValue(SeparatorProperty, value); }
        }

        public static readonly DependencyProperty SeparatorProperty =
            DependencyProperty.Register("Separator", typeof(string), typeof(MultiMuzMeasureControl), new PropertyMetadata(""));

        protected override void UnitFormat(string unit)
        {
            this.InternalUnit = " " + unit;
        }

        static MultiMuzMeasureControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MultiMuzMeasureControl), new FrameworkPropertyMetadata(typeof(MultiMuzMeasureControl)));
        }
    }
}
