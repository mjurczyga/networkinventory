﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace ELPLATFORMA.CompensationComponents.Controls
{
    public class MultiMuzAlarmControl : NotifyControl
    {
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(MultiMuzAlarmControl), new PropertyMetadata(""));

        #region Alarm value
        public object Value
        {
            get { return (object)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(object), typeof(MultiMuzAlarmControl), new PropertyMetadata(null, Value_PropertyChanged));

        private static void Value_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MultiMuzAlarmControl multiMuzAlarmControl = (MultiMuzAlarmControl)d;

            if (bool.TryParse(e.NewValue?.ToString(), out bool boolValue))
            {
                multiMuzAlarmControl.SetDiodeBrush(boolValue);
            }

        }

        private void SetDiodeBrush(bool boolValue)
        {
            if (boolValue)
                InternalDiodeBrush = (SolidColorBrush)new BrushConverter().ConvertFromString("#FFE41400");
            else
                InternalDiodeBrush = Brushes.Green;
        }

        private Brush _InternalDiodeBrush;
        public Brush InternalDiodeBrush
        {
            get { return _InternalDiodeBrush; }
            set { _InternalDiodeBrush = value; NotifyPropertyChanged("InternalDiodeBrush"); }
        }

        #endregion

        public GridLength DoideWidth
        {
            get { return (GridLength)GetValue(DoideWidthProperty); }
            set { SetValue(DoideWidthProperty, value); }
        }

        public static readonly DependencyProperty DoideWidthProperty =
            DependencyProperty.Register("DoideWidth", typeof(GridLength), typeof(MultiMuzAlarmControl), new PropertyMetadata(new GridLength(1, GridUnitType.Auto)));

        public Thickness DiodeMargin
        {
            get { return (Thickness)GetValue(DiodeMarginProperty); }
            set { SetValue(DiodeMarginProperty, value); }
        }

        public static readonly DependencyProperty DiodeMarginProperty =
            DependencyProperty.Register("DiodeMargin", typeof(Thickness), typeof(MultiMuzAlarmControl), new PropertyMetadata(default(Thickness)));

        public Thickness TextMargin
        {
            get { return (Thickness)GetValue(TextMarginProperty); }
            set { SetValue(TextMarginProperty, value); }
        }

        public static readonly DependencyProperty TextMarginProperty =
            DependencyProperty.Register("TextMargin", typeof(Thickness), typeof(MultiMuzAlarmControl), new PropertyMetadata(default(Thickness)));

        public Thickness TextPadding
        {
            get { return (Thickness)GetValue(TextPaddingProperty); }
            set { SetValue(TextPaddingProperty, value); }
        }

        public static readonly DependencyProperty TextPaddingProperty =
            DependencyProperty.Register("TextPadding", typeof(Thickness), typeof(MultiMuzAlarmControl), new PropertyMetadata(default(Thickness)));

        public Brush TextBorderBrush
        {
            get { return (Brush)GetValue(TextBorderBrushProperty); }
            set { SetValue(TextBorderBrushProperty, value); }
        }

        public static readonly DependencyProperty TextBorderBrushProperty =
            DependencyProperty.Register("TextBorderBrush", typeof(Brush), typeof(MultiMuzAlarmControl), new PropertyMetadata(null));

        static MultiMuzAlarmControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MultiMuzAlarmControl), new FrameworkPropertyMetadata(typeof(MultiMuzAlarmControl)));
        }

    }
}
