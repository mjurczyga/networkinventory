﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace ELPLATFORMA.CompensationComponents.Controls
{
    public class IOControl : NotifyControl
    {
        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }
        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(IOControl));

        #region Value
        public object Value
        {
            get { return (object)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(object), typeof(IOControl), new PropertyMetadata(null, Value_PropertyChanged));

        private static void Value_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is IOControl iOControl)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal))
                {
                    iOControl.InternalLedValue = boolVal;
                }
                else
                    iOControl.InternalLedValue = false;

            }
        }

        private bool _InternalLedValue;
        public bool InternalLedValue
        {
            get { return _InternalLedValue; }
            private set { _InternalLedValue = value; SetIOBackground(value); NotifyPropertyChanged("InternalLedValue"); }
        }
        #endregion

        private Brush _InternalBackground;
        public Brush InternalBackground
        {
            get { return _InternalBackground; }
            set { _InternalBackground = value; NotifyPropertyChanged("InternalBackground"); }
        }

        private void SetIOBackground(bool value)
        {
            if (value)
                InternalBackground = ControlsSettings.AlarmBrush;
            else
                InternalBackground = Brushes.Green;
        }

        public IOControl()
        {
            this.SetIOBackground(false);
        }

        static IOControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(IOControl), new FrameworkPropertyMetadata(typeof(IOControl)));
        }

    }
}
