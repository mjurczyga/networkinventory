﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace ELPLATFORMA.CompensationComponents.Controls
{
    public class SectionBox : NotifyControl
    {
        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("Title", typeof(string), typeof(SectionBox), new PropertyMetadata(null, Title_PropertyChanged));

        private static void Title_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SectionBox sectionBox = (SectionBox)d;
            if (string.IsNullOrEmpty(e.NewValue?.ToString()))
            {
                sectionBox.TitleVisibility = Visibility.Collapsed;
            }
            else
                sectionBox.TitleVisibility = Visibility.Visible;
        }

        public Visibility TitleVisibility { get; private set; } = Visibility.Collapsed;

        public object Content
        {
            get { return (object)GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }

        public static readonly DependencyProperty ContentProperty =
            DependencyProperty.Register("Content", typeof(object), typeof(SectionBox), new PropertyMetadata(""));

        public Thickness ContentMargin
        {
            get { return (Thickness)GetValue(ContentMarginProperty); }
            set { SetValue(ContentMarginProperty, value); }
        }

        public static readonly DependencyProperty ContentMarginProperty =
            DependencyProperty.Register("ContentMargin", typeof(Thickness), typeof(SectionBox));

        public bool IsMenuElement
        {
            get { return (bool)GetValue(IsMenuElementProperty); }
            set { SetValue(IsMenuElementProperty, value); }
        }

        public static readonly DependencyProperty IsMenuElementProperty =
            DependencyProperty.Register("IsMenuElement", typeof(bool), typeof(SectionBox), new PropertyMetadata(IsMenuElement_PropertyChanged));

        private static void IsMenuElement_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SectionBox sb)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool resultBool))
                {
                    if (resultBool)
                        sb.IconVisibility = Visibility.Visible;
                    else
                        sb.IconVisibility = Visibility.Collapsed;
                }
            }
        }

        public Brush AccentBrush
        {
            get { return (Brush)GetValue(AccentBrushProperty); }
            set { SetValue(AccentBrushProperty, value); }
        }

        public static readonly DependencyProperty AccentBrushProperty =
            DependencyProperty.Register("AccentBrush", typeof(Brush), typeof(SectionBox));

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(SectionBox), new PropertyMetadata(null, Command_PropertyChanged));

        public object CommandParameter
        {
            get { return (object)GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.Register("CommandParameter", typeof(object), typeof(SectionBox), new PropertyMetadata(null));

        private static void Command_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SectionBox sectionBox = (SectionBox)d;
            if (e.NewValue != null)
                sectionBox.Cursor = Cursors.Hand;
            else
                sectionBox.Cursor = Cursors.Arrow;
        }

        public bool MouseOverBackground
        {
            get { return (bool)GetValue(MouseOverBackgroundProperty); }
            set { SetValue(MouseOverBackgroundProperty, value); }
        }

        public static readonly DependencyProperty MouseOverBackgroundProperty =
            DependencyProperty.Register("MouseOverBackground", typeof(bool), typeof(SectionBox), new PropertyMetadata(false));

        private bool _InternalIsMouseOver;

        public bool InternalIsMouseOver
        {
            get { return _InternalIsMouseOver; }
            set { _InternalIsMouseOver = value; NotifyPropertyChanged("InternalIsMouseOver"); }
        }


        static SectionBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SectionBox), new FrameworkPropertyMetadata(typeof(SectionBox)));
        }

        public SectionBox()
        {
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.IconBrush = this.Foreground;
        }

        private void SectionBox_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            Command?.Execute(CommandParameter);
        }

        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            if (MouseOverBackground)
                InternalIsMouseOver = true;
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            InternalIsMouseOver = false;
        }


        public bool IsItemSelected { get; private set; }

        private Visibility _IconVisibility = Visibility.Collapsed;
        public Visibility IconVisibility
        {
            get { return _IconVisibility; }
            set { _IconVisibility = value; NotifyPropertyChanged("IconVisibility"); }
        }

        private Brush _IconBrush;

        public Brush IconBrush
        {
            get { return _IconBrush; }
            set { _IconBrush = value; NotifyPropertyChanged("IconBrush"); }
        }

        private Visibility _ContentVisibility = Visibility.Visible;
        public Visibility ContentVisibility
        {
            get { return _ContentVisibility; }
            set { _ContentVisibility = value; NotifyPropertyChanged("ContentVisibility"); }
        }

        public void CollapseContent()
        {
            IsItemSelected = false;
            ContentVisibility = Visibility.Collapsed;
            IconBrush = this.Foreground;
        }

        public void ShowContent()
        {
            IsItemSelected = true;
            ContentVisibility = Visibility.Visible;
            IconBrush = this.AccentBrush;

        }
    }
}
