﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace ELPLATFORMA.CompensationComponents.Controls
{
    public class SecureButton : NotifyControl
    {
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(SecureButton), new PropertyMetadata(null));



        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(SecureButton));

        public Thickness CornerRadius
        {
            get { return (Thickness)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(Thickness), typeof(SecureButton));


        public Brush AccentBrush
        {
            get { return (Brush)GetValue(AccentBrushProperty); }
            set { SetValue(AccentBrushProperty, value); }
        }

        public static readonly DependencyProperty AccentBrushProperty =
            DependencyProperty.Register("AccentBrush", typeof(Brush), typeof(SecureButton), new PropertyMetadata(AccentBrush_PropertyChanged));

        private static void AccentBrush_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SecureButton secureButton && secureButton._IsSelected && e.NewValue != null)
            {
                secureButton.InternalBackground = (Brush)e.NewValue;
            }
        }

        private Brush _InternalBackground;
        public Brush InternalBackground
        {
            get { return _InternalBackground; }
            set { _InternalBackground = value; NotifyPropertyChanged("InternalBackground"); }
        }

        public object TabContent
        {
            get { return (object)GetValue(TabContentProperty); }
            set { SetValue(TabContentProperty, value); }
        }

        public static readonly DependencyProperty TabContentProperty =
            DependencyProperty.Register("TabContent", typeof(object), typeof(SecureButton));


        #region Secure value
        public object SecureValue
        {
            get { return (object)GetValue(SecureValueProperty); }
            set { SetValue(SecureValueProperty, value); }
        }

        public static readonly DependencyProperty SecureValueProperty =
            DependencyProperty.Register("SecureValue", typeof(object), typeof(SecureButton), new PropertyMetadata(null, SecureValue_PropertyChanged));

        private static void SecureValue_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SecureButton secureButton)
            {
                if (int.TryParse(e.NewValue?.ToString(), out int intValue))
                    secureButton.InternalSecureValue = intValue;
                else
                    secureButton.InternalSecureValue = 0;
            }

        }

        private int _InternalSecureValue;
        public int InternalSecureValue
        {
            get { return _InternalSecureValue; }
            set
            {
                _InternalSecureValue = value;
                ButtonIcon = this.SelectIcon(value);
                NotifyPropertyChanged("InternalSecureValue");
            }
        }
        #endregion Secure value

        #region Icon
        private FrameworkElement _ButtonIcon;
        public FrameworkElement ButtonIcon
        {
            get { return _ButtonIcon; }
            set { _ButtonIcon = value; NotifyPropertyChanged("ButtonIcon"); }
        }

        private FrameworkElement SelectIcon(int value)
        {
            return null;
            //if (value == 0)
            //    return IconResources.GetIconElementByKey("StatusOK");
            //else
            //    return IconResources.GetIconElementByKey("StatusCriticalError");

        }

        #endregion

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            if (Command != null)
                Command.Execute(this);
        }

        private bool _IsSelected = false;

        public void SelectElement()
        {
            InternalBackground = AccentBrush;
            _IsSelected = true;
        }

        public void UnselectElement()
        {
            InternalBackground = this.Background;
            _IsSelected = false;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            if (!_IsSelected)
                this.UnselectElement();
        }

        public SecureButton()
        {
            ButtonIcon = this.SelectIcon(0);
        }

        static SecureButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SecureButton), new FrameworkPropertyMetadata(typeof(SecureButton)));
        }
    }
}
