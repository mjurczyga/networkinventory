﻿using Autofac;
using ELPLATFORMA.CompensationComponents.Attributes;
using ELPLATFORMA.CompensationComponents.Models;
using ELPLATFORMA.CompensationComponents.Services;
using ELPLATFORMA.CompensationComponents.ViewModels;
using ELPLATFORMA.CompensationComponents.ViewModels.EachDevice.Devices;
using GalaSoft.MvvmLight.Command;
using SchemeControlsLib.Controls.CommonControls;
using SchemeControlsLib.Controls.Events;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Services;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace ELPLATFORMA.CompensationComponents.ViewModels
{
    //[AutoViewMapper("SynopticView")]
    public class SynopticViewModel : BaseViewModel
    {
        private readonly IComponentContext _componentContext;
        private readonly IEvoVarsService _evoVarsService;
        private readonly ISchemeService _schemeService;

        public ICommand FieldClickCommand { get; private set; }
        public ICommand ButtonMouseOverCommand { get; private set; }
        public ICommand VarsBindingsDoneCommad { get; private set; }
        public ICommand TestCommand { get; private set; }

        private Dictionary<string, EvoVarModel> _Vars;
        public Dictionary<string, EvoVarModel> Vars
        {
            get { return _Vars; }
            protected set { _Vars = value; }
        }

        private Brush _BackgroundBrush = Brushes.Black;
        public Brush BackgroundBrush
        {
            get { return _BackgroundBrush; }
            set { _BackgroundBrush = value; }
        }


        public SynopticViewModel(IComponentContext componentContext, IEvoVarsService evoVarsService, ISchemeService schemeService)
        {
            _componentContext = componentContext;
            _evoVarsService = evoVarsService;
            _schemeService = schemeService;

            _evoVarsService.EvoObjectPropertyChangedEvent += _evoVarsService_EvoObjectPropertyChangedEvent;

            VarsBindingsDoneCommad = new RelayCommand<VarsBindingsDoneEventArgs>((args) => OnVarsBindingsDone(args));
            TestCommand = new RelayCommand(OnTestCommand);
            FieldClickCommand = new RelayCommand<object>(OnFieldClickCommand);
        }

        private void OnFieldClickCommand(object obj)
        {
            if (_evoVarsService != null)
            {
                _evoVarsService.ReportMessage(Askom.AddonManager.AddonMessageType.Info, "[SynopticViewModel] wykonanie akcji: " + obj?.ToString());
                if (obj != null && obj.ToString().StartsWith("^"))
                    _evoVarsService.RaiseEvoAction(obj.ToString());
            }
        }

        private void OnTestCommand()
        {
            if(_evoVarsService != null)
            {
                _evoVarsService.TestClick();
            }
        }


        private string _OldSchemePath = null;
        private void _evoVarsService_EvoObjectPropertyChangedEvent(object sender, Controls.EvoObjectPropertyChangedEventArgs evoObjectPropertyChangedEventArgs)
        {
            if (evoObjectPropertyChangedEventArgs.PropertyName == "SchemePath" && _schemeService != null && _schemeService.SchemeCanvas != null && (!Loaded || _evoVarsService != null && _evoVarsService.IsAsixControlEditMode()))
            {
                var filePath = evoObjectPropertyChangedEventArgs.PropertyValue.ToString();
                if (_OldSchemePath != filePath)
                {
                    _OldSchemePath = filePath;
                    _dispatcher.Invoke(() =>
                    {
                        _schemeService.SchemeCanvas.ClearScheme();
                        if (File.Exists(evoObjectPropertyChangedEventArgs.PropertyValue.ToString()))
                        {

                            _schemeService.SchemeCanvas.LoadAndBind(evoObjectPropertyChangedEventArgs.PropertyValue.ToString());
                            if (_evoVarsService != null)
                                _evoVarsService.ReportMessage(Askom.AddonManager.AddonMessageType.Info, "[SynopticViewModel] Ładowanie schematu ze zdarzenia właściwości");

                        }
                    });
                }
            }
            else if (evoObjectPropertyChangedEventArgs.PropertyName == "BackgroundColor" && evoObjectPropertyChangedEventArgs.PropertyValue is System.Drawing.Color newColor)
            {
                BackgroundBrush = new SolidColorBrush(Color.FromArgb(newColor.A, newColor.R, newColor.G, newColor.B));
            }
        }

        public void OnVarsBindingsDone(VarsBindingsDoneEventArgs obj)
        {
            foreach(var varName in obj.VarsNames)
            {
                _evoVarsService.SubscribeVar(varName);
            }
            Vars = _evoVarsService.Vars;
            RaisePropertyChanged("Vars");


        }
        private bool Loaded = false;

        public void OnCanvasLoaded(SchemeCanvas schemeCanvas)
        {
            if (!Loaded && _schemeService != null && schemeCanvas != null && _evoVarsService != null && !_evoVarsService.IsAsixControlEditMode())
            {

                if(_schemeService.SchemeCanvas == null)
                    _schemeService.SetSchemeCanvas(schemeCanvas); //Z10

                var path = _evoVarsService.GetEvoPropertyValue("SchemePath");

                if (path == null)
                    _evoVarsService.ReportMessage(Askom.AddonManager.AddonMessageType.Error, "[SynopticViewModel] Nie ustawiono właściwości SchemePath");
                else
                {
                    
                        _schemeService.SchemeCanvas.ClearScheme();
                        schemeCanvas.LoadAndBind(_evoVarsService.GetEvoPropertyValue("SchemePath")?.ToString());
                        _evoVarsService.ReportMessage(Askom.AddonManager.AddonMessageType.Info, "[SynopticViewModel] Ładowanie schematu ze zdarzenia OnCanvasLoaded");
                        Loaded = true;
                    
                    
                }
            }
        }

        private void OnButtonMouseOverCommand(object selectedElement)
        {
            //if (selectedElement is SchemeButton schemeButton)
            //{
            //    if (PopupPlacementTarget == null && _MouseEntered)
            //        _MouseEntered = false;

            //    string sourcePrefix = GvlNames.CukSourceName;
            //    if (!string.IsNullOrEmpty(schemeButton.Tag?.ToString()))
            //        sourcePrefix = schemeButton.Tag?.ToString();

            //    var deviceType = schemeButton.DeviceTypeName.Split(';').First();
            //    var varName = schemeButton.VarName.Split(';').First();

            //    if (!_MouseEntered)
            //    {
            //        _MouseEntered = true;
            //        string elementName = "";

            //        PopupPlacementTarget = schemeButton;
            //        elementName = schemeButton.Name;

            //        if (!string.IsNullOrEmpty(varName))
            //        {
            //            var popupContent = _componentContext.Resolve<SimpleMeasurementsViewModel>();

            //            if (deviceType.ToUpper().Contains("MULTIMUZ") || deviceType.ToUpper().Contains("ETANGO") || sourcePrefix.ToUpper() == "KR2")
            //                popupContent.SubscribeViewVars(deviceType, varName + "_Pomiary");
            //            else
            //                popupContent.SubscribeViewVars(deviceType, varName + "_POMIARY");

            //            popupContent.OnLoaded();
            //            PopupContent = popupContent;
            //        }
            //    }

            //    ButtonMouseOver = schemeButton.IsMouseOver;
            //}
        }

    }
}
