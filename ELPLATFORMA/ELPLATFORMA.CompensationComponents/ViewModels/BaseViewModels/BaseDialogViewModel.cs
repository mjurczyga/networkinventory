﻿using ELPLATFORMA.CompensationComponents.Services;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ELPLATFORMA.CompensationComponents.ViewModels.BaseViewModels
{
    public class BaseDialogViewModel : BaseViewModel, IBaseDialogViewModel
    {
        protected readonly IRegionManager _regionManager;

        private string _Title;
        public string Title
        {
            get { return _Title; }
            set { _Title = value; RaisePropertyChanged("Title"); }
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { message = value; RaisePropertyChanged(); }
        }

        public virtual string TargetRegion { get; set; } = "DialogRegion";

        public ICommand CloseCommand { get; set; }

        public BaseDialogViewModel(IRegionManager regionManager)
        {
            _regionManager = regionManager;

            InitCommands();
        }

        private void InitCommands()
        {
            CloseCommand = new RelayCommand<object>(OnCloseCommand);
        }

        protected virtual void OnCloseCommand(object obj)
        {
            _regionManager.CloseRegion(TargetRegion);
            AfterCloseCommand();
        }

        protected virtual void AfterCloseCommand()
        {

        }
    }
}
