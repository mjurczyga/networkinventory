﻿using ELPLATFORMA.CompensationComponents.Attributes;
using ELPLATFORMA.CompensationComponents.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace ELPLATFORMA.CompensationComponents.ViewModels
{
    //[ContainerRegistration]
    public abstract class BaseViewModel : ObservableObject, IBaseViewModel
    {
        protected Dispatcher _dispatcher = Dispatcher.CurrentDispatcher;

        public BaseViewModel()
        {
            Messenger.Default.Register<ApplicationClosingMessage>(this, OnClosingMessage);
        }

        public virtual void OnLoaded(object obj = null)
        {

        }

        protected void OnClosingMessage(ApplicationClosingMessage applicationClosingMessage)
        {
            OnUnloaded();
        }

        public virtual void OnUnloaded(object obj = null)
        {

        }

        protected virtual string GetClassName()
        {
            return this.GetType().Name;
        }

        void IBaseViewModel.StartAction<T>(Action<T> action)
        {
            action(this as T);
        }
    }
}
