﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELPLATFORMA.CompensationComponents.ViewModels.BaseViewModels
{
    public interface IBaseDialogViewModel
    {
        string TargetRegion { get; set; }
    }


    public interface IDialogViewModel
    {
        void InitDialogView(IBaseViewModel dialogContent, string targetRegion = "DialogRegion");
        IBaseViewModel DialogContent { get; }
    }
}
