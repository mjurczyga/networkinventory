﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELPLATFORMA.CompensationComponents.ViewModels
{
    public interface IBaseViewModel
    {
        void OnLoaded(object obj = null);
        void OnUnloaded(object obj = null);
        void StartAction<T>(Action<T> action) where T : class, IBaseViewModel;
    }
}
