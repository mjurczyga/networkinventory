﻿using Autofac;
using ELPLATFORMA.CompensationComponents.Attributes;
using ELPLATFORMA.CompensationComponents.Services;
using ELPLATFORMA.CompensationComponents.ViewModels.BaseViewModels;
using SchemeControlsLib.Controls.CommonControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELPLATFORMA.CompensationComponents.ViewModels.EachDevice.Devices
{
    //[AutoViewMapper("DeviceFieldView")]
    public class DeviceFieldViewModel : BaseDialogViewModel
    {
        public IDeviceViewModel DeviceContent { get; private set; }
        public IComponentContext _componentContext { get; private set; }


        public DeviceFieldViewModel(IRegionManager regionManager, IComponentContext componentContext) : base(regionManager)
        {
            _componentContext = componentContext;
        }

        public void InitView(SchemeButton schemeButton)
        {
            if (schemeButton != null)
            {
                string GvlPrefix = DeviceHelper.GetGvlPrefix(schemeButton?.FieldVarsNamePrefix);

                string deviceVarsPerfix = schemeButton?.VarName;
                string schemeVarsPrefix = GvlPrefix + schemeButton.FieldVarsNamePrefix;
                string schemePath = schemeButton.SchemeFileName;
                string windowTitle = schemeButton.WindowTitle;


                IDeviceViewModel fieldVM = DeviceHelper.GetDeviceViewModel(_componentContext, schemeButton?.DeviceTypeName);

                if(fieldVM != null)
                {
                    fieldVM.InitViewModel(schemeVarsPrefix, deviceVarsPerfix, schemePath, windowTitle);
                    fieldVM.OnLoaded();
                    Title = windowTitle;
                    DeviceContent = fieldVM;
                }

            }
        }

        public override void OnLoaded(object parameter = null)
        {
            base.OnLoaded();
        }

        public override void OnUnloaded(object parameter = null)
        {
            if(DeviceContent != null)
            {
                DeviceContent.OnUnloaded();
            }

            base.OnUnloaded();
        }
    }
}
