﻿using Autofac;
using ELPLATFORMA.CompensationComponents.Services;
using ELPLATFORMA.CompensationComponents.ViewModels.BaseViewModels;
using GalaSoft.MvvmLight.Command;
using SchemeControlsLib.Controls.CommonControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using VISU.Controls;
using VISU.Controls.Interfaces;

namespace ELPLATFORMA.CompensationComponents.ViewModels.EachDevice.Devices
{
    public class MultiDeviceFieldViewModel : BaseDialogViewModel
    {
        private IComponentContext _componentContext;
        private IRegionManager _regionManager;

        public StackPanel DeviceButtons { get; private set; }
        public IDeviceViewModel DeviceContent { get; private set; }


        private IImageButton _SelectedButton = null;

        private ICommand _MouseDownCommand = null;

        public MultiDeviceFieldViewModel(IComponentContext componentContext, IRegionManager regionManager) : base(regionManager)
        {
            _componentContext = componentContext;

            this.InitCommands();
        }

        private void On_MouseDownCommand(object obj)
        {
            if(obj is IImageButton imageButton)
            {
                this.UnselectButton(_SelectedButton);
                this.SelectButton(imageButton);
            }
        }

        private void InitCommands()
        {
            _MouseDownCommand = new RelayCommand<object>(On_MouseDownCommand);
        }

        private void SelectButton(IImageButton imageButton)
        {
            if (imageButton?.MouseDownCommandParameter is IDeviceViewModel deviceViewModel)
            {
                deviceViewModel?.OnLoaded();
                DeviceContent = deviceViewModel;
                imageButton?.SelectElement();
                _SelectedButton = imageButton;
            }
        }

        private void UnselectButton(IImageButton imageButton)
        {
            if (imageButton?.MouseDownCommandParameter is IDeviceViewModel deviceViewModel)
            {
                deviceViewModel?.OnUnloaded();
                imageButton?.UnselectElement();
                _SelectedButton = null;
            }
        }

        public void InitView(SchemeButton schemeButton)
        {
            if(schemeButton != null)
            {
                var deviceTypes = schemeButton?.DeviceTypeName?.Split(';');
                var deviceVars = schemeButton?.VarName?.Split(';');

                DeviceButtons = new StackPanel { Orientation = Orientation.Horizontal, Background = Brushes.Transparent };
                
                string gvlPrefix = DeviceHelper.GetGvlPrefix(schemeButton?.FieldVarsNamePrefix);
                string windowTitle = schemeButton.WindowTitle;
                string schemeVarsPrefix = gvlPrefix + schemeButton.FieldVarsNamePrefix;
                string schemePath = schemeButton.SchemeFileName;

                Title = windowTitle;

                for (int i = 0; i < deviceTypes.Count(); i++)
                {
                    var deviceType = deviceTypes[i];
                    string deviceVar = null;


                    if (deviceVars.Count() > i)
                        deviceVar = deviceVars[i];

                    var deviceButton = new ImageButton() { Text = deviceType };
                                    
                    var vm = DeviceHelper.GetDeviceViewModel(_componentContext, deviceType);//= null;
                    if (vm != null)
                    {
                        vm?.InitViewModel(schemeVarsPrefix, deviceVar, schemePath, windowTitle);
                        deviceButton.MouseDownCommandParameter = vm;//= null;
                        deviceButton.Command = _MouseDownCommand;
                        deviceButton.CommandParameter = deviceButton;
                        DeviceButtons.Children.Add(deviceButton);

                        if (i == 0)
                            _SelectedButton = deviceButton;
                    }
                }
            }
        }

        public override void OnLoaded(object parameter = null)
        {
            base.OnLoaded();

            if (_SelectedButton != null)
                this.SelectButton(_SelectedButton);
        }

        public override void OnUnloaded(object parameter = null)
        {
            if (_SelectedButton != null)
                this.UnselectButton(_SelectedButton);
            base.OnUnloaded();
        }


    }
}
