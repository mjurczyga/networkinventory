﻿using ELPLATFORMA.CompensationComponents.Attributes;
using ELPLATFORMA.CompensationComponents.Controls;
using ELPLATFORMA.CompensationComponents.Converters;
using ELPLATFORMA.CompensationComponents.Models;
using ELPLATFORMA.CompensationComponents.Services;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;

namespace ELPLATFORMA.CompensationComponents.ViewModels.EachDevice.Devices
{
    //[AutoViewMapper("EcoMuz2View")]
    public class EcoMuz2ViewModel : DeviceBaseViewModel
    {
        public readonly IEvoVarsService _evoVarsService;

        public object DeviceMeasurementsVars { get; private set; }
        public object DeviceSecureVars { get; private set; }
        public object DeviceInputVars { get; private set; }
        public object DeviceOutputVars { get; private set; }

        public Visibility MainPanelVisibility { get; private set; } = Visibility.Visible;
        public Visibility SecureVisibility { get; private set; } = Visibility.Collapsed;
        public Visibility IOVisibility { get; private set; } = Visibility.Collapsed;

        public ICommand MainViewButtonCommand { get; private set; }
        public ICommand IOButtonCommand { get; private set; }
        public ICommand SecureButtonCommand { get; private set; }

        public object IOContent { get; private set; }
        public object SecureMenuContent { get; private set; }

        public Dictionary<string, EvoVarModel> Vars { get; protected set; }

        #region Measurements Vars
        public EvoVarModel I1 { get; protected set; }
        public EvoVarModel I2 { get; protected set; }
        public EvoVarModel I3 { get; protected set; }
        public EvoVarModel I0 { get; protected set; }
        public EvoVarModel U0 { get; protected set; }
        #endregion Measurements Vars

        public EcoMuz2ViewModel(IRegionManager regionManager, IEvoVarsService evoVarsService) : base(regionManager)
        {
            _evoVarsService = evoVarsService;
            this.InitCommands();

            Vars = evoVarsService.Vars;
        }

        private void InitCommands()
        {
            MainViewButtonCommand = new RelayCommand(OnMainViewButtonCommand);
            IOButtonCommand = new RelayCommand(OnIOButtonCommand);
            SecureButtonCommand = new RelayCommand(OnSecureButtonCommand);
        }

        #region I/O
            private void BuildIoTab()
        {
            UniformGrid uniformGrid = new UniformGrid() { Columns = 2 };

            var inputs = new SectionBox() { Title = "Wejścia" };
            inputs.Content = DeviceHelper.GetIOContent(DeviceInputVars, nameof(DeviceInputVars));
            var outputs = new SectionBox() { Title = "Wyjścia" };
            outputs.Content = DeviceHelper.GetIOContent(DeviceOutputVars, nameof(DeviceOutputVars));

            uniformGrid.Children.Add(inputs);
            uniformGrid.Children.Add(outputs);
            IOContent = uniformGrid;
        }
        #endregion

        #region Zabezpieczenia
        public SecureButton SelectedButton { get; protected set; }

        private void BuildSecureMenu()
        {
            var wrapPanel = new WrapPanel();

            var valueConverter = new EcoMuz2SecurityConverter();

            SelectedButton = this.GetSecureButton("Zabezpieczenia 1", nameof(DeviceSecureVars), 1, valueConverter);
            wrapPanel.Children.Add(SelectedButton);
            SelectedButton.SelectElement();
            wrapPanel.Children.Add(this.GetSecureButton("Zabezpieczenia 2", nameof(DeviceSecureVars), 2, valueConverter));
            SecureMenuContent = wrapPanel;
        }

        protected SecureButton GetSecureButton(string text, string varPath, int type, IValueConverter descriptionConverter)
        {
            var sb = DeviceHelper.GetSecureButton(text, varPath, type, descriptionConverter, 2);
            sb.MouseDown += Sb_MouseDown;
            return sb;
        }

        private void Sb_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is SecureButton secureButton)
            {
                if (SelectedButton != null)
                    SelectedButton.UnselectElement();

                SelectedButton = secureButton;
                secureButton.SelectElement();
            }

        }
        #endregion

        #region Visibility button commands
        private void OnSecureButtonCommand()
        {
            MainPanelVisibility = Visibility.Collapsed;
            IOVisibility = Visibility.Collapsed;
            SecureVisibility = Visibility.Visible;
        }

        private void OnIOButtonCommand()
        {
            MainPanelVisibility = Visibility.Collapsed;
            IOVisibility = Visibility.Visible;
            SecureVisibility = Visibility.Collapsed;
        }

        private void OnMainViewButtonCommand()
        {
            MainPanelVisibility = Visibility.Visible;
            IOVisibility = Visibility.Collapsed;
            SecureVisibility = Visibility.Collapsed;

        }
        #endregion visibility button commands

        public override void OnLoaded(object obj = null)
        {
            base.OnLoaded(obj);

            I1 = _evoVarsService.SubscribeVar(_DeviceVarPrefix + "_" + nameof(I1));
            MessageBox.Show(I0?.Value.ToString());
            I2 = _evoVarsService.SubscribeVar(_DeviceVarPrefix + "_" + nameof(I2));
            I3 = _evoVarsService.SubscribeVar(_DeviceVarPrefix + "_" + nameof(I3));
            I0 = _evoVarsService.SubscribeVar(_DeviceVarPrefix + "_" + nameof(I0));
            U0 = _evoVarsService.SubscribeVar(_DeviceVarPrefix + "_" + nameof(U0));

        }

        public override void OnUnloaded(object obj = null)
        {
            base.OnUnloaded(obj);
            _evoVarsService.UnsubscribeVar(_DeviceVarPrefix + "_" + nameof(I1));
            _evoVarsService.UnsubscribeVar(_DeviceVarPrefix + "_" + nameof(I2));
            _evoVarsService.UnsubscribeVar(_DeviceVarPrefix + "_" + nameof(I3));
            _evoVarsService.UnsubscribeVar(_DeviceVarPrefix + "_" + nameof(I0));
            _evoVarsService.UnsubscribeVar(_DeviceVarPrefix + "_" + nameof(U0));
        }

        //protected override List<Action> PrepareSubscriptionTasks()
        //{
        //    var actions = base.PrepareSubscriptionTasks();
        //    if (!string.IsNullOrEmpty(_DeviceVarPrefix))
        //    {
        //        actions.Add(() => { DeviceMeasurementsVars = _varsService.SubscribeVar(_DeviceVarPrefix + "_Pomiary"); });
        //        actions.Add(() => { DeviceSecureVars = _varsService.SubscribeVar(_DeviceVarPrefix + "_Zabezpieczenia"); });
        //        actions.Add(() => { DeviceInputVars = _varsService.SubscribeVar(_DeviceVarPrefix + "_Wejscia"); });
        //        actions.Add(() => { DeviceOutputVars = _varsService.SubscribeVar(_DeviceVarPrefix + "_Wyjscia"); });
        //    }

        //    return actions;
        //}

        //protected override List<Action> PrepareUnsubscriptionTasks()
        //{
        //    var actions = base.PrepareUnsubscriptionTasks();

        //    actions.Add(() => { _varsService.UnsubscribeVar(_DeviceVarPrefix + "_Pomiary"); });
        //    actions.Add(() => { _varsService.UnsubscribeVar(_DeviceVarPrefix + "_Zabezpieczenia"); });
        //    actions.Add(() => { _varsService.UnsubscribeVar(_DeviceVarPrefix + "_Wejscia"); });
        //    actions.Add(() => { _varsService.UnsubscribeVar(_DeviceVarPrefix + "_Wyjscia"); });

        //    return actions;
        //}

        //protected override void AllVarsSubscribed()
        //{
        //    base.AllVarsSubscribed();

        //    if (DeviceSecureVars != null)
        //        this.BuildSecureMenu();
        //    if (DeviceInputVars != null && DeviceOutputVars != null)
        //        this.BuildIoTab();

        //}
    }
}
