﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Threading;

namespace ELPLATFORMA.CompensationComponents.ViewModels.EachDevice.Devices
{ 
    public class PAC3200ViewModel : IDeviceViewModel
    {
        private Dispatcher _Dispatcher = Dispatcher.CurrentDispatcher;

        private string _VarName;
        private string _SchemeVarsPrefix;
        private SectionBox _selectedSectionBox = null;


        #region Scheme vars
        public object Wylacznik { get; private set; }
        #endregion

        public object MeasurementVars { get; private set; }
        public string SchemePath { get; private set; }

        public object VarsContent { get; private set; }
        public object OthersContent { get; private set; }

        public Visibility LoadingCircleVisibility { get; private set; }
        private void SetVisibility(bool boolVal)
        {
            if (boolVal)
            {
                LoadingCircleVisibility = Visibility.Collapsed;
            }
            else
            {
                LoadingCircleVisibility = Visibility.Visible;
            }
        }

        public PAC3200ViewModel(IRegionManager regionManager, IVarsService varsService)
        {
            _varsService = varsService;
            this.SetVisibility(false);
        }

        public void InitViewModel(string schemeVarsPrefix, string deviceVarPrefix, string schemePath, string windowTitle)
        {
            _VarName = deviceVarPrefix;
            _SchemeVarsPrefix = schemeVarsPrefix;
            SchemePath = "Schemes/FieldSchemes/" + schemePath;
        }

        private void CollapseSectionBoxes()
        {
            foreach (var sb in _AddedSectionBox)
            {
                sb.IsMenuElement = true;

                if (sb.Title != "Moce")
                    sb.CollapseContent();
                else
                {
                    sb.ShowContent();
                    _selectedSectionBox = sb;
                }
            }
        }

        private List<SectionBox> _AddedSectionBox = new List<SectionBox>();
        private void GenerateContent(object vars)
        {
            var varsProperties = vars?.GetType().GetProperties();

            var prady = varsProperties.Where(x => x.Name.Contains("Prad"));

            var moce = varsProperties.Where(x => x.Name.Contains("Moc"));

            var allnapiecia = varsProperties.Where(x => x.Name.Contains("Napiecie"));

            var napieciaMiedzyfazowe = allnapiecia.Where(x => x.Name.Contains("Napiecie_miedzyfazowe"));

            var napiecia = allnapiecia.Where(x => !x.Name.Contains("Napiecie_miedzyfazowe"));


            var energie = varsProperties.Where(x => x.Name.Contains("Energia"));
            var reszta = varsProperties.Where(x => !x.Name.Contains("Napiecie") && !x.Name.Contains("Prad") && !x.Name.Contains("Moc") && !x.Name.Contains("Energia"));

            StackPanel stackPanel = new StackPanel();

            ItemsControl itemsControl = new ItemsControl();

            List<SectionBox> sectionBoxes = new List<SectionBox>();

            if (GetSectionBox(vars, napiecia, "Napięcia", typeof(ItemsControl), out SectionBox sectionBoxNapiecia))
            {
                sectionBoxNapiecia.MouseDown += SectionBoxNapiecia_MouseDown;
                _AddedSectionBox.Add(sectionBoxNapiecia);

                //stackPanel.Children.Add(sectionBoxNapiecia);
                sectionBoxes.Add(sectionBoxNapiecia);

            }
            if (GetSectionBox(vars, napieciaMiedzyfazowe, "Napięcia międzyfazowe ", typeof(ItemsControl), out SectionBox sectionBoxNapieciaMf))
            {
                sectionBoxNapieciaMf.MouseDown += SectionBoxNapiecia_MouseDown;
                _AddedSectionBox.Add(sectionBoxNapieciaMf);

                //stackPanel.Children.Add(sectionBoxNapieciaMf);

                sectionBoxes.Add(sectionBoxNapieciaMf);
            }
            if (GetSectionBox(vars, prady, "Prądy", typeof(ItemsControl), out SectionBox sectionBoxPrady))
            {
                sectionBoxPrady.MouseDown += SectionBoxNapiecia_MouseDown;
                _AddedSectionBox.Add(sectionBoxPrady);


                //stackPanel.Children.Add(sectionBoxPrady);
                sectionBoxes.Add(sectionBoxPrady);
            }
            if (GetSectionBox(vars, moce, "Moce", typeof(ItemsControl), out SectionBox sectionBoxMoce))
            {
                sectionBoxMoce.MouseDown += SectionBoxNapiecia_MouseDown;
                _AddedSectionBox.Add(sectionBoxMoce);


                //stackPanel.Children.Add(sectionBoxMoce);
                sectionBoxes.Add(sectionBoxMoce);
            }
            if (GetSectionBox(vars, energie, "Energie", typeof(ItemsControl), out SectionBox sectionBoxEnergie))
            {
                sectionBoxEnergie.MouseDown += SectionBoxNapiecia_MouseDown;
                _AddedSectionBox.Add(sectionBoxEnergie);


                //stackPanel.Children.Add(sectionBoxEnergie);
                sectionBoxes.Add(sectionBoxEnergie);
            }
            if (GetSectionBox(vars, reszta, "Pozostałe", typeof(UniformGrid), out SectionBox sectionBoxPozostale, Orientation.Horizontal))
            {
                OthersContent = sectionBoxPozostale;
            }


            itemsControl.ItemsSource = sectionBoxes;
            VarsContent = itemsControl;
        }

        private void SectionBoxNapiecia_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (sender is SectionBox sb)
            {
                _selectedSectionBox?.CollapseContent();
                sb.ShowContent();
                _selectedSectionBox = sb;
            }
        }

        private bool GetSectionBox(object vars, IEnumerable<PropertyInfo> sectionProperties, string sectionName, Type contentControlType, out SectionBox sectionBox, Orientation spOrientation = Orientation.Vertical)
        {
            sectionBox = null;
            int propertiesCount = sectionProperties.Count();

            if (vars != null && sectionProperties != null && propertiesCount > 0 && contentControlType != null)
            {
                sectionBox = new SectionBox() { Title = sectionName };
                var contentControlInstance = Activator.CreateInstance(contentControlType);

                if (contentControlInstance is Panel panel)
                {
                    if (panel is StackPanel stackPanel)
                    {
                        //var sc = new ScrollViewer() { VerticalScrollBarVisibility = ScrollBarVisibility.Auto };
                        //var grid = new Grid();

                        //grid.Children.Add(sc);
                        //sc.Content = stackPanel;

                        sectionBox.Content = stackPanel;

                        //stackPanel.Orientation = spOrientation;
                        stackPanel.Height = double.NaN;
                    }
                    else if (panel is UniformGrid uniformGrid)
                    {
                        // przypisane panela do contentu sectionBox (żeby rozdzielić typy)

                        sectionBox.Content = panel;
                        if (spOrientation == Orientation.Vertical)
                            uniformGrid.Rows = sectionProperties.Count();
                        else
                            uniformGrid.Columns = sectionProperties.Count();
                    }

                    foreach (var property in sectionProperties)
                    {
                        var baseVar = property.GetValue(vars);
                        if (baseVar != null)
                        {
                            if (GetMeasureControl(baseVar, out MeasurementDataViewer measurementDataViewer))
                                panel.Children.Add(measurementDataViewer);
                        }
                    }
                }
                else if (contentControlInstance is ItemsControl itemsControl)
                {
                    var controlsList = new List<MeasurementDataViewer>();

                    foreach (var property in sectionProperties)
                    {
                        var baseVar = property.GetValue(vars);
                        if (baseVar != null)
                        {
                            if (GetMeasureControl(baseVar, out MeasurementDataViewer measurementDataViewer))
                                controlsList.Add(measurementDataViewer);
                        }
                    }

                    itemsControl.ItemsSource = controlsList;

                    var sc = new ScrollViewer() { VerticalScrollBarVisibility = ScrollBarVisibility.Auto, HorizontalContentAlignment = HorizontalAlignment.Left, VerticalContentAlignment = VerticalAlignment.Top };
                    sc.Content = itemsControl;

                    sectionBox.Content = sc;
                }


                //ScrollViewer scrollViewer = new ScrollViewer() { Content = panel,
                //VerticalScrollBarVisibility = ScrollBarVisibility.Auto,
                //HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled };

                //panel.SetBinding(StackPanel.HeightProperty, new Binding("ActualHeight") { Source = panel } );


                return true;
            }
            return false;
        }

        private bool GetMeasureControl(object obj, out MeasurementDataViewer measurementDataViewer)
        {
            measurementDataViewer = null;
            if (obj is BaseVar baseVar)
            {
                measurementDataViewer = new MeasurementDataViewer();

                measurementDataViewer.SetBinding(MeasurementDataViewer.BaseVarProperty, this.GetBinding(nameof(MeasurementVars) + "." + baseVar.Name));
                //measurementDataViewer.SetBinding(MeasurementDataViewer.DescriptionProperty, this.GetBinding(nameof(MeasurementVars) + "." + baseVar.Name + ".Description"));
                //measurementDataViewer.SetBinding(MeasurementDataViewer.UnitProperty, this.GetBinding(nameof(MeasurementVars) + "." + baseVar.Name + ".Unit"));
                //measurementDataViewer.SetBinding(MeasurementDataViewer.ValueProperty, this.GetBinding(nameof(MeasurementVars) + "." + baseVar.Name + ".Value"));
                return true;
            }

            return false;
        }

        private Binding GetBinding(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                Binding binding = new Binding(path);
                binding.Source = this;
                binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                return binding;
            }
            return null;
        }

        private Task _SubscribeTask = null;
        private bool _SubscribeTaskLocekr = false;
        private void SubscribeVars()
        {
            if (!_SubscribeTaskLocekr)
            {
                _SubscribeTaskLocekr = true;
                List<Action> actions = new List<Action>();
                if (!string.IsNullOrEmpty(_VarName))
                    actions.Add(() => { MeasurementVars = _varsService.SubscribeVar(_VarName + "_Pomiary"); });

                if (!string.IsNullOrEmpty(_SchemeVarsPrefix))
                {
                    actions.Add(() => { Wylacznik = _varsService.SubscribeVar(_SchemeVarsPrefix + "_Wylacznik"); });
                }

                _SubscribeTask = _varsService.ParallelSubscribeVars(actions, () =>
                {
                    _SubscribeTaskLocekr = false;
                    _Dispatcher.Invoke(() =>
                    {
                        if (MeasurementVars != null)
                            GenerateContent(MeasurementVars);

                        this.SetVisibility(true);

                        this.CollapseSectionBoxes();
                    });
                });
            }
        }

        private bool _UnsubscribeTaskLocker = false;
        private void UnSubscribeVars()
        {
            List<Action> actions = new List<Action>();

            if (!string.IsNullOrEmpty(_VarName))
            {
                actions.Add(() => {
                    _varsService.UnsubscribeVar(_VarName + "_Pomiary");
                });
            }

            if (!string.IsNullOrEmpty(_SchemeVarsPrefix))
            {
                actions.Add(() => { _varsService.UnsubscribeVar(_SchemeVarsPrefix + "_Wylacznik"); });
            }

            _UnsubscribeTaskLocker = true;
            _varsService.ParallelUnsubscribeVars(actions, _SubscribeTask, () => { _UnsubscribeTaskLocker = false; });
        }

        public override void OnLoaded(object parameter = null)
        {
            base.OnLoaded();

            this.SubscribeVars();
        }

        public override void OnUnloaded(object parameter = null)
        {
            UnSubscribeVars();

            foreach (var sb in _AddedSectionBox)
            {
                sb.MouseDown -= SectionBoxNapiecia_MouseDown;
            }

            base.OnUnloaded();

        }
    }
}
