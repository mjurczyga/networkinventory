﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELPLATFORMA.CompensationComponents.ViewModels.EachDevice.Devices
{
    public interface IDeviceViewModel : IBaseViewModel
    {
        void InitViewModel(string schemeVarsPrefix, string deviceVarPrefix, string schemePath, string windowTitle);
    }
}
