﻿using ELPLATFORMA.CompensationComponents.Services;
using GalaSoft.MvvmLight.Command;
using SchemeControlsLib.Controls.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;

namespace ELPLATFORMA.CompensationComponents.ViewModels.EachDevice.Devices
{
    public abstract class DeviceBaseViewModel : BaseViewModel, IDeviceViewModel
    {
        protected readonly IRegionManager _regionManager;
        protected readonly Dispatcher _dispatcher = Dispatcher.CurrentDispatcher;

        protected string _DeviceVarPrefix;
        protected string _SchemeVarPrefix;
        protected IEnumerable<string> _SchemeVarsList;

        public string SchemePath { get; protected set; } = "Schemes/FieldSchemes/NormalField_Scheme";

        public object SYS1_Odlacznik { get; private set; }
        public object SYS2_Odlacznik { get; private set; }
        public object OdlacznikKablowy { get; private set; }
        public object Wylacznik { get; private set; }
        public object Uziemnik { get; private set; }
        public object Wozek { get; private set; }
        public object Odlacznik { get; private set; }
        public object Rozlacznik { get; private set; }


        public ICommand FieldSchemeBindingDone { get; private set; }

        public DeviceBaseViewModel(IRegionManager regionManager)
        {
            _regionManager = regionManager;
            FieldSchemeBindingDone = new RelayCommand<VarsBindingsDoneEventArgs>(OnFieldSchemeBindingDone);
        }

        public virtual void InitViewModel(string schemeVarsPrefix, string deviceVarPrefix, string schemePath, string windowTitle)
        {
            _DeviceVarPrefix = deviceVarPrefix;
            _SchemeVarPrefix = schemeVarsPrefix;


            SchemePath = "Schemes/FieldSchemes/" + schemePath;
        }

        private void OnFieldSchemeBindingDone(VarsBindingsDoneEventArgs obj)
        {
            var schemeVars = obj.VarsNames;
            List<string> synVars = new List<string>();
            foreach (var var in schemeVars)
            {
                synVars.Add(_SchemeVarPrefix + "_" + var);
            }
            _SchemeVarsList = synVars;

            //Tutaj ma być subskrypcja
        }

        protected void PrepareSubscriptionTasks()
        {
            if (_SchemeVarsList != null)
            {
                var wylacznikVarName = _SchemeVarsList.Where(x => x.Contains(nameof(Wylacznik))).FirstOrDefault();
                var sys1VarName = _SchemeVarsList.Where(x => x.Contains(nameof(SYS1_Odlacznik))).FirstOrDefault();
                var sys2VarName = _SchemeVarsList.Where(x => x.Contains(nameof(SYS2_Odlacznik))).FirstOrDefault();
                var uziemnikVarName = _SchemeVarsList.Where(x => x.Contains(nameof(Uziemnik))).FirstOrDefault();
                var odlacznikKablowyVarName = _SchemeVarsList.Where(x => x.Contains(nameof(OdlacznikKablowy))).FirstOrDefault();
                var wozekVarName = _SchemeVarsList.Where(x => x.Contains(nameof(Wozek))).FirstOrDefault();
                var odlacznikVarName = _SchemeVarsList.Where(x => x.Contains(nameof(Odlacznik))).FirstOrDefault();
                var RozlacznikVarName = _SchemeVarsList.Where(x => x.Contains(nameof(Rozlacznik))).FirstOrDefault();

            }
        }

        public override void OnLoaded(object obj = null)
        {
            
        }

        public override void OnUnloaded(object obj = null)
        {
        }

        void IBaseViewModel.StartAction<T>(Action<T> action)
        {
            
        }
    }  


}
