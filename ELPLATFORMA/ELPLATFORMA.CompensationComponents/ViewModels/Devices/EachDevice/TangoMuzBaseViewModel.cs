﻿using SchemeControlsLib.Controls.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace ELPLATFORMA.CompensationComponents.ViewModels.EachDevice.Devices
{
    public abstract class TangoMuzBaseViewModel : DeviceBaseViewModel
    {      
        public ICommand AlarmButtonCommand { get; private set; }
        public ICommand AlarmsBorderMouseEnterCommand { get; private set; }
        public ICommand AlarmsBorderMouseLeaveCommand { get; private set; }

        public double Splitter { get; set; }

        public int MuzeEventVal { get; set; } = 7;

        #region Scheme vars
        public object DeviceMeasurementsVars { get; private set; }
        public object DeviceSecureVars { get; private set; }
        public object DeviceEvents { get; private set; }
        public object DeviceControl { get; private set; }
        #endregion

        public Visibility ControlVisibility { get; private set; } = Visibility.Collapsed;
        public Visibility LedPanelVisibility { get; private set; } = Visibility.Visible;

        public Visibility MainPanelVisibility { get; private set; } = Visibility.Visible;
        public Visibility SecureVisibility { get; private set; } = Visibility.Collapsed;
        public Visibility IOVisibility { get; private set; } = Visibility.Collapsed;

        public Visibility AlarmButtonVisibility { get; private set; } = Visibility.Collapsed;


        public ICommand ControlButtonCommand { get; private set; }
        public ICommand LedsButtonCommand { get; private set; }
        public ICommand IOButtonCommand { get; private set; }
        public ICommand SecureButtonCommand { get; private set; }

        public DrawingBrush ButtonDrawingBrush { get; private set; } = IconResources.GetIconBrushByKey("GoToTop");

        public object SecureMenuContent { get; protected set; }
        public object IOContent { get; protected set; }

        public TangoMuzBaseViewModel()
        {
            this.InitCommands();
        }

        public override void InitViewModel(string schemeVarsPrefix, string DeviceVarPrefix, string schemePath, string windowTitle)
        {
            base.InitViewModel(schemeVarsPrefix, DeviceVarPrefix, schemePath, windowTitle);

            _FieldConnectorsDict = DlectricDeviceFieldConfiguration.GetFieldScheme(schemeVarsPrefix);
        }

        public void InitViewModel(IEnumerable<string> schemeVarsList, string DeviceVarPrefix, string schemePath)
        {
            SchemePath = "Schemes/FieldSchemes/" + schemePath;
            _SchemeVarsList = schemeVarsList;
            _DeviceVarPrefix = DeviceVarPrefix;
        }

        private void InitCommands()
        {
            AlarmButtonCommand = new RelayCommand<object>(args => OnAlarmButtonCommand(args));
            AlarmsBorderMouseEnterCommand = new RelayCommand<object>(args => OnAlarmsBorderMouseEnterCommand(args));
            AlarmsBorderMouseLeaveCommand = new RelayCommand<object>(args => OnAlarmsBorderMouseLeaveCommand(args));

            ControlButtonCommand = new RelayCommand(OnControlButtonCommand);
            LedsButtonCommand = new RelayCommand(OnLedsButtonCommand);
            IOButtonCommand = new RelayCommand(OnIOButtonCommand);
            SecureButtonCommand = new RelayCommand(OnSecureButtonCommand);

            InverseBoolSenderCommand = new RelayCommand<object>(OnSendTrueCommand);
        }

        private void OnAlarmsBorderMouseLeaveCommand(object args)
        {
            AlarmButtonVisibility = Visibility.Collapsed;
        }

        private void OnAlarmsBorderMouseEnterCommand(object args)
        {
            AlarmButtonVisibility = Visibility.Visible;
        }

        #region build control tab
        protected Dictionary<int, string> _FieldConnectorsDict = null;
        protected string _openStructName = "OtworzLacznik.OtworzLacznik";
        protected string _closeStructName = "ZamknijLacznik.ZamknijLacznik";
        protected string _commandErrorStructName = "BladSterowania.BladSterowania";

        public ICommand InverseBoolSenderCommand { get; private set; }

        public object SteeringContent { get; private set; }

        private void BuidControlTab()
        {
            StackPanel stackPanel = new StackPanel();
            if (_FieldConnectorsDict != null)
            {
                foreach (var lacznik in _FieldConnectorsDict)
                {
                    string suffix = "";
                    if (lacznik.Key > 0)
                    {
                        suffix = "_" + lacznik.Key;
                        stackPanel.Children.Add(this.GetElementSteeringSection(suffix, lacznik.Value));
                    }
                }
            }
            SteeringContent = stackPanel;
        }

        private SectionBox GetElementSteeringSection(string varsSufix, string sectionName)
        {
            var result = new SectionBox() { Title = sectionName };

            var uniformGrid = new UniformGrid() { Columns = 2 };

            var openBtn = new ImageButton() { Drawing = IconResources.GetIconBrushByKey("openSwitchIcon"), Text = "Otwórz" };
            openBtn.SetBinding(ImageButton.CommandParameterProperty, new Binding(nameof(DeviceControl) + "." + _openStructName + varsSufix));
            openBtn.SetBinding(ImageButton.CommandProperty, new Binding(nameof(InverseBoolSenderCommand)));

            var closeBtn = new ImageButton() { Drawing = IconResources.GetIconBrushByKey("closeSwitchIcon"), Text = "Zamknij" };
            closeBtn.SetBinding(ImageButton.CommandParameterProperty, new Binding(nameof(DeviceControl) + "." + _closeStructName + varsSufix));
            closeBtn.SetBinding(ImageButton.CommandProperty, new Binding(nameof(InverseBoolSenderCommand)));

            uniformGrid.Children.Add(openBtn);
            uniformGrid.Children.Add(closeBtn);

            var errorIcon = new Border() { Width = 16, Height = 16, Background = IconResources.GetIconBrushByKey("StatusCriticalError"), ToolTip = "Problem komunikacji pomiędzy sterownikiem PLC a Device'em" };
            var visibilityBinding = new Binding(nameof(DeviceControl) + "." + _commandErrorStructName + varsSufix + ".Value")
            {
                Converter = new BooleanToVisibilityConverter(),
            };
            errorIcon.SetBinding(Border.VisibilityProperty, visibilityBinding);
            var errorIconVb = new Viewbox() { Child = errorIcon };
            errorIconVb.SetBinding(Viewbox.HeightProperty, new Binding("ActualHeight") { Source = openBtn });
            Grid.SetColumn(errorIconVb, 1);
            var grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) });
            grid.Children.Add(uniformGrid);
            grid.Children.Add(errorIconVb);

            result.Content = grid;
            return result;
        }


        private void OnSendTrueCommand(object obj)
        {
            if (obj is BaseVar baseVar)
            {
                CommandExecutor.OnCommandExecuteCheckCredentials(async () =>
                {
                    if (baseVar?.FullName != null && bool.TryParse(baseVar?.Value?.ToString(), out bool baseVarValue) && !baseVarValue)
                    {
                        var response = await _varsService.SendValueAsync(baseVar.FullName, true.ToString());
                        MSGBox.Show(response, "Błąd wysyłania", MessageBoxTypes.WARNING);

                        if (string.IsNullOrEmpty(response))
                            await _archiveEventsService.AddEvent(EventTypes.SystemInformation, baseVar.FullName + "- zmiana wartości. Nowa wartość: Załączono (TRUE).", ActiveUser.UserName);
                        else
                            await _archiveEventsService.AddEvent(EventTypes.SystemWarning, baseVar.FullName + "- zmiana wartości. Nowa wartość: Załączono (TRUE). Błąd: " + response?.ToString(), ActiveUser.UserName);
                    }
                }, true); //ActiveUser.CanChangePreset
            }
        }
        #endregion

        #region Events
        public List<object> DeviceEventViewModels { get; protected set; } = new List<object>();

        protected abstract void PrepareEventsList();
        #endregion

        #region Visibility button commands
        private void OnSecureButtonCommand()
        {
            MainPanelVisibility = Visibility.Collapsed;
            IOVisibility = Visibility.Collapsed;
            SecureVisibility = Visibility.Visible;
        }

        private void OnIOButtonCommand()
        {
            MainPanelVisibility = Visibility.Collapsed;
            IOVisibility = Visibility.Visible;
            SecureVisibility = Visibility.Collapsed;
        }

        private void OnLedsButtonCommand()
        {
            MainPanelVisibility = Visibility.Visible;
            IOVisibility = Visibility.Collapsed;
            SecureVisibility = Visibility.Collapsed;

            ControlVisibility = Visibility.Collapsed;
            LedPanelVisibility = Visibility.Visible;
        }

        private void OnControlButtonCommand()
        {
            MainPanelVisibility = Visibility.Visible;
            IOVisibility = Visibility.Collapsed;
            SecureVisibility = Visibility.Collapsed;

            ControlVisibility = Visibility.Visible;
            LedPanelVisibility = Visibility.Collapsed;
        }
        #endregion visibility button commands

        #region Build Secure tab
        protected abstract void BuildSecureMenu();

        public SecureButton SelectedButton { get; protected set; }

        protected SecureButton GetSecureButton(string text, string varPath, int type, IValueConverter descriptionConverter)
        {
            var sb = DeviceHelper.GetSecureButton(text, varPath, type, descriptionConverter, 3);
            sb.MouseDown += Sb_MouseDown;
            return sb;
        }

        private void Sb_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is SecureButton secureButton)
            {
                if (SelectedButton != null)
                    SelectedButton.UnselectElement();

                SelectedButton = secureButton;
                secureButton.SelectElement();
            }

        }
        #endregion bulid secure tab

        #region Bulid IO tab
        protected abstract void BuildIoTab();
        #endregion Bulid IO tab

        #region Animacja rozwinięcia i zwinięcia
        private void OnAlarmButtonCommand(object args)
        {
            if (args is Grid grid)
            {
                if (grid.RowDefinitions.Count == 2)
                {
                    if (Math.Round(grid.RowDefinitions[0].Height.Value) == 1 && Math.Round(grid.RowDefinitions[1].Height.Value, 2) == 0.35)
                    {
                        this.ExpandAlarms(grid);
                        ButtonDrawingBrush = IconResources.GetIconBrushByKey("GoToBottom");
                    }
                    else if (grid.RowDefinitions[0].Height.Value == 0 && grid.RowDefinitions[1].Height.Value == 1)
                    {
                        this.CollapseAlarms(grid);
                        ButtonDrawingBrush = IconResources.GetIconBrushByKey("GoToTop");
                    }
                }

            }
        }

        private Task ExpandAlarms(Grid grid)
        {
            var currentHeight0 = grid.RowDefinitions[0].Height.Value;
            var currentHeight1 = grid.RowDefinitions[1].Height.Value;

            return Task.Run(async () =>
            {

                while (currentHeight0 - 0.05 >= 0)
                {
                    grid.Dispatcher.Invoke(() =>
                    {
                        grid.RowDefinitions[0].Height = new System.Windows.GridLength(currentHeight0 - 0.05, System.Windows.GridUnitType.Star);
                        grid.RowDefinitions[1].Height = new System.Windows.GridLength(currentHeight1 + 0.0325, System.Windows.GridUnitType.Star); //0.00325
                        currentHeight0 = grid.RowDefinitions[0].Height.Value;
                        currentHeight1 = grid.RowDefinitions[1].Height.Value;
                    });

                    await Task.Delay(1);
                }

                if (currentHeight0 > 0 && currentHeight0 - 0.05 < 0)
                {
                    grid.Dispatcher.Invoke(() =>
                    {
                        grid.RowDefinitions[0].Height = new System.Windows.GridLength(0, System.Windows.GridUnitType.Star);
                        grid.RowDefinitions[1].Height = new System.Windows.GridLength(1, System.Windows.GridUnitType.Star);
                    });
                }

            });
        }

        private Task CollapseAlarms(Grid grid)
        {
            var currentHeight0 = grid.RowDefinitions[0].Height.Value;
            var currentHeight1 = grid.RowDefinitions[1].Height.Value;

            return Task.Run(async () =>
            {

                while (currentHeight1 - 0.0325 >= 0.35)
                {
                    grid.Dispatcher.Invoke(() =>
                    {
                        grid.RowDefinitions[0].Height = new System.Windows.GridLength(currentHeight0 + 0.05, System.Windows.GridUnitType.Star);
                        grid.RowDefinitions[1].Height = new System.Windows.GridLength(currentHeight1 - 0.0325, System.Windows.GridUnitType.Star);
                        currentHeight0 = grid.RowDefinitions[0].Height.Value;
                        currentHeight1 = grid.RowDefinitions[1].Height.Value;
                    });



                    await Task.Delay(1);
                }

                if (currentHeight1 > 0.35 && currentHeight1 - 0.0325 < 0.35)
                {
                    grid.Dispatcher.Invoke(() =>
                    {
                        grid.RowDefinitions[0].Height = new System.Windows.GridLength(1, System.Windows.GridUnitType.Star);
                        grid.RowDefinitions[1].Height = new System.Windows.GridLength(0.35, System.Windows.GridUnitType.Star);
                    });

                }
            });
        }
        #endregion
      
        #region Subskrypcje/desubskrypcje

        protected override List<Action> PrepareSubscriptionTasks()
        {
            var actions = base.PrepareSubscriptionTasks();
            _SubscribeTaskLocekr = true;

            if (!string.IsNullOrEmpty(_DeviceVarPrefix))
            {
                actions.Add(() => { DeviceMeasurementsVars = _varsService.SubscribeVar(_DeviceVarPrefix + "_Pomiary"); });
                actions.Add(() => { DeviceSecureVars = _varsService.SubscribeVar(_DeviceVarPrefix + "_Zabezpieczenia"); });
                actions.Add(() => { DeviceEvents = _varsService.SubscribeVar(_DeviceVarPrefix + "_Zdarzenia"); });
                actions.Add(() => { DeviceControl = _varsService.SubscribeVar(_DeviceVarPrefix + "_Sterowanie"); });
            }
            return actions;
        }

        /// <summary>
        /// Metoda wywoływana po subskrypcji zmiennych (w dispatcher - do budowania zakłądek)
        /// </summary>
        protected override void AllVarsSubscribed()
        {
            base.AllVarsSubscribed();

            if (DeviceSecureVars != null)
                this.BuildSecureMenu();
            if (DeviceEvents != null)
            {
                this.PrepareEventsList();
            }
            if (DeviceControl != null)
                this.BuidControlTab();
        }

        protected override List<Action> PrepareUnsubscriptionTasks()
        {
            List<Action> actions = base.PrepareUnsubscriptionTasks();

            actions.Add(() => { _varsService.UnsubscribeVar(_DeviceVarPrefix + "_Pomiary"); });
            actions.Add(() => { _varsService.UnsubscribeVar(_DeviceVarPrefix + "_Zabezpieczenia"); });
            actions.Add(() => { _varsService.UnsubscribeVar(_DeviceVarPrefix + "_Zdarzenia"); });
            actions.Add(() => { _varsService.UnsubscribeVar(_DeviceVarPrefix + "_Sterowanie"); });

            return actions;
        }
        #endregion

        #region Raport

        #endregion
    }
}
