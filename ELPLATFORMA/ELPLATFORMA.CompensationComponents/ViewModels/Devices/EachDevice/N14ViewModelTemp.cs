﻿using eFOLZNON.Core.Attributes;
using VISU.Core.Models;
using VISU.Core.Services;
using VISU.Core.StaticConfigurations;
using VISU.Domain.Models.VarModels;
using VISU.Domain.Services;
using VISU.ServerCommunication.Services;
using VISU.ViewModels.ViewModels;
using HADES_CUK_PNIOWEK.Controls;
using HADES_CUK_PNIOWEK.Themes.Resources;
using HADES_CUK_PNIOWEK.ViewModels.ProjectViews.Devices.ElectricParametersMeasureDevices.EachDevice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Threading;

namespace ELPLATFORMA.CompensationComponents.ViewModels.EachDevice.Devices
{
    public class N14ViewModelTemp : IDeviceViewModel
    {
        private Dispatcher _Dispatcher = Dispatcher.CurrentDispatcher;

        private string _VarName;
        private string _SchemeVarsPrefix;

        public object N14_MeasurementVars { get; private set; }

        public string SchemePath { get; private set; }

        public object Wylacznik { get; private set; }
        public object SYS1_Odlacznik { get; private set; }
        public object SYS2_Odlacznik { get; private set; }

        public object VarsContent { get; private set; }
        public object OthersContent { get; private set; }
        public bool ShowPopup { get; set; }
        public object PopupContent { get; set; }
        public object PopupPlacementTarget { get; set; }

        public Visibility LoadingCircleVisibility { get; private set; }
        private void SetVisibility(bool boolVal)
        {
            if (boolVal)
            {
                LoadingCircleVisibility = Visibility.Collapsed;
            }
            else
            {
                LoadingCircleVisibility = Visibility.Visible;
            }
        }

        public N14ViewModelTemp(IRegionManager regionManager)
        {
            this.SetVisibility(false);
        }
        private string measureStructName = "_POMIARY";
        public void InitViewModel(string schemeVarPerefix, string deviceVarPrefix, string schemePath, string windowTitle)
        {
            _VarName = deviceVarPrefix;
            _SchemeVarsPrefix = schemeVarPerefix;
            SchemePath = "Schemes/FieldSchemes/" + schemePath;
            if (!string.IsNullOrEmpty(deviceVarPrefix) && deviceVarPrefix.ToUpper().Contains("KR2"))
                measureStructName = "_Pomiary";            
        }

        private void GenerateContent(object vars)
        {
            var varsProperties = vars?.GetType().GetProperties();

            var prady = varsProperties.Where(x => x.Name.Contains("Prad")).ToList();
            var pradSredni = GetAvgMeasureFromPropertiesGroup(prady);

            var moce = varsProperties.Where(x => x.Name.Contains("Moc")).ToList();
            var mocSrednia = GetAvgMeasureFromPropertiesGroup(moce);

            var allnapiecia = varsProperties.Where(x => x.Name.Contains("Napiecie"));
            var napieciaMiedzyfazowe = allnapiecia.Where(x => x.Name.Contains("Napiecie_miedzyfazowe")).ToList();
            var napiecieMiedzyfazoweSrenie = GetAvgMeasureFromPropertiesGroup(napieciaMiedzyfazowe);
            var napiecia = allnapiecia.Where(x => !x.Name.Contains("Napiecie_miedzyfazowe")).ToList();
            var napiecieSrednie = GetAvgMeasureFromPropertiesGroup(napiecia);

            //var energie = varsProperties.Where(x => x.Name.Contains("Energia")).ToList();
            //var energiaSrednia = GetAvgMeasureFromPropertiesGroup(energie);

            var reszta = varsProperties.Where(x => !x.Name.Contains("Napiecie") && !x.Name.Contains("Prad") && !x.Name.Contains("Moc") && !x.Name.Contains("Energia"));

            StackPanel stackPanel = new StackPanel();

            if (GetMeasureControlFromProperty(vars, pradSredni, prady, out var mdvPradSredni))
                stackPanel.Children.Add(mdvPradSredni);
            if (GetMeasureControlFromProperty(vars, mocSrednia, moce, out var mdvMocSrednia))
                stackPanel.Children.Add(mdvMocSrednia);
            if (GetMeasureControlFromProperty(vars, napiecieMiedzyfazoweSrenie, napieciaMiedzyfazowe, out var mdvNapiecieMiedzyfazoweSrenie))
                stackPanel.Children.Add(mdvNapiecieMiedzyfazoweSrenie);
            if (GetMeasureControlFromProperty(vars, napiecieSrednie, napiecia, out var mdvNapiecieSrednie))
                stackPanel.Children.Add(mdvNapiecieSrednie);
            //if (GetMeasureControlFromProperty(vars, pradSredni, energiaSrednia, out var mdvEnergiaSrednia))
                //stackPanel.Children.Add(mdvEnergiaSrednia);

            if (GetSectionBox(vars, reszta, "Pozostałe", out SectionBox sectionBoxPozostale))
                OthersContent = sectionBoxPozostale;

            VarsContent = stackPanel;
        }

        private bool PreparePopupContent(object vars, IEnumerable<PropertyInfo> properties, out StackPanel stackPanel)
        {
            stackPanel = new StackPanel() { MinWidth = ScreenRatios.ToFullHDWidthRatio * 220 };            

            if (properties != null)
            {
                foreach (var property in properties)
                {
                    var baseVar = property.GetValue(vars);
                    if (GetMeasureControl(baseVar, out MeasurementDataViewer measurementDataViewer))
                        stackPanel.Children.Add(measurementDataViewer);
                }
                return true;
            }

            return false;
        }

        private PropertyInfo GetAvgMeasureFromPropertiesGroup(List<PropertyInfo> properties)
        {
            PropertyInfo srednie = properties.Where(x => x.Name.ToUpper().Contains("SREDNI")).FirstOrDefault();
            if (srednie != null)
                properties.Remove(srednie);

            return srednie;
        }

        private bool GetSectionBox(object vars, IEnumerable<PropertyInfo> sectionProperties, string sectionName, out SectionBox sectionBox)
        {
            sectionBox = null;
            int propertiesCount = sectionProperties.Count();

            if (vars != null && sectionProperties != null && propertiesCount > 0)
            {
                UniformGrid uniformGrid = new UniformGrid() { Columns = propertiesCount };
                foreach (var property in sectionProperties)
                {
                    var baseVar = property.GetValue(vars);
                    if (baseVar != null)
                    {
                        if (GetMeasureControl(baseVar, out MeasurementDataViewer measurementDataViewer))
                            uniformGrid.Children.Add(measurementDataViewer);
                    }
                }

                sectionBox = new SectionBox() { Title = sectionName };
                sectionBox.Content = uniformGrid;
                return true;
            }
            return false;
        }

        private List<FrameworkElement> _elementsWithPopup = new List<FrameworkElement>();
        private bool GetMeasureControlFromProperty(object vars, PropertyInfo property, IEnumerable<PropertyInfo> popupVarsProperty, out MeasurementDataViewer measurementDataViewer)    
        {
            measurementDataViewer = null;
            if (property == null || vars == null || popupVarsProperty == null)
                return false;

            var baseVar = property.GetValue(vars);
            if(GetMeasureControl(baseVar, out var mdv))
            {
                PreparePopupContent(vars, popupVarsProperty, out var popupContent);
                mdv.Tag = popupContent;
                mdv.IconDrawingBrush = IconResources.GetIconBrushByKey("PopupIcon");
                mdv.IconVisibility = Visibility.Visible;
                mdv.MouseEnter += Mdv_MouseEnter;
                mdv.MouseLeave += Mdv_MouseLeave;

                _elementsWithPopup.Add(mdv);
                measurementDataViewer = mdv;
                return true;
            }

            return false;
        }

        private void Mdv_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (sender is FrameworkElement fv)
            {
                this.ShowPopup = false;
                this.PopupPlacementTarget = null;
            }
        }

        private void Mdv_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if(sender is FrameworkElement fv)
            {
                this.PopupPlacementTarget = fv;
                this.PopupContent = fv.Tag;
                this.ShowPopup = true;                
            }
        }

        private bool GetMeasureControl(object obj, out MeasurementDataViewer measurementDataViewer)
        {
            measurementDataViewer = null;
            if (obj is BaseVar baseVar)
            {
                measurementDataViewer = new MeasurementDataViewer();
                measurementDataViewer.SetBinding(MeasurementDataViewer.BaseVarProperty, this.GetBinding(nameof(N14_MeasurementVars) + "." + baseVar.Name));
                //measurementDataViewer.SetBinding(MeasurementDataViewer.DescriptionProperty, this.GetBinding(nameof(N14_MeasurementVars) + "." + baseVar.Name + ".Description"));
                //measurementDataViewer.SetBinding(MeasurementDataViewer.UnitProperty, this.GetBinding(nameof(N14_MeasurementVars) + "." + baseVar.Name + ".Unit"));
                //measurementDataViewer.SetBinding(MeasurementDataViewer.ValueProperty, this.GetBinding(nameof(N14_MeasurementVars) + "." + baseVar.Name + ".Value"));
                return true;
            }

            return false;
        }

        private Binding GetBinding(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                Binding binding = new Binding(path);
                binding.Source = this;
                binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                return binding;
            }
            return null;
        }

        private Task _SubscribeTask = null;
        private bool _SubscribeTaskLocekr = false;
        private void SubscribeVars()
        {
            if (!_SubscribeTaskLocekr)
            {
                _SubscribeTaskLocekr = true;
                List<Action> actions = new List<Action>();
                if (!string.IsNullOrEmpty(_VarName))
                    actions.Add(() => { N14_MeasurementVars = _varsService.SubscribeVar(_VarName + measureStructName); });

                if (!string.IsNullOrEmpty(_SchemeVarsPrefix))
                {
                    actions.Add(() => { SYS1_Odlacznik = _varsService.SubscribeVar(_SchemeVarsPrefix + "_SYS1_Odlacznik"); });
                    actions.Add(() => { SYS2_Odlacznik = _varsService.SubscribeVar(_SchemeVarsPrefix + "_SYS2_Odlacznik"); });
                    actions.Add(() => { Wylacznik = _varsService.SubscribeVar(_SchemeVarsPrefix + "_Wylacznik"); });
                }

                _SubscribeTask = _varsService.ParallelSubscribeVars(actions, () =>
                {
                    _SubscribeTaskLocekr = false;
                    _Dispatcher.Invoke(() =>
                    {
                        if (N14_MeasurementVars != null)
                            GenerateContent(N14_MeasurementVars);

                        this.SetVisibility(true);
                    });
                });
            }
        }

        private bool _UnsubscribeTaskLocker = false;
        private void UnSubscribeVars()
        {
            List<Action> actions = new List<Action>();

            if (!string.IsNullOrEmpty(_VarName))
            {
                actions.Add(() => {
                    _varsService.UnsubscribeVar(_VarName + measureStructName);
                });
            }

            if (!string.IsNullOrEmpty(_SchemeVarsPrefix))
            {
                actions.Add(() => { _varsService.UnsubscribeVar(_SchemeVarsPrefix + "_SYS1_Odlacznik"); });
                actions.Add(() => { _varsService.UnsubscribeVar(_SchemeVarsPrefix + "_SYS2_Odlacznik"); });
                actions.Add(() => { _varsService.UnsubscribeVar(_SchemeVarsPrefix + "_Wylacznik"); });
            }

            _UnsubscribeTaskLocker = true;
            _varsService.ParallelUnsubscribeVars(actions, _SubscribeTask, () => { _UnsubscribeTaskLocker = false; });
        }

        public override void OnLoaded(object parameter = null)
        {
            base.OnLoaded();

            this.SubscribeVars();
        }

        public override void OnUnloaded(object parameter = null)
        {
            base.OnUnloaded();
            UnSubscribeVars();

            foreach(var elementWithPopup in _elementsWithPopup)
            {
                elementWithPopup.MouseEnter -= Mdv_MouseEnter;
                elementWithPopup.MouseLeave -= Mdv_MouseLeave;
            }

        }
    }
}
