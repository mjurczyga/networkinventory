﻿using eFOLZNON.Core.Attributes;
using VISU.Core.Services;
using VISU.Domain.Models.VarModels;
using VISU.Domain.Services;
using VISU.Domain.Services.RestServices;
using VISU.ServerCommunication.Models.Devices;
using VISU.ServerCommunication.Services;
using VISU.ViewModels.ViewModels;
using HADES_CUK_PNIOWEK.Controls;
using HADES_CUK_PNIOWEK.Models.PlcModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;

namespace ELPLATFORMA.CompensationComponents.ViewModels.EachDevice.Devices
{
    [AutoViewMapper("ETangoView")]
    public class ETangoViewModel : TangoMuzBaseViewModel
    {
        public object IO { get; private set; }

        public ETangoViewModel(IRegionManager regionManager, IVarsService varsService, IArchiveEventsService archiveEventsService) : base(regionManager, varsService, archiveEventsService)
        {

        }

        protected override void BuildSecureMenu()
        {
            var wrapPanel = new WrapPanel();

            var valueConverter = new TangoSecurityConverter();

            SelectedButton = this.GetSecureButton("Zabezpieczenia 1", nameof(DeviceSecureVars), 1, valueConverter);
            wrapPanel.Children.Add(SelectedButton);
            SelectedButton.SelectElement();
            wrapPanel.Children.Add(this.GetSecureButton("Zabezpieczenia 2", nameof(DeviceSecureVars), 2, valueConverter));
            wrapPanel.Children.Add(this.GetSecureButton("Zabezpieczenia 3", nameof(DeviceSecureVars), 3, valueConverter));
            wrapPanel.Children.Add(this.GetSecureButton("Zabezpieczenia 4", nameof(DeviceSecureVars), 4, valueConverter));
            SecureMenuContent = wrapPanel;
        }

        #region Events
        protected override void PrepareEventsList()
        {
            var properties = DeviceEvents.GetType().GetProperties();
            List<object> events = new List<object>();
            if (properties != null)
            {
                foreach (var eventProperty in properties)
                {
                    var singleEventValue = eventProperty.GetValue(DeviceEvents);
                    if (singleEventValue != null)
                        events.Add(new TangoEventViewModel(singleEventValue, 1));
                }
            }
            DeviceEventViewModels = events;
            RaisePropertyChanged("DeviceEventViewModels");
        }
        #endregion

        #region Bulid IO tab
        protected override void BuildIoTab()
        {
            UniformGrid uniformGrid = new UniformGrid() { Columns = 4 };

            //var Slot_A = new SectionBox();
            //var Slot_A_VarName = nameof(IO) + ".Slot_A";
            //Slot_A.SetBinding(SectionBox.TitleProperty, new Binding(Slot_A_VarName + ".Description"));
            //Slot_A.Content = this.GetIOContent(IO, Slot_A_VarName);

            //var Slot_B = new SectionBox();
            //var Slot_B_VarName = nameof(IO) + ".Slot_B";
            //Slot_B.SetBinding(SectionBox.TitleProperty, new Binding(Slot_B_VarName + ".Description"));
            //Slot_B.Content = this.GetIOContent(IO, Slot_B_VarName);

            //uniformGrid.Children.Add(this.CreateSectionBox(IO, nameof(IO), "Slot_A"));
            //uniformGrid.Children.Add(this.CreateSectionBox(IO, nameof(IO), "Slot_B"));
            //uniformGrid.Children.Add(this.CreateSectionBox(IO, nameof(IO), "Slot_C"));
            //uniformGrid.Children.Add(this.CreateSectionBox(IO, nameof(IO), "Slot_D"));
            //uniformGrid.Children.Add(this.CreateSectionBox(IO, nameof(IO), "Slot_E"));
            //uniformGrid.Children.Add(this.CreateSectionBox(IO, nameof(IO), "Slot_F"));
            //uniformGrid.Children.Add(this.CreateSectionBox(IO, nameof(IO), "Slot_G"));
            //uniformGrid.Children.Add(this.CreateSectionBox(IO, nameof(IO), "Slot_H"));
            //uniformGrid.Children.Add(this.CreateSectionBox(IO, nameof(IO), "Slot_I"));
            //uniformGrid.Children.Add(this.CreateSectionBox(IO, nameof(IO), "Slot_J"));
            //uniformGrid.Children.Add(this.CreateSectionBox(IO, nameof(IO), "Slot_K"));
            //uniformGrid.Children.Add(this.CreateSectionBox(IO, nameof(IO), "Slot_L"));
            //uniformGrid.Children.Add(this.CreateSectionBox(IO, nameof(IO), "Slot_M"));
            //uniformGrid.Children.Add(this.CreateSectionBox(IO, nameof(IO), "Slot_N"));
            //uniformGrid.Children.Add(this.CreateSectionBox(IO, nameof(IO), "Zlacze_W"));
            //IOContent = uniformGrid;
        }

        private SectionBox CreateSectionBox(object ioVars, string ioVarsName, string slotName)
        {
            var Slot = new SectionBox();
            var Slot_VarName = ioVarsName + "." + slotName;
            Slot.SetBinding(SectionBox.TitleProperty, new Binding(Slot_VarName + ".Description"));
            Slot.Content = this.GetIOContent(ioVars, Slot_VarName);

            return Slot;
        }

        private FrameworkElement GetIOContent(object IOStruct, string varPrefix)
        {
            WrapPanel wrapPanel = new WrapPanel();

            var ioProperties = IOStruct.GetType().GetProperties().Where(x => x.PropertyType == typeof(BaseVar));

            if (ioProperties != null)
            {
                foreach (var ioProperty in ioProperties)
                {
                    var baseVar = ioProperty.GetValue(IOStruct) as BaseVar;
                    if (baseVar != null)
                    {
                        wrapPanel.Children.Add(this.GetIOControl(varPrefix + "." + ioProperty.Name));
                    }
                }
            }

            var grid = new Grid();
            grid.Children.Add(wrapPanel);

            return grid;
        }

        private IOControl GetIOControl(string bindingVarName)
        {
            var result = new IOControl();
            result.SetBinding(IOControl.ValueProperty, new Binding(bindingVarName + ".Value"));
            result.SetBinding(IOControl.DescriptionProperty, new Binding(bindingVarName + ".Description"));


            return result;
        }
        #endregion Bulid IO tab

        protected override List<Action> PrepareSubscriptionTasks()
        {
            var actions = base.PrepareSubscriptionTasks();

            actions.Add(() => { IO = _varsService.SubscribeVar(_DeviceVarPrefix + "_Wejscia_wyjscia"); });

            return actions;
        }

        protected override List<Action> PrepareUnsubscriptionTasks()
        {
            var actions = base.PrepareUnsubscriptionTasks();
            actions.Add(() => { _varsService.UnsubscribeVar(_DeviceVarPrefix + "_Wejscia_wyjscia"); });

            return actions;
        }

        protected override void AllVarsSubscribed()
        {
            base.AllVarsSubscribed();

            if (IO != null)
                this.BuildIoTab();
        }
    }
}
