﻿using Autofac;
using ELPLATFORMA.CompensationComponents.Controls;
using ELPLATFORMA.CompensationComponents.Converters;
using ELPLATFORMA.CompensationComponents.Models.StaticModels;
using SchemeControlsLib.Controls.CommonControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;

namespace ELPLATFORMA.CompensationComponents.ViewModels.EachDevice.Devices
{
    public static class DeviceHelper
    {
        public static FrameworkElement GetIOContent(object IOStruct, string varPrefix)
        {
            WrapPanel wrapPanel = new WrapPanel();

            //var ioProperties = IOStruct.GetType().GetProperties().Where(x => x.PropertyType == typeof(BaseVar));

            //if (ioProperties != null)
            //{
            //    foreach (var ioProperty in ioProperties)
            //    {
            //        var baseVar = ioProperty.GetValue(IOStruct) as BaseVar;
            //        if (baseVar != null)
            //        {
            //           wrapPanel.Children.Add(GetIOControl(varPrefix + "." + ioProperty.Name));
            //        }
            //    }
            //}

            var grid = new Grid();
            grid.Children.Add(wrapPanel);

            return grid;
        }

        public static IOControl GetIOControl(string bindingVarName)
        {
            var result = new IOControl();
            result.SetBinding(IOControl.ValueProperty, new Binding(bindingVarName + ".Value"));
            result.SetBinding(IOControl.DescriptionProperty, new Binding(bindingVarName + ".Description"));


            return result;
        }

        public static FrameworkElement GetSecureContent(string sectionName, string varPath, int type, IValueConverter descriptionConverter)
        {
            Grid grid = new Grid();
            grid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto, SharedSizeGroup = "Row1_TextRow" });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

            TextBlock tb = new TextBlock() { Text = sectionName };
            var listView = new ListView();
            Grid.SetRow(listView, 1);
            var lvBinding = new Binding(varPath) { Converter = descriptionConverter, ConverterParameter = type };
            listView.SetBinding(ListView.ItemsSourceProperty, lvBinding);
            grid.Children.Add(tb);
            grid.Children.Add(listView);

            Border border = new Border();
            border.Child = grid;

            return border;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="varPath"></param>
        /// <param name="type"></param>
        /// <param name="descriptionConverter"></param>
        /// <param name="muzType">2 - ecoMuz, 3 - MultiMuz/Tango800</param>
        /// <returns></returns>
        public static SecureButton GetSecureButton(string text, string varPath, int type, IValueConverter descriptionConverter, int muzType) // 1 - Pobudzenia zabezpieczeń, 2 Zadziałanie zabezpieczeń, 3 - Zadziałanie zabezpieczeń z podtrzymaniem
        {
            var multiBinding = new MultiBinding() { Converter = new IntOrConverter() };
            multiBinding.Bindings.Add(new Binding(varPath + ".Pobudzenie_zabezpieczen_" + type));

            if (muzType > 2)
                multiBinding.Bindings.Add(new Binding(varPath + ".Zadzialanie_zabezpieczen_" + type));

            multiBinding.Bindings.Add(new Binding(varPath + ".Zadzialanie_zabezpieczen_z_podtrzymaniem_" + type));

            var sb = new SecureButton() { Text = text };
            sb.SetBinding(SecureButton.SecureValueProperty, multiBinding);


            UniformGrid uniformGrid = new UniformGrid() { Columns = muzType };
            Grid.SetIsSharedSizeScope(uniformGrid, true);

            uniformGrid.Children.Add(GetSecureContent("Pobudzenie zabezpieczeń", varPath + ".Pobudzenie_zabezpieczen_" + type, type, descriptionConverter));
            if (muzType > 2)
                uniformGrid.Children.Add(GetSecureContent("Zadziałanie zabezpieczeń", varPath + ".Zadzialanie_zabezpieczen_" + type, type, descriptionConverter));
            uniformGrid.Children.Add(GetSecureContent("Zadziałanie zabezpieczeń z podtrzymaniem", varPath + ".Zadzialanie_zabezpieczen_z_podtrzymaniem_" + type, type, descriptionConverter));

            sb.TabContent = uniformGrid;

            return sb;
        }

        public static IDeviceViewModel GetDeviceViewModel(IComponentContext componentContext, string deviceType)
        {
            IDeviceViewModel fieldVM = null;
            if (deviceType == "PECA")
                fieldVM = componentContext.Resolve<PECAViewModel>();
            //else if (deviceType == "Lumel N14")
            //    fieldVM = componentContext.Resolve<N14ViewModelTemp>();
            //else if (deviceType == "multiMUZ-3")
            //    fieldVM = componentContext.Resolve<MultiMuzViewModel>();
            // else if (deviceType == "eTANGO")
            //    fieldVM = componentContext.Resolve<ETangoViewModel>();
            if (deviceType == "ecoMUZ2")           
                  fieldVM = componentContext.Resolve<EcoMuz2ViewModel>();
            
            //else if (deviceType == "PAC3200")
            //    fieldVM = componentContext.Resolve<PAC3200ViewModel>();

            return fieldVM;
        }

        public static string GetGvlPrefix(string fieldVarsNamePrefix)
        {
            return fieldVarsNamePrefix != null && SwitchboardModels.KR2_Switchboards.Any(x => fieldVarsNamePrefix.ToUpper().Contains(x)) ?
                    GvlNames.KR2_SourceName + "_" + GvlNames.GVL_Synoptyka + "_" :
                    GvlNames.CukSourceName + "_" + GvlNames.GVL_Synoptyka + "_";
        }

        public static string GetDeviceVarsPrefix(string dviceTypeName, string varName, string buttonTag = null)
        {
            string deviceVarsPerfix;
            string sourcePrefix = GvlNames.CukSourceName;
            if (!string.IsNullOrEmpty(buttonTag))
                sourcePrefix = buttonTag;

            if (dviceTypeName == "PECA" || dviceTypeName == "Lumel N14")
                deviceVarsPerfix = sourcePrefix.ToUpper() == "KR2" ? sourcePrefix + "_" + GvlNames.GVL_Devices : GvlNames.GVL_SMB + "_" + varName;
            else
                deviceVarsPerfix = sourcePrefix + "_" + GvlNames.GVL_Devices + "_" + varName;

            return deviceVarsPerfix;
        }
    }
}
