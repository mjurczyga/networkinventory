﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;

namespace ELPLATFORMA.CompensationComponents.ViewModels.EachDevice.Devices
{
    public class MultiMuzViewModel : TangoMuzBaseViewModel
    {
        #region Scheme vars
        //public object MultiMuzEvents { get; private set; }
        public object MultiMuzInputs { get; private set; }
        public object MultiMuzOutputs { get; private set; }
        #endregion

        public MultiMuzViewModel(IRegionManager regionManager, IVarsService varsService, IArchiveEventsService archiveEventsService) : base(regionManager, varsService, archiveEventsService)
        {

        }

        protected override void BuildSecureMenu()
        {
            var wrapPanel = new WrapPanel();

            var valueConverter = new MultiMuz3SecurityConverter();

            SelectedButton = this.GetSecureButton("Zabezpieczenia 1", nameof(DeviceSecureVars), 1, valueConverter);
            wrapPanel.Children.Add(SelectedButton);
            SelectedButton.SelectElement();
            wrapPanel.Children.Add(this.GetSecureButton("Zabezpieczenia 2", nameof(DeviceSecureVars), 2, valueConverter));
            wrapPanel.Children.Add(this.GetSecureButton("Zabezpieczenia 3", nameof(DeviceSecureVars), 3, valueConverter));
            SecureMenuContent = wrapPanel;
        }

        #region Events
        protected override void PrepareEventsList()
        {
            var properties = DeviceEvents.GetType().GetProperties();
            List<object> events = new List<object>();

            if (properties != null)
            {
                foreach (var eventProperty in properties)
                {
                    var singleEventValue = eventProperty.GetValue(DeviceEvents);
                    if (singleEventValue != null)
                        events.Add(new MuzEventViewModel(singleEventValue, 1));
                }
            }

            DeviceEventViewModels = events;
            RaisePropertyChanged("DeviceEventViewModels");
        }
        #endregion

        #region Bulid IO tab
        protected override void BuildIoTab()
        {
            UniformGrid uniformGrid = new UniformGrid() { Columns = 2 };

            var inputs = new SectionBox() { Title = "Wejścia" };
            inputs.Content = DeviceHelper.GetIOContent(MultiMuzInputs, nameof(MultiMuzInputs));
            var outputs = new SectionBox() { Title = "Wyjścia" };
            outputs.Content = DeviceHelper.GetIOContent(MultiMuzOutputs, nameof(MultiMuzOutputs));

            uniformGrid.Children.Add(inputs);
            uniformGrid.Children.Add(outputs);
            IOContent = uniformGrid;
        }
        #endregion Bulid IO tab

        protected override List<Action> PrepareSubscriptionTasks()
        {
            var actions = base.PrepareSubscriptionTasks();

            actions.Add(() => { MultiMuzInputs = _varsService.SubscribeVar(_DeviceVarPrefix + "_Wejscia"); });
            actions.Add(() => { MultiMuzOutputs = _varsService.SubscribeVar(_DeviceVarPrefix + "_Wyjscia"); });

            return actions;
        }

        protected override List<Action> PrepareUnsubscriptionTasks()
        {
            var actions = base.PrepareUnsubscriptionTasks();
            actions.Add(() => { _varsService.UnsubscribeVar(_DeviceVarPrefix + "_Wejscia"); });
            actions.Add(() => { _varsService.UnsubscribeVar(_DeviceVarPrefix + "_Wyjscia"); });

            return actions;
        }

        protected override void AllVarsSubscribed()
        {
            base.AllVarsSubscribed();

            if (MultiMuzInputs != null && MultiMuzOutputs != null)
                this.BuildIoTab();
        }
    }
}
