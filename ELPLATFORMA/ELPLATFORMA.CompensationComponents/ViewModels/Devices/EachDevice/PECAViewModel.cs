﻿using ELPLATFORMA.CompensationComponents.Attributes;
using ELPLATFORMA.CompensationComponents.Controls;
using ELPLATFORMA.CompensationComponents.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Threading;

namespace ELPLATFORMA.CompensationComponents.ViewModels.EachDevice.Devices
{
    //[AutoViewMapper("PECAView")]
    public class PECAViewModel : BaseViewModel, IDeviceViewModel
    {
        private Dispatcher _Dispatcher = Dispatcher.CurrentDispatcher;

        private string _VarName;
        private string _SchemeVarsPrefix;
        private SectionBox _selectedSectionBox = null;


        #region Scheme vars
        public object SYS1_Odlacznik { get; private set; }
        public object SYS2_Odlacznik { get; private set; }
        public object OdlacznikKablowy { get; private set; }
        public object Wylacznik { get; private set; }

        #endregion

        public object MeasurementVars { get; private set; }
        public string SchemePath { get; private set; }

        public object VarsContent { get; private set; }
        public object OthersContent { get; private set; }

        public Visibility LoadingCircleVisibility { get; private set; }
        private void SetVisibility(bool boolVal)
        {
            if (boolVal)
            {
                LoadingCircleVisibility = Visibility.Collapsed;
            }
            else
            {
                LoadingCircleVisibility = Visibility.Visible;
            }
        }

        public PECAViewModel(IRegionManager regionManager)
        {
            this.SetVisibility(false);
        }
        
        public void InitViewModel(string schemeVarsPrefix, string deviceVarPrefix, string schemePath, string windowTitle)
        {
            _VarName = deviceVarPrefix;
            _SchemeVarsPrefix = schemeVarsPrefix;
            SchemePath = "Schemes/FieldSchemes/" + schemePath;            
        }

        private void CollapseSectionBoxes()
        {
            foreach(var sb in _AddedSectionBox)
            {
                sb.IsMenuElement = true;

                if (sb.Title != "Moce")
                    sb.CollapseContent();
                else
                {
                    sb.ShowContent();
                    _selectedSectionBox = sb;
                }
            }
        }

        private List<SectionBox> _AddedSectionBox = new List<SectionBox>();
        private void GenerateContent(object vars)
        {
            var varsProperties = vars?.GetType().GetProperties();

            var prady = varsProperties.Where(x => x.Name.Contains("Prad"));


            var moce = varsProperties.Where(x => x.Name.Contains("Moc"));

            var allnapiecia = varsProperties.Where(x => x.Name.Contains("Napiecie"));

            var napieciaMiedzyfazowe = allnapiecia.Where(x => x.Name.Contains("Napiecie_miedzyfazowe")); 
 
            var napiecia = allnapiecia.Where(x => !x.Name.Contains("Napiecie_miedzyfazowe"));


            var energie = varsProperties.Where(x => x.Name.Contains("Energia"));
            var reszta = varsProperties.Where(x => !x.Name.Contains("Napiecie") && !x.Name.Contains("Prad") && !x.Name.Contains("Moc") && !x.Name.Contains("Energia"));

            StackPanel stackPanel = new StackPanel();

            if (GetSectionBox(vars, napiecia, "Napięcia", typeof(StackPanel), out SectionBox sectionBoxNapiecia))
            {
                sectionBoxNapiecia.MouseDown += SectionBoxNapiecia_MouseDown;                
                _AddedSectionBox.Add(sectionBoxNapiecia);
                stackPanel.Children.Add(sectionBoxNapiecia);
            }
            if (GetSectionBox(vars, napieciaMiedzyfazowe, "Napięcia międzyfazowe ", typeof(StackPanel), out SectionBox sectionBoxNapieciaMf))
            {
                sectionBoxNapieciaMf.MouseDown += SectionBoxNapiecia_MouseDown;
                _AddedSectionBox.Add(sectionBoxNapieciaMf);
                stackPanel.Children.Add(sectionBoxNapieciaMf);
            }
            if (GetSectionBox(vars, prady, "Prądy", typeof(StackPanel), out SectionBox sectionBoxPrady))
            {
                sectionBoxPrady.MouseDown += SectionBoxNapiecia_MouseDown;
                _AddedSectionBox.Add(sectionBoxPrady);
                stackPanel.Children.Add(sectionBoxPrady);
            }
            if (GetSectionBox(vars, moce, "Moce", typeof(StackPanel), out SectionBox sectionBoxMoce))
            {
                sectionBoxMoce.MouseDown += SectionBoxNapiecia_MouseDown;
                _AddedSectionBox.Add(sectionBoxMoce);
                stackPanel.Children.Add(sectionBoxMoce);
            }
            if (GetSectionBox(vars, energie, "Energie", typeof(StackPanel), out SectionBox sectionBoxEnergie))
            {
                sectionBoxEnergie.MouseDown += SectionBoxNapiecia_MouseDown;
                _AddedSectionBox.Add(sectionBoxEnergie);
                stackPanel.Children.Add(sectionBoxEnergie);
            }
            if (GetSectionBox(vars, reszta, "Pozostałe", typeof(UniformGrid), out SectionBox sectionBoxPozostale, Orientation.Horizontal))
            {
                OthersContent = sectionBoxPozostale;
            }

            VarsContent = stackPanel;
        }

        private void SectionBoxNapiecia_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (sender is SectionBox sb)
            {
                _selectedSectionBox?.CollapseContent();
                sb.ShowContent();
                _selectedSectionBox = sb;
            }
        }

        private bool GetSectionBox(object vars, IEnumerable<PropertyInfo> sectionProperties, string sectionName, Type contentControlType, out SectionBox sectionBox, Orientation spOrientation = Orientation.Vertical)
        {
            sectionBox = null;
            int propertiesCount = sectionProperties.Count();

            if(vars != null && sectionProperties != null && propertiesCount > 0 && contentControlType != null)
            {
                var contentControlInstance = Activator.CreateInstance(contentControlType);

                if (contentControlInstance is Panel panel)
                {
                    if (panel is StackPanel stackPanel)
                    {
                        stackPanel.Orientation = spOrientation;
                    }
                    else if (panel is UniformGrid uniformGrid)
                    {
                        if(spOrientation == Orientation.Vertical)
                            uniformGrid.Rows = sectionProperties.Count();
                        else
                            uniformGrid.Columns = sectionProperties.Count();
                    }

                    foreach (var property in sectionProperties)
                    {
                        var baseVar = property.GetValue(vars);
                        if (baseVar != null)
                        {
                            if (GetMeasureControl(baseVar, out MeasurementDataViewer measurementDataViewer))
                                panel.Children.Add(measurementDataViewer);
                        }
                    }

                    sectionBox = new SectionBox() { Title = sectionName };               
                    //ScrollViewer scrollViewer = new ScrollViewer() { Content = panel,
                        //VerticalScrollBarVisibility = ScrollBarVisibility.Auto,
                        //HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled };

                    //panel.SetBinding(StackPanel.HeightProperty, new Binding("ActualHeight") { Source = panel } );

                    sectionBox.Content = panel;

                    return true;
                }
            }
            return false;
        }

        private bool GetMeasureControl(object obj, out MeasurementDataViewer measurementDataViewer)
        {
            measurementDataViewer = null;
            //if (obj is BaseVar baseVar)
            //{
            //    measurementDataViewer = new MeasurementDataViewer();
            //    measurementDataViewer.SetBinding(MeasurementDataViewer.BaseVarProperty, this.GetBinding(nameof(MeasurementVars) + "." + baseVar.Name));
            //    //measurementDataViewer.SetBinding(MeasurementDataViewer.DescriptionProperty, this.GetBinding(nameof(MeasurementVars) + "." + baseVar.Name + ".Description")); 
            //    //measurementDataViewer.SetBinding(MeasurementDataViewer.UnitProperty, this.GetBinding(nameof(MeasurementVars) + "." + baseVar.Name + ".Unit")); 
            //    //measurementDataViewer.SetBinding(MeasurementDataViewer.ValueProperty, this.GetBinding(nameof(MeasurementVars) + "." + baseVar.Name + ".Value"));
            //    return true;
            //}

            return false;
        }

        private Binding GetBinding(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                Binding binding = new Binding(path);
                binding.Source = this;
                binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                return binding;
            }
            return null;
        }
 
        public override void OnLoaded(object parameter = null)
        {
            base.OnLoaded();

        }

        public override void OnUnloaded(object parameter = null)
        {

            foreach(var sb in _AddedSectionBox)
            {
                sb.MouseDown -= SectionBoxNapiecia_MouseDown;
            }

            base.OnUnloaded();

        }
    }
}
