﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Interop;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Windows.Threading;
using ELPLATFORMA.CompensationComponents.Services;

namespace Animation_Sample_For_RDP
{
    public class RDPSesionService : IRDPSesionService, INotifyPropertyChanged
    {
        string _RemoteDesktopStatus;
        bool _IsRemoteDesktopSession;
        public RDPSesionService()
        {
        }
        public RDPSesionService(string value)
        {
            this.RemoteDesktopStatus = value;
        }
        public string RemoteDesktopStatus
        {
            get { return _RemoteDesktopStatus; }
            set
            {
                _RemoteDesktopStatus = value;
                OnPropertyChanged("RemoteDesktopStatus");
            }
        }
        public bool IsRemoteDesktopSession
        {
            get { return _IsRemoteDesktopSession; }
            set
            {
                _IsRemoteDesktopSession = value;
                OnPropertyChanged("IsRemoteDesktopSession");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;  //OnPropertyChanged -update property value in binding
        private void OnPropertyChanged(String info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }
    }
}