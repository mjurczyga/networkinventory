﻿using Askom.AddonManager;
using ELPLATFORMA.CompensationComponents.Attributes;
using ELPLATFORMA.CompensationComponents.Controls;
using ELPLATFORMA.CompensationComponents.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ELPLATFORMA.CompensationComponents.Services
{
    public class EvoVarsService : ServiceBase, IEvoVarsService
    {
        private EvoObjectExtension _evoObjectExtension = null;

        private static readonly object _varsLocker = new object();
        public Dictionary<string, EvoVarModel> Vars { get; protected set; } = new Dictionary<string, EvoVarModel>();

        private Dictionary<string, int> _subscriptionCounters = new Dictionary<string, int>();

        public EvoVarsService()
        {

        }

        public void ReportMessage(AddonMessageType aType, string aMessage)
        {
            if (_evoObjectExtension != null)
                _evoObjectExtension.ReportMessageExt(aType, aMessage);
        }

        public void InitService(EvoObjectExtension evoObjectExtension)
        {

            _evoObjectExtension = evoObjectExtension;
            _evoObjectExtension.ResetEvent += _evoObjectExtension_ResetEvent;
            _evoObjectExtension.DisconnectEvent += _evoObjectExtension_DisconnectEvent;
            _evoObjectExtension.VarStateChangedEvent += _evoObjectExtension_VarStateChangedEvent;
            _evoObjectExtension.EvoObjectPropertyChangedEvent += _evoObjectExtension_EvoObjectPropertyChangedEvent;
            _evoObjectExtension.ReportMessageExt(AddonMessageType.Info, "[EvoVarsService] Zainicjalizowano serwis. Przypisano EvoObject do serwisu.");
        }

        public bool IsAsixControlEditMode()
        {
            if (_evoObjectExtension != null)
                return _evoObjectExtension.IsEditMode;
            return false;
        }

        public void RefreshEvoProperties()
        {
            if(_evoObjectExtension != null)
            {
                var evoProperties = _evoObjectExtension.GetType().GetProperties().Where(x => x.GetCustomAttributes(true).OfType<EvoObjectPropertyAttribute>().Count() > 0); 
                foreach (var prop in evoProperties)
                {
                    var tempValue = prop.GetValue(_evoObjectExtension);
                    if(prop.PropertyType == typeof(string))
                        prop.SetValue(_evoObjectExtension, null);
                    else if (prop.PropertyType == typeof(int))
                        prop.SetValue(_evoObjectExtension, 0);
                    else if (prop.PropertyType == typeof(double))
                        prop.SetValue(_evoObjectExtension, 0.0);
                    else if (prop.PropertyType == typeof(bool))
                        prop.SetValue(_evoObjectExtension, false);

                    prop.SetValue(_evoObjectExtension, tempValue);
                }
            }
        }

        public void TestClick()
        {
            if(_evoObjectExtension != null)
            {
                _evoObjectExtension.SchemeClick();
            }
        }

        public void RaiseEvoAction(string actionString)
        {
            Askom.AddonManager.AsixEvo.Application.ExecuteAction(actionString, out string message);
            if (_evoObjectExtension != null)
                _evoObjectExtension.ReportMessageExt(AddonMessageType.Error, "[Schemat kompensacji] " + message);
        }

        public object GetEvoPropertyValue(string propertyName)
        {
            object result = null;
            if (_evoObjectExtension != null)
            {
                var evoProperty = _evoObjectExtension.GetEvoObjectProperty(propertyName);//_evoObjectExtension.GetType().GetProperties().Where(x => x.GetCustomAttributes(true).OfType<EvoObjectPropertyAttribute>().Count() > 0 && x.Name == propertyName).FirstOrDefault();
                if (evoProperty != null)
                {
                    result = evoProperty; // evoProperty.GetValue(_evoObjectExtension);
                }
            }
            return result;
        }

        public event EvoObjectPropertyChangedEventHandler EvoObjectPropertyChangedEvent;

        private void _evoObjectExtension_EvoObjectPropertyChangedEvent(object sender, EvoObjectPropertyChangedEventArgs evoObjectPropertyChangedEventArgs)
        {
            EvoObjectPropertyChangedEvent?.Invoke(sender, evoObjectPropertyChangedEventArgs);
        }

        private void _evoObjectExtension_VarStateChangedEvent(string aVariableName, VariableState aVariableState)
        {
            lock (_varsLocker)
            {
                if (Vars.ContainsKey(aVariableName) && Vars[aVariableName] is EvoVarModel evoVarModel)
                {
                    if (aVariableState.IsGood)
                        evoVarModel.UpdateProperties(aVariableState);
                    else
                        evoVarModel.Value = null; //TODO: TESTY

                }
            }
        }

        private void _evoObjectExtension_DisconnectEvent(object sender, EventArgs e)
        {
            lock (_varsLocker)
            {
                _evoObjectExtension.UnregisterAllRegisteredVars();
            }
        }

        private void _evoObjectExtension_ResetEvent(object sender, EventArgs e)
        {
            lock (_varsLocker)
            {
                foreach (var var in Vars)
                {
                    bool result = _evoObjectExtension.RegisterVar(var.Key);
                }
            }
        }

        public void RefreshSubscription()
        {
            lock (_varsLocker)
            {
                _evoObjectExtension.Reset();
            }
        }

        #region Subscription
        public EvoVarModel SubscribeVar(string varName)
        {
            if(_evoObjectExtension != null)
            {
                #region Mechanizm subskrypcji
                lock (_varsLocker)
                {
                    if (_subscriptionCounters.ContainsKey(varName))
                    {
                        _subscriptionCounters[varName] += 1;
                        return Vars[varName];
                    }
                    else
                    {
                        _subscriptionCounters.Add(varName, 1);
                        if (!Vars.ContainsKey(varName))
                        {
                            VariableState variableState = _evoObjectExtension.GetVarState(varName);
                            if (variableState != null)
                            {
                                Vars.Add(varName, new EvoVarModel(variableState));
                                var result = _evoObjectExtension.RegisterVar(varName);
                                return Vars[varName];
                            }
                        }

                    }
                    #endregion Mechanizm subskrypcji
                }
            }

            else          
                MessageBox.Show("[EvoVarsService] Brak inicjalizacji serwisu.");
            
            return null;
        }

        public void UnsubscribeVar(string varName)
        {
            if (_evoObjectExtension != null)
            {
                #region Mechanizm subskrypcji
                if (_subscriptionCounters.ContainsKey(varName))
                {
                    _subscriptionCounters[varName] -= 1;
                    if (_subscriptionCounters[varName] == 0)
                    {
                        _evoObjectExtension.UnregisterVar(varName);
                        _subscriptionCounters.Remove(varName);
                        lock (_varsLocker)
                            if (Vars.ContainsKey(varName))
                                Vars.Remove(varName);
                    }
                }
                else
                {
                    _evoObjectExtension.UnregisterVar(varName);
                }
                #endregion Mechanizm subskrypcji               
            }

            else
            {
                MessageBox.Show("[EvoVarsService] Brak inicjalizacji serwisu.");
            }
        }
        #endregion Subscription

        #region variable methods
        public object GetVarAttribute(string varName, string attributeName)
        {
            if (_evoObjectExtension != null)
            {
                return _evoObjectExtension.GetVarAttribute(varName, attributeName);
            }
            else
                MessageBox.Show("[EvoVarsService] Brak inicjalizacji serwisu.");

            return null;
        }
        #endregion variable methods

        protected override void OnApplicationClosing(ApplicationClosingMessage applicationClosingMessage)
        {
            base.OnApplicationClosing(applicationClosingMessage);
            Vars.Clear();
            _subscriptionCounters.Clear();
        }
    }
}