﻿using ELPLATFORMA.CompensationComponents.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;

namespace ELPLATFORMA.CompensationComponents.Services
{
    [ContainerRegistration(true)]
    public class DataTemplateFactory : ServiceBase
    {
        private IEnumerable<Type> _types;

        public DataTemplateFactory()
        {
        }

        private IEnumerable<Type> LoadTypes()
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                       .SelectMany(t => t.GetTypes());
        }

        private void Load()
        {
            foreach (Type type in _types)
                ProcessType(type);
        }

        private void ProcessType(Type type)
        {
            foreach (Attribute attribute in type.GetCustomAttributes(false))
                if (attribute is AutoViewMapper)
                    ProcessAttribute(type, attribute as AutoViewMapper);
        }

        private void ProcessAttribute(Type type, AutoViewMapper autoViewMapper)
        {
            if (autoViewMapper != null)
            {
                var vmName = type.Name;

                Type view = _types.SingleOrDefault(v => v.Name == autoViewMapper.View);

                if (view != null)
                {
                    var template = createTemplate(type, view);

                    var key = template.DataTemplateKey;

                    if (!Application.Current.Resources.Contains(key))
                    {
                        Application.Current.Resources.Add(key, template);
                        //Log.Debug($"Zmapowano '{type.Name}' do widoku '{view.Name}'.");
                    }
                    else
                    {
                        //Log.Debug($"Pominięto mapowanie '{type.Name}' do widoku '{view.Name}' - DataTemplate już istnieje");
                    }
                }
            }
        }

        public void InitMapping()
        {
            _types = LoadTypes();
            Load();
        }

        public void BindViews(Type viewModelType, Type viewType)
        {
            var template = createTemplate(viewModelType, viewType);

            var key = template.DataTemplateKey;

            if (Application.Current.Resources.Contains(key))
                Application.Current.Resources.Remove(key);

            if (!Application.Current.Resources.Contains(key))
            {
                Application.Current.Resources.Add(key, template);
                //Log.Debug($"Zmapowano '{viewModelType.Name}' do widoku '{viewType.Name}'.");
            }
            else
            {
                //Log.Debug($"Pominięto mapowanie '{viewModelType.Name}' do widoku '{viewType.Name}' - DataTemplate już istnieje");
            }
        }

        private DataTemplate createTemplate(Type viewModelType, Type viewType)
        {
            const string xamlTemplate = "<DataTemplate DataType=\"{{x:Type vm:{0}}}\"><v:{1} /></DataTemplate>";
            var xaml = String.Format(xamlTemplate, viewModelType.Name, viewType.Name, viewModelType.Namespace, viewType.Namespace);

            var context = new ParserContext();

            context.XamlTypeMapper = new XamlTypeMapper(new string[0]);
            context.XamlTypeMapper.AddMappingProcessingInstruction("vm", viewModelType.Namespace, viewModelType.Assembly.FullName);
            context.XamlTypeMapper.AddMappingProcessingInstruction("v", viewType.Namespace, viewType.Assembly.FullName);

            context.XmlnsDictionary.Add("", "http://schemas.microsoft.com/winfx/2006/xaml/presentation");
            context.XmlnsDictionary.Add("x", "http://schemas.microsoft.com/winfx/2006/xaml");
            context.XmlnsDictionary.Add("vm", "vm");
            context.XmlnsDictionary.Add("v", "v");

            var template = (DataTemplate)XamlReader.Parse(xaml, context);
            return template;
        }
    }
}
