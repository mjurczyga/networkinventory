﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELPLATFORMA.CompensationComponents.Services
{
    public interface IRDPSesionService
    {
        bool IsRemoteDesktopSession { get; set; }
        string RemoteDesktopStatus { get; set; }
    }
}
