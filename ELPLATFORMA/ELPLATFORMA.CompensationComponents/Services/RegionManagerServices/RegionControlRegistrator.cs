﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ELPLATFORMA.CompensationComponents.Services.RegionRegistrator
{
    public static class RegionControlRegistrator
    {
        private static IComponentContext _componentContext;
        private static IRegionManager _regionManager;

        public static void SetContainer(IComponentContext componentContext)
        {
            _componentContext = componentContext;
            _regionManager = _componentContext.Resolve<IRegionManager>();
        }

        public static void Register(this Control senderControl, object sender, RoutedEventArgs e)
        {

            if (sender is ContentControl)
            {
                ContentControl regionControl = sender as ContentControl;

                _regionManager.RegisterRegion(regionControl);

                if (!string.IsNullOrEmpty(regionControl.Name))
                {
                    if (!regionControl.Name.StartsWith("_"))
                    {
                        var viewModelType = GetViewModelType(regionControl.Name);
                        if (viewModelType != null)
                        {
                            _regionManager.SwitchRegion(regionControl.Uid, _componentContext.Resolve(viewModelType));
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Próba zarejestrowania regionu innego typu niż ContentControl.");
                //Log.Error("Próba zarejestrowania regionu innego typu niż ContentControl.");
            }
        }

        private static Type GetViewModelType(string viewModel)
        {
            Type type = AppDomain.CurrentDomain.GetAssemblies()
                                   .SelectMany(x => x.GetTypes())
                                   .FirstOrDefault(x => x.Name == viewModel);
            return type;
        }
    }
}
