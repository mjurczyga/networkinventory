﻿using ELPLATFORMA.CompensationComponents.Models;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ELPLATFORMA.CompensationComponents.Services
{
    public class ServiceBase : INotifyPropertyChanged
    {
        public ServiceBase()
        {
            Messenger.Default.Register<ApplicationClosingMessage>(this, OnApplicationClosing);
        }

        protected virtual void OnApplicationClosing(ApplicationClosingMessage applicationClosingMessage)
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
