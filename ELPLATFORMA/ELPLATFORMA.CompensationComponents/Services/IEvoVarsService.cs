﻿using Askom.AddonManager;
using ELPLATFORMA.CompensationComponents.Controls;
using ELPLATFORMA.CompensationComponents.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELPLATFORMA.CompensationComponents.Services
{

    public interface IEvoVarsService
    {
        void TestClick();
        void RaiseEvoAction(string actionString);

        Dictionary<string, EvoVarModel> Vars { get; }
        EvoVarModel SubscribeVar(string varName);
        void UnsubscribeVar(string varName);
        void InitService(EvoObjectExtension evoObjectExtension);
        void RefreshEvoProperties();
        void RefreshSubscription();

        object GetEvoPropertyValue(string propertyName);

        event EvoObjectPropertyChangedEventHandler EvoObjectPropertyChangedEvent;

        void ReportMessage(AddonMessageType aType, string aMessage);

        bool IsAsixControlEditMode();
    }
}
