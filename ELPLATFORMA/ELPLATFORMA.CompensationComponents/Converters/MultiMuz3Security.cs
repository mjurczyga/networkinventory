﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ELPLATFORMA.CompensationComponents.Converters
{
    public class MultiMuz3SecurityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = new List<string>();

            int bitNumber = 0;
            if (uint.TryParse(value?.ToString(), out uint intVal) && int.TryParse(parameter?.ToString(), out int intParameter))
            {
                byte[] bytes = BitConverter.GetBytes(intVal);
                BitArray bits = new BitArray(bytes);

                foreach (var bit in bits)
                {
                    if (bool.TryParse(bit?.ToString(), out bool resultBool) && resultBool)
                    {
                        if (intParameter == 1 && MultiMuz3SecurityDicts.Zab1.TryGetValue(bitNumber, out string zab1))
                            result.Add(zab1);
                        else if (intParameter == 2 && MultiMuz3SecurityDicts.Zab2.TryGetValue(bitNumber, out string zab2))
                            result.Add(zab2);
                        else if (intParameter == 3 && MultiMuz3SecurityDicts.Zab3.TryGetValue(bitNumber, out string zab3))
                            result.Add(zab3);

                    }
                    bitNumber++;
                }

                return result;

            }

            return result;
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public static class MultiMuz3SecurityDicts
    {
        public static Dictionary<int, string> Zab1 = new Dictionary<int, string>()
        {
            { 0, "Zabezpieczenie zwarciowe"},
            { 1, "Zabezpieczenie nadprądowe niezależne 1"},
            { 2, "Zabezpieczenie nadprądowe niezależne 2"},
            { 3, "Zabezpieczenie nadprądowe niezależne 3"},
            { 4, "Zabezpieczenie nadprądowe"},
            { 5, "Zabezpieczenie od asymetrii"},
            { 6, "Zabezpieczenia cieplne (Q> %UP) (tylko w Pob_Zab1)"},
            { 7, "Zabezpieczenia cieplne (Q> %OTW) (tylko w"},
            { 8, "Zabezpieczenie różnicowe"},
            { 9, "Zabezpieczenie od utyku wirnika"},
            { 10, "Zabezpieczenie podprądowe"},
            { 11, "Zabezpieczenie ziemnozwarciowe 1"},
            { 12, "Zabezpieczenie ziemnozwarciowe 2"},
            { 13, "Zabezpieczenie ziemnozwarciowe kierunkowe"},
            { 14, "Zabezpieczenie admitancyjne"},
            { 15, "Zabezpieczenie admitancyjne kierunkowe 1"},
            { 16, "Zabezpieczenie admitancyjne kierunkowe 2"},
            { 17, "Zabezpieczenie nadnapięciowe U0"},
            { 18, "Zabezpieczenie podnapięciowe 1"},
            { 19, "Zabezpieczenie podnapięciowe 2"},
            { 20, "Zabezpieczenie nadnapięciowe 1"},
            { 21, "Zabezpieczenie nadnapięciowe 2"},
            { 22, "Zabezpieczenie zwrotnomocowe P"},
            { 23, "Zabezpieczenie zwrotnomocowe Q"},
            { 24, "Zabezpieczenie od prądu wewnętrznego baterii kondensatorów"},
            { 25, "Zabezpieczenia ziemnozwarciowe TU I ST."},
            { 26, "Zabezpieczenie ziemnozwarciowe TU II ST. t1"},
            { 27, "Zabezpieczenie ziemnozwarciowe TU II ST. t2"},
            { 28, "Zabezpieczenie ziemnozwarciowe TU II ST. t3"},

        };


        public static Dictionary<int, string> Zab2 = new Dictionary<int, string>()
        {
            { 0, "Zabezpieczenie termiczne 1"},
            { 1, "Zabezpieczenie termiczne 2"},
            { 2, "Zabezpieczenie gaz.-przepł. I st. trafo BTQ"},
            { 3, "Zabezpieczenie gaz.-przepł. II st. trafo BTV"},
            { 4, "Zabezpieczenie gaz.-przepł. I st. dławika BDQ"},
            { 5, "Zabezpieczenie gaz.-przepł. II st. dławika BDV"},
            { 6, "Zabezpieczenie gaz.-przepł. przełącznika zaczepów GPP"},
            { 7, "Zabezpieczenie różnicowe zewnętrzne"},
            { 8, "Zabezpieczenie odległościowe zewnętrzne"},
            { 9, "Zabezpieczenie zewnętrzne"},
            { 10, "Zabezpieczenia od zbyt długiego rozruchu (tylko w Zad_Zab2 i Zad_Zab_P2)"},
            { 11, "Rozbrojenie napędu"},
            { 12, "Niezgodność stanu łącznika"},
            { 13, "Ciągłość obwodu wyłączania 1 - COW1"},
            { 14, "Ciągłość obwodu wyłączania 2 - COW2"},
            { 15, "Ciągłość obwodu załączania - COZ"},
            { 16, "Lokalna Rezerwa Wyłącznikowa (wejścia)"},
            { 17, "Kontrola zużycia styków wyłącznika"},
            { 18, ""},
            { 19, "Błąd RTD"},
            { 20, "RTD1 ALARM"},
            { 21, "RTD2 ALARM"},
            { 22, "RTD3 ALARM"},
            { 23, "RTD4 ALARM"},
            { 24, "RTD5 ALARM"},
            { 25, "RTD6 ALARM"},
            { 26, "RTD1 OTWARCIE"},
            { 27, "RTD2 OTWARCIE"},
            { 28, "RTD3 OTWARCIE"},
            { 29, "RTD4 OTWARCIE"},
            { 30, "RTD5 OTWARCIE"},
            { 31, "RTD6 OTWARCIE"},
        };


        public static Dictionary<int, string> Zab3 = new Dictionary<int, string>()
        {
            { 0, "Zabezpieczenie technologiczne użytkownika 1"},
            { 1, "Zabezpieczenie technologiczne użytkownika 2"},
            { 2, "Zabezpieczenie technologiczne użytkownika 3"},
            { 3, "Zabezpieczenie technologiczne użytkownika 4"},
            { 4, "Zabezpieczenie technologiczne użytkownika 5"},
            { 5, "Zabezpieczenie technologiczne użytkownika 6"},
            { 6, "Zabezpieczenie technologiczne użytkownika 7"},
            { 7, "Zabezpieczenie technologiczne użytkownika 8"},
            { 8, "Zabezpieczenie technologiczne użytkownika 9"},
            { 9, "Zabezpieczenie technologiczne użytkownika 10"},
            { 10, "Zabezpieczenie technologiczne użytkownika 11"},
            { 11, "Zabezpieczenie technologiczne użytkownika 12"},
            { 12, ""},
            { 13, "Zabezpieczenie łukowe – zadziałanie czujnika łuku (tyko Zad_Zab3, Zad_Zab_P3)"},
            { 14, "Zabezpieczenie łukowe – otwarcie z innego pola (tyko Zad_Zab3, Zad_Zab_P3)"},
            { 15, ""},
            { 16, "Zabezpieczenie podczęstotliwościowe 1"},
            { 17, "Zabezpieczenie podczęstotliwościowe 2"},
            { 18, "Zabezpieczenie podczęstotliwościowe 3"},
            { 19, "Zabezpieczenie nadczęstotliwościowe"},
            { 20, "Zabezpieczenie nadprądowe składowej zerowej AWSC"},
            { 21, "Zabezpieczenie nadnapięciowe składowej zerowej AWSC"},
        };

    }
}
