﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ELPLATFORMA.CompensationComponents.Converters
{
    public class SimpleMathConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter != null && parameter is string && value != null)
            {
                var expression = parameter?.ToString();

                if (double.TryParse(expression.Remove(0, 1)?.Replace(".", ","), out double expressionValue) &&
                    double.TryParse(value?.ToString().Replace(".", ","), out double doubleValue))
                {
                    if (expression.Contains("*"))
                    {
                        var ase = doubleValue * expressionValue;
                        return Math.Round(doubleValue * expressionValue);
                    }
                    else if (expression.Contains("-"))
                    {
                        return doubleValue - expressionValue;
                    }

                    else if (expression.Contains("/"))
                    {
                        return Math.Round(doubleValue / expressionValue, 2).ToString("0.00");
                    }

                    else if (expression.Contains("+"))
                    {
                        return doubleValue + expressionValue;
                    }
                }

                if (double.TryParse(parameter?.ToString(), out double resultParam) && double.TryParse(value?.ToString(), out double resultVal))
                    return resultParam * resultVal;
            }

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
