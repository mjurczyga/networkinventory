﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ELPLATFORMA.CompensationComponents.Converters
{
    public class EcoMuz2SecurityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = new List<string>();

            int bitNumber = 0;
            if (uint.TryParse(value?.ToString(), out uint intVal) && int.TryParse(parameter?.ToString(), out int intParameter))
            {
                byte[] bytes = BitConverter.GetBytes(intVal);
                BitArray bits = new BitArray(bytes);

                foreach (var bit in bits)
                {
                    if (bool.TryParse(bit?.ToString(), out bool resultBool) && resultBool)
                    {
                        if (intParameter == 1 && EcoMuz2SecurityDicts.Zab1.TryGetValue(bitNumber, out string zab1))
                            result.Add(zab1);
                        else if (intParameter == 2 && EcoMuz2SecurityDicts.Zab2.TryGetValue(bitNumber, out string zab2))
                            result.Add(zab2);

                    }
                    bitNumber++;
                }

                return result;

            }

            return result;
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public static class EcoMuz2SecurityDicts
    {
        public static Dictionary<int, string> Zab1 = new Dictionary<int, string>()
        {
            { 0, "Zabezpieczenie zwarciowe"},
            { 1, "Zabezpieczenie przeciążeniowe niezależne"},
            { 3, "Zabezpieczenie przeciążeniowe zależne"},
            { 4, "Zabezpieczenie cieplne"},
            { 8, "Zabezpieczenie ziemnozwarciowe"},
            { 9, "Zabezpieczenie ziemnozwarciowe kierunkowe"},
        };


        public static Dictionary<int, string> Zab2 = new Dictionary<int, string>()
        {
            { 0, "Zabezpieczenie technologiczne 1"},
            { 1, "Zabezpieczenie technologiczne 2"},
            { 2, "Zabezpieczenie technologiczne 3"},
            { 3, "Zabezpieczenie technologiczne 4"},
            { 4, "Zabezpieczenie technologiczne 5"},
            { 5, "Zabezpieczenie technologiczne 6"},
            { 6, "Zabezpieczenie technologiczne 7"},
            { 7, "Zabezpieczenie technologiczne 8"},
            { 8, "Rozbrojenie napędu wyłącznika"},
            { 9, "Niezgodność stanu wyłącznika"},
        };
    }
}
