﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ELPLATFORMA.CompensationComponents.Converters
{
    public class AdditionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value != DependencyProperty.UnsetValue && parameter != null && parameter != DependencyProperty.UnsetValue)
            {
                if (double.TryParse(value.ToString(), out var result) && double.TryParse(parameter.ToString(), out var valueToAdd))
                {
                    var resultVal = result + (valueToAdd);

                    return resultVal < 0 ? 0 : resultVal;
                }
            }

            if (double.TryParse(value?.ToString(), out double dblResult) && dblResult < 0)
                return 0;

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
