﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ELPLATFORMA.CompensationComponents.Converters
{
    public class IntOrConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            bool allEqual = true;
            if (values != null)
            {
                foreach (var value in values)
                {
                    if (int.TryParse(value?.ToString(), out int intVal) && intVal != 0)
                    {
                        allEqual = false;
                    }

                    if (allEqual)
                        return 0;
                    else
                        return 1;
                }
            }

            return 0;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
