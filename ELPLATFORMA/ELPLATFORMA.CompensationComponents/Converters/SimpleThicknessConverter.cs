﻿using ELPLATFORMA.CompensationComponents.Models;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ELPLATFORMA.CompensationComponents.Converters
{
    public class SimpleThicknessConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double left = 0, top = 0, right = 0, bottom = 0;
            if (parameter != null && parameter is string && value != null)
            {
                var expression = parameter?.ToString();
                string mark = "", thicknessStr = "";

                if (expression.Count() > 0 && (expression[0] == '*' || expression[0] == '/' || expression[0] == '+' || expression[0] == '-'))
                {
                    mark = expression[0].ToString();
                    thicknessStr = expression.Remove(0, 1)?.Replace(".", ",");
                }
                else
                {
                    if (int.TryParse(expression, out int thickness))
                    {
                        thicknessStr = thickness + "," + thickness + "," + thickness + "," + thickness;
                    }
                    else
                        thicknessStr = expression.Replace(".", ",");
                }

                var converted = ThicknessConv.FromString(thicknessStr);
                if (double.TryParse(value?.ToString().Replace(".", ","), out double doubleValue))
                {
                    var thickness = (Thickness)converted;

                    if (mark == "")
                    {
                        left = thickness.Left * doubleValue;
                        top = thickness.Top * doubleValue;
                        right = thickness.Right * doubleValue;
                        bottom = thickness.Bottom * doubleValue;
                    }
                    /*else if (expression.Contains("-"))
                    {
                        left = thickness.Left - doubleValue;
                        top = thickness.Top - doubleValue;
                        right = thickness.Bottom - doubleValue;
                        bottom = thickness.Right - doubleValue;
                    }

                    else if (expression.Contains("/"))
                    {
                        left = thickness.Left / doubleValue;
                        top = thickness.Top / doubleValue;
                        right = thickness.Bottom / doubleValue;
                        bottom = thickness.Right / doubleValue;
                    }

                    else if (expression.Contains("+"))
                    {
                        left = thickness.Left + doubleValue;
                        top = thickness.Top + doubleValue;
                        right = thickness.Bottom + doubleValue;
                        bottom = thickness.Right + doubleValue;
                    }
                    else*/
                }
                else if (value is ScreenRatios)
                {
                    var thickness = (Thickness)converted;
                    var screenRatios = (ScreenRatios)value;

                    if (mark == "")
                    {
                        left = thickness.Left * screenRatios.ToFullHDWidthRatio;
                        top = thickness.Top * screenRatios.ToFullHDHeightRatio;
                        right = thickness.Right * screenRatios.ToFullHDWidthRatio;
                        bottom = thickness.Bottom * screenRatios.ToFullHDHeightRatio;
                    }
                    else if (mark == "*")
                    {
                        left = thickness.Left * screenRatios.ToFullHDWidthRatio;
                        top = thickness.Top * screenRatios.ToFullHDHeightRatio;
                        right = thickness.Right * screenRatios.ToFullHDWidthRatio;
                        bottom = thickness.Bottom * screenRatios.ToFullHDHeightRatio;
                    }
                }
            }

            return new Thickness(left, top, right, bottom);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
