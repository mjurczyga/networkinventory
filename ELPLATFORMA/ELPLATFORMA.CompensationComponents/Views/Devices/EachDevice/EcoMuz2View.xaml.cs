﻿using ELPLATFORMA.CompensationComponents.ViewModels.EachDevice.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ELPLATFORMA.CompensationComponents.Views
{
    /// <summary>
    /// Interaction logic for EcoMuz2View.xaml
    /// </summary>
    public partial class EcoMuz2View : UserControl
    {
        public EcoMuz2View()
        {
            InitializeComponent();
        }
    }
}
