﻿using ELPLATFORMA.CompensationComponents.Bootstrapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime;
using ELPLATFORMA.CompensationComponents.Controls;
using ELPLATFORMA.CompensationComponents.Services;
using Autofac;
using ELPLATFORMA.CompensationComponents.ViewModels;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Windows.Threading;

namespace ELPLATFORMA.CompensationComponents.Views
{
    /// <summary>
    /// Interaction logic for MainContainer.xaml
    /// </summary>
    public partial class MainContainer : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public IComponentContext _componentContext;


        public MainContainer()
        {
            //CreateApplicationObject();
            //if (!AppBootstrapper.IsBootstrapperLoaded)
            //{
            //    AppBootstrapper.Start();
            //}

            //InitializeComponent();
        }

        public MainContainer(EvoObjectExtension evoObjectExtension)
        {
            CreateApplicationObject();
            if (!AppBootstrapper.IsBootstrapperLoaded)
            {
                var appBootstrapper = new AppBootstrapper();
                appBootstrapper.Start();
            }


            var evoVarService = CommonServiceLocator.ServiceLocator.Current.GetInstance<IEvoVarsService>();
            _componentContext = CommonServiceLocator.ServiceLocator.Current.GetInstance<IComponentContext>();

            if (evoVarService != null)
            {
                evoVarService.InitService(evoObjectExtension);
                
            }
            

            InitializeComponent();

            //myClass = this;
        }  

        private static void CreateApplicationObject()
        {
            if (null == Application.Current)
            {
                new Application();

                Application.Current.Resources.MergedDictionaries.Add(Application.LoadComponent(new Uri("ELPLATFORMA.CompensationComponents;component/Themes/Theme.xaml", UriKind.Relative)) as ResourceDictionary);
                Application.Current.Resources.MergedDictionaries.Add(Application.LoadComponent(new Uri("ELPLATFORMA.CompensationComponents;component/Themes/ControlsResource.xaml", UriKind.Relative)) as ResourceDictionary);
                Application.Current.Resources.MergedDictionaries.Add(Application.LoadComponent(new Uri("ELPLATFORMA.CompensationComponents;component/Themes/Icons.xaml", UriKind.Relative)) as ResourceDictionary);
                Application.Current.Resources.MergedDictionaries.Add(Application.LoadComponent(new Uri("ELPLATFORMA.CompensationComponents;component/Themes/Converters.xaml", UriKind.Relative)) as ResourceDictionary);
 
                // PRZYKŁAD:  new Uri("MyLibrary;component/Resources/MyResourceDictionary.xaml", UriKind.Relative)) as ResourceDictionary);

            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
     
        }

       
    }
}
