﻿using ELPLATFORMA.CompensationComponents.ViewModels;
using SchemeControlsLib.Controls.Events;
using SchemeControlsLib.Controls.LibControls;
using SchemeControlsLib.Services;
using SchemeControlsLib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ELPLATFORMA.CompensationComponents.Views
{
    /// <summary>
    /// Interaction logic for Synoptic.xaml
    /// </summary>
    public partial class SynopticView : UserControl
    {

        public event EventHandler<VarsBindingsDoneEventArgs> VarsBindingsDone;

        protected virtual void OnVarsBindingsDone(VarsBindingsDoneEventArgs e)
        {
            EventHandler<VarsBindingsDoneEventArgs> handler = this.VarsBindingsDone;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public SynopticView()
        {
           
            InitializeComponent();
            this.DataContext = CommonServiceLocator.ServiceLocator.Current.GetInstance<SynopticViewModel>();

        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            var schemeCanvasObj = this.FindName("_Canvas");
            if (schemeCanvasObj is SchemeCanvas schemeCanvas)
            {
                var schemeService = CommonServiceLocator.ServiceLocator.Current.GetInstance<ISchemeService>();
                schemeService.SetSchemeCanvas(schemeCanvas); //Z10
            }
        }

        public void SetVars(Dictionary<string, object> vars)
        {
            //if (SynopticViewModel != null)
            //    SynopticViewModel.Vars = vars;
        }

        private void _Canvas_VarsBindingsDone(object sender, VarsBindingsDoneEventArgs e)
        {
            if (this.DataContext is SynopticViewModel synopticViewModel)
                synopticViewModel.OnVarsBindingsDone(e);
        }

        private void _Canvas_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.DataContext is SynopticViewModel synopticViewModel && sender is SchemeCanvas schemeCanvas)
                synopticViewModel.OnCanvasLoaded(schemeCanvas);
        }
    }
}
