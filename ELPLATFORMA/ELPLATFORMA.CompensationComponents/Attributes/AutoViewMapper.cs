﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELPLATFORMA.CompensationComponents.Attributes
{
    public class AutoViewMapper : Attribute
    {
        public string View { get; set; }

        public AutoViewMapper(string view)
        {
            View = view;
        }
    }
}
