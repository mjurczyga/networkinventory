﻿using Askom.AddonManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ELPLATFORMA.CompensationComponents.Models
{
    public class EvoVarModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        private object _RawValue;
        public object RawValue
        {
            get { return _RawValue; }
            set { _RawValue = value; NotifyPropertyChanged("RawValue"); }
        }

        private object _Value;
        public object Value
        {
            get { return _Value; }
            set { _Value = value; NotifyPropertyChanged("Value"); }
        }

        private string _FormattedValue;
        public string FormattedValue
        {
            get { return _FormattedValue; }
            set { _FormattedValue = value; NotifyPropertyChanged("FormattedValue"); }
        }

        private StatusOpc _Status;
        public StatusOpc Status
        {
            get { return _Status; }
            set { _Status = value; NotifyPropertyChanged("Status"); }
        }

        private DateTime _TimeStamp;
        public DateTime TimeStamp
        {
            get { return _TimeStamp; }
            set { _TimeStamp = value; NotifyPropertyChanged("TimeStamp"); }
        }

        private bool _IsGood;
        public bool IsGood
        {
            get { return _IsGood; }
            set { _IsGood = value; NotifyPropertyChanged("IsGood"); }
        }

        private bool _IsBad;
        public bool IsBad
        {
            get { return _IsBad; }
            set { _IsBad = value; NotifyPropertyChanged("IsBad"); }
        }

        private bool _IsUncertain;
        public bool IsUncertain
        {
            get { return _IsUncertain; }
            set { _IsUncertain = value; NotifyPropertyChanged("IsUncertain"); }
        }

        private string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; NotifyPropertyChanged("Description"); }
        }

        private string _FullName;
        public string FullName
        {
            get { return _FullName; }
            set { _FullName = value; NotifyPropertyChanged("FullName"); }
        }

        private string _Unit;
        public string Unit
        {
            get { return _Unit; }
            set { _Unit = value; NotifyPropertyChanged("Unit"); }
        }


        public EvoVarModel()
        {

        }

        public EvoVarModel(VariableState variableState)
        {
            UpdateProperties(variableState);
        }

        public void UpdateProperties(VariableState variableState)
        {
            RawValue = variableState.RawValue;
            Value = variableState.Value;
            FormattedValue = variableState.FormattedValue;
            Status = variableState.Status;
            TimeStamp = variableState.TimeStamp;
            IsGood = variableState.IsGood;
            IsBad = variableState.IsBad;
            IsUncertain = variableState.IsUncertain;
        }


    }
}
