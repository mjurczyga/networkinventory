﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELPLATFORMA.CompensationComponents.Models.StaticModels
{
    public static class SwitchboardModels
    {
        public static List<string> KR2_Switchboards = new List<string>()
        {
            { "ZPZ" },
            { "ZP2X400V" },
            { "PAC" },
            { "KR2" },
        };
    }
}
