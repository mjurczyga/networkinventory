﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELPLATFORMA.CompensationComponents.Models.StaticModels
{
    public static class GvlNames
    {
        public static string CukSourceName = "CUK";
        public static string KR2_SourceName = "KR2";

        public static string GVL_Devices = "VISU_Komunikacja";
        public static string GVL_Synoptyka = "VISU_Synoptyka";
        public static string GVL_Moduly = "CUK_VISU_moduly";
        public static string GVL_SMB = "CUK_VISU_SMB";


    }
}
