﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELPLATFORMA.CompensationComponents.Models
{
    public enum ScreenResolution
    {
        R_NotSpecified = 0,
        R_1366x768 = 1,
        R_1920x1080 = 2,
        R_3840x2160 = 3,
    }

    public class ScreenRatios : Core_ObservableObject, IEquatable<ScreenRatios>
    {
        private double _ScreenRatio = 16.0 / 9.0;
        public double ScreenRatio { get { return _ScreenRatio; } set { _ScreenRatio = value; NotifyPropertyChanged("ScreenRatio"); } }

        private double _ToFullHDWidthRatio = 1;
        public double ToFullHDWidthRatio { get { return _ToFullHDWidthRatio; } set { _ToFullHDWidthRatio = value; NotifyPropertyChanged("ToFullHDWidthRatio"); } }

        private double _ToFullHDHeightRatio = 1;
        public double ToFullHDHeightRatio { get { return _ToFullHDHeightRatio; } set { _ToFullHDHeightRatio = value; NotifyPropertyChanged("ToFullHDHeightRatio"); } }

        public ScreenRatios()
        {

        }

        public ScreenRatios(ScreenRatios screenRatios)
        {
            this.ScreenRatio = screenRatios.ScreenRatio;
            this.ToFullHDWidthRatio = screenRatios.ToFullHDWidthRatio;
            this.ToFullHDHeightRatio = screenRatios.ToFullHDHeightRatio;
        }

        #region IEquatable<ScreenRatios>
        public override bool Equals(object obj) => this.Equals(obj as ScreenRatios);

        public bool Equals(ScreenRatios screenRatios)
        {
            if (screenRatios is null)
            {
                return false;
            }

            if (Object.ReferenceEquals(this, screenRatios))
            {
                return true;
            }

            if (this.GetType() != screenRatios.GetType())
            {
                return false;
            }

            return (ScreenRatio == screenRatios.ScreenRatio) && (ToFullHDWidthRatio == screenRatios.ToFullHDWidthRatio) && (ToFullHDHeightRatio == screenRatios.ToFullHDHeightRatio);
        }

        public override int GetHashCode()
        {
            var hashCode = 1347824707;
            hashCode = hashCode * -1521134295 + ScreenRatio.GetHashCode();
            hashCode = hashCode * -1521134295 + ToFullHDWidthRatio.GetHashCode();
            hashCode = hashCode * -1521134295 + ToFullHDHeightRatio.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(ScreenRatios lhs, ScreenRatios rhs)
        {
            if (lhs is null)
            {
                if (rhs is null)
                {
                    return true;
                }

                // Only the left side is null.
                return false;
            }
            // Equals handles case of null on right side.
            return lhs.Equals(rhs);
        }

        public static bool operator !=(ScreenRatios lhs, ScreenRatios rhs) => !(lhs == rhs);
        #endregion IEquatable<ScreenRatios>
    }

    // Default settings are for 1920x1080
    public class ScreenSettings : Core_ObservableObject, IEquatable<ScreenSettings>
    {
        // Pole przygotowane dla możliwości rozwoju o ustawienia usera
        private bool _DefaultValues = true;
        public bool DefaultValues { get { return _DefaultValues; } set { _DefaultValues = value; NotifyPropertyChanged("DefaultValues"); } }

        private ScreenResolution _ScreenResolution = ScreenResolution.R_1920x1080;
        public ScreenResolution ScreenResolution { get { return _ScreenResolution; } set { _ScreenResolution = value; NotifyPropertyChanged("ScreenResolution"); } }

        private ScreenRatios _ScreenRatios = new ScreenRatios();
        public ScreenRatios ScreenRatios { get { return _ScreenRatios; } set { _ScreenRatios = value; NotifyPropertyChanged("ScreenRatios"); } }

        public ScreenSettings()
        {

        }

        public ScreenSettings(ScreenSettings screenSettings)
        {
            this.DefaultValues = screenSettings.DefaultValues;
            this.ScreenResolution = screenSettings.ScreenResolution;
            this.ScreenRatios = new ScreenRatios(screenSettings.ScreenRatios);
        }

        #region  IEquatable<ScreenSettings>
        public override bool Equals(object obj) => this.Equals(obj as ScreenSettings);

        public bool Equals(ScreenSettings screenSettings)
        {
            if (screenSettings is null)
            {
                return false;
            }

            if (Object.ReferenceEquals(this, screenSettings))
            {
                return true;
            }

            if (this.GetType() != screenSettings.GetType())
            {
                return false;
            }

            return (DefaultValues == screenSettings.DefaultValues) && (ScreenResolution == screenSettings.ScreenResolution) && (ScreenRatios == screenSettings.ScreenRatios);
        }

        public override int GetHashCode()
        {
            var hashCode = 1948116032;
            hashCode = hashCode * -1521134295 + DefaultValues.GetHashCode();
            hashCode = hashCode * -1521134295 + ScreenResolution.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<ScreenRatios>.Default.GetHashCode(ScreenRatios);
            return hashCode;
        }

        public static bool operator ==(ScreenSettings lhs, ScreenSettings rhs)
        {
            if (lhs is null)
            {
                if (rhs is null)
                {
                    return true;
                }

                // Only the left side is null.
                return false;
            }
            // Equals handles case of null on right side.
            return lhs.Equals(rhs);
        }

        public static bool operator !=(ScreenSettings lhs, ScreenSettings rhs) => !(lhs == rhs);
        #endregion  IEquatable<ScreenSettings>

    }
}
