﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using VISU.Controls.Fans;

namespace VISU.Controls
{
    public class ValuePicker : BaseVarControl
    {
        #region propertyChanged for inherited properties
        private void Value_PropertyChanged(object sender, EventArgs e)
        {
            if (sender is ValuePicker valuePicker)
            {
                if (double.TryParse(valuePicker.Value?.ToString(), out double dblVal))
                    valuePicker.InternalValue = dblVal;
                else
                    valuePicker.InternalValue = 0;
            }
        }
        #endregion

        private double _InternalValue;
        public double InternalValue
        {
            get { return _InternalValue; }
            set { _InternalValue = value; NotifyPropertyChanged("InternalValue"); }
        }

        public double Interval
        {
            get { return (double)GetValue(IntervalProperty); }
            set { SetValue(IntervalProperty, value); }
        }
        public static readonly DependencyProperty IntervalProperty =
            DependencyProperty.Register("Interval", typeof(double), typeof(ValuePicker), new PropertyMetadata(1.0));

        #region Buttons
        public Brush ButtonBackground
        {
            get { return (Brush)GetValue(ButtonBackgroundProperty); }
            set { SetValue(ButtonBackgroundProperty, value); }
        }

        public static readonly DependencyProperty ButtonBackgroundProperty =
            DependencyProperty.Register("ButtonBackground", typeof(Brush), typeof(ValuePicker));

        public Thickness ButtonBorderThickness
        {
            get { return (Thickness)GetValue(ButtonBorderThicknessProperty); }
            set { SetValue(ButtonBorderThicknessProperty, value); }
        }

        public static readonly DependencyProperty ButtonBorderThicknessProperty =
            DependencyProperty.Register("ButtonBorderThickness", typeof(Thickness), typeof(ValuePicker));
        #endregion Buttons


        #region commands
        public ICommand AddCommand
        {
            get { return (ICommand)GetValue(MyPropertyProperty); }
            set { SetValue(MyPropertyProperty, value); }
        }

        public static readonly DependencyProperty MyPropertyProperty =
            DependencyProperty.Register("MyProperty", typeof(ICommand), typeof(ValuePicker));

        public ICommand SubtractCommand
        {
            get { return (ICommand)GetValue(SubtractCommandProperty); }
            set { SetValue(SubtractCommandProperty, value); }
        }

        public static readonly DependencyProperty SubtractCommandProperty =
            DependencyProperty.Register("SubtractCommand", typeof(ICommand), typeof(ValuePicker));
        #endregion

        private ICommand _InternalAddCommand;
        public ICommand InternalAddCommand
        {
            get { return _InternalAddCommand; }
            set { _InternalAddCommand = value; NotifyPropertyChanged("InternalAddCommand"); }
        }

        private ICommand _InternalSubtractCommand;
        public ICommand InternalSubtractCommand
        {
            get { return _InternalSubtractCommand; }
            set { _InternalSubtractCommand = value; NotifyPropertyChanged("InternalSubtractCommand"); }
        }



        public ValuePicker()
        {
            DependencyPropertyDescriptor.FromProperty(ValueProperty, typeof(RedinessBoolControl)).AddValueChanged(this, Value_PropertyChanged);

            InternalAddCommand = new RelayCommand(OnInternalAddCommand);
            InternalSubtractCommand = new RelayCommand(OnInternalSubtractCommand);
        }

        private void OnInternalSubtractCommand()
        {
            InternalValue -= Interval;
            if (SubtractCommand != null)
                SubtractCommand.Execute(this);
        }

        private void OnInternalAddCommand()
        {
            InternalValue += Interval;
            if (AddCommand != null)
                AddCommand.Execute(this);
        }

        static ValuePicker()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ValuePicker), new FrameworkPropertyMetadata(typeof(ValuePicker)));
        }
    }
}
