﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace VISU.Controls.BaseControls.Text
{
    public class CustomTextBlock : TextBlock
    {
        public double TextRotation
        {
            get { return (double)GetValue(TextRotationProperty); }
            set { SetValue(TextRotationProperty, value); }
        }

        public static readonly DependencyProperty TextRotationProperty =
            DependencyProperty.Register("TextRotation", typeof(double), typeof(CustomTextBlock), new PropertyMetadata(TextRotation_PropertyChanged));

        private static void TextRotation_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //throw new NotImplementedException();
        }
    }
}
