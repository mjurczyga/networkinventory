﻿using System;
using System.Windows;
using System.Windows.Input;


namespace VISU.Controls
{
    public class CustomCheckbox : NotifyControl
    {


        public double ComboboxSize
        {
            get { return (double)GetValue(ComboboxSizeProperty); }
            set { SetValue(ComboboxSizeProperty, value); }
        }

        public static readonly DependencyProperty ComboboxSizeProperty =
            DependencyProperty.Register("ComboboxSize", typeof(double), typeof(CustomCheckbox), new PropertyMetadata(20.0));



        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(CustomCheckbox), new PropertyMetadata(Text_PropertyChanged));

        private static void Text_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is CustomCheckbox customCheckbox)
            {
                if (string.IsNullOrEmpty(e.NewValue?.ToString()))
                {
                    customCheckbox.TextVisibility = Visibility.Collapsed;
                }
                else
                {
                    customCheckbox.TextVisibility = Visibility.Visible;
                }
            }
        }

        private Visibility _TextVisibility = Visibility.Collapsed;
        public Visibility TextVisibility
        {
            get { return _TextVisibility; }
            set { _TextVisibility = value; NotifyPropertyChanged("TextVisibility"); }
        }



        public object IsChecked
        {
            get { return (object)GetValue(IsCheckedProperty); }
            set { SetValue(IsCheckedProperty, value); }
        }

        public static readonly DependencyProperty IsCheckedProperty =
            DependencyProperty.Register("IsChecked", typeof(object), typeof(CustomCheckbox), new PropertyMetadata(IsChecked_PropertyChanged));

        private static void IsChecked_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is CustomCheckbox customCheckbox)
            {
                if(bool.TryParse(e.NewValue?.ToString(), out bool boolValue))
                {
                    customCheckbox.SetIconVisibility(boolValue);
                }
                else
                {
                    customCheckbox.SetIconVisibility(false);
                }
            }
        }

        public ICommand MouseDownCommand
        {
            get { return (ICommand)GetValue(MouseDownCommandProperty); }
            set { SetValue(MouseDownCommandProperty, value); }
        }

        public static readonly DependencyProperty MouseDownCommandProperty =
            DependencyProperty.Register("MouseDownCommand", typeof(ICommand), typeof(CustomCheckbox), new PropertyMetadata(MouseDownCommand_PropertyChanged));

        private static void MouseDownCommand_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is CustomCheckbox customCheckbox)
            {
                if (e.NewValue != null)
                {
                    customCheckbox.Cursor = Cursors.Hand;
                }
                else
                    customCheckbox.Cursor = Cursors.Arrow;
            }
        }

        private Visibility _CheckedVisiility;
        public Visibility CheckedVisiility
        {
            get { return _CheckedVisiility; }
            set { _CheckedVisiility = value; NotifyPropertyChanged("CheckedVisiility"); }
        }

        private Visibility _UncheckedVisibility;
        public Visibility UncheckedVisibility
        {
            get { return _UncheckedVisibility; }
            set { _UncheckedVisibility = value; NotifyPropertyChanged("UncheckedVisibility"); }
        }

        private void SetIconVisibility(bool isChecked)
        {
            if(isChecked)
            {
                CheckedVisiility = Visibility.Visible;
                UncheckedVisibility = Visibility.Collapsed;
            }
            else
            {
                CheckedVisiility = Visibility.Collapsed;
                UncheckedVisibility = Visibility.Visible;
            }
        }

        private double _MouseOverOpacity;
        public double MouseOverOpacity
        {
            get { return _MouseOverOpacity; }
            set { _MouseOverOpacity = value; NotifyPropertyChanged("MouseOverOpacity"); }
        }

        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);

            MouseOverOpacity = 0.4;
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            MouseOverOpacity = 0.0;
        }


        static CustomCheckbox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CustomCheckbox), new FrameworkPropertyMetadata(typeof(CustomCheckbox)));
        }

        public CustomCheckbox()
        {
            this.SetIconVisibility(false);
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            if(MouseDownCommand != null)
            {
                MouseDownCommand.Execute(this);
            }

        }
    }
}
