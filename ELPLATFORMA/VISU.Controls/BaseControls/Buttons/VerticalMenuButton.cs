﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using VISU.Controls.Interfaces;

namespace VISU.Controls
{
    public class VerticalMenuButton : NotifyControl, IMenuButton
    {
        #region propertyChanged for inherited properties
        private void Background_PropertyCHanged(object sender, EventArgs e)
        {
            if (sender is VerticalMenuButton verticalMenuButton)
            {
                verticalMenuButton.SetInternalBackground();
            }
        }
        #endregion

        #region IImageButton properties 
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(VerticalMenuButton), new PropertyMetadata(""));



        public DrawingBrush Drawing
        {
            get { return (DrawingBrush)GetValue(DrawingProperty); }
            set { SetValue(DrawingProperty, value); }
        }

        public static readonly DependencyProperty DrawingProperty =
            DependencyProperty.Register("Drawing", typeof(DrawingBrush), typeof(VerticalMenuButton));

        #region DrawingContent
        public UIElement DrawingContent
        {
            get { return (UIElement)GetValue(DrawingContentProperty); }
            set { SetValue(DrawingContentProperty, value); }
        }

        public static readonly DependencyProperty DrawingContentProperty =
            DependencyProperty.Register("DrawingContent", typeof(VerticalMenuButton), typeof(VerticalMenuButton));
        #endregion DrawingContent
        #endregion IImageButton properties 

        #region StylePeroperties
        public Brush SelectedBrush
        {
            get { return (Brush)GetValue(SelectedBrushProperty); }
            set { SetValue(SelectedBrushProperty, value); }
        }

        public static readonly DependencyProperty SelectedBrushProperty =
            DependencyProperty.Register("SelectedBrush", typeof(Brush), typeof(VerticalMenuButton), new PropertyMetadata(Brushes.Transparent));

        public double IconWidth
        {
            get { return (double)GetValue(IconWidthProperty); }
            set { SetValue(IconWidthProperty, value); }
        }

        public static readonly DependencyProperty IconWidthProperty =
            DependencyProperty.Register("IconWidth", typeof(double), typeof(VerticalMenuButton), new PropertyMetadata(10.0));
        #endregion

        #region Icon
        public object Icon
        {
            get { return (object)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        public static readonly DependencyProperty IconProperty =
            DependencyProperty.Register("Icon", typeof(object), typeof(VerticalMenuButton), new PropertyMetadata(null, Icon_PropertyChanged));

        private static void Icon_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            VerticalMenuButton verticalMenuButton = (VerticalMenuButton)d;
            verticalMenuButton.InternalIcon = verticalMenuButton.GetIconFormObject(e.NewValue);


        }

        public object SelectedIcon
        {
            get { return (object)GetValue(SelectedIconProperty); }
            set { SetValue(SelectedIconProperty, value); }
        }

        public static readonly DependencyProperty SelectedIconProperty =
            DependencyProperty.Register("SelectedIcon", typeof(object), typeof(VerticalMenuButton), new PropertyMetadata(default(object)));

        private FrameworkElement _InternalIcon;
        public FrameworkElement InternalIcon
        {
            get { return _InternalIcon; }
            set { _InternalIcon = value; NotifyPropertyChanged("InternalIcon"); }
        }
        #endregion Icon

        #region RegionManager properties
        public Type TargetViewModelType
        {
            get { return (Type)GetValue(TargetViewModelTypeProperty); }
            set { SetValue(TargetViewModelTypeProperty, value); }
        }

        public static readonly DependencyProperty TargetViewModelTypeProperty =
            DependencyProperty.Register("TargetViewModelType", typeof(Type), typeof(VerticalMenuButton), new PropertyMetadata(default(Type)));

        public string TargetRegionName
        {
            get { return (string)GetValue(TargetRegionNameProperty); }
            set { SetValue(TargetRegionNameProperty, value); }
        }

        public static readonly DependencyProperty TargetRegionNameProperty =
            DependencyProperty.Register("TargetRegionName", typeof(string), typeof(VerticalMenuButton), new PropertyMetadata(""));


        public TargetType TargetType
        {
            get { return (TargetType)GetValue(TargetTypeProperty); }
            set { SetValue(TargetTypeProperty, value); }
        }

        public static readonly DependencyProperty TargetTypeProperty =
            DependencyProperty.Register("TargetType", typeof(TargetType), typeof(VerticalMenuButton), new PropertyMetadata(TargetType.Container));
        #endregion RegionManager properties

        #region commands
        public ICommand MouseDownCommand
        {
            get { return (ICommand)GetValue(MouseDownCommandProperty); }
            set { SetValue(MouseDownCommandProperty, value); }
        }

        public static readonly DependencyProperty MouseDownCommandProperty =
            DependencyProperty.Register("MouseDownCommand", typeof(ICommand), typeof(VerticalMenuButton), new PropertyMetadata(null, MouseDownCommand_PropertyChanged));

        private static void MouseDownCommand_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            VerticalMenuButton verticalMenuButton = (VerticalMenuButton)d;
            if (e.NewValue != null)
                verticalMenuButton.Cursor = Cursors.Hand;
            else
                verticalMenuButton.Cursor = Cursors.Arrow;

        }

        public object MouseDownCommandParameter
        {
            get { return (object)GetValue(MouseDownCommandParameterProperty); }
            set { SetValue(MouseDownCommandParameterProperty, value); }
        }

        public static readonly DependencyProperty MouseDownCommandParameterProperty =
            DependencyProperty.Register("MouseDownCommandParameter", typeof(object), typeof(VerticalMenuButton), new PropertyMetadata(null));

        public ICommand CustomMouseDownCommand
        {
            get { return (ICommand)GetValue(CustomMouseDownCommandProperty); }
            set { SetValue(CustomMouseDownCommandProperty, value); }
        }

        public static readonly DependencyProperty CustomMouseDownCommandProperty =
            DependencyProperty.Register("CustomMouseDownCommand", typeof(ICommand), typeof(VerticalMenuButton), new PropertyMetadata(null));
        #endregion commands

        #region internal properties
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            private set { _isSelected = value; NotifyPropertyChanged("IsSelected"); }
        }

        private Brush _InternalBackground;
        public Brush InternalBackground
        {
            get { return _InternalBackground; }
            set { _InternalBackground = value; NotifyPropertyChanged("InternalBackground"); }
        }

        private double _MouseOverOpacity;
        public double MouseOverOpacity
        {
            get { return _MouseOverOpacity; }
            set { _MouseOverOpacity = value; NotifyPropertyChanged("MouseOverOpacity"); }
        }
        #endregion internal properties

        #region public methods
        public void SelectElement()
        {
            this.IsSelected = true;

            this.InternalIcon = this.GetIconFormObject(this.SelectedIcon);
            if (this.InternalIcon == null)
                this.InternalIcon = this.GetIconFormObject(this.Icon);
            this.SetInternalBackground();
        }

        public void UnselectElement()
        {
            IsSelected = false;
            this.InternalIcon = this.GetIconFormObject(this.Icon);
            this.SetInternalBackground();
        }
        #endregion public methods

        #region private methods
        private void SetInternalBackground()
        {
            if (this.IsSelected)
                this.InternalBackground = SelectedBrush;
            else
                this.InternalBackground = this.Background;


            if (this.IsMouseOver)
                this.MouseOverOpacity = 0.3;
            else
                this.MouseOverOpacity = 0.0;
        }

        private FrameworkElement GetIconFormObject(object icon)
        {
            if (icon is FrameworkElement iconFW)
                return iconFW;
            else if (icon is DrawingBrush iconDB)
            {
                Border br = new Border() { Background = iconDB, Width = 16, Height = 16 };
                Viewbox vb = new Viewbox() { Child = br };
                return vb;
            }
            else
                return null;
        }
        #endregion private methods

        #region overrides
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            if (MouseDownCommand != null)
            {
                if (MouseDownCommandParameter == null)
                    MouseDownCommand.Execute(this);
                else
                    MouseDownCommand.Execute(MouseDownCommandParameter);
            }
        }

        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            this.SetInternalBackground();
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            this.SetInternalBackground();
        }
        #endregion overrides

        public VerticalMenuButton()
        {
            DependencyPropertyDescriptor.FromProperty(BackgroundProperty, typeof(VerticalMenuButton)).AddValueChanged(this, Background_PropertyCHanged);

            this.SetInternalBackground();
        }

        static VerticalMenuButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(VerticalMenuButton), new FrameworkPropertyMetadata(typeof(VerticalMenuButton)));
        }
    }
}
