﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace VISU.Controls.BaseControls
{
    public class VisuTabItemControl : NotifyControl//, IVisuTabItemControl
    {
        #region propertyChanged for inherited properties
        private void Background_PropertyChanged(object sender, EventArgs e)
        {
            if (sender is VisuTabItemControl verticalMenuButton)
            {
                verticalMenuButton.SetInternalBackground();
            }
        }
        #endregion

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("Title", typeof(string), typeof(VisuTabItemControl), new PropertyMetadata(""));

        public object Content
        {
            get { return (object)GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }

        public static readonly DependencyProperty ContentProperty =
            DependencyProperty.Register("Content", typeof(object), typeof(VisuTabItemControl), new PropertyMetadata(null));

        #region StylePeroperties
        public Brush SelectedBrush
        {
            get { return (Brush)GetValue(SelectedBrushProperty); }
            set { SetValue(SelectedBrushProperty, value); }
        }

        public static readonly DependencyProperty SelectedBrushProperty =
            DependencyProperty.Register("SelectedBrush", typeof(Brush), typeof(VisuTabItemControl), new PropertyMetadata(Brushes.Transparent));

        public double IconWidth
        {
            get { return (double)GetValue(IconWidthProperty); }
            set { SetValue(IconWidthProperty, value); }
        }

        public static readonly DependencyProperty IconWidthProperty =
            DependencyProperty.Register("IconWidth", typeof(double), typeof(VisuTabItemControl), new PropertyMetadata(10.0));
        #endregion

        #region Icon
        public object Icon
        {
            get { return (object)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        public static readonly DependencyProperty IconProperty =
            DependencyProperty.Register("Icon", typeof(object), typeof(VisuTabItemControl), new PropertyMetadata(null, Icon_PropertyChanged));

        private static void Icon_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            VisuTabItemControl visuTabItemControl = (VisuTabItemControl)d;
            visuTabItemControl.InternalIcon = visuTabItemControl.GetIconFormObject(e.NewValue);


        }

        public object SelectedIcon
        {
            get { return (object)GetValue(SelectedIconProperty); }
            set { SetValue(SelectedIconProperty, value); }
        }

        public static readonly DependencyProperty SelectedIconProperty =
            DependencyProperty.Register("SelectedIcon", typeof(object), typeof(VisuTabItemControl), new PropertyMetadata(default(object)));

        private FrameworkElement _InternalIcon;
        public FrameworkElement InternalIcon
        {
            get { return _InternalIcon; }
            set { _InternalIcon = value; NotifyPropertyChanged("InternalIcon"); }
        }
        #endregion Icon

        #region commands
        public ICommand MouseDownCommand
        {
            get { return (ICommand)GetValue(MouseDownCommandProperty); }
            set { SetValue(MouseDownCommandProperty, value); }
        }

        public static readonly DependencyProperty MouseDownCommandProperty =
            DependencyProperty.Register("MouseDownCommand", typeof(ICommand), typeof(VisuTabItemControl), new PropertyMetadata(null, MouseDownCommand_PropertyChanged));

        private static void MouseDownCommand_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            VisuTabItemControl visuTabItemControl = (VisuTabItemControl)d;
            if (e.NewValue != null)
                visuTabItemControl.Cursor = Cursors.Hand;
            else
                visuTabItemControl.Cursor = Cursors.Arrow;

        }

        public object CommandParameter
        {
            get { return (object)GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.Register("CommandParameter", typeof(object), typeof(VisuTabItemControl), new PropertyMetadata(null));

        public ICommand CustomMouseDownCommand
        {
            get { return (ICommand)GetValue(CustomMouseDownCommandProperty); }
            set { SetValue(CustomMouseDownCommandProperty, value); }
        }

        public static readonly DependencyProperty CustomMouseDownCommandProperty =
            DependencyProperty.Register("CustomMouseDownCommand", typeof(ICommand), typeof(VisuTabItemControl), new PropertyMetadata(null));
        #endregion commands

        #region internal properties
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            private set { _isSelected = value; NotifyPropertyChanged("IsSelected"); }
        }

        private Brush _InternalBackground;
        public Brush InternalBackground
        {
            get { return _InternalBackground; }
            set { _InternalBackground = value; NotifyPropertyChanged("InternalBackground"); }
        }

        private double _MouseOverOpacity;
        public double MouseOverOpacity
        {
            get { return _MouseOverOpacity; }
            set { _MouseOverOpacity = value; NotifyPropertyChanged("MouseOverOpacity"); }
        }
        #endregion internal properties

        #region public methods
        public void SelectElement()
        {
            this.IsSelected = true;

            this.InternalIcon = this.GetIconFormObject(this.SelectedIcon);
            if (this.InternalIcon == null)
                this.InternalIcon = this.GetIconFormObject(this.Icon);
            this.SetInternalBackground();
        }

        public void UnselectElement()
        {
            IsSelected = false;
            this.InternalIcon = this.GetIconFormObject(this.Icon);
            this.SetInternalBackground();
        }
        #endregion public methods

        #region private methods
        private void SetInternalBackground()
        {
            if (this.IsSelected)
                this.InternalBackground = SelectedBrush;
            else
                this.InternalBackground = this.Background;


            if (this.IsMouseOver)
                this.MouseOverOpacity = 0.3;
            else
                this.MouseOverOpacity = 0.0;
        }

        private FrameworkElement GetIconFormObject(object icon)
        {
            if (icon is FrameworkElement iconFW)
                return iconFW;
            else if (icon is DrawingBrush iconDB)
            {
                Border br = new Border() { Background = iconDB, Width = 16, Height = 16 };
                Viewbox vb = new Viewbox() { Child = br };
                return vb;
            }
            else
                return null;
        }
        #endregion private methods

        #region overrides
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            if (MouseDownCommand != null)
            {
                if (CommandParameter == null)
                    MouseDownCommand.Execute(this);
                else
                    MouseDownCommand.Execute(CommandParameter);
            }
        }

        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            this.SetInternalBackground();
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            this.SetInternalBackground();
        }
        #endregion overrides

        public VisuTabItemControl()
        {
            DependencyPropertyDescriptor.FromProperty(BackgroundProperty, typeof(VisuTabItemControl)).AddValueChanged(this, Background_PropertyChanged);

            this.SetInternalBackground();
        }

        static VisuTabItemControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(VisuTabItemControl), new FrameworkPropertyMetadata(typeof(VisuTabItemControl)));
        }
    }
}
