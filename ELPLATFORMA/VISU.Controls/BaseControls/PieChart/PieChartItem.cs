﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace VISU.Controls.BaseControls.PieChart
{
    public class PieChartItem : DependencyObject, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }    

        public string ShortDescription
        {
            get { return (string)GetValue(ShortDescriptionProperty); }
            set { SetValue(ShortDescriptionProperty, value); }
        }

        public static readonly DependencyProperty ShortDescriptionProperty =
            DependencyProperty.Register("ShortDescription", typeof(string), typeof(PieChartItem), new PropertyMetadata(raisePropertyChanged));

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(PieChartItem), new PropertyMetadata(raisePropertyChanged));

        public double Percent
        {
            get { return (double)GetValue(PercentProperty); }
            set { SetValue(PercentProperty, value); }
        }

        public static readonly DependencyProperty PercentProperty =
            DependencyProperty.Register("Percent", typeof(double), typeof(PieChartItem), new PropertyMetadata(raisePropertyChanged));

        public Brush Brush
        {
            get { return (Brush)GetValue(BrushProperty); }
            set { SetValue(BrushProperty, value); }
        }

        public static readonly DependencyProperty BrushProperty =
            DependencyProperty.Register("Brush", typeof(Brush), typeof(PieChartItem), new PropertyMetadata(raisePropertyChanged));



        private static void raisePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is PieChartItem pieChartElementModel)
                pieChartElementModel.invokePropertyChanged(e.Property.Name);
        }

        private void invokePropertyChanged(string propertyName)
        {
            if(PropertyChanged != null)
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
