﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace VISU.Controls.BaseControls.PieChart
{
    public class PieChartElementShape : Shape
    {
        #region Dependency properties 
        #region CenterPoint
        public Point CenterPoint
        {
            get { return (Point)GetValue(CenterPointProperty); }
            set { SetValue(CenterPointProperty, value); }
        }

        public static readonly DependencyProperty CenterPointProperty =
            DependencyProperty.Register("CenterPoint", typeof(Point), typeof(PieChartElementShape), new PropertyMetadata(CenterPoint_PropertyChanged));

        private static void CenterPoint_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is PieChartElementShape pieChartElementShape)
            {
                pieChartElementShape.InvalidateVisual();
            }
        }
        #endregion CenterPoint

        #region Radius
        public double Radius
        {
            get { return (double)GetValue(RadiusProperty); }
            set { SetValue(RadiusProperty, value); }
        }

        public static readonly DependencyProperty RadiusProperty =
            DependencyProperty.Register("Radius", typeof(double), typeof(PieChartElementShape), new PropertyMetadata(Radius_PropertyChanged));

        private static void Radius_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is PieChartElementShape pieChartElementShape)
            {
                pieChartElementShape.InvalidateVisual();
            }
        }
        #endregion Radius

        #region RotationAngle
        public double RotationAngle
        {
            get { return (double)GetValue(RotationAngleProperty); }
            set { SetValue(RotationAngleProperty, value); }
        }

        public Brush Brush { get; }

        public static readonly DependencyProperty RotationAngleProperty =
            DependencyProperty.Register("RotationAngle", typeof(double), typeof(PieChartElementShape), new PropertyMetadata(RotationAngle_PropertyChanged));

        private static void RotationAngle_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is PieChartElementShape pieChartElementShape && double.TryParse(e.NewValue?.ToString(), out double dblVal))
            {
                pieChartElementShape.RotationAngleRadians = (dblVal * 2.0 * Math.PI) / 360.0;

                pieChartElementShape.InvalidateVisual();
            }
        }

        private double _RotationAngleRadians;
        public double RotationAngleRadians
        {
            get { return _RotationAngleRadians; }
            set { _RotationAngleRadians = value; }
        }


        #endregion RotationAngle

        #region StartAngle
        public double StartAngle
        {
            get { return (double)GetValue(StartAngleProperty); }
            set { SetValue(StartAngleProperty, value); }
        }

        public static readonly DependencyProperty StartAngleProperty =
            DependencyProperty.Register("StartAngle", typeof(double), typeof(PieChartElementShape), new PropertyMetadata(0.0, StartAngle_PropertyChanged));

        private static void StartAngle_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is PieChartElementShape pieChartElementShape && double.TryParse(e.NewValue?.ToString(), out double dblVal))
            {
                pieChartElementShape.StartAngleRadians = (dblVal * 2.0 * Math.PI) / 360.0;
                pieChartElementShape.InvalidateVisual();
            }
        }


        private double _StartAngleRadians;
        public double StartAngleRadians
        {
            get { return _StartAngleRadians; }
            set { _StartAngleRadians = value; }
        }



        #endregion StartAngle
        #endregion Dependency properties

        public PieChartElementShape ()
        {
            StrokeLineJoin = PenLineJoin.Round;
        }

        public PieChartElementShape(double radius, double startAngle, double rotationAngle, Brush brush)
        {
            StrokeLineJoin = PenLineJoin.Round;
            Radius = radius;
            StartAngle = startAngle;
            RotationAngle = rotationAngle;
            Brush = brush;
        }

        protected override Geometry DefiningGeometry
        {
            get { return GetGeometry(); }
        }

        private Geometry GetGeometry()
        {
            StartAngleRadians = ((StartAngleRadians % (Math.PI * 2)) + Math.PI * 2) % (Math.PI * 2);
            var endAngle = StartAngleRadians + RotationAngleRadians;

            endAngle = ((endAngle % (Math.PI * 2)) + Math.PI * 2) % (Math.PI * 2);
            if (endAngle < StartAngleRadians)
            {
                double temp_angle = endAngle;
                endAngle = StartAngleRadians;
                StartAngleRadians = temp_angle;
            }
            double angle_diff = endAngle - StartAngleRadians;
            PathGeometry pathGeometry = new PathGeometry();
            PathFigure pathFigure = new PathFigure() { IsClosed = true };
            ArcSegment arcSegment = new ArcSegment();
            arcSegment.IsLargeArc = angle_diff >= Math.PI;
            //Set start of arc
            pathFigure.StartPoint = new Point(CenterPoint.X + Radius * Math.Cos(StartAngleRadians), CenterPoint.Y + Radius * Math.Sin(StartAngleRadians));
            //set end point of arc.
            arcSegment.Point = new Point(CenterPoint.X + Radius * Math.Cos(endAngle), CenterPoint.Y + Radius * Math.Sin(endAngle));
            arcSegment.Size = new Size(Radius, Radius);
            arcSegment.SweepDirection = SweepDirection.Clockwise;

            pathFigure.Segments.Add(arcSegment);

            pathFigure.Segments.Add(new LineSegment(CenterPoint, true));

            pathGeometry.Figures.Add(pathFigure);

            return pathGeometry;
        }
    }
}
