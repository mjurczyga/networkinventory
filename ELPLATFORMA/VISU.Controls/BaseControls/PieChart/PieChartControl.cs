﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using MS.Internal;
using MS.Internal.Controls;
using MS.Internal.Data;
using MS.Internal.KnownBoxes;
using MS.Internal.PresentationFramework;
using MS.Win32;
using VISU.Controls.BaseControls.PieChart;

namespace VISU.Controls.BaseControls.PieChart
{
    public class PieChartItemClickEventArgs
    {
        public PieChartItem Value { get; }

        public PieChartItemClickEventArgs(PieChartItem value)
        {
            Value = value;
        }
    }

    public class PieChartControl : NotifyControl
    {
        #region events
        #region ElementClick
        public delegate void PieChartItemClickEventHandler(object sender, PieChartItemClickEventArgs e);
        public event PieChartItemClickEventHandler PieChartItemClick;
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.LeftButton == MouseButtonState.Pressed && CheckedElement != null)
            { 
                PieChartItemClick?.Invoke(this, new PieChartItemClickEventArgs(CheckedElement));
            }
        }
        #endregion ElementClick
        #endregion events

        #region DependencyProperties
        #region ItemsSource
        public ObservableCollection<object> ItemsSource
        {
            get { return (ObservableCollection<object>)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(ObservableCollection<object>), typeof(PieChartControl), new PropertyMetadata(ItemsSource_PropertyChanged));

        private static void ItemsSource_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is PieChartControl pieChartControl)
            {
                pieChartControl.InvalidateVisual();
                if (e.NewValue is ObservableCollection<object> newCollection)
                    pieChartControl.AddObservableCollectionChangedEvent(newCollection);
                
                if(e.OldValue is ObservableCollection<object> oldCollection)
                    pieChartControl.RemoveObservableCollectionChangedEvent(oldCollection);
            }
        }
        #endregion ItemsSource

        #region ShowItemsInfo
        public bool ShowItemsInfo
        {
            get { return (bool)GetValue(ShowItemsInfoProperty); }
            set { SetValue(ShowItemsInfoProperty, value); }
        }

        public static readonly DependencyProperty ShowItemsInfoProperty =
            DependencyProperty.Register("ShowItemsInfo", typeof(bool), typeof(PieChartControl), new PropertyMetadata(ShowItemsInfo_PropertyChanged));

        private static void ShowItemsInfo_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is PieChartControl pieChartControl)
            {
                pieChartControl.InvalidateVisual();
            }
        }
        #endregion ShowItemsInfo

        #region Radius
        public double Radius
        {
            get { return (double)GetValue(RadiusProperty); }
            set { SetValue(RadiusProperty, value); }
        }

        public static readonly DependencyProperty RadiusProperty =
            DependencyProperty.Register("Radius", typeof(double), typeof(PieChartControl), new PropertyMetadata(Radius_PropertyChanged));

        private static void Radius_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is PieChartControl pieChartControl)
            {
                pieChartControl.InvalidateVisual();
            }
        }
        #endregion Radius

        #region AccentBrush
        public Brush AccentBrush
        {
            get { return (Brush)GetValue(AccentBrushProperty); }
            set { SetValue(AccentBrushProperty, value); }
        }

        public static readonly DependencyProperty AccentBrushProperty =
            DependencyProperty.Register("AccentBrush", typeof(Brush), typeof(PieChartControl));
        #endregion AccentBrush
        #endregion DependencyProperties

        #region Constructor and events
        public PieChartControl()
        {
            ItemsSource = new ObservableCollection<object>();
            ItemsSource.CollectionChanged += ItemsSource_CollectionChanged;

            this.MouseMove += PieChartControl_MouseMove;
            this.MouseLeave += PieChartControl_MouseLeave;
        }

        private void PieChartControl_MouseLeave(object sender, MouseEventArgs e)
        {
            CheckedElement = null;
        }

        private void PieChartControl_MouseMove(object sender, MouseEventArgs e)
        {
            SelectElement(e.GetPosition(this));
        }

        private void ItemsSource_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (object item in e.NewItems)
                {
                    if (item is INotifyPropertyChanged notifyProperty)
                        notifyProperty.PropertyChanged += ItemPropertyChanged;
                }
            }
            if (e.OldItems != null)
            {
                foreach (object item in e.OldItems)
                {
                    if (item is INotifyPropertyChanged notifyProperty)
                        notifyProperty.PropertyChanged -= ItemPropertyChanged;
                }
            }
        }

        private void ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            InvalidateVisual();
        }
        #endregion Constructor and events

        #region Render
        protected override void OnRender(DrawingContext drawingContext)
        {
            //base.OnRender(drawingContext);

            if (ItemsSource != null)
            {
                var itemsCount = ItemsSource.Count;
                var wholeAngle = 360.0;// - itemsCount;
                var startAngle = 0.0;

                this.Width = 2 * Radius;
                this.Height = 2 * Radius;

                Point circleCenterPoint = new Point(Radius, Radius);
                foreach (var item in ItemsSource)
                {
                    if (item is PieChartItem pieChartElement)
                    {
                        var itemAngle = wholeAngle * pieChartElement.Percent / 100.0;
                        var radius = Radius;
                        Pen pen = null;

                        // ograniczenie na pełen okrąg
                        if (startAngle + itemAngle <= 360.0)
                        {
                            if (pieChartElement == CheckedElement)
                            {
                                radius += Radius * 0.1; // zaznaczenie 10%
                                if (AccentBrush != null)
                                    pen = new Pen(Brushes.White, Radius * 0.02);
                                if (!ShowItemsInfo)
                                    GenerateText(drawingContext, pieChartElement, startAngle + itemAngle / 2.0, radius, circleCenterPoint, new Pen(Brushes.White, Radius * 0.02));
                            }

                            if (ShowItemsInfo)
                                GenerateText(drawingContext, pieChartElement, startAngle + itemAngle / 2.0, radius, circleCenterPoint, new Pen(Brushes.White, Radius * 0.02));

                            Geometry geometry = this.GetPieElementGeometry(startAngle, itemAngle, circleCenterPoint, radius);
                            drawingContext.DrawGeometry(pieChartElement.Brush, pen, geometry);
                            startAngle += itemAngle;// + 1.0;
                        }
                    }
                }
            }
        }

        private Geometry GetPieElementGeometry(double startAngle, double rotationAngle, Point centerPoint, double radius)
        {
            var startAngleRadians = (startAngle * 2.0 * Math.PI) / 360.0;
            var rotationAngleRadians = (rotationAngle * 2.0 * Math.PI) / 360.0;

            startAngleRadians = ((startAngleRadians % (Math.PI * 2)) + Math.PI * 2) % (Math.PI * 2);
            var endAngle = startAngleRadians + rotationAngleRadians;

            if(endAngle > 2 * Math.PI)
                endAngle = ((endAngle % (Math.PI * 2)) + Math.PI * 2) % (Math.PI * 2);

            if (endAngle < startAngleRadians)
            {
                double temp_angle = endAngle;
                endAngle = startAngleRadians;
                startAngleRadians = temp_angle;
            }
            double angle_diff = endAngle - startAngleRadians;
            PathGeometry pathGeometry = new PathGeometry();
            PathFigure pathFigure = new PathFigure() { IsClosed = true };
            ArcSegment arcSegment = new ArcSegment();
            arcSegment.IsLargeArc = angle_diff >= Math.PI;
            //Set start of arc
            pathFigure.StartPoint = new Point(centerPoint.X + radius * Math.Cos(startAngleRadians), centerPoint.Y + radius * Math.Sin(startAngleRadians));
            //set end point of arc.
            arcSegment.Point = new Point(centerPoint.X + radius * Math.Cos(endAngle), centerPoint.Y + radius * Math.Sin(endAngle));
            arcSegment.Size = new Size(radius, radius);
            arcSegment.SweepDirection = SweepDirection.Clockwise;

            pathFigure.Segments.Add(arcSegment);

            pathFigure.Segments.Add(new LineSegment(centerPoint, true));

            pathGeometry.Figures.Add(pathFigure);

            return pathGeometry;
        }

        private void GenerateText(DrawingContext drawingContext, PieChartItem pieChartElementModel, double startAngle, double radius, Point centerPoint, Pen pen = null)
        {
            if (drawingContext != null && pieChartElementModel != null)
            {
                var startAngleRadians = (startAngle * 2.0 * Math.PI) / 360.0;

                var textReferenceStartPoint = getPointOnCircleFromAngle(startAngleRadians, radius, centerPoint);

                var tg = Math.Tan(startAngleRadians);

                var distanceFromCircle = FontSize * 1.5; // dystans od obwodu okręgu (stały dla danego kąta)
                Point textReferenceEndPoint = new Point(textReferenceStartPoint.X + distanceFromCircle * Math.Cos(startAngleRadians), textReferenceStartPoint.Y + distanceFromCircle * Math.Sin(startAngleRadians));
                // Odnosnik
                drawingContext.DrawLine(pen, textReferenceStartPoint, textReferenceEndPoint);

                //Text
                var text = pieChartElementModel.ShortDescription + ": " + pieChartElementModel.Percent + "%";
                var formattedText = new FormattedText(
                text,
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface(this.FontFamily, this.FontStyle, this.FontWeight, this.FontStretch),
                this.FontSize,
                Foreground,
                new NumberSubstitution());

                var textSize = new Size(formattedText.Width + pen.Thickness * 2.0, formattedText.Height);

                var textQuarter = getCircleQuarterNumber(startAngleRadians);
                var textBottomLinePoint = new Point(textReferenceEndPoint.X, textReferenceEndPoint.Y);
                var textStartPoint = new Point(textBottomLinePoint.X + pen.Thickness, textBottomLinePoint.Y - pen.Thickness);
                switch(textQuarter)
                {
                    case 1:
                        textBottomLinePoint.X = textBottomLinePoint.X + textSize.Width;
                        textStartPoint.Y = textStartPoint.Y - textSize.Height;
                        break;
                    case 2:
                        textBottomLinePoint.X = textBottomLinePoint.X - textSize.Width;
                        textStartPoint.X = textStartPoint.X - textSize.Width;
                        textStartPoint.Y = textStartPoint.Y - textSize.Height;
                        break;
                    case 3:
                        textBottomLinePoint.X = textBottomLinePoint.X - textSize.Width;
                        textStartPoint.X = textStartPoint.X - textSize.Width;
                        textStartPoint.Y = textStartPoint.Y - textSize.Height;
                        break;
                    case 4:
                        textBottomLinePoint.X = textBottomLinePoint.X + textSize.Width;
                        textStartPoint.Y = textStartPoint.Y - textSize.Height;
                        break;
                }
                // linia pod tekstem
                drawingContext.DrawLine(pen, textReferenceEndPoint, textBottomLinePoint);

                drawingContext.DrawText(formattedText, textStartPoint);


            }
        }
        #endregion Render

        #region CheckElement
        private PieChartItem _checkedElement = null;
        public PieChartItem CheckedElement
        {
            get { return _checkedElement; }
            private set { _checkedElement = value; this.InvalidateVisual(); IsElementSelected = value != null; NotifyPropertyChanged("CheckedElement"); }
        }

        private bool _IsElementSelected;
        public bool IsElementSelected
        {
            get { return _IsElementSelected; }
            private set { _IsElementSelected = value; NotifyPropertyChanged("IsElementSelected"); }
        }


        private void SelectElement(Point mousePosition)
        {
            var circleCenterPoint = new Point(Radius, Radius);
            _checkedElement = null;
            if (CheckIfPointIsInCircle(mousePosition, new Point(Radius, Radius), Radius))
            {
                var pointAngle = getAngleRadiansOffset(mousePosition, circleCenterPoint);
                var currentAngle = 0.0;
                foreach (var item in ItemsSource)
                {
                    if (item is PieChartItem pieChartElement)
                    {
                        var itemAngle = 360.0 * pieChartElement.Percent / 100.0;
                        itemAngle = (itemAngle * 2.0 * Math.PI) / 360.0;
                        if (pointAngle > currentAngle && pointAngle < currentAngle + itemAngle)
                        {
                            CheckedElement = pieChartElement;
                        }
                        currentAngle += itemAngle;
                    }
                }
            }
        }
        #endregion CheckElement

        #region CircleHelpers
        private bool CheckIfPointIsInCircle(Point point, Point centerPoint, double radius)
        {
            bool result = Math.Pow(point.X - centerPoint.X, 2) + Math.Pow(point.Y - centerPoint.Y, 2) <= Math.Pow(radius, 2);          
            return result;
        }

        private double getAngleRadiansOffset(double angleRadians)
        {
            var cos = Math.Cos(angleRadians);
            var sin = Math.Sin(angleRadians);

            if (cos > 0 && sin > 0)
                return 0;
            else if (cos < 0 && sin > 0)
                return Math.PI / 2.0;
            else if (cos < 0 && sin < 0)
                return Math.PI;
            else if (cos > 0 && sin < 0)
                return Math.PI * 3 / 2.0;

            return 0;
        }

        private int getCircleQuarterNumber(double angleRadians)
        {
            var cos = Math.Cos(angleRadians);
            var sin = Math.Sin(angleRadians);

            if (cos >= 0 && sin >= 0)
                return 1;
            else if (cos < 0 && sin >= 0)
                return 2;
            else if (cos < 0 && sin < 0)
                return 3;
            else if (cos >= 0 && sin < 0)
                return 4;
            else return 0;
        }
        
        /// <summary>
        /// Podaje kąt w radianach po punkcie
        /// </summary>
        /// <param name="point">punkt wzgledem którego liczony jest kont</param>
        /// <param name="centerPoint">środek koła</param>
        /// <returns></returns>
        private double getAngleRadiansOffset(Point point, Point centerPoint)
        {
            if (point.X > centerPoint.X && point.Y > centerPoint.Y)
                return Math.Atan((point.Y - centerPoint.Y) / (point.X - centerPoint.X));
            else if (point.X < centerPoint.X && point.Y > centerPoint.Y) 
                return Math.PI - Math.Atan((point.Y - centerPoint.Y) / (centerPoint.X - point.X));
            else if (point.X < centerPoint.X && point.Y < centerPoint.Y)
                return Math.PI + Math.Atan((centerPoint.Y - point.Y) / (centerPoint.X - point.X));
            else if (point.X > centerPoint.X && point.Y < centerPoint.Y)
                return 2 * Math.PI - Math.Atan((centerPoint.Y - point.Y) / (point.X - centerPoint.X));

            return 0;
        }

        private Point getPointOnCircleFromAngle(double angle, double radius, Point centerPoint)
        {
            var x = radius * Math.Cos(angle) + centerPoint.X;
            var y = radius * Math.Sin(angle) + centerPoint.Y;
            return new Point(x, y);
        }

        private Point getPointWithDistanceFromPointWithAngle(double angle, double distance, Point point)
        {
            var x = point.X + distance * Math.Sin(angle);
            var y = point.Y + distance * Math.Sin(angle);



            return new Point(x, y);
        }
        #endregion CircleHelpers

        #region textHelpers
        private Size measureString(string candidate)
        {
            var formattedText = new FormattedText(
                candidate,
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface(this.FontFamily, this.FontStyle, this.FontWeight, this.FontStretch),
                this.FontSize,
                Brushes.Black,
                new NumberSubstitution());

            return new Size(formattedText.Width, formattedText.Height);
        }
        #endregion textHelpers

        #region CollectionHelper
        private void AddObservableCollectionChangedEvent(ObservableCollection<object> observableCollection)
        {
            if (observableCollection != null)
            {
                observableCollection.CollectionChanged += ItemsSource_CollectionChanged;
            }
        }

        private void RemoveObservableCollectionChangedEvent(ObservableCollection<object> observableCollection)
        {
            if (observableCollection != null)
            {
                observableCollection.CollectionChanged -= ItemsSource_CollectionChanged;
            }
        }
        #endregion CollectionHelper

    }
}
