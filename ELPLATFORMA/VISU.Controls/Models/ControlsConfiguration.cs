﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace VISU.Controls.Models
{
    public static class ControlsConfiguration
    {
        private static Brush _CustomRead = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFE41400"));
        private static Brush _CustomYellow = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFCC00"));

        public static Brush ReadyBrush { get; private set; } = Brushes.Green;
        public static Brush UnreadyBrush { get; private set; } = _CustomRead;


        public static Brush OpenBrush { get; private set; } = Brushes.DarkGreen;
        public static Brush CloseBrush { get; private set; } = Brushes.DarkRed;
        public static Brush InvalidBrush { get; private set; } = _CustomYellow;
        public static Brush DisabledBrush { get; private set; } = Brushes.LightGray;
        public static Brush AlarmBrush { get; private set; } = _CustomRead;
        public static Brush WarningBrush { get; private set; } = _CustomYellow;


    }
}
