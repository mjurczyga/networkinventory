﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace VISU.Controls.Interfaces
{
    public interface IImageButton
    {
        void SelectElement();
        void UnselectElement();

        DrawingBrush Drawing { get; set; }
        UIElement DrawingContent { get; set; }

        string Text { get; set; }
        bool IsSelected { get; }

        ICommand MouseDownCommand { get; set; }
        object MouseDownCommandParameter { get; }
    }
}
