﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VISU.Controls.Interfaces
{
    public interface IBaseVarControl
    {
        object Value { get; set; }
        string Description { get; set; }
        string Unit { get; set; }
        string InternalUnit { get; }
    }
}
