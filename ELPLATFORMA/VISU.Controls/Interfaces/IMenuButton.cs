﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace VISU.Controls.Interfaces
{
    public interface IMenuButton : IImageButton
    {
        Type TargetViewModelType { get; set; }
        string TargetRegionName { get; set; }
        TargetType TargetType { get; set; }
        ICommand CustomMouseDownCommand { get; set; }

    }
}
