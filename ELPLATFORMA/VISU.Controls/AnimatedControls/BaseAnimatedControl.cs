﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Threading;

namespace VISU.Controls.AnimatedControls
{
    public abstract class BaseAnimatedControl : NotifyControl
    {
        private Dispatcher _dispatcher = Dispatcher.CurrentDispatcher;

        #region cleanControl property
        public bool CleanControl
        {
            get { return (bool)GetValue(CleanControlProperty); }
            set { SetValue(CleanControlProperty, value); }
        }

        public static readonly DependencyProperty CleanControlProperty =
            DependencyProperty.Register("CleanControl", typeof(bool), typeof(BaseAnimatedControl), new PropertyMetadata(false, CleanControl_PropertyChanged));

        private static void CleanControl_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is BaseAnimatedControl baseAnimatedControl && bool.TryParse(e.NewValue?.ToString(), out bool newBoolValue) && newBoolValue)
            {
                baseAnimatedControl.CleanTimer();
            }
        }
        #endregion

        private Timer _animationTimer;
        private int _animationFrame = 1;

        private void Animate(bool isAnimating)
        {
            if (isAnimating && _animationTimer == null)
            {
                _animationTimer = new Timer(125);
                _animationTimer.Elapsed += _animationTimer_Elapsed;
                _animationTimer.AutoReset = true;
                _animationTimer.Start();
                Console.WriteLine("Start animacji rotor");

            }
            else if (!isAnimating && _animationTimer != null)
            {

                this.CleanTimer();
            }
        }

        private void _animationTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                _dispatcher?.Invoke(() =>
                {

                    this.OnTimerElapsed();

                });
            }
            catch { }
        }

        protected abstract void OnTimerElapsed();

        protected virtual void CleanTimer()
        {
            if (_animationTimer != null)
            {
                _animationTimer.Stop();
                _animationTimer.Elapsed -= _animationTimer_Elapsed;
                _animationTimer.Dispose();
                _animationTimer = null;
                Console.WriteLine("KONIEC animacji rotor");
            }
        }
    }
}
