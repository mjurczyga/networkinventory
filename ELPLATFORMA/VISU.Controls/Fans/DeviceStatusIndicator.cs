﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace VISU.Controls.Fans
{
    public class DeviceStatusIndicator : NotifyControl
    {
        #region Miejsce sterowania
        #region IsLocal
        public bool IsLocalControl
        {
            get { return (bool)GetValue(IsLocalControlProperty); }
            set { SetValue(IsLocalControlProperty, value); }
        }

        public static readonly DependencyProperty IsLocalControlProperty =
            DependencyProperty.Register("IsLocalControl", typeof(bool), typeof(DeviceStatusIndicator), new PropertyMetadata(false, IsLocalControl_PropertyChanged));

        private static void IsLocalControl_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is DeviceStatusIndicator deviceStatusIndicator)
            {
                if(bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    deviceStatusIndicator.IsLocalControlIconVisibility = Visibility.Visible;
                else
                    deviceStatusIndicator.IsLocalControlIconVisibility = Visibility.Collapsed;

            }
        }

        private Visibility _IsLocalControlIconVisibility;
        public Visibility IsLocalControlIconVisibility
        {
            get { return _IsLocalControlIconVisibility; }
            set { _IsLocalControlIconVisibility = value; NotifyPropertyChanged("IsLocalControlIconVisibility"); }
        }
        #endregion IsLocal

        #region IsPLCControl
        public bool IsPLCControl
        {
            get { return (bool)GetValue(IsPLCControlProperty); }
            set { SetValue(IsPLCControlProperty, value); }
        }

        public static readonly DependencyProperty IsPLCControlProperty =
            DependencyProperty.Register("IsPLCControl", typeof(bool), typeof(DeviceStatusIndicator), new PropertyMetadata(false, IsPLCControl_PropertyChanged));

        private static void IsPLCControl_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is DeviceStatusIndicator deviceStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    deviceStatusIndicator.IsPLCControlIconVisibility = Visibility.Visible;
                else
                    deviceStatusIndicator.IsPLCControlIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsPLCControlIconVisibility;
        public Visibility IsPLCControlIconVisibility
        {
            get { return _IsPLCControlIconVisibility; }
            set { _IsPLCControlIconVisibility = value; NotifyPropertyChanged("IsPLCControlIconVisibility"); }
        }

        #endregion IsPLCControl
        #endregion Miejsce sterowania

        #region IsRepairButton
        public bool IsRepairButton
        {
            get { return (bool)GetValue(IsRepairButtonProperty); }
            set { SetValue(IsRepairButtonProperty, value); }
        }

        public static readonly DependencyProperty IsRepairButtonProperty =
            DependencyProperty.Register("IsRepairButton", typeof(bool), typeof(DeviceStatusIndicator), new PropertyMetadata(false, IsRepairButton_PropertyChanged));

        private static void IsRepairButton_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is DeviceStatusIndicator deviceStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    deviceStatusIndicator.IsRepairButtonIconVisibility = Visibility.Visible;
                else
                    deviceStatusIndicator.IsRepairButtonIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsRepairButtonIconVisibility;
        public Visibility IsRepairButtonIconVisibility
        {
            get { return _IsRepairButtonIconVisibility; }
            set { _IsRepairButtonIconVisibility = value; NotifyPropertyChanged("IsRepairButtonIconVisibility"); }
        }
        #endregion IsRepairButton

        #region IsSafetyButton
        public bool IsSafetyButton
        {
            get { return (bool)GetValue(IsSafetyButtonProperty); }
            set { SetValue(IsSafetyButtonProperty, value); }
        }

        public static readonly DependencyProperty IsSafetyButtonProperty =
            DependencyProperty.Register("IsSafetyButton", typeof(bool), typeof(DeviceStatusIndicator), new PropertyMetadata(false, IsSafetyButton_PropertyChanged));

        private static void IsSafetyButton_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is DeviceStatusIndicator deviceStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    deviceStatusIndicator.IsSafetyButtonIconVisibility = Visibility.Visible;
                else
                    deviceStatusIndicator.IsSafetyButtonIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsSafetyButtonIconVisibility;
        public Visibility IsSafetyButtonIconVisibility
        {
            get { return _IsSafetyButtonIconVisibility; }
            set { _IsSafetyButtonIconVisibility = value; NotifyPropertyChanged("IsSafetyButtonIconVisibility"); }
        }
        #endregion IsSafetyButton

        #region ControlVoltage
        public bool IsControlVoltageOk
        {
            get { return (bool)GetValue(IsControlVoltageOkProperty); }
            set { SetValue(IsControlVoltageOkProperty, value); }
        }

        public static readonly DependencyProperty IsControlVoltageOkProperty =
            DependencyProperty.Register("IsControlVoltageOk", typeof(bool), typeof(DeviceStatusIndicator), new PropertyMetadata(false, IsControlVoltageOk_PropertyControl));

        private static void IsControlVoltageOk_PropertyControl(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is DeviceStatusIndicator deviceStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    deviceStatusIndicator.IsRepairButtonIconVisibility = Visibility.Visible;
                else
                    deviceStatusIndicator.IsRepairButtonIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsControlVoltageOkIconVisibility;

        public Visibility IsControlVoltageOkIconVisibility
        {
            get { return _IsControlVoltageOkIconVisibility; }
            set { _IsControlVoltageOkIconVisibility = value; NotifyPropertyChanged("_IsControlVoltageOkIconVisibility"); }
        }
        #endregion ControlVoltage

        static DeviceStatusIndicator()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DeviceStatusIndicator), new FrameworkPropertyMetadata(typeof(DeviceStatusIndicator)));
        }
    }
}
