﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace VISU.Controls.Fans
{
    public class PumpStatusIndicator : DeviceStatusIndicator
    {

        #region IsWorking Status
        public bool IsWorkingStatus
        {
            get { return (bool)GetValue(IsWorkingStstusProperty); }
            set { SetValue(IsWorkingStstusProperty, value); }
        }

        public static readonly DependencyProperty IsWorkingStstusProperty =
            DependencyProperty.Register("IsWorkingStstus", typeof(bool), typeof(PumpStatusIndicator), new PropertyMetadata(false, IsWorkingStatus_PropertyChanged));

        private static void IsWorkingStatus_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is PumpStatusIndicator pumpStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    pumpStatusIndicator.IsWorkingStatusIconVisibility = Visibility.Visible;
                else
                    pumpStatusIndicator.IsWorkingStatusIconVisibility = Visibility.Collapsed;

            }
        }

        private Visibility _IsWorkingStatusIconVisibility;
        public Visibility IsWorkingStatusIconVisibility
        {
            get { return _IsWorkingStatusIconVisibility; }
            set { _IsWorkingStatusIconVisibility = value; NotifyPropertyChanged("IsWorkingStatusIconVisibility"); }
        }
        #endregion IsWorking Status
    }
}
