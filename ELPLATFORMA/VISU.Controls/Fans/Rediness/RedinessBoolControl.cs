﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using VISU.Controls.Models;

namespace VISU.Controls.Fans
{
    public class RedinessBoolControl : BaseVarControl
    {
        public TextAlignment TextAlignment
        {
            get { return (TextAlignment)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }
        public static readonly DependencyProperty TextAlignmentProperty =
            DependencyProperty.Register("TextAlignment", typeof(TextAlignment), typeof(RedinessBoolControl));

        private Brush _InternalBackground;
        public Brush InternalBackground
        {
            get { return _InternalBackground; }
            set { _InternalBackground = value; NotifyPropertyChanged("InternalBackground"); }
        }


        #region propertyChanged for inherited properties
        private void Value_PropertyChanged(object sender, EventArgs e)
        {
            if (sender is RedinessBoolControl redinessBoolControl)
            {
                redinessBoolControl.ProcessValue();
            }
        }
        #endregion

        private bool _InternalValue;
        public bool InternalValue
        {
            get { return _InternalValue; }
            private set { _InternalValue = value; this.ProcessBackground(); NotifyPropertyChanged("InternalValue"); }
        }


        private void ProcessValue()
        {
            if (bool.TryParse(Value?.ToString(), out bool boolValue))
            {
                InternalValue = boolValue;
            }
        }

        private void ProcessBackground()
        {
            if (Value != null)
            {
                if (InternalValue)
                    this.InternalBackground = ControlsConfiguration.ReadyBrush;
                else
                    this.InternalBackground = ControlsConfiguration.UnreadyBrush;
            }
            else
                this.InternalBackground = this.Background;
        }

        public RedinessBoolControl()
        {
            DependencyPropertyDescriptor.FromProperty(ValueProperty, typeof(RedinessBoolControl)).AddValueChanged(this, Value_PropertyChanged);

            this.ProcessBackground();
        }

        static RedinessBoolControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(RedinessBoolControl), new FrameworkPropertyMetadata(typeof(RedinessBoolControl)));
        }
    }
}
