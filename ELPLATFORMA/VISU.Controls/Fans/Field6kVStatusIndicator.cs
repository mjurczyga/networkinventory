﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace VISU.Controls.Fans
{
    public class Field6kVStatusIndicator : NotifyControl
    {
        #region IsBrokenMUZ
        public bool IsBrokenMUZ
        {
            get { return (bool)GetValue(IsBrokenMUZProperty); }
            set { SetValue(IsBrokenMUZProperty, value); }
        }

        public static readonly DependencyProperty IsBrokenMUZProperty =
            DependencyProperty.Register("IsBrokenMUZ", typeof(bool), typeof(Field6kVStatusIndicator), new PropertyMetadata(false, IsBrokenMUZ_PropertyChanged));

        private static void IsBrokenMUZ_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Field6kVStatusIndicator field6KVStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    field6KVStatusIndicator.IsBrokenMUZIconVisibility = Visibility.Visible;
                else
                    field6KVStatusIndicator.IsBrokenMUZIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsBrokenMUZIconVisibility;
        public Visibility IsBrokenMUZIconVisibility
        {
            get { return _IsBrokenMUZIconVisibility; }
            set { _IsBrokenMUZIconVisibility = value; NotifyPropertyChanged("IsBrokenMUZIconVisibility"); }
        }
        #endregion IsBrokenMUZ

        #region IsWarrning
        public bool IsWarrning
        {
            get { return (bool)GetValue(IsWarrningProperty); }
            set { SetValue(IsWarrningProperty, value); }
        }

        public static readonly DependencyProperty IsWarrningProperty =
            DependencyProperty.Register("IsWarrning", typeof(bool), typeof(Field6kVStatusIndicator), new PropertyMetadata(false, IsWarrning_PropertyChanged));

        private static void IsWarrning_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Field6kVStatusIndicator field6KVStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    field6KVStatusIndicator.IsWarrningIconVisibility = Visibility.Visible;
                else
                    field6KVStatusIndicator.IsWarrningIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsWarrningIconVisibility;
        public Visibility IsWarrningIconVisibility
        {
            get { return _IsWarrningIconVisibility; }
            set { _IsWarrningIconVisibility = value; NotifyPropertyChanged("IsWarrningIconVisibility"); }
        }
        #endregion IsWarrning

        #region IsAutoStation
        public bool IsAutoStation
        {
            get { return (bool)GetValue(IsAutoStationProperty); }
            set { SetValue(IsAutoStationProperty, value); }
        }

        public static readonly DependencyProperty IsAutoStationProperty =
            DependencyProperty.Register("IsAutoStation", typeof(bool), typeof(Field6kVStatusIndicator), new PropertyMetadata(false, IsAutoStation_PropertyChanged));

        private static void IsAutoStation_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Field6kVStatusIndicator field6KVStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    field6KVStatusIndicator.IsAutoStationIconVisibility = Visibility.Visible;
                else
                    field6KVStatusIndicator.IsAutoStationIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsAutoStationIconVisibility;
        public Visibility IsAutoStationIconVisibility
        {
            get { return _IsAutoStationIconVisibility; }
            set { _IsAutoStationIconVisibility = value; NotifyPropertyChanged("IsAutoStationIconVisibility"); }
        }
        #endregion IsAutoStation

        #region IsPanelControl
        public bool IsPanelControl
        {
            get { return (bool)GetValue(IsPanelControlProperty); }
            set { SetValue(IsPanelControlProperty, value); }
        }

        public static readonly DependencyProperty IsPanelControlProperty =
            DependencyProperty.Register("IsPanelControl", typeof(bool), typeof(Field6kVStatusIndicator), new PropertyMetadata(false, IsPanelControl_PropertyChanged));

        private static void IsPanelControl_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Field6kVStatusIndicator field6KVStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    field6KVStatusIndicator.IsPanelControlIconVisibility = Visibility.Visible;
                else
                    field6KVStatusIndicator.IsPanelControlIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsPanelControlIconVisibility;
        public Visibility IsPanelControlIconVisibility
        {
            get { return _IsPanelControlIconVisibility; }
            set { _IsPanelControlIconVisibility = value; NotifyPropertyChanged("IsPanelControlIconVisibility"); }
        }
        #endregion IsPanelControl

        #region IsAccidentControl
        public bool IsAccidentControl
        {
            get { return (bool)GetValue(IsAccidentControlProperty); }
            set { SetValue(IsAccidentControlProperty, value); }
        }

        public static readonly DependencyProperty IsAccidentControlProperty =
            DependencyProperty.Register("IsAccidentControl", typeof(bool), typeof(Field6kVStatusIndicator), new PropertyMetadata(false, IsAccidentControl_PropertyChanged));

        private static void IsAccidentControl_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Field6kVStatusIndicator field6KVStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    field6KVStatusIndicator.IsAccidentControlIconVisibility = Visibility.Visible;
                else
                    field6KVStatusIndicator.IsAccidentControlIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsAccidentControlIconVisibility;
        public Visibility IsAccidentControlIconVisibility
        {
            get { return _IsAccidentControlIconVisibility; }
            set { _IsAccidentControlIconVisibility = value; NotifyPropertyChanged("IsAccidentControlIconVisibility"); }
        }
        #endregion IsAccidentControl

        #region IsSys1Connector
        public bool IsSys1Connector
        {
            get { return (bool)GetValue(IsSys1ConnectorProperty); }
            set { SetValue(IsSys1ConnectorProperty, value); }
        }

        public static readonly DependencyProperty IsSys1ConnectorProperty =
            DependencyProperty.Register("IsSys1Connector", typeof(bool), typeof(Field6kVStatusIndicator), new PropertyMetadata(false, IsSys1Connector_PropertyChanged));

        private static void IsSys1Connector_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Field6kVStatusIndicator field6KVStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    field6KVStatusIndicator.IsSys1ConnectorIconVisibility = Visibility.Visible;
                else
                    field6KVStatusIndicator.IsSys1ConnectorIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsSys1ConnectorIconVisibility;
        public Visibility IsSys1ConnectorIconVisibility
        {
            get { return _IsSys1ConnectorIconVisibility; }
            set { _IsSys1ConnectorIconVisibility = value; NotifyPropertyChanged("IsSys1ConnectorIconVisibility"); }
        }
        #endregion IsSys1Connector

        #region IsSys2Connector
        public bool IsSys2Connector
        {
            get { return (bool)GetValue(IsSys2ConnectorProperty); }
            set { SetValue(IsSys2ConnectorProperty, value); }
        }

        public static readonly DependencyProperty IsSys2ConnectorProperty =
            DependencyProperty.Register("IsSys2Connector", typeof(bool), typeof(Field6kVStatusIndicator), new PropertyMetadata(false, IsSys2Connector_PropertyChanged));

        private static void IsSys2Connector_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Field6kVStatusIndicator field6KVStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    field6KVStatusIndicator.IsSys2ConnectorIconVisibility = Visibility.Visible;
                else
                    field6KVStatusIndicator.IsSys2ConnectorIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsSys2ConnectorIconVisibility;
        public Visibility IsSys2ConnectorIconVisibility
        {
            get { return _IsSys2ConnectorIconVisibility; }
            set { _IsSys2ConnectorIconVisibility = value; NotifyPropertyChanged("IsSys2ConnectorIconVisibility"); }
        }
        #endregion IsSys2Connector

        #region Is6kVDisconnector
        public bool Is6kVDisconnector
        {
            get { return (bool)GetValue(Is6kVDisconnectorProperty); }
            set { SetValue(Is6kVDisconnectorProperty, value); }
        }

        public static readonly DependencyProperty Is6kVDisconnectorProperty =
            DependencyProperty.Register("Is6kVDisconnector", typeof(bool), typeof(Field6kVStatusIndicator), new PropertyMetadata(false, Is6kVDisconnector_PropertyChanged));

        private static void Is6kVDisconnector_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Field6kVStatusIndicator field6KVStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    field6KVStatusIndicator.Is6kVDisconnectorIconVisibility = Visibility.Visible;
                else
                    field6KVStatusIndicator.Is6kVDisconnectorIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _Is6kVDisconnectorIconVisibility;
        public Visibility Is6kVDisconnectorIconVisibility
        {
            get { return _Is6kVDisconnectorIconVisibility; }
            set { _Is6kVDisconnectorIconVisibility = value; NotifyPropertyChanged("Is6kVDisconnectorIconVisibility"); }
        }
        #endregion Is6kVDisconnector

        #region IsAccident
        public bool IsAccident
        {
            get { return (bool)GetValue(IsAccidentProperty); }
            set { SetValue(IsAccidentProperty, value); }
        }

        public static readonly DependencyProperty IsAccidentProperty =
            DependencyProperty.Register("IsAccident", typeof(bool), typeof(Field6kVStatusIndicator), new PropertyMetadata(false, IsAccident_PropertyChanged));

        private static void IsAccident_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Field6kVStatusIndicator field6KVStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    field6KVStatusIndicator.IsAccidentIconVisibility = Visibility.Visible;
                else
                    field6KVStatusIndicator.IsAccidentIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsAccidentIconVisibility;
        public Visibility IsAccidentIconVisibility
        {
            get { return _IsAccidentIconVisibility; }
            set { _IsAccidentIconVisibility = value; NotifyPropertyChanged("IsAccidentIconVisibility"); }
        }
        #endregion IsAccident
    }
}
