﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace VISU.Controls.Fans
{
    public class BistableDeviceStatusIndicator : DeviceStatusIndicator
    {
        #region Max position
        #region IsMaxColsingPosition
        public bool IsMaxClosingPosition
        {
            get { return (bool)GetValue(IsMaxClosingPositionProperty); }
            set { SetValue(IsMaxClosingPositionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsMaxClosingPosition.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsMaxClosingPositionProperty =
            DependencyProperty.Register("IsMaxClosingPosition", typeof(bool), typeof(BistableDeviceStatusIndicator), new PropertyMetadata(false, IsMaxClosingPosition_PropertyChanged));

        private static void IsMaxClosingPosition_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is BistableDeviceStatusIndicator BistableDeviceStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    BistableDeviceStatusIndicator.IsMaxClosingPositionIconVisibility = Visibility.Visible;
                else
                    BistableDeviceStatusIndicator.IsMaxClosingPositionIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsMaxClosingPositionIconVisibility;
        public Visibility IsMaxClosingPositionIconVisibility
        {
            get { return _IsMaxClosingPositionIconVisibility; }
            set { _IsMaxClosingPositionIconVisibility = value; NotifyPropertyChanged("IsMaxClosingPositionIconVisibility"); }
        }
        #endregion IsMaxColsingPosition

        #region IsMaxOpeningPosition
        public bool IsMaxOpeningPosition
        {
            get { return (bool)GetValue(IsMaxOpeningPositionProperty); }
            set { SetValue(IsMaxOpeningPositionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsMaxOpeningPosition.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsMaxOpeningPositionProperty =
            DependencyProperty.Register("IsMaxOpeningPosition", typeof(bool), typeof(BistableDeviceStatusIndicator), new PropertyMetadata(false, IsMaxOpeningPosition_PropertyChanged));

        private static void IsMaxOpeningPosition_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is BistableDeviceStatusIndicator BistableDeviceStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    BistableDeviceStatusIndicator.IsMaxOpeningPositionIconVisibility = Visibility.Visible;
                else
                    BistableDeviceStatusIndicator.IsMaxOpeningPositionIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsMaxOpeningPositionIconVisibility;
        public Visibility IsMaxOpeningPositionIconVisibility
        {
            get { return _IsMaxOpeningPositionIconVisibility; }
            set { _IsMaxOpeningPositionIconVisibility = value; NotifyPropertyChanged("IsMaxOpeningPositionIconVisibility"); }
        }
        #endregion IsMaxOpeningPosition
        #endregion Max position


        #region Opening/Closing
        #region IsWorkOpening
        public bool IsWorkOpening
        {
            get { return (bool)GetValue(IsWorkOpeningProperty); }
            set { SetValue(IsWorkOpeningProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsWorkOpening.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsWorkOpeningProperty =
            DependencyProperty.Register("IsWorkOpening", typeof(bool), typeof(BistableDeviceStatusIndicator), new PropertyMetadata(false, IsWorkOpening_PropertyChanged));

        private static void IsWorkOpening_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is BistableDeviceStatusIndicator BistableDeviceStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    BistableDeviceStatusIndicator.IsWorkOpeningIconVisibility = Visibility.Visible;
                else
                    BistableDeviceStatusIndicator.IsWorkOpeningIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsWorkOpeningIconVisibility;
        public Visibility IsWorkOpeningIconVisibility
        {
            get { return _IsWorkOpeningIconVisibility; }
            set { _IsWorkOpeningIconVisibility = value; NotifyPropertyChanged("IsWorkOpeningIconVisibility"); }
        }
        #endregion IsWorkOpening

        #region IsWorkClosing
        public bool IsWorkClosing
        {
            get { return (bool)GetValue(IsWorkClosingProperty); }
            set { SetValue(IsWorkClosingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsWorkClosing.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsWorkClosingProperty =
            DependencyProperty.Register("IsWorkClosing", typeof(bool), typeof(BistableDeviceStatusIndicator), new PropertyMetadata(false, IsWorkClosing_PropertyChanged));

        private static void IsWorkClosing_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is BistableDeviceStatusIndicator BistableDeviceStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    BistableDeviceStatusIndicator.IsWorkClosingIconVisibility = Visibility.Visible;
                else
                    BistableDeviceStatusIndicator.IsWorkClosingIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsWorkClosingIconVisibility;
        public Visibility IsWorkClosingIconVisibility
        {
            get { return _IsWorkClosingIconVisibility; }
            set { _IsWorkClosingIconVisibility = value; NotifyPropertyChanged("IsWorkClosingIconVisibility"); }
        }
        #endregion IsWorkClosing
        #endregion Opening/Closing

        static BistableDeviceStatusIndicator()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(BistableDeviceStatusIndicator), new FrameworkPropertyMetadata(typeof(BistableDeviceStatusIndicator)));
        }
    }
}
