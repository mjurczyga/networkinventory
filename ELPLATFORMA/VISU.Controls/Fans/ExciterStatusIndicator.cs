﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace VISU.Controls.Fans
{
    public class ExciterStatusIndicator : NotifyControl
    {
        #region VoltageCOntrol
        #region IsOk_220VDC
        public bool IsOk_220VDC
        {
            get { return (bool)GetValue(IsOk_220VDCProperty); }
            set { SetValue(IsOk_220VDCProperty, value); }
        }

        public static readonly DependencyProperty IsOk_220VDCProperty =
            DependencyProperty.Register("IsOk_220VDC", typeof(bool), typeof(BistableDeviceStatusIndicator), new PropertyMetadata(false, IsOk_220VDC_PropertyChanged));

        private static void IsOk_220VDC_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ExciterStatusIndicator exciterStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    exciterStatusIndicator.IsOk_220VDCIconVisibility = Visibility.Visible;
                else
                    exciterStatusIndicator.IsOk_220VDCIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsOk_220VDCIconVisibility;
        public Visibility IsOk_220VDCIconVisibility
        {
            get { return _IsOk_220VDCIconVisibility; }
            set { _IsOk_220VDCIconVisibility = value; NotifyPropertyChanged("IsOk_220VDCIconVisibility"); }
        }
        #endregion IsOk_220VDC

        #region IsOk_24VDC
        public bool IsOk_24VDC
        {
            get { return (bool)GetValue(IsOk_24VDCProperty); }
            set { SetValue(IsOk_24VDCProperty, value); }
        }

        public static readonly DependencyProperty IsOk_24VDCProperty =
            DependencyProperty.Register("IsOk_24VDC", typeof(bool), typeof(BistableDeviceStatusIndicator), new PropertyMetadata(false, IsOk_24VDC_PropertyChanged));

        private static void IsOk_24VDC_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ExciterStatusIndicator exciterStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    exciterStatusIndicator.IsOk_24VDCIconVisibility = Visibility.Visible;
                else
                    exciterStatusIndicator.IsOk_24VDCIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsOk_24VDCIconVisibility;
        public Visibility IsOk_24VDCIconVisibility
        {
            get { return _IsOk_24VDCIconVisibility; }
            set { _IsOk_24VDCIconVisibility = value; NotifyPropertyChanged("IsOk_24VDCIconVisibility"); }
        }
        #endregion IsOk_24VDC

        #region IsOk_230VAC
        public bool IsOk_230VAC
        {
            get { return (bool)GetValue(IsOk_230VACProperty); }
            set { SetValue(IsOk_230VACProperty, value); }
        }

        public static readonly DependencyProperty IsOk_230VACProperty =
            DependencyProperty.Register("IsOk_230VAC", typeof(bool), typeof(BistableDeviceStatusIndicator), new PropertyMetadata(false, IsOk_230VAC_PropertyChanged));

        private static void IsOk_230VAC_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ExciterStatusIndicator exciterStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    exciterStatusIndicator.IsOk_230VACIconVisibility = Visibility.Visible;
                else
                    exciterStatusIndicator.IsOk_230VACIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsOk_230VACIconVisibility;
        public Visibility IsOk_230VACIconVisibility
        {
            get { return _IsOk_230VACIconVisibility; }
            set { _IsOk_230VACIconVisibility = value; NotifyPropertyChanged("IsOk_230VACIconVisibility"); }
        }
        #endregion IsOk_230VAC
        #endregion

        #region IsReady
        public bool IsReady
        {
            get { return (bool)GetValue(IsReadyProperty); }
            set { SetValue(IsReadyProperty, value); }
        }

        public static readonly DependencyProperty IsReadyProperty =
            DependencyProperty.Register("IsReady", typeof(bool), typeof(BistableDeviceStatusIndicator), new PropertyMetadata(false, IsReady_PropertyChanged));

        private static void IsReady_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ExciterStatusIndicator exciterStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    exciterStatusIndicator.IsReadyIconVisibility = Visibility.Visible;
                else
                    exciterStatusIndicator.IsReadyIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsReadyIconVisibility;
        public Visibility IsReadyIconVisibility
        {
            get { return _IsReadyIconVisibility; }
            set { _IsReadyIconVisibility = value; NotifyPropertyChanged("IsReadyIconVisibility"); }
        }
        #endregion IsReady

        #region IsStarting
        public bool IsStarting
        {
            get { return (bool)GetValue(IsStartingProperty); }
            set { SetValue(IsStartingProperty, value); }
        }

        public static readonly DependencyProperty IsStartingProperty =
            DependencyProperty.Register("IsStarting", typeof(bool), typeof(BistableDeviceStatusIndicator), new PropertyMetadata(false, IsStarting_PropertyChanged));

        private static void IsStarting_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ExciterStatusIndicator exciterStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    exciterStatusIndicator.IsStartingIconVisibility = Visibility.Visible;
                else
                    exciterStatusIndicator.IsStartingIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsStartingIconVisibility;
        public Visibility IsStartingIconVisibility
        {
            get { return _IsStartingIconVisibility; }
            set { _IsStartingIconVisibility = value; NotifyPropertyChanged("IsStartingIconVisibility"); }
        }
        #endregion IsStarting

        #region IsWorking
        public bool IsWorking
        {
            get { return (bool)GetValue(IsWorkingProperty); }
            set { SetValue(IsWorkingProperty, value); }
        }

        public static readonly DependencyProperty IsWorkingProperty =
            DependencyProperty.Register("IsWorking", typeof(bool), typeof(BistableDeviceStatusIndicator), new PropertyMetadata(false, IsWorking_PropertyChanged));

        private static void IsWorking_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ExciterStatusIndicator exciterStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    exciterStatusIndicator.IsWorkingIconVisibility = Visibility.Visible;
                else
                    exciterStatusIndicator.IsWorkingIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsWorkingIconVisibility;
        public Visibility IsWorkingIconVisibility
        {
            get { return _IsWorkingIconVisibility; }
            set { _IsWorkingIconVisibility = value; NotifyPropertyChanged("IsWorkingIconVisibility"); }
        }
        #endregion IsWorking

        #region IsWarrning
        public bool IsWarrning
        {
            get { return (bool)GetValue(IsWarrningProperty); }
            set { SetValue(IsWarrningProperty, value); }
        }

        public static readonly DependencyProperty IsWarrningProperty =
            DependencyProperty.Register("IsWarrning", typeof(bool), typeof(BistableDeviceStatusIndicator), new PropertyMetadata(false, IsWarrning_PropertyChanged));

        private static void IsWarrning_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ExciterStatusIndicator exciterStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    exciterStatusIndicator.IsWarrningIconVisibility = Visibility.Visible;
                else
                    exciterStatusIndicator.IsWarrningIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsWarrningIconVisibility;
        public Visibility IsWarrningIconVisibility
        {
            get { return _IsWarrningIconVisibility; }
            set { _IsWarrningIconVisibility = value; NotifyPropertyChanged("IsWarrningIconVisibility"); }
        }
        #endregion IsWarrning

        #region IsProtectionOn
        public bool IsProtectionOn
        {
            get { return (bool)GetValue(IsProtectionOnProperty); }
            set { SetValue(IsProtectionOnProperty, value); }
        }

        public static readonly DependencyProperty IsProtectionOnProperty =
            DependencyProperty.Register("IsProtectionOn", typeof(bool), typeof(BistableDeviceStatusIndicator), new PropertyMetadata(false, IsProtectionOn_PropertyChanged));

        private static void IsProtectionOn_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ExciterStatusIndicator exciterStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    exciterStatusIndicator.IsProtectionOnIconVisibility = Visibility.Visible;
                else
                    exciterStatusIndicator.IsProtectionOnIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsProtectionOnIconVisibility;
        public Visibility IsProtectionOnIconVisibility
        {
            get { return _IsProtectionOnIconVisibility; }
            set { _IsProtectionOnIconVisibility = value; NotifyPropertyChanged("IsProtectionOnIconVisibility"); }
        }
        #endregion IsProtectionOn

        #region CurrentSettingMethod 
        //false - Rw, true - RMB
        public bool CurrentSettingMethod 
        {
            get { return (bool)GetValue(CurrentSettingMethodProperty); }
            set { SetValue(CurrentSettingMethodProperty, value); }
        }

        public static readonly DependencyProperty CurrentSettingMethodProperty =
            DependencyProperty.Register("CurrentSettingMethod", typeof(bool), typeof(BistableDeviceStatusIndicator), new PropertyMetadata(false, CurrentSettingMethod_PropertyChanged));

        private static void CurrentSettingMethod_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ExciterStatusIndicator exciterStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    exciterStatusIndicator.CurrentSettingMethodIconVisibility = Visibility.Visible;
                else
                    exciterStatusIndicator.CurrentSettingMethodIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _CurrentSettingMethodIconVisibility;
        public Visibility CurrentSettingMethodIconVisibility
        {
            get { return _CurrentSettingMethodIconVisibility; }
            set { _CurrentSettingMethodIconVisibility = value; NotifyPropertyChanged("CurrentSettingMethodIconVisibility"); }
        }
        #endregion CurrentSettingMethod

        #region IsSafetyButton
        public bool IsSafetyButton
        {
            get { return (bool)GetValue(IsSafetyButtonProperty); }
            set { SetValue(IsSafetyButtonProperty, value); }
        }

        public static readonly DependencyProperty IsSafetyButtonProperty =
            DependencyProperty.Register("IsSafetyButton", typeof(bool), typeof(BistableDeviceStatusIndicator), new PropertyMetadata(false, IsSafetyButton_PropertyChanged));

        private static void IsSafetyButton_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ExciterStatusIndicator exciterStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    exciterStatusIndicator.IsSafetyButtonIconVisibility = Visibility.Visible;
                else
                    exciterStatusIndicator.IsSafetyButtonIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsSafetyButtonIconVisibility;
        public Visibility IsSafetyButtonIconVisibility
        {
            get { return _IsSafetyButtonIconVisibility; }
            set { _IsSafetyButtonIconVisibility = value; NotifyPropertyChanged("IsSafetyButtonIconVisibility"); }
        }
        #endregion IsSafetyButton

        #region IsDirtyFilter
        public bool IsDirtyFilter
        {
            get { return (bool)GetValue(IsDirtyFilterProperty); }
            set { SetValue(IsDirtyFilterProperty, value); }
        }

        public static readonly DependencyProperty IsDirtyFilterProperty =
            DependencyProperty.Register("IsDirtyFilter", typeof(bool), typeof(BistableDeviceStatusIndicator), new PropertyMetadata(false, IsDirtyFilter_PropertyChanged));

        private static void IsDirtyFilter_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ExciterStatusIndicator exciterStatusIndicator)
            {
                if (bool.TryParse(e.NewValue?.ToString(), out bool boolVal) && boolVal)
                    exciterStatusIndicator.IsDirtyFilterIconVisibility = Visibility.Visible;
                else
                    exciterStatusIndicator.IsDirtyFilterIconVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _IsDirtyFilterIconVisibility;
        public Visibility IsDirtyFilterIconVisibility
        {
            get { return _IsDirtyFilterIconVisibility; }
            set { _IsDirtyFilterIconVisibility = value; NotifyPropertyChanged("IsDirtyFilterIconVisibility"); }
        }
        #endregion IsDirtyFilter
    }
}
