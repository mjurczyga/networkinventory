﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using VISU.Controls.Interfaces;

namespace VISU.Controls.Fans
{
    public class BaseVarControl : NotifyControl, IBaseVarControl
    {      
        public object Value
        {
            get { return (object)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(object), typeof(BaseVarControl), new PropertyMetadata("Brak danych"));

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }
        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(BaseVarControl), new PropertyMetadata("Opis zmiennej"));

        public string Unit
        {
            get { return (string)GetValue(UnitProperty); }
            set { SetValue(UnitProperty, value); }
        }

        public static readonly DependencyProperty UnitProperty =
            DependencyProperty.Register("Unit", typeof(string), typeof(BaseVarControl), new PropertyMetadata(Unit_PropertyChanged));

        private static void Unit_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            BaseVarControl baseVarControl = (BaseVarControl)d;
            if (!string.IsNullOrEmpty((string)e.NewValue))
            {
                baseVarControl.InternalUnit = (string)e.NewValue;
                if (!baseVarControl.InternalUnit.Contains('['))
                    baseVarControl.InternalUnit = "[" + baseVarControl.InternalUnit;
                if (!baseVarControl.InternalUnit.Contains(']'))
                    baseVarControl.InternalUnit = baseVarControl.InternalUnit + "]";

                baseVarControl.InternalUnitVisibility = Visibility.Visible;
            }
            else
            {
                baseVarControl.InternalUnit = "";
                baseVarControl.InternalUnitVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _InternalUnitVisibility = Visibility.Collapsed;
        public Visibility InternalUnitVisibility
        {
            get { return _InternalUnitVisibility; }
            set { _InternalUnitVisibility = value; NotifyPropertyChanged("InternalUnitVisibility"); }
        }


        private string _InternalUnit;
        public string InternalUnit { get { return _InternalUnit; } protected set { _InternalUnit = value; NotifyPropertyChanged("InternalUnit"); } }
    }
}
